# Angular Starter



This project serves as an [Angular best practices](https://pokermaps.atlassian.net/wiki/display/UAG/Styles) guide.
This Angular Starter application provides documented practices for `Out of the Box` development of `next-level-generation` applications.

All of the common libraries, tooling, and directory structure that have been identified and documented in our extensive developer [wikis](https://pokermaps.atlassian.net/wiki/display/UAG/UI+Architectural+Guidance).
This service is meant to allow easy ramp up for all new PokerMaps developers, on any PokerMaps Angular 2.x project.

This application is built with [Angular](https://angular.io/) and utilizes a number of 3rd party dependencies including:

 - [Karma](http://karma-runner.github.io/0.12/index.html) test runner
 - [Jasmine](http://jasmine.github.io/) framework for unit testing
 - [mocha](https://github.com/mochajs/mocha/wiki) test reporter
 - [NPM](https://www.npmjs.com/) package manager
 - [Webpack 2](https://webpack.js.org/) module loader / bundler
 - [RxJS](http://reactivex.io/) Reactive Observables extension for JavaScript
 - [ImmutableJS](https://facebook.github.io/immutable-js/) immutable Data for JavaScript applications
 - [TypeScript](http://www.typescriptlang.org/) typed superset of JavaScript
 - [@Types](https://www.npmjs.com/~types) npm type definition packages
 - [TSLint](http://palantir.github.io/tslint/) pluggable linting utility for TypeScript
 - [TypeDoc](http://typedoc.io/) for generating code documentation based on source code comments
 - [D3.js](http://d3js.org/) for advanced data visualizations and animations
 - [Bootstrap](http://getbootstrap.com/) CSS Framework for responsive, mobile-first web projects
 - [Font Awesome](http://fortawesome.github.io/Font-Awesome/) icon and css toolkit
 - [compodoc](https://github.com/compodoc/compodoc) documentation tool for Angular applications
 - Built in support for layout and styling of the application based on [SASS](http://sass-lang.com/)

## Node Version
* Running on [Node](https://nodejs.org/en/) v6.x.x LTS

## Global Prerequisites Installation `npm install whatever -g`
  - cross-env
  - istanbul
  - karma-cli
  - npm
  - rimraf
  - stylelint
  - tslint
  - typescript
  - compodoc

## Local Prerequisites Installation

1. Install [GIT](http://git-scm.com/) if it isn't already
2. Install [NodeJS/NPM](http://nodejs.org/) if it isn't already
3. cd into the project directory from terminal/command prompt (NOTE: if using Windows be sure and launch command prompt as [Administrator](http://www.howtogeek.com/194041/how-to-open-the-command-prompt-as-administrator-in-windows-8.1/))
4. Install Client dependencies by typing `npm run clean-install`
5. Run dev build `npm run debug:mock`

## npm Commands

* Generate code documentation: `npm run docs`
* Execute unit tests without watching for changes: `npm run test`
* Execute unit tests with watch: `npm run test-watch`
* Lint TypeScript source code: `npm run lint`
* Lint SASS stylesheets: `npm run stylelint`
* Debug application locally with mock API: `npm run debug:mock`
* Debug application locally with 'wrapped' API: `npm run debug:wrap`
* Debug application locally with live data API: `npm run debug:live`
* Debug application locally with live data QA API and minified code: `npm run debug:test`
* Debug application locally with live data Staging and AND minified code: `npm run debug:stage`
* Debug application locally with live data Production and AND minified code: `npm run debug:prod`
* Debug application locally with mock API and minified code: `npm run debug:prod:mock`
* Build application using mock API with unminified code: `npm run build:dev:mock`
* Build application using 'wrapped' API with unminified code: `npm run build:dev:wrap`
* Build application using live API with unminified code targeting DEV environment: `npm run build:dev:live`
* Build application using live API minified code targeting QA environment: `npm run build:test`
* Build application using live API minified code targeting STAGING environment: `npm run build:stage`
* Build application using live API minified code targeting PROD environment: `npm run build:prod`

## Further Information

Refer to [Angular 2.x - Application Solution Structure](https://pokermaps.atlassian.net/wiki/display/UAG/Application+Solution+Structure)

Refer to [package.json](https://bitbucket.org/teampoker/ng2_pokermaps_ui/branch/master) for more build commands
