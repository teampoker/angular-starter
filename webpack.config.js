switch (process.env.NODE_ENV) {
    case 'prod' :
    case 'production' :
        module.exports = require('./config/webpack.prod');
        break;
    case 'stage':
        module.exports = require('./config/webpack.stage');
        break;
    case 'test' :
    case 'testing' :
        module.exports = require('./config/webpack.test.js');
        break;
    case 'karma':
        module.exports = require('./config/webpack.karma.js');
        break;
    case 'dev' :
    case 'development' :
    default:
        module.exports = require('./config/webpack.dev');
        break;
}
