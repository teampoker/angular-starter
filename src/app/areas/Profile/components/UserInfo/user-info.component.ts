import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    ViewChild
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';

import {UserInfoState} from '../../../../store/User/types/user-info-state.model';
import {IKeyValuePair} from '../../../../store/types/key-value-pair.model';

@Component({
    selector        : 'user-info',
    templateUrl     : './user-info.component.html',
    styleUrls       : ['./user-info.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for UserInfoComponent: displays user profile info and handles updates to the information
 */
export class UserInfoComponent {
    /**
     * UserInfoComponent constructor
     */
    constructor (private builder : FormBuilder) {
        // init form input fields
        this.employeeId     = new FormControl('', Validators.required);
        this.language       = new FormControl('', Validators.required);
        this.street         = new FormControl('', Validators.required);
        this.city           = new FormControl('', Validators.required);
        this.state          = new FormControl('', Validators.required);
        this.postalCode     = new FormControl('', Validators.required);
        this.phoneNumber    = new FormControl('', Validators.required);
        this.emailAddress   = new FormControl('', Validators.required);

        // build FormControl group
        this.userProfileForm = builder.group({
            employeeId      : this.employeeId,
            language        : this.language,
            street          : this.street,
            city            : this.city,
            state           : this.state,
            postalCode      : this.postalCode,
            phoneNumber     : this.phoneNumber,
            emailAddress    : this.emailAddress
        });

        // subscribe to any form changes
        this.employeeId.valueChanges.debounceTime(250).subscribe((value : string) => {
            // update model with latest value
            this.profileDetails = this.profileDetails.set('employeeId', value) as UserInfoState;
        });

        this.language.valueChanges.debounceTime(250).subscribe((value : string) => {
            // update model with latest value
            this.profileDetails = this.profileDetails.set('language', value) as UserInfoState;
        });

        this.street.valueChanges.debounceTime(250).subscribe((value : string) => {
            // update model with latest value
            this.profileDetails = this.profileDetails.set('street', value) as UserInfoState;
        });

        this.city.valueChanges.debounceTime(250).subscribe((value : string) => {
            // update model with latest value
            this.profileDetails = this.profileDetails.set('city', value) as UserInfoState;
        });

        this.state.valueChanges.debounceTime(250).subscribe((value : string) => {
            // update model with latest value
            this.profileDetails = this.profileDetails.set('state', value) as UserInfoState;
        });

        this.postalCode.valueChanges.debounceTime(250).subscribe((value : string) => {
            // update model with latest value
            this.profileDetails = this.profileDetails.set('postalCode', value) as UserInfoState;
        });
    }

    /**
     * DOM reference for pikaday datepicker
     */
    @ViewChild('datePickerButton') datePickerButton : any;

    /**
     * logged in user's display name
     * @type {UserInfoState}
     */
    @Input() profileDetails : UserInfoState;

    /**
     * controls the collapsed state of User Info section
     * @type {boolean}
     */
    @Input() isExpanded : boolean = false;

    /**
     * flag to keep track of when the user profile form is being edited
     * @type {boolean}
     */
    @Input() isEditing : boolean = false;

    /**
     * flag to allow component to
     */
    @Input() isCollapsible : any;

    /**
     * event triggered when user's profile information is edited/saved
     * @type {"events".EventEmitter}
     */
    @Output() updateProfile : EventEmitter<any> = new EventEmitter();

    /**
     * event triggered when user expands/collapses Personal Information section
     * @type {EventEmitter}
     */
    @Output() toggleExpanded : EventEmitter<any> = new EventEmitter();

    /**
     * event triggered when user edits the personal information section
     * @type {EventEmitter}
     */
    @Output() toggleEditing : EventEmitter<boolean> = new EventEmitter<boolean>(false);

    /**
     * used to store pristine copy of form data
     * in case user cancels their changes when editing
     */
    private userProfilePristine : UserInfoState = new UserInfoState();

    /**
     * grouping object for user profile form
     */
    userProfileForm : FormGroup;

    /**
     * user's employeeId input field
     */
    employeeId : FormControl;

    /**
     * user's language preference input field
     */
    language : FormControl;

    /**
     * user's street address input field
     */
    street : FormControl;

    /**
     * user's city input field
     */
    city : FormControl;

    /**
     * user's state input field
     */
    state : FormControl;

    /**
     * user's zip code input field
     */
    postalCode : FormControl;

    /**
     * user's phone number input field
     */
    phoneNumber : FormControl;

    /**
     * user's email addy input field
     */
    emailAddress : FormControl;

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * store user's selected phone type here for their contact phone #
     * @type {string}
     */
    selectedPhoneType : string = 'Phone';

    /**
     * store user's selected gender here
     * @type {string}
     */
    selectedGender : string = 'Male';

    /**
     * list of SVG icons being used in our template
     */
    icons : any = {
        edit       : require('images/SvgIcons/icon-edit.svg'),
        employeeId : require('images/SvgIcons/icon-employee-id.svg'),
        language   : require('images/SvgIcons/icon-language.svg'),
        address    : require('images/SvgIcons/icon-address.svg'),
        gender     : require('images/SvgIcons/icon-gender.svg'),
        birthday   : require('images/SvgIcons/icon-birthday.svg'),
        phone      : require('images/SvgIcons/icon-phone.svg'),
        email      : require('images/SvgIcons/icon-email.svg'),
        minus      : require('images/SvgIcons/icon-minus.svg'),
        plus       : require('images/SvgIcons/icon-plus.svg')
    };

    /**
     * list of available phone types for user to select
     * @type {[IKeyValuePair]}
     */
    phoneTypes : Array<IKeyValuePair> = [
        {
            id    : '0',
            value : 'Home'
        },
        {
            id    : '1',
            value : 'Phone'
        },
        {
            id    : '2',
            value : 'Other'
        }
    ];

    /**
     * list of available gender types for user to select
     * @type {[IKeyValuePair]}
     */
    genders : Array<IKeyValuePair> = [
        {
            id    : '0',
            value : 'Male'
        },
        {
            id    : '1',
            value : 'Female'
        }
    ];

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * format the selected birth date
     * @param date
     */
    updateSelectedDOB(date : string) {
        this.profileDetails = this.profileDetails.set('DOB', date) as UserInfoState;
    }

    /**
     * event handler for hide/show collapse toggle
     */
    toggleExpandedState() {
        // is enabled for hide/show??
        if (this.isCollapsible) {
            // trigger event
            this.toggleExpanded.emit(undefined);
        }
    }

    /**
     * handles enable/disable of editing fields on user profile form
     * @param action
     * @param $event
     */
    enableEdit(action : boolean, $event : any) {
        $event.preventDefault();
        $event.stopPropagation();

        // if user is editing, make a pristine copy of form data
        // if they are canceling an edit without saving we need to restore
        // the form data with the original values passed back from api
        action ? this.userProfilePristine = this.userProfilePristine.mergeDeep(this.profileDetails) as UserInfoState : this.profileDetails = this.profileDetails.mergeDeep(this.userProfilePristine) as UserInfoState;

        // update current editing state of form
        this.toggleEditing.emit(action);

        // expand section below when edit icon clicked
        if (!this.isExpanded && action) {
            this.toggleExpanded.emit(undefined);
        }
    }

    /**
     * event handler for phone type selection event
     * @param choice selected phone tye
     * @param event DOM event
     */
    updatePhoneType(choice : { id : number, value : string }, event : any) {
        // update profile phone type
        this.profileDetails = this.profileDetails.set('phoneType', choice.value) as UserInfoState;
    }

    /**
     * event handler for gender selection event
     * @param choice selected phone tye
     * @param event DOM event
     */
    updateGender(choice : { id : number, value : string }, event : any) {
        // update profile phone type
        this.profileDetails = this.profileDetails.set('gender', choice.value) as UserInfoState;
    }

    /**
     * triggers save of user's edited profile data
     * @param $event
     */
    saveUserProfile($event : any) {
        $event.preventDefault();
        $event.stopPropagation();

        // update current editing state of form
        this.toggleEditing.emit(false);

        // trigger save user event
        this.updateProfile.emit(this.profileDetails);
    }
}
