import {NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {ProfileRoutingModule} from './profile-routing.module';
import {ProfileEntryComponent} from './profile-entry.component';
import {UserInfoComponent} from './components/UserInfo/user-info.component';

@NgModule({
    imports         : [
        SharedModule,
        ProfileRoutingModule
   ],
    declarations    : [
        ProfileEntryComponent,
        UserInfoComponent
   ],
    providers       : []
})

export class ProfileModule {

}
