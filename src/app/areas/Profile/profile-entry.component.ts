import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

import {IAppStore} from '../../store/app-store';
import {UserService} from '../../shared/services/User/user.service';
import {UserState} from '../../store/User/types/user-state.model';
import {UserInfoState} from '../../store/User/types/user-info-state.model';

@Component({
    templateUrl     : 'profile-entry.component.html',
    styleUrls       : ['profile-entry.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ProfileEntryComponent: responsible for displaying logged in user profile details
 */
export class ProfileEntryComponent {
    /**
     * ProfileEntryComponent constructor
     * @param userService
     * @param store
     * @param router
     * @param cd
     */
    constructor (
        private userService     : UserService,
        private store           : NgRedux<IAppStore>,
        private router          : Router,
        private cd              : ChangeDetectorRef
    ) {
        // observe user state
        this.userProfileSubscription = this.store.select(state => state.userState).subscribe(val => {
            // did we get new data?
            if (val.equals(this.userState) !== true) {
                // update local state
                this.userState = val;

                // mark the path from root of component tree to this component
                // for change detected on the next tick
                // We do this because we're set to ChangeDetectionStrategy.OnPush
                this.cd.markForCheck();
            }
        });
    }

    /**
     * Redux state subscription
     */
    private userProfileSubscription : Subscription;

    /**
     * current snapshot of user state
     */
    userState : UserState;

    /**
     * list of SVG icons being used in our template
     * @type {{user: any}}
     */
    icons : any = {
        close   : require('images/SvgIcons/icon-close.svg'),
        minus   : require('images/SvgIcons/icon-minus.svg'),
        plus    : require('images/SvgIcons/icon-plus.svg'),
        user    : require('images/SvgIcons/icon-user.svg')
    };

    /**
     * close profile and send user back to landing page
     */
    closeProfile() {
        this.router.navigate(['/Dashboard']);
    }

    /**
     * event handler for on updated user profile event
     * @param event the updated information for the user's profile
     */
    onUpdateProfile(event : UserInfoState) {
        // update user's profile
        this.userService.updateUser(event);
    }
}
