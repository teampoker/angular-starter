import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ProfileEntryComponent} from './profile-entry.component';

@NgModule({
    imports : [
        RouterModule.forChild([
            {
                path        : '',
                component   : ProfileEntryComponent
            }
        ])
   ],
    exports : [
        RouterModule
    ]
})

export class ProfileRoutingModule {

}
