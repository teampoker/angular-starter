import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {DashboardEntryComponent} from './dashboard-entry.component';
import {DashboardBodyContainerComponent} from './components/container/DashboardBodyContainer/dashboard-body-container.component';
import {DashboardResolveService} from './services/DashboardResolveService/dashboard-resolve.service';

@NgModule({
    imports : [
        RouterModule.forChild([
            {
                path        : '',
                redirectTo  : '/Dashboard',
                pathMatch   : 'full'

            },
            {
                path        : 'Dashboard',
                component   : DashboardEntryComponent,
                children    : [
                    {
                        path        : '',
                        component   : DashboardBodyContainerComponent,
                        resolve         : {
                            DashboardResolveService
                        }
                    }
                ]
            }
        ])
   ],
    exports : [
        RouterModule
   ],
    providers : [
        DashboardResolveService
    ]
})

export class DashboardRoutingModule {

}
