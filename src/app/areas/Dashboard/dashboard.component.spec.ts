import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {Http} from '@angular/http';
import {RouterTestingModule} from '@angular/router/testing';
import {
    TranslateModule,
    TranslateLoader,
    TranslateStaticLoader
} from 'ng2-translate';

import {EnumNavOption} from '../../store/Navigation/types/nav-option.model';
import {DashboardEntryComponent} from './dashboard-entry.component';
import {DashboardHeaderComponent} from './components/presentational/DashboardHeader/dashboard-header.component';
import {NavActions} from '../../store/Navigation/nav.actions';

// mock dependencies
class MockNavActions {
    constructor() {}

    updateActiveNavState(navOption : EnumNavOption) {}
}

// tests
describe('Component: DashboardEntryComponent', () => {
    let fixture         : ComponentFixture<DashboardEntryComponent>,
        compInstance    : DashboardEntryComponent,
        element         : any,
        navActions      : MockNavActions;

    // use the async() wrapper to allow the Angular template compiler time to read the files
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports : [
                TranslateModule.forRoot({
                    provide     : TranslateLoader,
                    useFactory  : (http : Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
                    deps        : [Http]
                }),
                ReactiveFormsModule,
                RouterTestingModule
            ],
            declarations : [
                DashboardEntryComponent,
                DashboardHeaderComponent
            ],
            providers : [
                {
                    provide     : NavActions,
                    useClass    : MockNavActions
                }
            ]
        });

        // create component fixture
        fixture = TestBed.createComponent(DashboardEntryComponent);

        // grab instance of component class
        compInstance = fixture.componentInstance;

        // grab DOM representation
        element = fixture.nativeElement;

        // grab Mock from the root injector
        navActions = fixture.debugElement.injector.get(NavActions);

        // trigger initial bindings
        fixture.detectChanges();
    }));

    it('should be initialized', () => {
        expect(compInstance).toBeDefined();
    });

    it('should set active epic to dashboard on init ', () => {
        // spy on action creator method
        spyOn(navActions, 'updateActiveNavState');

        // retrigger ngOnInit
        compInstance.ngOnInit();

        // verify dispatch
        expect(navActions.updateActiveNavState).toHaveBeenCalledWith(EnumNavOption.DASHBOARD);
    });
});
