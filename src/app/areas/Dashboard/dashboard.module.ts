import {NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashboardEntryComponent} from './dashboard-entry.component';
import {DashboardHeaderComponent} from './components/presentational/DashboardHeader/dashboard-header.component';
import {DashboardBodyContainerComponent} from './components/container/DashboardBodyContainer/dashboard-body-container.component';
import {ActivityLogPanelComponent} from './components/presentational/ActivityLogPanel/activity-log-panel.component';
import {CallActivityPanelComponent} from './components/presentational/CallActivityPanel/call-activity-panel.component';
import {CsrRequestsPanelComponent} from './components/presentational/CsrRequestsPanel/csr-requests-panel.component';
import {CurrentDateWidgetComponent} from './components/presentational/CurrentDateWidget/current-date-widget.component';
import {CurrentTimeWidgetComponent} from './components/presentational/CurrentTimeWidget/current-time-widget.component';
import {EligibilityQueuePanelComponent} from './components/presentational/EligibilityQueuePanel/eligibility-queue-panel.component';

@NgModule({
    imports         : [
        SharedModule,
        DashboardRoutingModule
   ],
    declarations    : [
        DashboardEntryComponent,
        DashboardHeaderComponent,
        DashboardBodyContainerComponent,
        ActivityLogPanelComponent,
        CallActivityPanelComponent,
        CsrRequestsPanelComponent,
        CurrentDateWidgetComponent,
        CurrentTimeWidgetComponent,
        EligibilityQueuePanelComponent
   ],
    providers       : []
})

export class DashboardModule {

}
