import {
    Component,
    ChangeDetectionStrategy,
    Input
} from '@angular/core';

@Component({
    selector        : 'current-date-widget',
    templateUrl     : 'current-date-widget.component.html',
    styleUrls       : ['current-date-widget.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for CurrentDateWidget: handles display of Current Date in dashboard header
 */
export class CurrentDateWidgetComponent {

    /**
     * CurrentDateWidget constructor
     */
    constructor() {}

    @Input() currentDateTime : string;
}
