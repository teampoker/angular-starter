import {
    Component,
    ChangeDetectionStrategy,
    Input
} from '@angular/core';

@Component({
    selector        : 'current-time-widget',
    templateUrl     : 'current-time-widget.component.html',
    styleUrls       : ['current-time-widget.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for CurrentTimeWidget: handles display of dashboard current time
 */
export class CurrentTimeWidgetComponent {

    /**
     * CurrentTimeWidget constructor
     */
    constructor() {

    }

    @Input() currentDateTime : string;
}
