import {
    Component,
    Input
} from '@angular/core';

import {CallActivity} from '../../../../../store/Dashboard/types/call-activity.model';

@Component({
    selector        : 'call-activity-panel',
    templateUrl     : 'call-activity-panel.component.html',
    styleUrls       : ['call-activity-panel.component.scss']
})

/**
 * Implementation for CallActivityPanelComponent: handles display of section panels
 */
export class CallActivityPanelComponent {
    /**
     * CallActivityPanelComponent constructor
     */
    constructor() {}

    @Input() callDetails : CallActivity;

    /**
     * component init lifecycle hook
     */
    ngOnInit() {

    }
}
