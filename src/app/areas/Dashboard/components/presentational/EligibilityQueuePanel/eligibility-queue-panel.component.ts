import {
    Component,
    Input
} from '@angular/core';

import {EligibilityQueue} from '../../../../../store/Dashboard/types/eligibility-queue.model';

@Component({
    selector        : 'eligibility-queue-panel',
    templateUrl     : 'eligibility-queue-panel.component.html',
    styleUrls       : ['eligibility-queue-panel.component.scss']
})

/**
 * Implementation for EligibilityQueuePanelComponent: handles display of section panels
 */
export class EligibilityQueuePanelComponent {
    /**
     * CallActivityPanelComponent constructor
     */
    constructor() {}

    @Input() eligibilityDetails : EligibilityQueue;

    /**
     * component init lifecycle hook
     */
    ngOnInit() {

    }
}
