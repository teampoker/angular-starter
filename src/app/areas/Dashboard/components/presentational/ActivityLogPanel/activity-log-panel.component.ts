import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    selector        : 'activity-log-panel',
    templateUrl     : 'activity-log-panel.component.html',
    styleUrls       : ['activity-log-panel.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ActivityLogPanelComponent: handles display of section panels
 */
export class ActivityLogPanelComponent {
    /**
     * ActivityLogPanelComponent constructor
     */
    constructor() {}
}
