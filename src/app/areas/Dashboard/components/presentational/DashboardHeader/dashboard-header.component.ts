import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    selector        : 'dashboard-header',
    templateUrl     : 'dashboard-header.component.html',
    styleUrls       : ['dashboard-header.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for DashboardHeaderComponent: handles display of dashboard status cards title
 */
export class DashboardHeaderComponent {
    /**
     * DashboardHeaderComponent constructor
     */
    constructor() {}
}
