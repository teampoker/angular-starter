import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    selector        : 'csr-requests-panel',
    templateUrl     : 'csr-requests-panel.component.html',
    styleUrls       : ['csr-requests-panel.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for CsrRequestsPanelComponent: handles display of section panels
 */
export class CsrRequestsPanelComponent {
    /**
     * CsrRequestsPanelComponent constructor
     */
    constructor() {}
}
