import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

import {DashboardStateSelectors} from '../../../../../store/Dashboard/dashboard.selectors';
import {DashboardActions} from '../../../../../store/Dashboard/dashboard.actions';
import {DashboardInfo} from '../../../../../store/Dashboard/types/dashboard-info.model';
import {CallActivity} from '../../../../../store/Dashboard/types/call-activity.model';
import {EligibilityQueue} from '../../../../../store/Dashboard/types/eligibility-queue.model';

@Component({
    selector        : 'dashboard-body-container',
    templateUrl     : 'dashboard-body-container..component.html',
    styleUrls       : ['dashboard-body-container..component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for DashboardBodyContainerComponent: handles display of dashboard status cards
 */
export class DashboardBodyContainerComponent {
    /**
     * DashboardBodyContainerComponent constructor
     * @param dashboardSelectors
     * @param cd
     * @param dashboardActions
     */
    constructor(
        private dashboardSelectors  : DashboardStateSelectors,
        private cd                  : ChangeDetectorRef,
        private dashboardActions    : DashboardActions
    ) {
        // setup subscription to dashboard state
        this.dashboardSubscription = Observable.combineLatest(
            this.dashboardSelectors.dashboard(),
            this.dashboardSelectors.callActivity(),
            this.dashboardSelectors.currentDateTime() // ,
            // this.dashboardSelectors.eligibilityQueue()
        )
        .subscribe(val => {
            // update local state
            this.dashboard           = val[0];
            this.callDetails         = val[1];
            this.currentDateTime     = val[2];
            // this.eligibilityDetails  = val[3];

            // trigger change detection
            this.cd.markForCheck();
        });
    }

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * Redux state subscription
     */
    private dashboardSubscription : Subscription;

    /**
     * current snapshot of dashboard state
     */
    dashboard : DashboardInfo;

    /**
     * current snapshot of callActivity state
     */
    callDetails : CallActivity;

    /**
     * current snapshot of eligibilityQueue state
     */
    eligibilityDetails : EligibilityQueue;

    /**
     * current snapshot of currentDateTime state
     */
    currentDateTime : string;

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        this.dashboardActions.getCallActivity();
        // this.dashboardActions.getEligibilityQueue();
        this.dashboardActions.currentDateTimeRefresh();
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        // unsubscribe
        this.dashboardSubscription.unsubscribe();
    }
}
