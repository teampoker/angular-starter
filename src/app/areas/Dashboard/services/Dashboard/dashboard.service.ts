import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as moment from 'moment';

import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {DASHBOARD_MOCK} from './dashboard.service.mock';

@Injectable()

/**
 * Implementation of DashboardService: returns summary data for display on Admin dashboard
 */
export class DashboardService {
    /**
     * DashBoardService constructor
     * @param backendService
     */
    constructor(private backendService : BackendService) {}

    private dateTimeInterval : any;

    /**
     * retrieve the current dashboard display data
     */
    getDashboard() {
        const handle  : string = 'getDashboard',
              apiUrl  : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let dashboard;

            // refresh data dashboard
            this.backendService.get(apiUrl, handle, DASHBOARD_MOCK)
                .first()
                .subscribe(response => {
                    // store dashboard data
                    response.length ? dashboard = response[0] : dashboard = response;

                    // update Redux store
                    observer.next(dashboard);
                }, error => {
                    observer.error(undefined);
                });
        });
    }

    /**
     * retrieve the current callActivity display data
     */
    getCallActivity() {
        return Observable.create(observer => {
            // let callActivity;

            // TODO remove this after release
            observer.next(DASHBOARD_MOCK.getCallActivity[0]);

            return false;
            // refresh callActivity data
            /*this.backendService.get('getCallActivity', DASHBOARD_MOCK)
             .first()
             .subscribe(response => {
             // store callActivity data
             response.length ? callActivity = response[0] : callActivity = response;

             // update Redux store
             observer.next(callActivity);
             }, error => {
             observer.error(undefined);
             });*/
        });
    }

    /**
     * retrieve the current EligibilityQueue display data
     */
    getEligibilityQueue() {
        const handle    : string = 'getEligibilityQueue',
              apiUrl    : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let eligibilityQueue;

            // refresh eligibilityQueue data
            this.backendService.get(apiUrl, handle, DASHBOARD_MOCK)
                .first()
                .subscribe(response => {
                    // store callActivity data
                    response.length ? eligibilityQueue = response[0] : eligibilityQueue = response;

                    // update Redux store
                    observer.next(eligibilityQueue);
                }, error => {
                    observer.error(undefined);
                });
        });
    }

    /**
     * retrieve the current taskList display data
     */
    getTaskList() {
        const handle    : string = 'getTaskList',
              apiUrl    : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let taskList;

            // refresh eligibilityQueue data
            this.backendService.get(apiUrl, handle, DASHBOARD_MOCK)
                .first()
                .subscribe(response => {
                    // store callActivity data
                    response.length ? taskList = response[0] : taskList = response;

                    // update Redux store
                    observer.next(taskList);
                }, error => {
                    observer.error(undefined);
                });
        });
    }

    /**
     * retrieves current date time stamp and creates interval
     * to continually update this value over time for display in the UI
     * @returns {any}
     */
    dateTimeRefresh() : Observable<any> {
        return Observable.create(observer => {
            observer.next(moment().format());

            this.dateTimeInterval = setInterval(() => {
                observer.next(moment().format());
            }, 30000);
        });
    }
}
