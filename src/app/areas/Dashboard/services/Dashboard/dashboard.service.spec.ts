import {
    async,
    inject,
    TestBed
} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {APIMockService} from '../../../../shared/services/Mock/api-mock.service';
import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {DashboardService} from './dashboard.service';
import {DASHBOARD_MOCK} from './dashboard.service.mock';

describe('DashboardService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports     : [HttpModule],
            providers   : [
                APIMockService,
                BackendService,
                DashboardService
            ]
        });
    });

    it('should get the current dashboard display data', async(inject([DashboardService], dashboardService => {
        dashboardService.getDashboard().subscribe(response => {
            expect(response).toEqual(DASHBOARD_MOCK.getDashboard[0]);
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));

    it('should get the current callActivity display data', async(inject([DashboardService], dashboardService => {
        dashboardService.getCallActivity().subscribe(response => {
            expect(response).toEqual(DASHBOARD_MOCK.getCallActivity[0]);
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));
});
