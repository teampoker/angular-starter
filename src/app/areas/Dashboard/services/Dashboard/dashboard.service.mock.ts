export const DASHBOARD_MOCK : any = {
    // TODO: need example data to mock for tests
    getDashboard : [{
    }],
    getDashboardWrap : {
    },
    getCallActivity : [{
        callsReceived: 62,
        callsPlaced: 849,
        avgCallTime: '8m33s'
    }],
    getCallActivityWrap: {
    },
    getEligibilityQueue : [{
        nextDay: 55,
        urgent: 12,
        standard: 124
    }],
    getEligibilityQueueWrap: {
    }
};
