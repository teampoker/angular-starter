import {Injectable} from '@angular/core';
import {
    Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';

import {SearchActions} from '../../../../store/Search/search.actions';

@Injectable()

/**
 * Implementation of DashboardResolveService : ensure Beneficiary Profile Dashboard route loads with the correct BeneficiaryState values
 */
export class DashboardResolveService implements Resolve<boolean> {
    /**
     * DashboardResolveService constructor
     * @param searchActions
     */
    constructor(private searchActions : SearchActions) {}

    resolve(route : ActivatedRouteSnapshot) : boolean {
        // clear header search state
        this.searchActions.resetHeaderSearchState();

        return true;
    }
}
