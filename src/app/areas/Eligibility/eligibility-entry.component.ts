import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

import {NavActions} from '../../store/Navigation/nav.actions';
import {EnumNavOption} from '../../store/Navigation/types/nav-option.model';

@Component({
    templateUrl     : 'eligibility-entry.component.html',
    styleUrls       : ['eligibility-entry.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for EligibilityEntryComponent: responsible for eligibility page layout
 */
export class EligibilityEntryComponent {
    /**
     * EligibilityEntryComponent constructor
     * @param navActions
     */
    constructor (private navActions : NavActions) {}

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // set active epic to eligibility
        this.navActions.updateActiveNavState(EnumNavOption.ELIGIBILITY);
    }
}
