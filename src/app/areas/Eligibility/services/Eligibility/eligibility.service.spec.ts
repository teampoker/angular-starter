import {
    async,
    inject,
    TestBed
} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {APIMockService} from '../../../../shared/services/Mock/api-mock.service';
import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {EligibilityService} from './eligibility.service';

describe('EligibilityService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports     : [HttpModule],
            providers   : [
                APIMockService,
                BackendService,
                EligibilityService
            ]
        });
    });

    xit('should get the current eligibility display data', async(inject([EligibilityService], eligibilityService => {

    })));
});
