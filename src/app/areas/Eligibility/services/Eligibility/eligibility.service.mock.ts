export const ELIGIBILITY_MOCK : any = {

    getEligibilityRequests : [{
        beneficiary : {
            firstName : 'Jonathon',
            lastName : 'Doe'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '08/01/2016',
        assignedTo : {
            status : 'Unassigned',
            firstName : undefined
        },
        risk   : {
            level : 1,
            title : 'High'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Jane',
            lastName : 'Thomas'
        },
        plan : {
            name : 'Florida Blue',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '06/11/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'William',
            lastName : 'Jones',
            phone : '(555) 555-9988',
            email : 'william.jones@angular-starter.com'
        },
        risk   : {
            level : 1,
            title : 'High'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'David',
            lastName : 'Thomas'
        },
        plan : {
            name : 'Aetna',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '05/01/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Franklin',
            lastName : 'Smith',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 2,
            title : 'Medium'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Henry',
            lastName : 'Williams'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '03/20/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Patrick',
            lastName : 'Green',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 2,
            title : 'Medium'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Rebecca',
            lastName : 'Brown'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '03/20/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Becky',
            lastName : 'Ford',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 2,
            title : 'Medium'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'George',
            lastName : 'Hamilton'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '05/25/2016',
        assignedTo : {
            status : 'Unassigned',
            firstName : undefined
        },
        risk   : {
            level : 2,
            title : 'Medium'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Jack',
            lastName : 'Black'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '08/01/2016',
        assignedTo : {
            status : 'Unassigned',
            firstName : undefined
        },
        risk   : {
            level : 2,
            title : 'Medium'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Jane',
            lastName : 'West'
        },
        plan : {
            name : 'Florida Blue',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '06/11/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'William',
            lastName : 'Jones',
            phone : '(555) 555-9988',
            email : 'william.jones@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Harper',
            lastName : 'Thomas'
        },
        plan : {
            name : 'Aetna',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '05/01/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Franklin',
            lastName : 'Smith',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Paul',
            lastName : 'Davidson'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '03/20/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Patrick',
            lastName : 'Green',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Jimmy',
            lastName : 'Hendrix'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '03/20/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Becky',
            lastName : 'Ford',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Pat',
            lastName : 'Benetar'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '05/25/2016',
        assignedTo : {
            status : 'Unassigned',
            firstName : undefined
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Mark',
            lastName : 'smith'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '08/01/2016',
        assignedTo : {
            status : 'Unassigned',
            firstName : undefined
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Billy',
            lastName : 'Bob'
        },
        plan : {
            name : 'Florida Blue',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '06/11/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'William',
            lastName : 'Jones',
            phone : '(555) 555-9988',
            email : 'william.jones@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'David',
            lastName : 'Thomas'
        },
        plan : {
            name : 'Aetna',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '05/01/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Franklin',
            lastName : 'Smith',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Michael',
            lastName : 'Scott'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '03/20/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Patrick',
            lastName : 'Green',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Bobby',
            lastName : 'Brown'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '03/20/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Becky',
            lastName : 'Ford',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'George',
            lastName : 'Washington'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '05/25/2016',
        assignedTo : {
            status : 'Unassigned',
            firstName : undefined
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Jimmy',
            lastName : 'Paige'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '08/01/2016',
        assignedTo : {
            status : 'Unassigned',
            firstName : undefined
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Robert',
            lastName : 'Plant'
        },
        plan : {
            name : 'Florida Blue',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '06/11/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'William',
            lastName : 'Jones',
            phone : '(555) 555-9988',
            email : 'william.jones@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Ozzy',
            lastName : 'Osborne'
        },
        plan : {
            name : 'Aetna',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '05/01/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Franklin',
            lastName : 'Smith',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Rhianna',
            lastName : 'Davidson'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '03/20/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Patrick',
            lastName : 'Green',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Summer',
            lastName : 'Belle'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '03/20/2016',
        assignedTo : {
            status : 'Assigned',
            firstName : 'Becky',
            lastName : 'Ford',
            phone : '(555) 555-9988',
            email : 'franklin.smith@angular-starter.com'
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }, {
        beneficiary : {
            firstName : 'Nat',
            lastName : 'Cole'
        },
        plan : {
            name : 'UnitedHealthcare',
            phone : '(888) 555-1234',
            memberId : '1234567890',
            startDate : '08/01/2016',
            endDate : '08/31/2016'
        },
        state : 'GA',
        date : '05/25/2016',
        assignedTo : {
            status : 'Unassigned',
            firstName : undefined
        },
        risk   : {
            level : 10,
            title : 'Low'
        },
        status : 'In Review'
    }],
    getEligibilityRequestsWrap : {

    }
};
