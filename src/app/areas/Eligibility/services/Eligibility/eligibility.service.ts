import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {ELIGIBILITY_MOCK} from './eligibility.service.mock';

@Injectable()

/**
 * Implementation of EligibilityService: returns summary data for display on Admin eligibility
 */
export class EligibilityService {
    /**
     *  EligibilityService constructor
     *
     * @param backendService
     */
    constructor (private backendService : BackendService) {}

    /**
     * @description - search
     * @returns {Promise<void>|Promise<any|string>}
     */
    getEligibilityRequests() {
        const handle   : string = 'getEligibilityRequests',
              apiUrl   : string = API_CONFIG[handle];

        return Observable.create(observer => {
            this.backendService.get(apiUrl, handle, ELIGIBILITY_MOCK)
                .first()
                .subscribe(response => {
                    observer.next(response);
                }, error => {
                    observer.error(undefined);
                });
        });
    }
}
