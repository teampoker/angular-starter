import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector        : 'eligibility-header',
    templateUrl     : './eligibility-header.component.html',
    styleUrls       : ['./eligibility-header.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for EligibilityHeaderComponent: handles display of eligibility status cards title
 */
export class EligibilityHeaderComponent {
    /**
     * EligibilityHeaderComponent constructor
     */
    constructor(private router : Router) { }

    /**
     * Object containing eligibility information
     * @type {any}
     */
    @Input() eligibility : any;

    /**
     * Current list of eligibility results
     */
    @Input() requestList : any;

    /**
     * Object containing header information
     * @type {any}
     */
    @Input() info : any;
}
