import {
    Component,
    Input,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {TranslateService} from 'ng2-translate/ng2-translate';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../../../store/types/key-value-pair.model';
import {EligibilityService} from '../../services/Eligibility/eligibility.service';

@Component({
    selector        : 'eligibility-requests',
    templateUrl     : './eligibility-requests.component.html',
    styleUrls       : ['./eligibility-requests.component.scss'],
    changeDetection : ChangeDetectionStrategy.Default
})

/**
 * Implementation for EligibilityRequestsComponent: handles display of section panels
 */
export class EligibilityRequestsComponent {
    /**
     * EligibilityRequestsComponent constructor
     */
    constructor(
        private eligibilityService : EligibilityService,
        private translate          : TranslateService,
        private cd                 : ChangeDetectorRef
     ) {
        // subscribe to refreshed eligibility results
        // this.eligibilityService.updateEligibilityResults
        //     .subscribe(results => {
        //         this.eligibilityResults = results;
        //         cd.detectChanges();
        //     });
    }

    /**
     * Current list of eligibility results
     */
    @Input() requestList : any;

    /**
     * The orderby string
     */
    orderByString : Array<string>;

    /**
     * Array of toggled eligibility requests
     */
    toggledEligibilityRequests : Array<any> = [];

    /**
     * The orderby style/class string name
     */
    orderStyle : string;

    /**
     * The orderby style/class string name
     */
    toggleAllEligibilityRequests : boolean = false;

    /**
     * list of available gender types for user to select
     */
    resultsToDisplay : any;

    resultOptions : Array<IKeyValuePair> = [{
        id    : '0',
        value : '10'
    }, {
        id    : '1',
        value : '25'
    }, {
        id    : '2',
        value : '50'
    }, {
        id    : '3',
        value : '100'
    }, {
        id    : '4',
        value : this.translate.instant('ALL')
    }];

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for gender selection event
     * @param choice selected phone type
     * @param status toggled state of eligibility request
     */
    toggleThisEligibilityRequest(choice : number, status : boolean) {
        this.toggledEligibilityRequests[choice] = status;
    }

    /**
     * event handler for gender selection event
     * @param choice selected phone type
     */
    updateResultsToDisplay(choice : KeyValuePair) {
        // update profile phone type
        this.resultsToDisplay = choice.value;
    }

    /**
     * format date func for the pikaday datepicker
     * @param date
     */
    toggleAllCheckbox(date : string) {

    }

    /**
     * component init lifecycle hook
     */
    changeOrderBy(value : string, direction : string) {
        switch (value) {
            case 'NAME' :
                this.orderByString = direction === 'up' ? ['beneficiary.lastName'] : ['-beneficiary.lastName'];
                this.orderStyle = direction === 'up' ? 'LastName__ASC' : 'LastName__DESC';
            break;
            case 'PLAN' :
                this.orderByString = direction === 'up' ? ['plan.name'] : ['-plan.name'];
                this.orderStyle = direction === 'up' ? 'PLAN__ASC' : 'PLAN__DESC';
            break;
            case 'STATE' :
                this.orderByString = direction === 'up' ? ['state'] : ['-state'];
                this.orderStyle = direction === 'up' ? 'STATE__ASC' : 'STATE__DESC';
            break;
            case 'DATE' :
                this.orderByString = direction === 'up' ? ['date'] : ['-date'];
                this.orderStyle = direction === 'up' ? 'DATE__ASC' : 'DATE__DESC';
            break;
            case 'ASSIGNED' :
                this.orderByString = direction === 'up' ? ['assignedTo.status'] : ['-assignedTo.status'];
                this.orderStyle = direction === 'up' ? 'ASSIGNED__ASC' : 'ASSIGNED__DESC';
            break;
            default :
        }
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        this.orderByString    = ['risk.level'];
        this.orderStyle       = '';
        this.resultsToDisplay = '10';
    }
}
