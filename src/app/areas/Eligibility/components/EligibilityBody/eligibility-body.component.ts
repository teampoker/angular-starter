import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';

import {EligibilityStateSelectors} from '../../../../store/Eligibility/eligibility.selectors';
import {EligibilityService} from '../../services/Eligibility/eligibility.service';
import {EligibilityState} from '../../../../store/Eligibility/types/eligibility-state.model';

@Component({
    selector        : 'eligibility-body',
    templateUrl     : './eligibility-body.component.html',
    styleUrls       : ['./eligibility-body.component.scss'],
    changeDetection : ChangeDetectionStrategy.Default
})

/**
 * Implementation for EligibilityBodyComponent: handles display of eligibility status cards
 */
export class EligibilityBodyComponent {
    /**
     * EligibilityBodyComponent constructor
     * @param eligibilityService
     * @param eligibilitySelectors
     * @param cd
     */
    constructor(
        private eligibilityService      : EligibilityService,
        private eligibilitySelectors    : EligibilityStateSelectors,
        private cd                      : ChangeDetectorRef
    ) {
        // observe eligibility state
        this.eligibilitySubscription = this.eligibilitySelectors.getEligibilityState().subscribe(val => {
            // did we get new data?
            if (val.equals(this.eligibilityState) !== true) {
                // update local state
                this.eligibilityState = val;

                // mark the path from root of component tree to this component
                // for change detected on the next tick
                // We do this because we're set to ChangeDetectionStrategy.OnPush
                this.cd.markForCheck();
            }
        });

        this.eligibilityService.getEligibilityRequests()
            .first()
            .subscribe(response => {
                this.eligibilityRequests = response;
                cd.detectChanges();
            });
    }

    /**
     * Redux state subscription
     */
    private eligibilitySubscription : Subscription;

    /**
     * current snapshot of eligibility state
     */
    eligibilityState : EligibilityState;

    /**
     * Contains header title information
     */
    headerObject : any;

    /**
     * Placeholder list for Eligibility Requests
     */
    eligibilityRequests : any;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        this.headerObject   = {
            type          : 'EligibilityRequests',
            title         : 'Eligibility Requests'
        };
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        // unsubscribe
        this.eligibilitySubscription.unsubscribe();
    }
}
