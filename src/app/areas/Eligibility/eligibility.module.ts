import {NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {EligibilityRoutingModule} from './eligibility-routing.module';
import {EligibilityEntryComponent} from './eligibility-entry.component';
import {EligibilityHeaderComponent} from './components/EligibilityHeader/eligibility-header.component';
import {EligibilityBodyComponent} from './components/EligibilityBody/eligibility-body.component';
import {EligibilityRequestsComponent} from './components/EligibilityRequests/eligibility-requests.component';
import {EligibilityActions} from '../../store/Eligibility/eligibility.actions';
import {EligibilityService} from './services/Eligibility/eligibility.service';
import {EligibilityStateSelectors} from '../../store/Eligibility/eligibility.selectors';

@NgModule({
    imports         : [
        SharedModule,
        EligibilityRoutingModule
   ],
    declarations    : [
        EligibilityEntryComponent,
        EligibilityHeaderComponent,
        EligibilityBodyComponent,
        EligibilityRequestsComponent
   ],
    providers       : [
        EligibilityActions,
        EligibilityService,
        EligibilityStateSelectors
    ]
})

export class EligibilityModule {

}
