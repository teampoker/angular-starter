import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EligibilityEntryComponent} from './eligibility-entry.component';
import {EligibilityBodyComponent} from './components/EligibilityBody/eligibility-body.component';

@NgModule({
    imports : [
        RouterModule.forChild([
            {
                path        : '',
                component   : EligibilityEntryComponent,
                children    : [
                    {
                        path        : '',
                        component   : EligibilityBodyComponent
                    }
                ]
            }
        ])
   ],
    exports : [
        RouterModule
    ]
})

export class EligibilityRoutingModule {

}
