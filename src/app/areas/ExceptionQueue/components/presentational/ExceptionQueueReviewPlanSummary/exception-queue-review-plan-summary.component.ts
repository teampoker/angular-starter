import {
    Component,
    ChangeDetectionStrategy,
    Input
} from '@angular/core';
import { List } from 'immutable';
import * as moment from 'moment';

import {BeneficiaryPlansStateSummary} from '../../../../../store/Beneficiary/types/beneficiary-plans-state-summary.model';

@Component({
    selector       : 'exception-review-plan-summary',
    templateUrl     : './exception-queue-review-plan-summary.component.html',
    styleUrls       : ['./exception-queue-review-plan-summary.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ExceptionQueueReviewPlanSummaryComponent: displays plan details of the exeption being reviewed.
 */
export class ExceptionQueueReviewPlanSummaryComponent {
    /**
     * ExceptionQueueReviewPlanSummaryComponent constructor
     */
    constructor () {}

    /**
     * beneficiary plans
     */
    @Input() plansState : List<BeneficiaryPlansStateSummary>;

    /**
     * method to format plan dates
     */
     formatDate (date : string) {
         return moment(date).format('MM/DD/YYYY');
     }

     /**
      * custom track by index function for ngFor directives
      * @param index
      * @param obj
      * @returns {number}
      */
     trackByIndex(index : number, obj : any) : any {
         return index;
     }

}
