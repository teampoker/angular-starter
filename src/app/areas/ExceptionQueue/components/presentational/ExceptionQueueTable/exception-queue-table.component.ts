import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import {List} from 'immutable';

import {ExceptionQueueActions} from '../../../../../store/ExceptionQueue/actions/exception-queue.actions';
import {Exception} from '../../../../../store/ExceptionQueue/types/exception.model';
import {SearchResultsPaging} from '../../../../../store/Search/types/search-results-paging.model';
import {
    QueueQueryParams,
    QueueTabType,
    SortColumn,
    SortOrder
} from '../../../../../store/ExceptionQueue/types/queue-query-params.model';

@Component({
    selector        : 'exception-queue-table',
    templateUrl     : './exception-queue-table.component.html',
    styleUrls       : ['./exception-queue-table.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ExceptionQueueTableComponent: displays exception queue table
 */
export class ExceptionQueueTableComponent {
    /**
     * ExceptionQueueTableComponent constructor
     * @param exceptionQueueActions
     */
    constructor (private exceptionQueueActions : ExceptionQueueActions) {}

    /**
     * current tab
     */
    @Input() currentTab : QueueTabType;

    /**
     *  Exception Queue : list of reservation exceptions
     */
    @Input() exceptionQueue : List<Exception>;

    /**
     * checked state of select all checkbox
     */
    @Input() isSelectAll : boolean;

    /**
     *  pagingInfo : paging info of exception queue state
     */
    @Input() pagingInfo : SearchResultsPaging;

    @Input() queryParams : QueueQueryParams;

    queueTabTypes : any = QueueTabType;

    /**
     *  changeSortOrder : method to change sort order
     */
    changeSortOrder(sortKey : SortColumn = 'beneficiaryName', direction : SortOrder = 'asc') {
        this.exceptionQueueActions.updateSortParams(sortKey, direction);
    };

    /**
     * handler for pagination component incrementPageIndex event
     */
    onIncrementPageIndex() {
        this.exceptionQueueActions.incrementPage();
    }

    /**
     * handler for pagination component decrementPageIndex event
     */
    onDecrementPageIndex() {
        this.exceptionQueueActions.decrementPage();
    }

    /**
     * handler for individual exception select/unselect event
     * @param {Exception} exception
     */
    onExceptionSelectedToggle(exception : Exception) {
        this.exceptionQueueActions.updateSelectException(exception);
    }

    /**
     * toggle state of select all exceptions flag
     */
    selectAllExceptions () {
        // update store with selectedExceptions list
        this.exceptionQueueActions.updateSelectAllExceptions(!this.isSelectAll);
    }
}
