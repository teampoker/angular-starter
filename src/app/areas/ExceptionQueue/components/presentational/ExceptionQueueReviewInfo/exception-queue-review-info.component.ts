import {
    Component,
    ChangeDetectionStrategy,
    Input
} from '@angular/core';
import * as moment from 'moment';

import {BeneficiaryContactMechanismStateDetail} from '../../../../../store/Beneficiary/types/beneficiary-contact-mechanism-state-detail.model';
import {BeneficiaryInfo} from '../../../../../store/Beneficiary/types/beneficiary-info';

@Component({
    selector       : 'exception-review-info',
    templateUrl     : './exception-queue-review-info.component.html',
    styleUrls       : ['./exception-queue-review-info.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ExceptionQueueReviewInfoComponent: displays details of the exeption being reviewed.
 */
export class ExceptionQueueReviewInfoComponent {
    /**
     * ExceptionQueueReviewInfoComponent constructor
     */
    constructor () {
    }

    /**
     * reservation being reviewed
     */
    @Input() reservationReferenceId : string;

    /**
     * beneficiaryInfo
     */
    @Input() beneficiaryInfo : BeneficiaryInfo;

    /**
     * BeneficiaryContactMechanismStateDetail
     */
    @Input() beneficiaryContactMechanisms : BeneficiaryContactMechanismStateDetail;

    /**
     * priorityLevel of reservation
     */
    @Input() priorityLevel : string;

    /**
     * formatDate : method to format date to MM/DD/YYYY
     */
    formatDate (date : string) {
        return moment(date).format('MM/DD/YYYY');
    }

    /**
     * getYearsFromDate : method to get the years for use in the xx years old field
     */
    getYearsFromDate (date : string) {
        return moment().diff(date, 'years');
    }
}
