import {
    Component,
    ChangeDetectionStrategy,
    Input
} from '@angular/core';
import {List} from 'immutable';

import {QueueTaskItem} from '../../../../../store/ExceptionQueue/types/queue-task-item.model';
import {ExceptionQueueActions} from '../../../../../store/ExceptionQueue/actions/exception-queue.actions';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';

@Component({
    selector       : 'exception-review-detail',
    templateUrl     : './exception-queue-review-detail.component.html',
    styleUrls       : ['./exception-queue-review-detail.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ExceptionQueueReviewDetailComponent: displays details of exceptions being reviewed.
 */
export class ExceptionQueueReviewDetailComponent {
    /**
     * ExceptionQueueReviewDetailComponent constructor
     */
    constructor (private exceptionQueueActions : ExceptionQueueActions) {}

    /**
     * exceptions being reviewed
     */
    @Input() queueTaskItems : List<QueueTaskItem>;

    /**
     * onUpdateFormField: method to update different form fields on the target task item
     */
    onUpdateFormField (fieldValueUpdate : IBeneficiaryFieldUpdate, index : number) {
        fieldValueUpdate.primaryUpdateIndex = index;

        this.exceptionQueueActions.updateReviewFormFields(fieldValueUpdate);
    }

    /**
     * trackByUuid: trackFn to track by task uuid
     */
    trackByUuid(index : number, task : QueueTaskItem) : string {
        return task.get('uuid');
    }
}
