import {
    Input,
    Component,
    ChangeDetectionStrategy
} from '@angular/core';
import {List} from 'immutable';

import {Exception} from '../../../../../store/ExceptionQueue/types/exception.model';
import {ExceptionQueueActions} from '../../../../../store/ExceptionQueue/actions/exception-queue.actions';

@Component({
    selector        : 'exception-review',
    templateUrl     : './exception-queue-review.component.html',
    styleUrls       : ['./exception-queue-review.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ExceptionQueueReviewComponent: displays form to handle review of Exception tasks
 */
export class ExceptionQueueReviewComponent {
    /**
     * ExceptionQueueReviewComponent constructor
     * @param exceptionQueueActions
     */
    constructor (private exceptionQueueActions : ExceptionQueueActions) {}

    /**
     * exceptions being reviewed
     */
    @Input() exceptions : List<Exception>;

    /**
     * index of exception being reviewed
     */
    @Input() currentExceptionIndex : number;

    /**
     *  closeReviewPanel: method to close exception review component
     */
     closeReviewPanel () {
        this.exceptionQueueActions.updateIsReviewExceptionsActive(false);
    }

    /**
     *  incrementCurrentIndex: method to increment currentExceptionIndex
     */
     incrementCurrentIndex () {
        this.exceptionQueueActions.updateCurrentExceptionIndex(true);
    }

    /**
     *  decrementCurrentIndex: method to decrement currentExceptionIndex
     */
     decrementCurrentIndex () {
        this.exceptionQueueActions.updateCurrentExceptionIndex(false);
    }

    /**
     *  isLeftButtonDisabled: method to get boolean to disable the left pager button
     */
    isLeftButtonDisabled () {
        return this.currentExceptionIndex === 0;
    }

    /**
     *  isRightButtonDisabled: method to get boolean to disable the right pager button
     */
    isRightButtonDisabled () {
        return this.currentExceptionIndex + 1 === this.exceptions.size;
    }

    /**
     *  submitReview: method to submit the review form
     */
    submitReviewForm () {
        this.exceptionQueueActions.submitReviewForm();

        // if the exception is the last one, just close the panel
        // if not then incrementCurrentIndex and move on to the next
        if (this.isLast()) {
            this.closeReviewPanel();
        }
        else {
            this.incrementCurrentIndex();
        }
    }

    /**
     *  isLast: returns boolean to see if last exception is shown
     */
    isLast () {
        return this.currentExceptionIndex + 1 === this.exceptions.size;
    }
}
