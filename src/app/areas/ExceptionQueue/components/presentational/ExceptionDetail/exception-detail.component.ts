import {
    Component,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter
} from '@angular/core';

import {Exception} from '../../../../../store/ExceptionQueue/types/exception.model';
import {ExceptionQueueActions} from '../../../../../store/ExceptionQueue/actions/exception-queue.actions';
import {QueueTabType} from '../../../../../store/ExceptionQueue/types/queue-query-params.model';

@Component({
    selector       : 'exception-detail',
    templateUrl     : './exception-detail.component.html',
    styleUrls       : ['./exception-detail.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ExceptionDetailComponent: displays details of the exception.
 */
export class ExceptionDetailComponent {
    /**
     * ExceptionDetailComponent constructor
     */
    constructor (
        private exceptionQueueActions : ExceptionQueueActions
    ) {}

    /**
     * current tab
     * @type {QueueTabType}
     */
    @Input() currentTab : QueueTabType;

    /**
     * single exception record
     * @type {Exception}
     */
    @Input() exception : Exception;

    /**
     * custom event emitted whenever exception's selection state is modified
     * @type {EventEmitter<any>}
     */
    @Output() exceptionSelectedToggle : EventEmitter<any> = new EventEmitter<any>();

    getExceptionTypes() {
        return this.exception.getIn(['queueTask', 'queueTaskItems'])
            .map(item => item.getIn(['queueTaskItemType', 'name']))
            .sort()
            .join(', ');
    }

    /**
     * event handler for checkbox check/uncheck event
     */
    toggleSelection() {
        this.exceptionSelectedToggle.emit();
    }

    /**
     * kicks off flow to edit a single exception when plencil edit button is clicked.
     */
     editException(exception : Exception) {
         this.exceptionQueueActions.editException(exception);
     }

     /**
      * Enum tab types
      * @type {QueueTabType}
      */
     queueTabTypes : typeof QueueTabType = QueueTabType;
}
