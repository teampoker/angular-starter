import {
    Input,
    Output,
    Component,
    ChangeDetectionStrategy,
    EventEmitter
} from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    FormControl,
    Validators
} from '@angular/forms';

import {QueueTaskItem} from '../../../../../store/ExceptionQueue/types/queue-task-item.model';
import {formatPhoneForApi, PHONE_MASK_REGEX} from '../../../../../store/utils/parse-phone-numbers';
import {MetaDataTypesSelectors} from '../../../../../store/MetaDataTypes/meta-data-types.selectors';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';

@Component({
    selector       : 'exception-review-single-exception',
    templateUrl     : './exception-queue-review-single-exception.component.html',
    styleUrls       : ['./exception-queue-review-single-exception.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ExceptionQueueReviewSingleExceptionComponent: displays details of a single excpetion in the review list.
 */
export class ExceptionQueueReviewSingleExceptionComponent {
    /**
     * ExceptionQueueReviewSingleExceptionComponent constructor
     */
    constructor (
        private builder                 : FormBuilder,
        private metaDataTypeSelector    : MetaDataTypesSelectors
    ) {

        // form input fields
        this.authorizationCode      = new FormControl('', Validators.required);
        this.note                   = new FormControl('', undefined);
        this.planFacilityName       = new FormControl('', undefined);
        this.isApprovedTrue         = new FormControl('', undefined);
        this.isApprovedFalse        = new FormControl('', undefined);
        this.verifiedWith           = new FormControl('', undefined);
        this.overrideReason         = new FormControl('', undefined);
        this.phoneNumber            = new FormControl('', Validators.compose([
            Validators.pattern(/^(\(\d{3}\))|(\d{3}-)\d{3}-\d{4}$/)
        ]));

        // form group
        this.reviewForm = builder.group({
            authorizationCode  : this.authorizationCode,
            note               : this.note,
            planFacilityName   : this.planFacilityName,
            isApprovedTrue     : this.isApprovedTrue,
            isApprovedFalse    : this.isApprovedFalse,
            verifiedWith       : this.verifiedWith,
            overrideReason     : this.overrideReason,
            phoneNumber        : this.phoneNumber
        });

        // authorizationCode update handler
        this.authorizationCode.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateFormField.emit({
                fieldName   : 'authorizationCode',
                fieldValue  : value
            });
        });

        // note update handler
        this.note.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateFormField.emit({
                fieldName   : 'note',
                fieldValue  : value
            });
        });

        // planFacilityName update handler
        this.planFacilityName.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateFormField.emit({
                fieldName   : 'planFacilityName',
                fieldValue  : value
            });
        });

        // verifiedWith update handler
        this.verifiedWith.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateFormField.emit({
                fieldName   : 'verifiedWith',
                fieldValue  : value
            });
        });

        // overrideReason update handler
        this.overrideReason.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateFormField.emit({
                fieldName   : 'overrideReason',
                fieldValue  : value
            });
        });

        // phoneNumber update handler
        this.phoneNumber.valueChanges.debounceTime(250).subscribe((value : string) => {

            value = formatPhoneForApi(value);

            // emit updated value
            this.updateFormField.emit({
                fieldName   : 'phoneNumber',
                fieldValue  : value
            });
        });

        // isApproved update handler
        this.isApprovedTrue.valueChanges.subscribe((value : string) => {

            if (this.queueTaskItem.get('overrideReason')) {
                // clear out existing reason value
                this.updateFormField.emit({
                    fieldName   : 'overrideReason',
                    fieldValue  : ''
                });
                this.updateFormField.emit({
                    fieldName   : 'overrideReasonName',
                    fieldValue  : ''
                });
            }

            // emit updated value
            this.updateFormField.emit({
                fieldName   : 'isApproved',
                fieldValue  : value
            });
        });

        // isApproved update handler
        this.isApprovedFalse.valueChanges.subscribe((value : string) => {

            if (this.queueTaskItem.get('overrideReason')) {
                // clear out existing reason value
                this.updateFormField.emit({
                    fieldName   : 'overrideReason',
                    fieldValue  : ''
                });
                this.updateFormField.emit({
                    fieldName   : 'overrideReasonName',
                    fieldValue  : ''
                });
            }

            // emit updated value
            this.updateFormField.emit({
                fieldName   : 'isApproved',
                fieldValue  : value
            });
        });
    }

    /**
     * exceptions being reviewed
     */
    @Input() queueTaskItem : QueueTaskItem;

    /**
     * event triggered when fields other than the radio is changed
     * @type {"events".EventEmitter}
     */
    @Output() updateFormField : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * form group for review form
     */
     reviewForm : FormGroup;

    /**
     * for authorizationCode
     */
    authorizationCode : FormControl;

    /**
     * for verifiedWith
     */
    verifiedWith : FormControl;

    /**
     * for overrideReason
     */
    overrideReason : FormControl;

    /**
     * for note
     */
    note : FormControl;

    /**
     * for phoneNumber
     */
    phoneNumber : FormControl;

    /**
     * for planFacilityName
     */
    planFacilityName : FormControl;

    /**
     * for isApproved
     */
    isApprovedTrue : FormControl;

    /**
     * for isApproved
     */
    isApprovedFalse : FormControl;

    /**
     * decisionReasons
     */
     decisionReasons : any;

    /**
     * phoneMaskRegex : regex for phone number mask
     */
    phoneMaskRegex  :  Array<any> = PHONE_MASK_REGEX;

    /**
     * updateOverrideReason : emits an event to update overrideReason and overrideReasonName
     * @param reason reason object
     */
    updateOverrideReason (reason : any) {
        this.updateFormField.emit({
                fieldName   : 'overrideReason',
                fieldValue  : reason.uuid
            });
        this.updateFormField.emit({
                fieldName   : 'overrideReasonName',
                fieldValue  : reason.name
            });
    }

    // set up reasons
    ngOnInit () {
        this.metaDataTypeSelector.reasonsByTaskItemType(this.queueTaskItem.getIn(['queueTaskItemType', 'name'])).subscribe(reasonsObj => this.decisionReasons = reasonsObj).unsubscribe();
    }
}
