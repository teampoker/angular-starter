import {
    Component,
    ChangeDetectorRef,
    ChangeDetectionStrategy
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl
} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {
    List,
    Map
} from 'immutable';

import {NameUuid} from '../../../../../store/types/name-uuid.model';
import {
    QueueTabType,
    CallType,
    QueueQueryParams
} from '../../../../../store/ExceptionQueue/types/queue-query-params.model';
import {ServiceCoveragePlan} from '../../../../../store/MetaDataTypes/types/service-coverage-plan.model';
import {MetaDataTypesSelectors} from '../../../../../store/MetaDataTypes/meta-data-types.selectors';
import {ExceptionQueueStateSelectors} from '../../../../../store/ExceptionQueue/selectors/exception-queue.selector';
import {ExceptionQueueActions} from '../../../../../store/ExceptionQueue/actions/exception-queue.actions';
import {Exception} from '../../../../../store/ExceptionQueue/types/exception.model';
import {SearchResultsPaging} from '../../../../../store/Search/types/search-results-paging.model';
import {createDateValidator} from '../../../../../shared/components/DatePicker/date-picker.component';
import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';

@Component({
    templateUrl     : './exception-queue-container.component.html',
    styleUrls       : ['./exception-queue-container.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ExceptionQueueContainerComponent: base container component for Exception Queue module
 */
export class ExceptionQueueContainerComponent {
    /**
     * ExceptionQueueContainerComponent constructor
     * @param builder
     * @param cd
     * @param metaDataTypesSelectors
     * @param exceptionQueueActions
     * @param exceptionQueueSelectors
     */
    constructor(
        private builder                 : FormBuilder,
        private cd                      : ChangeDetectorRef,
        private metaDataTypesSelectors  : MetaDataTypesSelectors,
        private exceptionQueueActions   : ExceptionQueueActions,
        private exceptionQueueSelectors : ExceptionQueueStateSelectors
    ) {
        this.exceptionQueueActions.getExceptionQueue();

        // subscribe to aggregated Redux state changes globally that we're interested in
        this.stateSubscription = Observable.combineLatest(
            this.exceptionQueueSelectors.pagingInfo(),
            this.exceptionQueueSelectors.currentTab(),
            this.exceptionQueueSelectors.searchString(),
            this.exceptionQueueSelectors.selectedExceptions(),
            this.exceptionQueueSelectors.startDate(),
            this.exceptionQueueSelectors.endDate(),
            this.exceptionQueueSelectors.isSelectAll(),
            this.exceptionQueueSelectors.isReviewExceptionsActive(),
            this.exceptionQueueSelectors.currentExceptionIndex(),
            this.exceptionQueueSelectors.planTypeFilter(),
            this.exceptionQueueSelectors.taskItemTypeFilter(),
            this.exceptionQueueSelectors.planTypeFilterString(),
            this.exceptionQueueSelectors.taskItemTypeFilterString(),
            this.metaDataTypesSelectors.queueTaskItemType(),
            this.metaDataTypesSelectors.serviceCoveragePlans(),
            this.exceptionQueueSelectors.isFetching(),
            this.exceptionQueueSelectors.priorities(),
            this.exceptionQueueSelectors.prioritiesFilter(),
            this.exceptionQueueSelectors.params()
        )
            .subscribe(val => {
                // update local state
                this.pagingInfo                 = val[0];
                this.currentTab                 = val[1];
                this.searchString               = val[2];
                this.selectedExceptions         = val[3];
                this.startDate                  = val[4];
                this.endDate                    = val[5];
                this.isSelectAll                = val[6];
                this.isReviewExceptionsActive   = val[7];
                this.currentExceptionIndex      = val[8];
                this.planTypeFilter             = val[9];
                this.taskItemTypeFilter         = val[10];
                this.planTypeFilterString       = val[11];
                this.taskItemTypeFilterString   = val[12];
                this.queueTaskTypes             = val[13];
                this.planTypes                  = val[14];
                this.isFetching                 = val[15];
                this.priorities                 = val[16];
                this.prioritiesFilter           = val[17];
                this.params                     = val[18];

                // trigger change detection
                this.cd.markForCheck();
            });

        this.search             = new FormControl('', undefined);
        this.startDateControl   = new FormControl('', createDateValidator(this.dateFormat));
        this.endDateControl     = new FormControl('', createDateValidator(this.dateFormat));

        // build Beneficiary Information FormControl group
        this.dateRangeForm = builder.group({
            search              : this.search,
            startDateControl    : this.startDateControl,
            endDateControl      : this.endDateControl
        });

        this.search.valueChanges.debounceTime(250).subscribe((value : string) => {
            this.exceptionQueueActions.updateSearchString(value);
        });

        this.startDateControl.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.exceptionQueueActions.updateStartDate(value);
        });

        this.endDateControl.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.exceptionQueueActions.updateEndDate(value);
        });

        this.exceptionQueueSubscription = this.exceptionQueueSelectors
            .getFilteredExceptions()
            .subscribe(exceptions => {
                this.exceptionQueue = exceptions
                    .sort(this.exceptionQueueSelectors.getExceptionSortFunction(this.params.get('sortColumn'), this.params.get('sortOrder')))
                    .skip(this.pagingInfo.get('pageOffset', 0) * this.pagingInfo.get('pageSize', 25))
                    .take(this.pagingInfo.get('pageSize', 25)) as List<Exception>;

                this.pagingInfo = this.pagingInfo.set('totalRecords', this.exceptionQueue.count()) as SearchResultsPaging;

                this.cd.markForCheck();
            });
    }

    /**
     * Redux state subscription
     */
    private stateSubscription : Subscription;

    /**
     * checked/unchecked state of select all checkbox
     */
    isSelectAll : boolean;

    /**
     * flag to indicate when queue GET is active
     */
    isFetching : boolean;

    /**
     * toggles exceptions review component
     */
    isReviewExceptionsActive : boolean;

    /**
     * currentExceptionIndex
     */
    currentExceptionIndex : number;

    /**
     *  exception queue
     */
    exceptionQueue : List<Exception>;

    exceptionQueueSubscription : Subscription;

    /**
     * List of reservation uuids representing selected exceptions
     */
    selectedExceptions : List<string>;

    /**
     * paging info of exception queue state
     */
    pagingInfo : SearchResultsPaging;

    /**
     *  planTypes  : a list of plan types
     */
    planTypes : List<ServiceCoveragePlan>;

    priorities : List<KeyValuePair>;

    prioritiesFilter : Map<number, boolean>;

    params : QueueQueryParams;

    /**
     *  queueTaskTypes  : a list of queueTaskTypes
     */
    queueTaskTypes : List<NameUuid>;

    /**
     *  searchString  : string value used to filter Beneficiaries by name
     */
    searchString : string;

    /**
     *  planTypeFilter  : map of planType uuid with a boolean representing if checkbox is selected for that type
     */
    planTypeFilter : Map<string, boolean>;

    /**
     *  planTypeFilterString  : string of keys where the value is set to true in planTypeFilter. Used for checking if any filters are set.
     */
    planTypeFilterString : string;

    /**
     *  taskItemTypeFilter  : map of taskItemType uuid with a boolean representing if checkbox is selected for that type
     */
    taskItemTypeFilter : Map<string, boolean>;

    /**
     *  taskItemTypeFilterString  : string of keys where the value is set to true in taskItemTypeFilter. Used for checking if any filters are set.
     */
    taskItemTypeFilterString : string;

    /**
     *  currentTab  : string value representing current tab
     */
    currentTab : QueueTabType;

    /**
     * Object contains form group
     */
    dateRangeForm : FormGroup;

    /**
     *  startDate  : string value representing startDate form control
     */
    startDateControl : FormControl;

    /**
     *  endDate  : string value representing endDate form control
     */
    endDateControl : FormControl;

    /**
     *  startDate  : prop that holds date value for filtering
     */
    startDate : string = '';

    /**
     *  endDate  : prop that holds date value for filtering
     */
    endDate : string = '';

    /**
     * Enum tab types
     * @type {QueueTabType}
     */
    queueTabTypes : typeof QueueTabType = QueueTabType;

    /**
     * Enum call types
     * @type {CallType}
     */
    callTypes : typeof CallType = CallType;

    /**
     *  search  : search input
     */
    search : FormControl;

    /**
     *  isDateRangeOpen  : boolean to keep dropdown open
     */
    isOpen : boolean = false;

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index  : number, obj  : any)  : any {
        return index;
    }

    /**
     *  changeCurrentTab : method to select current tab
     */
    changeCurrentTab(newTab : QueueTabType) {
        this.exceptionQueueActions.updateCurrentTab(newTab);
    }

    /**
     *  changePageSize : method to select pageSize
     */
    changePageSize(newSize : string) {
        this.exceptionQueueActions.updatePageSize(newSize);
    }

    /**
     *  assignExceptions : method to assign or unassign selected exceptions
     */
    assignExceptions(type : CallType) {
        this.exceptionQueueActions.assignExceptions(type);
    }

    clearPriorityFilter() {
        this.exceptionQueueActions.clearPrioritiesFilter();
    }

    isFilteredByPriority() : boolean {
        return this.prioritiesFilter.filter(priority => priority).count() > 0;
    }

    selectPriority(priority : KeyValuePair) {
        this.exceptionQueueActions.updatePrioritiesFilter(priority);
    }

    /**
     *  openReviewPanel : method to open exception review component
     */
    openReviewPanel() {
        this.exceptionQueueActions.updateIsReviewExceptionsActive(true);
    }

    /**
     * Handles date picker open state changes
     * @param {boolean} isOpen
     */
    onSetDatePickerIsOpen(isOpen : boolean) {
        this.isOpen = isOpen;
    }

    /**
     * calls action to add/remove taskItemType from exception queue search param
     * @param {NameUuid} itemType
     */
    selectTaskItemType(itemType : NameUuid) {
        this.exceptionQueueActions.updateTaskItemTypeFilter(itemType.get('uuid'));
    }

    /**
     * calls action to add/remove plan type from exception queue search param
     * @param {ServiceCoveragePlan}  planType
     */
    selectPlanType(planType : ServiceCoveragePlan) {
        this.exceptionQueueActions.updatePlanTypeFilter(planType.get('id'));
    }

    /**
     *  isFilteredByTaskItemType : method to check if taskItemType filter is set
     */
    isFilteredByTaskItemType() {
        return this.taskItemTypeFilterString.length;
    }

    /**
     *  clearTaskItemType : method to clear task item type filter
     */
    clearTaskItemTypeFilter() {
        this.exceptionQueueActions.clearTaskItemTypeFilter();
    }

    /**
     *  isFilteredByPlanType : method to check if planType filter is set
     */
    isFilteredByPlanType() {
        return this.planTypeFilterString.length;
    }

    /**
     *  clearPlanType : method to clear plan type filter
     */
    clearPlanTypeFilter() {
        this.exceptionQueueActions.clearPlanTypeFilter();
    }

    /**
     *  isFilteredByDate : method to check if date range filter is set
     */
    isFilteredByDate() {
        return this.startDate || this.endDate;
    }

    /**
     *  clearDateRange : method to clearing selected date range
     */
    clearDateRangeFilter() {
        this.exceptionQueueActions.clearDateRangeFilter();
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        this.exceptionQueueSubscription.unsubscribe();
        this.stateSubscription.unsubscribe();
    }
}
