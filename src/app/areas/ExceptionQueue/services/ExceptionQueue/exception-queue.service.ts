import {Injectable} from '@angular/core';
import {List} from 'immutable';
import {Observer} from 'rxjs';
import {Observable} from 'rxjs/Observable';
import {
    URLSearchParams,
    RequestOptions
} from '@angular/http';

import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {QueueQueryParams} from '../../../../store/ExceptionQueue/types/queue-query-params.model';
import {Exception} from '../../../../store/ExceptionQueue/types/exception.model';
import {EXCEPTION_QUEUE_MOCK} from './exception-queue.service.mock';
import {QueueTaskItem, toApi as QueueTaskItemToApi} from '../../../../store/ExceptionQueue/types/queue-task-item.model';

@Injectable()
/**
 * Implementation of ExceptionQueueService: returns exception queue data from api
 */
export class ExceptionQueueService {
    /**
     * BeneficiaryService constructor
     * @param backendService
     */
    constructor(private backendService : BackendService) {}

    /**
     *
     * @param response
     * @param observer
     */
    handleExceptionQueueResponse(response : any, observer : Observer<any>) {
        let exceptions : any = response;

        if (!exceptions.errors) {
            response.length ? exceptions = response[0] : exceptions = response;

            // there are no errors with the response, so return the object
            observer.next(exceptions);
        }
        else {
            /*
             * pass the error message back to the observer
             *
             * */
            observer.error(exceptions.message);
        }
    }

    /**
     *
     * @param error
     * @param observer
     */
    handleExceptionQueueError(error : any, observer : Observer<any>) {
        /*
         * simply notify the observer that there's an error, but we are stopping
         * the buck here - data about the error ain't going nowhere... hehe...
         *
         * (honestly... I'm not sure why we're obscuring the error. I haven't taken that yoga class yet)
         */
        if (observer != null) {
            observer.error(undefined);
        }
    }

    /**
     * @description - get exception queue information
     * @param params
     * @returns {any}
     */
    getExceptionQueue(params : QueueQueryParams = new QueueQueryParams()) : Observable<any> {
        return Observable.create(observer => {
            const handle        : string = `getExceptionQueue`,
                  apiUrl        : string = API_CONFIG[handle],
                  queryParams   : URLSearchParams = new URLSearchParams();

            let options : RequestOptions;

            if (params.get('search')) {
                queryParams.set('search', params.get('search'));
            }

            queryParams.set('page', params.get('page'));
            queryParams.set('pageSize', params.get('pageSize'));
            queryParams.set('sortColumn', params.get('sortColumn'));
            queryParams.set('sortOrder', params.get('sortOrder'));

            options = new RequestOptions({
                search : queryParams
            });

            // get beneficiary data
            this.backendService.get(apiUrl, handle, EXCEPTION_QUEUE_MOCK)
                .first()
                .subscribe(
                    response => this.handleExceptionQueueResponse(response, observer),
                    error => this.handleExceptionQueueError(error, observer)
                );
        });
    }

    /**
     * @description - assign or unassign exceptions
     * @param userName
     * @param exceptions
     * @param type
     * @returns {any}
     */
    assignExceptions(userName : string, exceptions : List<Exception>, type : number) : Observable<any> {

        return Observable.create(observer => {
            const handle        : string = type === 0 ? `claimExceptions` : 'unassignExceptions',
                  apiUrl        : string = API_CONFIG[handle];

            const payload = {
                assignedUser : userName,
                reservationUuids : exceptions.map(exception => exception.get('reservationUuid')).toJS()
            };

            // put to assign exceptions/reservationUuids to a user
            this.backendService.put(apiUrl, handle, EXCEPTION_QUEUE_MOCK, JSON.stringify(payload))
                .first()
                .subscribe(
                    response => this.handleExceptionQueueResponse(response, observer),
                    error => this.handleExceptionQueueError(error, observer)
                );
        });
    }

    /**
     * @description - submit exception review
     * @param reviewObjectArray
     * @returns {any}
     */
    submitExceptionReview(reviewObjectArray : List<QueueTaskItem>) : Observable<any> {
        return Observable.create(observer => {
            const handle        : string = 'submitExceptionReview',
                  apiUrl        : string = API_CONFIG[handle];

            const payload = reviewObjectArray.map(task => QueueTaskItemToApi(task));

            // put to submit exception review
            this.backendService.put(apiUrl, handle, EXCEPTION_QUEUE_MOCK, JSON.stringify(payload.toJS()))
                .first()
                .subscribe(
                    response => this.handleExceptionQueueResponse(response, observer),
                    error => this.handleExceptionQueueError(error, observer)
                );
        });
    }
}
