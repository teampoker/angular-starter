import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ExceptionQueueEntryComponent} from './exception-queue-entry.component';
import {ExceptionQueueContainerComponent} from './components/container/ExceptionQueueContainer/exception-queue-container.component';

@NgModule({
    imports : [
        RouterModule.forChild([
            {
                path        : '',
                component   : ExceptionQueueEntryComponent,
                children    : [
                    {
                        path        : '',
                        component   : ExceptionQueueContainerComponent
                    }
                ]
            }
        ])
    ],
    exports : [
        RouterModule
    ]
})

export class ExceptionQueueRoutingModule {

}
