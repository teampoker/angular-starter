import {NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {ExceptionQueueRoutingModule} from './exception-queue-routing.module';
import {ExceptionQueueEntryComponent} from './exception-queue-entry.component';
import {ExceptionQueueReviewComponent} from './components/presentational/ExceptionQueueReview/exception-queue-review.component';
import {ExceptionQueueReviewInfoComponent} from './components/presentational/ExceptionQueueReviewInfo/exception-queue-review-info.component';
import {ExceptionQueueReviewPlanSummaryComponent} from './components/presentational/ExceptionQueueReviewPlanSummary/exception-queue-review-plan-summary.component';
import {ExceptionQueueReviewDetailComponent} from './components/presentational/ExceptionQueueReviewDetail/exception-queue-review-detail.component';
import {ExceptionQueueContainerComponent} from './components/container/ExceptionQueueContainer/exception-queue-container.component';
import {ExceptionDetailComponent} from './components/presentational/ExceptionDetail/exception-detail.component';
import {ExceptionQueueTableComponent} from './components/presentational/ExceptionQueueTable/exception-queue-table.component';
import {ExceptionQueueReviewSingleExceptionComponent} from './components/presentational/ExceptionQueueReviewSingleException/exception-queue-review-single-exception.component';

@NgModule({
    imports         : [
        SharedModule,
        ExceptionQueueRoutingModule
    ],
    declarations    : [
        ExceptionQueueEntryComponent,
        ExceptionQueueContainerComponent,
        ExceptionDetailComponent,
        ExceptionQueueTableComponent,
        ExceptionQueueReviewComponent,
        ExceptionQueueReviewInfoComponent,
        ExceptionQueueReviewPlanSummaryComponent,
        ExceptionQueueReviewDetailComponent,
        ExceptionQueueReviewSingleExceptionComponent
    ],
    providers       : []
})

export class ExceptionQueueModule {

}
