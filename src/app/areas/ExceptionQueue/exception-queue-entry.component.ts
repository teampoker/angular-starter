import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

import {NavActions} from '../../store/Navigation/nav.actions';
import {EnumNavOption} from '../../store/Navigation/types/nav-option.model';

@Component({
    templateUrl     : 'exception-queue-entry.component.html',
    styleUrls       : ['exception-queue-entry.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ExceptionQueueEntryComponent: responsible for exception queue page layout
 */
export class ExceptionQueueEntryComponent {
    /**
     * ExceptionQueueEntryComponent constructor
     * @param navActions
     */
    constructor (private navActions : NavActions) {

    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // set active epic to exception queue
        this.navActions.updateActiveNavState(EnumNavOption.EXCEPTION_QUEUE);
    }
}
