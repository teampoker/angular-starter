import {NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {BeneficiaryRoutingModule} from './beneficiary-routing.module';
import {BeneficiaryEntryComponent} from './beneficiary-entry.component';
import {BeneficiaryHeaderComponent} from './components/presentational/BeneficiaryHeader/beneficiary-header.component';
import {BeneficiaryBodyContainerComponent} from './components/container/BeneficiaryBody/beneficiary-body-container.component';
import {BeneficiaryProfileContainerComponent} from './components/container/BeneficiaryProfile/beneficiary-profile-container.component';
import {CreateNewProfileContainerComponent} from './components/container/CreateNewProfile/create-new-profile-container.component';
import {BeneficiaryActivityLogPanelComponent} from './components/presentational/BeneficiaryActivityLogPanel/beneficiary-activity-log-panel.component';
import {BeneficiaryPlansComponent} from './components/presentational/BeneficiaryPlans/beneficiary-plans.component';
import {BeneficiaryPlansPanelComponent} from './components/presentational/BeneficiaryPlansPanel/beneficiary-plans-panel.component';
import {BeneficiaryPlansDashboardComponent} from './components/presentational/BeneficiaryPlansDashboard/beneficiary-plans-dashboard.component';
import {BeneficiaryInformationComponent} from './components/presentational/BeneficiaryInformation/beneficiary-information.component';
import {BeneficiaryToDoPanelComponent} from './components/presentational/BeneficiaryToDoPanel/beneficiary-to-do-panel.component';
import {BeneficiarySpecialRequirementsComponent} from './components/presentational/BeneficiarySpecialRequirements/beneficiary-special-requirements.component';
import {BeneficiaryContactMechanismsComponent} from './components/presentational/BeneficiaryContactMechanisms/beneficiary-contact-mechanisms.component';
import {BeneficiaryEmailComponent} from './components/presentational/BeneficiaryEmail/beneficiary-email.component';
import {BeneficiaryPhoneComponent} from './components/presentational/BeneficiaryPhone/beneficiary-phone.component';
import {BeneficiaryAddressComponent} from './components/presentational/BeneficiaryAddress/beneficiary-address.component';
import {BeneficiaryConnectionsPanelComponent} from './components/presentational/BeneficiaryConnectionsPanel/beneficiary-connections-panel.component';
import {BeneficiaryMedicalConditionsComponent} from './components/presentational/BeneficiaryMedicalConditions/beneficiary-medical-conditions.component';
import {BeneficiaryTransportationProviderComponent} from './components/presentational/BeneficiaryTransportationProvider/beneficiary-transportation-provider.component';
import {BeneficiaryFormComponent} from './components/presentational/BeneficiaryForm/beneficiary-form.component';
import {BeneficiaryFormPhoneComponent} from './components/presentational/BeneficiaryFormPhone/beneficiary-form-phone.component';

@NgModule({
    imports         : [
        SharedModule,
        BeneficiaryRoutingModule
   ],
    declarations    : [
        BeneficiaryEntryComponent,
        BeneficiaryHeaderComponent,
        BeneficiaryBodyContainerComponent,
        BeneficiaryProfileContainerComponent,
        CreateNewProfileContainerComponent,
        BeneficiaryActivityLogPanelComponent,
        BeneficiaryPlansComponent,
        BeneficiaryPlansPanelComponent,
        BeneficiaryPlansDashboardComponent,
        BeneficiaryInformationComponent,
        BeneficiaryToDoPanelComponent,
        BeneficiarySpecialRequirementsComponent,
        BeneficiaryContactMechanismsComponent,
        BeneficiaryEmailComponent,
        BeneficiaryPhoneComponent,
        BeneficiaryAddressComponent,
        BeneficiaryConnectionsPanelComponent,
        BeneficiaryMedicalConditionsComponent,
        BeneficiaryTransportationProviderComponent,
        BeneficiaryFormComponent,
        BeneficiaryFormPhoneComponent
   ],
    providers       : []
})

export class BeneficiaryModule {

}
