import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    selector        : 'beneficiary-activity-log-panel',
    templateUrl     : './beneficiary-activity-log-panel.component.html',
    styleUrls       : ['./beneficiary-activity-log-panel.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ActivityLogPanelComponent: handles display of section panels
 */
export class BeneficiaryActivityLogPanelComponent {
    /**
     * ActivityLogPanelComponent constructor
     */
    constructor() {}

    /**
     * component init lifecycle hook
     */
    ngOnInit() {

    }
}
