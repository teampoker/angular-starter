import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    selector        : 'beneficiary-transportation-provider',
    templateUrl     : './beneficiary-transportation-provider.component.html',
    styleUrls       : ['./beneficiary-transportation-provider.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryTransportationProviderComponent
 */
export class BeneficiaryTransportationProviderComponent {
    /**
     * BeneficiaryTransportationProviderComponent constructor
     */
    constructor() { }
}
