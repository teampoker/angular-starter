import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
    SimpleChanges
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {List} from 'immutable';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {BeneficiaryServiceCoveragePlanClassificationState} from '../../../../../store/Beneficiary/types/beneficiary-service-coverage-plan-classification-state.model';
import {BeneficiaryPlansStateSummary} from '../../../../../store/Beneficiary/types/beneficiary-plans-state-summary.model';
import {BeneficiaryPlansActions} from '../../../../../store/Beneficiary/actions/beneficiary-plans.actions';
import {createDateValidator} from '../../../../../shared/components/DatePicker/date-picker.component';

@Component({
    selector        : 'beneficiary-plans',
    templateUrl     : './beneficiary-plans.component.html',
    styleUrls       : ['./beneficiary-plans.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryPlansComponent: handles display of this section panel
 */
export class BeneficiaryPlansComponent {
    /**
     * BeneficiaryPlansComponent constructor
     * @param builder
     * @param beneficiaryPlansActions
     */
    constructor(
        private builder                 : FormBuilder,
        private beneficiaryPlansActions : BeneficiaryPlansActions
    ) {
        // init form input fields
        this.organization               = new FormControl('', this.validateSelectDropDown);
        this.planDescription            = new FormControl('', this.validateSelectDropDown);
        this.fromDate                   = new FormControl('', createDateValidator(this.dateFormat, true));
        this.thruDate                   = new FormControl('', createDateValidator(this.dateFormat, true));
        this.beneficiaryIdPrimary       = new FormControl('', undefined);
        this.beneficiaryIdSecondary     = new FormControl('', undefined);
        this.beneficiaryIdTertiary      = new FormControl('', undefined);
        this.subPlan                    = new FormControl('', undefined);

        // build Beneficiary Information FormControl group
        this.planForm = builder.group({
            organization            : this.organization,
            planDescription         : this.planDescription,
            fromDate                : this.fromDate,
            thruDate                : this.thruDate,
            beneficiaryIdPrimary    : this.beneficiaryIdPrimary,
            beneficiaryIdSecondary  : this.beneficiaryIdSecondary,
            beneficiaryIdTertiary   : this.beneficiaryIdTertiary,
            subPlan                 : this.subPlan
        });

        this.planForm.valueChanges.subscribe(() => {
            // emit update form valid state
            this.planFormValid.emit(this.planForm.valid);
        });

        this.beneficiaryIdPrimary.valueChanges.debounceTime(250).subscribe((value : string) => {
            // dispatch action with updated field value
            if (this.planIdFieldPrimary !== undefined) {
                this.beneficiaryPlansActions.updateDynamicPlanField({
                    fieldName           : this.planIdFieldPrimary.get('value'),     // this value is populated in ngOnChanges every time plan selection is changed
                    fieldValue          : value
                });
            }
        });

        this.beneficiaryIdSecondary.valueChanges.debounceTime(250).subscribe((value : string) => {
            if (this.planIdFieldSecondary  !== undefined) {
                // dispatch action with updated field value
                this.beneficiaryPlansActions.updateDynamicPlanField({
                    fieldName           : this.planIdFieldSecondary.get('value'),     // this value is populated in ngOnChanges every time plan selection is changed
                    fieldValue          : value
                });
            }
        });

        this.beneficiaryIdTertiary.valueChanges.debounceTime(250).subscribe((value : string) => {
            if (this.planIdFieldTertiary  !== undefined) {
                // dispatch action with updated field value
                this.beneficiaryPlansActions.updateDynamicPlanField({
                    fieldName           : this.planIdFieldTertiary.get('value'),     // this value is populated in ngOnChanges every time plan selection is changed
                    fieldValue          : value
                });
            }
        });

        this.fromDate.valueChanges.subscribe((value : string) => {
            this.beneficiaryPlansActions.updatePlanField({
                fieldName   : 'fromDate',
                fieldValue  : value
            });
        });

        this.thruDate.valueChanges.subscribe((value : string) => {
            this.beneficiaryPlansActions.updatePlanField({
                fieldName   : 'thruDate',
                fieldValue  : value
            });
        });
    }

    /**
     * boolean toggle edit mode
     * @type {boolean}
     */
    @Input() isEditing : boolean;

    /**
     * number index of the organization being selected
     * @type {number}
     */
    @Input() organizationIndex : number;

    /**
     * number index of the coveragePlan being selected
     * @type {number}
     */
    @Input() coveragePlanIndex : number;

    /**
     * Object contains beneficiary information
     * @type {Object}
     */
    @Input() coverage : BeneficiaryPlansStateSummary;

    /**
     * event triggered when the form valid state has changed
     * @type {EventEmitter<boolean>}
     */
    @Output() planFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * event send close add command
     * @type {"events".EventEmitter}
     */
    @Output() closeAdd : EventEmitter<any> = new EventEmitter<any>();

    /**
     * store reference to current dynamically added planId field here
     */
    private planIdFieldPrimary : BeneficiaryServiceCoveragePlanClassificationState;

    /**
     * store reference to current dynamically added planId field here
     */
    private planIdFieldSecondary : BeneficiaryServiceCoveragePlanClassificationState;

    /**
     * store reference to current dynamically added planId field here
     */
    private planIdFieldTertiary : BeneficiaryServiceCoveragePlanClassificationState;

    /**
     * custom validation for dropdown type FormControls
     */
    private validateSelectDropDown(c : FormControl) : { [key : string] : any } {
        if (c.value && c.value !== 'SELECT') {
            return null;
        }

        return {
            validateSelection : {
                valid : false
            }
        };
    }

    /**
     * list of only Id type fields from the coverage @Input
     */
    planIdFields : List<BeneficiaryServiceCoveragePlanClassificationState>;

    /**
     * list of only Benefit type fields from the coverage @Input
     */
    planBenefitFields : List<BeneficiaryServiceCoveragePlanClassificationState>;

    /**
     * Object contains form group
     */
    planForm : FormGroup;

    /**
     * organization input field
     */
    organization : FormControl;

    /**
     * plan description input field
     */
    planDescription : FormControl;

    /**
     * fromDate input field
     */
    fromDate : FormControl;

    /**
     * thruDate input field
     */
    thruDate : FormControl;

    /**
     * subplan dropdown selection input field
     */
    subPlan : FormControl;

    /**
     * plan primary beneficiary ID input field
     */
    beneficiaryIdPrimary : FormControl;

    /**
     * plan secondary beneficiary ID input field
     */
    beneficiaryIdSecondary : FormControl;

    /**
     * plan tertiary beneficiary ID input field
     */
    beneficiaryIdTertiary : FormControl;

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for canceling edit mode
     */
    cancelEdit() {
        this.beneficiaryPlansActions.cancelEditBeneficiaryPlan();
    }

    /**
     * event handler for canceling edit mode
     */
    cancelAdd() {
        this.beneficiaryPlansActions.cancelAddBeneficiaryPlan();
    }

    /**
     * event handler for canceling edit mode
     */
    closeAddClick() {
        this.closeAdd.emit();
    }

    /**
     * event handler for canceling edit mode
     */
    closeEditClick() {
        // save updated plan info
        this.beneficiaryPlansActions.saveBeneficiaryPlans();

        // close edit connection form
        this.beneficiaryPlansActions.closeEditBeneficiaryPlan();
    }

    /**
     * event handler for organization selection event
     * @param choice selected organization
     * @param orgIndex inex of selected organization
     */
    updateOrganization(choice : KeyValuePair, orgIndex : number) {
        // dispatch action with updated field value
        this.beneficiaryPlansActions.updatePlanField({
            fieldName   : 'organization',
            fieldValue  : choice.get('value')
        });

        this.beneficiaryPlansActions.updatePlanField({
            fieldName   : 'organizationUuid',
            fieldValue  : choice.get('id')
        });

        // dispatch update oganizationIndex action
        this.beneficiaryPlansActions.updateOrganizationIndex(orgIndex);
    }

    /**
     * event handler for plan description event
     * @param choice plan description choice
     * @param planIndex index of selected plan
     */
    updatePlanDescription(choice : KeyValuePair, planIndex : number) {
        this.beneficiaryPlansActions.updatePlanField({
            fieldName   : 'planDescription',
            fieldValue  : choice.get('value')
        });

        this.beneficiaryPlansActions.updatePlanField({
            fieldName   : 'planUuid',
            fieldValue  : choice.get('id')
        });

        // dispatch update coveragePlan action
        this.beneficiaryPlansActions.updateCoveragePlanIndex(planIndex);
    }

    /**
     * handler for dynamic plan dropdown selection field selection event
     * @param fieldName name of field that triggered the event
     * @param fieldValue value selected from dropdown
     * @param fieldIndex index of field input
     */
    updatePlanSelectionField(fieldName : string, fieldValue : string, fieldIndex : number) {
        // emit updated value
        this.beneficiaryPlansActions.updateDynamicPlanField({
            fieldName,
            fieldValue
        });
    }

    /**
     * data-bound property value changes lifecycle hook
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        // look for change to coveragePlanIndex as this indicates the CSR
        // selected a different plan, which will then change the data bound
        // to each of the dynamic plan fields
        if (changes['coverage']  !== undefined) {
            if (changes['coverage'].currentValue  !== undefined) {
                // grab validation info for any active plan Id fields
                this.planIdFields = this.coverage.getIn(['plansStateDetail', this.organizationIndex, 'serviceCoveragePlans', this.coveragePlanIndex, 'serviceCoveragePlanClassifications'])
                                                 .filter(stuff => stuff.getIn(['classificationType', 'value']) === 'Id');

                this.planBenefitFields = this.coverage.getIn(['plansStateDetail', this.organizationIndex, 'serviceCoveragePlans', this.coveragePlanIndex, 'serviceCoveragePlanClassifications'])
                                                      .filter(stuff => stuff.getIn(['classificationType', 'value']) === 'Benefit');

                // subplan selection field
                if (this.planBenefitFields.size > 0) {
                    const benefitField : BeneficiaryServiceCoveragePlanClassificationState = this.planBenefitFields.get(0);

                    // check validation required
                    if (benefitField.get('isRequired')) {
                        this.planForm.controls['subPlan'].setValidators([Validators.required]);
                    }
                    else {
                        this.planForm.controls['subPlan'].setValidators([]);
                    }
                }
                else {
                    this.planForm.controls['subPlan'].setValidators([]);
                }

                // update validations
                this.planForm.controls['subPlan'].updateValueAndValidity();

                /**
                 * Field validator expression: assume that plan field ID's need to be 10 digits long.
                 *
                 * This was part of the API response model in metadataTypes, but was always a bad value.
                 * Hard-coding the validation pattern until a decision is made.
                 * @type {string}
                 */
                const planFieldValidatorExpression : string = '^\\d{10}$';

                // look at how many dynamic field entries there are.  We'll have multiples if the
                // selected plan allows for selecting an alternate ID
                if (this.planIdFields.size >= 1) {
                    this.planIdFieldPrimary = this.planIdFields.get(0);

                    // check validation RegEx

                    if (this.planIdFieldPrimary.get('isRequired')) {
                        this.planForm.controls['beneficiaryIdPrimary'].setValidators(Validators.compose([
                            Validators.required,
                            Validators.pattern(planFieldValidatorExpression)
                        ]));
                    }
                    else {
                        this.planForm.controls['beneficiaryIdPrimary'].setValidators([
                            Validators.pattern(planFieldValidatorExpression)
                        ]);
                    }
                }
                else {
                    this.planForm.controls['beneficiaryIdPrimary'].setValidators([]);
                }

                // update validations
                this.planForm.controls['beneficiaryIdPrimary'].updateValueAndValidity();

                if (this.planIdFields.size >= 2) {
                    this.planIdFieldSecondary = this.planIdFields.get(1);

                    // check validation RegEx
                    if (this.planIdFieldSecondary.get('isRequired')) {
                        this.planForm.controls['beneficiaryIdSecondary'].setValidators(Validators.compose([
                            Validators.required,
                            Validators.pattern(planFieldValidatorExpression)
                        ]));
                    }
                    else {
                        this.planForm.controls['beneficiaryIdSecondary'].setValidators(Validators.pattern(planFieldValidatorExpression));
                    }
                }
                else {
                    this.planForm.controls['beneficiaryIdSecondary'].setValidators([]);
                }

                // update validations
                this.planForm.controls['beneficiaryIdSecondary'].updateValueAndValidity();

                if (this.planIdFields.size >= 3) {
                    this.planIdFieldTertiary = this.planIdFields.get(2);

                    // check validation RegEx
                    if (this.planIdFieldTertiary.get('isRequired')) {
                        this.planForm.controls['beneficiaryIdTertiary'].setValidators(Validators.compose([
                            Validators.required,
                            Validators.pattern(planFieldValidatorExpression)
                        ]));
                    }
                    else {
                        this.planForm.controls['beneficiaryIdTertiary'].setValidators([Validators.pattern(planFieldValidatorExpression)]);
                    }
                }
                else {
                    this.planForm.controls['beneficiaryIdTertiary'].setValidators([]);
                }

                // update validations
                this.planForm.controls['beneficiaryIdTertiary'].updateValueAndValidity();
            }
        }
    }
}
