import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl
} from '@angular/forms';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {BeneficiaryMedicalConditionsState} from '../../../../../store/Beneficiary/types/beneficiary-medical-conditions-state.model';
import {BeneficiaryMedicalConditionsActions} from '../../../../../store/Beneficiary/actions/beneficiary-medical-conditions.actions';
import {createDateValidator} from '../../../../../shared/components/DatePicker/date-picker.component';

@Component({
    selector        : 'beneficiary-medical-conditions',
    templateUrl     : './beneficiary-medical-conditions.component.html',
    styleUrls       : ['./beneficiary-medical-conditions.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryMedicalConditionsComponent
 */
export class BeneficiaryMedicalConditionsComponent {
    /**
     * BeneficiaryMedicalConditionsComponent constructor
     * @param builder
     * @param beneficiaryMedicalConditionsActions
     */
    constructor(
        private builder                             : FormBuilder,
        private beneficiaryMedicalConditionsActions : BeneficiaryMedicalConditionsActions
    ) {
        // init form input fields
        this.medicalCondition  = new FormControl('', this.validateSelectDropDown);
        this.conditionReported = new FormControl('', createDateValidator(this.dateFormat, true));
        this.conditionReview   = new FormControl('', createDateValidator(this.dateFormat, true));

        // build medical conditions FormControl group
        this.medicalConditionsForm = builder.group({
            medicalCondition  : this.medicalCondition,
            conditionReported : this.conditionReported,
            conditionreview   : this.conditionReview
        });

        this.conditionReported.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.beneficiaryMedicalConditionsActions.updateMedicalConditionField({
                fieldName   : 'fromDate',
                fieldValue  : value
            });
        });

        this.conditionReview.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.beneficiaryMedicalConditionsActions.updateMedicalConditionField({
                fieldName   : 'thruDate',
                fieldValue  : value
            });
        });
    }

    /**
     * boolean toggle indicating component is part of
     * Create Beneficiary Profile form
     * @type {boolean}
     */
    @Input() isNewProfile : boolean;

    /**
     * medical conditions aggregated UI state
     */
    @Input() medicalConditionsState : BeneficiaryMedicalConditionsState;

    /**
     * custom validation for medical condition FormControl
     */
    private validateSelectDropDown(c : FormControl) : { [key : string] : any } {
        if (c.value && c.value !== 'SELECT') {
            return null;
        }

        return {
            validateMedicalCondition : {
                valid : false
            }
        };
    }

    /**
     * Object contains form group
     */
    medicalConditionsForm : FormGroup;

    /**
     * medicalCondition input field
     */
    medicalCondition  : FormControl;

    /**
     * conditionReported input field
     */
    conditionReported : FormControl;

    /**
     * conditionReview input field
     */
    conditionReview : FormControl;

    /**
     * Date format
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for canceling add medical condition event
     */
    cancelAddMedicalCondition() {
        this.beneficiaryMedicalConditionsActions.cancelNewMedicalCondition();
    }

    /**
     * event handler for when a new medical condition is being added
     */
    addBeneficiaryMedicalConditionActive() {
        this.beneficiaryMedicalConditionsActions.updateIsAddMedicalCondition(true);
    }

    /**
     * event handler for removing a single medical condition from a beneficiary
     * @param index the index of the medical condition to be removed
     */
    removeMedicalCondition(index : number) {
        this.beneficiaryMedicalConditionsActions.removeMedicalCondition(index);
    }

    /**
     * event handler for TempMedicalCondition selection event
     * @param choice selected MedicalCondition type
     */
    updateTempMedicalConditionType(choice : KeyValuePair) {
        this.beneficiaryMedicalConditionsActions.updateTempMedicalConditionType(choice);
    }

    /**
     * handler for form submit button.
     */
    onSubmit() {
        this.beneficiaryMedicalConditionsActions.saveMedicalConditions();
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        if (this.medicalConditionsState.get('isAddMedicalConditionActive')) {
            this.beneficiaryMedicalConditionsActions.cancelNewMedicalCondition();
        }
    }
}
