import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl
} from '@angular/forms';
import {List} from 'immutable';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {BeneficiaryEmail} from '../../../../../store/Beneficiary/types/beneficiary-email.model';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';

@Component({
    selector        : 'beneficiary-email',
    templateUrl     : './beneficiary-email.component.html',
    styleUrls       : ['./beneficiary-email.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryEmailComponent: handles display of this section panel
 */
export class BeneficiaryEmailComponent {
    /**
     * BeneficiaryEmailComponent constructor
     * @param builder
     */
    constructor(private builder : FormBuilder) {
        // init form input fields
        this.email              = new FormControl('', undefined);
        this.emailType          = new FormControl('', undefined);
        this.emailNotifications = new FormControl('', undefined);

        // build Beneficiary Information FormControl group
        this.emailForm = builder.group({
            email              : this.email,
            emailType          : this.emailType,
            emailNotifications : this.emailNotifications
        });

        this.emailForm.statusChanges.subscribe(() => {
            // emit updated form valid state
            this.emailFormValid.emit(this.emailForm.valid);
        });

        this.email.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateEmailField.emit({
                fieldName   : 'email',
                fieldValue  : value
            });
        });
    }

    /**
     * beneficiary email
     */
    @Input() beneficiaryEmail : BeneficiaryEmail;

    /**
     * solicitationIndicator dropdown values
     */
    @Input() solicitationIndicatorTypes : List<KeyValuePair>;

    /**
     * available email types for dropdowns
     */
    @Input() emailTypes : List<KeyValuePair>;

    /**
     * event triggered when beneficiary form fields are edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() updateEmailField : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when beneficiary email types are edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() updateEmailType : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when beneficiary email notification types are edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() updateEmailNotificationType : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when the form valid state has changed
     * @type {EventEmitter<boolean>}
     */
    @Output() emailFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * Object contains form group
     */
    emailForm : FormGroup;

    /**
     * email input field
     */
    email : FormControl;

    /**
     * emailType input field
     */
    emailType : FormControl;

    /**
     * emailNotifications input field
     */
    emailNotifications : FormControl;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for emailType selection event
     * @param choice selected email type
     */
    updateEmailTypes(choice : KeyValuePair) {
        // emit updated value
        this.updateEmailType.emit({
            fieldName   : choice.value,
            fieldValue  : choice.id
        });
    }

    /**
     * event handler for emailNotifications selection event
     * @param choice selected email notification type
     */
    updateEmailNotifications(choice : KeyValuePair) {
        // emit updated value
        this.updateEmailNotificationType.emit({
            fieldName   : choice.value,
            fieldValue  : choice.id
        });
    }
}
