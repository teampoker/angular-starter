import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
    ViewChild,
    ElementRef
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {List} from 'immutable';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {BeneficiaryAddress} from '../../../../../store/Beneficiary/types/beneficiary-address.model';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';
import {getAddressComponent} from '../../../../../store/utils/parse-google-places';

@Component({
    selector        : 'beneficiary-address',
    templateUrl     : './beneficiary-address.component.html',
    styleUrls       : ['./beneficiary-address.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryAddressComponent: handles display of this section panel
 */
export class BeneficiaryAddressComponent {
    /**
     * BeneficiaryAddressComponent constructor
     * @param builder
     */
    constructor(private builder : FormBuilder) {
        // init form input fields
        this.street                = new FormControl('', Validators.required);
        this.apt                   = new FormControl('', undefined);
        this.geographicDetails     = new FormControl('', undefined);
        this.addressNotifications  = new FormControl('', undefined);
        this.addressType           = new FormControl('', this.validateSelectDropDown);

        // build Beneficiary Information FormControl group
        this.addressForm = builder.group({
            apt                  : this.apt,
            geographicDetails    : this.geographicDetails,
            addressNotifications : this.addressNotifications,
            street               : this.street,
            addressType          : this.addressType
        });

        this.addressForm.statusChanges.subscribe(() => {
            // emit updated form valid state
            this.addressFormValid.emit(this.addressForm.valid);
        });

        this.street.valueChanges.debounceTime(250).subscribe((value : string) => {
            // do not emit a value if the google places autocomplete is visible
            if (!this.showGooglePlaces) {
                // emit updated value
                this.updateAddressField.emit({
                    fieldName   : 'street',
                    fieldValue  : value
                });

                // was the value an empty string?
                if (value === '') {
                    // show the autocomplete
                    this.showGooglePlaces = true;
                }
            }
        });

        this.apt.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateAddressField.emit({
                fieldName   : 'apt',
                fieldValue  : value
            });
        });

        this.geographicDetails.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateAddressField.emit({
                fieldName   : 'geographicDetails',
                fieldValue  : value
            });
        });
    }

    /**
     * DOM reference for google autocomplete
     */
    @ViewChild('googlePlace') googlePlace : ElementRef;

    /**
     * beneficiary address
     */
    @Input() beneficiaryAddress : BeneficiaryAddress;

    /**
     * solicitationIndicator dropdown values
     */
    @Input() solicitationIndicatorTypes : List<KeyValuePair>;

    /**
     * available address types for dropdowns
     */
    @Input() addressTypes : List<KeyValuePair>;

    /**
     * event triggered when beneficiary form fields are edited/updated
     * @type {EventEmitter<IBeneficiaryFieldUpdate>}
     */
    @Output() updateAddressField : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when beneficiary address notification type are edited/updated
     * @type {EventEmitter<IKeyValuePair>}
     */
    @Output() updateAddressType : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when beneficiary address type are edited/updated
     * @type {EventEmitter<IKeyValuePair>}
     */
    @Output() updateAddressNotification : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when the form valid state has changed
     * @type {EventEmitter<boolean>}
     */
    @Output() addressFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * custom validation for authorizationReason FormControl
     */
    private validateSelectDropDown(c : FormControl) : { [key : string] : any } {
        if (c.value && c.value !== 'SELECT') {
            return null;
        }

        return {
            validateAuthReason : {
                valid : false
            }
        };
    }

    /**
     * local flag used to trigger hide/show of google places autocomplete
     */
    showGooglePlaces : boolean;

    /**
     * Object contains form group
     */
    addressForm : FormGroup;

    /**
     * street input field
     */
    street : FormControl;

    /**
     * apt input field
     */
    apt : FormControl;

    /**
     * addressNotifications input field
     */
    addressNotifications : FormControl;

    /**
     * addressType input field
     */
    addressType : FormControl;

    /**
     * geographicDetails input field
     */
    geographicDetails : FormControl;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for addressNotifications selection event
     * @param choice selected address notification type
     */
    updateAddressNotifications(choice : KeyValuePair) {
        // emit updated value
        this.updateAddressNotification.emit({
            fieldName   : choice.value,
            fieldValue  : choice.id
        });
    }

    /**
     * event handler for addressType selection event
     * @param choice selected address type
     */
    updateAddressTypes(choice : KeyValuePair) {
        // emit updated value
        this.updateAddressType.emit({
            fieldName   : choice.value,
            fieldValue  : choice.id
        });
    }

    /**
     * event handler for location selection event
     * @param choice selected location type
     */
    onUpdateLocation(choice? : google.maps.places.PlaceResult) {
        // parse response into separate address fields and update store
        const components    = choice.address_components,
              city          = getAddressComponent(components, 'locality'),
              streetName    = getAddressComponent(components, 'route'),
              streetNumber  = getAddressComponent(components, 'street_number'),
              state         = getAddressComponent(components, 'administrative_area_level_1', true),
              postalCode    = getAddressComponent(components, 'postal_code');

        // emit updated values

        // street
        this.updateAddressField.emit({
            fieldName   : 'street',
            fieldValue  : streetNumber + ' ' + streetName
        });

        // city
        this.updateAddressField.emit({
            fieldName   : 'city',
            fieldValue  : city
        });

        // state
        this.updateAddressField.emit({
            fieldName   : 'state',
            fieldValue  : state
        });

        // postalCode
        this.updateAddressField.emit({
            fieldName   : 'postalCode',
            fieldValue  : postalCode
        });
    }

    /**
     * event handler autocomplete accelerator
     * @param edit ReservationAccelerator
     */
    onAddressTextEntered(edit : string)  {
        // look for blank text entered here i.e. field value deleted by CSR
        if (edit === '') {
            // clear values

            // street
            this.updateAddressField.emit({
                fieldName   : 'street',
                fieldValue  : ''
            });

            // city
            this.updateAddressField.emit({
                fieldName   : 'city',
                fieldValue  : ''
            });

            // state
            this.updateAddressField.emit({
                fieldName   : 'state',
                fieldValue  : ''
            });

            // postalCode
            this.updateAddressField.emit({
                fieldName   : 'postalCode',
                fieldValue  : ''
            });
        }
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // if initial value of street address is empty string the google places autocomplete needs to be displayed
        this.beneficiaryAddress.get('street') ? this.showGooglePlaces = false : this.showGooglePlaces = true;
    }
}
