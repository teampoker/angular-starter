import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import {
    ReactiveFormsModule,
    FormBuilder
} from '@angular/forms';
import {Http} from '@angular/http';
import {
    TranslateModule,
    TranslateLoader,
    TranslateStaticLoader
} from 'ng2-translate';

import {BeneficiaryAddressComponent} from './beneficiary-address.component';
import {BeneficiaryAddress} from '../../../../../store/Beneficiary/types/beneficiary-address.model';

// mock dependencies

// tests
describe('Component: BeneficiaryAddressComponent', () => {
    let fixture         : ComponentFixture<BeneficiaryAddressComponent>,
        compInstance    : BeneficiaryAddressComponent;

    // use the async() wrapper to allow the Angular template compiler time to read the files
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                BeneficiaryAddressComponent  // declare the test component
                // add dependencies defined in the template
            ],
            imports: [
                // import the translation for template pipes
                TranslateModule.forRoot({
                    provide     : TranslateLoader,
                    useFactory  : (http : Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
                    deps        : [Http]
                }),
                ReactiveFormsModule
            ],
            providers: [
                FormBuilder
            ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(BeneficiaryAddressComponent);
            compInstance = fixture.componentInstance;  // search box test instance
            compInstance.beneficiaryAddress = new BeneficiaryAddress();
        });
    }));

    it('should have address fields initialized', () => {
        expect(compInstance).toBeDefined();
        expect(compInstance.street).toBeDefined();
        expect(compInstance.addressType).toBeDefined();
        expect(compInstance.geographicDetails).toBeDefined();
        expect(compInstance.addressNotifications).toBeDefined();
    });

    // passing in the 'done' async method to ensure that timeouts are not falsely reported and that test doesn't complete prior to events are emitted
    it('should emit address form valid event when street changes', done => {
        const newStreetValue = 'Elm Street';

        // subscribe to the event emitter and setup test assertions/expectations for when the event occurs
        compInstance.updateAddressField.subscribe(af => {
            expect(af).toEqual({
                fieldName   : 'street',
                fieldValue  : newStreetValue
            });
        });

        compInstance.addressFormValid.subscribe(afv => {
            expect(afv).toEqual(compInstance.addressForm.valid);
            done();
        });

        compInstance.street.setValue(newStreetValue);
    });

    it('should emit address form valid event when geographic details changes', done => {
        const newGeographicDetailsValue = 'no idea what this is...';

        // subscribe to the event emitter and setup test assertions/expectations for when the event occurs
        compInstance.updateAddressField.subscribe(af => {
            expect(af).toEqual({
                fieldName   : 'geographicDetails',
                fieldValue  : newGeographicDetailsValue
            });
        });

        compInstance.addressFormValid.subscribe(afv => {
            expect(afv).toEqual(compInstance.addressForm.valid);
            done();
        });

        compInstance.geographicDetails.setValue(newGeographicDetailsValue);
    });
});
