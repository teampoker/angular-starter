import {
    Component,
    Output,
    EventEmitter,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    selector        : 'beneficiary-to-do-panel',
    templateUrl     : './beneficiary-to-do-panel.component.html',
    styleUrls       : ['./beneficiary-to-do-panel.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryToDoPanelComponent: handles display of this section panel
 */
export class BeneficiaryToDoPanelComponent {
    /**
     * BeneficiaryToDoPanelComponent constructor
     */
    constructor() {}

    /**
     * custom event emitted when user wishes to navigate to the reservation area
     */
    @Output() viewReservation : EventEmitter<any> = new EventEmitter<any>();

    /**
     * component init lifecycle hook
     */
    goViewReservation() {
        // emit to viewReservation
        this.viewReservation.emit();
    }
}
