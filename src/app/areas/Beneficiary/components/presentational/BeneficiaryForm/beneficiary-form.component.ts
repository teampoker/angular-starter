import {
    Input,
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    SimpleChanges
} from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    FormControl,
    FormArray,
    Validators
} from '@angular/forms';
import {List} from 'immutable';

import {FormType} from '../../../../../store/Forms/types/form-type.model';
import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {FormsState} from '../../../../../store/Forms/types/forms-state.model';
import {FormsActions} from '../../../../../store/Forms/forms.actions';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';
import {FormStateDetail} from '../../../../../store/Forms/types/form-state-detail.model';
import {createDateValidator} from '../../../../../shared/components/DatePicker/date-picker.component';

@Component({
    selector        : 'beneficiary-form',
    templateUrl     : 'beneficiary-form.component.html',
    styleUrls       : ['beneficiary-form.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryFormComponent: handles display of beneficiary form
 */
export class BeneficiaryFormComponent {
    /**
     * BeneficiaryFormComponent constructor
     */
    constructor(
        private cd                      : ChangeDetectorRef,
        private builder                 : FormBuilder,
        private beneficiaryFormActions  : FormsActions
    ) {
        // init form input fields
        this.formType               = new FormControl('', this.validateSelectDropDown);
        this.serviceOfferings       = new FormControl('', undefined);
        this.effectiveDate          = new FormControl('', Validators.compose([
            Validators.required,
            createDateValidator(this.dateFormat)
        ]));
        this.expirationDate         = new FormControl('', createDateValidator(this.dateFormat));
        this.noExpiration           = new FormControl('', undefined);
        this.authorizedBy           = new FormControl('', Validators.required);
        this.authorizationCode      = new FormControl('', undefined);
        this.authorizationReason    = new FormControl('', undefined);
        this.note                   = new FormControl('', undefined);
        this.phones                 = new FormArray([this.createPhoneFormGroup()]);

        // build FormControl group
        this.beneficiaryForm = builder.group({
            formType            : this.formType,
            serviceOfferings    : this.serviceOfferings,
            effectiveDate       : this.effectiveDate,
            expirationDate      : this.expirationDate,
            noExpiration        : this.noExpiration,
            authorizedBy        : this.authorizedBy,
            authorizationCode   : this.authorizationCode,
            authorizationReason : this.authorizationReason,
            note                : this.note,
            phones              : this.phones
        });

        this.beneficiaryForm.statusChanges.subscribe(() => {
            // emit updated form valid state
            this.beneficiaryFormActions.updateFormValid(this.beneficiaryForm.valid);
        });

        this.authorizationCode.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.beneficiaryFormActions.updateAuthorizedFields({
                fieldName   : 'authorizationCode',
                fieldValue  : value
            });
        });

        this.authorizedBy.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.beneficiaryFormActions.updateAuthorizedFields({
                fieldName   : 'authorizedBy',
                fieldValue  : value
            });
        });

        this.note.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.beneficiaryFormActions.updateAuthorizedFields({
                fieldName   : 'note',
                fieldValue  : value
            });
        });

        this.effectiveDate.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.beneficiaryFormActions.updateEffectiveDate(value);
        });

        this.expirationDate.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.beneficiaryFormActions.updateExpirationDate(value);
        });
    }

    /**
     * current state of overall form validation
     */
    @Input() isFormValid : boolean;

    /**
     * available beneficiary form types
     */
    @Input() formTypes : List<FormType>;

    /**
     * available beneficiary form auth reasons
     */
    @Input() authReasons : List<KeyValuePair>;

    /**
     * possible phone types
     */
    @Input() phoneTypes : List<KeyValuePair>;

    /**
     * list of beneficiary's forms
     */
    @Input() beneficiaryForms : List<FormStateDetail>;

    /**
     * Beneficiary form state
     */
    @Input() beneficiaryFormData : FormStateDetail;

    /**
     * keep track of # of phones beneficiary has saved here
     * @type {number}
     */
    private phoneCount : number = 0;

    /**
     * dynamically creates phones form array for editing
     */
    private buildPhoneFormControls(value : number) {
        // grab reference to array of form phones controls
        const phoneControls : FormArray = this.beneficiaryForm.get('phones') as FormArray;

        if (phoneControls.length > 0) {
            // tslint:disable-next-line: prefer-const
            for (let i = 0, len = phoneControls.length; i < len; i++) {
                // clear existing FormGroup item
                phoneControls.removeAt(i);
            }
        }

        // tslint:disable-next-line: prefer-const
        for (let i = 0, len = value; i < len; i++) {
            // clear existing FormGroup item
            phoneControls.removeAt(i);

            // push new FormGroup item
            phoneControls.push(this.createPhoneFormGroup());
        }
    }

    /**
     * form group builder used to add dynamically added phone fields
     */
    private createPhoneFormGroup() {
        return this.builder.group({
            phoneType   : ['', undefined],
            phone       : ['', undefined]
        });
    }

    /**
     * dynamically adjust validations for various form controls
     */
    private setRequiredValidators () {
        // check validation required
        if (this.noExpiration.value) {
            this.beneficiaryForm.controls['effectiveDate'].setValidators([]);
            this.beneficiaryForm.controls['expirationDate'].setValidators([]);
            this.beneficiaryForm.controls['authorizationReason'].setValidators([this.validateSelectDropDown]);
        }
        else {
            this.beneficiaryForm.controls['effectiveDate'].setValidators([Validators.required]);
            this.beneficiaryForm.controls['expirationDate'].setValidators([
                Validators.required,
                createDateValidator(this.dateFormat)
            ]);
            this.beneficiaryForm.controls['authorizationReason'].setValidators([]);
        }

        // update validations
        this.beneficiaryForm.controls['effectiveDate'].updateValueAndValidity();
        this.beneficiaryForm.controls['expirationDate'].updateValueAndValidity();
        this.beneficiaryForm.controls['authorizationReason'].updateValueAndValidity();
    }

    /**
     * custom validation for authorizationReason FormControl
     */
    private validateSelectDropDown(c : FormControl) : { [key : string] : any } {
        if (c.value && c.value !== 'SELECT') {
            return null;
        }

        return {
            validateAuthReason : {
                valid : false
            }
        };
    }

    /**
     * Object contains form group
     */
    beneficiaryForm : FormGroup;

    /**
     * phones
     */
    phones : FormArray;

    /**
     * Handles form type selection
     */
    formType : FormControl;

    /**
     * Handles form reason selection
     */
    serviceOfferings : FormControl;

    /**
     * Handles effective date
     */
    effectiveDate : FormControl;

    /**
     * Handles expiriation date
     */
    expirationDate : FormControl;

    /**
     * Handles no expiration checkbox selection
     */
    noExpiration : FormControl;

    /**
     * Handles authorized by
     */
    authorizedBy : FormControl;

    /**
     * Handles authorization code
     */
    authorizationCode : FormControl;

    /**
     * Handles authorization reason selection
     */
    authorizationReason : FormControl;

    /**
     * Handles reason note
     */
    note : FormControl;

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for phone form valid updates
     * @param isPhoneValid
     * @param phoneIndex
     */
    onPhoneFormValid(isPhoneValid : boolean, phoneIndex : number) {
        this.beneficiaryFormActions.updatePhoneFormValid({
            isValid             : isPhoneValid,
            primaryUpdateIndex  : phoneIndex
        });
    }

    /**
     * event handler for update beneficiary phone record event
     * @param update new values
     * @param updateIndex index of phone the update needs to be applied to
     */
    onUpdatePhoneField(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch address field update Action
        this.beneficiaryFormActions.updatePhoneField(update);
    }

    /**
     * event handler for update beneficiary phone type selection event
     * @param update new values
     * @param updateIndex index of phone the update needs to be applied to
     */
    onUpdatePhoneType(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch phone type update Action
        this.beneficiaryFormActions.updatePhoneType(update);
    }

    /**
     * event handler for add phones
     */
    addPhone() {
        // grab reference to array of form phone controls
        const phoneControls : FormArray = this.beneficiaryForm.get('phones') as FormArray;

        // add new phone record
        this.beneficiaryFormActions.addAdditionalFormPhone();

        // add new FormGroup
        phoneControls.push(this.createPhoneFormGroup());
    }

    /**
     * event handler for remove phone
     * @param phoneIndex index of phone in phones collection
     */
    removePhone(phoneIndex : number) {
        // grab reference to array of form phone controls
        const phoneControls : FormArray = this.beneficiaryForm.get('phones') as FormArray;

        // remove phone record
        this.beneficiaryFormActions.removeAdditionalFormPhone(phoneIndex);

        // remove FormGroup from array
        phoneControls.removeAt(phoneIndex);
    }

    /**
     * event handler for keyboard key actions
     * @param event DOM event
     */
    editKeyPressHandler(event : { key : string }) {
        if (event.key === 'Enter') {
            this.showAddBeneficiaryForm();
        }
    }

    /**
     * event handler for toggling add beneficiary form data
     */
    showAddBeneficiaryForm() {
        // update current # of phones
        this.phoneCount = this.beneficiaryFormData.get('telecommunicationsNumbers').size;

        // create a FormGroup instance to map to each of the beneficiary's phones
        this.buildPhoneFormControls(this.phoneCount);

        // update current editing state of form
        this.beneficiaryFormActions.updateBeneficiaryShowAddForm();

        // add new phone record
        this.beneficiaryFormActions.addAdditionalFormPhone();
    }

    /**
     * event handler for form type selection event
     */
    updateSelectedFormType(choice : FormType) {
        // emit updated value
        this.beneficiaryFormActions.updateSelectedFormType(choice);

        // only make the select reason dropdown required if
        // there are values to populate it
        if (choice.get('serviceOfferings')) {
            this.beneficiaryForm.controls['serviceOfferings'].setValidators([Validators.required]);
        }
        else {
            this.beneficiaryForm.controls['serviceOfferings'].setValidators([]);
        }

        this.beneficiaryForm.controls['serviceOfferings'].updateValueAndValidity();
    }

    /**
     * event handler for form type selection event
     */
    updateSelectedServiceOffering(choice : KeyValuePair) {
        // emit updated value
        this.beneficiaryFormActions.updateSelectedServiceOffering(choice);
    }

    /**
     * event handler for auth reason selection event
     */
    updateSelectedAuthReason(choice : KeyValuePair) {
        // emit updated value
        this.beneficiaryFormActions.updateSelectedAuthorizationReason(choice);
    }

    /**
     * event handler for no expiration selection event
     * @param event DOM change event
     */
    updateNoExpiration(event : any) {
        const isChecked = event.currentTarget.checked;

        this.beneficiaryFormActions.updateSelectedNoExpiration(isChecked);

        // update dynamic form validation based on this selection
        this.setRequiredValidators();
    }

    /**
     * handler used to reverse any forms changes made in this component
     */
    cancelUpdate() {
        this.beneficiaryFormActions.cancelForm();
    }

    /**
     * handler for form save button
     */
    onSubmit() {
        this.beneficiaryFormActions.saveBeneficiaryForm();
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        // changes.prop contains the old and the new value...
        if (changes['beneficiaryFormData']) {
            // if not valid previous value then component has just initialized
            if (changes['beneficiaryFormData'].previousValue && changes['beneficiaryFormData'].previousValue instanceof FormsState) {
                // build form controls if any items were added/remove from connection
                if (changes['beneficiaryFormData'].currentValue.get('telecommunicationsNumbers').size !== changes['beneficiaryFormData'].previousValue.get('telecommunicationsNumbers').size) {
                    this.buildPhoneFormControls(changes['beneficiaryFormData'].currentValue.get('telecommunicationsNumbers').size);
                }
            }
            else {
                // component just initialized
                this.buildPhoneFormControls(changes['beneficiaryFormData'].currentValue.get('telecommunicationsNumbers').size);
            }
        }
    }
}
