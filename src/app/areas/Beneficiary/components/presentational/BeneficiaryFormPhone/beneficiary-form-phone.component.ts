import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {List} from 'immutable';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';
import {BeneficiaryFormPhone} from '../../../../../store/Forms/types/form-phone.model';
import {formatPhoneForApi, PHONE_MASK_REGEX} from '../../../../../store/utils/parse-phone-numbers';

@Component({
    selector        : 'beneficiary-form-phone',
    templateUrl     : './beneficiary-form-phone.component.html',
    styleUrls       : ['./beneficiary-form-phone.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryFormPhoneComponent: provides functionality to enter phone data in a beneficiary form
 */
export class BeneficiaryFormPhoneComponent {
    /**
     * BeneficiaryFormPhoneComponent constructor
     * @param builder
     */
    constructor(private builder : FormBuilder) {
        // init form input fields
        this.phoneNumber = new FormControl('', Validators.compose([
            Validators.required,
            Validators.pattern(/^(\(\d{3}\))|(\d{3}-)\d{3}-\d{4}$/)
        ]));

        this.phoneType = new FormControl('', Validators.required);

        // build Beneficiary Information FormControl group
        this.phoneForm = builder.group({
            phoneNumber        : this.phoneNumber,
            phoneType          : this.phoneType
        } /*{
            validator : (formGroup : FormGroup) => this.faxRequired(formGroup)
        }*/);

        this.phoneForm.statusChanges.subscribe(() => {
            // emit updated form valid state
            this.phoneFormValid.emit(this.phoneForm.valid);
        });

        this.phoneNumber.valueChanges.debounceTime(250).subscribe((value : string) => {
            value = formatPhoneForApi(value);

            // emit updated value
            this.updatePhoneField.emit({
                fieldName   : 'phone',
                fieldValue  : value
            });
        });
    }

    /**
     * beneficiary phone
     */
    @Input() beneficiaryPhone : BeneficiaryFormPhone;

    /**
     * available phone types for dropdowns
     */
    @Input() phoneTypes : List<KeyValuePair>;

    /**
     * event triggered when beneficiary form fields are edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() updatePhoneField : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when beneficiary phone type are edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() updatePhoneType : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when the form valid state has changed
     * @type {EventEmitter<boolean>}
     */
    @Output() phoneFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * Object contains form group
     */
    phoneForm : FormGroup;

    /**
     * phone input field
     */
    phoneNumber : FormControl;

    /**
     * phoneType input field
     */
    phoneType : FormControl;

    /**
     * phoneMaskRegex : regext for phone number mask
     */
    phoneMaskRegex  :  Array<any> = PHONE_MASK_REGEX;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for phone type selection event
     * @param choice selected phone type
     * @param event DOM click event
     */
    updatePhonePurposeType(choice : KeyValuePair, event : any) {
        event.preventDefault();
        event.stopPropagation();

        // emit updated value
        this.updatePhoneType.emit({
            fieldName   : 'purposeType',
            fieldValue  : choice
        });
    }
}
