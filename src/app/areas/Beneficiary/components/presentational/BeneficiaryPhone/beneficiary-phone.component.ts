import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {List} from 'immutable';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';
import {BeneficiaryPhone} from '../../../../../store/Beneficiary/types/beneficiary-phone.model';
import {formatPhoneForApi, PHONE_MASK_REGEX} from '../../../../../store/utils/parse-phone-numbers';

@Component({
    selector        : 'beneficiary-phone',
    templateUrl     : './beneficiary-phone.component.html',
    styleUrls       : ['./beneficiary-phone.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryPhoneComponent: handles display of this section panel
 */
export class BeneficiaryPhoneComponent {
    /**
     * BeneficiaryPhoneComponent constructor
     * @param builder
     */
    constructor(private builder : FormBuilder) {
        // init form input fields
        this.phone              = new FormControl('', Validators.compose([
            Validators.required,
            Validators.pattern(/^(\(\d{3}\))|(\d{3}-)\d{3}-\d{4}$/)
        ]));
        this.phoneType          = new FormControl('', this.validateSelectDropDown);
        this.phoneNotifications = new FormControl('');

        // build Beneficiary Information FormControl group
        this.phoneForm = builder.group({
            phone              : this.phone,
            phoneType          : this.phoneType,
            phoneNotifications : this.phoneNotifications
        });

        this.phoneForm.statusChanges.subscribe(() => {
            // emit updated form valid state
            this.phoneFormValid.emit(this.phoneForm.valid);
        });

        this.phone.valueChanges.debounceTime(250).subscribe((value : string) => {
            // format entered value for format api expects
            value = formatPhoneForApi(value);

            // emit updated value
            this.updatePhoneField.emit({
                fieldName   : 'phone',
                fieldValue  : value
            });
        });
    }

    /**
     * beneficiary phone
     */
    @Input() beneficiaryPhone : BeneficiaryPhone;

    /**
     * solicitationIndicator dropdown values
     */
    @Input() solicitationIndicatorTypes : List<KeyValuePair>;

    /**
     * available phone types for dropdowns
     */
    @Input() phoneTypes : List<KeyValuePair>;

    /**
     * event triggered when beneficiary form fields are edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() updatePhoneField : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when beneficiary phone type are edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() updatePhoneType : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when beneficiary phone notification type are edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() updatePhoneNotificationType : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when the form valid state has changed
     * @type {EventEmitter<boolean>}
     */
    @Output() phoneFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * custom validation for authorizationReason FormControl
     */
    private validateSelectDropDown(c : FormControl) : { [key : string] : any } {
        if (c.value && c.value !== 'SELECT') {
            return null;
        }

        return {
            validateAuthReason : {
                valid : false
            }
        };
    }

    /**
     * Object contains form group
     */
    phoneForm : FormGroup;

    /**
     * phone input field
     */
    phone : FormControl;

    /**
     * phoneType input field
     */
    phoneType : FormControl;

    /**
     * phoneNotifications input field
     */
    phoneNotifications : FormControl;

    /**
     * phoneMaskRegex : regext for phone number mask
     */
    phoneMaskRegex  :  Array<any> = PHONE_MASK_REGEX;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for phone type selection event
     * @param choice selected phone type
     */
    updatePhoneTypes(choice : KeyValuePair) {
        // emit updated value
        this.updatePhoneType.emit({
            fieldName   : choice.value,
            fieldValue  : choice.id
        });
    }

    /**
     * event handler for phoneNotifications selection event
     * @param choice selected phone notifications type
     */
    updatePhoneNotifications(choice : KeyValuePair) {
        // emit updated value
        this.updatePhoneNotificationType.emit({
            fieldName   : choice.value,
            fieldValue  : choice.id
        });
    }
}
