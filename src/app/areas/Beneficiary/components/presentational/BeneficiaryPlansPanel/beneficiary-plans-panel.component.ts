import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
    SimpleChanges
} from '@angular/core';
import {
    FormBuilder,
    FormArray,
    FormGroup,
    Validators
} from '@angular/forms';

import {BeneficiaryPlansActions} from '../../../../../store/Beneficiary/actions/beneficiary-plans.actions';
import {BeneficiaryPlansState} from '../../../../../store/Beneficiary/types/beneficiary-plans-state.model';

@Component({
    selector        : 'beneficiary-plans-panel',
    templateUrl     : './beneficiary-plans-panel.component.html',
    styleUrls       : ['./beneficiary-plans-panel.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryPlansPanelComponent: handles display of this section panel
 */
export class BeneficiaryPlansPanelComponent {
    /**
     * BeneficiaryPlansPanelComponent constructor
     * @param builder
     * @param beneficiaryPlansActions
     */
    constructor(
        private builder                 : FormBuilder,
        private beneficiaryPlansActions : BeneficiaryPlansActions
    ) {
        // init form input fields
        this.coverages = new FormArray([]);

        // build Beneficiary Information FormControl group
        this.beneficiaryPlansForm = builder.group({
            coverages : this.coverages
        });
    }

    /**
     * boolean toggle indicating component is part of
     * Create Beneficiary Profile form
     * @type {boolean}
     */
    @Input() isNewProfile : boolean;

    /**
     * beneficiary plans UI state
     */
    @Input() plansState : BeneficiaryPlansState;

    /**
     * event triggered when the beneficiary plans form valid flag changes
     * @type {EventEmitter<boolean>}
     */
    @Output() plansFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * dynamically creates plans form array for editing
     */
    private buildFormControls(value : number) {
        const coverageControls : FormArray = this.beneficiaryPlansForm.get('coverages') as FormArray;

        if (coverageControls.length > 0) {
            // tslint:disable-next-line: prefer-const
            for (let i = 0, len = coverageControls.length; i < len; i++) {
                // clear existing FormGroup item
                coverageControls.removeAt(i);
            }
        }

        // tslint:disable-next-line: prefer-const
        for (let i = 0, len = value; i < len; i++) {
            // clear existing FormGroup item
            coverageControls.removeAt(i);

            // push new FormGroup item
            coverageControls.push(this.createPlansFormGroup());
        }
    }

    /**
     * Object contains form group
     */
    beneficiaryPlansForm : FormGroup;

    /**
     * coverages form array
     */
    coverages : FormArray;

    /**
     * plan form valid state
     */
    validPlanForm : boolean = true;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * form group builder used to add dynamically added plan fields
     */
    createPlansFormGroup() {
        return this.builder.group({
            name          : ['', Validators.required],
            beneficiaryId : ['', Validators.required],
            description   : ['', Validators.required],
            phone         : [''],
            subPlan       : [''],
            fromDate      : [''],
            thruDate      : ['']
        });
    }

    /**
     * update the plan form valid state
     */
    onPlanFormValid(event : boolean) {
        this.validPlanForm = event;

        this.plansFormValid.emit(this.validPlanForm);
    }

    /**
     * event handler for add new plan click
     */
    addBeneficiaryPlan() {
        this.beneficiaryPlansActions.addBeneficiaryPlan();
    }

    /**
     * event handler for add plan keypress
     * @param event
     */
    addPlanKeyPressHandler(event : { key : string }) {
        if (event.key === 'Enter') {
            this.beneficiaryPlansActions.addBeneficiaryPlan();
        }
    }

    /**
     * event handler for editing beneficiary plans
     * @param editIndex
     * @returns {boolean}
     */
    editBeneficiaryPlan(editIndex : number) {
        this.beneficiaryPlansActions.editBeneficiaryPlan(editIndex);
    }

    /**
     * event handler for edit plan keypress
     * @param event
     * @param editIndex
     */
    editPlanKeyPressHandler(event : { key : string }, editIndex : number) {
        if (event.key === 'Enter') {
            this.beneficiaryPlansActions.editBeneficiaryPlan(editIndex);
        }
    }

    /**
     * event handler for remove plan click
     * @param removeIndex number
     */
    removeBeneficiaryPlan(removeIndex : number) {
        // dispatch action to remove this plan
        this.beneficiaryPlansActions.removeBeneficiaryPlan(removeIndex);
    }

    /**
     * event handler for remove plan keypress
     * @param event
     * @param removeIndex
     */
    removePlanKeyPressHandler(event : { key : string }, removeIndex : number) {
        if (event.key === 'Enter') {
            // dispatch action to remove this plan
            this.beneficiaryPlansActions.removeBeneficiaryPlan(removeIndex);
        }
    }

    /**
     * event handler for canceling add new plan
     */
    cancelAdd() {
        // dispatch action
        this.beneficiaryPlansActions.cancelAddBeneficiaryPlan();
    }

    /**
     * event handler for saving new plan
     */
    onCloseAdd() {
        // save updated plan info to redux store
        this.beneficiaryPlansActions.saveBeneficiaryPlans();

        // dispatch action
        this.beneficiaryPlansActions.closeAddBeneficiaryPlan();
    }

    /**
     * event handler for canceling edit mode
     */
    cancelEdit() {
        this.beneficiaryPlansActions.cancelEditBeneficiaryPlan();
    }

    /**
     * Helper function to determine if there's benefciary plans in the collection.
     * @returns {boolean}
     */
    hasPlans() {
        return (
            // the collection contains at least 1 element
            this.plansState.plansStateSummary.size > 0 &&
            // the first item must be an 'actual' record, not a seed with defaults
            this.plansState.plansStateSummary.first().planUuid !== '');
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        // changes.prop contains the old and the new value...
        if (changes['plansState']) {
            // if not valid previous value then component has just initialized
            if (changes['plansState'].previousValue && changes['plansState'].previousValue instanceof BeneficiaryPlansState) {
                // build form controls if any items were added/remove from plansStateSummary
                if (changes['plansState'].currentValue.get('plansStateSummary').size !== changes['plansState'].previousValue.get('plansStateSummary').size) {
                    this.buildFormControls(changes['plansState'].currentValue.get('plansStateSummary').size);
                }
            }
            else {
                // component just initialized
                this.buildFormControls(changes['plansState'].currentValue.get('plansStateSummary').size);
            }
        }
    }
}
