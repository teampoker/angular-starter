import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import * as moment from 'moment';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {BeneficiaryInfoState} from '../../../../../store/Beneficiary/types/beneficiary-info-state.model';
import {BeneficiaryInfoActions} from '../../../../../store/Beneficiary/actions/beneficiary-info.actions';
import {LanguageType} from '../../../../../store/MetaDataTypes/types/language-type.model';
import {BeneficiaryInfoStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-info.selectors';
import {createDateValidator} from '../../../../../shared/components/DatePicker/date-picker.component';

@Component({
    selector        : 'beneficiary-information',
    templateUrl     : './beneficiary-information.component.html',
    styleUrls       : ['./beneficiary-information.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryInformationComponent: handles display of beneficiary status cards
 */
export class BeneficiaryInformationComponent {
    /**
     * BeneficiaryInformationComponent constructor
     * @param builder
     * @param beneficiaryInfoActions
     * @param beneficiarySelector
     */
    constructor(
        private builder                 : FormBuilder,
        private beneficiaryInfoActions  : BeneficiaryInfoActions,
        private beneficiarySelector     : BeneficiaryInfoStateSelectors
    ) {
        // init form input fields
        this.personalTitle          = new FormControl('', undefined);
        this.firstName              = new FormControl('', Validators.required);
        this.lastName               = new FormControl('', Validators.required);
        this.middleName             = new FormControl('', undefined);
        this.birthDate              = new FormControl('', Validators.compose([
            Validators.required,
            createDateValidator(this.dateFormat)
        ]));
        this.suffix                 = new FormControl('', undefined);
        this.deathDate              = new FormControl('', undefined);
        this.isDeceasedYesButton    = new FormControl('', undefined);
        this.isDeceasedNoButton     = new FormControl('', undefined);
        this.gender                 = new FormControl('', this.validateSelectDropDown);
        this.weight                 = new FormControl('', undefined);
        this.heightFeet             = new FormControl('', undefined);
        this.heightInches           = new FormControl('', undefined);
        this.language               = new FormControl('', undefined);

        // build FormControl group
        this.beneficiaryInfoForm = builder.group({
            personalTitle       : this.personalTitle,
            firstName           : this.firstName,
            lastName            : this.lastName,
            middleName          : this.middleName,
            birthDate           : this.birthDate,
            suffix              : this.suffix,
            deathDate           : this.deathDate,
            isDeceasedYesButton : this.isDeceasedYesButton,
            isDeceasedNoButton  : this.isDeceasedNoButton,
            gender              : this.gender,
            weight              : this.weight,
            heightFeet          : this.heightFeet,
            heightInches        : this.heightInches,
            language            : this.language
        });

        this.beneficiaryInfoForm.valueChanges.subscribe(() => {
            // emit update form valid state
            this.beneficiaryInfoFormValid.emit(this.beneficiaryInfoForm.valid);
        });

        this.firstName.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'firstName',
                fieldValue  : value
            });
        });

        this.lastName.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'lastName',
                fieldValue  : value
            });
        });

        this.middleName.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'middleName',
                fieldValue  : value
            });
        });

        this.suffix.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'suffix',
                fieldValue  : value
            });
        });

        this.weight.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'weight',
                fieldValue  : value
            });
        });

        this.birthDate.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'birthDate',
                fieldValue  : value
            });
        });

        this.deathDate.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'deathDate',
                fieldValue  : value
            });
        });
    }

    /**
     * boolean toggle indicating component is part of
     * Create Beneficiary Profile form
     * @type {boolean}
     */
    @Input() isNewProfile : boolean;

    /**
     * beneficiary info UI state
     */
    @Input() beneficiaryInfoState : BeneficiaryInfoState;

    /**
     * event triggered when the beneficiary information form valid flag changes
     * @type {EventEmitter<boolean>}
     */
    @Output() beneficiaryInfoFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * custom validation for gender type FormControl
     */
    private validateSelectDropDown(c : FormControl) : { [key : string] : any } {
        if (c.value && c.value !== 'SELECT') {
            return null;
        }

        return {
            validateGender : {
                valid : false
            }
        };
    }

    /**
     * Object contains form group
     */
    beneficiaryInfoForm : FormGroup;

    /**
     * first name input field
     */
    firstName : FormControl;

    /**
     * middle name input field
     */
    middleName : FormControl;

    /**
     * last name input field
     */
    lastName : FormControl;

    /**
     * suffix input field
     */
    suffix : FormControl;

    /**
     * date of birth input field
     */
    birthDate : FormControl;

    /**
     * date of birth input field
     */
    deathDate : FormControl;

    /**
     * YES deceased radio button
     */
    isDeceasedYesButton : FormControl;

    /**
     * NO deceased radio button
     */
    isDeceasedNoButton : FormControl;

    /**
     * height in feet input field
     */
    heightFeet : FormControl;

    /**
     * height in inches input field
     */
    heightInches : FormControl;

    /**
     * primary language input field
     */
    language : FormControl;

    /**
     * weight input field
     */
    weight : FormControl;

    /**
     * gender input field
     */
    gender : FormControl;

    /**
     * personalTitle input field
     */
    personalTitle : FormControl;

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * store display name used in accelerator
     */
    displayPropertyName : string = '';

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * determine max available date the birthDate pickers should allow
     */
    setMaxBirthDate() : string {
        return this.isNewProfile ? moment().format(this.dateFormat) : this.beneficiaryInfoState.getIn(['beneficiaryInfo', 'birthDate']);
    }

    /**
     * determine max available date the deceasedDate pickers should allow
     */
    setMinDeceasedDate() : string {
        return this.isNewProfile ? undefined : this.beneficiaryInfoState.getIn(['beneficiaryInfo', 'birthDate']);
    }

    /**
     * event handler for keyboard key actions
     * @param event DOM event
     */
    editKeyPressHandler(event : { key : string }) {
         if (event.key === 'Enter') {
             this.editBeneficiaryInformation();
         }
    }

    /**
     * event handler for gender selection event
     * @param choice selected gender type
     */
    updateGender(choice : KeyValuePair) {
        // emit updated value
        this.beneficiaryInfoActions.updateBeneficiaryInfoField({
            fieldName   : 'gender',
            fieldValue  : choice.value
        });
    }

    /**
     * event handler for height selection event
     * @param choice selected height
     */
    updateHeight(choice : KeyValuePair) {
        // emit updated value
        this.beneficiaryInfoActions.updateBeneficiaryInfoField({
            fieldName   : 'heightFeet',
            fieldValue  : choice.value
        });
    }

    /**
     * event handler for inches selection event
     * @param choice selected inches length
     */
    updateInches(choice : KeyValuePair) {
        // emit updated value
        this.beneficiaryInfoActions.updateBeneficiaryInfoField({
            fieldName   : 'heightInches',
            fieldValue  : choice.value
        });
    }

    /**
     * event handler for language selection event
     * @param choice selected language type
     */
    updateLanguage(choice : LanguageType | string)  {
        if (choice === 'Select') {
            // emit updated value
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'language',
                fieldValue  : choice
            });
        }
        else if (typeof choice !== 'string') {
            // emit updated value
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'language',
                fieldValue  : choice.get('value')
            });
        }

        if (typeof choice !== 'string') {
            // emit update form valid state
            this.beneficiaryInfoFormValid.emit(this.beneficiaryInfoForm.valid);
        }
    }

    /**
     * custom ng2-auto-complete list formatter
     * @param data
     * @returns {string}
     */
    listFormatter(data : any) : string {
        let html : string = '';

        html += data[this.displayPropertyName] ? data[this.displayPropertyName] + ' ' : '';

        return html;
    }

    /**
     * event handler for deceased radio button
     * @param value
     */
    updateDeceased(value : boolean) {
        // emit updated value
        this.beneficiaryInfoActions.updateBeneficiaryInfoField({
            fieldName   : 'isDeceased',
            fieldValue  : value
        });

        // if deceased set to NO then clear any deathDate value
        if (!value) {
            this.beneficiaryInfoActions.updateBeneficiaryInfoField({
                fieldName   : 'deathDate',
                fieldValue  : undefined
            });
        }

        // update required state of deathDate field
        value ? this.deathDate.setValidators([Validators.required, createDateValidator(this.dateFormat)]) : this.deathDate.setValidators([]);

        this.deathDate.updateValueAndValidity();
    }

    /**
     * event handler for personalTitle selection event
     * @param choice selected language type
     */
    updatePersonalTitle(choice : KeyValuePair) {
        // emit updated value
        this.beneficiaryInfoActions.updateBeneficiaryInfoField({
            fieldName   : 'personalTitle',
            fieldValue  : choice.value
        });
    }

    /**
     * event handler for toggling editing beneficiary information
     */
    editBeneficiaryInformation() {
        // reset form state
        this.beneficiaryInfoForm.reset({
            // restore control value of datepicker otherwise it will display as blank
            birthDate : this.beneficiaryInfoState.getIn(['beneficiaryInfo', 'birthDate'])
        });

        // update current editing state of form
        this.beneficiaryInfoActions.updateBeneficiaryInfoEditing(true);
    }

    /**
     * handler used to reverse any beneficiary changes made in this component
     */
    cancelUpdate() {
        // update current editing state of form
        this.beneficiaryInfoActions.updateBeneficiaryInfoEditing(false);
    }

    /**
     * handler for form save button
     */
    onSubmit() {
        this.beneficiaryInfoActions.saveBeneficiaryInfo();
    }
}
