import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
    SimpleChanges
} from '@angular/core';
import {
    FormBuilder,
    FormArray,
    FormGroup,
    Validators
} from '@angular/forms';

import {BeneficiaryConnectionsActions} from '../../../../../store/Beneficiary/actions/beneficiary-connections.actions';
import {BeneficiaryConnectionsState} from '../../../../../store/Beneficiary/types/beneficiary-connections-state.model';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';
import {EnumConnectionTypes} from '../../../../../store/Beneficiary/types/beneficiary-connection-types.model';
import {SearchActions} from '../../../../../store/Search/search.actions';

@Component({
    selector        : 'beneficiary-connections-panel',
    templateUrl     : './beneficiary-connections-panel.component.html',
    styleUrls       : ['./beneficiary-connections-panel.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryConnectionsPanelComponent: handles display of this section panel
 */
export class BeneficiaryConnectionsPanelComponent {
    /**
     * BeneficiaryConnectionsPanelComponent constructor
     * @param builder
     * @param beneficiaryConnectionsActions
     * @param searchActions
     */
    constructor(
        private builder                         : FormBuilder,
        private beneficiaryConnectionsActions   : BeneficiaryConnectionsActions,
        private searchActions                   : SearchActions
    ) {
        // init form input fields
        this.connections = new FormArray([]);

        // build Beneficiary Information FormControl group
        this.beneficiaryConnectionsForm = builder.group({
            // placeholder for dynamically generated form groups
            connections : this.connections
        });
    }

    /**
     * boolean toggle indicating component is part of
     * Create Beneficiary Profile form
     */
    @Input() isNewProfile : boolean;

    /**
     * connections panel UI state
     */
    @Input() connectionsState : BeneficiaryConnectionsState;

    /**
     * event triggered when the beneficiary connections form valid flag changes
     * @type {EventEmitter<boolean>}
     */
    @Output() connectionsFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * dynamically creates connections form array for editing
     */
    private buildFormControls(value : number) {
        const connectionsControls : FormArray = this.beneficiaryConnectionsForm.get('connections') as FormArray;

        if (connectionsControls.length > 0) {
            // tslint:disable-next-line: prefer-const
            for (let i = 0, len = connectionsControls.length; i < len; i++) {
                // clear existing FormGroup item
                connectionsControls.removeAt(i);
            }
        }

        // tslint:disable-next-line: prefer-const
        for (let i = 0, len = value; i < len; i++) {
            // clear existing FormGroup item
            connectionsControls.removeAt(i);

            // push new FormGroup item
            connectionsControls.push(this.createConnectionsFormGroup());
        }
    }

    /**
     * Object contains form group
     */
    beneficiaryConnectionsForm : FormGroup;

    /**
     * connections for array
     */
    connections : FormArray;

    /**
     * valid state of connections form
     */
    validConnectionForm : boolean;

    /**
     * Enum connection types to match against
     * @type {EnumConnectionTypes}
     */
    connectionTypes : typeof EnumConnectionTypes = EnumConnectionTypes;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * form group builder used to add dynamically added connection fields
     */
    createConnectionsFormGroup() : FormGroup {
        return this.builder.group({
            salutation    : ['', undefined],
            firstName     : ['', Validators.required],
            lastName      : ['', Validators.required],
            middleName    : ['', undefined],
            subConnection : ['', undefined],
            suffix        : ['', undefined],
            phones        : ['', undefined]
        });
    }

    /**
     * update the connection form valid state
     */
    onConnectionFormValid(event : boolean) {
        this.validConnectionForm = event;

        this.connectionsFormValid.emit(this.validConnectionForm);
    }

    /**
     * event handler for adding beneficiary connections
     */
    addBeneficiaryConnection() {
        // clear relevant people search state
        this.searchActions.resetPeopleSearchState();

        // set index of next connection to be added
        this.beneficiaryConnectionsActions.addBeneficiaryConnection();
    }

    /**
     * event handler for add connection keypress
     * @param event
     */
    addConnectionKeyPressHandler(event : { key : string }) {
        if (event.key === 'Enter') {
            this.beneficiaryConnectionsActions.addBeneficiaryConnection();
        }
    }

    /**
     * event handler for editing beneficiary connections
     * @param editIndex
     *
     * @returns {boolean}
     */
    editBeneficiaryConnection(editIndex : number) {
        // update edit connection index
        this.beneficiaryConnectionsActions.editBeneficiaryConnection(editIndex);
    }

    /**
     * event handler for edit connection keypress
     * @param event
     * @param editIndex
     */
    editConnectionKeyPressHandler(event : { key : string }, editIndex : number) {
        if (event.key === 'Enter') {
            this.beneficiaryConnectionsActions.editBeneficiaryConnection(editIndex);
        }
    }

    /**
     * event handler for remove connection click
     * @param removeIndex number
     */
    removeBeneficiaryConnection(removeIndex : number) {
        // dispatch action to remove this connection
        this.beneficiaryConnectionsActions.removeBeneficiaryConnection(removeIndex);
    }

    /**
     * event handler for remove connection keypress
     * @param event
     * @param removeIndex
     */
    removeConnectionKeyPressHandler(event : { key : string }, removeIndex : number) {
        if (event.key === 'Enter') {
            // dispatch action to remove this connection
            this.beneficiaryConnectionsActions.removeBeneficiaryConnection(removeIndex);
        }
    }

    /**
     * event handler for canceling add new connection
     */
    cancelAdd() {
        // dispatch action
        this.beneficiaryConnectionsActions.cancelAddBeneficiaryConnection();
    }

    /**
     * event handler for canceling edit mode
     */
    cancelEdit() {
        this.beneficiaryConnectionsActions.cancelEditBeneficiaryConnection();
    }

    /**
     * event handler for update beneficiary email record event
     * @param update new values
     * @param updateIndex index of email the update needs to be applied to
     */
    onUpdateConnectionField(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch address field update Action
        this.beneficiaryConnectionsActions.updateConnectionField(update);
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        // changes.prop contains the old and the new value...
        if (changes['connectionsState']) {
            // if not valid previous value then component has just initialized
            if (changes['connectionsState'].previousValue && changes['connectionsState'].previousValue instanceof BeneficiaryConnectionsState) {
                // build form controls if any items were added/remove from connectionsStateDetail
                if (changes['connectionsState'].currentValue.get('connectionsStateDetail').size !== changes['connectionsState'].previousValue.get('connectionsStateDetail').size) {
                    this.buildFormControls(changes['connectionsState'].currentValue.get('connectionsStateDetail').size);
                }
            }
            else {
                // component just initialized
                this.buildFormControls(changes['connectionsState'].currentValue.get('connectionsStateDetail').size);
            }
        }
    }
}
