import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {BeneficiarySpecialRequirementsState} from '../../../../../store/Beneficiary/types/beneficiary-special-requirements-state.model';
import {BeneficiarySpecialRequirementsActions} from '../../../../../store/Beneficiary/actions/beneficiary-special-requirements.actions';
import {createDateValidator} from '../../../../../shared/components/DatePicker/date-picker.component';

@Component({
    selector        : 'beneficiary-special-requirements',
    templateUrl     : './beneficiary-special-requirements.component.html',
    styleUrls       : ['./beneficiary-special-requirements.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiarySpecialRequirementsComponent: handles display of beneficiary status cards
 */
export class BeneficiarySpecialRequirementsComponent {
    /**
     * BeneficiarySpecialRequirementsComponent constructor
     * @param builder
     * @param beneficiarySpecialRequirementsActions
     */
    constructor(
        private builder                                 : FormBuilder,
        private beneficiarySpecialRequirementsActions   : BeneficiarySpecialRequirementsActions
    ) {
        // init form input fields
        this.specialRequirement     = new FormControl('', this.validateSelectDropDown);
        this.requirementStartDate   = new FormControl('', createDateValidator(this.dateFormat));
        this.requirementThruDate    = new FormControl('', createDateValidator(this.dateFormat));
        this.permanentRequirement   = new FormControl('', undefined);

        // build special requirements FormControl group
        this.specialRequirementsForm = builder.group({
            specialRequirement    : this.specialRequirement,
            requirementStartDate  : this.requirementStartDate,
            requirementEndDate    : this.requirementThruDate,
            permanentRequirement  : this.requirementThruDate
        });

        this.requirementStartDate.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.beneficiarySpecialRequirementsActions.updateSpecialRequirementField({
                fieldName   : 'fromDate',
                fieldValue  : value
            });
        });

        this.requirementThruDate.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.beneficiarySpecialRequirementsActions.updateSpecialRequirementField({
                fieldName   : 'thruDate',
                fieldValue  : value
            });
        });
    }

    /**
     * boolean toggle indicating component is part of
     * Create Beneficiary Profile form
     * @type {boolean}
     */
    @Input() isNewProfile : boolean;

    /**
     * special requirements aggregated UI state
     */
    @Input() specialRequirementsState : BeneficiarySpecialRequirementsState;

    /**
     * boolean toggle indicating component is part of
     * read only dashboard
     * @type {boolean}
     */
    @Input() isReadOnly : boolean = false;

    /**
     * custom validation for specialRequirement FormControl
     */
    private validateSelectDropDown(c : FormControl) : { [key : string] : any } {
        if (c.value && c.value !== 'SELECT') {
            // check for KeyValuePair here
            if (c.value instanceof KeyValuePair) {
                if (c.value.get('value') !== 'SELECT') {
                    return null;
                }
            }
            else {
                return null;
            }
        }

        return {
            validateSpecialRequirement : {
                valid : false
            }
        };
    }

    /**
     * adjusts validations on the fromDate/thruDate form controls
     * based on whether or not this is a permanent special requirement or not
     * @param isPermanentRequirement
     */
    private updateDateValidations(isPermanentRequirement : boolean) {
        // is permanent requirement true
        if (isPermanentRequirement) {
            this.requirementStartDate.setValidators([]);
            this.requirementThruDate.setValidators([]);
        }
        else {
            this.requirementStartDate.setValidators(Validators.compose([
                Validators.required,
                createDateValidator(this.dateFormat)
            ]));

            this.requirementThruDate.setValidators(Validators.compose([
                Validators.required,
                createDateValidator(this.dateFormat)
            ]));
        }

        this.requirementStartDate.updateValueAndValidity();
        this.requirementThruDate.updateValueAndValidity();
    }

    /**
     * grouping object for special requirements validation form
     */
    specialRequirementsForm : FormGroup;

    /**
     * specialRequirement input field
     */
    specialRequirement  : FormControl;

    /**
     * requirementStartDate input field
     */
    requirementStartDate : FormControl;

    /**
     * requirementThruDate input field
     */
    requirementThruDate   : FormControl;

    /**
     * permanentRequirement input field
     */
    permanentRequirement   : FormControl;

    /**
     * Date format
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for canceling add special requirement event
     */
    cancelAddSpecialRequirement() {
        this.beneficiarySpecialRequirementsActions.cancelNewSpecialRequirement();
    }

    /**
     * event handler for when a new special requirement is being added
     */
    addBeneficiarySpecialRequirementActive() {
        this.beneficiarySpecialRequirementsActions.updateIsAddSpecialRequirement(true);

        // update form validations
        this.updateDateValidations(this.specialRequirementsState.getIn(['tempSpecialRequirement', 'permanentRequirement']));
    }

    /**
     * event handler for removing a single special requirement from a beneficiary
     * @param index the index of the special requirement to be removed
     */
    removeSpecialRequirement(index : number) {
        this.beneficiarySpecialRequirementsActions.removeSpecialRequirement(index);
    }

    /**
     * event handler for TempSpecialRequirement selection event
     * @param choice selected SpecialRequirement type
     */
    updateTempSpecialRequirementType(choice : KeyValuePair) {
        this.beneficiarySpecialRequirementsActions.updateSpecialRequirementType(choice);
    }

    /**
     * event handler for update connection type PermanentRequirement selection event
     * @param value
     */
    updatePermanentRequirement(value : boolean) {
        // emit updated value
        this.beneficiarySpecialRequirementsActions.updateSpecialRequirementField({
            fieldName   : 'permanentRequirement',
            fieldValue  : !value
        });

        // update form validation based on this value
        this.updateDateValidations(!value);
    }

    /**
     * handler for form submit button.
     */
    onSubmit() {
        this.beneficiarySpecialRequirementsActions.saveSpecialRequirement();
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        this.updateDateValidations(this.specialRequirementsState.getIn(['tempSpecialRequirement', 'permanentRequirement']));
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        if (this.specialRequirementsState.get('isAddSpecialRequirementActive')) {
            this.beneficiarySpecialRequirementsActions.cancelNewSpecialRequirement();
        }
    }
}
