import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy
} from '@angular/core';

import {
    EnumBeneficiaryHeaderType,
    BeneficiaryHeader
} from '../../../../../store/Beneficiary/types/beneficiary-header.model';

@Component({
    selector        : 'beneficiary-header',
    templateUrl     : './beneficiary-header.component.html',
    styleUrls       : ['./beneficiary-header.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryHeaderComponent: handles display of beneficiary status cards title
 */
export class BeneficiaryHeaderComponent {
    /**
     * BeneficiaryHeaderComponent constructor
     */
    constructor() { }

    /**
     * Object containing header information
     * @type {Object}
     */
    @Input() headerConfig : BeneficiaryHeader;

    /**
     * custom event emitted when user wishes to navigate to add beneficiary profile
     */
    @Output() viewBeneficiaryProfile : EventEmitter<any> = new EventEmitter<any>();

    /**
     * exposing beneficiary header enum types to component's template
     * @type {EnumBeneficiaryHeaderType}
     */
    headerTypes : typeof EnumBeneficiaryHeaderType = EnumBeneficiaryHeaderType;

    /**
     * component init lifecycle hook
     */
    goViewProfile() {
        // emit to viewBeneficiaryProfile
        this.viewBeneficiaryProfile.emit();
    }
}
