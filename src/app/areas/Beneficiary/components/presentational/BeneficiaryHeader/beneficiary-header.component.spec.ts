import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {
    TranslateModule,
    TranslateStaticLoader,
    TranslateLoader
} from 'ng2-translate';
import {Http} from '@angular/http';

import {BeneficiaryHeaderComponent} from './beneficiary-header.component';
import {BeneficiaryHeader} from '../../../../../store/Beneficiary/types/beneficiary-header.model';
import {DateDisplayFormatPipe} from '../../../../../shared/pipes/DateDisplayFormat/date-display-format.pipe';
import {PhoneFormatPipe} from '../../../../../shared/pipes/PhoneFormat/phone-format.pipe';
import {EnumBeneficiaryHeaderType} from '../../../../../store/Beneficiary/types/beneficiary-header.model';
import {BeneficiaryAddress} from '../../../../../store/Beneficiary/types/beneficiary-address.model';
import {BeneficiaryEmail} from '../../../../../store/Beneficiary/types/beneficiary-email.model';
import {BeneficiaryPhone} from '../../../../../store/Beneficiary/types/beneficiary-phone.model';

// mock any services or models needed here
const BeneficiaryHeaderMocks = {
    getDefaultHeader : new BeneficiaryHeader({
        title          : '',
        headerType     : EnumBeneficiaryHeaderType.BENEFICIARY_MAIN,
        firstName      : 'Grandpa Rick',
        middleName     : '',
        lastName       : 'Sanchez',
        birthDate      : '1936-12-16',
        primaryAddress : new BeneficiaryAddress(),
        primaryEmail   : new BeneficiaryEmail(),
        primaryPhone   : new BeneficiaryPhone()
    })
};

// test definitions here
describe('Component: BeneficiaryHeaderComponent', () => {
    let componentInstance   : BeneficiaryHeaderComponent,
        fixture             : ComponentFixture<BeneficiaryHeaderComponent>,
        beneficiaryMock     : BeneficiaryHeader;

    // use the async() wrapper to allow the Angular template compiler time to read the files
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                BeneficiaryHeaderComponent,     // declare the test component
                // add dependencies defined in the template
                DateDisplayFormatPipe,
                PhoneFormatPipe
            ],
            imports: [
                // import the translation for template pipes
                TranslateModule.forRoot({
                    provide     : TranslateLoader,
                    useFactory  : (http : Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
                    deps    : [Http]
                }),
                ReactiveFormsModule
            ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(BeneficiaryHeaderComponent);
            componentInstance = fixture.componentInstance;  // search box test instance
            beneficiaryMock = BeneficiaryHeaderMocks.getDefaultHeader;
            componentInstance.headerConfig = beneficiaryMock;
        });
    }));

    describe('defaults', () => {
        it('should be defined', () => {
            expect(componentInstance).toBeDefined();
        });
    });

    it('should emit view profile event when clicked', () => {
        // arrange the test using spyOn(); would otherwise use subscribe if we needed to inspect the event itself
        spyOn(componentInstance.viewBeneficiaryProfile, 'emit');

        // trigger the form submit
        componentInstance.goViewProfile();

        expect(componentInstance.viewBeneficiaryProfile.emit).toHaveBeenCalled();
    });

});
