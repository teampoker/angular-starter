import {
    Input,
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

import {BeneficiaryPlansState} from '../../../../../store/Beneficiary/types/beneficiary-plans-state.model';

@Component({
    selector        : 'beneficiary-plans-dashboard',
    templateUrl     : './beneficiary-plans-dashboard.component.html',
    styleUrls       : ['./beneficiary-plans-dashboard.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryPlansDashboardComponent: handles display of this section dashboard
 */
export class BeneficiaryPlansDashboardComponent {
    /**
     * BeneficiaryPlansDashboardComponent constructor
     */
    constructor() {}

    /**
     * beneficiary service coverages UI state
     */
    @Input() beneficiaryPlansState : BeneficiaryPlansState;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }
}
