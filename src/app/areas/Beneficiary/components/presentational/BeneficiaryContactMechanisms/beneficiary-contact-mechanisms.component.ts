import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter
} from '@angular/core';
import {
    FormBuilder,
    FormArray,
    FormGroup,
    Validators
} from '@angular/forms';

import {BeneficiaryContactMechanismActions} from '../../../../../store/Beneficiary/actions/beneficiary-contact-mechanisms.actions';
import {BeneficiaryContactMechanismsState} from '../../../../../store/Beneficiary/types/beneficiary-contact-mechanisms-state.model';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';

@Component({
    selector        : 'beneficiary-contact-mechanisms',
    templateUrl     : './beneficiary-contact-mechanisms.component.html',
    styleUrls       : ['./beneficiary-contact-mechanisms.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryContactMechanismsComponent: handles display of beneficiary status cards
 */
export class BeneficiaryContactMechanismsComponent {
    /**
     * BeneficiaryContactMechanismsComponent constructor
     * @param builder
     * @param beneficiaryContactMechanismActions
     */
    constructor(
        private builder                             : FormBuilder,
        private beneficiaryContactMechanismActions  : BeneficiaryContactMechanismActions
    ) {
        // init form input fields
        this.addresses = new FormArray([this.createAddressFormGroup()]);
        this.emails    = new FormArray([this.createEmailFormGroup()]);
        this.phones    = new FormArray([this.createPhoneFormGroup()]);

        // build Beneficiary Information FormControl group
        this.beneficiaryContactInfoForm = builder.group({
            addresses : this.addresses,
            emails    : this.emails,
            phones    : this.phones
        });
    }

    /**
     * boolean toggle indicating component is part of create Beneficiary Profile form
     * @type {boolean}
     */
    @Input() isNewProfile : boolean;

    /**
     * beneficiary contact info UI state
     */
    @Input() beneficiaryContactMechanismsState : BeneficiaryContactMechanismsState;

    /**
     * event triggered when the beneficiary contact info form valid flag changes
     * @type {EventEmitter<boolean>}
     */
    @Output() contactInfoFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * keep track of # of addresses beneficiary has saved here
     * @type {number}
     */
    private addressCount : number = 0;

    /**
     * keep track of # of emails beneficiary has saved here
     * @type {number}
     */
    private emailCount : number = 0;

    /**
     * keep track of # of phones beneficiary has saved here
     * @type {number}
     */
    private phoneCount : number = 0;

    /**
     * update form valid state
     */
    private updateContactInfoFormValid() {
        // is part of create new profile form
        if (this.isNewProfile) {
            const valid = this.validAddressForm && this.validEmailForm && this.validPhoneForm;

            this.contactInfoFormValid.emit(valid);
        }
    }

    /**
     * dynamically creates addresses form array for editing
     */
    private buildAddressFormControls(value : number) {
        // grab reference to array of form addresses controls
        const addressControls : FormArray = this.beneficiaryContactInfoForm.get('addresses') as FormArray;

        // build new Form Array
        // tslint:disable-next-line: prefer-const
        for (let i = 0, len = value; i < len; i++) {
            // clear existing FormGroup item
            addressControls.removeAt(i);

            // push new FormGroup item
            addressControls.push(this.createAddressFormGroup());
        }
    }

    /**
     * dynamically creates emails form array for editing
     */
    private buildEmailFormControls(value : number) {
        // grab reference to array of form emails controls
        const createEmailFormGroup : FormArray = this.beneficiaryContactInfoForm.get('emails') as FormArray;

        // build new Form Array
        // tslint:disable-next-line: prefer-const
        for (let i = 0, len = value; i < len; i++) {
            // clear existing FormGroup item
            createEmailFormGroup.removeAt(i);

            // push new FormGroup item
            createEmailFormGroup.push(this.createEmailFormGroup());
        }
    }

    /**
     * dynamically creates phones form array for editing
     */
    private buildPhoneFormControls(value : number) {
        // grab reference to array of form phones controls
        const phoneControls : FormArray = this.beneficiaryContactInfoForm.get('phones') as FormArray;

        // build new Form Array
        // tslint:disable-next-line: prefer-const
        for (let i = 0, len = value; i < len; i++) {
            // clear existing FormGroup item
            phoneControls.removeAt(i);

            // push new FormGroup item
            phoneControls.push(this.createPhoneFormGroup());
        }
    }

    /**
     * Object contains form group
     */
    beneficiaryContactInfoForm : FormGroup;

    /**
     * addresses
     */
    addresses : FormArray;

    /**
     * emails
     */
    emails : FormArray;

    /**
     * phones
     */
    phones : FormArray;

    /**
     * address form valid state
     */
    validAddressForm : boolean;

    /**
     * email form valid state
     */
    validEmailForm : boolean;

    /**
     * phone form valid state
     */
    validPhoneForm : boolean;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for address form valid updates
     * @param formState valid/invalid state of form
     * @param addressIndex index of the address form that triggered the event
     */
    onAddressFormValid(formState : boolean, addressIndex : number) {
        this.validAddressForm = formState;

        this.updateContactInfoFormValid();
    }

    /**
     * event handler for email form valid updates
     * @param formState valid/invalid state of form
     * @param emailIndex index of the email form that triggered the event
     */
    onEmailFormValid(formState : boolean, emailIndex : number) {
        this.validEmailForm = formState;

        this.updateContactInfoFormValid();
    }

    /**
     * event handler for phone form valid updates
     * @param formState valid/invalid state of form
     * @param phoneIndex index of the phone form that triggered the event
     */
    onPhoneFormValid(formState : boolean, phoneIndex : number) {
        this.validPhoneForm = formState;

        this.updateContactInfoFormValid();
    }

    /**
     * event handler for update beneficiary address record event
     * @param update new values
     * @param updateIndex index of address the update needs to be applied to
     */
    onUpdateAddressField(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch address field update Action
        this.beneficiaryContactMechanismActions.updateAddressField(update);
    }

    /**
     * event handler for update beneficiary address type selection event
     * @param update new values
     * @param updateIndex index of address the update needs to be applied to
     */
    onUpdateAddressType(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch address type update Action
        this.beneficiaryContactMechanismActions.updateAddressType(update);
    }

    /**
     * event handler for update beneficiary address notification type selection event
     * @param update new values
     * @param updateIndex index of address the update needs to be applied to
     */
    onUpdateAddressNotificationType(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch address notification type update Action
        this.beneficiaryContactMechanismActions.updateAddressNotificationType(update);
    }

    /**
     * event handler for update beneficiary email record event
     * @param update new values
     * @param updateIndex index of email the update needs to be applied to
     */
    onUpdateEmailField(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch address field update Action
        this.beneficiaryContactMechanismActions.updateEmailField(update);
    }

    /**
     * event handler for update beneficiary email type selection event
     * @param update new values
     * @param updateIndex index of email the update needs to be applied to
     */
    onUpdateEmailType(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch email type update Action
        this.beneficiaryContactMechanismActions.updateEmailType(update);
    }

    /**
     * event handler for update beneficiary email notification type selection event
     * @param update new values
     * @param updateIndex index of email the update needs to be applied to
     */
    onUpdateEmailNotificationType(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch email notification type update Action
        this.beneficiaryContactMechanismActions.updateEmailNotificationType(update);
    }

    /**
     * event handler for update beneficiary phone record event
     * @param update new values
     * @param updateIndex index of phone the update needs to be applied to
     */
    onUpdatePhoneField(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch address field update Action
        this.beneficiaryContactMechanismActions.updatePhoneField(update);
    }

    /**
     * event handler for update beneficiary phone type selection event
     * @param update new values
     * @param updateIndex index of phone the update needs to be applied to
     */
    onUpdatePhoneType(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch phone type update Action
        this.beneficiaryContactMechanismActions.updatePhoneType(update);
    }

    /**
     * event handler for update beneficiary phone notification type selection event
     * @param update new values
     * @param updateIndex index of phone the update needs to be applied to
     */
    onUpdatePhoneNotificationType(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch phone notification type update Action
        this.beneficiaryContactMechanismActions.updatePhoneNotificationType(update);
    }

    /**
     * event handler for keyboard key actions
     * @param event DOM event
     */
    editKeyPressHandler(event : { key : string }) {
         if (event.key === 'Enter') {
             this.editBeneficiaryContactInformation();
         }
    }

    /**
     * form group builder used to add dynamically added address fields
     */
    createAddressFormGroup() {
        return this.builder.group({
            street               : ['', Validators.required],
            apt                  : [''],
            addressNotifications : [''],
            addressType          : ['', Validators.required],
            geographicDetails    : ['']
        });
    }

    /**
     * form group builder used to add dynamically added email fields
     */
    createEmailFormGroup() {
        return this.builder.group({
            emailType             : [''],
            email                 : [''],
            emailNotifications    : ['', Validators.required]
        });
    }

    /**
     * form group builder used to add dynamically added phone fields
     */
    createPhoneFormGroup() {
        return this.builder.group({
            phoneType             : ['', Validators.required],
            phone                 : ['', Validators.required],
            phoneNotifications    : ['', Validators.required]
        });
    }

    /**
     * event handler for add address
     */
    addAddress() {
        // grab reference to array of form address controls
        const addressControls : FormArray = this.beneficiaryContactInfoForm.get('addresses') as FormArray;

        // add new address record
        this.beneficiaryContactMechanismActions.addAdditionalBeneficiaryAddress();

        // add new FormControl
        addressControls.push(this.createAddressFormGroup());
    }

    /**
     * event handler for remove address
     * @param addressIndex index of address in addresses collection
     */
    removeAddress(addressIndex : number) {
        // grab reference to array of form address controls
        const addressControls : FormArray = this.beneficiaryContactInfoForm.get('addresses') as FormArray;

        // remove address record
        this.beneficiaryContactMechanismActions.removeAdditionalBeneficiaryAddress(addressIndex);

        // remove FormGroup from array
        addressControls.removeAt(addressIndex);
    }

    /**
     * event handler for add emails
     */
    addEmail() {
        // grab reference to array of form address controls
        const emailControls : FormArray = this.beneficiaryContactInfoForm.get('emails') as FormArray;

        // add new address record
        this.beneficiaryContactMechanismActions.addAdditionalBeneficiaryEmail();

        // add new FormControl
        emailControls.push(this.createEmailFormGroup());
    }

    /**
     * event handler for remove email
     * @param emailIndex index of email in emails collection
     */
    removeEmail(emailIndex : number) {
        // grab reference to array of form emails controls
        const emailControls : FormArray = this.beneficiaryContactInfoForm.get('emails') as FormArray;

        // remove address record
        this.beneficiaryContactMechanismActions.removeAdditionalBeneficiaryEmail(emailIndex);

        // remove FormGroup from array
        emailControls.removeAt(emailIndex);
    }

    /**
     * event handler for add phones
     */
    addPhone() {
        // grab reference to array of form address controls
        const phoneControls : FormArray = this.beneficiaryContactInfoForm.get('phones') as FormArray;

        // add new address record
        this.beneficiaryContactMechanismActions.addAdditionalBeneficiaryPhone();

        // add new FormControl
        phoneControls.push(this.createPhoneFormGroup());
    }

    /**
     * event handler for remove phone
     * @param phoneIndex index of phone in phones collection
     */
    removePhone(phoneIndex : number) {
        // grab reference to array of form address controls
        const phoneControls : FormArray = this.beneficiaryContactInfoForm.get('phones') as FormArray;

        // remove address record
        this.beneficiaryContactMechanismActions.removeAdditionalBeneficiaryPhone(phoneIndex);

        // remove FormGroup from array
        phoneControls.removeAt(phoneIndex);
    }

    /**
     * event handler for toggling editing beneficiary information
     */
    editBeneficiaryContactInformation() {
        // toggle editing state
        this.beneficiaryContactMechanismActions.updateContactInfoEditing(true);
    }

    /**
     * handler used to reverse any changes made in this component
     */
    cancelUpdate() {
        const addressControls : FormArray = this.beneficiaryContactInfoForm.get('addresses') as FormArray,
              emailControls   : FormArray = this.beneficiaryContactInfoForm.get('emails') as FormArray,
              phoneControls   : FormArray = this.beneficiaryContactInfoForm.get('phones') as FormArray;

        // toggle editing state
        this.beneficiaryContactMechanismActions.updateContactInfoEditing(false);

        // remove any form stuff that was being used temporarily
        if (this.addressCount < addressControls.length) {
            // remove extra form elements
            // tslint:disable-next-line: prefer-const
            for (let i = 0, len = addressControls.length; i < len; i++) {
                if (i > this.addressCount - i) {
                    addressControls.removeAt(this.addressCount);
                }
            }
        }

        if (this.emailCount < emailControls.length) {
            // remove extra form elements
            // tslint:disable-next-line: prefer-const
            for (let i = 0, len = emailControls.length; i < len; i++) {
                if (i > this.emailCount - i) {
                    emailControls.removeAt(this.emailCount);
                }
            }
        }

        if (this.phoneCount < phoneControls.length) {
            // remove extra form elements
            // tslint:disable-next-line: prefer-const
            for (let i = 0, len = phoneControls.length; i < len; i++) {
                if (i > this.phoneCount - i) {
                    phoneControls.removeAt(this.phoneCount);
                }
            }
        }

        // create a FormGroup instance to map to each of the beneficiary's addresses
        this.buildAddressFormControls(this.addressCount);

        // create a FormGroup instance to map to each of the beneficiary's emails
        this.buildEmailFormControls(this.emailCount);

        // create a FormGroup instance to map to each of the beneficiary's phones
        this.buildPhoneFormControls(this.phoneCount);
    }

    /**
     * handler for form save button click
     */
    onSubmit() {
        // store current # of addresses
        this.addressCount = this.beneficiaryContactMechanismsState.getIn(['contactMechanismsState', 'addresses']).size;

        // create a FormGroup instance to map to each of the beneficiary's addresses
        this.buildAddressFormControls(this.addressCount);

        // update current # of emails
        this.emailCount = this.beneficiaryContactMechanismsState.getIn(['contactMechanismsState', 'emails']).size;

        // create a FormGroup instance to map to each of the beneficiary's emails
        this.buildEmailFormControls(this.emailCount);

        // update current # of phones
        this.phoneCount = this.beneficiaryContactMechanismsState.getIn(['contactMechanismsState', 'phones']).size;

        // create a FormGroup instance to map to each of the beneficiary's phones
        this.buildPhoneFormControls(this.phoneCount);

        // toggle editing state
        this.beneficiaryContactMechanismActions.saveBeneficiaryContactMechanisms();
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // store current # of addresses
        this.addressCount = this.beneficiaryContactMechanismsState.getIn(['contactMechanismsState', 'addresses']).size;

        // create a FormGroup instance to map to each of the beneficiary's addresses
        this.buildAddressFormControls(this.addressCount);

        // update current # of emails
        this.emailCount = this.beneficiaryContactMechanismsState.getIn(['contactMechanismsState', 'emails']).size;

        // create a FormGroup instance to map to each of the beneficiary's emails
        this.buildEmailFormControls(this.emailCount);

        // update current # of phones
        this.phoneCount = this.beneficiaryContactMechanismsState.getIn(['contactMechanismsState', 'phones']).size;

        // create a FormGroup instance to map to each of the beneficiary's phones
        this.buildPhoneFormControls(this.phoneCount);
    }
}
