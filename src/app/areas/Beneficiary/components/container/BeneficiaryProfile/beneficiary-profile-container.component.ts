import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

import {NavActions} from '../../../../../store/Navigation/nav.actions';
import {BeneficiaryStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary.selectors';
import {BeneficiaryInfoStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-info.selectors';
import {BeneficiaryContactMechanismsStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-contact-mechanisms.selectors';
import {BeneficiarySpecialRequirementsSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-special-requirements.selectors';
import {BeneficiaryConnectionsStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-connections.selectors';
import {BeneficiaryMedicalConditionsStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-medical-conditions.selectors';
import {BeneficiaryPlansStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-plans.selectors';
import {BeneficiaryActions} from '../../../../../store/Beneficiary/actions/beneficiary.actions';
import {EnumBeneficiaryHeaderType, BeneficiaryHeader} from '../../../../../store/Beneficiary/types/beneficiary-header.model';
import {BeneficiaryInfoState} from '../../../../../store/Beneficiary/types/beneficiary-info-state.model';
import {BeneficiarySpecialRequirementsState} from '../../../../../store/Beneficiary/types/beneficiary-special-requirements-state.model';
import {BeneficiaryContactMechanismsState} from '../../../../../store/Beneficiary/types/beneficiary-contact-mechanisms-state.model';
import {BeneficiaryPlansState} from '../../../../../store/Beneficiary/types/beneficiary-plans-state.model';
import {BeneficiaryConnectionsState} from '../../../../../store/Beneficiary/types/beneficiary-connections-state.model';
import {BeneficiaryMedicalConditionsState} from '../../../../../store/Beneficiary/types/beneficiary-medical-conditions-state.model';

@Component({
    selector        : 'beneficiary-profile',
    templateUrl     : './beneficiary-profile-container.component.html',
    styleUrls       : ['./beneficiary-profile-container.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryProfileContainerComponent: handles display of beneficiary profile view
 */
export class BeneficiaryProfileContainerComponent {
    /**
     * BeneficiaryProfileContainerComponent constructor
     * @param router
     * @param cd
     * @param navActions
     * @param beneficiarySelectors
     * @param beneficiaryInfoSelectors
     * @param beneficiaryContactMechanismsStateSelectors
     * @param beneficiarySpecialRequirementsSelectors
     * @param beneficiaryConnectionsStateSelectors
     * @param beneficiaryMedicalConditionsStateSelectors
     * @param beneficiaryPlansStateSelectors
     * @param beneficiaryActions
     * @param route
     */
    constructor(
        private router                                      : Router,
        private cd                                          : ChangeDetectorRef,
        private navActions                                  : NavActions,
        private beneficiarySelectors                        : BeneficiaryStateSelectors,
        private beneficiaryInfoSelectors                    : BeneficiaryInfoStateSelectors,
        private beneficiaryContactMechanismsStateSelectors  : BeneficiaryContactMechanismsStateSelectors,
        private beneficiarySpecialRequirementsSelectors     : BeneficiarySpecialRequirementsSelectors,
        private beneficiaryConnectionsStateSelectors        : BeneficiaryConnectionsStateSelectors,
        private beneficiaryMedicalConditionsStateSelectors  : BeneficiaryMedicalConditionsStateSelectors,
        private beneficiaryPlansStateSelectors              : BeneficiaryPlansStateSelectors,
        private beneficiaryActions                          : BeneficiaryActions,
        private route                                       : ActivatedRoute
    ) {
        // subscribe to aggregated Redux state changes globally that we're interested in
        this.stateSubscription = Observable.combineLatest(
            this.beneficiarySelectors.beneficiaryHeader(EnumBeneficiaryHeaderType.BENEFICIARY_PROFILE),
            this.beneficiarySelectors.newProfileActive(),
            this.beneficiaryInfoSelectors.beneficiaryInfoState(),
            this.beneficiarySpecialRequirementsSelectors.beneficiarySpecialRequirementsState(),
            this.beneficiaryContactMechanismsStateSelectors.beneficiaryContactMechanismsState(),
            this.beneficiaryMedicalConditionsStateSelectors.beneficiaryMedicalConditionsState(),
            this.beneficiaryConnectionsStateSelectors.beneficiaryConnectionsState(),
            this.beneficiaryPlansStateSelectors.beneficiaryPlansState()
        )
        .subscribe(val => {
            // update local state
            this.headerConfig                           = val[0];
            this.isNewProfileActive                     = val[1];
            this.beneficiaryInfoState                   = val[2];
            this.beneficiarySpecialRequirementsState    = val[3];
            this.beneficiaryContactMechanismsState      = val[4];
            this.medicalConditionsState                 = val[5];
            this.beneficiaryConnectionState             = val[6];
            this.beneficiaryPlansState                  = val[7];

            // trigger change detection
            this.cd.markForCheck();
        });

        this.beneficiaryActions.getBeneficiary(this.route.snapshot.params['uuid'], false);
    }

    /**
     * Redux state subscription
     */
    private stateSubscription : Subscription;

    /**
     * flag indicating if Create Profile route is active
     */
    isNewProfileActive : boolean;

    /**
     * beneficiary info UI state
     */
    beneficiaryInfoState : BeneficiaryInfoState;

    /**
     * beneficiary special requirements UI state
     */
    beneficiarySpecialRequirementsState : BeneficiarySpecialRequirementsState;

    /**
     * beneficiary contact info UI state
     */
    beneficiaryContactMechanismsState : BeneficiaryContactMechanismsState;

    /**
     * beneficiary plans UI state
     */
    beneficiaryPlansState : BeneficiaryPlansState;

    /**
     * beneficiary connections UI state
     */
    beneficiaryConnectionState : BeneficiaryConnectionsState;

    /**
     * beneficiary medical conditions UI state
     */
    medicalConditionsState : BeneficiaryMedicalConditionsState;

    /**
     * Beneficiary Header configuration
     */
    headerConfig : BeneficiaryHeader;

    /**
     * navigate user to Add A Beneficiary Profile
     */
    onViewBeneficiaryProfile() {
        this.router.navigate([`/Beneficiary/${this.route.snapshot.params['uuid']}/Profile`]);
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        // unsubscribe
        this.stateSubscription.unsubscribe();

        // clear any open modals
        this.navActions.toggleIsPoppedNavState(false);
    }
}
