import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

import {NavActions} from '../../../../../store/Navigation/nav.actions';
import {BeneficiaryActions} from '../../../../../store/Beneficiary/actions/beneficiary.actions';
import {BeneficiaryStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary.selectors';
import {BeneficiaryInfoStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-info.selectors';
import {BeneficiaryContactMechanismsStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-contact-mechanisms.selectors';
import {BeneficiarySpecialRequirementsSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-special-requirements.selectors';
import {BeneficiaryConnectionsStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-connections.selectors';
import {BeneficiaryMedicalConditionsStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-medical-conditions.selectors';
import {BeneficiaryPlansStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-plans.selectors';
import {NavStateSelectors} from '../../../../../store/Navigation/nav.selectors';
import {EnumBeneficiaryHeaderType, BeneficiaryHeader} from '../../../../../store/Beneficiary/types/beneficiary-header.model';
import {BeneficiaryInfoState} from '../../../../../store/Beneficiary/types/beneficiary-info-state.model';
import {BeneficiarySpecialRequirementsState} from '../../../../../store/Beneficiary/types/beneficiary-special-requirements-state.model';
import {BeneficiaryContactMechanismsState} from '../../../../../store/Beneficiary/types/beneficiary-contact-mechanisms-state.model';
import {BeneficiaryPlansState} from '../../../../../store/Beneficiary/types/beneficiary-plans-state.model';
import {BeneficiaryConnectionsState} from '../../../../../store/Beneficiary/types/beneficiary-connections-state.model';
import {BeneficiaryMedicalConditionsState} from '../../../../../store/Beneficiary/types/beneficiary-medical-conditions-state.model';
import {Beneficiary} from '../../../../../store/Beneficiary/types/beneficiary.model';
import {AlertItem} from '../../../../../store/Navigation/types/alert-item.model';

@Component({
    selector        : 'create-new-profile-container',
    templateUrl     : './create-new-profile-container.component.html',
    styleUrls       : ['./create-new-profile-container.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for CreateNewProfileContainerComponent: handles display of beneficiary status cards
 */
export class CreateNewProfileContainerComponent {
    /**
     * CreateNewProfileContainerComponent constructor
     * @param cd
     * @param navActions
     * @param beneficiaryActions
     * @param beneficiarySelectors
     * @param beneficiaryInfoSelectors
     * @param beneficiaryContactMechanismsStateSelectors
     * @param beneficiarySpecialRequirementsSelectors
     * @param beneficiaryConnectionsStateSelectors
     * @param beneficiaryMedicalConditionsStateSelectors
     * @param beneficiaryPlansStateSelectors
     * @param navStateSelectors
     */
    constructor(
        private cd                                          : ChangeDetectorRef,
        private navActions                                  : NavActions,
        private beneficiaryActions                          : BeneficiaryActions,
        private beneficiarySelectors                        : BeneficiaryStateSelectors,
        private beneficiaryInfoSelectors                    : BeneficiaryInfoStateSelectors,
        private beneficiaryContactMechanismsStateSelectors  : BeneficiaryContactMechanismsStateSelectors,
        private beneficiarySpecialRequirementsSelectors     : BeneficiarySpecialRequirementsSelectors,
        private beneficiaryConnectionsStateSelectors        : BeneficiaryConnectionsStateSelectors,
        private beneficiaryMedicalConditionsStateSelectors  : BeneficiaryMedicalConditionsStateSelectors,
        private beneficiaryPlansStateSelectors              : BeneficiaryPlansStateSelectors,
        private navStateSelectors                           : NavStateSelectors

    ) {
        // subscribe to aggregated Redux state changes globally that we're interested in
        this.stateSubscription = Observable.combineLatest(
            this.beneficiarySelectors.beneficiaryHeader(EnumBeneficiaryHeaderType.CREATE_NEW_PROFILE),
            this.beneficiarySelectors.newProfileActive(),
            this.beneficiaryInfoSelectors.beneficiaryInfoState(),
            this.beneficiarySpecialRequirementsSelectors.beneficiarySpecialRequirementsState(),
            this.beneficiaryContactMechanismsStateSelectors.beneficiaryContactMechanismsState(),
            this.beneficiaryMedicalConditionsStateSelectors.beneficiaryMedicalConditionsState(),
            this.beneficiaryConnectionsStateSelectors.beneficiaryConnectionsState(),
            this.beneficiaryPlansStateSelectors.beneficiaryPlansState(),
            this.navStateSelectors.alertMessage(),
            this.beneficiarySelectors.beneficiary(),
            this.beneficiarySelectors.isFetching
        )
        .subscribe(val => {
            // update local state
            this.headerConfig                           = val[0];
            this.isNewProfileActive                     = val[1];
            this.beneficiaryInfoState                   = val[2];
            this.beneficiarySpecialRequirementsState    = val[3];
            this.beneficiaryContactMechanismsState      = val[4];
            this.medicalConditionsState                 = val[5];
            this.beneficiaryConnectionState             = val[6];
            this.beneficiaryPlansState                  = val[7];
            this.alertMessage                           = val[8];
            this.beneficiaryDomainModel                 = val[9];
            this.isBusy                                 = val[10];

            // trigger change detection
            this.cd.markForCheck();
        });
    }

    /**
     * Redux state subscription
     */
    private stateSubscription : Subscription;

    /**
     * flag indicating if Create Profile route is active
     */
    isNewProfileActive : boolean;

    /**
     * beneficiary info UI state
     */
    beneficiaryInfoState : BeneficiaryInfoState;

    /**
     * beneficiary special requirements UI state
     */
    beneficiarySpecialRequirementsState : BeneficiarySpecialRequirementsState;

    /**
     * beneficiary contact info UI state
     */
    beneficiaryContactMechanismsState : BeneficiaryContactMechanismsState;

    /**
     * beneficiary plans UI state
     */
    beneficiaryPlansState : BeneficiaryPlansState;

    /**
     * beneficiary connections UI state
     */
    beneficiaryConnectionState : BeneficiaryConnectionsState;

    /**
     * beneficiary medical conditions UI state
     */
    medicalConditionsState : BeneficiaryMedicalConditionsState;

    /**
     * Beneficiary Header configuration
     */
    headerConfig : BeneficiaryHeader;

    /**
     * Beneficiary DomainModel
     */
    beneficiaryDomainModel : Beneficiary;

    /**
     * grouping object for beneficiaryInformation form
     */
    validInformationForm : boolean;

    /**
     * grouping object for beneficiaryContactInformation form
     */
    validContactForm : boolean;

    /**
     * grouping object for beneficiaryConnectionsPanel form
     */
    validConnectionsForm : boolean;

    /**
     * grouping object for beneficiaryPlans form
     */
    validPlanForm : boolean;

    /**
     * alert message state
     */
    alertMessage : AlertItem;

    /**
     * indicator of any background process actively doing something
     */
    isBusy : boolean;

    /**
     * update the contact forms valid state
     */
    onContactInfoFormValid(formState : boolean) {
        this.validContactForm = formState;
    }

    /**
     * update the information forms valid state
     */
    onBeneficiaryInfoFormValid(formState : boolean) {
        this.validInformationForm = formState;
    }

    /**
     * update the connections forms valid state
     */
    onConnectionsFormValid(formState : boolean) {
        this.validConnectionsForm = formState;
    }

    /**
     * update the plan forms valid state
     */
    onPlansFormValid(formState : boolean) {
        this.validPlanForm = formState;
    }

    /**
     * clear the form of all entered values
     */
    clearBeneficiaryForm() {
        this.beneficiaryActions.initNewBeneficiaryProfile();
    }

    /**
     * handler for form submit button. Triggers login call to API
     */
    onSubmit() {
        // create new beneficiary profile
        this.beneficiaryActions.createNewBeneficiary();
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        // unsubscribe
        this.stateSubscription.unsubscribe();
    }
}
