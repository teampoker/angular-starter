import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {List} from 'immutable';

import {BeneficiaryStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary.selectors';
import {BeneficiarySpecialRequirementsSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-special-requirements.selectors';
import {BeneficiaryPlansStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-plans.selectors';
import {BeneficiaryActions} from '../../../../../store/Beneficiary/actions/beneficiary.actions';
import {EnumBeneficiaryHeaderType, BeneficiaryHeader} from '../../../../../store/Beneficiary/types/beneficiary-header.model';
import {BeneficiarySpecialRequirementsState} from '../../../../../store/Beneficiary/types/beneficiary-special-requirements-state.model';
import {BeneficiaryPlansState} from '../../../../../store/Beneficiary/types/beneficiary-plans-state.model';
import {FormType} from '../../../../../store/Forms/types/form-type.model';
import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {MetaDataTypesSelectors} from '../../../../../store/MetaDataTypes/meta-data-types.selectors';
import {FormsStateSelectors} from '../../../../../store/Forms/forms.selectors';
import {FormStateDetail} from '../../../../../store/Forms/types/form-state-detail.model';
import {ReservationCardsActions} from '../../../../../store/Reservation/actions/reservation-cards.actions';
import {ReservationHistorySelectors} from '../../../../../store/Reservation/selectors/reservation-history.selectors';
import {ReservationStateSelectors} from '../../../../../store/Reservation/selectors/reservation.selectors';
import {Reservation} from '../../../../../store/Reservation/types/reservation.model';
import {ReservationCardState} from '../../../../../store/Reservation/types/reservation-card-state.model';
import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';

@Component({
    selector        : 'beneficiary-body-container',
    templateUrl     : './beneficiary-body-container.component.html',
    styleUrls       : ['./beneficiary-body-container.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryBodyContainerComponent: handles display of beneficiary landing page
 */
export class BeneficiaryBodyContainerComponent {
    /**
     * BeneficiaryBodyContainerComponent constructor
     * @param router
     * @param cd
     * @param beneficiarySelectors
     * @param beneficiarySpecialRequirementsSelectors
     * @param beneficiaryPlansStateSelectors
     * @param beneficiaryActions
     * @param route
     * @param formsStateSelectors
     * @param metaDataTypesSelectors
     * @param reservationHistorySelectors
     * @param reservationCardsActions
     * @param reservationStateSelectors
     * @param reservationActions
     */
    constructor(
        private router                                  : Router,
        private cd                                      : ChangeDetectorRef,
        private beneficiarySelectors                    : BeneficiaryStateSelectors,
        private beneficiarySpecialRequirementsSelectors : BeneficiarySpecialRequirementsSelectors,
        private beneficiaryPlansStateSelectors          : BeneficiaryPlansStateSelectors,
        private beneficiaryActions                      : BeneficiaryActions,
        private route                                   : ActivatedRoute,
        private formsStateSelectors                     : FormsStateSelectors,
        private metaDataTypesSelectors                  : MetaDataTypesSelectors,
        private reservationHistorySelectors             : ReservationHistorySelectors,
        private reservationCardsActions                 : ReservationCardsActions,
        private reservationStateSelectors               : ReservationStateSelectors,
        private reservationActions                      : ReservationActions
    ) {
        this.beneficiaryUuid = this.route.snapshot.params['uuid'];

        // setup subscription to beneficiary state
        this.stateSubscription = Observable.combineLatest(
            this.beneficiaryPlansStateSelectors.beneficiaryPlansState(),
            this.beneficiarySelectors.beneficiaryHeader(EnumBeneficiaryHeaderType.BENEFICIARY_MAIN),
            this.beneficiarySelectors.newProfileActive(),
            this.beneficiarySpecialRequirementsSelectors.beneficiarySpecialRequirementsState(),
            this.formsStateSelectors.beneficiaryForms(),
            this.formsStateSelectors.beneficiaryFormTypes(),
            this.formsStateSelectors.beneficiaryFormAuthReasons(),
            this.metaDataTypesSelectors.phoneTypes(),
            this.formsStateSelectors.addBeneficiaryForm(),
            this.formsStateSelectors.isBeneficiaryFormValid(),
            this.reservationStateSelectors.getHistoryCardState(),
            this.reservationStateSelectors.getScheduledCardState(),
            this.reservationHistorySelectors.getReservations(this.beneficiaryUuid),
            this.reservationHistorySelectors.getScheduledRides(this.beneficiaryUuid)
        )
        .subscribe(val => {
            // update local state
            this.plansState                     = val[0];
            this.headerConfig                   = val[1];
            this.isNewProfileActive             = val[2];
            this.specialRequirementsState       = val[3];
            this.beneficiaryForms               = val[4];
            this.beneficiaryFormTypes           = val[5];
            this.beneficiaryFormAuthReasons     = val[6];
            this.phoneTypes                     = val[7];
            this.addBeneficiaryForm             = val[8];
            this.isBeneficiaryFormValid         = val[9];
            this.historyCardState               = val[10];
            this.scheduledCardState             = val[11];
            this.historyReservations            = val[12];
            this.scheduledReservations          = val[13];

            // trigger change detection
            this.cd.markForCheck();
        });

        this.beneficiaryActions.getBeneficiary(this.beneficiaryUuid, false);
        this.reservationActions.getReservations(this.beneficiaryUuid);
    }

    /**
     * Redux state subscription
     */
    private stateSubscription : Subscription;

    /**
     * the currently selected beneficiary's uuid
     * @type {string}
     */
    beneficiaryUuid : string;

    /**
     * reservation history card state
     * @type {ReservationCardState}
     */
    historyCardState : ReservationCardState;

    /**
     * all past reservations for the beneficiary
     * @type {List<Reservation>
     */
    historyReservations : List<Reservation>;

    /**
     * scheduled card state
     * @type {ReservationCardState}
     */
    scheduledCardState : ReservationCardState;

    /**
     * the beneficiary's scheduled rides
     * @type {List<Reservation>}
     */
    scheduledReservations : List<Reservation>;

    /**
     * flag indicating if Create Profile route is active
     */
    isNewProfileActive : boolean;

    /**
     * overall validation state of beneficiary form
     */
    isBeneficiaryFormValid : boolean;

    /**
     * beneficiary special requirements UI state
     */
    specialRequirementsState : BeneficiarySpecialRequirementsState;

    /**
     * Beneficiary Header configuration
     */
    headerConfig : BeneficiaryHeader;

    /**
     * Beneficiary Service Coverages state
     */
    plansState : BeneficiaryPlansState;

    /**
     * Beneficiary form state
     */
    beneficiaryForms : List<FormStateDetail>;

    /**
     * Beneficiary form types
     */
    beneficiaryFormTypes : List<FormType>;

    /**
     * placeholder in state for a form that is being added
     */
    addBeneficiaryForm : FormStateDetail;

    /**
     * Beneficiary form auth reasons
     */
    beneficiaryFormAuthReasons : List<KeyValuePair>;

    /**
     * possible phone types available
     */
    phoneTypes : List<KeyValuePair>;

    /**
     * Closes details for a reservation in the card widget
     */
    onHistoryCloseDetails() {
        this.reservationCardsActions.closeHistoryItem();
    }

    /**
     * Shows details for a reservation in the card widget
     * @param uuid
     */
    onHistoryShowDetails(uuid : string) {
        this.reservationCardsActions.selectHistoryItem(uuid);
    }

    /**
     * Toggles leg details for a reservation in the card widget
     * @param index
     */
    onHistoryToggleLeg(index : number) {
        this.reservationCardsActions.toggleHistoryLeg(index);
    }

    /**
     * Closes details for a scheduled reservation in the card widget
     */
    onScheduledCloseDetails() {
        this.reservationCardsActions.closeScheduledItem();
    }

    /**
     * Shows details for a scheduled reservation in the card widget
     * @param uuid
     */
    onScheduledShowDetails(uuid : string) {
        this.reservationCardsActions.selectScheduledItem(uuid);
    }

    /**
     * Toggles leg details for a scheduled reservation in the card widget
     * @param index
     */
    onScheduledToggleLeg(index : number) {
        this.reservationCardsActions.toggleScheduledLeg(index);
    }

    /**
     * navigate user to Add A Beneficiary Profile
     */
    onViewBeneficiaryProfile() {
        // route user to Beneficiary Profile view
        this.router.navigate([`/Beneficiary/${this.beneficiaryUuid}/Profile`]);
    }

    /**
     * navigate user to edit a reservation
     * @param {string} reservationUuid
     */
    onViewEditReservation(reservationUuid : string) {
        // route user
        this.router.navigate([`/Reservation/${this.beneficiaryUuid}/EditReservation/${reservationUuid}`]);

        // make sure our cards are at a default state
        this.onHistoryCloseDetails();
        this.onScheduledCloseDetails();
    }

    /**
     * navigate user to Make A Reservation area
     */
    onViewReservation() {
        // route user to Reservation view
        this.router.navigate([`/Reservation/${this.beneficiaryUuid}/CreateNewReservation`]);
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        // unsubscribe
        this.stateSubscription.unsubscribe();
    }
}
