import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {BeneficiaryEntryComponent} from './beneficiary-entry.component';
import {CreateNewProfileContainerComponent} from './components/container/CreateNewProfile/create-new-profile-container.component';
import {BeneficiaryBodyContainerComponent} from './components/container/BeneficiaryBody/beneficiary-body-container.component';
import {BeneficiaryProfileContainerComponent} from './components/container/BeneficiaryProfile/beneficiary-profile-container.component';
import {CreateProfileResolveService} from './services/CreateProfileResolve/create-profile-resolve.service';
import {BeneficiaryDashboardResolveService} from './services/BeneficiaryDashboardResolve/beneficiary-dashboard-resolve.service';
import {ViewProfileResolveService} from './services/ViewProfileResolve/view-profile-resolve.service';

@NgModule({
    imports : [
        RouterModule.forChild([
            {
                path        : '',
                component   : BeneficiaryEntryComponent,
                children    : [
                    {
                        path            : 'CreateNewProfile',
                        component       : CreateNewProfileContainerComponent,
                        resolve         : {
                            CreateProfileResolveService
                        }
                    },
                    {
                        path            : ':uuid',
                        component       : BeneficiaryBodyContainerComponent,
                        resolve         : {
                            BeneficiaryDashboardResolveService
                        }
                    },
                    {
                        path            : ':uuid/Profile',
                        component       : BeneficiaryProfileContainerComponent,
                        resolve         : {
                            ViewProfileResolveService
                        }
                    }
                ]
            }
        ])
   ],
    exports : [
        RouterModule
   ],
    providers : [
        BeneficiaryDashboardResolveService,
        CreateProfileResolveService,
        ViewProfileResolveService
    ]
})

export class BeneficiaryRoutingModule {

}
