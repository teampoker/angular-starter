import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

import {NavActions} from '../../store/Navigation/nav.actions';
import {EnumNavOption} from '../../store/Navigation/types/nav-option.model';

@Component({
    templateUrl     : 'beneficiary-entry.component.html',
    styleUrls       : ['beneficiary-entry.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for BeneficiaryEntryComponent: responsible for beneficiary page layout
 */
export class BeneficiaryEntryComponent {
    /**
     * BeneficiaryEntryComponent constructor
     * @param navActions
     */
    constructor(private navActions : NavActions) { }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // set active epic to beneficiary
        this.navActions.updateActiveNavState(EnumNavOption.BENEFICIARY);
    }
}
