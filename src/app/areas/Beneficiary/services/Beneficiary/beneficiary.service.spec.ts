import {
    async,
    inject,
    TestBed
} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {BeneficiaryService} from './beneficiary.service';
import {APIMockService} from '../../../../shared/services/Mock/api-mock.service';
import {BackendService} from '../../../../shared/services/Backend/backend.service';

describe('BeneficiaryService', () => {
    let backendService, beneficiaryService;

    // setup
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports   : [HttpModule],
            providers : [
                APIMockService,
                BackendService,
                BeneficiaryService
            ]
        });
    });

    beforeEach(inject([BackendService, BeneficiaryService], (backend, service) => {
        backendService      = backend;
        beneficiaryService  = service;
    }));

    xit('should get personal beneficiary information', async(() => {
      expect(beneficiaryService).toBeDefined();
      // beneficiaryService.getBeneficiary(' ').subscribe(response => {
      //   expect(response).toEqual(BENEFICIARY_MOCK.getBeneficiary);
      // });
    }));

    it('should return beneficiary to subscriber', inject([BeneficiaryService], service => {
        service.getBeneficiary('123').subscribe(
            response => {
                expect(response).toBeDefined();
            },
            error => {
                fail(error);
            }
        );
    }));

    it('should not throw error when no observer provided', inject([BeneficiaryService], service => {
        const testError : any = {random: 'value'};
        service.handleBeneficiaryError(testError, null);
    }));
});
