import {Injectable} from '@angular/core';
import {Observer} from 'rxjs';
import {Observable} from 'rxjs/Observable';

import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {Beneficiary} from '../../../../store/Beneficiary/types/beneficiary.model';
import {KeyValuePair} from '../../../../store/types/key-value-pair.model';
import {LanguageType} from '../../../../store/MetaDataTypes/types/language-type.model';
import {BENEFICIARY_MOCK} from './beneficiary.service.mock';

@Injectable()
/**
 * Implementation of BeneficiaryService: returns summary data for display on Admin beneficiary
 */
export class BeneficiaryService {
    /**
     * BeneficiaryService constructor
     * @param backendService
     */
    constructor(private backendService : BackendService) {}

    /**
     * cleans up the  Beneficiary domain model and converts to a POJO for saving to API
     * @param beneficiaryModel beneficiary domain model
     * @param isEdit flag indicates if resulting model will be used for a PUT operation
     * @returns {any}
     */
    private scrubBeneficiaryModel(beneficiaryModel : Beneficiary, isEdit : boolean) : any {
        let beneficiaryPOJO : any;

        // convert any KeyValuePair instances to uuid/name pairings
        beneficiaryModel = beneficiaryModel.withMutations(record => record
                .set('gender', record.get('gender') ? KeyValuePair.toApi(record.get('gender')) : record.get('gender'))
                .set('languages', record.get('languages').map(language => language.withMutations(languageRecord => {
                    if (languageRecord.get('type')) {
                        return languageRecord
                            .set('language', LanguageType.toApi(languageRecord.get('language')))
                            .set('type', LanguageType.toApi(languageRecord.get('type')));
                    }
                    else {
                        return languageRecord;
                    }
                })))
                .set('physicalCharacteristics', record.get('physicalCharacteristics').map(physical => physical.withMutations(physicalRecord => physicalRecord
                        .set('type', KeyValuePair.toApi(physicalRecord.get('type'))))))
                .set('specialRequirements', record.get('specialRequirements').map(requirement => requirement.withMutations(requirementRecord => requirementRecord
                        .set('type', KeyValuePair.toApi(requirementRecord.get('type'))))))
                .set('medicalConditions', record.get('medicalConditions').map(condition => condition.withMutations(conditionRecord => conditionRecord
                        .set('type', KeyValuePair.toApi(conditionRecord.get('type'))))))
                .set('contactMechanisms', record.get('contactMechanisms').map(mechanism => mechanism.withMutations(mechanismRecord => mechanismRecord
                            .set('solicitationIndicatorType', KeyValuePair.toApi(mechanismRecord.get('solicitationIndicatorType')))
                            .set('purposeType', KeyValuePair.toApi(mechanismRecord.get('purposeType')))
                            .setIn(['contactMechanism', 'type'], KeyValuePair.toApi(mechanismRecord.getIn(['contactMechanism', 'type']))))))
                .set('connections', record.get('connections').map(connection => connection.withMutations(connectionRecord => connectionRecord
                        .set('contactMechanisms', connectionRecord.get('contactMechanisms').map(mechanism => mechanism.withMutations(mechanismRecord => mechanismRecord
                                    .set('solicitationIndicatorType', KeyValuePair.toApi(mechanismRecord.get('solicitationIndicatorType')))
                                    .set('purposeType', KeyValuePair.toApi(mechanismRecord.get('purposeType')))
                                    .setIn(['contactMechanism', 'type'], KeyValuePair.toApi(mechanismRecord.getIn(['contactMechanism', 'type']))))))
                        .set('types', connectionRecord.get('types').map(connectionType => KeyValuePair.toApi(connectionType))))))
        ) as Beneficiary;

        // convert to POJO
        beneficiaryPOJO = beneficiaryModel.toJS();

        // remove timestamp stuff
        delete beneficiaryPOJO.createdOn;
        delete beneficiaryPOJO.createdBy;
        delete beneficiaryPOJO.updatedOn;
        delete beneficiaryPOJO.updatedBy;

        // clean up gender
        if (beneficiaryPOJO.gender) {
            delete beneficiaryPOJO.gender.name;

            // is there a uuid?
            if (!beneficiaryPOJO.gender.uuid) {
                // don't pass an empty uuid
                delete beneficiaryPOJO.gender.uuid;

                // force gender to undefined
                beneficiaryPOJO.gender = undefined;
            }
        }

        // clean up languages
        if (beneficiaryPOJO.languages.length > 0) {
            beneficiaryPOJO.languages = beneficiaryPOJO.languages.map(language => {
                const returnValue : any = {
                    ordinality  : language.ordinality,
                    language    : {
                        uuid : language.language.uuid
                    }
                };

                if (isEdit) {
                    returnValue.sequenceId  = language.sequenceId;
                    returnValue.version     = language.version;
                }

                return returnValue;
            });
        }

        // clean up medical conditions
        if (beneficiaryPOJO.medicalConditions.length > 0) {
            beneficiaryPOJO.medicalConditions = beneficiaryPOJO.medicalConditions.map(condition => {
                const returnValue : any = {
                    fromDate : condition.fromDate,
                    thruDate : condition.thruDate,
                    type : {
                        uuid : condition.type.uuid
                    }
                };

                if (isEdit) {
                    returnValue.sequenceId  = condition.sequenceId;
                    returnValue.version     = condition.version;
                }

                return returnValue;
            });
        }

        // clean up special requirements
        if (beneficiaryPOJO.specialRequirements.length > 0) {
            beneficiaryPOJO.specialRequirements = beneficiaryPOJO.specialRequirements.map(requirement => {
                const returnValue : any = {
                    fromDate : requirement.fromDate,
                    thruDate : requirement.thruDate,
                    type : {
                        uuid : requirement.type.uuid
                    }
                };

                if (isEdit) {
                    returnValue.sequenceId  = requirement.sequenceId;
                    returnValue.version     = requirement.version;
                }

                return returnValue;
            });
        }

        // clean up physical characteristics
        if (beneficiaryPOJO.physicalCharacteristics.length > 0) {
            beneficiaryPOJO.physicalCharacteristics = beneficiaryPOJO.physicalCharacteristics.map(characteristic => {
                const returnValue : any = {
                    value : characteristic.value,
                    type : {
                        uuid : characteristic.type.uuid
                    }
                };

                if (isEdit) {
                    returnValue.sequenceId  = characteristic.sequenceId;
                    returnValue.version     = characteristic.version;
                }

                return returnValue;
            });
        }

        // clean up connections
        if (beneficiaryPOJO.connections.length > 0) {
            beneficiaryPOJO.connections = beneficiaryPOJO.connections.map(connection => {
                const returnValue : any = {
                    uuid                : connection.uuid,
                    types               : connection.types.map(type => {
                        return {
                            uuid : type.uuid
                        };
                    })
                };

                if (isEdit) {
                    returnValue.version = connection.version;
                }

                return returnValue;
            });
        }

        // clean up contact mechanisms
        if (beneficiaryPOJO.contactMechanisms.length > 0) {
            const tempContactMechanisms = [];

            beneficiaryPOJO.contactMechanisms = beneficiaryPOJO.contactMechanisms.forEach(mechanism => {
                // check for null placeholder value here.  Because of the way the form works on contact mechanisms
                // we have to add essentially dummy records to bind the FormGroup stuff to.  So make sure we're not
                // dealing with an Array of those here first
                if (
                    mechanism.contactMechanism.type.name === 'Address' && mechanism.contactMechanism.address1 !== ''                            ||
                    mechanism.contactMechanism.type.name === 'Electronic Address' && mechanism.contactMechanism.electronicAddressString !== ''  ||
                    mechanism.contactMechanism.type.name === 'Telecommunications Number' && mechanism.contactMechanism.contactNumber !== ''
                ) {
                    const returnValue : any = {
                        ordinality  : mechanism.ordinality,
                        comments    : mechanism.comments,
                        extension   : mechanism.extension,
                        contactMechanism : {
                            type : {}
                        }
                    };

                    // include personUuid and sequenceId on PUT
                    if (isEdit) {
                        returnValue.version     = mechanism.version;
                        returnValue.sequenceId  = mechanism.sequenceId;
                        returnValue.personUuid  = mechanism.personUuid;
                    }

                    // purpose type
                    if (mechanism.purposeType.uuid !== 0) {
                        returnValue.purposeType = {
                            uuid : mechanism.purposeType.uuid
                        };
                    }
                    else {
                        returnValue.purposeType = {};
                    }

                    // solicitations
                    if (mechanism.solicitationIndicatorType.uuid !== 0) {
                        returnValue.solicitationIndicatorType = {
                            uuid : mechanism.solicitationIndicatorType.uuid
                        };
                    }
                    else {
                        returnValue.solicitationIndicatorType = {};
                    }

                    // phone?
                    if (mechanism.contactMechanism.type.name === 'Telecommunications Number') {
                        returnValue.contactMechanism.countryCode    = mechanism.contactMechanism.countryCode;
                        returnValue.contactMechanism.areaCode       = mechanism.contactMechanism.areaCode;
                        returnValue.contactMechanism.contactNumber  = mechanism.contactMechanism.contactNumber;
                    }
                    // email?
                    else if (mechanism.contactMechanism.type.name === 'Electronic Address') {
                        returnValue.contactMechanism.electronicAddressString = mechanism.contactMechanism.electronicAddressString;
                    }
                    // address?
                    else if (mechanism.contactMechanism.type.name === 'Address') {
                        returnValue.contactMechanism.address1   = mechanism.contactMechanism.address1;
                        returnValue.contactMechanism.address2   = mechanism.contactMechanism.address2;
                        returnValue.contactMechanism.city       = mechanism.contactMechanism.city;
                        returnValue.contactMechanism.state      = mechanism.contactMechanism.state;
                        returnValue.contactMechanism.postalCode = mechanism.contactMechanism.postalCode;
                        returnValue.contactMechanism.directions = mechanism.contactMechanism.directions;
                    }

                    // set contact mechanism type and uuid
                    returnValue.contactMechanism.type.uuid  = mechanism.contactMechanism.type.uuid;

                    if (isEdit) {
                        returnValue.contactMechanism.uuid = mechanism.contactMechanism.uuid;
                    }

                    // set contact mechanism version on PUT
                    if (isEdit) {
                        returnValue.contactMechanism.version = mechanism.contactMechanism.version;
                    }

                    // add this mechanism record to temporary collection
                    tempContactMechanisms.push(returnValue);
                }
            });

            // update contact mechanisms on model
            beneficiaryPOJO.contactMechanisms = tempContactMechanisms;
        }

        // clean up serviceCoverages
        if (beneficiaryPOJO.serviceCoverages.length > 0) {
            beneficiaryPOJO.serviceCoverages = beneficiaryPOJO.serviceCoverages.map(coverage => {
                // delete timestamp stuff
                delete coverage.createdBy;
                delete coverage.createdOn;
                delete coverage.updatedBy;
                delete coverage.updatedOn;

                // delete root version and sequenceId and personUuid if this is a create
                if (!isEdit) {
                    delete coverage.version;
                    delete coverage.sequenceId;
                    delete coverage.personUuid;
                }

                // service coverage plan classifications
                if (coverage.personServiceCoveragePlanClassifications.length > 0) {
                    coverage.personServiceCoveragePlanClassifications = coverage.personServiceCoveragePlanClassifications.map(classification => {
                        // do not include version and sequenceId if this is a create
                        const returnValue : any = {
                            fromDate    : classification.fromDate,
                            thruDate    : classification.thruDate,
                            value       : classification.value,
                            serviceCoveragePlanClassification: {
                                uuid : classification.serviceCoveragePlanClassification.uuid
                            }
                        };

                        // include sequenceId and version on PUT operation
                        if (isEdit) {
                            returnValue.sequenceId                                  = classification.sequenceId;
                            returnValue.version                                     = classification.version;
                            returnValue.serviceCoveragePlanClassification.version   = classification.serviceCoveragePlanClassification.version;
                        }

                        return returnValue;
                    });
                }
                else {
                    coverage.personServiceCoveragePlanClassifications = [];
                }

                // service coverage plans
                if (isEdit) {
                    coverage.serviceCoveragePlan = {
                        uuid    : coverage.serviceCoveragePlan.uuid,
                        version : coverage.serviceCoveragePlan.version,
                        serviceCoverageOrganization : {
                            uuid    : coverage.serviceCoveragePlan.serviceCoverageOrganization.uuid,
                            version : coverage.serviceCoveragePlan.serviceCoverageOrganization.version
                        }
                    };
                }
                else {
                    coverage.serviceCoveragePlan = {
                        uuid : coverage.serviceCoveragePlan.uuid,
                        serviceCoverageOrganization : {
                            uuid : coverage.serviceCoveragePlan.serviceCoverageOrganization.uuid
                        }
                    };
                }

                return coverage;
            });
        }
        else {
            beneficiaryPOJO.serviceCoverages = [];
        }

        // delete version
        if (!isEdit) {
            delete beneficiaryPOJO.version;
        }

        return beneficiaryPOJO;
    }

    /**
     *
     * @param response
     * @param observer
     */
    handleBeneficiaryResponse(response : any, observer : Observer<any>) {
        let beneficiary : any = response;

        if (!beneficiary.errors) {
            response.length ? beneficiary = response[0] : beneficiary = response;

            // there are no errors on this beneficiary, so return the object
            observer.next(beneficiary);
        }
        else {
            /*
             * pass the beneficiary message back to the observer
             *
             * This presumes that a beneficiary will have a message property on it.
             * */
            observer.error(beneficiary.message);
        }
    }

    /**
     *
     * @param error
     * @param observer
     */
    handleBeneficiaryError(error : any, observer : Observer<any>) {
        /*
         * simply notify the observer that there's an error, but we are stopping
         * the buck here - data about the error ain't going nowhere... hehe...
         *
         * (honestly... I'm not sure why we're obscuring the error. I haven't taken that yoga class yet)
         */
        if (observer != null) {
            observer.error(undefined);
        }

    }

    /**
     * @description - get personal beneficiary information
     * @param uuid
     * @returns {Observable<any>}
     */
    getBeneficiary(uuid : string) : Observable<any> {
        // since this is a root endpoint we have to extract it's base url from the webpack config
        const handle  : string = 'getBeneficiary',
              apiUrl  : string = API_CONFIG[handle] + uuid;

        return Observable.create(observer => {
            // get beneficiary data
            this.backendService.get(apiUrl, handle, BENEFICIARY_MOCK)
                .first()
                .subscribe(
                    response => this.handleBeneficiaryResponse(response, observer),
                    error => this.handleBeneficiaryError(error, observer)
                );
        });
    }

    /**
     * @description create a new Beneficiary Profile
     * @param newBeneficiaryProfile new Beneficiary Profile domain model to POST
     * @returns {Observable<any>}
     */
    createBeneficiary(newBeneficiaryProfile : Beneficiary) : Observable<any> {
        // we need this to handle the POST properly
        const handle  : string = 'createBeneficiary',
              apiUrl  : string = API_CONFIG[handle];

        // scrub the model
        const createBeneficiaryPayload : any = this.scrubBeneficiaryModel(newBeneficiaryProfile, false);

        // setup the request
        const body    = JSON.stringify(createBeneficiaryPayload);

        return Observable.create(observer => {
            // get beneficiary data
            this.backendService.post(apiUrl, handle, BENEFICIARY_MOCK, body)
                .first()
                .subscribe(
                    response => this.handleBeneficiaryResponse(response, observer),
                    error => this.handleBeneficiaryError(error, observer)
                );
        });
    }

    /**
     * updates the person portion of beneficiary domain model
     * @param beneficiaryPersonUpdate
     * @returns {any}
     */
    updateBeneficiaryPerson(beneficiaryPersonUpdate : Beneficiary) {

        return Observable.create(observer => {
            // we need this to handle the PUT properly
            const handle  : string = 'updateBeneficiaryPerson',
                  apiUrl  : string = API_CONFIG[handle] + beneficiaryPersonUpdate.get('uuid');

            // scrub the model
            let updateBeneficiaryPersonPayload : any;
            try {
                updateBeneficiaryPersonPayload = this.scrubBeneficiaryModel(beneficiaryPersonUpdate, true);
            }
            catch (error) {
                observer.error('Problem scrubbing beneficiary model: ' + error.message);
            }

            // remove extra stuff that's not part of Beneficiary Person
            delete updateBeneficiaryPersonPayload.contactMechanisms;
            delete updateBeneficiaryPersonPayload.serviceCoverages;

            // setup the request
            const body  = JSON.stringify(updateBeneficiaryPersonPayload);

            // get beneficiary data
            this.backendService.put(apiUrl, handle, BENEFICIARY_MOCK, body)
                .first()
                .subscribe(
                    response => this.handleBeneficiaryResponse(response, observer),
                    error => this.handleBeneficiaryError(error, observer)
                );
        });
    }

    /**
     * updates the service coverage portion of beneficiary domain model
     * @param beneficiaryServiceCoveragesUpdate
     * @returns {any}
     */
    updateBeneficiaryServiceCoverages(beneficiaryServiceCoveragesUpdate : Beneficiary) {
        // we need this to handle the PUT properly
        const handle  : string = 'updateBeneficiaryServiceCoverages',
              uuid    : string = beneficiaryServiceCoveragesUpdate.get('uuid');

        let apiUrl : string = API_CONFIG[handle];

        // scrub the model
        let updateBeneficiaryServiceCoveragesPayload : any = this.scrubBeneficiaryModel(beneficiaryServiceCoveragesUpdate, true);

        // remove everything except service coverages
        updateBeneficiaryServiceCoveragesPayload = updateBeneficiaryServiceCoveragesPayload.serviceCoverages;

        // setup the request
        const body  = JSON.stringify(updateBeneficiaryServiceCoveragesPayload);

        return Observable.create(observer => {
            // inject uuid into url
            apiUrl = apiUrl.replace('${uuid}', uuid);

            // get beneficiary data
            this.backendService.put(apiUrl, handle, BENEFICIARY_MOCK, body)
                .first()
                .subscribe(
                    response => this.handleBeneficiaryResponse(response, observer),
                    error => this.handleBeneficiaryError(error, observer)
                );
        });
    }

    /**
     * updates the contact mechanisms portion of beneficiary domain model
     * @param beneficiaryServiceContactMechanismsUpdate
     * @returns {any}
     */
    updateBeneficiaryContactMechanisms(beneficiaryServiceContactMechanismsUpdate : Beneficiary) {
        // we need this to handle the PUT properly
        const handle  : string = 'updateBeneficiaryContactMechanisms',
              uuid    : string = beneficiaryServiceContactMechanismsUpdate.get('uuid');

        let apiUrl : string = API_CONFIG[handle];

        // scrub the model
        let updateBeneficiaryContactMechanismsPayload : any = this.scrubBeneficiaryModel(beneficiaryServiceContactMechanismsUpdate, true);

        // remove everything except contact mechanisms
        updateBeneficiaryContactMechanismsPayload = updateBeneficiaryContactMechanismsPayload.contactMechanisms;

        // setup the request
        const body  = JSON.stringify(updateBeneficiaryContactMechanismsPayload);

        return Observable.create(observer => {
            // inject uuid into url
            apiUrl = apiUrl.replace('${uuid}', uuid);

            // get beneficiary data
            this.backendService.put(apiUrl, handle, BENEFICIARY_MOCK, body)
                .first()
                .subscribe(
                    response => this.handleBeneficiaryResponse(response, observer),
                    error => this.handleBeneficiaryError(error, observer)
                );
        });
    }
}
