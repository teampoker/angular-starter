import {Injectable} from '@angular/core';
import {
    Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';

import {BeneficiaryActions} from '../../../../store/Beneficiary/actions/beneficiary.actions';
import {FormsActions} from '../../../../store/Forms/forms.actions';
import {NavActions} from '../../../../store/Navigation/nav.actions';
import {ReservationActions} from '../../../../store/Reservation/actions/reservation.actions';

@Injectable()

/**
 * Implementation of BeneficiaryDashboardResolveService : ensure Beneficiary Profile Dashboard route loads with the correct BeneficiaryState values
 */
export class BeneficiaryDashboardResolveService implements Resolve<boolean> {
    /**
     * BeneficiaryDashboardResolveService constructor
     * @param {BeneficiaryActions} beneficiaryActions
     * @param {FormsActions} beneficiaryFormsActions
     * @param {NavActions} navActions
     * @param {ReservationActions} reservationActions
     */
    constructor(
        private beneficiaryActions      : BeneficiaryActions,
        private beneficiaryFormsActions : FormsActions,
        private navActions              : NavActions,
        private reservationActions      : ReservationActions
    ) {}

    resolve(route : ActivatedRouteSnapshot) : boolean {
        // reset lightbox isPopped value
        this.navActions.toggleIsPoppedNavState(false);

        // init FormsState
        this.beneficiaryFormsActions.initFormsState();

        // grab form types metadata
        this.beneficiaryFormsActions.getFormTypes();
        this.beneficiaryFormsActions.getAuthReasons();

        // init BeneficiaryState for View Profile view
        this.beneficiaryActions.initBeneficiaryDashboard();

        // grab the event reasons
        this.reservationActions.fetchReservationEventReasons();

        return true;
    }
}
