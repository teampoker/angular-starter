import {
    async,
    inject,
    TestBed
} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {APIMockService} from '../../../../shared/services/Mock/api-mock.service';
import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {BeneficiaryFormsService} from './beneficiary-forms.service';
import {BENEFICIARY_FORMS_MOCK} from './beneficiary-forms.service.mock';

describe('BeneficiaryFormsService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports     : [HttpModule],
            providers   : [
                APIMockService,
                BackendService,
                BeneficiaryFormsService
            ]
        });
    });

    it('should get a list of form types', async(inject([BeneficiaryFormsService], beneficiaryFormsService => {
        // perform the api call
        beneficiaryFormsService.getFormTypes().subscribe(response => {
            expect(response).toEqual(BENEFICIARY_FORMS_MOCK.getFormTypes);
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));
});
