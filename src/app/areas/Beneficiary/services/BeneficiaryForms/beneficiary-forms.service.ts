import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {BENEFICIARY_FORMS_MOCK} from './beneficiary-forms.service.mock';
import {
    toApi as newBeneficiaryFormToApi,
    FormStateDetail
} from '../../../../store/Forms/types/form-state-detail.model';

@Injectable()

/**
 * Implementation of BeneficiaryFormsService: serves up common data mainly used for handling beneficiary forms data
 */
export class BeneficiaryFormsService {
    /**
     * BeneficiaryFormsService constructor
     * @param backendService
     */
    constructor (private backendService : BackendService) {}

    /**
     * retrieve the list of available form types from the API
     */
    getFormTypes() {
        const handle    : string = 'getFormTypes',
              apiUrl    : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let formTypes : any;

            this.backendService.get(apiUrl, handle, BENEFICIARY_FORMS_MOCK)
                .first()
                .subscribe(response => {
                    // store data types
                    formTypes = response;

                    // update store
                    observer.next(formTypes);
                }, error => {
                    // update store
                    observer.error(error);
                });
        });
    }

    /**
     * retrieve the list of available form authorization reasons from the API
     */
    getAuthorizationReasons() {
        const handle    : string = 'getAuthorizationReasons',
              apiUrl    : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let authReasons : any;

            this.backendService.get(apiUrl, handle, BENEFICIARY_FORMS_MOCK)
                .first()
                .subscribe(response => {
                    // store data types
                    authReasons = response;

                    // update store
                    observer.next(authReasons);
                }, error => {
                    // update store
                    observer.error(error);
                });
        });
    }

    /**
     * save a new form for a given beneficiary
     * @param uuid
     * @param newFormData
     * @returns {void}
     */
    createForm(uuid : string, newFormData : FormStateDetail) {
        // we need this to handle the POST properly
        const handle : string = 'createForm';

        let apiUrl : string = API_CONFIG[handle];

        // scrub the model
        const createFormPayload : any = newBeneficiaryFormToApi(newFormData, false);

        // stringify request payload
        const body = JSON.stringify(createFormPayload);

        return Observable.create(observer => {
            // inject uuid into url
            apiUrl = apiUrl.replace('${personUuid}', uuid);

            // get beneficiary data
            this.backendService.post(apiUrl, handle, BENEFICIARY_FORMS_MOCK, body)
                               .first()
                               .subscribe(response => {
                                   // update store
                                   observer.next(response);
                               }, error => {
                                   // update store
                                   observer.error(error);
                               });
        });
    }

    /**
     * get list of forms for a given beneficiary
     * @param uuid beneficiary uuid
     * @returns {any}
     */
    getForms(uuid : string) {
        const handle : string = 'getForms';

        let apiUrl : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let authReasons : any;

            // inject uuid into url
            apiUrl = apiUrl.replace('{$personUuid}', uuid);

            this.backendService.get(apiUrl, handle, BENEFICIARY_FORMS_MOCK)
                .first()
                .subscribe(response => {
                    // store data types
                    authReasons = response;

                    // update store
                    observer.next(authReasons);
                }, error => {
                    // update store
                    observer.error(error);
                });
        });
    }

    /**
     * get specific form's detailed data
     * @param uuid beneficiary uuid
     * @param formUuid form uuid
     * @returns {any}
     */
    getFormDetails(uuid : string, formUuid : string) {
        const handle : string = 'getFormDetails';

        let apiUrl : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let authReasons : any;

            // inject uuid into url
            apiUrl = apiUrl.replace('{$personUuid}', uuid);
            apiUrl = apiUrl.replace('{$formUuid}', formUuid);

            this.backendService.get(apiUrl, handle, BENEFICIARY_FORMS_MOCK)
                .first()
                .subscribe(response => {
                    // store data types
                    authReasons = response;

                    // update store
                    observer.next(authReasons);
                }, error => {
                    // update store
                    observer.error(error);
                });
        });
    }
}
