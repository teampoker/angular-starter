import {Injectable} from '@angular/core';
import {
    Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';

import {BeneficiaryActions} from '../../../../store/Beneficiary/actions/beneficiary.actions';
import {BeneficiaryInfoActions} from '../../../../store/Beneficiary/actions/beneficiary-info.actions';
import {BeneficiaryContactMechanismActions} from '../../../../store/Beneficiary/actions/beneficiary-contact-mechanisms.actions';
import {BeneficiaryConnectionsActions} from '../../../../store/Beneficiary/actions/beneficiary-connections.actions';
import {BeneficiarySpecialRequirementsActions} from '../../../../store/Beneficiary/actions/beneficiary-special-requirements.actions';
import {BeneficiaryMedicalConditionsActions} from '../../../../store/Beneficiary/actions/beneficiary-medical-conditions.actions';
import {SearchActions} from '../../../../store/Search/search.actions';
import {NavActions} from '../../../../store/Navigation/nav.actions';

@Injectable()

/**
 * Implementation of ViewProfileResolveService : ensure View Profile route loads with the correct BeneficiaryState values
 */
export class ViewProfileResolveService implements Resolve<boolean> {
    /**
     * ViewProfileResolveService constructor
     * @param beneficiaryActions
     * @param beneficiaryInfoActions
     * @param beneficiaryContactMechanismActions
     * @param beneficiaryConnectionsActions
     * @param beneficiarySpecialRequirementsActions
     * @param beneficiaryMedicalConditionsActions
     * @param searchActions
     * @param navActions
     */
    constructor(
        private beneficiaryActions                      : BeneficiaryActions,
        private beneficiaryInfoActions                  : BeneficiaryInfoActions,
        private beneficiaryContactMechanismActions      : BeneficiaryContactMechanismActions,
        private beneficiaryConnectionsActions           : BeneficiaryConnectionsActions,
        private beneficiarySpecialRequirementsActions   : BeneficiarySpecialRequirementsActions,
        private beneficiaryMedicalConditionsActions     : BeneficiaryMedicalConditionsActions,
        private searchActions                           : SearchActions,
        private navActions                              : NavActions
    ) {}

    resolve(route : ActivatedRouteSnapshot) : boolean {
        // clear relevant people search state
        this.searchActions.resetPeopleSearchState();

        // update any needed dropdown types
        this.beneficiaryInfoActions.updateBeneficiaryInfoDropdownTypes();
        this.beneficiaryContactMechanismActions.updateContactInfoDropdownTypes();
        this.beneficiaryConnectionsActions.updateBeneficiaryConnectionDropdownTypes();
        this.beneficiarySpecialRequirementsActions.updateSpecialRequirementDropdownTypes();
        this.beneficiaryMedicalConditionsActions.updateMedicalConditionDropdownTypes();

        // init BeneficiaryState for View Profile view
        this.beneficiaryActions.initViewBeneficiaryProfile();

        // reset lightbox isPopped value
        this.navActions.toggleIsPoppedNavState(false);

        return true;
    }
}
