import {
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';

import {SearchEntryComponent} from './search-entry.component';
import {SearchModule} from './search.module';
import {NavActions} from '../../store/Navigation/nav.actions';
import {EnumNavOption} from '../../store/Navigation/types/nav-option.model';

// Mock the NavActions with the methods we need to trigger
class MockNavActions {
    constructor() {}

    updateActiveNavState(navOption : EnumNavOption) {

    }
}

// suite of related tests
describe('SearchEntryComponent', () => {
    let fixture         : ComponentFixture<SearchEntryComponent>,
        compInstance    : SearchEntryComponent,
        element         : any,
        mockNavActions  : MockNavActions;

    // setup tasks to perform before each test
    beforeEach(() => {
        // refine the initial testing module configuration
        TestBed.configureTestingModule({
            imports     : [
                RouterTestingModule,
                SearchModule
           ],
            providers   : [
                {
                    provide     : NavActions,
                    useClass    : MockNavActions
                }
            ]
        });

        // create component fixture
        fixture = TestBed.createComponent(SearchEntryComponent);

        // grab instance of component class
        compInstance = fixture.componentInstance;

        // grab DOM representation
        element = fixture.nativeElement;

        // grab MockPaymentActions from the root injector
        mockNavActions = fixture.debugElement.injector.get(NavActions);

        // trigger initial bindings
        fixture.detectChanges();
    });

    // test definitions
    it('should set activeNavState to SEARCH on Init', () => {
        // spy on mock updateActiveNavState method
        spyOn(mockNavActions, 'updateActiveNavState');

        // trigger initial bindings
        compInstance.ngOnInit();

        expect(mockNavActions.updateActiveNavState).toHaveBeenCalled();
        expect(mockNavActions.updateActiveNavState).toHaveBeenCalledWith(EnumNavOption.SEARCH);
    });
});
