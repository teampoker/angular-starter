import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

import {NavActions} from '../../store/Navigation/nav.actions';
import {EnumNavOption} from '../../store/Navigation/types/nav-option.model';

@Component({
    templateUrl     : 'search-entry.component.html',
    styleUrls       : ['search-entry.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for SearchEntryComponent: responsible for search page layout
 */
export class SearchEntryComponent {
    /**
     * SearchEntryComponent constructor
     * @param navActions
     */
    constructor (private navActions : NavActions) {
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // set active epic to search
        this.navActions.updateActiveNavState(EnumNavOption.SEARCH);
    }
}
