import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';

import {SearchActions} from '../../../../../store/Search/search.actions';
import {SearchResultsPaging} from '../../../../../store/Search/types/search-results-paging.model';

@Component({
    selector        : 'search-header',
    templateUrl     : 'search-header.component.html',
    styleUrls       : ['search-header.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for SearchHeaderComponent: handles display of search results header info
 */
export class SearchHeaderComponent {
    /**
     * SearchHeaderComponent constructor
     */
    constructor(private searchActions : SearchActions) {}

    /**
     * current search paging results
     */
    @Input() paginationConfig : SearchResultsPaging;

    /**
     * handler for pagination component incrementHeaderPageIndex event
     */
    onIncrementPageIndex() {
        // dispatch increment page index action
        this.searchActions.incrementHeaderPageIndex();

        // refresh results with this new value
        this.searchActions.getHeaderSearchResults();
    }

    /**
     * handler for pagination component decrementHeaderPageIndex event
     */
    onDecrementPageIndex() {
        // dispatch decrement page index action
        this.searchActions.decrementHeaderPageIndex();

        // refresh results with this new value
        this.searchActions.getHeaderSearchResults();
    }
}
