import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy
} from '@angular/core';
import {List} from 'immutable';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {SearchActions} from '../../../../../store/Search/search.actions';
import {SearchResultsPaging} from '../../../../../store/Search/types/search-results-paging.model';
import {SearchResultState} from '../../../../../store/Search/types/search-result-state.model';
import {SearchResultsOrderByConfig} from '../../../../../store/Search/types/search-results-order-by-config.model';

@Component({
    selector        : 'search-results-panel',
    templateUrl     : 'search-results-panel.component.html',
    styleUrls       : ['search-results-panel.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for SearchResultsPanelComponent: handles display of search results
 */
export class SearchResultsPanelComponent {
    /**
     * SearchResultsPanelComponent constructor
     */
    constructor(private searchActions : SearchActions) {}

    /**
     * currently selected page size
     */
    @Input() selectedPageSize : KeyValuePair;

    /**
     * current result view count
     */
    @Input() paginationConfig : SearchResultsPaging;

    /**
     * Current list of search results
     */
    @Input() searchResults : List<SearchResultState>;

    /**
     * search results order by configuration
     */
    @Input() resultsOrderByConfig : SearchResultsOrderByConfig;

    /**
     * custom event emitted when search results order by config is changed
     * @type {EventEmitter<boolean>}
     */
    @Output() orderByConfigUpdated : EventEmitter<SearchResultsOrderByConfig> = new EventEmitter<SearchResultsOrderByConfig>();

    /**
     * custom event emitted when user changes the search results count
     */
    @Output() updatePageSize : EventEmitter<KeyValuePair> = new EventEmitter<KeyValuePair>();

    /**
     * custom event emitted when user wishes to navigate to add beneficiary profile
     */
    @Output() addBeneficiary : EventEmitter<any> = new EventEmitter<any>();

    /**
     * custom event emitted when user wishes to navigate to view a beneficiary profile
     */
    @Output() viewBeneficiary : EventEmitter<any> = new EventEmitter<any>();

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for change page size event
     * @param choice selected option value
     */
    changePageSize(choice : KeyValuePair) {
        this.updatePageSize.emit(choice);
    }

    /**
     * asc / desc sort button click event handler
     * @param value
     * @param direction
     */
    changeOrderBy(value : string, direction : string) {
        let config = new SearchResultsOrderByConfig();

        config = config.set('sortColumn', value).set('sortOrder', direction) as SearchResultsOrderByConfig;

        // emit new config
        this.orderByConfigUpdated.emit(config);
    }

    /**
     * handler for pagination component incrementHeaderPageIndex event
     */
    onIncrementPageIndex() {
        // dispatch increment page index action
        this.searchActions.incrementHeaderPageIndex();

        // refresh results with this new value
        this.searchActions.getHeaderSearchResults();
    }

    /**
     * handler for pagination component decrementHeaderPageIndex event
     */
    onDecrementPageIndex() {
        // dispatch decrement page index action
        this.searchActions.decrementHeaderPageIndex();

        // refresh results with this new value
        this.searchActions.getHeaderSearchResults();
    }

    /**
     * navigate user to create new profile
     */
    goToAddBeneficiary() {
        // emit to addBeneficiary
        this.addBeneficiary.emit();
    }

    /**
     * navigate user to view profile
     */
    goViewBeneficiary(uuid : string) {
        // emit to view Beneficiary
        this.viewBeneficiary.emit(uuid);
    }
}
