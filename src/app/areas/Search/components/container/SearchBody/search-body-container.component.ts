import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {List} from 'immutable';

import {SearchActions} from '../../../../../store/Search/search.actions';
import {BeneficiaryActions} from '../../../../../store/Beneficiary/actions/beneficiary.actions';
import {SearchStateSelectors} from '../../../../../store/Search/search.selectors';
import {SearchResultState} from '../../../../../store/Search/types/search-result-state.model';
import {SearchResultsOrderByConfig} from '../../../../../store/Search/types/search-results-order-by-config.model';
import {SearchResultsPaging} from '../../../../../store/Search/types/search-results-paging.model';
import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';

@Component({
    selector        : 'search-body-container',
    templateUrl     : 'search-body-container.component.html',
    styleUrls       : ['search-body-container.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for SearchBodyContainerComponent: handles display of search status cards
 */
export class SearchBodyContainerComponent {
    /**
     * SearchBodyContainerComponent constructor
     * @param router
     * @param cd
     * @param searchActions
     * @param beneficiaryActions
     * @param searchSelectors
     */
    constructor(
        private router              : Router,
        private cd                  : ChangeDetectorRef,
        private searchActions       : SearchActions,
        private beneficiaryActions  : BeneficiaryActions,
        private searchSelectors     : SearchStateSelectors
    ) {
        // subscribe to aggregated state
        this.stateSubscription = Observable.combineLatest(
            this.searchSelectors.headerSearchResults(),
            this.searchSelectors.headerSearchOrderByConfig(),
            this.searchSelectors.headerSearchResultsPaging(),
            this.searchSelectors.headerSearchSelectedPageSize()
        )
        .subscribe(val => {
            // update local state
            this.searchResults          = val[0];
            this.resultsOrderByConfig   = val[1];
            this.searchResultsPaging    = val[2];
            this.selectedPageSize       = val[3];

            // trigger change detection
            this.cd.markForCheck();
        });
    }

    /**
     * Redux state subscription
     */
    private stateSubscription : Subscription;

    /**
     * list of search results returned by API
     */
    searchResults : List<SearchResultState>;

    /**
     * filter and order by settings for search results
     */
    resultsOrderByConfig : SearchResultsOrderByConfig;

    /**
     * pagination state for search results
     */
    searchResultsPaging : SearchResultsPaging;

    /**
     * currently selected page size
     */
    selectedPageSize : KeyValuePair;

    /**
     * event handler for page size update event
     * @param pageSize
     */
    onUpdatePageSize(pageSize : KeyValuePair) {
        // dispatch update search search results page size action
        this.searchActions.updateHeaderSearchSelectedPageSize(pageSize);
    }

    /**
     * event handler for beneficiary update event
     * @param uuid
     */
    onViewBeneficiary(uuid : string) {
        this.router.navigate(['/Beneficiary/', uuid]);
    }

    /**
     * event handler for search results order by config update event
     * @param config
     */
    onOrderByConfigUpdated(config : SearchResultsOrderByConfig) {
        // dispatch update search results order by config action
        this.searchActions.updateHeaderSearchOrderByConfig(config);

        // refresh results
        this.searchActions.getHeaderSearchResults();
    }

    /**
     * navigate user to Create New Beneficiary Profile
     */
    onAddBeneficiary() {
        // route user to create new profile view
        this.router.navigate(['/Beneficiary/CreateNewProfile']);
    }

    /**
     * component lifecycle destroy hook
     */
    ngOnDestroy() {
        // unsubscribe from Redux store
        this.stateSubscription.unsubscribe();
    }
}
