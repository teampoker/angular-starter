import {NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {SearchRoutingModule} from './search-routing.module';
import {SearchEntryComponent} from './search-entry.component';
import {SearchBodyContainerComponent} from './components/container/SearchBody/search-body-container.component';
import {SearchHeaderComponent} from './components/presentational/SearchHeader/search-header.component';
import {SearchResultsPanelComponent} from './components/presentational/SearchResultsPanel/search-results-panel.component';

@NgModule({
    imports         : [
        SharedModule,
        SearchRoutingModule
   ],
    declarations    : [
        SearchEntryComponent,
        SearchBodyContainerComponent,
        SearchHeaderComponent,
        SearchResultsPanelComponent
   ],
    providers       : []
})

export class SearchModule {

}
