import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {SearchEntryComponent} from './search-entry.component';
import {SearchBodyContainerComponent} from './components/container/SearchBody/search-body-container.component';

@NgModule({
    imports : [
        RouterModule.forChild([
            {
                path        : '',
                component   : SearchEntryComponent,
                children    : [
                    {
                        path        : '',
                        component   : SearchBodyContainerComponent
                    }
                ]
            }
        ])
   ],
    exports : [
        RouterModule
    ]
})

export class SearchRoutingModule {

}
