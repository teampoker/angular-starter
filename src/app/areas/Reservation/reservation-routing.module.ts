import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ReservationBodyContainerComponent} from './components/container/ReservationBody/reservation-body-container.component';
import {ReservationHistoryContainerComponent} from './components/container/ReservationHistory/reservation-history-container.component';
import {ReservationEntryComponent} from './reservation-entry.component';
import {ReservationResolveService} from './services/ReservationResolveService/reservation-resolve.service';

@NgModule({
    imports : [
        RouterModule.forChild([
            {
                path        : '',
                component   : ReservationEntryComponent,
                children    : [
                    {
                        path            : ':beneficiaryUUID/CreateNewReservation',
                        component       : ReservationBodyContainerComponent,
                        resolve         : {
                            ReservationResolveService
                        }
                    },
                    {
                        path            : ':beneficiaryUUID/EditReservation/:reservationUUID',
                        component       : ReservationBodyContainerComponent,
                        resolve         : {
                            ReservationResolveService
                        }
                    },
                    {
                        path            : ':beneficiaryUUID/History',
                        component       : ReservationHistoryContainerComponent,
                        resolve         : {
                            ReservationResolveService
                        }
                    },
                    {
                        path            : ':beneficiaryUUID/Scheduled',
                        component       : ReservationHistoryContainerComponent,
                        resolve         : {
                            ReservationResolveService
                        }
                    }
                ]
            }
        ])
   ],
    exports : [
        RouterModule
    ],
    providers : [
        ReservationResolveService
    ]
})

export class ReservationRoutingModule {

}
