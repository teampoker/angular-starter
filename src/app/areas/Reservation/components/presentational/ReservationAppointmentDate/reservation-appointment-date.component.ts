import {
    Input,
    Component,
    ChangeDetectionStrategy,
    SimpleChanges
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import * as moment from 'moment';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {ReservationAppointmentDateState} from '../../../../../store/Reservation/types/reservation-appointment-date-state.model';
import {createTimeValidator} from '../../../../../shared/components/TimePicker/time-picker.component';
import {createDateValidator} from '../../../../../shared/components/DatePicker/date-picker.component';

@Component({
    selector        : 'reservation-appointment-date',
    templateUrl     : 'reservation-appointment-date.component.html',
    styleUrls       : ['reservation-appointment-date.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationAppointmentDateComponent: handles display of reservation status cards title
 */
export class ReservationAppointmentDateComponent {
    /**
     * ReservationAppointmentDateComponent constructor
     */
    constructor(
        private builder                : FormBuilder,
        private reservationActions     : ReservationActions
    ) {
        // init form input fields
        this.appointmentDate = new FormControl('', Validators.compose([
            Validators.required,
            createDateValidator(this.dateFormat)
        ]));
        this.appointmentTime = new FormControl('', Validators.compose([
            Validators.required,
            createTimeValidator(this.timeFormat)
        ]));

        // build reservation appointment FormControl group
        this.reservationAppointmentDateForm = this.builder.group({
            appointmentDate : this.appointmentDate,
            appointmentTime : this.appointmentTime
        });

        // subscribe to appointment time updates
        this.appointmentTime.valueChanges.subscribe(value => {
            if (value) {
                if (value !== 'Invalid date') {
                    // pull appointmentDate and appointmentTime into moment
                    const apptDate : any = moment(this.reservationAppointmentDateState.get('appointmentDate')),
                          apptTime : any = moment(value, this.timeFormat);

                    // store newly selected appointmentTime
                    this.selectedAppointmentTime = value;

                    // create raw moment object to work with
                    const dateTime : any = moment();

                    // configure new dateTime moment with appointmentDate / appointmentTime values
                    dateTime.year(apptDate.year()).month(apptDate.month()).date(apptDate.date()).hour(apptTime.hour()).minute(apptTime.minute());

                    // convert to ISO string so API doesn't detonate
                    this.reservationActions.updateAppointmentTime({
                        fieldName   : 'appointmentTime',
                        fieldValue  : dateTime
                    });

                    this.reservationActions.updateAppointmentTimeValid({
                        fieldName: 'appointmentTimeValid',
                        fieldValue: true
                    });
                }
                else {
                    this.reservationActions.updateAppointmentTimeValid({
                        fieldName   : 'appointmentTimeValid',
                        fieldValue  : false
                    });
                }
            }
        });

        this.appointmentDate.valueChanges.subscribe(value => {
            if (value) {
                this.reservationActions.updateAppointmentDate({
                    fieldName   : 'appointmentDate',
                    fieldValue  : moment(value).format(this.dateFormat)
                });

                // clear out time holder and set valid to false
                this.selectedAppointmentTime = undefined;

                this.reservationActions.updateAppointmentTimeValid({
                    fieldName   : 'appointmentTimeValid',
                    fieldValue  : false
                });
            }
        });
    }

    /**
     * reservation reset form indicator
     */
    @Input() resetForm : boolean = false;

    /**
     * reservation is editing indicator
     */
    @Input() isEditing : boolean;

    /**
     * boolean enabled/disabled indicator
     */
    @Input() disabled : boolean = false;

    /**
     * reservation appointment date UI state
     */
    @Input() reservationAppointmentDateState : ReservationAppointmentDateState;

    /**
     * Object contains the appointment date form group
     */
    reservationAppointmentDateForm : FormGroup;

    /**
     * appointmentDate field
     */
    appointmentDate : FormControl;

    /**
     * appointmentTime field
     */
    appointmentTime : FormControl;

    /**
     * store property name used in accelerator
     */
    valuePropertyName : string = '';

    /**
     * store display name used in accelerator
     */
    displayPropertyName : string = '';

    /**
     * date picker selected date
     */
    selectedDate : string = '';

    /**
     * selected appointment time
     */
    selectedAppointmentTime : string;

    /**
     * time picker hourStep
     */
    hourStep : number = 1;

    /**
     * time picker hourStep
     */
    minuteStep : number = 10;

    /**
     * time picker boolean used to determine hide/show of am/pm
     */
    showMeridiem : boolean = true;

    /**
     * Date format to use in date picker
     * @type {string}
     */
     dateFormat : string = 'MM/DD/YYYY';

    /**
     * Date format to use in date picker
     * @type {string}
     */
    timeFormat : string = 'YYYY-MM-DDTHH:mm:ssZ';

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     */
    ngOnChanges(changes : SimpleChanges) {
        // reset form
        if (changes['resetForm']) {
            // only toggle is previous value was false and is now true
            if (!changes['resetForm'].previousValue && changes['resetForm'].currentValue) {
                this.reservationAppointmentDateForm.reset({
                    // restore control value of datepicker otherwise it will display as blank
                    birthDate : this.reservationAppointmentDateState.get('appointmentDate')
                });
            }
        }
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        if (this.isEditing) {
            this.selectedAppointmentTime = moment(this.reservationAppointmentDateState.get('appointmentTime', '')).format(this.timeFormat);
        }
    }
}
