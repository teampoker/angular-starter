import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';

import {BeneficiaryHeader} from '../../../../../store/Beneficiary/types/beneficiary-header.model';
import {BeneficiaryPlansState} from '../../../../../store/Beneficiary/types/beneficiary-plans-state.model';
import {BeneficiarySpecialRequirementsState} from '../../../../../store/Beneficiary/types/beneficiary-special-requirements-state.model';

@Component({
    selector        : 'reservation-header',
    templateUrl     : 'reservation-header.component.html',
    styleUrls       : ['reservation-header.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationHeaderComponent: handles display of reservation status cards title
 */
export class ReservationHeaderComponent {
    /**
     * ReservationHeaderComponent constructor
     */
    constructor() {}

    /**
     * reservation is editing indicator
     */
    @Input() isEditing : boolean;

    /**
     * beneficiary's uuid
     */
    @Input() beneficiaryUuid : string;

    /**
     * Object containing header information
     * @type {Object}
     */
    @Input() headerConfig : BeneficiaryHeader;

    /**
     * beneficiary service coverages UI state
     */
    @Input() beneficiaryPlansState : BeneficiaryPlansState;

    /**
     * beneficiary special requirements UI state
     */
    @Input() specialRequirementsState : BeneficiarySpecialRequirementsState;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }
}
