import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnChanges,
    SimpleChanges
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {Iterable} from 'immutable';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {BeneficiaryConnectionsActions} from '../../../../../store/Beneficiary/actions/beneficiary-connections.actions';
import {BeneficiaryConnectionsState} from '../../../../../store/Beneficiary/types/beneficiary-connections-state.model';
import {ReservationModeOfTransportationState} from '../../../../../store/Reservation/types/reservation-mode-of-transportation-state.model';
import {ReservationAccelerator} from '../../../../../store/Reservation/types/reservation-accelerator.model';
import {NameUuid} from '../../../../../store/types/name-uuid.model';
import {MetaDataTypesSelectors} from '../../../../../store/MetaDataTypes/meta-data-types.selectors';
import {ServiceCategories} from '../../../../../store/MetaDataTypes/types/service-categories.model';

@Component({
    selector        : 'reservation-mode-of-transportation',
    templateUrl     : 'reservation-mode-of-transportation.component.html',
    styleUrls       : ['reservation-mode-of-transportation.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationModeOfTransportationComponent: handles display of reservation status cards title
 */
export class ReservationModeOfTransportationComponent implements OnChanges {
    /**
     * ReservationModeOfTransportationComponent constructor
     * @param {FormBuilder} builder
     * @param {ReservationActions} reservationActions
     * @param {BeneficiaryConnectionsActions} beneficiaryConnectionsActions
     * @param {MetaDataTypesSelectors} metaDataSelectors
     */
    constructor(
        private builder                         : FormBuilder,
        private reservationActions              : ReservationActions,
        private beneficiaryConnectionsActions   : BeneficiaryConnectionsActions,
        private metaDataSelectors               : MetaDataTypesSelectors
    ) {
        // init form input fields
        this.modeOfTransportation = new FormControl('', Validators.required);
        this.driver               = new FormControl('', Validators.required);

        // build FormControl group
        this.reservationModeOfTransportationForm = this.newFormGroup();
    }

    /**
     * beneficiary connections panel UI state
     */
    @Input() beneficiaryConnectionsState : BeneficiaryConnectionsState;

    /**
     * boolean enabled/disabled indicator
     */
    @Input() disabled : boolean = false;

    /**
     * reservation reset form indicator
     */
    @Input() resetForm : boolean = false;

    /**
     * reservation is editing indicator
     */
    @Input() isEditing : boolean;

    /**
     * reservation mode of transportation UI state
     */
    @Input() reservationModeOfTransportationState : ReservationModeOfTransportationState;

    /**
     * store display name used in accelerator
     */
    displayPropertyName : string = '';

    /**
     * driver field
     */
    driver : FormControl;

    /**
     * modeOfTransportation field
     */
    modeOfTransportation : FormControl;

    modeOfTransportationValue : ServiceCategories = new ServiceCategories();

    /**
     * Object contains the  treatment type form group
     */
    reservationModeOfTransportationForm : FormGroup;

    /**
     * store property name used in accelerator
     */
    valuePropertyName : string = '';

    /**
     * Formats the display of a selected value
     * @param data
     * @returns {string}
     */
    displayFormatter(data : any) : string {
        if (Iterable.isIterable(data)) {
            return this.listFormatter(data);
        }

        return data;
    }

    /**
     * event handler for driver accelerator selector
     * @param choice ReservationAccelerator
     */
    driverSelection(choice : ReservationAccelerator | any)  {
        // make sure that the form has been touched
        // the plugin allows for blank select
        // blank selection happens on init
        // so we need to check to see if it was
        // meant for add new connection
        if (choice !== undefined) {
            if (choice.addAConnection || choice.name === 'PLUS_ADD_A_CONNECTION' || choice === 'PLUS_ADD_A_CONNECTION') {
                // add a connection
                this.beneficiaryConnectionsActions.addBeneficiaryConnection();
                this.reservationActions.connectionRequestIssuedBy('DRIVER');
            }
            else {
                if (choice.uuid) {
                    const driver = new NameUuid().withMutations(accelerator => accelerator
                        .set('uuid', choice.uuid)
                        .set('name', choice.name)) as NameUuid;

                    // emit updated value
                    this.reservationActions.updateModeOfTransportationDriver(driver);
                }
            }
        }
    }

    /**
     * custom ng2-auto-complete list formatter
     * @param data
     * @returns {string}
     */
    listFormatter(data : any) : string {
        let html : string = '';

        html += data[this.displayPropertyName] ? data[this.displayPropertyName] + ' ' : '';
        html += data[this.valuePropertyName] ? '(' + data[this.valuePropertyName] + ')' : '';
        return html;
    }

    /**
     * custom track by index function for ngFor directives
     * @param {number} index
     * @returns {number}
     */
    trackByIndex(index : number) : any {
        return index;
    }

    /**
     * event handler for modeOfTransportation type selection event
     * @param {NameUuid} choice selected modeOfTransportation
     */
    updateModeOfTransportation(choice : NameUuid) {
        // emit updated value
        this.reservationActions.updateModeOfTransportation(choice);
    }

    /**
     * event handler for modeOfTransportation type selection event
     * @param {string} name
     * @param {string} uuid
     */
    updateServiceOffering(name : string, uuid : string) {
        // emit updated value
        const newMot : NameUuid = new NameUuid({
            name,
            uuid
        });

        this.reservationActions.updateModeOfTransportation(newMot);
    }

    /**
     * event handler for resetting forms
     */
    newFormGroup() {
        return this.builder.group({
            // placeholder for dynamically generated form groups
            driver               : this.driver,
            modeOfTransportation : this.modeOfTransportation
        });
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        if (this.isEditing) {
            // set driver info
            this.driver.setValue(this.reservationModeOfTransportationState.getIn(['driverInfo', 'name']));
        }

        // get the name of the mode of transportation if a uuid is present and set form control value
        if (this.reservationModeOfTransportationState.getIn(['modeOfTransportation', 'uuid'], '') !== '') {
            this.metaDataSelectors
                .getModeOfTransportation(this.reservationModeOfTransportationState.getIn(['modeOfTransportation', 'uuid']))
                .first()
                .subscribe(data => this.modeOfTransportationValue = data);
        }
        // reset form
        if (changes['resetForm']) {
            if (this.resetForm) {
                this.updateModeOfTransportation(new NameUuid());
                this.modeOfTransportationValue = new ServiceCategories();
                this.reservationModeOfTransportationForm = this.newFormGroup();
            }
        }
    }
}
