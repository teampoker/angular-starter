import {
    Component,
    Input,
    ChangeDetectionStrategy,
    EventEmitter,
    ElementRef,
    ViewChild,
    Output,
    SimpleChanges
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {List} from 'immutable';

import {NameUuid} from '../../../../../store/types/name-uuid.model';
import {AddressLocation} from '../../../../../store/types/address-location.model';
import {getAddressComponent} from '../../../../../store/utils/parse-google-places';

@Component({
    selector        : 'reservation-location',
    templateUrl     : 'reservation-location.component.html',
    styleUrls       : ['reservation-location.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
/**
 * Implementation for ReservationPickupLocationComponent: handles display of reservation status cards title
 */
export class ReservationLocationComponent {
    /**
     * ReservationLocationComponent constructor
     */
    constructor(private builder : FormBuilder) {
        // init form input fields
        this.location = new FormControl('', Validators.required);
        this.previousLocations = new FormControl('', Validators.required);

        // build FormControl group
        this.reservationLocationForm = this.newFormGroup();
    }

    /**
     * DOM reference for google autocomplete
     */
    @ViewChild('googlePlace') googlePlace : ElementRef;

    /**
     * DOM reference for previous location autocomplete
     */
    @ViewChild('previous') previous : ElementRef;

    /**
     * reservation reset form indicator
     */
    @Input() resetForm : boolean = false;

    /**
     * reservation is editing indicator
     */
    @Input() isEditing : boolean;

    /**
     * boolean enabled/disabled indicator
     */
    @Input() disabled : boolean = false;

    /**
     * Text to display as label and placeholder
     */
    @Input() formDisplayName : string;

    /**
     * reservation location UI state
     */
    @Input() reservationLocationState : AddressLocation;

    /**
     * reservation recent locations
     */
    @Input() recentLocations : List<NameUuid>;

    /**
     * send change events upstream
     */
    @Output() updateLocation : EventEmitter<AddressLocation> = new EventEmitter<AddressLocation>();

    /**
     * Converts a Google Places response to a AddressLocation Record
     *
     * @param places
     * @returns {Location}
     */
    private getLocationModelFromPlaces(places : google.maps.places.PlaceResult) : AddressLocation {
        const components = places.address_components,
            city = getAddressComponent(components, 'locality'),
            lat = places.geometry.location.lat().toString(),
            lng = places.geometry.location.lng().toString(),
            name = places.name,
            streetName = getAddressComponent(components, 'route'),
            streetNumber = getAddressComponent(components, 'street_number'),
            state = getAddressComponent(components, 'administrative_area_level_1', true),
            zipCode = getAddressComponent(components, 'postal_code'),
            isFormValid = false,
            additionalInfo = '',
            address2 = '',
            phoneNumber = '';

        return new AddressLocation({
            address1 : streetNumber !== undefined ? `${streetNumber} ${streetName}` : '',
            city,
            latLong : {
                lat,
                lng
            },
            name,
            state,
            zipCode,
            isFormValid,
            additionalInfo,
            address2,
            phoneNumber
        });
    }

    /**
     * Object contains the  pickup location form group
     * @type {FormGroup}
     */
    reservationLocationForm : FormGroup;

    /**
     * location field
     * @type {FormControl}
     */
    location : FormControl;

    /**
     * previousLocations field
     * @type {FormControl}
     */
    previousLocations : FormControl;

    /**
     * store display name used in accelerator
     * @type {boolean}
     */
    showGooglePlaces : boolean = true;

    /**
     * Handles info updates from downstream
     * @param {any} event
     */
    onLocationInfoChange(event : any) {
        const location = this.reservationLocationState.merge(event)
                .set('uuid', '') as AddressLocation;

        this.updateLocation.emit(location);
    }

    /**
     * event handler for location selection event
     * @param {google.maps.places.PlaceResult} [choice] selected location type
     */
    onUpdateLocation(choice? : google.maps.places.PlaceResult) {
        const location : AddressLocation = choice ?
            this.reservationLocationState
                .mergeDeep(this.getLocationModelFromPlaces(choice)).set('isFormValid', true) as AddressLocation :
            new AddressLocation();

        this.updateLocation.emit(location);
    }

    /**
     * Clears out location information
     */
    removeAddress() {
        this.onUpdateLocation();
        if (this.recentLocations.size > 0) {
            this.showGooglePlaces = false;
            this.previousLocations.reset();

            // allow time to set focus
            setTimeout(() => {
                if (this.previous) {
                    this.previous.nativeElement.focus();
                }
            });
        }
        else {
            this.location.reset();

            // allow time to set focus
            setTimeout(() => {
                if (this.googlePlace) {
                    this.googlePlace.nativeElement.focus();
                }
            });
        }
    }

    /**
     * event handler auto-complete accelerator
     * @param {AddressLocation|any|null} edit ReservationAccelerator
     */
    previousChanged(edit : AddressLocation | any)  {
        if (edit !== null) {
            if (edit.size !== undefined) {
                // a selection has been made
                this.updateLocation.emit(edit.set('isFormValid', true));
            }
            else {
                this.showGooglePlaces = true;

                this.location.setValue(edit);

                // allow time to set focus
                setTimeout(() => {
                    if (this.googlePlace) {
                        this.googlePlace.nativeElement.focus();
                    }
                });
            }
        }
    }

    /**
     * event handler autocomplete accelerator
     * @param {any} edit ReservationAccelerator
     */
    googleChanged(edit : any)  {
        if (edit.length === 0 && this.recentLocations.size > 0) {
            this.showGooglePlaces = false;
            this.previousLocations.reset();

            setTimeout(() => {
                if (this.previous) {
                    this.previous.nativeElement.focus();
                }
            });
        }
    }

    /**
     * custom auto-complete display formatter
     * @param {AddressLocation} data
     * @returns {string}
     */
    displayFormatter(data : AddressLocation) : string {
        return data ? this.listFormatter(data) : '';
    }

    /**
     * custom auto-complete list formatter
     * @param {AddressLocation} data
     * @returns {string}
     */
    listFormatter(data : AddressLocation) : string {
        let value : string = '';

        if (data.get('name', '') !== '') {
            value = `${data.get('name')} - `;
        }

        value += data.get('address1');

        if (data.get('address2', '') !== '') {
            value += `, ${data.get('address2')}`;
        }

        if (data.get('city', '') !== '') {
            value += `, ${data.get('city')}, ${data.get('state')} ${data.get('zipCode')}`;
        }

        return value;
    }

    /**
     * event handler for resetting forms
     */
    newFormGroup() {
        return this.builder.group({
            // placeholder for dynamically generated form groups
            location          : this.location,
            previousLocations : this.previousLocations
        });
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        this.location.setValue(this.displayFormatter(this.reservationLocationState));
        if (this.recentLocations.count() > 0 && !this.reservationLocationState.get('isFormValid')) {
            this.showGooglePlaces = false;
        }
        // reset form
        if (changes['resetForm']) {
            if (this.resetForm) {
                this.reservationLocationForm = this.newFormGroup();
            }
        }
    }
}
