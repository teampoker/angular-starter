import {
    Input,
    Component,
    ChangeDetectionStrategy,
    SimpleChanges
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {BeneficiaryConnectionsActions} from '../../../../../store/Beneficiary/actions/beneficiary-connections.actions';
import {BeneficiaryConnectionsState} from '../../../../../store/Beneficiary/types/beneficiary-connections-state.model';
import {ReservationRequestedByState} from '../../../../../store/Reservation/types/reservation-requested-by-state.model';
import {ReservationAccelerator} from '../../../../../store/Reservation/types/reservation-accelerator.model';
import {NameUuid} from '../../../../../store/types/name-uuid.model';
import {Iterable} from 'immutable';

@Component({
    selector        : 'reservation-requested-by',
    templateUrl     : 'reservation-requested-by.component.html',
    styleUrls       : ['reservation-requested-by.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationRequestedByComponent: handles display of reservation status cards title
 */
export class ReservationRequestedByComponent {
    /**
     * ReservationRequestedByComponent constructor
     * @param builder
     * @param reservationActions
     * @param beneficiaryConnectionsActions
     */
    constructor(
        private builder                         : FormBuilder,
        private reservationActions              : ReservationActions,
        private beneficiaryConnectionsActions   : BeneficiaryConnectionsActions
        ) {
        // init form input fields
        this.requestedBy = new FormControl('', Validators.required);

        // build reservation requested by FormControl group
        this.reservationRequestedByForm = this.newFormGroup();
    }

    /**
     * reservation reset form indicator
     */
    @Input() resetForm : boolean = false;

    /**
     * reservation is editing indicator
     */
    @Input() isEditing : boolean;

    /**
     * beneficiary connections panel UI state
     */
    @Input() beneficiaryConnectionsState : BeneficiaryConnectionsState;

    /**
     * beneficiary requested by UI state
     */
    @Input() reservationRequestedByState : ReservationRequestedByState;

    /**
     * Object contains the requested by group
     */
    reservationRequestedByForm : FormGroup;

    /**
     * requestedBy field
     */
    requestedBy : FormControl;

    /**
     * store property name used in accelerator
     */
    valuePropertyName : string = '';

    /**
     * store display name used in accelerator
     */
    displayPropertyName : string = '';

    /**
     * event handler for requested by accelerator selector
     * @param choice ReservationAccelerator
     */
    requestedBySelection(choice : ReservationAccelerator | any)  {
        // make sure that the form has been touched
        // the plugin allows for blank select
        // blank selection happens on init
        // so we need to check to see if it was
        // ment for add new connection
        if (choice !== undefined) {
            if (choice.addAConnection || choice.name === 'PLUS_ADD_A_CONNECTION' || choice === 'PLUS_ADD_A_CONNECTION') {
                // add a connection
                this.beneficiaryConnectionsActions.addBeneficiaryConnection();
                this.reservationActions.connectionRequestIssuedBy('REQUESTED_BY');
            }
            else {
                if (choice.uuid) {
                    const requestedBy = new NameUuid().withMutations(accelerator => accelerator
                        .set('uuid', choice.uuid)
                        .set('name', choice.name)) as NameUuid;

                    // emit updated value
                    this.reservationActions.updateRequestedBy(requestedBy);
                }
            }
        }
    }

    /**
     * Formats the display value of a selected item
     * @param data
     * @returns {string}
     */
    displayFormatter(data : any) : string {
        if (Iterable.isIterable(data)) {
            return this.listFormatter(data);
        }

        return data;
    }

    /**
     * custom ng2-auto-complete list formatter
     * @param data
     * @returns {string}
     */
    listFormatter(data : any) : string {
        let html : string = '';

        html += data[this.displayPropertyName] ? data[this.displayPropertyName] + ' ' : '';
        html += data[this.valuePropertyName] ? '(' + data[this.valuePropertyName] + ')' : '';
        return html;
    }

    /**
     * event handler for resetting forms
     */
    newFormGroup() {
        return this.builder.group({
            // placeholder for dynamically generated form groups
            requestedBy : this.requestedBy
        });
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        // editing change
        if (this.isEditing) {
            this.requestedBy.setValue(
                this.reservationRequestedByState.getIn(['requestedByInfo', 'name']) +
                ' (' + this.reservationRequestedByState.getIn(['requestedByInfo', 'connectionType']) + ')'
            );
        }
        // reset form
        if (changes['resetForm']) {
            if (this.resetForm) {
                this.requestedBySelection({
                    uuid : '',
                    name : ''
                });
                this.reservationRequestedByForm = this.newFormGroup();
            }
        }
    }
}
