import {
    ChangeDetectionStrategy,
    Component,
    Input
} from '@angular/core';
import {List} from 'immutable';
import * as moment from 'moment';

import {MassTransitRoute} from '../../../../../store/types/mass-transit-route.model';
import {AddressLocation} from '../../../../../store/types/address-location.model';

@Component({
    selector : 'reservation-mass-transit-routes',
    templateUrl : 'reservation-mass-transit-routes.component.html',
    styleUrls : ['reservation-mass-transit-routes.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
export class ReservationMassTransitRoutesComponent {
    /**
     * pickup address location
     * @type {AddressLocation}
     */
    @Input() pickupLocation : AddressLocation;

    /**
     * mass transit routes
     * @type {List<MassTransitRoute>}
     */
    @Input() routes : List<MassTransitRoute>;

    /**
     * returns a formatted time for display in the template
     * @param {string} time
     * @returns {string}
     */
    getFormattedTime(time : string) : string {
        return moment(time).format('MM/DD/YYYY HH:mm');
    }

    /**
     * NgFor trackyBy handler
     *
     * @param {number} index
     *
     * @returns {number}
     */
    trackByIndex(index : number) : number {
        return index;
    }
}
