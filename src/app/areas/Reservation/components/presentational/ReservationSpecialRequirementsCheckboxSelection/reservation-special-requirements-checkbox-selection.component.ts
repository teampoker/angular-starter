import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl
} from '@angular/forms';
import {List} from 'immutable';
import {
    AlertItem,
    EnumAlertType
} from '../../../../../store/Navigation/types/alert-item.model';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {NavActions} from '../../../../../store/Navigation/nav.actions';
import {BeneficiarySpecialRequirementsState} from '../../../../../store/Beneficiary/types/beneficiary-special-requirements-state.model';
import {BeneficiarySpecialRequirement} from '../../../../../store/Beneficiary/types/beneficiary-special-requirement.model';
import {createDateValidator} from '../../../../../shared/components/DatePicker/date-picker.component';

@Component({
    selector       : 'reservation-special-requirements-checkbox-selection',
    templateUrl    : 'reservation-special-requirements-checkbox-selection.component.html',
    styleUrls      : ['reservation-special-requirements-checkbox-selection.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationSpecialRequirementsCheckboxSelection: handles display of reservation status cards title
 */
export class ReservationSpecialRequirementsCheckboxSelectionComponent {
    constructor(
        private builder : FormBuilder,
        private navActions : NavActions
    ) {
        this.fromDate = new FormControl('', createDateValidator(this.dateFormat));
        this.thruDate = new FormControl('', createDateValidator(this.dateFormat));

        this.form = builder.group({
            name        : '',
            fromDate    : this.fromDate,
            thruDate    : this.thruDate
        });
    }

    @Input() specialRequirementsList : List<KeyValuePair>;
    @Input() specialRequirementsState : BeneficiarySpecialRequirementsState;

    @Output() addSpecialRequirement : EventEmitter<any> = new EventEmitter();
    @Output() cancel : EventEmitter<any> = new EventEmitter();
    @Output() removeSpecialRequirement : EventEmitter<any> = new EventEmitter();
    @Output() save : EventEmitter<any> = new EventEmitter();
    @Output() setPermanence : EventEmitter<any> = new EventEmitter();
    @Output() updateFromDate : EventEmitter<any> = new EventEmitter();
    @Output() updateThruDate : EventEmitter<any> = new EventEmitter();

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * Whether checkbox selections are valid
     * @type {boolean}
     */
    isValid : boolean = true;

    /**
     * Error message to display if checkbox selections aren't compatible
     * @type {string}
     */
    errorMessage : string;

    /**
     * Object contains the special requirements checkbox selection form group
     * @type {FormGroup}
     */
    form : FormGroup;

    /**
     * fromDate date picker control
     */
    fromDate : FormControl;

    /**
     * thruDate date picker control
     */
    thruDate : FormControl;

    /**
     * check to map checked boxes to active special requirements
     * @param uuid id of special requirement
     */
    isActive(uuid : string) : boolean {
        return this.specialRequirementsState
            .map(requirement => requirement.getIn(['type', 'id']))
            .includes(uuid);
    }

    /**
     * check to find from dates on special requirements, not permanent
     * @param uuid id of special requirement
     */
    getFromDate(uuid : string) : string {
        return this.specialRequirementsState
            .find(requirement => requirement.getIn(['type', 'id']) === uuid)
            .get('fromDate');
    }

    /**
     * check to find thru dates on special requirements, not permanent
     * @param uuid id of special requirement
     */
    getThruDate(uuid : string) : string {
        return this.specialRequirementsState
            .find(requirement => requirement.getIn(['type', 'id']) === uuid)
            .get('thruDate');
    }

    /**
     * check to find which special requirements are permanent, no from/thru dates
     * @param uuid id of special requirement
     */
    isPermanent(uuid : string) : string {
        return this.specialRequirementsState
            .find(requirement => requirement.getIn(['type', 'id']) === uuid)
            .get('permanentRequirement');
    }

    /**
     * event handler for addSpecialRequirement click event
     * @param requirement special requirement that was checked true
     */
    onAddSpecialRequirement(requirement : KeyValuePair) {
        this.addSpecialRequirement.emit(new BeneficiarySpecialRequirement({ type : requirement }));
    }

    /**
     * event handler for onCheckboxClick click event
     * @param requirement special requirement that was checked true
     */
    onCheckboxClick(requirement : KeyValuePair) {
        if (this.isActive(requirement.get('id'))) {
            this.onRemoveSpecialRequirement(requirement);
        }
        else {
            this.onAddSpecialRequirement(requirement);
        }
    }

    /**
     * event handler for removeSpecialRequirement click event
     * @param requirement special requirement that was checked false
     */
    onRemoveSpecialRequirement(requirement : KeyValuePair) {
        this.removeSpecialRequirement.emit(requirement);
    }

    /**
     * event handler for onCancel click event
     */
    onCancel() {
        this.cancel.emit();
    }

    /**
     * if door to door checked, see if remaining selections are compatible
     */
    checkDoorToDoor () : boolean {
        let doorToDoor = true;

        // No Door through Door, Hand to Hand, Stretcher
        this.specialRequirementsState.forEach(req => {
            switch (req.getIn(['type', 'value'])) {
                case 'Door through Door' :
                case 'Hand to Hand' :
                case 'Stretcher' :
                    doorToDoor = false;
                    break;
                default:
                    break;
            }
        });

        return doorToDoor;
    }

    /**
     * if door through door checked, see if remaining selections are compatible
     */
    checkDoorThroughDoor () : boolean {
        let doorThroughDoor = true;

        // No Door to Door, Hand to Hand
        this.specialRequirementsState.forEach(req => {
            switch (req.getIn(['type', 'value'])) {
                case 'Door to Door' :
                case 'Hand to Hand' :
                    doorThroughDoor = false;
                    break;
                default:
                    break;
            }
        });

        return doorThroughDoor;
    }

    /**
     * if hand to hand checked, see if remaining selections are compatible
     */
    checkHandToHand () : boolean {
        let handToHand = true;

        // No Door to Door, Door through Door, Wheelchair, Wheelchair Lift
        this.specialRequirementsState.forEach(req => {
            switch (req.getIn(['type', 'value'])) {
                case 'Door to Door' :
                case 'Door through Door' :
                case 'Wheelchair' :
                case 'Wheelchair Lift' :
                    handToHand = false;
                    break;
                default:
                    break;
            }
        });

        return handToHand;
    }

    /**
     * if stretcher checked, see if remaining selections are compatible
     */
    checkStretcher() : boolean {
        let stretcher = true;

        // No Ambulatory, Door to Door, Hand to Hand, Wheelchair, Wheelchair Lift
        this.specialRequirementsState.forEach(req => {
            switch (req.getIn(['type', 'value'])) {
                case 'Ambulatory' :
                case 'Door to Door' :
                case 'Hand to Hand' :
                case 'Wheelchair' :
                case 'Wheelchair Lift' :
                    stretcher = false;
                    break;
                default:
                    break;
            }
        });

        return stretcher;
    }

    /**
     * if wheelchair checked, see if remaining selections are compatible
     */
    checkWheelchair() : boolean {
        let wheelchairValid = true;

        // No Ambulatory, Hand to Hand, Stretcher
        this.specialRequirementsState.forEach(req => {
            switch (req.getIn(['type', 'value'])) {
                case 'Ambulatory' :
                case 'Hand to Hand' :
                case 'Stretcher' :
                   wheelchairValid = false;
                    break;
                default:
                    break;
            }
        });

        return wheelchairValid;
    }

    /**
     * if wheelchair lift checked, see if remaining selections are compatible
     */
    checkWheelchairLift() : boolean {
        let wheelchairLiftValid = true;

        // No Ambulatory, Hand to Hand, Stretcher
        this.specialRequirementsState.forEach(req => {
            switch (req.getIn(['type', 'value'])) {
                case 'Ambulatory' :
                case 'Hand to Hand' :
                case 'Stretcher' :
                    wheelchairLiftValid = false;
                    break;
                default:
                    break;
            }
        });

        return wheelchairLiftValid;
    }

    /**
     * entire compatability check.
     * break if incompatibility found.
     */
    checkSpecialRequirementCompatibility () {
        this.specialRequirementsState.forEach(req => {
            switch (req.getIn(['type', 'value'])) {
                case 'Door to Door' :
                    if (!this.checkDoorToDoor()) {
                        this.errorMessage = 'Door to Door selected. Can\'t select Door through Door, Hand to Hand, Stretcher as well';
                        return this.isValid = false;
                    }
                    break;

                case 'Door through Door' :
                    if (!this.checkDoorThroughDoor()) {
                        this.errorMessage = 'Door through Door selected. Can\'t select Door to Door, Hand to Hand as well';
                        return this.isValid = false;
                    }
                    break;

                case 'Hand to Hand' :
                    if (!this.checkHandToHand()) {
                        this.errorMessage = 'Hand to Hand selected. Can\'t select Door to Door, Door through Door, Wheelchair, Wheelchair Lift as well';
                        return this.isValid = false;
                    }
                    break;

                case 'Stretcher' :
                    if (!this.checkStretcher()) {
                        this.errorMessage = 'Stretcher selected. Can\'t select Ambulatory, Door to Door, Hand to Hand, Wheelchair, Wheelchair Lift as well';
                        return this.isValid = false;
                    }
                    break;

                case 'Wheelchair' :
                    if (!this.checkWheelchair()) {
                        this.errorMessage = 'Wheelchair selected. Can\'t select Ambulatory, Hand to Hand, Stretcher as well';
                        return this.isValid = false;
                    }
                    break;

                case 'Wheelchair Lift' :
                    if (!this.checkWheelchairLift()) {
                        this.errorMessage = 'Wheelchair Lift selected. Can\'t select Ambulatory, Hand to Hand, Stretcher as well';
                        return this.isValid = false;
                    }
                    break;

                default:
                    return this.isValid = true;
            }
        });
    }

    /**
     * event handler for onSave click event
     */
    onSave(form : any, event : Event) {
        event.preventDefault();

        this.checkSpecialRequirementCompatibility();

        if (this.isValid) {
            this.save.emit();
        }
        else {
            this.navActions.updateAlertMessageState(
                new AlertItem({
                    alertType : EnumAlertType.ERROR,
                    message : this.errorMessage
                })
            );
        }
    }

    /**
     * event handler for onSetPermanence click event
     * @param isPermanent whether it's permanent
     * @param requirement special requirement that was checked true
     */
    onSetPermanence(isPermanent : boolean, requirement : KeyValuePair) {
        this.setPermanence.emit({isPermanent, requirement});
    }

    /**
     * event handler for onUpdateFromDate click event
     * @param date from date to update
     * @param requirement special requirement that was checked true
     */
    onUpdateFromDate(date : string, requirement : KeyValuePair) {
        this.updateFromDate.emit({date, requirement});
    }

    /**
     * event handler for onUpdateThruDate click event
     * @param date thru date to update
     * @param requirement special requirement that was checked true
     */
    onUpdateThruDate(date : string, requirement : KeyValuePair) {
        this.updateThruDate.emit({date, requirement});
    }
}
