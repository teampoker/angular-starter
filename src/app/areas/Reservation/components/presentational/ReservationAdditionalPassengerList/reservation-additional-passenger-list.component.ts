import {
    ChangeDetectionStrategy,
    Component,
    Input
} from '@angular/core';
import {List} from 'immutable';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {ReservationPassengerState} from '../../../../../store/Reservation/types/reservation-passenger';
import {BeneficiarySpecialRequirement} from '../../../../../store/Beneficiary/types/beneficiary-special-requirement.model';
import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';

@Component({
    selector        : 'reservation-additional-passenger-list',
    templateUrl     : 'reservation-additional-passenger-list.component.html',
    styleUrls       : ['reservation-additional-passenger-list.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
export class ReservationAdditionalPassengerListComponent {
    /**
     * ReservationAdditionalPassengerListComponent constructor
     * @param reservationActions
     */
    constructor(private reservationActions : ReservationActions) {}

    /**
     * collection of all passengers tied to the reservation
     */
    @Input() passengerList : List<ReservationPassengerState>;

    /**
     * collection of all possible special requirements
     */
    @Input() specialRequirementsList : List<BeneficiarySpecialRequirement>;

    getHeaderText(passenger : ReservationPassengerState) : string {
        let header = passenger.get('name');
        const connectionType = passenger.get('connectionType');

        if (connectionType) {
            header = `${header} (${connectionType})`;
        }

        return header;
    }

    /**
     * Adds a special requirement to a passenger
     * @param {ReservationPassengerState} passenger
     * @param {BeneficiarySpecialRequirement} requirement
     */
    onAddSpecialRequirement(passenger : ReservationPassengerState, requirement : BeneficiarySpecialRequirement) {
        this.reservationActions.addPassengerTempSpecialRequirement(requirement, passenger);
    }

    /**
     * Initiates a temporary passenger special requirements state
     * @param {KeyValuePair} passenger
     */
    onInitEdit(passenger : KeyValuePair) {
        this.reservationActions.initTempPassengerSpecialRequirementState(passenger);
    }

    /**
     * Removes a passenger
     * @param {ReservationPassengerState} passenger
     */
    onRemove(passenger : ReservationPassengerState) {
        this.reservationActions.removePassenger(passenger);
    }

    /**
     * Removes a special requirement from a passenger
     * @param {ReservationPassengerState} passenger
     * @param {KeyValuePair} requirement
     */
    onRemoveSpecialRequirement(passenger : ReservationPassengerState, requirement : KeyValuePair) {
        this.reservationActions.removeTempPassengerSpecialRequirement(requirement, passenger);
    }

    /**
     * Saves the special requirement changes
     * @param {ReservationPassengerState} passenger
     */
    onSave(passenger : ReservationPassengerState) {
        this.reservationActions.savePassengerSpecialRequirement(passenger);
    }

    /**
     * Updates the from date
     * @param {ReservationPassengerState} passenger
     * @param event
     */
    onUpdateFromDate(passenger : ReservationPassengerState, event : { date : string, requirement : KeyValuePair }) {
        this.reservationActions.updateTempPassengerSpecialRequirementFromDate(event.date, event.requirement, passenger);
    }

    /**
     * Updates the thru date
     * @param {ReservationPassengerState} passenger
     * @param event
     */
    onUpdateThruDate(passenger : ReservationPassengerState, event : { date : string, requirement : KeyValuePair }) {
        this.reservationActions.updateTempPassengerSpecialRequirementThruDate(event.date, event.requirement, passenger);
    }

    /**
     * Sets the special requirement as permanent/temporary
     * @param {ReservationPassengerState} passenger
     * @param event
     */
    onSetPermanence(passenger : ReservationPassengerState, event : { isPermanent : boolean, requirement : KeyValuePair }) {
        this.reservationActions.setTempPassengerSpecialRequirementPermanence(event.isPermanent, event.requirement, passenger);
    }

    /**
     * ngFor tracking function
     * @param {number} index
     * @param {ReservationPassengerState} obj
     * @returns {string}
     */
    trackByIndex(index : number, obj : ReservationPassengerState) : string {
        return obj.get('uuid');
    }
}
