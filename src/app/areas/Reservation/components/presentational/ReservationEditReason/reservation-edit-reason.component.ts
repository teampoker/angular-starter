import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    Output
} from '@angular/core';
import {
    FormControl,
    FormGroup
} from '@angular/forms';
import {List} from 'immutable';

import {NameUuid} from '../../../../../store/types/name-uuid.model';
import {ReservationStateSelectors} from '../../../../../store/Reservation/selectors/reservation.selectors';
import {Subscription} from 'rxjs';

@Component({
    selector : 'reservation-edit-reason',
    templateUrl : 'reservation-edit-reason.component.html',
    styleUrls : ['reservation-edit-reason.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
export class ReservationEditReasonComponent implements OnChanges, OnDestroy {
    /**
     * @param {ReservationStateSelectors} reservationSelectors
     */
    constructor(private reservationSelectors : ReservationStateSelectors) {
        this.commentControl = new FormControl('');

        this.form = new FormGroup({
            comment : this.commentControl
        });

        this.commentSubscription = this.commentControl.valueChanges
            .debounceTime(250)
            .subscribe(comment => this.onSelectReason(this.selectedReason, comment));
    }

    /**
     * comment for the reason given
     * @type {string | null}
     */
    @Input() reasonComment : string | null;

    /**
     * list of possible reasons
     * @type {List<NameUuid>}
     */
    @Input() reasons : List<NameUuid>;

    /**
     * selected reason
     * @type {string | null}
     */
    @Input() selectedReasonUuid : string | null;

    /**
     * emits when a reason is selected or comment changed
     * @type {EventEmitter<{ reason : NameUuid, comment : string }>}
     */
    @Output() selectReason : EventEmitter<{ reason : NameUuid, comment : string }> = new EventEmitter();

    /**
     * subscription for comment value changes
     * @type {Subscription}
     */
    private commentSubscription : Subscription;

    /**
     * the form group
     * @type {FormGroup}
     */
    form : FormGroup;

    /**
     * form control for the comment field
     * @type {FormControl}
     */
    commentControl : FormControl;

    /**
     * the selected reason
     * @type {NameUuid}
     */
    selectedReason : NameUuid;

    /**
     * handles reason selection and comment changes
     * @param {NameUuid} reason
     * @param {string} comment
     */
    onSelectReason(reason : NameUuid, comment : string) {
        this.selectReason.emit({ reason, comment });
    }

    /**
     * ngFor tracking by index
     * @param {number} index
     * @returns {number}
     */
    trackByIndex(index : number) : number {
        return index;
    }

    ngOnChanges() {
        if (this.selectedReasonUuid && this.selectedReasonUuid !== '') {
            this.reservationSelectors.getEditReason(this.selectedReasonUuid)
                .first()
                .subscribe(reason => this.selectedReason = reason);
        }
    }

    ngOnDestroy() {
        this.commentSubscription.unsubscribe();
    }
}
