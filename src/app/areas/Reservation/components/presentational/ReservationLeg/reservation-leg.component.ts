import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnChanges
} from '@angular/core';
import {List} from 'immutable';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {ReservationLegState} from '../../../../../store/Reservation/types/reservation-leg-state.model';
import {MetaDataTypesSelectors} from '../../../../../store/MetaDataTypes/meta-data-types.selectors';

@Component({
    selector        : 'reservation-leg',
    templateUrl     : 'reservation-leg.component.html',
    styleUrls       : ['reservation-leg.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationLegComponent: handles display of reservation status cards title
 */
export class ReservationLegComponent implements OnChanges {
    /**
     * ReservationLegComponent constructor
     * @param {MetaDataTypesSelectors} metaDataSelectors
     * @param {ReservationActions} reservationActions
     */
    constructor(
        private metaDataSelectors : MetaDataTypesSelectors,
        private reservationActions  : ReservationActions
    ) {}

    /**
     * reservation is editing indicator
     */
    @Input() isEditing : boolean;

    /**
     * current reservation leg number
     */
    @Input() reservationLeg : number;

    /**
     * current reservation leg number
     */
    @Input() reservationLegState : List<ReservationLegState>;

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MMMM DD, YYYY';

    /**
     * Name of mode of transportation
     * @type {string}
     */
    modeOfTransportationName : string = '';

    /**
     * Date format to use in date picker
     * @type {string}
     */
    timeFormat : string = 'h:mm a';

    /**
     * update reservation leg being edited
     * @type {number}
     */
    updateEditLeg (leg : number) {
        this.reservationActions.updateEditLeg(leg);
    }

    /**
     * update reservation leg being deleted
     * @type {number}
     */
    updateDeleteLeg (leg : number) {
        this.reservationActions.updateDeleteLeg(leg);
    }

    ngOnChanges() {
        // get the name of the leg's mode of transportation if a uuid is present
        if (this.reservationLegState.getIn([this.reservationLeg, 'modeOfTransportation', 'uuid'], '') !== '') {
            this.metaDataSelectors
                .getModeOfTransportation(this.reservationLegState.getIn([this.reservationLeg, 'modeOfTransportation', 'uuid']))
                .first()
                .subscribe(data => this.modeOfTransportationName = data ? data.get('name') : '');
        }
    }
}
