import {
    Input,
    Component,
    ChangeDetectionStrategy
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import * as moment from 'moment';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {ReservationTripIndicatorState} from '../../../../../store/Reservation/types/reservation-trip-indicator-state.model';
import {ReservationPickupTimeState} from '../../../../../store/Reservation/types/reservation-pickup-time-state.model';
import {ReservationAppointmentDateState} from '../../../../../store/Reservation/types/reservation-appointment-date-state.model';

@Component({
    selector        : 'reservation-trip-indicator',
    templateUrl     : 'reservation-trip-indicator.component.html',
    styleUrls       : ['reservation-trip-indicator.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationTripIndicatorComponent: handles display of reservation status cards title
 */
export class ReservationTripIndicatorComponent {
    /**
     * ReservationTripIndicatorComponent constructor
     * @param builder
     * @param reservationActions
     */
    constructor(
        private builder                : FormBuilder,
        private reservationActions     : ReservationActions
        ) {
        // init form input fields
        this.roundTrip = new FormControl('', Validators.required);
        this.willCall = new FormControl('', undefined);

        // build reservation appointment FormControl group
        this.reservationTripIndicatorForm = builder.group({
            roundTrip : this.roundTrip,
            willCall  : this.willCall
        });
    }

    /**
     * reservation appointment date UI state
     */
    @Input() reservationAppointmentDateState : ReservationAppointmentDateState;

    /**
     * current reservation leg number
     */
    @Input() reservationLeg : number;

    /**
     * reservation pickup time UI state
     */
    @Input() reservationTripIndicatorState : ReservationTripIndicatorState;

    /**
     * reservation pickup time UI state
     */
    @Input() reservationPickupTimeState : ReservationPickupTimeState;

    /**
     * Object contains the pickup time form group
     */
    reservationTripIndicatorForm : FormGroup;

    /**
     * roundTrip field
     */
     roundTrip    : FormControl;

    /**
     * willCall field
     */
     willCall    : FormControl;

    /**
     * Date format to use in date picker
     * @type {string}
     */
    timeFormat : string = 'YYYY-MM-DDTHH:mm:ssZ';

    /**
     * event handler for round trip updater
     * @param choice
     */
    updateRoundTrip(choice : boolean) {
        this.reservationActions.updateRoundTrip(choice);
    }

    /**
     * event handler for will call updater
     * @param choice
     */
    updateWillCall(choice : boolean) {
        this.reservationActions.updateWillCall(!choice);
    }

    /**
     * event handler for time picker
     * @param choice selected time
     */
    onTimeSelected(choice? : string) {
        this.reservationActions.updatePickupTime({
            fieldName   : 'pickupTime',
            fieldValue  :  moment(choice).format(this.timeFormat)
        });
    }

    /**
     * event handler for time picker
     * @param event selected time
     */
    onIsTimeValid(event : boolean) {
        this.reservationActions.updatePickupTimeValid({
            fieldName   : 'isFormValid',
            fieldValue  : event
        });
    }
}
