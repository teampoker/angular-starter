import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnChanges
} from '@angular/core';
import {List} from 'immutable';
import * as moment from 'moment';

import {ReservationLegState} from '../../../../../store/Reservation/types/reservation-leg-state.model';
import {Beneficiary} from '../../../../../store/Beneficiary/types/beneficiary.model';
import {TransportationItemSpecialRequirement} from '../../../../../store/types/transportation-item-special-requirement.model';
import {TransportationItemPassenger} from '../../../../../store/types/transportation-item-passenger.model';
import {MetaDataTypesSelectors} from '../../../../../store/MetaDataTypes/meta-data-types.selectors';

@Component({
    selector : 'reservation-list-leg',
    templateUrl : './reservation-list-leg.component.html',
    styleUrls : ['./reservation-list-leg.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
/**
 * Displays Reservation Leg information
 * @class ReservationListLegComponent
 * @export
 */
export class ReservationListLegComponent implements OnChanges {
    constructor(private metaDataSelectors : MetaDataTypesSelectors) {}

    /**
     * The beneficiary for the leg
     * @type {Beneficiary}
     */
    @Input() beneficiary : Beneficiary;

    /**
     * If we're showing history (scheduled is the alternative)
     * @type {boolean}
     */
    @Input() isHistory : boolean;

    /**
     * The label on the header
     * @type {string}
     */
    @Input() label : string = 'Leg';

    /**
     * The leg to display
     * @type {ReservationLegState}
     */
    @Input() leg : ReservationLegState;

    /**
     * If the additional details should be shown
     * @type {boolean}
     */
    expanded : boolean = false;

    /**
     * Height of the beneficiary
     * @type {string}
     */
    height : string;

    /**
     * Name of mode of transportation
     * @type {string}
     */
    modeOfTransportationName : string = '';

    /**
     * Weight of the beneficiary
     * @type {string}
     */
    weight : string;

    /**
     * Returns a formatted date string
     * @returns {string}
     */
    getDisplayDate() : string {
        return moment(
            this.leg.getIn(['reservationDateTime', 'date']),
            'YYYY-MM-DDTHH:mm:ssZ[UTC]'
        )
            .format('MM/DD/YYYY');
    }

    /**
     * Returns a formatted time string
     * @returns {string}
     */
    getDisplayTime() : string {
        return moment(
            this.leg.get('pickupTime'),
            'YYYY-MM-DDTHH:mm:ssZ[UTC]'
        )
            .format('hh:mm A');
    }

    /**
     * Returns a string representation of passengers
     * @param {List<TransportationItemPassenger>} passengers
     * @returns {string}
     */
    getAdditionalPassengersList(passengers : List<TransportationItemPassenger>) : string {
        return passengers.map(p => `${p.getIn(['person', 'firstName'])} ${p.getIn(['person', 'lastName'])}`).join(', ');
    }

    /**
     * Returns a string representation of passenger special requirements
     * @param {List<TransportationItemPassenger>} passengers
     * @returns {string}
     */
    getPassengerSpecialRequirementsList(passengers : List<TransportationItemPassenger>) : string {
        return passengers
            .map(passenger => passenger.get('passengerSpecialRequirements', List()))
            .flatMap(requirements => requirements
                .map(requirement => requirement.getIn(['specialRequirementType', 'name']))
            )
            .groupBy(requirement => requirement)
            .map(requirementGroup => requirementGroup.first())
            .sort()
            .join(', ');
    }

    /**
     * Returns a string representation of special requirements
     * @param {List<TransportationItemSpecialRequirement>} requirements
     * @returns {string}
     */
    getSpecialRequirementsList(requirements : List<TransportationItemSpecialRequirement>) : string {
        return requirements.map(r => r.getIn(['specialRequirementType', 'name'])).join(', ');
    }

    /**
     * Toggles the display of additional information
     */
    toggleExpanded() {
        this.expanded = !this.expanded;
    }

    ngOnChanges() {
        // get a text representation of the beneficiary's height
        this.height = this.beneficiary
            .get('physicalCharacteristics', List())
            .filter(characteristic => characteristic.getIn(['type', 'value']) === 'Height')
            .map(characteristic => parseInt(characteristic.get('value'), 10))
            .map(inches => {
                if (isNaN(inches)) {
                    return '';
                }
                else {
                    const feet = Math.floor(inches / 12);

                    return `${feet} ft ${inches % 12} in`;
                }
            })
            .first();

        // get a text representation of the beneficiary's weight
        this.weight = this.beneficiary
            .get('physicalCharacteristics', List())
            .filter(characteristic => characteristic.getIn(['type', 'value']) === 'Weight')
            .map(characteristic => `${characteristic.get('value')} lbs`)
            .first();

        // get the name of the leg's mode of transportation if a uuid is present
        if (this.leg.getIn(['modeOfTransportation', 'uuid'], '') !== '') {
            this.metaDataSelectors
                .getModeOfTransportation(this.leg.getIn(['modeOfTransportation', 'uuid']))
                .first()
                .subscribe(data => this.modeOfTransportationName = data ? data.get('name') : '');
        }
    }
}
