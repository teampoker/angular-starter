import {
    Input,
    Component,
    SimpleChanges,
    ChangeDetectionStrategy
} from '@angular/core';
import {List} from 'immutable';

import {NameUuid} from '../../../../../store/types/name-uuid.model';
import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {ReservationBusinessRules} from '../../../../../store/Reservation/types/reservation-business-rules-state.model';

@Component({
    selector        : 'reservation-business-rules',
    templateUrl     : 'reservation-business-rules.component.html',
    styleUrls       : ['reservation-business-rules.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationBuinessRulesComponent: handles validation of business rules
 */
export class ReservationBuinessRulesComponent {
    /**
     * ReservationBuinessRulesComponent constructor
     * @param reservationActions
     */
    constructor(private reservationActions : ReservationActions) {}

    /**
     * reservation business rules state
     */
    @Input() reservationBusinessRules : ReservationBusinessRules;

    /**
     * mileage Distance
     */
    @Input() mileageDistance : number = null;

    /**
     * reservation rule type
     */
    @Input() ruleType : string;

    /**
     * additional passenger UI state has validated
     */
    @Input() escortHasValidated : boolean = false;

    /**
     * pickupTime UI state has validated
     */
    @Input() pickupTimeHasValidated : boolean = false;

    /**
     * treatmentType UI state has validated
     */
    @Input() treatmentTypeHasValidated : boolean = false;

    /**
     * apointmentDateTime UI state has validated
     */
    @Input() apointmentDateTimeHasValidated : boolean = false;

    /**
     * modeOfTransportation UI state has validated
     */
    @Input() modeOfTransportationHasValidated : boolean = false;

    /**
     * reservation override reason
     */
    overrideReason : string;

    /**
     * queueTaskItemTypes
     */
    queueTaskItemTypes : List<NameUuid> = List<NameUuid>();

    /**
     * dispatch action to validate locations for a reservation
     */
    validateEscort() {
        // check the business rules for additional passengers
        this.reservationActions.validateEscort();
    }

    /**
     * dispatch action to validate locations for a reservation
     */
    validateModeOfTransportation() {
        // check the business rules for MOT
        this.reservationActions.validateModeOfTransportation();
    }

    /**
     * dispatch action to validate locations for a reservation
     */
    validateLocation() {
        // check the business rules for location
        this.reservationActions.validateLocation();
    }

    /**
     * dispatch action to validate treatments type for a reservation
     */
    validateTreatmentType() {
        // check the business rules for treatment type
        this.reservationActions.validateTreatmentType();
    }

    /**
     * dispatch action to get reservation pickup time
     */
    getReservationPickupTime() {
        this.reservationActions.getReservationPickupTime();
    }

    /**
     * event handler for Override Reason selection event
     * @param choice
     * @param index
     */
    updateOverrideReason(choice : NameUuid, index : number) {
        switch (this.ruleType) {
            case 'location' :
                this.reservationActions.updateLocationOverrideReason(choice, index);
            break;
            case 'treatmentType' :
                this.reservationActions.updateTreatmentTypeOverrideReason(choice, index);
            break;
            case 'escort' :
                this.reservationActions.updateEscortOverrideReason(choice, index);
            break;
            case 'modeOfTransportation' :
                this.reservationActions.updateModeOfTransportationOverrideReason(choice, index);
            break;
            default:
        }
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        this.overrideReason = this.ruleType + 'OverrideReason';
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes object whose keys are property names and values are instances of SimpleChange
     */
    ngOnChanges(changes : SimpleChanges) {
        /********************************************************
         * RULES ************************************************
        *********************************************************/
        // validateTreatmentType
        if (changes['treatmentTypeHasValidated'] || changes['apointmentDateTimeHasValidated']) {
            if (this.treatmentTypeHasValidated && this.apointmentDateTimeHasValidated) {
                this.validateTreatmentType();
            }
            else {
                // TODO
                // clear TreatmentType
            }
        }
        // validateLocation
        if (changes['mileageDistance']) {
            if (this.mileageDistance) {
                this.validateLocation();
            }
            else {
                // TODO
                // clear Location
            }
        }
        // validateEscort
        if (changes['escortHasValidated']) {
            // TODO
            // add a check to see if there is no passenger...
            // this may be valid but not need to run
            if (this.escortHasValidated && this.mileageDistance) {
                this.validateEscort();
            }
            else {
                // TODO
                // clear Escort
            }
        }
        // validateModeOfTransportation
        if (changes['pickupTimeHasValidated']) {
            if (this.pickupTimeHasValidated) {
                this.validateModeOfTransportation();
            }
            else {
                // TODO
                // clear ModeOfTransportation
            }
        }

        /********************************************************
         * TRIGGERS *********************************************
        *********************************************************/
        // getReservationPickupTime
        if (changes['modeOfTransportationHasValidated']) {
            if (this.modeOfTransportationHasValidated) {
                this.getReservationPickupTime();
            }
        }

        // junk for UX
        // so we are only EVER gonna have 1 reason
        // as an object but the UX wants it in a
        // drop down list so... here it is
        if (this.reservationBusinessRules) {
            this.queueTaskItemTypes.set(0, new NameUuid({
                name : this.reservationBusinessRules.getIn([this.ruleType, 'queueTaskItemType', 'name']),
                uuid : this.reservationBusinessRules.getIn([this.ruleType, 'queueTaskItemType', 'uuid'])
            }));
        }
    }
}
