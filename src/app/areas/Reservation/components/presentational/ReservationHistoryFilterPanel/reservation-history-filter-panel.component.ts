import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs';
import {
    List,
    Map
} from 'immutable';

import {ReservationHistoryState} from '../../../../../store/Reservation/types/reservation-history-state.model';
import {DateRange} from '../../../../../store/types/date-range.model';
import {ServiceType} from '../../../../../store/MetaDataTypes/types/service-type.model';
import {ReservationStatus} from '../../../../../store/Reservation/types/reservation-status.model';
import {createDateValidator} from '../../../../../shared/components/DatePicker/date-picker.component';

/**
 * Displays the filter bar for the Reservation History List
 * @export
 * @class ReservationHistoryFilterPanelComponent
 */
@Component({
    selector        : 'reservation-history-filter-panel',
    templateUrl     : 'reservation-history-filter-panel.component.html',
    styleUrls       : ['reservation-history-filter-panel.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
export class ReservationHistoryFilterPanelComponent implements OnChanges, OnDestroy, OnInit {
    /**
     * ReservationHistoryFilterPanelComponent constructor
     */
    constructor() {
        /**
         * setup datepicker form controls
         * @type {FormControl}
         */
        this.fromDate   = new FormControl('', createDateValidator(this.dateFormat));
        this.thruDate   = new FormControl('', createDateValidator(this.dateFormat));

        this.fromDate.valueChanges.subscribe((value : string) => {
            // emit updated value
            const range = this.historyState
                .getIn(['filters', 'dateRange'])
                .set('fromDate', value);

            this.changeDateFilter.emit(range);
        });

        this.thruDate.valueChanges.subscribe((value : string) => {
            // emit updated value
            const range = this.historyState
                .getIn(['filters', 'dateRange'])
                .set('thruDate', value);

            this.changeDateFilter.emit(range);
        });
    }

    /**
     * Current Reservation History State
     * @type {ReservationHistoryState}
     */
    @Input() historyState : ReservationHistoryState;

    /**
     * If we're showing history (scheduled is the alternative)
     * @type {boolean}
     */
    @Input() isHistory : boolean;

    /**
     * Collection of treatment type options
     * @type {Map<string, ReservationStatus>}
     */
    @Input() statuses : Map<string, ReservationStatus>;

    /**
     * Collection of treatment type options
     * @type {List<ServiceType>}
     */
    @Input() treatmentTypes : List<ServiceType> = List<ServiceType>();

    /**
     * List of trip types
     * @type {List<string>}
     */
    @Input() tripTypes : List<string>;

    /**
     * Emits when the date filter has changed
     * @type {EventEmitter<DateRange>}
     */
    @Output() changeDateFilter : EventEmitter<DateRange> = new EventEmitter();

    /**
     * Emits new item per page value
     * @type {EventEmitter<number>}
     */
    @Output() changeItemsPerPage : EventEmitter<number> = new EventEmitter();

    /**
     * Emits new text filter value
     * @type {EventEmitter<string>}
     */
    @Output() changeTextFilter : EventEmitter<string> = new EventEmitter();

    /**
     * Emits when the status filter is cleared
     * @type {EventEmitter<undefined>}
     */
    @Output() clearStatusFilter : EventEmitter<undefined> = new EventEmitter();

    /**
     * Emits when the treatment type filter is cleared
     * @type {EventEmitter<undefined>}
     */
    @Output() clearTreatmentTypeFilter : EventEmitter<undefined> = new EventEmitter();

    /**
     * Emits when the trip type filter is cleared
     * @type {EventEmitter<undefined>}
     */
    @Output() clearTripTypeFilter : EventEmitter<undefined> = new EventEmitter();

    /**
     * Emits when a status is toggled
     * @type {EventEmitter}
     */
    @Output() toggleStatusFilterValue : EventEmitter<{ type : ReservationStatus, active : boolean }> = new EventEmitter();

    /**
     * Emits when a treatment type is toggled
     * @type {EventEmitter}
     */
    @Output() toggleTreatmentTypeFilterValue : EventEmitter<{ type : ServiceType, active : boolean }> = new EventEmitter();

    /**
     * Emits when a trip type is toggled
     * @type {EventEmitter}
     */
    @Output() toggleTripTypeFilterValue : EventEmitter<{ type : string, active : boolean }> = new EventEmitter();

    /**
     * Text filter subscription
     */
    private textFilterSubscription : Subscription;

    /**
     * Handles change events for the text filter control
     * @param {string} value
     */
    private handleTextFilterChange(value : string) {
        this.changeTextFilter.emit(value);
    }

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * If the date picker is open
     *
     * Note:
     * normally, this would be placed in the store, but since the datepicker only reacts to mouse events
     * we want to make sure it's not persisted anywhere on reload
     *
     * @type {boolean}
     */
    isDatePickerOpen : boolean = false;

    /**
     * An alphabetically sorted collection of treatment types
     * @type {List<ServiceType>}
     */
    sortedTreatmentTypes : List<ServiceType> = List<ServiceType>();

    /**
     * FormControl for text filter
     * @type {FormControl}
     */
    textFilter : FormControl;

    /**
     * fromDate datepicker control
     */
    fromDate : FormControl;

    /**
     * thruDate datepicker control
     */
    thruDate : FormControl;

    /**
     * Handles change events for items per page
     * @param {number} numberOfItems
     */
    changePageSize(numberOfItems : number) {
        this.changeItemsPerPage.emit(numberOfItems);
    }

    /**
     * Clears the date range filter
     */
    clearDateRangeFilter() {
        this.changeDateFilter.emit(new DateRange());
    }

    /**
     * Returns true if a date filter is set
     * @returns {boolean}
     */
    isFilteredByDate() : boolean {
        return this.historyState.getIn(['filters', 'dateRange', 'fromDate'], '') !== '' ||
            this.historyState.getIn(['filters', 'dateRange', 'thruDate'], '') !== '';
    }

    /**
     * Returns true if a treatment type is selected
     * @returns {boolean}
     */
    isFilteredByTreatmentType() : boolean {
        return this.historyState.getIn(['filters', 'treatmentType'], Map()).filter(active => active).count() !== 0;
    }

    /**
     * Returns true if a trip status is selected
     * @returns {boolean}
     */
    isFilteredByTripStatus() : boolean {
        return this.historyState.getIn(['filters', 'tripStatus'], Map()).filter(active => active).count() !== 0;
    }

    /**
     * Returns true if a trip type is selected
     * @returns {boolean}
     */
    isFilteredByTripType() : boolean {
        return this.historyState.getIn(['filters', 'tripType'], Map()).filter(active => active).count() !== 0;
    }

    /**
     * Handles clearing the status filter
     */
    onClearStatusFilter() {
        this.clearStatusFilter.emit();
    }

    /**
     * Handles clearing the treatment type filter
     */
    onClearTreatmentTypeFilter() {
        this.clearTreatmentTypeFilter.emit();
    }

    /**
     * Handles clearing the trip status filter
     */
    onClearTripTypeFilter() {
        this.clearTripTypeFilter.emit();
    }

    /**
     * Handles date picker open state changes
     * @param {boolean} isOpen
     */
    onSetDatePickerIsOpen(isOpen : boolean) {
        this.isDatePickerOpen = isOpen;
    }

    /**
     * Handles status checkbox clicking
     * @param {ReservationStatus} status
     */
    onStatusClick(status : ReservationStatus) {
        const currentValue = this.historyState
            .getIn(['filters', 'tripStatus', status.get('id')], false);

        this.toggleStatusFilterValue.emit({ type : status, active : !currentValue });
    }

    /**
     * Handles treatment type checkbox clicking
     * @param {ServiceType} treatment
     */
    onTreatmentTypeClick(treatment : ServiceType) {
        const currentValue = this.historyState
            .getIn(['filters', 'treatmentType', treatment.get('uuid')], false);

        this.toggleTreatmentTypeFilterValue.emit({ type : treatment, active : !currentValue });
    }

    /**
     * Handles trip type checkbox clicking
     * @param {string} type
     */
    onTripTypeClick(type : string) {
        const currentValue = this.historyState
            .getIn(['filters', 'tripType', type], false);

        this.toggleTripTypeFilterValue.emit({ type, active : !currentValue });
    }

    /**
     * Super sophisticated trackBy function
     * @param value
     * @returns {number}
     */
    trackByValue(value : number) {
        return value;
    }

    ngOnChanges() {
        this.sortedTreatmentTypes = this.treatmentTypes.sortBy(name => name) as List<ServiceType>;
    }

    ngOnDestroy() {
        this.textFilterSubscription.unsubscribe();
    }

    ngOnInit() {
        this.textFilter = new FormControl(this.historyState.getIn(['filters', 'text']));

        this.textFilterSubscription = this.textFilter.valueChanges
            .subscribe(value => this.handleTextFilterChange(value));
    }
}
