import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup
} from '@angular/forms';
import {Subscription} from 'rxjs';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {AddressLocation} from '../../../../../store/types/address-location.model';
import {formatPhoneForApi, PHONE_MASK_REGEX} from '../../../../../store/utils/parse-phone-numbers';

@Component({
    selector        : 'reservation-location-info',
    templateUrl     : 'reservation-location-info.component.html',
    styleUrls       : ['reservation-location-info.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
export class ReservationLocationInfoComponent implements OnDestroy, OnInit {
    /**
     * ReservationBuinessRulesComponent constructor
     * @param builder
     * @param reservationActions
     */
    constructor(
        private builder                : FormBuilder,
        private reservationActions     : ReservationActions
    ) {}

    /**
     * The state of the location
     */
    @Input() reservationLocationState : AddressLocation;

    /**
     * Send change events upstream
     */
    @Output() onChange : EventEmitter<any> = new EventEmitter();

    /**
     * Send component close events upstream
     */
    @Output() onClose : EventEmitter<any> = new EventEmitter();

    /**
     * additional information input field
     * @type {FormControl}
     */
    additionalInfo : FormControl;

    /**
     * address 2 field
     * @type {FormControl}
     */
    address2 : FormControl;

    /**
     * location info form
     * @type {FormGroup}
     */
    form : FormGroup;

    /**
     * phoneMaskRegex : regex for phone number mask
     * @type {Array<any>}
     */
    phoneMaskRegex : Array<any> = PHONE_MASK_REGEX;

    /**
     * phone number field
     * @type {FormControl}
     */
    phoneNumber : FormControl;

    /**
     * Subscriptions active in this component
     * @type {Array}
     */
    subscriptions : Array<Subscription> = [];

    /**
     * Handle closing of the component
     */
    close() {
        this.onClose.emit();
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        this.additionalInfo = new FormControl('');
        this.address2 = new FormControl('');
        this.phoneNumber = new FormControl('');

        this.form = this.builder.group({
            additionalInfo : this.additionalInfo,
            address2       : this.address2,
            phoneNumber    : this.phoneNumber
        });

        this.subscriptions.push(this.form.valueChanges
            .debounceTime(500)
            .subscribe(value => {
                value.phoneNumber = formatPhoneForApi(value.phoneNumber);
                this.onChange.emit(value);
            }));
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
}
