import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output
} from '@angular/core';
import {List} from 'immutable';
import * as moment from 'moment';

import {Reservation} from '../../../../../store/Reservation/types/reservation.model';
import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {Beneficiary} from '../../../../../store/Beneficiary/types/beneficiary.model';
import {
    ReservationHistoryState,
    SortOptions
} from '../../../../../store/Reservation/types/reservation-history-state.model';
import {ReservationLegState} from '../../../../../store/Reservation/types/reservation-leg-state.model';
import {
    getStatusColor,
    showDeniedReason
} from '../../../../../store/utils/get-reservation-status-info';

@Component({
    selector : 'reservation-list',
    templateUrl : 'reservation-list.component.html',
    styleUrls : ['reservation-list.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
/**
 * Displays a list of reservations
 * @class ReservationListComponent
 * @export
 */
export class ReservationListComponent {
    /**
     * @param reservationActions
     */
    constructor(private reservationActions : ReservationActions) {}

    /**
     * The beneficiary for which reservations are being shown
     * @type {Beneficiary}
     */
    @Input() beneficiary : Beneficiary;

    /**
     * Current Reservation History State
     * @type {ReservationHistoryState}
     */
    @Input() historyState : ReservationHistoryState;

    /**
     * If we're showing history (scheduled is the alternative)
     * @type {boolean}
     */
    @Input() isHistory : boolean;

    /**
     * The reservations to list
     * @type {List<Reservation>}
     */
    @Input() reservations : List<Reservation>;

    /**
     * Emits an event to change the sort option
     * @type {EventEmitter}
     */
    @Output() setSortOptions : EventEmitter<SortOptions> = new EventEmitter();

    /**
     * A hash map of expanded/not expanded elements
     * @type {any}
     */
    expanded : { [ key : string ] : boolean } = {};

    /**
     * the reservation that is selected to be cancelled
     * @type {Reservation}
     */
    reservationToCancel : Reservation;

    /**
     * Expose SortOptions to template
     * Note: This feels hacky, but it was the first thing that came to mind.
     * @type {SortOptions}
     */
    sortOptions : any = SortOptions;

    /**
     * starts the cancellation confirmation workflow
     * @param {Reservation} reservation
     */
    cancelReservation(reservation : Reservation) {
        this.reservationToCancel = reservation;
    }

    /**
     * executes cancellation API workflow
     * @param {{ eventReasonUuid : string, eventComment : string }} reason
     */
    confirmCancelReservation(reason : { eventReasonUuid : string, eventComment : string }) {
        this.reservationActions.cancelReservation(this.reservationToCancel, reason.eventReasonUuid, reason.eventComment);
        this.reservationToCancel = undefined;
    }

    /**
     * cancels the cancellation workflow
     */
    cancelReservationCancellation() {
        this.reservationToCancel = undefined;
    }

    /**
     * Toggles the expansion of a reservation element
     * @param {Reservation} reservation
     */
    toggleExpandReservation(reservation : Reservation) {
        this.expanded[reservation.get('uuid')] = !this.expanded[reservation.get('uuid')];
        this.reservationActions.fetchReservation(reservation.get('uuid'));
    }

    /**
     * Returns a formatted date string
     * @param reservation
     * @returns {string}
     */
    getDateDisplay(reservation : Reservation) : string {
        return moment(
            reservation.getIn(['legs', 0, 'reservationDateTime', 'date']),
            'YYYY-MM-DDTHH:mm:ssZ[UTC]'
        )
            .format('MM/DD/YYYY');
    }

    /**
     * Returns a sorted list of legs
     * @param {Reservation} reservation
     * @returns {List<ReservationLegState>}
     */
    getSortedLegs(reservation : Reservation) : List<ReservationLegState> {
        return reservation.get('legs').sortBy(leg => leg.get('ordinality'));
    }

    /**
     * Returns a formatted time string
     * @param reservation
     * @returns {string}
     */
    getTimeDisplay(reservation : Reservation) : string {
        return moment(
            reservation.getIn(['legs', 0, 'reservationDateTime', 'date']),
            'YYYY-MM-DDTHH:mm:ssZ[UTC]'
        )
            .format('hh:mm A');
    }

    /**
     * Event handler for changing sort option
     * @param {SortOptions} option
     */
    onSetSortOptions(option : SortOptions) {
        this.setSortOptions.emit(option);
    }

    /**
     * Tracks by uuid
     * @param index
     * @param reservation
     * @returns {string}
     */
    trackByUuid(index : number, reservation : Reservation) : string {
        return reservation.get('uuid');
    }

    /**
     * Returns color for status
     * @returns {string}
     */
     getStatusColor(statusId : number) {
         return getStatusColor(statusId);
     }

    /**
     * Returns boolean used to show denied reaons
     * @returns {boolean}
     */
     showDeniedReason (statusId : number) {
         return showDeniedReason(statusId);
     }
}
