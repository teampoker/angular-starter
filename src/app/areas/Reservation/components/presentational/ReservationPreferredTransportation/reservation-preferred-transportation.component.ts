import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';

import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';
import {ReservationPreferredTransportationState} from '../../../../../store/Reservation/types/reservation-preferred-transportation-state.model';
import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';

@Component({
    selector        : 'reservation-preferred-transportation',
    templateUrl     : 'reservation-preferred-transportation.component.html',
    styleUrls       : ['reservation-preferred-transportation.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationPreferredTransportationComponent: handles display of reservation status cards title
 */
export class ReservationPreferredTransportationComponent {
    /**
     * ReservationPreferredTransportationComponent constructor
     */
    constructor(
        private builder             : FormBuilder,
        private reservationActions  : ReservationActions
        ) {
        // init form input fields
        this.transportationProvider = new FormControl('', Validators.required);

        // build Beneficiary Information FormControl group
        this.reservationPreferredTransportationForm = builder.group({
            // placeholder for dynamically generated form groups
            transportationProvider : this.transportationProvider
        });
    }

    /**
     * reservation preferred transportation provider UI state
     */
    @Input() reservationPreferredTransportationState : ReservationPreferredTransportationState;

    /**
     * Object contains the  preferred transportation provider form group
     */
    reservationPreferredTransportationForm : FormGroup;

    /**
     * transportationProvider field
     */
    transportationProvider : FormControl;

    /**
     * store property name used in accelerator
     */
    valuePropertyName : string = '';

    /**
     * store display name used in accelerator
     */
    displayPropertyName : string = '';

    /**
     * temp array until metaDataTypes are ready
     */
    transportationProviders : Array<{ id : string, value : string }> = [{
        id    : '(976) 535-5819',
        value : '1st CHOICE AMBULETTE'
    }, {
        id    : '(854) 654-2875',
        value : '3210 Webster Ave Prestiage Car Service'
    }, {
        id    : '(753) 951-3449',
        value : '7 OCEAN EXPRESS (Bay Transportation)'
    }, {
        id    : '(594) 789-2715',
        value : '811 Transit Corp.'
    }, {
        id    : '(439) 678-8585',
        value : 'A Class 1 s Transportation Corp'
    }];

    /**
     * custom ng2-auto-complete list formatter
     * @param data
     * @returns {string}
     */
    listFormatter(data : any) : string {
        let html : string = '';

        html += data[this.displayPropertyName] ? data[this.displayPropertyName] + ' ' : '';
        html += data[this.valuePropertyName] ? '' : '';
        return html;
    }

    /**
     * event handler for preferred transportation provider selection event
     * @param choice selected transportationProvider type
     */
    updatePreferredTransportationProvider(choice : KeyValuePair) {
        if (choice.value !== undefined) {
            this.reservationActions.updatePreferredTransportationProvider({
                fieldName   : choice.value,
                fieldValue  : choice.id
            });
        }
    }

    /**
     * event handler to clear preferred transportation provider
     */
    clearTransportationProvider() {
        this.reservationActions.clearTransportationProvider();
    }
}
