import {
    Component,
    Input,
    ChangeDetectionStrategy,
    SimpleChanges
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {ReservationTreatmentTypeState} from '../../../../../store/Reservation/types/reservation-treatment-type-state.model';
import {NameUuid} from '../../../../../store/types/name-uuid.model';

@Component({
    selector        : 'reservation-treatment-type',
    templateUrl     : 'reservation-treatment-type.component.html',
    styleUrls       : ['reservation-treatment-type.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationTreatmentTypeComponent: handles display of reservation status cards title
 */
export class ReservationTreatmentTypeComponent {
    /**
     * ReservationTreatmentTypeComponent constructor
     */
    constructor(
        private builder             : FormBuilder,
        private reservationActions  : ReservationActions
        ) {
        // init form input fields
        this.treatmentType = new FormControl('', Validators.required);

        // build Beneficiary Information FormControl group
        this.reservationTreatmentTypeForm = this.newFormGroup();
    }

    /**
     * reservation reset form indicator
     */
    @Input() resetForm : boolean = false;

    /**
     * reservation is editing indicator
     */
    @Input() isEditing : boolean;

    /**
     * reservation treatment type UI state
     */
    @Input() reservationTreatmentTypeState : ReservationTreatmentTypeState;

    /**
     * Object contains the  treatment type form group
     */
    reservationTreatmentTypeForm : FormGroup;

    /**
     * store display name used in accelerator
     */
    displayPropertyName : string = '';

    /**
     * treatmentType field
     */
    treatmentType : FormControl;

    /**
     * event handler for treatment type selection event
     * @param choice selected treatmentType type
     */
    updateTreatmentType(choice : NameUuid) {
        if (choice.uuid !== undefined) {
            this.reservationActions.updateTreatmentType(choice);
        }
    }

    /**
     * custom ng2-auto-complete list formatter
     * @param data
     * @returns {string}
     */
    listFormatter(data : any) : string {
        let html : string = '';

        html += data[this.displayPropertyName] ? data[this.displayPropertyName] + ' ' : '';
        return html;
    }

    /**
     * event handler for resetting forms
     */
    newFormGroup() {
        return this.builder.group({
            // placeholder for dynamically generated form groups
            treatmentType : this.treatmentType
        });
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        // editing change
        if (this.isEditing) {
            this.treatmentType.setValue(this.reservationTreatmentTypeState.getIn(['treatmentType', 'name']));
        }
        // reset form
        if (changes['resetForm']) {
            if (this.resetForm) {
                this.reservationTreatmentTypeForm = this.newFormGroup();
            }
        }
    }
}
