import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {
    Iterable,
    List
} from 'immutable';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {BeneficiaryConnectionsActions} from '../../../../../store/Beneficiary/actions/beneficiary-connections.actions';
import {BeneficiaryConnectionsState} from '../../../../../store/Beneficiary/types/beneficiary-connections-state.model';
import {ReservationAdditionalPassengersState} from '../../../../../store/Reservation/types/reservation-additional-passengers-state.model';
import {ReservationAccelerator} from '../../../../../store/Reservation/types/reservation-accelerator.model';
import {ReservationPassengerState} from '../../../../../store/Reservation/types/reservation-passenger';
import {BeneficiarySpecialRequirement} from '../../../../../store/Beneficiary/types/beneficiary-special-requirement.model';

@Component({
    selector        : 'reservation-additional-passenger',
    templateUrl     : 'reservation-additional-passenger.component.html',
    styleUrls       : ['reservation-additional-passenger.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationAdditionalPassengerComponent: handles display of reservation status cards title
 */
export class ReservationAdditionalPassengerComponent {
    /**
     * reservation is editing indicator
     */
    @Input() isEditing : boolean;

    /**
     * boolean enabled/disabled indicator
     */
    @Input() disabled : boolean = false;

    /**
     * beneficiary connections panel UI state
     */
    @Input() beneficiaryConnectionsState : BeneficiaryConnectionsState;

    /**
     * reservation additional passengers UI state
     */
    @Input() reservationAdditionalPassengersState : ReservationAdditionalPassengersState;

    /**
     * collection of all possible special requirements
     */
    @Input() specialRequirementsList : List<BeneficiarySpecialRequirement>;

    /**
     * ReservationAdditionalPassengerComponent constructor
     *
     * @param builder
     * @param reservationActions
     * @param beneficiaryConnectionsActions
     */
    constructor(
        private builder                         : FormBuilder,
        private reservationActions              : ReservationActions,
        private beneficiaryConnectionsActions   : BeneficiaryConnectionsActions
    ) {
        // init form input fields
        this.additionalPassenger = new FormControl('', Validators.required);
        this.additionalPassengers = new FormControl('');

        // build reservation additional passengers FormControl group
        this.reservationAdditionalPassengersForm = builder.group({
            // placeholder for dynamically generated form groups
            additionalPassenger  : this.additionalPassenger,
            additionalPassengers : this.additionalPassengers
        });
    }

    /**
     * Object contains the  additional passengers form group
     */
    reservationAdditionalPassengersForm : FormGroup;

    /**
     * additionalPassenger field
     */
    additionalPassenger : FormControl;

    /**
     * additionalPassengers field
     */
    additionalPassengers : FormControl;

    /**
     * store property name used in accelerator
     */
    valuePropertyName : string = '';

    /**
     * store display name used in accelerator
     */
    displayPropertyName : string = '';

    /**
     * event handler for additional passengers selection event
     * @param choice boolean additional passengers?
     */
    updateAdditionalPassengers(choice : boolean) {
        this.reservationActions.updateAdditionalPassengers({
            fieldName   : 'additionalPassengers',
            fieldValue  : choice
        });
    }

    /**
     * event handler for additional passenger accelerator selector
     * @param choice ReservationAccelerator
     */
    additionalPassengerSelection(choice : ReservationAccelerator | any)  {
        // make sure that the form has been touched
        // the plugin allows for blank select
        // blank selection happens on init
        // so we need to check to see if it was
        // meant for add new connection
        if (choice !== undefined) {
            if (choice.addAConnection || choice.name === 'PLUS_ADD_A_CONNECTION' || choice === 'PLUS_ADD_A_CONNECTION') {
                // add a connection
                this.beneficiaryConnectionsActions.addBeneficiaryConnection();
                this.reservationActions.connectionRequestIssuedBy('PASSENGER');
            }
            else {
                if (choice.uuid) {
                    const additionalPassenger = new ReservationPassengerState({
                        uuid : choice.uuid,
                        name : choice.name,
                        specialRequirements : List<BeneficiarySpecialRequirement>(),
                        connectionType : choice.connectionType
                    });

                    // emit updated value
                    this.reservationActions.updatePassenger(additionalPassenger);
                }
            }
        }
    }

    /**
     * Formats the display of a selected value
     * @param data
     * @returns {string}
     */
    displayFormatter(data : any) : string {
        if (Iterable.isIterable(data)) {
            return this.listFormatter(data);
        }

        return data;
    }

    /**
     * custom ng2-auto-complete list formatter
     * @param data
     * @returns {string}
     */
    listFormatter(data : any) : string {
        let html : string = '';

        html += data.get(this.displayPropertyName) ? data.get(this.displayPropertyName) + ' ' : '';
        html += data.get(this.valuePropertyName) ? '(' + data.get(this.valuePropertyName) + ')' : '';

        return html;
    }
}
