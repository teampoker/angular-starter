import {
    Input,
    Component,
    ChangeDetectionStrategy,
    SimpleChanges
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import * as moment from 'moment';

import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {ReservationPickupTimeState} from '../../../../../store/Reservation/types/reservation-pickup-time-state.model';
import {ReservationAppointmentDateState} from '../../../../../store/Reservation/types/reservation-appointment-date-state.model';
import {createTimeValidator} from '../../../../../shared/components/TimePicker/time-picker.component';

@Component({
    selector        : 'reservation-pickup-time',
    templateUrl     : 'reservation-pickup-time.component.html',
    styleUrls       : ['reservation-pickup-time.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationPickupTimeComponent: handles display of reservation status cards title
 */
export class ReservationPickupTimeComponent {
    /**
     * ReservationPickupTimeComponent constructor
     */
    constructor(
        private builder                : FormBuilder,
        private reservationActions     : ReservationActions
    ) {
        // init form input fields
        this.pickupTime = new FormControl('', Validators.compose([
            Validators.required,
            createTimeValidator(this.timeFormat)
        ]));

        // build reservation appointment FormControl group
        this.reservationPickupTimeForm = builder.group({
            pickupTime : this.pickupTime
        });

        // subscribe to pickup time updates
        this.pickupTime.valueChanges.subscribe(value => {
            if (value) {
                if (value !== 'Invalid date') {
                    // pull appointmentDate and pickupTime into moment
                    const apptDate   : any = moment(this.reservationAppointmentDateState.get('appointmentDate')),
                          pickupTime : any = moment(value, this.timeFormat);

                    // store newly selected pickupTime
                    this.selectedPickupTime = value;

                    // create raw moment object to work with
                    const dateTime : any = moment();

                    // configure new dateTime moment with appointmentDate / pickupTime values
                    dateTime.year(apptDate.year()).month(apptDate.month()).date(apptDate.date()).hour(pickupTime.hour()).minute(pickupTime.minute());

                    // convert to ISO string so API doesn't detonate
                    this.reservationActions.updatePickupTime({
                        fieldName   : 'pickupTime',
                        fieldValue  : dateTime
                    });

                    this.reservationActions.updatePickupTimeValid({
                        fieldName   : 'isFormValid',
                        fieldValue  : true
                    });
                }
                else {
                    this.reservationActions.updatePickupTimeValid({
                        fieldName   : 'isFormValid',
                        fieldValue  : false
                    });
                }
            }
        });
    }

    /**
     * time picker boolean used to determine disabled instance
     */
    @Input() disabled : boolean = false;

    /**
     * reservation reset form indicator
     */
    @Input() resetForm : boolean = false;

    /**
     * reservation pickup time UI state
     */
    @Input() reservationPickupTimeState : ReservationPickupTimeState;

    /**
     * reservation appointment date UI state
     */
    @Input() reservationAppointmentDateState : ReservationAppointmentDateState;

    /**
     * Object contains the pickup time form group
     */
    reservationPickupTimeForm : FormGroup;

    /**
     * pickupTime field
     */
    pickupTime : FormControl;

    /**
     * selected pickup time
     */
    selectedPickupTime : string;

    /**
     * time picker hour step
     */
    hourStep : number = 1;

    /**
     * time picker minute step
     */
    minuteStep : number = 10;

    /**
     * time picker boolean used to determine hide/show of am/pm
     */
    showMeridiem : boolean = true;

    /**
     * Date format to use in date picker
     * @type {string}
     */
    timeFormat : string = 'YYYY-MM-DDTHH:mm:ssZ';

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        // changes.prop contains the old and the new value...
        if (changes['reservationPickupTimeState']) {
            // if not valid previous value then component has just initialized
            if (
                changes['reservationPickupTimeState'].currentValue &&
                changes['reservationPickupTimeState'].currentValue instanceof ReservationPickupTimeState
            ) {
                const pickupTime : string = this.reservationPickupTimeState.get('pickupTime'),
                      pickupDate : string = this.reservationAppointmentDateState.get('appointmentDate');

                // if selectedPickupTime isn't already populated...
                if (this.selectedPickupTime === undefined && pickupTime && pickupDate) {
                    const dateTime = moment(pickupTime);

                    if (dateTime.isValid()) {
                        // update selectedPickupTime
                        this.selectedPickupTime = dateTime.format(this.timeFormat);

                        // update timepicker validator to enforce the decrement only constraint
                        this.pickupTime.setValidators(Validators.compose([
                            Validators.required,
                            createTimeValidator(this.timeFormat, pickupDate, undefined, this.selectedPickupTime)
                        ]));

                        this.pickupTime.updateValueAndValidity();
                    }
                }
            }
        }

        // reset form
        if (changes['resetForm']) {
            // only toggle is previous value was false and is now true
            if (!changes['resetForm'].previousValue && changes['resetForm'].currentValue) {
                this.reservationPickupTimeForm.reset();
            }
        }
    }
}
