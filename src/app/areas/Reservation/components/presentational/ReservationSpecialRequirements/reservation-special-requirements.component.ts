import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output
} from '@angular/core';
import {List} from 'immutable';

import {BeneficiarySpecialRequirement} from '../../../../../store/Beneficiary/types/beneficiary-special-requirement.model';
import {KeyValuePair} from '../../../../../store/types/key-value-pair.model';

@Component({
    selector       : 'reservation-special-requirements',
    templateUrl    : 'reservation-special-requirements.component.html',
    styleUrls      : ['reservation-special-requirements.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationSpecialRequirementsComponent: handles display of reservation status cards title
 */
export class ReservationSpecialRequirementsComponent {
    /**
     * text to display in the "header" section of the component
     */
    @Input() headerText : string;

    /**
     * if the remove icon should be displayed
     */
    @Input() showRemoveIcon : boolean;

    /**
     * beneficiary special requirements UI state
     */
    @Input() specialRequirementsState : List<BeneficiarySpecialRequirement>;

    /**
     * list of all possible special requirements
     */
    @Input() specialRequirementsList : List<KeyValuePair>;

    /**
     * the state for special requirements editing
     */
    @Input() tempSpecialRequirementsState : List<BeneficiarySpecialRequirement>;

    /**
     * add a special requirement to a passenger
     * @type {EventEmitter}
     */
    @Output() addSpecialRequirement : EventEmitter<BeneficiarySpecialRequirement> = new EventEmitter();

    /**
     * initiate edit mode for a passenger
     * @type {EventEmitter}
     */
    @Output() initEdit : EventEmitter<undefined> = new EventEmitter();

    /**
     * remove the passenger
     * @type {EventEmitter}
     */
    @Output() remove : EventEmitter<undefined> = new EventEmitter();

    /**
     * remove a special requirement from a passenger
     * @type {EventEmitter}
     */
    @Output() removeSpecialRequirement : EventEmitter<KeyValuePair> = new EventEmitter();

    /**
     * save edit changes
     * @type {EventEmitter}
     */
    @Output() save : EventEmitter<undefined> = new EventEmitter();

    /**
     * set permanence of a special requirement
     * @type {EventEmitter}
     */
    @Output() setPermanence : EventEmitter<{ isPermanent : boolean, requirement : KeyValuePair }> = new EventEmitter();

    /**
     * update the from date for a special requirement
     * @type {EventEmitter}
     */
    @Output() updateFromDate : EventEmitter<{ date : string, requirement : KeyValuePair }> = new EventEmitter();

    /**
     * update the thru date for a special requirement
     * @type {EventEmitter}
     */
    @Output() updateThruDate : EventEmitter<{ date : string, requirement : KeyValuePair }> = new EventEmitter();

    /**
     * is the user editing the special requirements
     */
    isEditActive : boolean = false;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : BeneficiarySpecialRequirement) : any {
        return obj.get('uuid');
    }

    /**
     * event handler for keyboard key actions
     * @param event DOM event
     */
    editKeyPressHandler(event : KeyboardEvent) {
        if (event.key === 'Enter') {
            this.editSpecialRequirements();
        }
    }

    /**
     * event handler for toggling editing beneficiary special requirements
     */
    editSpecialRequirements() {
        // update current editing state of form
        this.isEditActive = true;

        this.initEdit.emit();
    }

    /**
     * handler used to reverse any beneficiary changes made in this component
     */
    cancelUpdate() {
        // update current editing state of form
        this.isEditActive = false;
    }

    /**
     * event handler for adding a special requirement
     * @param {BeneficiarySpecialRequirement} requirement
     */
    onAddSpecialRequirement(requirement : BeneficiarySpecialRequirement) {
        this.addSpecialRequirement.emit(requirement);
    }

    /**
     * event handler for removing a special requirement
     * @param {KeyValuePair} requirement
     */
    onRemoveSpecialRequirement(requirement : KeyValuePair) {
        this.removeSpecialRequirement.emit(requirement);
    }

    /**
     * event handler for remove icon clicks
     */
    onRemove() {
        this.remove.emit();
    }

    /**
     * event handler for setting a special requirement as permanent/temporary
     * @param event
     */
    onSetPermanence(event : { isPermanent : boolean, requirement : KeyValuePair }) {
        this.setPermanence.emit(event);
    }

    /**
     * event handler for updating the from date
     * @param event
     */
    onUpdateFromDate(event : { date : string, requirement : KeyValuePair }) {
        // dispatch fromDate field  update Action
        this.updateFromDate.emit(event);
    }

    /**
     * event handler for thruDate selection event
     * @param event
     */
    onUpdateThruDate(event : { date : string, requirement : KeyValuePair }) {
        // dispatch fromDate field  update Action
        this.updateThruDate.emit(event);
    }

    /**
     * handler for form save button. Emits save.
     */
    onSubmit() {
        this.isEditActive = false;

        // save current editing state of form
        this.save.emit();
    }
}
