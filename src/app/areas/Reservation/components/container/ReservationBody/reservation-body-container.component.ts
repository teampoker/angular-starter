import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    FormArray,
    FormBuilder,
    FormGroup
} from '@angular/forms';
import {List} from 'immutable';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {TranslateService} from 'ng2-translate/ng2-translate';
import {NgRedux} from '@angular-redux/store';

import {NavActions} from '../../../../../store/Navigation/nav.actions';
import {BeneficiaryStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary.selectors';
import {BeneficiarySpecialRequirementsSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-special-requirements.selectors';
import {BeneficiaryPlansStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-plans.selectors';
import {BeneficiaryConnectionsStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary-connections.selectors';
import {ReservationStateSelectors} from '../../../../../store/Reservation/selectors/reservation.selectors';
import {BeneficiaryConnectionsActions} from '../../../../../store/Beneficiary/actions/beneficiary-connections.actions';
import {BeneficiaryActions} from '../../../../../store/Beneficiary/actions/beneficiary.actions';
import {ReservationActions} from '../../../../../store/Reservation/actions/reservation.actions';
import {MetaDataTypesSelectors} from '../../../../../store/MetaDataTypes/meta-data-types.selectors';

import {BeneficiaryPlansState} from '../../../../../store/Beneficiary/types/beneficiary-plans-state.model';
import {BeneficiarySpecialRequirementsState} from '../../../../../store/Beneficiary/types/beneficiary-special-requirements-state.model';
import {
    IKeyValuePair,
    KeyValuePair
} from '../../../../../store/types/key-value-pair.model';
import {BeneficiarySpecialRequirement} from '../../../../../store/Beneficiary/types/beneficiary-special-requirement.model';
import {BeneficiaryConnectionsState} from '../../../../../store/Beneficiary/types/beneficiary-connections-state.model';
import {ReservationRequestedByState} from '../../../../../store/Reservation/types/reservation-requested-by-state.model';
import {ReservationTreatmentTypeState} from '../../../../../store/Reservation/types/reservation-treatment-type-state.model';
import {ReservationAppointmentDateState} from '../../../../../store/Reservation/types/reservation-appointment-date-state.model';
import {ReservationAdditionalPassengersState} from '../../../../../store/Reservation/types/reservation-additional-passengers-state.model';
import {ReservationModeOfTransportationState} from '../../../../../store/Reservation/types/reservation-mode-of-transportation-state.model';
import {ReservationPreferredTransportationState} from '../../../../../store/Reservation/types/reservation-preferred-transportation-state.model';
import {ReservationPickupTimeState} from '../../../../../store/Reservation/types/reservation-pickup-time-state.model';
import {ReservationLegState} from '../../../../../store/Reservation/types/reservation-leg-state.model';
import {IBeneficiaryFieldUpdate} from '../../../../../store/Beneficiary/types/beneficiary-field-update.model';
import {ReservationTripIndicatorState} from '../../../../../store/Reservation/types/reservation-trip-indicator-state.model';
import {Reservation} from '../../../../../store/Reservation/types/reservation.model';
import {ReservationBusinessRules} from '../../../../../store/Reservation/types/reservation-business-rules-state.model';
import {
    EnumBeneficiaryHeaderType,
    BeneficiaryHeader
} from '../../../../../store/Beneficiary/types/beneficiary-header.model';
import {
    AlertItem,
    EnumAlertType
} from '../../../../../store/Navigation/types/alert-item.model';
import {AddressLocation} from '../../../../../store/types/address-location.model';
import {NameUuid} from '../../../../../store/types/name-uuid.model';
import {ReservationCardState} from '../../../../../store/Reservation/types/reservation-card-state.model';
import {ReservationHistorySelectors} from '../../../../../store/Reservation/selectors/reservation-history.selectors';
import {ReservationCardsActions} from '../../../../../store/Reservation/actions/reservation-cards.actions';
import {ReservationStatus} from '../../../../../store/Reservation/types/reservation-status.model';
import {IAppStore} from '../../../../../store/app-store';
import {ReservationState} from '../../../../../store/Reservation/types/reservation-state.model';

@Component({
    selector        : 'reservation-body',
    templateUrl     : 'reservation-body-container.component.html',
    styleUrls       : ['reservation-body-container.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationBodyContainerComponent: handles display of reservation status cards
 */
export class ReservationBodyContainerComponent {
    /**
     * ReservationBodyContainerComponent constructor
     * @param {Router} router
     * @param {ChangeDetectorRef} cd
     * @param {FormBuilder} builder
     * @param {TranslateService} translate
     * @param {NavActions} navActions
     * @param {BeneficiaryStateSelectors} beneficiarySelectors
     * @param {BeneficiarySpecialRequirementsSelectors} beneficiarySpecialRequirementsSelectors
     * @param {BeneficiaryPlansStateSelectors} beneficiaryPlansStateSelectors
     * @param {BeneficiaryConnectionsStateSelectors} beneficiaryConnectionsStateSelectors
     * @param {ReservationStateSelectors} reservationStateSelectors
     * @param {BeneficiaryConnectionsActions} beneficiaryConnectionsActions
     * @param {BeneficiaryActions} beneficiaryActions
     * @param {ReservationActions} reservationActions
     * @param {ActivatedRoute} route
     * @param {MetaDataTypesSelectors} metaDataTypesSelectors
     * @param {ReservationHistorySelectors} reservationHistorySelectors
     * @param {ReservationCardsActions} reservationCardsActions
     * @param {NgRedux<IAppStore>} store
     */
    constructor(
        private router                                  : Router,
        private cd                                      : ChangeDetectorRef,
        private builder                                 : FormBuilder,
        private translate                               : TranslateService,
        private navActions                              : NavActions,
        private beneficiarySelectors                    : BeneficiaryStateSelectors,
        private beneficiarySpecialRequirementsSelectors : BeneficiarySpecialRequirementsSelectors,
        private beneficiaryPlansStateSelectors          : BeneficiaryPlansStateSelectors,
        private beneficiaryConnectionsStateSelectors    : BeneficiaryConnectionsStateSelectors,
        private reservationStateSelectors               : ReservationStateSelectors,
        private beneficiaryConnectionsActions           : BeneficiaryConnectionsActions,
        private beneficiaryActions                      : BeneficiaryActions,
        private reservationActions                      : ReservationActions,
        private route                                   : ActivatedRoute,
        private metaDataTypesSelectors                  : MetaDataTypesSelectors,
        private reservationHistorySelectors             : ReservationHistorySelectors,
        private reservationCardsActions                 : ReservationCardsActions,
        private store                                   : NgRedux<IAppStore>
    ) {
        this.beneficiaryUuid = this.route.snapshot.params['beneficiaryUUID'];

        // setup subscription to beneficiary state
        // note: please add a comment indicating index number to ease merge pains
        this.stateSubscription = Observable.combineLatest(
            this.beneficiaryPlansStateSelectors.beneficiaryPlansState(), // 0
            this.beneficiarySelectors.beneficiaryHeader(EnumBeneficiaryHeaderType.BENEFICIARY_MAIN), // 1
            this.beneficiarySpecialRequirementsSelectors.beneficiarySpecialRequirementsState(), // 2
            this.beneficiaryConnectionsStateSelectors.beneficiaryConnectionsState(), // 3
            this.reservationStateSelectors.reservationRequestedByState(), // 4
            this.reservationStateSelectors.reservationTreatmentTypeState(), // 5
            this.reservationStateSelectors.reservationAppointmentDateState(), // 6
            this.reservationStateSelectors.reservationPickupLocationState(), // 7
            this.reservationStateSelectors.reservationDropOffLocationState(), // 8
            this.reservationStateSelectors.reservationAdditionalPassengersState(), // 9
            this.reservationStateSelectors.reservationModeOfTransportationState(), // 10
            this.reservationStateSelectors.reservationPreferredTransportationState(), // 11
            this.reservationStateSelectors.reservationPickupTimeState(), // 12
            this.metaDataTypesSelectors.specialRequirementTypes(), // 13
            this.reservationStateSelectors.reservationLegState(), // 14
            this.reservationStateSelectors.reservationSpecialRequirementState(), // 15
            this.reservationStateSelectors.reservationSaveLegIndicator(), // 16
            this.reservationStateSelectors.reservationTripIndicatorState(), // 17
            this.reservationStateSelectors.reservationTempRequirementState(), // 18
            this.reservationStateSelectors.reservationConfirmationNumber(), // 19
            this.reservationStateSelectors.reservation(), // 20
            this.reservationStateSelectors.reservations(), // 21
            this.reservationStateSelectors.reservationBusinessRules(), // 22
            this.reservationStateSelectors.recentPickupLocations(), // 23
            this.reservationStateSelectors.recentDropoffLocations(), // 24
            this.reservationStateSelectors.getHistoryCardState(), // 25
            this.reservationStateSelectors.getScheduledCardState(), // 26
            this.reservationHistorySelectors.getReservations(this.beneficiaryUuid), // 27
            this.reservationHistorySelectors.getScheduledRides(this.beneficiaryUuid), // 28
            this.reservationStateSelectors.isEditing(), // 29
            this.reservationStateSelectors.editLeg(), // 30
            this.reservationStateSelectors.deleteLeg(), // 31
            this.reservationStateSelectors.getEditReasons(), // 32
            this.reservationStateSelectors.reservationMileageDistance(), // 33
            this.store.select(state => state.reservationState) // 34
        )
            .subscribe(val => {
                // update local state
                this.plansState                               = val[0];
                this.headerConfig                             = val[1];
                this.specialRequirementsState                 = val[2];
                this.beneficiaryConnectionsState              = val[3];
                this.reservationRequestedByState              = val[4];
                this.reservationTreatmentTypeState            = val[5];
                this.reservationAppointmentDateState          = val[6];
                this.reservationPickupLocationState           = val[7];
                this.reservationDropOffLocationState          = val[8];
                this.reservationAdditionalPassengersState     = val[9];
                this.reservationModeOfTransportationState     = val[10];
                this.reservationPreferredTransportationState  = val[11];
                this.reservationPickupTimeState               = val[12];
                this.specialRequirementsList                  = val[13];
                this.reservationLegState                      = val[14];
                this.reservationSpecialRequirementState       = val[15];
                this.reservationSaveLegIndicator              = val[16];
                this.reservationTripIndicatorState            = val[17];
                this.tempSpecialRequirementsState             = val[18];
                this.reservationConfirmationNumber            = val[19];
                this.reservation                              = val[20];
                this.reservations                             = val[21];
                this.reservationBusinessRules                 = val[22];
                this.recentPickupLocations                    = val[23];
                this.recentDropoffLocations                   = val[24];
                this.historyCardState                         = val[25];
                this.scheduledCardState                       = val[26];
                this.historyReservations                      = val[27];
                this.scheduledReservations                    = val[28];
                this.isEditing                                = val[29];
                this.editLeg                                  = val[30];
                this.deleteLeg                                = val[31];
                this.editReasons                              = val[32];
                this.reservationMileageDistance               = val[33];
                this.reservationState                         = val[34];

                // trigger change detection
                this.cd.markForCheck();
            });

        this.beneficiaryActions.getBeneficiary(this.beneficiaryUuid, false);

        // init form input fields
        this.connections = new FormArray([]);

        // build connections FormControl group
        this.connectionsForm = builder.group({
            // placeholder for dynamically generated form groups
            connections : this.connections
        });
    }

    /**
     * Redux state subscription
     */
    private stateSubscription : Subscription;

    /**
     * Beneficiary's uuid
     * @type {String}
     */
    beneficiaryUuid : string;

    /**
     * reservation edit reason codes
     * @type {List<NameUuid>}
     */
    editReasons : List<NameUuid>;

    /**
     * reservation Mileage Distance
     * @type {String}
     */
    reservationMileageDistance : string;

    /**
     * Beneficiary Service Coverages state
     */
    plansState : BeneficiaryPlansState;

    /**
     * Beneficiary Header configuration
     */
    headerConfig : BeneficiaryHeader;

    /**
     * reservation history card state
     * @type {ReservationCardState}
     */
    historyCardState : ReservationCardState;

    /**
     * all past reservations for the beneficiary
     * @type {List<Reservation>
     */
    historyReservations : List<Reservation>;

    /**
     * beneficiary special requirements UI state
     */
    specialRequirementsState : BeneficiarySpecialRequirementsState;

    /**
     * list of all possible special requirements
     */
    specialRequirementsList : List<IKeyValuePair>;

    /**
     * the state for special requirements editing
     */
    tempSpecialRequirementsState : List<BeneficiarySpecialRequirement>;

    /**
     * special requirements specific to this reservation
     */
    reservationSpecialRequirementState : BeneficiarySpecialRequirement;

    /**
     * beneficiary connections UI state
     */
    beneficiaryConnectionsState : BeneficiaryConnectionsState;

    /**
     * reservation requested UI state
     */
    reservationRequestedByState : ReservationRequestedByState;

    /**
     * reservation treatment type UI state
     */
    reservationTreatmentTypeState : ReservationTreatmentTypeState;

    /**
     * reservation appointment date UI state
     */
    reservationAppointmentDateState : ReservationAppointmentDateState;

    /**
     * reservation pickup location UI state
     */
    reservationPickupLocationState : AddressLocation;

    /**
     * reservation drop off location UI state
     */
    reservationDropOffLocationState : AddressLocation;

    /**
     * reservation additional passengers UI state
     */
    reservationAdditionalPassengersState : ReservationAdditionalPassengersState;

    /**
     * reservation mode of transportation UI state
     */
    reservationModeOfTransportationState : ReservationModeOfTransportationState;

    /**
     * reservation preferred transportation provider UI state
     */
    reservationPreferredTransportationState : ReservationPreferredTransportationState;

    /**
     * reservation pickup time UI state
     */
    reservationPickupTimeState : ReservationPickupTimeState;

    /**
     * reservation trip indicator UI state
     */
    reservationTripIndicatorState : ReservationTripIndicatorState;

    /**
     * reservation business rules state
     */
    reservationBusinessRules : ReservationBusinessRules;

    /**
     * recent Pickup Locations
     */
    recentPickupLocations : List<NameUuid>;

    /**
     * recent Dropoff Locations
     */
    recentDropoffLocations : List<NameUuid>;

    /**
     * single reservation
     */
    reservation : Reservation;

    /**
     * list of reservations
     */
    reservations : List<Reservation>;

    /**
     * reservation trip indicator UI state
     */
    reservationConfirmationNumber : string;

    /**
     * reservation save leg UI state
     */
    reservationSaveLegIndicator : boolean;

    /**
     * reservation leg provider UI state
     */
    reservationLegState : List<ReservationLegState>;

    /**
     * scheduled card state
     * @type {ReservationCardState}
     */
    scheduledCardState : ReservationCardState;

    /**
     * the beneficiary's scheduled rides
     * @type {List<Reservation>}
     */
    scheduledReservations : List<Reservation>;

    /**
     * Object contains the connections form group
     */
    connectionsForm : FormGroup;

    /**
     * connections for array
     */
    connections : FormArray;

    /**
     * valid state of connections form
     */
    validConnectionForm : boolean;

    /**
     * beneficiary has a valid plan indicator
     */
    validPlan : boolean = true;

    /**
     * reservation denial
     */
    denied : boolean = false;

    /**
     * reservation is editing indicator
     */
    isEditing : boolean;

    /**
     * reservation leg being edited
     */
    editLeg : number;

    /**
     * reservation leg being deleted
     */
    deleteLeg : number;

    /**
     * reservation reset form indicator
     */
    resetForm : boolean = false;

    /**
     * reservation state
     * @type {ReservationState} reservationState
     */
    reservationState : ReservationState;

    /**
     * navigate user to edit a reservation
     * @param {string} reservationUuid
     */
    onViewEditReservation(reservationUuid : string) {
        // route user
        this.router.navigate([`/Reservation/${this.route.snapshot.params['beneficiaryUUID']}/EditReservation/${reservationUuid}`]);

        // make sure our cards are at a default state
        this.onHistoryCloseDetails();
        this.onScheduledCloseDetails();
    }

    /**
     * update the connection form valid state
     * @param event
     */
    onConnectionFormValid(event : boolean) {
        this.validConnectionForm = event;
    }

    /**
     * event handler for add additional leg event
     */
    addAdditionalLeg() {
        this.reservationActions.addAdditionalLeg();
    }

    /**
     * event handler for save leg event
     */
    saveLeg() {
        this.reservationActions.saveLeg();
    }

    /**
     * event handler for add additional leg event
     */
    updateAddAdditionalLeg() {
        this.reservationActions.addAdditionalLeg();
    }

    /**
     * event handler for save leg event
     */
    updateLeg() {
        this.reservationActions.updateLeg();
    }

    /**
     * event handler for reset form event
     */
    onResetForm() {
        this.reservationActions.resetForm();

        this.connectionsForm.reset();

        this.resetForm = true;

        const timer = setTimeout(() => {
            this.resetForm = false;
            clearTimeout(timer);
        }, 50);
    }

    /**
     * event handler for cancel save leg event
     */
    cancelSaveLeg() {
        this.reservationActions.cancelSaveLeg();
        this.resetForm = true;
        const timer = setTimeout(() => {
            this.resetForm = false;
            clearTimeout(timer);
        }, 50);
    }

    /**
     * event handler for cancel edit leg event
     */
    cancelEditLeg() {
        this.reservationActions.cancelEditLeg();
    }

    /**
     * cancel the add a connection workflow
     */
    cancelAdd() {
        // dispatch action
        this.beneficiaryConnectionsActions.cancelAddBeneficiaryConnection();
    }

    /**
     * Adds the special requirement to temporary special requirements
     * @param {BeneficiarySpecialRequirement} requirement
     */
    onAddTempSpecialRequirement(requirement : BeneficiarySpecialRequirement) {
        this.reservationActions.addTempSpecialRequirement(requirement);
    }

    /**
     * Closes details for a reservation in the card widget
     */
    onHistoryCloseDetails() {
        this.reservationCardsActions.closeHistoryItem();
    }

    /**
     * Shows details for a reservation in the card widget
     * @param uuid
     */
    onHistoryShowDetails(uuid : string) {
        this.reservationCardsActions.selectHistoryItem(uuid);
    }

    /**
     * Toggles leg details for a reservation in the card widget
     * @param index
     */
    onHistoryToggleLeg(index : number) {
        this.reservationCardsActions.toggleHistoryLeg(index);
    }

    /**
     * Initiate special requirements editing
     */
    onInitSpecialRequirementsEdit() {
        this.reservationActions.initTempSpecialRequirementState();
    }

    /**
     * Removes the special requirement from temporary special requirements
     * @param {KeyValuePair} requirement
     */
    onRemoveTempSpecialRequirement(requirement : KeyValuePair) {
        this.reservationActions.removeTempSpecialRequirement(requirement);
    }

    /**
     * Fires off a temporary special requirements save
     */
    onSaveTempSR() {
        this.reservationActions.saveSpecialRequirements();
    }

    /**
     * Closes details for a scheduled reservation in the card widget
     */
    onScheduledCloseDetails() {
        this.reservationCardsActions.closeScheduledItem();
    }

    /**
     * Shows details for a scheduled reservation in the card widget
     * @param uuid
     */
    onScheduledShowDetails(uuid : string) {
        this.reservationCardsActions.selectScheduledItem(uuid);
    }

    /**
     * Toggles leg details for a scheduled reservation in the card widget
     * @param index
     */
    onScheduledToggleLeg(index : number) {
        this.reservationCardsActions.toggleScheduledLeg(index);
    }

    /**
     * Sets the edit reason code
     * @param {{ reason : NameUuid, comment : string }} event
     */
    onSelectEditReason(event : { reason : NameUuid, comment : string }) {
        this.reservationActions.setReservationEditReason(event.reason, event.comment);
    }

    /**
     * Sets a temporary special requirement as permanent/temporary
     * @param event
     */
    onSetTempSRPermanence(event : { isPermanent : boolean, requirement : KeyValuePair }) {
        this.reservationActions.setTempSpecialRequirementPermanence(event.isPermanent, event.requirement);
    }

    /**
     * Updates the drop off location
     * @param {AddressLocation} location
     */
    onUpdateDropOffLocation(location : AddressLocation) {
        this.reservationActions.updateDropOffLocation(location);

        // get this legs mileage
        this.reservationActions.getReservationLegMileage(
            this.reservationPickupLocationState.get('latLong'),
            location.get('latLong'),
            this.reservationAppointmentDateState.get('appointmentTime')
        );
    }

    /**
     * Updates the pickup location
     * @param {AddressLocation} location
     */
    onUpdatePickupLocation(location : AddressLocation) {
        this.reservationActions.updatePickupLocation(location);

        // get this legs milage
        // once a drop off address has been selected
        if (this.reservationDropOffLocationState.getIn(['latLong', 'lat']) !== '') {
            this.reservationActions.getReservationLegMileage(
                location.get('latLong'),
                this.reservationDropOffLocationState.get('latLong'),
                this.reservationAppointmentDateState.get('appointmentTime')
            );
        }
    }

    /**
     * event handler for update beneficiary email record event
     * @param update new values
     * @param updateIndex index of email the update needs to be applied to
     */
    onUpdateConnectionField(update : IBeneficiaryFieldUpdate, updateIndex : number) {
        // append update index
        update.primaryUpdateIndex = updateIndex;

        // dispatch address field update Action
        this.beneficiaryConnectionsActions.updateConnectionField(update);
    }

    /**
     * event handler for fromDate selection event
     * @param event
     */
    onUpdateTempSRFromDate(event : { date : string, requirement : KeyValuePair }) {
        this.reservationActions.updateTempSpecialRequirementFromDate(event.date, event.requirement);
    }

    /**
     * event handler for thruDate selection event
     * @param event
     */
    onUpdateTempSRThruDate(event : { date : string, requirement : KeyValuePair }) {
        // dispatch fromDate field  update Action
        this.reservationActions.updateTempSpecialRequirementThruDate(event.date, event.requirement);
    }

    /**
     * event handler no available plans alert message
     */
    noPlansAvailable() {
        const alertMessage = new AlertItem({
            alertType : EnumAlertType.ERROR,
            message : this.translate.instant('A_RESERVATION_CAN_NOT_BE_CREATED_WITH_NO_PLAN'),
            durable: true
        });

        this.validPlan = false;

        this.navActions.updateAlertMessageState(alertMessage);
    }

    /**
     * event handler to create a reservation
     */
    submitReservation() {
        const reservation = new Reservation({
            legs : this.reservationLegState,
            personUuid : this.beneficiaryUuid,
            status : new ReservationStatus({ id : 1, name : 'Pending' }),
            version : 0
        } as any);

        this.reservationActions.createReservation(reservation);
    }

    /**
     * event handler for save leg event
     */
    saveDeniedReservation() {
        this.reservationActions.saveLeg();

        const reservation = new Reservation({

            legs            : this.reservationLegState,
            personUuid      : this.beneficiaryUuid,
            status          : new ReservationStatus({ id : 3, name : 'Denied' }),

            // this code needs to come from the service but currently does not
            eventReasonUuid : '6819c6ee-0f1c-11e7-a2b6-00ac0399812e',
            version         : 0
        } as any);

        this.reservationActions.saveDeniedReservation(reservation);
        // route user
        this.router.navigate([`/Beneficiary/${this.route.snapshot.params['beneficiaryUUID']}/`]);
    }

    /**
     * event handler to edit a reservation
     */
    editReservation() {
        this.reservationActions.editReservation(this.reservation.set('legs', this.reservationLegState) as Reservation);
    }

    /**
     * trip distance calculator
     * @param legs
     * @returns {number}
     */
    tripDistance (legs : List<ReservationLegState>) : number {
        let totalDistance : number = 0;

        legs.forEach(distance => totalDistance += isNaN(distance.get('legDistance')) ? 0 :  distance.get('legDistance'));

        return totalDistance;
    }

    /**
     * event handler used to validate business rules as a whole
     */
    validateRules() {
        // reset denied
        this.denied = false;

        // define variables
        let exceptionCount : number = 0,
            reasonCount    : number = 0,
            treatmentType,
            treatmentTypeOverrideReason,
            location,
            locationOverrideReason,
            escort,
            escortOverrideReason,
            modeOfTransportation,
            modeOfTransportationOverrideReason;

        // round trip rule validation
        if (!this.reservationTripIndicatorState.get('roundTrip')) {
            // treatmentType
            treatmentType = this.reservationBusinessRules.get('treatmentType');
            treatmentType.forEach(response => {
                if (response.get('status') === 'PENDING') {
                    exceptionCount ++;
                }
                if (response.get('status') === 'DENIED') {
                    this.denied = true;
                }
            });
            treatmentTypeOverrideReason = this.reservationBusinessRules.get('treatmentTypeOverrideReason');
            treatmentTypeOverrideReason.forEach(reason => {
                reasonCount ++;
            });

        }

        // location
        location = this.reservationBusinessRules.get('location');
        location.forEach(response => {
            if (response.get('status') === 'PENDING') {
                exceptionCount ++;
            }
            if (response.get('status') === 'DENIED') {
                this.denied = true;
            }
        });
        locationOverrideReason = this.reservationBusinessRules.get('locationOverrideReason');
        locationOverrideReason.forEach(reason => {
            reasonCount ++;
        });

        // escort
        escort = this.reservationBusinessRules.get('escort');
        escort.forEach(response => {
            if (response.get('status') === 'PENDING') {
                exceptionCount ++;
            }
            if (response.get('status') === 'DENIED') {
                this.denied = true;
            }
        });
        escortOverrideReason = this.reservationBusinessRules.get('escortOverrideReason');
        escortOverrideReason.forEach(reason => {
            reasonCount ++;
        });

        // modeOfTransportation
        modeOfTransportation = this.reservationBusinessRules.get('modeOfTransportation');
        modeOfTransportation.forEach(response => {
            if (response.get('status') === 'PENDING') {
                exceptionCount ++;
            }
            if (response.get('status') === 'DENIED') {
                this.denied = true;
            }
        });
        modeOfTransportationOverrideReason = this.reservationBusinessRules.get('modeOfTransportationOverrideReason');
        modeOfTransportationOverrideReason.forEach(reason => {
            reasonCount ++;
        });

        // non exception selection === denial
        if (exceptionCount !== reasonCount) {
            this.denied = true;
        }

        return exceptionCount === reasonCount;
    }

    /**
     * event handler used to validate the form as a whole
     */
    validateForm() {
        let disabled : boolean = false;

        // round trip validation
        if (this.reservationTripIndicatorState.get('roundTrip')) {
            disabled = !this.validPlan ||
            !this.reservationPickupLocationState.get('isFormValid') ||
            !this.reservationDropOffLocationState.get('isFormValid') ||
            !this.reservationAdditionalPassengersState.get('isFormValid') ||
            !this.reservationModeOfTransportationState.get('isFormValid') ||
            !this.reservationPickupTimeState.get('isFormValid');
        }
        // validate all fields in a reservation
        else {
            disabled = !this.validPlan ||
            !this.reservationRequestedByState.get('isFormValid') ||
            !this.reservationTreatmentTypeState.get('isFormValid') ||
            !this.reservationAppointmentDateState.get('appointmentDateValid') ||
            !this.reservationAppointmentDateState.get('appointmentTimeValid') ||
            !this.reservationPickupLocationState.get('isFormValid') ||
            !this.reservationDropOffLocationState.get('isFormValid') ||
            !this.reservationAdditionalPassengersState.get('isFormValid') ||
            !this.reservationModeOfTransportationState.get('isFormValid') ||
            !this.reservationPickupTimeState.get('isFormValid');
        }

        return disabled;
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        if (this.plansState.get('plansStateSummary').size < 1 && !this.isEditing) {
            this.noPlansAvailable();
        }
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        // unsubscribe
        this.stateSubscription.unsubscribe();
    }
}
