import {
    Component,
    ChangeDetectionStrategy,
    OnDestroy,
    ChangeDetectorRef
} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {
    List,
    Map
} from 'immutable';

import {ReservationHistoryActions} from '../../../../../store/Reservation/actions/reservation-history.actions';
import {ReservationHistorySelectors} from '../../../../../store/Reservation/selectors/reservation-history.selectors';
import {
    ReservationHistoryState,
    SortOptions
} from '../../../../../store/Reservation/types/reservation-history-state.model';
import {SearchResultsPaging} from '../../../../../store/Search/types/search-results-paging.model';
import {Reservation} from '../../../../../store/Reservation/types/reservation.model';
import {DateRange} from '../../../../../store/types/date-range.model';
import {MetaDataTypesSelectors} from '../../../../../store/MetaDataTypes/meta-data-types.selectors';
import {ServiceType} from '../../../../../store/MetaDataTypes/types/service-type.model';
import {ReservationStatus} from '../../../../../store/Reservation/types/reservation-status.model';
import {BeneficiaryStateSelectors} from '../../../../../store/Beneficiary/selectors/beneficiary.selectors';
import {Beneficiary} from '../../../../../store/Beneficiary/types/beneficiary.model';

/**
 * Displays a list of reservations for a beneficiary
 * @export
 * @class ReservationHistoryContainerComponent
 */
@Component({
    selector : 'reservation-history',
    templateUrl : 'reservation-history-container.component.html',
    styleUrls : ['reservation-history-container.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
export class ReservationHistoryContainerComponent implements OnDestroy {
    /**
     * Creates an instance of ReservationHistoryContainerComponent.
     *
     * @param {BeneficiaryStateSelectors} beneficiarySelectors
     * @param {ReservationHistoryActions} historyActions
     * @param {ReservationHistorySelectors} historySelectors
     * @param {MetaDataTypesSelectors} metaDataSelectors
     * @param {ActivatedRoute} route
     * @param {ChangeDetectorRef} cd
     *
     * @memberOf ReservationHistoryContainerComponent
     */
    constructor(
        private beneficiarySelectors : BeneficiaryStateSelectors,
        private historyActions : ReservationHistoryActions,
        private historySelectors : ReservationHistorySelectors,
        private metaDataSelectors : MetaDataTypesSelectors,
        private route : ActivatedRoute,
        private cd : ChangeDetectorRef
    ) {
        // Figure out if we're dealing with Reservation History or Scheduled Rides
        this.route.url
            .first()
            .subscribe(data => this.isHistory = data[1].path === 'History');

        // Set the default sorting option based on History vs Scheduled
        if (this.isHistory) {
            this.onSetSortOptions(SortOptions.TRIP_DATE_DESC);
        }
        else {
            this.onSetSortOptions(SortOptions.APPOINTMENT_TIME_ASC);
        }

        // Get the state information for the component
        this.subscriptions.push(this.historySelectors
            .getSettings()
            .subscribe(state => {
                this.historyState = state;

                this.paginationConfig = this.paginationConfig
                    .withMutations(options => {
                        return options
                            .set('pageOffset', state.get('pageOffset'))
                            .set('pageSize', state.get('itemsPerPage'));
                    }) as SearchResultsPaging;
            }));

        // get the reservations based on Reservation History vs Scheduled Rides
        if (this.isHistory) {
            this.subscriptions.push(this.historySelectors
                .getReservations(this.route.snapshot.params['beneficiaryUUID'], true)
                .subscribe(reservations => {
                    this.reservations = reservations
                        .sort(this.historySelectors.getSortFunction(this.historyState.get('sortOption')))
                        .skip(this.historyState.get('pageOffset', 0) * this.historyState.get('itemsPerPage', 20))
                        .take(this.historyState.get('itemsPerPage', 20)) as List<Reservation>;

                    this.paginationConfig = this.paginationConfig
                        .set('totalRecords', reservations.count()) as SearchResultsPaging;

                    this.cd.markForCheck();
                }));
        }
        else {
            this.subscriptions.push(this.historySelectors
                .getScheduledRides(this.route.snapshot.params['beneficiaryUUID'], true)
                .subscribe(reservations => {
                    this.reservations = reservations
                        .sort(this.historySelectors.getSortFunction(this.historyState.get('sortOption')))
                        .skip(this.historyState.get('pageOffset', 0) * this.historyState.get('itemsPerPage', 20))
                        .take(this.historyState.get('itemsPerPage', 20)) as List<Reservation>;

                    this.paginationConfig = this.paginationConfig
                        .set('totalRecords', reservations.count()) as SearchResultsPaging;

                    this.cd.markForCheck();
                }));
        }

        // get all treatment types so we can populate filters
        this.subscriptions.push(this.metaDataSelectors
            .treatmentTypeList()
            .subscribe(state => this.treatmentTypes = state));

        // get all trip statuses so we can populate filters
        this.subscriptions.push(this.historySelectors
            .getStatuses(this.route.snapshot.params['beneficiaryUUID'])
            .subscribe(statuses => this.statuses = statuses));

        // get the beneficiary state
        this.subscriptions.push(this.beneficiarySelectors
            .beneficiary()
            .subscribe(beneficiary => this.beneficiary = beneficiary));

        // get trip types
        this.subscriptions.push(this.historySelectors
            .getTripTypes(this.route.snapshot.params['beneficiaryUUID'])
            .subscribe(types => this.tripTypes = types));
    }

    /**
     * Subscriptions for state retrieval
     * @type {Array<Subscription>}
     */
    private subscriptions : Array<Subscription> = [];

    beneficiary : Beneficiary;

    /**
     * Current reservation history state
     * @type {ReservationHistoryState}
     * @memberOf ReservationHistoryContainerComponent
     */
    historyState : ReservationHistoryState = new ReservationHistoryState();

    /**
     * This component is used for "Reservation History" and "Scheduled Rides". This tells us which one is requested.
     * @type {boolean}
     * @memberOf ReservationHistoryContainerComponent
     */
    isHistory : boolean;

    /**
     * Configuration object for the Pagination component
     * @type {SearchResultsPaging}
     * @memberOf ReservationHistoryContainerComponent
     */
    paginationConfig : SearchResultsPaging = new SearchResultsPaging();

    /**
     * Collection of the beneficiary's reservations
     * @type {List<Reservation>}
     * @memberOf ReservationHistoryContainerComponent
     */
    reservations : List<Reservation>;

    /**
     * Collection of all statuses represented in the beneficiary's reservations
     * @type {Map<string, ReservationStatus>}
     * @memberOf ReservationHistoryContainerComponent
     */
    statuses : Map<string, ReservationStatus>;

    /**
     * Collection of all treatment types
     * @type {List<ServiceType>}
     * @memberOf ReservationHistoryContainerComponent
     */
    treatmentTypes : List<ServiceType>;

    /**
     * Collection of all trip types represented in the beneficiary's reservations
     * @type {List<string>}
     * @memberOf ReservationHistoryContainerComponent
     */
    tripTypes : List<string>;

    /**
     * Changes the date filter
     * @param {DateRange} range
     * @memberOf ReservationHistoryContainerComponent
     */
    onChangeDateFilter(range : DateRange) {
        this.historyActions.changeDateFilter(range);
    }

    /**
     * Changes the text filter's value
     * @param {string} value
     * @memberOf ReservationHistoryContainerComponent
     */
    onChangeTextFilter(value : string) {
        this.historyActions.changeTextFilter(value);
    }

    /**
     * Clears the Status filter
     * @memberOf ReservationHistoryContainerComponent
     */
    onClearStatusFilter() {
        this.historyActions.clearStatusFilter();
    }

    /**
     * Clears the Treatment Type filter
     * @memberOf ReservationHistoryContainerComponent
     */
    onClearTreatmentTypeFilter() {
        this.historyActions.clearTreatmentTypeFilter();
    }

    /**
     * Clears the Trip Type filter
     * @memberOf ReservationHistoryContainerComponent
     */
    onClearTripTypeFilter() {
        this.historyActions.clearTripTypeFilter();
    }

    /**
     * Navigates to the next page
     * @memberOf ReservationHistoryContainerComponent
     */
    onGoToNextPage() {
        this.historyActions.goToNextPage();
    }

    /**
     * Navigates to the previous page
     * @memberOf ReservationHistoryContainerComponent
     */
    onGoToPreviousPage() {
        this.historyActions.goToPreviousPage();
    }

    /**
     * Sets the number of items displayed per page
     * @param {number} numberOfItems
     * @memberOf ReservationHistoryContainerComponent
     */
    onSetItemsPerPage(numberOfItems : number) {
        this.historyActions.setItemsPerPage(numberOfItems);
    }

    /**
     * Sets the sort option
     * @param {SortOptions} option
     */
    onSetSortOptions(option : SortOptions) {
        this.historyActions.setSortOptions(option);
    }

    /**
     * Changes the value of a specific Status (filter active vs inactive)
     * @param {any} event
     * @memberOf ReservationHistoryContainerComponent
     */
    onToggleStatusFilterValue(event : { type : ReservationStatus, active : boolean }) {
        this.historyActions.setStatusFilterValue(event.type, event.active);
    }

    /**
     * Changes the value of a specific Treatment Type (filter active vs inactive)
     * @param {any} event
     * @memberOf ReservationHistoryContainerComponent
     */
    onToggleTreatmentTypeFilterValue(event : { type : ServiceType, active : boolean }) {
        this.historyActions.setTreatmentTypeFilterValue(event.type, event.active);
    }

    /**
     * Changes the value of a specific Trip Type (filter active vs inactive)
     * @param {any} event
     * @memberOf ReservationHistoryContainerComponent
     */
    onToggleTripTypeFilterValue(event : { type : string, active : boolean }) {
        this.historyActions.setTripTypeFilterValue(event.type, event.active);
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
}
