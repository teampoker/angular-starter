import {NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {ReservationRoutingModule} from './reservation-routing.module';
import {ReservationEntryComponent} from './reservation-entry.component';
import {ReservationService} from './services/Reservation/reservation.service';
import {ReservationAdditionalPassengerComponent} from './components/presentational/ReservationAdditionalPassenger/reservation-additional-passenger.component';
import {ReservationAppointmentDateComponent} from './components/presentational/ReservationAppointmentDate/reservation-appointment-date.component';
import {ReservationBodyContainerComponent} from './components/container/ReservationBody/reservation-body-container.component';
import {ReservationHeaderComponent} from './components/presentational/ReservationHeader/reservation-header.component';
import {ReservationModeOfTransportationComponent} from './components/presentational/ReservationModeOfTransportation/reservation-mode-of-transportation.component';
import {ReservationPickupTimeComponent} from './components/presentational/ReservationPickupTime/reservation-pickup-time.component';
import {ReservationPreferredTransportationComponent} from './components/presentational/ReservationPreferredTransportation/reservation-preferred-transportation.component';
import {ReservationRequestedByComponent} from './components/presentational/ReservationRequestedBy/reservation-requested-by.component';
import {ReservationSpecialRequirementsComponent} from './components/presentational/ReservationSpecialRequirements/reservation-special-requirements.component';
import {ReservationSpecialRequirementsCheckboxSelectionComponent} from './components/presentational/ReservationSpecialRequirementsCheckboxSelection/reservation-special-requirements-checkbox-selection.component';
import {ReservationTreatmentTypeComponent} from './components/presentational/ReservationTreatmentType/reservation-treatment-type.component';
import {ReservationLegComponent} from './components/presentational/ReservationLeg/reservation-leg.component';
import {ReservationLocationComponent} from './components/presentational/ReservationLocation/reservation-location.component';
import {ReservationLocationInfoComponent} from './components/presentational/ReservationLocationInfo/reservation-location-info.component';
import {ReservationAdditionalPassengerListComponent} from './components/presentational/ReservationAdditionalPassengerList/reservation-additional-passenger-list.component';
import {ReservationTripIndicatorComponent} from './components/presentational/ReservationTripIndicator/reservation-trip-indicator.component';
import {ReservationHistoryContainerComponent} from './components/container/ReservationHistory/reservation-history-container.component';
import {ReservationHistoryFilterPanelComponent} from './components/presentational/ReservationHistoryFilterPanel/reservation-history-filter-panel.component';
import {ReservationListComponent} from './components/presentational/ReservationList/reservation-list.component';
import {ReservationListLegComponent} from './components/presentational/ReservationListLeg/reservation-list-leg.component';
import {ReservationBuinessRulesComponent} from './components/presentational/ReservationBuinessRules/reservation-business-rules.component';
import {ReservationEditReasonComponent} from './components/presentational/ReservationEditReason/reservation-edit-reason.component';
import {ReservationMassTransitRoutesComponent} from './components/presentational/ReservationMassTransitRoutes/reservation-mass-transit-routes.component';

@NgModule({
    imports         : [
        SharedModule,
        ReservationRoutingModule
   ],
    declarations    : [
        ReservationAdditionalPassengerComponent,
        ReservationAppointmentDateComponent,
        ReservationBodyContainerComponent,
        ReservationEntryComponent,
        ReservationHeaderComponent,
        ReservationModeOfTransportationComponent,
        ReservationPickupTimeComponent,
        ReservationPreferredTransportationComponent,
        ReservationRequestedByComponent,
        ReservationSpecialRequirementsComponent,
        ReservationSpecialRequirementsCheckboxSelectionComponent,
        ReservationTreatmentTypeComponent,
        ReservationLegComponent,
        ReservationLocationComponent,
        ReservationLocationInfoComponent,
        ReservationAdditionalPassengerListComponent,
        ReservationTripIndicatorComponent,
        ReservationHistoryContainerComponent,
        ReservationHistoryFilterPanelComponent,
        ReservationListComponent,
        ReservationListLegComponent,
        ReservationBuinessRulesComponent,
        ReservationEditReasonComponent,
        ReservationMassTransitRoutesComponent
   ],
    providers       : [
        ReservationService
    ]
})

export class ReservationModule {

}
