import {
    async,
    inject,
    TestBed
} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {APIMockService} from '../../../../shared/services/Mock/api-mock.service';
import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {ReservationService} from './reservation.service';

describe('ReservationService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports     : [HttpModule],
            providers   : [
                APIMockService,
                BackendService,
                ReservationService
            ]
        });
    });

    xit('should get the current reservation display data', async(inject([ReservationService], reservationService => {

    })));
});
