export const getReservationsResponse : Array<any> = [
  {
    uuid : '858371d4-30bd-4765-8eaf-57f661b8fd0b',
    referenceId : '1-20170410-100001',
    items: [
      {
        type : 'Transportation',
        uuid : 'ab0b29ad-67bd-40b5-be4b-4c237aed9091',
        reservationUuid : '858371d4-30bd-4765-8eaf-57f661b8fd0b',
        status: {
          id: 1,
          name : 'In Process'
        },
        ordinality: 0,
        treatmentType: {
          uuid : '6723ef10-df66-11e6-9155-125e1c46ef60',
          name : 'Sleep Study'
        },
        serviceOfferingUuid : '6723ef10-df66-11e6-9155-125e1c46ef60',
        modeOfTransportation: {
          uuid : '07c2600c-ee4f-11e6-9155-125e1c46ef60'
        },
        transportationServiceOfferingUuid : '07c2600c-ee4f-11e6-9155-125e1c46ef60',
        requestedOn : '2017-03-13T19:56:20Z[UTC]',
        requestedByPersonUuid : '95a825b7-cbdb-449b-8238-d956056b07cf',
        requestedBy: {
          targetUuid : '95a825b7-cbdb-449b-8238-d956056b07cf',
          uuid : '95a825b7-cbdb-449b-8238-d956056b07cf'
        },
        createdOn : '2017-03-13T19:56:21Z[UTC]',
        createdBy : 'Create-Reservation-User-Placeholder',
        updatedOn : '2017-03-13T20:11:45Z[UTC]',
        updatedBy : 'Update-Reservation-User-Placeholder',
        version: 2,
        appointmentOn : '2017-04-10T05:10:00Z[UTC]',
        appointmentOnTimezone : 'EDT',
        appointmentOnTime: {
          dateTime : '2017-04-10T05:10:00Z[UTC]',
          timezone : 'EDT'
        },
        pickUpOnTimezone : 'EDT',
        pickUpOnTime: {
          timezone : 'EDT'
        },
        dropOffOnTimezone : 'EDT',
        dropOffOnTime: {
          timezone : 'EDT'
        },
        pickUpLocation: {
          name : '1600 Marina Rd',
          uuid : '5a53d611-86af-45d7-8795-9c3cbf319016',
          address: {
            uuid : '10635405-9e6c-409f-a01b-0900ac92cf69'
          },
          directions : '',
          createdOn : '2017-03-13T19:56:21Z[UTC]',
          createdBy : 'Create-Reservation-User-Placeholder',
          updatedOn : '2017-03-13T19:56:21Z[UTC]',
          updatedBy : 'Create-Reservation-User-Placeholder',
          version: 0
        },
        dropOffLocation: {
          name : '1600 Pennsylvania Ave NW',
          uuid : 'b6bbb901-4256-4539-88e8-d818b0492a84',
          address: {
            uuid : '13daf2f9-8171-4e4a-80d6-0250a0975d5e'
          },
          directions : '',
          createdOn : '2017-03-13T19:56:21Z[UTC]',
          createdBy : 'Create-Reservation-User-Placeholder',
          updatedOn : '2017-03-13T19:56:21Z[UTC]',
          updatedBy : 'Create-Reservation-User-Placeholder',
          version: 0
        },
        driver: {
          targetUuid : '1c5c21a4-f6aa-4cb8-a701-2d37e459b55d',
          uuid : '1c5c21a4-f6aa-4cb8-a701-2d37e459b55d'
        },
        driverUuid : '1c5c21a4-f6aa-4cb8-a701-2d37e459b55d',
        transportationItemPassengers: [
          {
            sequenceId: 1,
            createdOn : '2017-03-13T20:11:45Z[UTC]',
            createdBy : 'Update-Reservation-User-Placeholder',
            updatedOn : '2017-03-13T20:11:45Z[UTC]',
            updatedBy : 'Update-Reservation-User-Placeholder',
            version: 0,
            transportationReservationItemUuid : 'ab0b29ad-67bd-40b5-be4b-4c237aed9091',
            personUuid : '84dcb81e-18e6-4ccc-acdf-5b2c0852f50e',
            person: {
              targetUuid : '84dcb81e-18e6-4ccc-acdf-5b2c0852f50e',
              uuid : '84dcb81e-18e6-4ccc-acdf-5b2c0852f50e'
            },
            passengerSpecialRequirements: [

            ]
          }
        ],
        transportationItemSpecialRequirements: [
          {
            sequenceId: 2,
            createdOn : '2017-03-13T20:11:45Z[UTC]',
            createdBy : 'Update-Reservation-User-Placeholder',
            updatedOn : '2017-03-13T20:11:45Z[UTC]',
            updatedBy : 'Update-Reservation-User-Placeholder',
            version: 0,
            transportationReservationItemUuid : 'ab0b29ad-67bd-40b5-be4b-4c237aed9091',
            specialRequirementTypeUuid : '68ff5eef-a53e-11e6-9155-125e1c46ef60',
            specialRequirementType: {
              uuid : '68ff5eef-a53e-11e6-9155-125e1c46ef60'
            },
            comment: null
          }
        ]
      }
    ],
    status: {
      id: 1,
      name : 'Pending'
    },
    createdOn : '2017-03-13T19:56:21Z[UTC]',
    createdBy : 'Create-Reservation-User-Placeholder',
    personUuid : '95a825b7-cbdb-449b-8238-d956056b07cf',
    updatedOn : '2017-03-13T19:56:21Z[UTC]',
    updatedBy : 'Create-Reservation-User-Placeholder',
    version: 0
  }
];
