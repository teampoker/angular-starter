import {IMassTransitRouteApi} from '../../../../../store/types/mass-transit-route.model';

export const getMassTransitRoutesResponse : Array<IMassTransitRouteApi> = [
    {
        distanceForRoute: 7.66,
        durationForRoute: 55,
        arrivalTimeForRoute: '2017-03-14T18:28:29',
        departureTimeForRoute: '2017-03-14T17:34:28',
        status: 'OK',
        massTransitRouteSteps: [
            {
                distanceForStep: 0.49,
                durationForStep: 11,
                instructionsForStep: 'Walk to Greenhouse - Agronomy Building',
                travelModeForStep: 'WALKING'
            },
            {
                distanceForStep: 6.9,
                durationForStep: 39,
                instructionsForStep: 'Bus towards Shands to Santa',
                travelModeForStep: 'TRANSIT'
            },
            {
                distanceForStep: 0.27,
                durationForStep: 6,
                instructionsForStep: 'Walk to 4101-4199 NW 50th Terrace, Gainesville, FL 32606, USA',
                travelModeForStep: 'WALKING'
            }
        ]
    },
    {
        distanceForRoute: 6.31,
        durationForRoute: 52,
        arrivalTimeForRoute: '2017-03-14T18:31:44',
        departureTimeForRoute: '2017-03-14T17:40:03',
        status: 'OK',
        massTransitRouteSteps: [
            {
                distanceForStep: 0.47,
                durationForStep: 10,
                instructionsForStep: 'Walk to Gale Lemerand Athletic Center',
                travelModeForStep: 'WALKING'
            },
            {
                distanceForStep: 5.01,
                durationForStep: 26,
                instructionsForStep: 'Bus towards The Hub to Hunters Crossing',
                travelModeForStep: 'TRANSIT'
            },
            {
                distanceForStep: 0.84,
                durationForStep: 17,
                instructionsForStep: 'Walk to 4101-4199 NW 50th Terrace, Gainesville, FL 32606, USA',
                travelModeForStep: 'WALKING'
            }
        ]
    },
    {
        distanceForRoute: 7.66,
        durationForRoute: 55,
        arrivalTimeForRoute: '2017-03-14T17:58:29',
        departureTimeForRoute: '2017-03-14T17:04:28',
        status: 'OK',
        massTransitRouteSteps: [
            {
                distanceForStep: 0.49,
                durationForStep: 11,
                instructionsForStep: 'Walk to Greenhouse - Agronomy Building',
                travelModeForStep: 'WALKING'
            },
            {
                distanceForStep: 6.9,
                durationForStep: 39,
                instructionsForStep: 'Bus towards Shands to Santa',
                travelModeForStep: 'TRANSIT'
            },
            {
                distanceForStep: 0.27,
                durationForStep: 6,
                instructionsForStep: 'Walk to 4101-4199 NW 50th Terrace, Gainesville, FL 32606, USA',
                travelModeForStep: 'WALKING'
            }
        ]
    },
    {
        distanceForRoute: 12.73,
        durationForRoute: 77,
        arrivalTimeForRoute: '2017-03-14T18:15:29',
        departureTimeForRoute: '2017-03-14T16:59:05',
        status: 'OK',
        massTransitRouteSteps: [
            {
                distanceForStep: 0.14,
                durationForStep: 3,
                instructionsForStep: 'Walk to Sigma Alpha Epsilon',
                travelModeForStep: 'WALKING'
            },
            {
                distanceForStep: 5.25,
                durationForStep: 22,
                instructionsForStep: 'Bus towards Reitz Union to Oaks Mall',
                travelModeForStep: 'TRANSIT'
            },
            {
                distanceForStep: 4.13,
                durationForStep: 22,
                instructionsForStep: 'Bus towards Haile Market Square to Santa Fe College',
                travelModeForStep: 'TRANSIT'
            },
            {
                distanceForStep: 2.93,
                durationForStep: 11,
                instructionsForStep: 'Bus towards Santa Fe to Airport',
                travelModeForStep: 'TRANSIT'
            },
            {
                distanceForStep: 0.28,
                durationForStep: 6,
                instructionsForStep: 'Walk to 4101-4199 NW 50th Terrace, Gainesville, FL 32606, USA',
                travelModeForStep: 'WALKING'
            }
        ]
    }
];
