export const editReservationResponse : Array<any> = [{
    uuid : '4780b7b8-bef7-4c9a-bd00-896a967ec384',
    referenceId : '11-124-65-998748-12',
    items : [
    {
        type : 'Transportation',
        uuid : '02e31ed3-4484-420f-8e27-852f46a92e06',
        reservationUuid : '4780b7b8-bef7-4c9a-bd00-896a967ec384',
        status : {
            id : 1,
            name : 'In Process'
        },
        ordinality : 0,
        treatmentType : {
            uuid : '6723f033-df66-11e6-9155-125e1c46ef60'
        },
        serviceOfferingUuid : '6723f033-df66-11e6-9155-125e1c46ef60',
        modeOfTransportation : {
            uuid : 'c1750d65-ee51-11e6-9155-125e1c46ef60'
        },
        transportationServiceOfferingUuid : 'c1750d65-ee51-11e6-9155-125e1c46ef60',
        requestedOn : '2017-03-11T14:37:05Z[UTC]',
        requestedByPersonUuid : '1c5c21a4-f6aa-4cb8-a701-2d37e459b55d',
        requestedBy : {
            targetUuid : '1c5c21a4-f6aa-4cb8-a701-2d37e459b55d',
            uuid : '1c5c21a4-f6aa-4cb8-a701-2d37e459b55d'
        },
        updatedOn : '2017-03-12T16:45:02.188Z[UTC]',
        updatedBy : 'Update-Reservation-User-Placeholder',
        version : 3,
        appointmentOn : '2017-03-31T04:00:00Z[UTC]',
        appointmentOnTimezone : 'EDT',
        appointmentOnTime : {
            dateTime : '2017-03-31T04:00:00Z[UTC]',
            timezone : 'EDT'
        },
        pickUpOnTimezone : 'EDT',
        pickUpOnTime : {
            timezone : 'EDT'
        },
        dropOffOnTimezone : 'EDT',
        dropOffOnTime : {
            timezone : 'EDT'
        },
        pickUpLocation : {
            name : '1600 Marina Rd',
            uuid : '1dd76a6c-db68-40ae-837e-1cd1efb0caa8',
            telecommunicationsNumber : {
                uuid : 'ca239dfc-8c55-4cc3-bed4-7e887c46c57a'
            },
            address : {
                uuid : '6a05340f-2683-432a-8f40-148b0cf57a6b'
            },
            directions : 'Upstairs... you can see it from the Bar',
            createdOn : '2017-03-06T00:11:11Z[UTC]',
            createdBy : 'Auditing-User',
            updatedOn : '2017-03-06T00:11:11Z[UTC]',
            updatedBy : 'Auditing-User',
            version : 0
        },
        dropOffLocation : {
            name : 'American Sleep Study',
            uuid : '5f200d2e-8019-4a71-951f-87d226ba3066',
            telecommunicationsNumber : {
                uuid : '8f00391e-d347-4877-9a20-a31d5af5cc8f'
            },
            address : {
                uuid : 'a687a55b-c538-4748-94c7-664bdb93fafc'
            },
            directions : '',
            createdOn : '2017-03-12T16:45:01.987Z[UTC]',
            createdBy : 'Update-Reservation-User-Placeholder',
            updatedOn : '2017-03-12T16:45:01.987Z[UTC]',
            updatedBy : 'Update-Reservation-User-Placeholder',
            version : 0
        },
        distance : 507,
        duration : 440,
        driver : {
            targetUuid : 'f1a3ce6c-2100-407f-af46-0af26473d3db',
            uuid : 'f1a3ce6c-2100-407f-af46-0af26473d3db'
        },
        driverUuid : 'f1a3ce6c-2100-407f-af46-0af26473d3db',
        transportationItemPassengers : [
        {
            sequenceId : 74,
            createdOn : '2017-03-12T16:45:02.041Z[UTC]',
            createdBy : 'Update-Reservation-User-Placeholder',
            updatedOn : '2017-03-12T16:45:02.041Z[UTC]',
            updatedBy : 'Update-Reservation-User-Placeholder',
            version : 0,
            transportationReservationItemUuid : '02e31ed3-4484-420f-8e27-852f46a92e06',
            personUuid : '84dcb81e-18e6-4ccc-acdf-5b2c0852f50e',
            person : {
                targetUuid : '84dcb81e-18e6-4ccc-acdf-5b2c0852f50e',
                uuid : '84dcb81e-18e6-4ccc-acdf-5b2c0852f50e'
            },
            passengerSpecialRequirements : [
            {
                sequenceId : 66,
                createdOn : '2017-03-12T16:45:02.07Z[UTC]',
                createdBy : 'Update-Reservation-User-Placeholder',
                updatedOn : '2017-03-12T16:45:02.07Z[UTC]',
                updatedBy : 'Update-Reservation-User-Placeholder',
                version : 0,
                transportationItemPassengerSequenceId : 74,
                specialRequirementTypeUuid : '68ff5eef-a53e-11e6-9155-125e1c46ef60',
                specialRequirementType : {
                    uuid : '68ff5eef-a53e-11e6-9155-125e1c46ef60'
                }
            }
            ]
        }
        ],
        transportationItemSpecialRequirements : [
        {
            sequenceId : 115,
            createdOn : '2017-03-12T16:45:02.078Z[UTC]',
            createdBy : 'Update-Reservation-User-Placeholder',
            updatedOn : '2017-03-12T16:45:02.078Z[UTC]',
            updatedBy : 'Update-Reservation-User-Placeholder',
            version : 0,
            transportationReservationItemUuid : '02e31ed3-4484-420f-8e27-852f46a92e06',
            specialRequirementTypeUuid : '68ff5eef-a53e-11e6-9155-125e1c46ef60',
            specialRequirementType : {
                uuid : '68ff5eef-a53e-11e6-9155-125e1c46ef60'
            },
            comment : null
        }
        ]
    }
    ],
    status : {
        id : 1,
        name : 'Pending'
    },
    personUuid : '95a825b7-cbdb-449b-8238-d956056b07cf',
    updatedOn : '2017-03-12T16:45:02.185Z[UTC]',
    updatedBy : 'Update-Reservation-User-Placeholder',
    version : 2
}];
