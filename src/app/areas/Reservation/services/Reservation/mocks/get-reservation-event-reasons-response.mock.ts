import {IReservationEventReasons} from '../../../../../store/Reservation/types/reservation-event-reasons.model';

export const getReservationEventReasonsResponse : Array<IReservationEventReasons> = [
    {
        cancelReasons: [
            {
                uuid: '6b8e7d14-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Caller Cancelled '
            },
            {
                uuid: '6b8e8584-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Caller Hung-Up'
            },
            {
                uuid: '8538d4f8-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Duplicate Trip'
            },
            {
                uuid: '8538dd04-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Member Expired'
            },
            {
                uuid: '94651400-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Holiday reschedule'
            },
            {
                uuid: '94651cd4-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Incomplete trip info'
            },
            {
                uuid: 'a1ae77a0-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Member no show'
            },
            {
                uuid: 'a1ae8024-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Unable to Transport'
            },
            {
                uuid: 'af4e547a-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'State of Emergency/Weather'
            },
            {
                uuid: 'af4e5d94-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'TP: no show/late'
            },
            {
                uuid: 'bb391bd0-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Canceled by Sending Provider'
            },
            {
                uuid: 'bb392558-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Canceled by Receiving Provider'
            },
            {
                uuid: 'c1b932f6-ffcd-11e6-9ad7-5425bd441bbb',
                name: 'Cancel from Auto Reminder/IVR'
            }
        ],
        updateReasons: [
            {
                uuid: 'f8ed6bb5-0516-11e7-8f14-00ac0399812e',
                name: 'LogistiCare Error'
            },
            {
                uuid: 'f8ed6bac-0516-11e7-8f14-00ac0399812e',
                name: 'Client Requested'
            },
            {
                uuid: 'eacda09d-0516-11e7-8f14-00ac0399812e',
                name: 'Facility Requested'
            },
            {
                uuid: '6b236d3d-0516-11e7-8f14-00ac0399812e',
                name: 'Member Requested'
            }
        ]
    }
];
