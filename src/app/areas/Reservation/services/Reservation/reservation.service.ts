import {Injectable} from '@angular/core';
import {URLSearchParams} from '@angular/http';
import {List} from 'immutable';
import {Observable} from 'rxjs/Observable';
import * as moment from 'moment';

import {BackendService} from '../../../../shared/services/Backend/backend.service';
import {
    fromApi,
    Reservation,
    toApi as newReservationFormToApi
} from '../../../../store/Reservation/types/reservation.model';
import {
    fromApi as addressLocationFromApi,
    AddressLocation
} from '../../../../store/types/address-location.model';
import {RESERVATION_MOCK} from './reservation.service.mock';
import {
    fromApi as eventReasonsFromApi,
    ReservationEventReasons
} from '../../../../store/Reservation/types/reservation-event-reasons.model';
import {ReservationRuleResponse} from '../../../../store/Reservation/types/reservation-rule-response.model';
import {
    fromApi as routeFromApi,
    IMassTransitRouteApi,
    MassTransitRoute
} from '../../../../store/types/mass-transit-route.model';
import {LatLong} from '../../../../store/types/latitude-longitude.model';

@Injectable()

/**
 * Implementation of ReservationService: returns summary data for display on Admin reservation
 */
export class ReservationService {
    /**
     * ReservationService constructor
     * @param backendService
     */
    constructor (private backendService : BackendService) {}

    /**
     * @description - subscribe upstream so the caller can unsubscribe and cancel the request if needed
     * @param reservation
     * @returns {any}
     */
    private mapReservationResponse(reservation : any) {
        if (!reservation.errors) {
            return Array.isArray(reservation) ? reservation[0] : reservation;
        }
        else {
            return Observable.throw(new Error(reservation.message));
        }
    }

    /**
     * @description - get a single reservation based on the reservation uuid
     * @param uuid - reservation uuid
     * @returns {Observable<any>}
     */
    getReservation(uuid : string) : Observable<any> {
        // since this is a root endpoint we have to extract its base url from the webpack config
        const handle  : string = 'getReservation',
              apiUrl  : string = API_CONFIG[handle] + uuid;

        // get reservation data
        return this.backendService.get(apiUrl, handle, RESERVATION_MOCK)
            .map(this.mapReservationResponse)
            .map(fromApi);
    }

    /**
     * @description - get all the reservations for a beneficiary
     * @param uuid - beneficiary uuid
     * @returns {Observable<Array<any>>}
     */
    getReservations(uuid : string) : Observable<Array<any>> {
        // since this is a root endpoint we have to extract its base url from the webpack config
        const handle  : string = 'getReservations',
              apiUrl  : string = API_CONFIG[handle] + uuid;

        // get reservation data
        return this.backendService.get(apiUrl, handle, RESERVATION_MOCK)
            .map(response => response);
    }

    /**
     * @description - get reservation leg mileage information
     * @param query
     * @returns {Observable<any>}
     */
    getReservationLegMileage(query : string) : Observable<any> {
        // since this is a root endpoint we have to extract its base url from the webpack config
        const handle  : string = 'getReservationLegMileage',
              apiUrl  : string = API_CONFIG[handle] + query;

        return this.backendService.get(apiUrl, handle, RESERVATION_MOCK)
            .map(this.mapReservationResponse);
    }

    /**
     * @description validates acceptable ride along escort/additional passenger for a reservation
     * @param payload
     * @returns {Observable<List<ReservationRuleResponse>>}
     */
    validateEscort(payload : any) : Observable<List<ReservationRuleResponse>> {
        // we need this to handle the POST properly
        const handle  : string = 'validateEscort',
              apiUrl  : string = API_CONFIG[handle],
              body    = payload;

        return Observable.create(observer => {
            this.backendService.post(apiUrl, handle, RESERVATION_MOCK, body)
                .first()
                .subscribe(response => {
                    // return observer
                    observer.next(List<ReservationRuleResponse>(response.map(value => new ReservationRuleResponse(value))));
                }, error => {
                    // return error
                    observer.error(error);
                });
        });
    }

    /**
     * @description validates acceptable MOT type for a reservation
     * @returns {Observable<List<ReservationRuleResponse>>}
     */
    validateModeOfTransportation(payload : any) : Observable<List<ReservationRuleResponse>> {
        // we need this to handle the POST properly
        const handle  : string = 'validateModeOfTransportation',
              apiUrl  : string = API_CONFIG[handle],
              body    = payload;

        return Observable.create(observer => {
            this.backendService.post(apiUrl, handle, RESERVATION_MOCK, body)
                .first()
                .subscribe(response => {
                    // return observer
                    observer.next(List<ReservationRuleResponse>(response.map(value => new ReservationRuleResponse(value))));
                }, error => {
                    // return error
                    observer.error(error);
                });
        });
    }

    /**
     * @description validates acceptable treatment type for a reservation
     * @param payload
     * @returns {Observable<List<ReservationRuleResponse>>}
     */
    validateTreatmentType(payload : any) : Observable<List<ReservationRuleResponse>> {
        // we need this to handle the POST properly
        const handle  : string = 'validateTreatmentType',
              apiUrl  : string = API_CONFIG[handle];

        return Observable.create(observer => {
            this.backendService.post(apiUrl, handle, RESERVATION_MOCK, payload)
                .first()
                .subscribe(response => {
                    // return observer
                    observer.next(List<ReservationRuleResponse>(response.map(value => new ReservationRuleResponse(value))));
                }, error => {
                    // return error
                    observer.error(error);
                });
        });
    }

    /**
     * @description validates acceptable pickup/dropoff locations for a reservation
     * @param payload
     * @returns {Observable<List<ReservationRuleResponse>>}
     */
    validateLocation(payload : any) : Observable<List<ReservationRuleResponse>> {
        // we need this to handle the POST properly
        const handle  : string = 'validateLocation',
              apiUrl  : string = API_CONFIG[handle],
              body    = payload;

        return Observable.create(observer => {
            this.backendService.post(apiUrl, handle, RESERVATION_MOCK, body)
                .first()
                .subscribe(response => {
                    // return observer
                    observer.next(List<ReservationRuleResponse>(response.map(value => new ReservationRuleResponse(value))));
                }, error => {
                    // return error
                    observer.error(error);
                });
        });
    }

    /**
     * @description calculates pickup time for a reservation
     * @param payload
     * @returns {Observable<any>}
     */
    getReservationPickupTime(payload : any) : Observable<any> {
        // we need this to handle the POST properly
        const handle  : string = 'getReservationPickupTime',
              apiUrl  : string = API_CONFIG[handle],
              body    = payload;

        return Observable.create(observer => {
            this.backendService.post(apiUrl, handle, RESERVATION_MOCK, body)
                .first()
                .subscribe(response => {
                    // return observer
                    observer.next(response.pickUpTime);
                }, error => {
                    // return error
                    observer.error(error);
                });
        });
    }

    /**
     * @description create a new Reservation Profile
     * @param {Reservation} reservation
     * @returns {Observable<any>}
     */
    createReservation(reservation : Reservation) : Observable<any> {
        // we need this to handle the POST properly
        const handle  : string = 'createReservation',
              apiUrl  : string = API_CONFIG[handle];

        // scrub the model
        const createFormPayload : any = newReservationFormToApi(reservation);

        // setup the payload
        const body    = JSON.stringify(createFormPayload);

        return Observable.create(observer => {
            this.backendService.post(apiUrl, handle, RESERVATION_MOCK, body)
                .first()
                .subscribe(response => {
                    // return observer
                    observer.next(response.referenceId);
                }, error => {
                    // return error
                    observer.error(error);
                });
        });
    }

    /**
     * @description create a new Reservation Profile
     * @param {Reservation} reservation
     * @returns {Observable<any>}
     */
    editReservation(reservation : Reservation) : Observable<any> {
        // we need this to handle the PUT properly
        const handle  : string = 'editReservation';

        let apiUrl : string = API_CONFIG[handle];

        // scrub the model
        const createFormPayload : any = newReservationFormToApi(reservation);

        // setup the payload
        const body    = JSON.stringify(createFormPayload);

        return Observable.create(observer => {
            // inject uuid into url
            apiUrl = apiUrl.replace('${uuid}', reservation.get('uuid'));

            // put reservation data
            this.backendService.put(apiUrl, handle, RESERVATION_MOCK, body)
                .first()
                .subscribe(response => {
                    // return observer
                    observer.next(response.referenceId);
                }, error => {
                    // return error
                    observer.error(error);
                });
        });
    }

    /**
     * @description gets the top 5 most recent pickup locations
     * @param {string} uuid - beneficiary uuid
     * @returns {Observable<List<AddressLocation>>}
     */
    getReservationPickUpLocations(uuid : string) : Observable<List<AddressLocation>> {
        const handle  : string = 'getReservationPickUpLocations';
        let   apiUrl  : string = API_CONFIG[handle];

        // inject uuid into url
        apiUrl = apiUrl.replace('${uuid}', uuid);

        return this.backendService.get(apiUrl, handle, RESERVATION_MOCK)
            .first()
            .map(response => List<AddressLocation>(response.map(addressLocationFromApi)));
    }

    /**
     * @description gets the top 5 most recent drop off locations
     * @param {string} uuid - beneficiary uuid
     * @returns {Observable<List<AddressLocation>>}
     */
    getReservationDropOffLocations(uuid : string) : Observable<List<AddressLocation>> {
        const handle  : string = 'getReservationDropOffLocations';
        let   apiUrl  : string = API_CONFIG[handle];

        // inject uuid into url
        apiUrl = apiUrl.replace('${uuid}', uuid);

        return this.backendService.get(apiUrl, handle, RESERVATION_MOCK)
            .first()
            .map(response => List<AddressLocation>(response.map(addressLocationFromApi)));
    }

    /**
     * gets the reservation event reasons
     * @returns {Observable<ReservationEventReasons>}
     */
    getReservationEventReasons() : Observable<ReservationEventReasons> {
        const handle = 'getReservationEventReasons';
        const apiUrl = API_CONFIG[handle];

        return this.backendService
            .get(apiUrl, handle, RESERVATION_MOCK)
            .first()
            .map(this.mapReservationResponse)
            .map(eventReasonsFromApi);
    }

    /**
     * gets mass transit routes
     *
     * @param {string} appointmentTime
     * @param {LatLong} dropOffLocation
     * @param {LatLong} pickupLocation
     *
     * @returns {Observable<List<MassTransitRoute>>}
     */
    getMassTransitRoutes(appointmentTime : any, dropOffLocation : LatLong, pickupLocation : LatLong) : Observable<List<MassTransitRoute>> {
        const handle = 'getMassTransitRoutes';
        const apiUrl = API_CONFIG[handle];
        const params = new URLSearchParams();

        params.set('appointmentTime', moment.isMoment(appointmentTime) ? appointmentTime.toISOString() : appointmentTime);
        params.set('dropOffLocationLat', dropOffLocation.get('lat'));
        params.set('dropOffLocationLong', dropOffLocation.get('lng'));
        params.set('pickUpLocationLat', pickupLocation.get('lat'));
        params.set('pickUpLocationLong', pickupLocation.get('lng'));

        return this.backendService
            .get(`${apiUrl}?${params.toString()}`, handle, RESERVATION_MOCK)
            .first()
            .map((routes : Array<IMassTransitRouteApi>) => {
                routes = Array.isArray(routes) ? routes : [];

                return List(
                    routes
                        .map(routeFromApi)
                        // remove elements that had a status that are not 'OK'
                        .filter(value => value !== undefined)
                );
            });
    }

    /**
     * sends a request to cancel a reservation
     * @param {Reservation} reservation
     * @returns {Observable<Reservation>}
     */
    cancelReservation(reservation : Reservation) : Observable<Reservation> {
        const handle = 'cancelReservation';
        const apiUrl = API_CONFIG[handle].replace('${uuid}', reservation.get('uuid'));
        const reservationPojo = newReservationFormToApi(reservation);
        const body = JSON.stringify(reservationPojo);

        return this.backendService
            .post(apiUrl, handle, RESERVATION_MOCK, body)
            .map(this.mapReservationResponse)
            .map(fromApi);
    }
}
