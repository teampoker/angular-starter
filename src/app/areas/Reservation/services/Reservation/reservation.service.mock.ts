import {getReservationEventReasonsResponse} from './mocks/get-reservation-event-reasons-response.mock';
import {getReservationResponse} from './mocks/get-reservation-response.mock';
import {getReservationsResponse} from './mocks/get-reservations-response.mock';
import {editReservationResponse} from './mocks/edit-reservation-response.mock';
import {getMassTransitRoutesResponse} from './mocks/get-mass-transit-routes.mock';

export const RESERVATION_MOCK : any = {

    /**
     * Edit Reservation Reasons
     */
    editReservationReasons : [{
        cancelReasons : [{
            name : 'I Felt like it',
            uuid : '123-456'
        }],
        editReasons : [{
            name : 'changed my mind',
            uuid : 'abc-123'
        }]
    }],

    /**
     * Get Reservation
     */
    getReservation : getReservationResponse,

    /**
     * Get Reservations
     */
    getReservations : getReservationsResponse,

    /**
     * cancel reservation
     */
    cancelReservation : getReservationResponse,

    cancelReservationWrap : getReservationResponse,

    /**
     * edit Reservation
     */
    editReservation : editReservationResponse,

    getReservationsWrap : getReservationsResponse,

    getReservationPickupTime : [{
        pickUpTime : '2017-02-14T17:55:24Z'
    }],

    validateModeOfTransportation : [{
        message : 'You\'re plan does not even cover bus tokens',
        status : 'DENIED',
        queueTaskItemType : {
            name : '',
            uuid : ''
        },
        serviceCoveragePlan : {
            name : '',
            uuid : ''
        }
    }, {
        message : 'Well maybe',
        status : 'PENDING',
        queueTaskItemType : {
            name : 'Sell your sister',
            uuid : '784-555-325-121212'
        },
        serviceCoveragePlan : {
            name : 'the dumb-ass plan',
            uuid : '753-555-543-121212'
        }
    }],

    validateEscort : [{
        message : 'This person doesn\'t like you and refuses to ride with you.',
        status : 'DENIED',
        queueTaskItemType : {
            name : '',
            uuid : ''
        },
        serviceCoveragePlan : {
            name : '',
            uuid : ''
        }
    }, {
        message : 'Well maybe',
        status : 'PENDING',
        queueTaskItemType : {
            name : 'Agrees to pay $100 for the company',
            uuid : '784-555-325-121212'
        },
        serviceCoveragePlan : {
            name : 'the dumb-ass plan',
            uuid : '753-555-543-121212'
        }
    }],

    validateLocation : [{
        message : 'What?? Hell no you can\'t go there. Take a taxi',
        status : 'DENIED',
        queueTaskItemType : {
            name : '',
            uuid : ''
        },
        serviceCoveragePlan : {
            name : '',
            uuid : ''
        }
    }, {
        message : 'Well maybe',
        status : 'PENDING',
        queueTaskItemType : {
            name : 'He really, really wants to go',
            uuid : '784-555-325-121212'
        },
        serviceCoveragePlan : {
            name : 'the dumb-ass plan',
            uuid : '753-555-543-121212'
        }
    }],

    validateTreatmentType : [{
        status : 'PENDING',
        message : 'I am unable to complete this trip because the request does not meet our advance notice requirement of two business days.',
        queueTaskItemType : {
            uuid : '1798aab7-f6d1-11e6-9155-125e1c46ef60',
            name : 'Waive Advance Notice'
        },
        serviceCoveragePlan : {
            uuid : 'd1802355-b687-11e6-9155-125e1c46ef60',
            name : 'Elderly Transportation Plan'
        }
    }, {
        status : 'PENDING',
        message : ' Please remember to schedule your appointments at least two days in advance. ',
        queueTaskItemType : {
            uuid : '1798aab7-f6d1-11e6-9155-125e1c46ef60',
            name : 'Send to Queue'
        },
        serviceCoveragePlan : {
            uuid : 'd1802355-b687-11e6-9155-125e1c46ef60',
            name : 'Elderly Transportation Plan'
        }
    }],

    /**
     * Get Pick Up/Drop Off Locations
     */
    getReservationDropOffLocations : [
        {
            name       : '5551 Bush River Rd',
            uuid       : '979816d7-fb2d-4118-bde7-58113daa0542',
            address    : {
                uuid       : '8886d510-443c-471f-b5ad-3e9283fd1319',
                address1   : '5551 Bush River Road',
                address2   : '',
                city       : 'Columbia',
                state      : 'SC',
                postalCode : '29212',
                latitude   : '34.0536097',
                longitude  : '-81.18497539999998'
            },
            directions : '',
            createdOn  : '2017-02-28T21:37:29Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-28T21:37:29Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }, {
            name       : '1600 Marina Rd',
            uuid       : '33862890-b514-41f9-aed0-ab96e023a3c6',
            address    : {
                uuid       : '10635405-9e6c-409f-a01b-0900ac92cf69',
                address1   : '1600 Marina Road',
                address2   : '',
                city       : 'Irmo',
                state      : 'SC',
                postalCode : '29063',
                latitude   : '34.1159089',
                longitude  : '-81.25223629999999'
            },
            directions : '',
            createdOn  : '2017-02-28T21:37:29Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-28T21:37:29Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }, {
            name       : '1600 Pennsylvania Ave NW',
            uuid       : 'e1200bcc-2376-403e-8ca2-065047c39f46',
            address    : {
                uuid       : '13daf2f9-8171-4e4a-80d6-0250a0975d5e',
                address1   : '1600 Pennsylvania Avenue Northwest',
                address2   : '',
                city       : 'Washington',
                state      : 'DC',
                postalCode : '20500',
                latitude   : '38.8976758',
                longitude  : '-77.03648229999999'
            },
            directions : '',
            createdOn  : '2017-02-28T21:37:29Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-28T21:37:29Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }, {
            name       : '555 Chadford Rd',
            uuid       : 'de9b6ddf-c47d-44aa-9526-ad35352cdb99',
            address    : {
                uuid       : 'a51f7b3f-b9c6-4401-bb07-07f073cef67c',
                address1   : '555 Chadford Road',
                address2   : '',
                city       : 'Irmo',
                state      : 'SC',
                postalCode : '29063',
                latitude   : '34.101336',
                longitude  : '-81.197089'
            },
            directions : '',
            createdOn  : '2017-02-27T20:15:43Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-27T20:15:43Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }, {
            name       : '887 Bentley Dr',
            uuid       : 'ced4e5b7-cb0e-4a7e-8bc1-8ca5416af696',
            address    : {
                uuid       : 'e853a603-5120-411c-9e2f-03c6a968e85f',
                address1   : '887 Bentley Drive',
                address2   : '',
                city       : 'Lexington',
                state      : 'SC',
                postalCode : '29072',
                latitude   : '34.010769',
                longitude  : '-81.22107199999999'
            },
            directions : '',
            createdOn  : '2017-02-28T18:11:40Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-28T18:11:40Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }
    ],

    getReservationPickUpLocations : [
        {
            name       : '555 Chadford Rd',
            uuid       : 'de9b6ddf-c47d-44aa-9526-ad35352cdb99',
            address    : {
                uuid       : 'a51f7b3f-b9c6-4401-bb07-07f073cef67c',
                address1   : '555 Chadford Road',
                address2   : '',
                city       : 'Irmo',
                state      : 'SC',
                postalCode : '29063',
                latitude   : '34.101336',
                longitude  : '-81.197089'
            },
            directions : '',
            createdOn  : '2017-02-27T20:15:43Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-27T20:15:43Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }, {
            name       : '1600 Pennsylvania Ave NW',
            uuid       : 'e1200bcc-2376-403e-8ca2-065047c39f46',
            address    : {
                uuid       : '13daf2f9-8171-4e4a-80d6-0250a0975d5e',
                address1   : '1600 Pennsylvania Avenue Northwest',
                address2   : '',
                city       : 'Washington',
                state      : 'DC',
                postalCode : '20500',
                latitude   : '38.8976758',
                longitude  : '-77.03648229999999'
            },
            directions : '',
            createdOn  : '2017-02-28T21:37:29Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-28T21:37:29Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }, {
            name       : '5551 Bush River Rd',
            uuid       : '979816d7-fb2d-4118-bde7-58113daa0542',
            address    : {
                uuid       : '8886d510-443c-471f-b5ad-3e9283fd1319',
                address1   : '5551 Bush River Road',
                address2   : '',
                city       : 'Columbia',
                state      : 'SC',
                postalCode : '29212',
                latitude   : '34.0536097',
                longitude  : '-81.18497539999998'
            },
            directions : '',
            createdOn  : '2017-02-28T21:37:29Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-28T21:37:29Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }, {
            name       : '1600 Marina Rd',
            uuid       : '33862890-b514-41f9-aed0-ab96e023a3c6',
            address    : {
                uuid       : '10635405-9e6c-409f-a01b-0900ac92cf69',
                address1   : '1600 Marina Road',
                address2   : '',
                city       : 'Irmo',
                state      : 'SC',
                postalCode : '29063',
                latitude   : '34.1159089',
                longitude  : '-81.25223629999999'
            },
            directions : '',
            createdOn  : '2017-02-28T21:37:29Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-28T21:37:29Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }, {
            name       : '5580 Forest Dr',
            uuid       : 'da447ecb-77fa-40c8-93aa-c62d69f49f60',
            address    : {
                uuid       : 'aa5b03ac-ae55-4d68-bcb4-a0b6d0591d8f',
                address1   : '5580 Forest Drive',
                address2   : '',
                city       : 'Columbia',
                state      : 'SC',
                postalCode : '29206',
                latitude   : '34.0208065',
                longitude  : '-80.9481563'
            },
            directions : '',
            createdOn  : '2017-02-28T18:11:40Z[UTC]',
            createdBy  : 'SERVICE_DEFAULT',
            updatedOn  : '2017-02-28T18:11:40Z[UTC]',
            updatedBy  : 'SERVICE_DEFAULT',
            version    : 0
        }
    ],

    editReservationReasonsWrap : {
        cancelReasons: [{
            name : 'I Felt like it',
            uuid : '123-456'
        }],
        editReasons: [{
            name : 'changed my mind',
            uuid : 'abc-123'
        }]
    },

    getReservationPickupTimeWrap : {
        pickUpTime : '2017-02-14T17:55:24Z'
    },

    validateLocationWrap : {
        message : 'What?? Hell no you can\'t go there. Take a taxi',
        status : 'DENIED',
        queueTaskItemType : {
            name : 'He really, really wants to go',
            uuid : '784-555-325-121212'
        },
        serviceCoveragePlan : {
            name : '',
            uuid : ''
        }
    },

    validateTreatmentTypeWrap : {
        message : 'I am unable to complete this trip because the request does not meet our advance notice requirement of two business days.   Please remember to schedule your appointments at least two days in advance.',
        status : 'PENDING',
        queueTaskItemType : {
            name : 'He really, really wants to go',
            uuid : '976-555-555-121212'
        },
        serviceCoveragePlan : {
            name : 'the dumb-ass plan',
            uuid : '753-555-543-121212'
        }
    },

    getReservationDropOffLocationsWrap : {
        name       : '5551 Bush River Rd',
        uuid       : '979816d7-fb2d-4118-bde7-58113daa0542',
        address    : {
            uuid       : '8886d510-443c-471f-b5ad-3e9283fd1319',
            address1   : '5551 Bush River Road',
            address2   : '',
            city       : 'Columbia',
            state      : 'SC',
            postalCode : '29212',
            latitude   : '34.0536097',
            longitude  : '-81.18497539999998'
        },
        directions : '',
        createdOn  : '2017-02-28T21:37:29Z[UTC]',
        createdBy  : 'SERVICE_DEFAULT',
        updatedOn  : '2017-02-28T21:37:29Z[UTC]',
        updatedBy  : 'SERVICE_DEFAULT',
        version    : 0
    },

    getReservationPickUpLocationsWrap : {
        name       : '555 Chadford Rd',
        uuid       : 'de9b6ddf-c47d-44aa-9526-ad35352cdb99',
        address    : {
            uuid       : 'a51f7b3f-b9c6-4401-bb07-07f073cef67c',
            address1   : '555 Chadford Road',
            address2   : '',
            city       : 'Irmo',
            state      : 'SC',
            postalCode : '29063',
            latitude   : '34.101336',
            longitude  : '-81.197089'
        },
        directions : '',
        createdOn  : '2017-02-27T20:15:43Z[UTC]',
        createdBy  : 'SERVICE_DEFAULT',
        updatedOn  : '2017-02-27T20:15:43Z[UTC]',
        updatedBy  : 'SERVICE_DEFAULT',
        version    : 0
    },

    /**
     * edit Reservation wrap
     */
    editReservationWrap : editReservationResponse[0],

    /**
     * Get Leg Mileage
     */
    getReservationLegMileage : [{
        distanceText  : '788 mi',
        distanceValue : 787.53,
        durationText  : '11 hours 50 mins',
        durationValue : 710,
        status : 'OK'
    }],

    getReservationLegMileageWrap : {
        distanceText  : '788 mi',
        distanceValue : 787.53,
        durationText  : '11 hours 50 mins',
        durationValue : 710,
        status : 'OK'
    },

    /**
     * Create Reservation
     */
    createReservation : [
        {
            confirmationNumber : '123-12-12345678-12'
        }
    ],

    createReservationWrap : {
        confirmationNumber : '123-12-12345678-12'
    },

    /**
     * GET Event Reasons
     */

    getReservationEventReasons : getReservationEventReasonsResponse,

    getReservationEventReasonsWrap : getReservationEventReasonsResponse,

    getMassTransitRoutes : getMassTransitRoutesResponse,

    getMassTransitRoutesWrap : getMassTransitRoutesResponse
};
