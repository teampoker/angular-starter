import {Injectable} from '@angular/core';
import {
    Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';

import {ReservationActions} from '../../../../store/Reservation/actions/reservation.actions';
import {SearchActions} from '../../../../store/Search/search.actions';
import {BeneficiaryConnectionsActions} from '../../../../store/Beneficiary/actions/beneficiary-connections.actions';

@Injectable()

/**
 * Implementation of ReservationResolveService : ensure Beneficiary Profile Dashboard route loads with the correct BeneficiaryState values
 */
export class ReservationResolveService implements Resolve<boolean> {
    /**
     * ReservationResolveService constructor
     * @param reservationActions
     * @param searchActions
     * @param beneficiaryConnectionsActions
     */
    constructor(
        private reservationActions              : ReservationActions,
        private searchActions                   : SearchActions,
        private beneficiaryConnectionsActions   : BeneficiaryConnectionsActions
    ) {}

    resolve(route : ActivatedRouteSnapshot) : boolean {
        // clear relevant people search state
        this.searchActions.resetPeopleSearchState();

        // update any needed dropdown types
        this.beneficiaryConnectionsActions.updateBeneficiaryConnectionDropdownTypes();

        // init ReservationState for UI view
        this.reservationActions.initReservationState();

        // grab the event reasons
        this.reservationActions.fetchReservationEventReasons();

        // check if edit reservation and load the reservation to be edited
        if (route.params['reservationUUID']) {
            this.reservationActions.getEditReservation(route.params['reservationUUID']);
        }
        // this.reservationActions.getReservation(route.params['reservationUUID']);

        return true;
    }
}
