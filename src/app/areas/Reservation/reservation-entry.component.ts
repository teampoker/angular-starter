import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

import {NavActions} from '../../store/Navigation/nav.actions';
import {EnumNavOption} from '../../store/Navigation/types/nav-option.model';

@Component({
    templateUrl     : 'reservation-entry.component.html',
    styleUrls       : ['reservation-entry.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for ReservationEntryComponent: responsible for reservation page layout
 */
export class ReservationEntryComponent {
    /**
     * ReservationEntryComponent constructor
     * @param navActions
     */
    constructor (private navActions : NavActions) {}

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // set active epic to reservation
        this.navActions.updateActiveNavState(EnumNavOption.RESERVATION);
    }
}
