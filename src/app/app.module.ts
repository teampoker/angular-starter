/**
 * Core Angular Dependencies
 */
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Http} from '@angular/http';

/**
 * ng2-translate
 */
import {
    TranslateModule,
    TranslateLoader,
    TranslateStaticLoader,
    TranslateService
} from 'ng2-translate/ng2-translate';

/**
 * ng2redux
 */
import {
    DevToolsExtension,
    NgReduxModule,
    NgRedux
} from '@angular-redux/store';

/**
 * ngx-toastr
 */
import {ToastrModule} from 'ngx-toastr';

/**
 * Idle timer
 */
import {NgIdleKeepaliveModule} from '@ng-idle/keepalive';

/**
 * Http Interception Modules
 */
import {
    HttpInterceptorModule,
    HttpInterceptor
} from 'angular2-http-interceptor';

/**
 * Root App Component
 */
import {AppComponent} from './app.component';

/**
 * Routing Config
 */
import {AppRoutingModule} from './app-routing.module';
import {NoContentComponent} from './no-content.component';

/**
 * Shared Application Module
 */
import {SharedModule} from './shared/shared.module';

/**
 * Epic Modules
 */
import {DashboardModule} from './areas/Dashboard/dashboard.module';

/**
 * static translations JSON file
 */
import {APP_TRANSLATIONS} from './app.translations';

/**
 * Redux middleware
 */
import {
    IAppStore,
    enhancers,
    reimmutify,
    ROOT_REDUCER,
    middleware
} from './store/app-store';

/**
 * Actions and Selectors
 */
import {BeneficiaryActions} from './store/Beneficiary/actions/beneficiary.actions';
import {BeneficiaryInfoActions} from './store/Beneficiary/actions/beneficiary-info.actions';
import {BeneficiaryContactMechanismActions} from './store/Beneficiary/actions/beneficiary-contact-mechanisms.actions';
import {BeneficiarySpecialRequirementsActions} from './store/Beneficiary/actions/beneficiary-special-requirements.actions';
import {BeneficiaryConnectionsActions} from './store/Beneficiary/actions/beneficiary-connections.actions';
import {BeneficiaryMedicalConditionsActions} from './store/Beneficiary/actions/beneficiary-medical-conditions.actions';
import {BeneficiaryPlansActions} from './store/Beneficiary/actions/beneficiary-plans.actions';
import {BeneficiaryStateSelectors} from './store/Beneficiary/selectors/beneficiary.selectors';
import {BeneficiaryInfoStateSelectors} from './store/Beneficiary/selectors/beneficiary-info.selectors';
import {BeneficiaryContactMechanismsStateSelectors} from './store/Beneficiary/selectors/beneficiary-contact-mechanisms.selectors';
import {BeneficiarySpecialRequirementsSelectors} from './store/Beneficiary/selectors/beneficiary-special-requirements.selectors';
import {BeneficiaryConnectionsStateSelectors} from './store/Beneficiary/selectors/beneficiary-connections.selectors';
import {DashboardStateSelectors} from './store/Dashboard/dashboard.selectors';
import {BeneficiaryMedicalConditionsStateSelectors} from './store/Beneficiary/selectors/beneficiary-medical-conditions.selectors';
import {BeneficiaryPlansStateSelectors} from './store/Beneficiary/selectors/beneficiary-plans.selectors';
import {DashboardActions} from './store/Dashboard/dashboard.actions';
import {NavActions} from './store/Navigation/nav.actions';
import {NavStateSelectors} from './store/Navigation/nav.selectors';
import {ReservationActions} from './store/Reservation/actions/reservation.actions';
import {ReservationStateSelectors} from './store/Reservation/selectors/reservation.selectors';
import {SearchActions} from './store/Search/search.actions';
import {SearchStateSelectors} from './store/Search/search.selectors';
import {UserActions} from './store/User/user.actions';
import {UserStateSelectors} from './store/User/user.selectors';
import {MetaDataTypesActions} from './store/MetaDataTypes/meta-data-types.actions';
import {MetaDataTypesSelectors} from './store/MetaDataTypes/meta-data-types.selectors';
import {ExceptionQueueActions} from './store/ExceptionQueue/actions/exception-queue.actions';
import {ExceptionQueueStateSelectors} from './store/ExceptionQueue/selectors/exception-queue.selector';
import {SessionActions} from './store/AppConfig/session.actions';
import {FormsStateSelectors} from './store/Forms/forms.selectors';
import {FormsActions} from './store/Forms/forms.actions';
import {ReservationHistoryActions} from './store/Reservation/actions/reservation-history.actions';
import {ReservationHistorySelectors} from './store/Reservation/selectors/reservation-history.selectors';
import {ReservationCardsActions} from './store/Reservation/actions/reservation-cards.actions';

/**
 * shared services
 */
import {UserNotificationService} from './shared/services/UserNotification/user-notification.service';
import {UserNotificationComponent} from './shared/components/UserNotification/user-notification.component';
import {APIMockService} from './shared/services/Mock/api-mock.service';
import {BackendService} from './shared/services/Backend/backend.service';
import {UserService} from './shared/services/User/user.service';
import {MetaDataTypesService} from './shared/services/MetaDataTypes/meta-data-types.service';
import {SearchService} from './shared/services/Search/search.service';
import {StandardHttpInterceptor} from './shared/services/Backend/standard-http-interceptor';
import {SessionManagerService} from './shared/services/AppConfig/session-manager.service';

/**
 * area specific services
 */
import {BeneficiaryService} from './areas/Beneficiary/services/Beneficiary/beneficiary.service';
import {BeneficiaryFormsService} from './areas/Beneficiary/services/BeneficiaryForms/beneficiary-forms.service';
import {DashboardService} from './areas/Dashboard/services/Dashboard/dashboard.service';
import {ExceptionQueueService} from './areas/ExceptionQueue/services/ExceptionQueue/exception-queue.service';
import {ReservationService} from './areas/Reservation/services/Reservation/reservation.service';

/**
 * components required by AppComponent template
 */
import {LeftNavigationComponent} from './shared/components/LeftNavigation/left-navigation.component';
import {TopNavigationComponent} from './shared/components/TopNavigation/top-navigation.component';
import {LogoutComponent} from './shared/components/Logout/logout.component';
import {AppStateActions} from './store/App/app-state.actions';
import {AppStateSelectors} from './store/App/app-state.selectors';
import {AppService} from './shared/services/App/app.service';

@NgModule({
    bootstrap       : [AppComponent],
    imports         : [
        // browser specific renderers and core directives e.g. ngIf, ngFor, etc
        BrowserModule,

        // animations
        BrowserAnimationsModule,

        // ng2-redux
        NgReduxModule,

        // ng2-translate
        TranslateModule.forRoot({
            provide     : TranslateLoader,
            useFactory  : (http : Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
            deps        : [Http]
        }),

        // shared application resources
        SharedModule,

        // synchronous epic modules
        DashboardModule,

        // main application routing config
        AppRoutingModule,

        // idle timer
        NgIdleKeepaliveModule.forRoot(),

        // intercepting Http requests
        HttpInterceptorModule.withInterceptors([{
            provide     : HttpInterceptor,
            useClass    : StandardHttpInterceptor,
            multi       : true
        }]),

        // Toastr messages, user notifications
        ToastrModule.forRoot()

        // Busy module for spinners / app busy UI stuff
        // BusyModule.forRoot(new BusyConfig({
        //     message: 'Loading...',
        //     backdrop: true,
        //     minDuration: 600,
        //     template: '<div>{{message}}</div>'
        // }))
   ],
    declarations    : [
        AppComponent,
        NoContentComponent,
        LogoutComponent,
        LeftNavigationComponent,
        TopNavigationComponent
   ],
    providers   : [
        APIMockService,
        AppService,
        BackendService,
        UserService,
        UserNotificationService,
        MetaDataTypesService,
        BeneficiaryService,
        DashboardService,
        SearchService,
        AppStateActions,
        ExceptionQueueService,
        SessionManagerService,
        BeneficiaryFormsService,
        BeneficiaryActions,
        BeneficiaryInfoActions,
        BeneficiaryContactMechanismActions,
        BeneficiarySpecialRequirementsActions,
        BeneficiaryConnectionsActions,
        BeneficiaryMedicalConditionsActions,
        BeneficiaryPlansActions,
        AppStateSelectors,
        BeneficiaryStateSelectors,
        BeneficiaryInfoStateSelectors,
        BeneficiaryContactMechanismsStateSelectors,
        BeneficiarySpecialRequirementsSelectors,
        BeneficiaryConnectionsStateSelectors,
        BeneficiaryMedicalConditionsStateSelectors,
        BeneficiaryPlansStateSelectors,
        FormsStateSelectors,
        FormsActions,
        DashboardActions,
        DashboardStateSelectors,
        NavActions,
        NavStateSelectors,
        ReservationActions,
        ReservationStateSelectors,
        SearchActions,
        SearchStateSelectors,
        UserActions,
        UserStateSelectors,
        MetaDataTypesActions,
        MetaDataTypesSelectors,
        ExceptionQueueActions,
        ExceptionQueueStateSelectors,
        SessionActions,
        SessionManagerService,
        ReservationService,
        ReservationHistoryActions,
        ReservationHistorySelectors,
        ReservationCardsActions
    ],
    // declared dynamic component for alerts
    entryComponents: [
        UserNotificationComponent
    ]
})

/**
 * Implementation of AppModule: Root App Module for entire application
 */
export class AppModule {
    /**
     * AppModule constructor
     */
    constructor(
        private translate   : TranslateService,
        private devTools    : DevToolsExtension,
        private ngRedux     : NgRedux<IAppStore>
    ) {
        // force ng2-translate to use english for now
        translate.setTranslation('en', APP_TRANSLATIONS);

        translate.use('en');

        // tslint:disable-next-line: no-unused-variable
        const enh = (ENV.indexOf('debug') !== -1 && devTools.isEnabled()) ?
            [... enhancers, devTools.enhancer({
                deserializeState : reimmutify
            })] :
            enhancers;

        ngRedux.configureStore(ROOT_REDUCER, {}, middleware, enhancers);
    }
}
