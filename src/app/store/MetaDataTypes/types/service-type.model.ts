import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IServiceCategories,
    ServiceCategories
} from './service-categories.model';

export interface IServiceType {
    name              : string;
    uuid              : string;
    serviceCategories : Array<IServiceCategories>;
}

export const SERVICE_TYPE = Record({
    name              : '',
    uuid              : '',
    serviceCategories : List<ServiceCategories>()
});

export class ServiceType extends SERVICE_TYPE {
    name              : string;
    uuid              : string;
    serviceCategories : List<ServiceCategories>;

    constructor(values? : ServiceType | IServiceType) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ServiceType) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCategories
                convertedValues = convertedValues.set('serviceCategories', List(convertedValues.get('serviceCategories', List()).sortBy(ordinality => ordinality.get('ordinality')).map(value => new ServiceCategories(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
