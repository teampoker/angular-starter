import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IServiceOfferings,
    ServiceOfferings
} from './service-offerings.model';

export interface IServiceCategories {
    name                : string;
    uuid                : string;
    ordinality?         : number;
    serviceOfferings    : Array<IServiceOfferings>;
}

export const SERVICE_CATEGORIES = Record({
    name                : '',
    uuid                : '',
    ordinality          : 0,
    serviceOfferings    : List<ServiceOfferings>()
});

export class ServiceCategories extends SERVICE_CATEGORIES {
    name                : string;
    uuid                : string;
    ordinality?         : number;
    serviceOfferings    : List<ServiceOfferings>;

    constructor(values? : ServiceCategories | IServiceCategories) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ServiceCategories) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCategories
                if (convertedValues.getIn(['serviceOfferings', 0, 'uuid'])) {
                    convertedValues = convertedValues.set('serviceOfferings', List(convertedValues.get('serviceOfferings', []).map(value => new ServiceOfferings(value))));
                }
                else {
                    convertedValues = convertedValues.set('serviceOfferings', List(convertedValues.get('serviceOfferings', []).map(value => new ServiceOfferings(value))));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
