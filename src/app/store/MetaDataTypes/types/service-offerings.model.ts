import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

export interface IServiceOfferings {
    name                 : string;
    uuid                 : string;
    serviceOfferingType  : INameUuid;
}

export const SERVICE_OFFERINGS = Record({
    name                 : '',
    uuid                 : '',
    serviceOfferingType  : new NameUuid()
});

export class ServiceOfferings extends SERVICE_OFFERINGS {
    name                 : string;
    uuid                 : string;
    serviceOfferingType  : NameUuid;

    constructor(values? : ServiceOfferings | IServiceOfferings) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ServiceOfferings) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCategoryType
                convertedValues = convertedValues.set('serviceOfferingType', new NameUuid(convertedValues.get('serviceOfferingType')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
