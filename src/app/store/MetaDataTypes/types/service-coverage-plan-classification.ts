import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IServiceCoveragePlanClassification {
    id                              : string;
    value                           : string;
    ordinality                      : number;
    maximum                         : number;
    minimum                         : number;
    validationFormat                : string;
    classificationType              : IKeyValuePair;
    benefitClassificationValues     : Array<IKeyValuePair>;
}

export const SERVICE_COVERAGE_PLAN = Record({
    id                              : '',
    value                           : '',
    ordinality                      : 0,
    maximum                         : 0,
    minimum                         : 0,
    validationFormat                : '',
    classificationType              : new KeyValuePair(),
    benefitClassificationValues     : List<KeyValuePair>()
});

const apiMap = Map({
    uuid                            : 'id',
    name                            : 'value',
    ordinality                      : 'ordinality',
    maximum                         : 'maximum',
    minimum                         : 'minimum',
    validationFormat                : 'validationFormat',
    classificationType              : 'classificationType',
    benefitClassificationValues     : 'benefitClassificationValues'
});

export class ServiceCoveragePlanClassification extends SERVICE_COVERAGE_PLAN {
    id                              : string;
    value                           : string;
    ordinality                      : number;
    maximum                         : number;
    minimum                         : number;
    validationFormat                : string;
    classificationType              : KeyValuePair;
    benefitClassificationValues     : List<KeyValuePair>;

    constructor(values? : ServiceCoveragePlanClassification | IServiceCoveragePlanClassification) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ServiceCoveragePlanClassification) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // classificationType
                if (convertedValues.getIn(['classificationType', 'uuid'])) {
                    convertedValues = convertedValues.set('classificationType', KeyValuePair.fromApi(convertedValues.get('classificationType')));
                }
                else {
                    convertedValues = convertedValues.set('classificationType', new KeyValuePair(convertedValues.get('classificationType')));
                }

                // benefitClassificationValues
                if (convertedValues.getIn(['benefitClassificationValues', 0, 'uuid'])) {
                    convertedValues = convertedValues.set('benefitClassificationValues', List(convertedValues.get('benefitClassificationValues', []).map(value => KeyValuePair.fromApi(value))));
                }
                else {
                    convertedValues = convertedValues.set('benefitClassificationValues', List(convertedValues.get('benefitClassificationValues', []).map(value => new KeyValuePair(value))));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }

    static fromApi(rawObject : any) : ServiceCoveragePlanClassification {
        const newObject : any = {};

        if (Map.isMap(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject.get(apiKey);
            });
        }
        else {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject[apiKey];
            });
        }

        return new ServiceCoveragePlanClassification(newObject);
    }
}
