import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IContactMechanismType {
    id              : string;
    value           : string;
    purposeTypes    : Array<IKeyValuePair>;
}

export const CONTACT_MECHANISM_TYPE = Record({
    id              : '',
    value           : '',
    purposeTypes    : List<KeyValuePair>()
});

const apiMap = Map({
    uuid            : 'id',
    name            : 'value',
    purposeTypes    : 'purposeTypes'
});

export class ContactMechanismType extends CONTACT_MECHANISM_TYPE {
    id              : string;
    value           : string;
    purposeTypes    : List<KeyValuePair>;

    constructor(values? : ContactMechanismType | IContactMechanismType) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ContactMechanismType) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // purposeTypes
                if (convertedValues.getIn(['purposeTypes', 0, 'uuid'])) {
                    convertedValues = convertedValues.set('purposeTypes', List(convertedValues.get('purposeTypes', []).map(value => KeyValuePair.fromApi(value))));
                }
                else {
                    convertedValues = convertedValues.set('purposeTypes', List(convertedValues.get('purposeTypes', []).map(value => new KeyValuePair(value))));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }

    static fromApi(rawObject : any) : ContactMechanismType {
        const newObject : any = {};

        if (Map.isMap(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject.get(apiKey);
            });
        }
        else {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject[apiKey];
            });
        }

        return new ContactMechanismType(newObject);
    }
}
