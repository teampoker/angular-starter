import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IServiceCoveragePlan,
    ServiceCoveragePlan
} from './service-coverage-plan.model';

export interface IServiceCoverageOrganizationType {
    id                      : string;
    value                   : string;
    serviceCoveragePlans    : Array<IServiceCoveragePlan>;
}

export const SERVICE_COVERAGE_ORGANIZATION_TYPE = Record({
    id                      : '',
    value                   : '',
    serviceCoveragePlans    : List<ServiceCoveragePlan>()
});

const apiMap = Map({
    uuid                  : 'id',
    name                  : 'value',
    serviceCoveragePlans  : 'serviceCoveragePlans'
});

export class ServiceCoverageOrganizationType extends SERVICE_COVERAGE_ORGANIZATION_TYPE {
    id                      : string;
    value                   : string;
    serviceCoveragePlans    : List<ServiceCoveragePlan>;

    constructor(values? : ServiceCoverageOrganizationType | IServiceCoverageOrganizationType) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ServiceCoverageOrganizationType) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoveragePlans
                if (convertedValues.getIn(['serviceCoveragePlans', 0, 'uuid'])) {
                    convertedValues = convertedValues.set('serviceCoveragePlans', List(convertedValues.get('serviceCoveragePlans', []).map(value => ServiceCoveragePlan.fromApi(value))));
                }
                else {
                    convertedValues = convertedValues.set('serviceCoveragePlans', List(convertedValues.get('serviceCoveragePlans', []).map(value => new ServiceCoveragePlan(value))));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }

    static fromApi(rawObject : any) : ServiceCoverageOrganizationType {
        const newObject : any = {};

        if (Map.isMap(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject.get(apiKey);
            });
        }
        else {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject[apiKey];
            });
        }

        return new ServiceCoverageOrganizationType(newObject);
    }
}
