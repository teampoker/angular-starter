import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IServiceCategoryType {
    name     : string;
    uuid     : string;
}

export const SERVICE_CATEGORY_TYPE = Record({
    name     : '',
    uuid     : ''
});

export class ServiceCategoryType extends SERVICE_CATEGORY_TYPE {
    name     : string;
    uuid     : string;

    constructor(values? : ServiceCategoryType | IServiceCategoryType) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ServiceCategoryType) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
