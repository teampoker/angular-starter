import {
    fromJS,
    List,
    Record
} from 'immutable';
import {isObject} from 'rxjs/util/isObject';

import {LanguageType} from './language-type.model';
import {ContactMechanismType} from './contact-mechanism-type.model';
import {ServiceCoverageOrganizationType} from './service-coverage-organization-type.model';
import {ServiceType} from './service-type.model';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {NameUuid} from '../../types/name-uuid.model';

export const META_DATA_TYPES_STATE = Record({
    genderTypes                             : List<KeyValuePair>(),
    medicalConditionTypes                   : List<KeyValuePair>(),
    personConnectionTypes                   : List<KeyValuePair>(),
    physicalCharacteristicTypes             : List<KeyValuePair>(),
    serviceCoveragePlanClassificationTypes  : List<KeyValuePair>(),
    solicitationIndicatorTypes              : List<KeyValuePair>(),
    specialRequirementTypes                 : List<KeyValuePair>(),
    salutationTypes                         : List<KeyValuePair>(),
    heightInFeetTypes                       : List<KeyValuePair>(),
    heightInInchesTypes                     : List<KeyValuePair>(),
    languages                               : List<LanguageType>(),
    contactMechanismTypes                   : List<ContactMechanismType>(),
    serviceCoverageOrganizations            : List<ServiceCoverageOrganizationType>(),
    modeOfTransportation                    : List<ServiceType>(),
    treatmentType                           : List<ServiceType>(),
    queueTaskItemStatus                     : List<NameUuid>(),
    queueTaskItemStatusReason               : List<NameUuid>(),
    queueTaskItemType                       : List<NameUuid>(),
    queueTaskStatus                         : List<NameUuid>(),
    queueTaskType                           : List<NameUuid>()
});

export class MetaDataTypesState extends META_DATA_TYPES_STATE {
    genderTypes                             : List<KeyValuePair>;
    medicalConditionTypes                   : List<KeyValuePair>;
    personConnectionTypes                   : List<KeyValuePair>;
    physicalCharacteristicTypes             : List<KeyValuePair>;
    serviceCoveragePlanClassificationTypes  : List<KeyValuePair>;
    solicitationIndicatorTypes              : List<KeyValuePair>;
    specialRequirementTypes                 : List<KeyValuePair>;
    salutationTypes                         : List<KeyValuePair>;
    heightInFeetTypes                       : List<KeyValuePair>;
    heightInInchesTypes                     : List<KeyValuePair>;
    languages                               : List<LanguageType>;
    contactMechanismTypes                   : List<ContactMechanismType>;
    serviceCoverageOrganizations            : List<ServiceCoverageOrganizationType>;
    modeOfTransportation                    : List<ServiceType>;
    treatmentType                           : List<ServiceType>;
    queueTaskItemStatus                     : List<NameUuid>;
    queueTaskItemStatusReason               : List<NameUuid>;
    queueTaskItemType                       : List<NameUuid>;
    queueTaskStatus                         : List<NameUuid>;
    queueTaskType                           : List<NameUuid>;

    constructor() {
        // call parent constructor
        super();
    }
}

/**
 * transform raw JSON from local storage into
 * MetaDataTypesState Immutable Record type
 *
 * @param rawObject
 *
 * @returns {MetaDataTypesState}
 */
export function fromStorage(rawObject : any) : MetaDataTypesState {
    let metaDataTypes : MetaDataTypesState = new MetaDataTypesState();

    if (isObject(rawObject) && !Array.isArray(rawObject)) {
        // convert POJO to Immutable
        rawObject = fromJS(rawObject);

        metaDataTypes = new MetaDataTypesState().withMutations(newMetaData => newMetaData
            .set('genderTypes', rawObject.get('genderTypes', List()).map(value => new KeyValuePair(value)))
            .set('medicalConditionTypes', rawObject.get('medicalConditionTypes', List()).map(value => new KeyValuePair(value)))
            .set('personConnectionTypes', rawObject.get('personConnectionTypes', List()).map(value => new KeyValuePair(value)))
            .set('physicalCharacteristicTypes', rawObject.get('physicalCharacteristicTypes', List()).map(value => new KeyValuePair(value)))
            .set('serviceCoveragePlanClassificationTypes', rawObject.get('serviceCoveragePlanClassificationTypes', List()).map(value => new KeyValuePair(value)))
            .set('solicitationIndicatorTypes', rawObject.get('solicitationIndicatorTypes', List()).map(value => new KeyValuePair(value)))
            .set('specialRequirementTypes', rawObject.get('specialRequirementTypes', List()).map(value => new KeyValuePair(value)))
            .set('salutationTypes', rawObject.get('salutationTypes', List()).map(value => new KeyValuePair(value)))
            .set('heightInFeetTypes', rawObject.get('heightInFeetTypes', List()).map(value => new KeyValuePair(value)))
            .set('heightInInchesTypes', rawObject.get('heightInInchesTypes', List()).map(value => new KeyValuePair(value)))
            .set('languages', rawObject.get('languages', List()).map(value => new LanguageType(value)))
            .set('contactMechanismTypes', rawObject.get('contactMechanismTypes', List()).map(value => new ContactMechanismType(value)))
            .set('serviceCoverageOrganizations', rawObject.get('serviceCoverageOrganizations', List()).map(value => new ServiceCoverageOrganizationType(value)))
            .set('modeOfTransportation', rawObject.get('modeOfTransportation', List()).map(value => new ServiceType(value)))
            .set('treatmentType', rawObject.get('treatmentType', List()).map(value => new ServiceType(value)))
            .set('queueTaskItemStatus', rawObject.get('queueTaskItemStatus', List()).map(value => new NameUuid(value)))
            .set('queueTaskItemStatusReason', rawObject.get('queueTaskItemStatusReason', List()).map(value => new NameUuid(value)))
            .set('queueTaskItemType', rawObject.get('queueTaskItemType', List()).map(value => new NameUuid(value)))
            .set('queueTaskStatus', rawObject.get('queueTaskStatus', List()).map(value => new NameUuid(value)))
            .set('queueTaskType', rawObject.get('queueTaskType', List()).map(value => new NameUuid(value)))
        ) as MetaDataTypesState;
    }

    return metaDataTypes;
}

/**
 * transform raw JSON from http API endpoint into
 * MetaDataTypesState Immutable Record type
 *
 * @param rawObject
 *
 * @returns {MetaDataTypesState}
 */
export function fromApi(rawObject : any) : MetaDataTypesState {
    let metaDataTypes : MetaDataTypesState = new MetaDataTypesState();

    if (isObject(rawObject) && !Array.isArray(rawObject)) {
        // convert POJO to Immutable
        rawObject = fromJS(rawObject);

        metaDataTypes = new MetaDataTypesState().withMutations(newMetaData => newMetaData
            .set('genderTypes', rawObject.get('genderTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('medicalConditionTypes', rawObject.get('medicalConditionTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('personConnectionTypes', rawObject.get('personConnectionTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('physicalCharacteristicTypes', rawObject.get('physicalCharacteristicTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('serviceCoveragePlanClassificationTypes', rawObject.get('serviceCoveragePlanClassificationTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('solicitationIndicatorTypes', rawObject.get('solicitationIndicatorTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('specialRequirementTypes', rawObject.get('specialRequirementTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('salutationTypes', rawObject.get('salutationTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('heightInFeetTypes', rawObject.get('heightInFeetTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('heightInInchesTypes', rawObject.get('heightInInchesTypes', List()).map(value => KeyValuePair.fromApi(value)))
            .set('languages', rawObject.get('languages', List()).map(value => LanguageType.fromApi(value)))
            .set('contactMechanismTypes', rawObject.get('contactMechanismTypes', List()).map(value => ContactMechanismType.fromApi(value)))
            .set('serviceCoverageOrganizations', rawObject.get('serviceCoverageOrganizations', List()).map(value => ServiceCoverageOrganizationType.fromApi(value)))
            .set('modeOfTransportation', rawObject.get('modeOfTransportation', List()).map(value => new ServiceType(value)))
            .set('treatmentType', rawObject.get('treatmentType', List()).map(value => new ServiceType(value)))
            .set('queueTaskItemStatus', rawObject.get('queueTaskItemStatus', List()).map(value => new NameUuid(value)))
            .set('queueTaskItemStatusReason', rawObject.get('queueTaskItemStatusReason', List()).map(value => new NameUuid(value)))
            .set('queueTaskItemType', rawObject.get('queueTaskItemType', List()).map(value => new NameUuid(value)))
            .set('queueTaskStatus', rawObject.get('queueTaskStatus', List()).map(value => new NameUuid(value)))
            .set('queueTaskType', rawObject.get('queueTaskType', List()).map(value => new NameUuid(value)))
        ) as MetaDataTypesState;

        // sort languages alphabetically
        metaDataTypes = metaDataTypes.set('languages', metaDataTypes.get('languages').sortBy(language => language.get('value'))) as MetaDataTypesState;
    }

    return metaDataTypes;
}
