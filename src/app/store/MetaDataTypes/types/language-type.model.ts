import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ILanguageType {
    id          : string;
    value       : string;
    isoCode     : string;
    family      : string;
}

export const LANGUAGE_TYPE = Record({
    id          : '',
    value       : '',
    isoCode     : '',
    family      : ''
});

const apiMap = Map({
    uuid    : 'id',
    name    : 'value',
    isoCode : 'isoCode',
    family  : 'family'
});

export class LanguageType extends LANGUAGE_TYPE {
    id          : string;
    value       : string;
    isoCode     : string;
    family      : string;

    constructor(values? : LanguageType | ILanguageType) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof LanguageType) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }

    static fromApi(rawObject : any) : LanguageType {
        const newObject : any = {};

        if (Map.isMap(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject.get(apiKey);
            });
        }
        else {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject[apiKey];
            });
        }

        return new LanguageType(newObject);
    }

    static toApi(rawObject : LanguageType) : any {
        const newObject : any = {};

        if (Map.isMap(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject.get(apiKey);
            });
        }
        else {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[apiKey] = rawObject[translatedKey];
            });
        }

        return Map(newObject);
    }
}
