import {LanguageType} from './language-type.model';
import {META_DATA_TYPES_MOCK} from '../../../shared/services/MetaDataTypes/meta-data-types.service.mocks';

describe('LanguageType Model', () => {
    // test definitions
    it('should return an Immutable Record type of LanguageType on init from POJO', () => {
        const languageType : LanguageType = new LanguageType(META_DATA_TYPES_MOCK.getCoreMetaDataTypes[0].languages[0]);

        expect(languageType instanceof LanguageType).toEqual(true);
        expect(languageType.get('id')).toBeDefined();
    });

    it('should return an Immutable Record type of LanguageType on init from Immutable', () => {
        const languageType : LanguageType = new LanguageType(new LanguageType());

        expect(languageType instanceof LanguageType).toEqual(true);
    });
});
