import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IServiceCoveragePlanClassification,
    ServiceCoveragePlanClassification
} from './service-coverage-plan-classification';

export interface IServiceCoveragePlan {
    id                                  : string;
    value                               : string;
    serviceCoveragePlanClassifications  : Array<IServiceCoveragePlanClassification>;
}

export const SERVICE_COVERAGE_PLAN = Record({
    id                                  : '',
    value                               : '',
    serviceCoveragePlanClassifications  : List<ServiceCoveragePlanClassification>()
});

const apiMap = Map({
    uuid                                : 'id',
    name                                : 'value',
    serviceCoveragePlanClassifications  : 'serviceCoveragePlanClassifications'
});

export class ServiceCoveragePlan extends SERVICE_COVERAGE_PLAN {
    id                                  : string;
    value                               : string;
    serviceCoveragePlanClassifications  : List<ServiceCoveragePlanClassification>;

    constructor(values? : ServiceCoveragePlan | IServiceCoveragePlan) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ServiceCoveragePlan) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoveragePlanClassifications
                if (convertedValues.getIn(['serviceCoveragePlanClassifications', 0, 'uuid'])) {
                    convertedValues = convertedValues.set('serviceCoveragePlanClassifications', List(convertedValues.get('serviceCoveragePlanClassifications', []).map(value => ServiceCoveragePlanClassification.fromApi(value))));
                }
                else {
                    convertedValues = convertedValues.set('serviceCoveragePlanClassifications', List(convertedValues.get('serviceCoveragePlanClassifications', []).map(value => new ServiceCoveragePlanClassification(value))));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }

    static fromApi(rawObject : any) : ServiceCoveragePlan {
        const newObject : any = {};

        if (Map.isMap(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject.get(apiKey);
            });
        }
        else {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject[apiKey];
            });
        }

        return new ServiceCoveragePlan(newObject);
    }
}
