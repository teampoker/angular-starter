import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {Observable} from 'rxjs/Observable';
import {List} from 'immutable';

import {IAppStore} from '../app-store';
import {KeyValuePair} from '../types/key-value-pair.model';
import {LanguageType} from './types/language-type.model';
import {ContactMechanismType} from './types/contact-mechanism-type.model';
import {ServiceCoverageOrganizationType} from './types/service-coverage-organization-type.model';
import {ServiceCategories} from './types/service-categories.model';
import {ServiceCoveragePlan} from './types/service-coverage-plan.model';
import {ServiceType} from './types/service-type.model';
import {NameUuid} from '../types/name-uuid.model';

@Injectable()

/**
 * implementation for MetaDataTypesSelectors: responsible for exposing custom state subscriptions to MetaDataTypesState
 */
export class MetaDataTypesSelectors {
    /**
     * MetaDataTypesSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * private method for parsing treatment types
     * @returns {List<ServiceType>}
     */
    private parseTreatmentTypes (types : List<ServiceType>) : List<ServiceType> {
        if (types && types.size) {
            return List([
                ...types.getIn([0, 'serviceCategories', 0, 'serviceOfferings']),
                ...types.getIn([0, 'serviceCategories', 1, 'serviceOfferings']),
                ...types.getIn([0, 'serviceCategories', 2, 'serviceOfferings']),
                ...types.getIn([0, 'serviceCategories', 3, 'serviceOfferings'])
            ]);
        }
        else {
            return List([]);
        }
    }

    /**
     * private method that returns specific approval or denial reasons given a task type
     * @returns {object}
     */
    private getReasonsForTaskType (taskItemTypeName : string, reasons : List<NameUuid>) : any {
        switch (taskItemTypeName) {
                case  'Prior Authorization Required from Plan':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Plan Approved' || reason.name === 'LogistiCare Error'),
                        notAuthorizedReasons :  reasons.filter(reason =>  reason.name === 'Not Authorized By Plan' || reason.name === 'Request could not be verified')
                    };
                case 'Treatment Type = Other':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Plan Approved' || reason.name === 'Facility Approved' || reason.name === 'Manager Approved' || reason.name === 'Beneficiary Eligible'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Plan' || reason.name === 'Not Authorized by Facility/Doctor' || reason.name === 'Not Authorized by Manager' || reason.name === 'Beneficiary Ineligible' || reason.name === 'Request could not be verified')
                    };
                case 'Trip Limit Met':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Plan Approved' || reason.name === 'Manager Approved'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Plan' || reason.name === 'Request could not be verified' || reason.name === 'Not Authorized by Manager')
                    };
                case 'Waive Advance Notice':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Plan Approved'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Plan' || reason.name === 'Advance Notice Not Met')
                    };
                case 'Need MNF - Treatment Type':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Form Received'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Form Not Received' || reason.name === 'Not Authorized by Facility/Doctor')
                    };
                case 'Need MNF - Mode of Transportation':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Form Received'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Form Not Received' || reason.name === 'Not Authorized by Facility/Doctor')
                    };
                case 'Mileage Limit Exceeded':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Plan Approved' || reason.name === 'Manager Approved'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Plan' || reason.name === 'Beneficiary Ineligible')
                    };
                case 'Facility - Out of Network':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Plan Approved' || reason.name === 'Facility Approved'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Plan' || reason.name === 'Not Authorized by Facility/Doctor')
                    };
                case 'From Nursing Facility': // Facility Requested Override?
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Plan Approved' || reason.name === 'Facility Approved'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Plan' || reason.name === 'Not Authorized by Facility/Doctor')
                    };
                case 'Call Facility to Verify':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Facility Approved'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Facility/Doctor' || reason.name  === 'Request could not be verified')
                    };
                case 'Mass Transit - <5 Business Days Notice':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Manager Approved'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Manager')
                    };
                case 'Review Plan of Care':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'POC Reviewed - Approved'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'POC Reviewed - Not Authorized' || reason.name === 'Beneficiary Ineligible')
                    };
                case 'Needs Plan of Care':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'POC Received'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'POC Not Received')
                    };
                case 'Exceeds Max Passengers':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Plan Approved'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Plan')
                    };
                case 'Treatment Type = not Covered':
                    return {
                        authorizedReasons : reasons.filter(reason => reason.name === 'Plan Approved' || reason.name === 'LogistiCare Error'),
                        notAuthorizedReasons :  reasons.filter(reason => reason.name === 'Not Authorized by Plan' || reason.name === 'Override Not Authorized' || reason.name === 'Beneficiary Ineligible' || reason.name === 'Needs 9-1-1/Emergency' || reason.name === 'Request could not be verified')
                    };
                default:
                    return '';

            }
    }

    /**
     * private method for parsing plans
     * @returns {List<ServiceCoveragePlan>}
     */
    private parsePlanTypes (types : List<ServiceCoverageOrganizationType>) : List<ServiceCoveragePlan> {
        const plans = [];

        types.valueSeq().forEach(v => {
            const plansArray =  v.get('serviceCoveragePlans');

            plans.push(plansArray);
        });

        return List<ServiceCoveragePlan>(plans);
    }

    /**
     * expose Observable to MetaDataTypesState.genderTypes
     * @returns {Observable<List<KeyValuePair>>}
     */
    genderTypes() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('genderTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.medicalConditionTypes
     * @returns {Observable<List<KeyValuePair>>}
     */
    medicalConditionTypes() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('medicalConditionTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.personConnectionTypes
     * @returns {Observable<List<KeyValuePair>>}
     */
    personConnectionTypes() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('personConnectionTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.physicalCharacteristics
     * @returns {Observable<List<KeyValuePair>>}
     */
    physicalCharacteristics() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('physicalCharacteristicTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.serviceCoveragePlanClassificationTypes
     * @returns {Observable<List<KeyValuePair>>}
     */
    serviceCoveragePlanClassificationTypes() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('serviceCoveragePlanClassificationTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.solicitationIndicatorTypes
     * @returns {Observable<List<KeyValuePair>>}
     */
    solicitationIndicatorTypes() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('solicitationIndicatorTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.specialRequirementTypes
     * @returns {Observable<List<KeyValuePair>>}
     */
    specialRequirementTypes() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('specialRequirementTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.salutationTypes
     * @returns {Observable<List<KeyValuePair>>}
     */
    salutations() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('salutationTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.heightInFeetTypes
     * @returns {Observable<List<KeyValuePair>>}
     */
    heightInFeetTypes() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('heightInFeetTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.heightInInchesTypes
     * @returns {Observable<List<KeyValuePair>>}
     */
    heightInInchesTypes() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.metaDataTypesState.get('heightInInchesTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.languages
     * @returns {Observable<List<LanguageType>>}
     */
    languages() : Observable<List<LanguageType>> {
        return this.store.select(state => state.metaDataTypesState.get('languages'));
    }

    /**
     * expose Observable to MetaDataTypesState.contactMechanismTypes
     * @returns {Observable<List<ContactMechanismType>>}
     */
    contactMechanismTypes() : Observable<List<ContactMechanismType>> {
        return this.store.select(state => state.metaDataTypesState.get('contactMechanismTypes'));
    }

    /**
     * expose Observable to MetaDataTypesState.contactMechanismTypes address types
     * @returns {Observable<List<KeyValuePair>>}
     */
    addressTypes() : Observable<List<KeyValuePair>> {
        let addressIndex : number;

        // determine index of address types
        this.store.select(state => state.metaDataTypesState.get('contactMechanismTypes')).subscribe(value => value.forEach((type, index) => {
            if (type.get('value') === 'Address') {
                addressIndex = index;
            }
        })).unsubscribe();

        return this.store.select(state => state.metaDataTypesState.getIn(['contactMechanismTypes', addressIndex, 'purposeTypes']));
    }

    /**
     * expose Observable to MetaDataTypesState.contactMechanismTypes email types
     * @returns {Observable<List<KeyValuePair>>}
     */
    emailTypes() : Observable<List<KeyValuePair>> {
        let emailIndex : number;

        // determine index of address types
        this.store.select(state => state.metaDataTypesState.get('contactMechanismTypes')).subscribe(value => value.forEach((type, index) => {
            if (type.get('value') === 'Electronic Address') {
                emailIndex = index;
            }
        })).unsubscribe();

        return this.store.select(state => state.metaDataTypesState.getIn(['contactMechanismTypes', emailIndex, 'purposeTypes']));
    }

    /**
     * expose Observable to MetaDataTypesState.contactMechanismTypes phone types
     * @returns {Observable<List<KeyValuePair>>}
     */
    phoneTypes() : Observable<List<KeyValuePair>> {
        let phoneIndex : number;

        // determine index of address types
        this.store.select(state => state.metaDataTypesState.get('contactMechanismTypes')).subscribe(value => value.forEach((type, index) => {
            if (type.get('value') === 'Telecommunications Number') {
                phoneIndex = index;
            }
        })).unsubscribe();

        return this.store.select(state => state.metaDataTypesState.getIn(['contactMechanismTypes', phoneIndex, 'purposeTypes']));
    }

    /**
     * expose Observable to MetaDataTypesState.serviceCoverageOrganizations
     * @returns {Observable<List<ServiceCoverageOrganizationType>>}
     */
    serviceCoverageOrganizations() : Observable<List<ServiceCoverageOrganizationType>> {
        return this.store.select(state => state.metaDataTypesState.get('serviceCoverageOrganizations'));
    }

    /**
     * expose Observable to MetaDataTypesState.serviceCoverageOrganizations
     * @returns {Observable<List<ServiceCoverageOrganizationType>>}
     */
    serviceCoveragePlans() : Observable<List<ServiceCoveragePlan>> {
        return this.store.select(state => this.parsePlanTypes(state.metaDataTypesState.get('serviceCoverageOrganizations')));
    }

    /**
     * expose Observable to MetaDataTypesState.serviceCategories
     * @returns {Observable<List<ServiceCategories>>}
     */
    serviceCategories() : Observable<List<ServiceCategories>> {
        return this.store.select(state => state.metaDataTypesState.get('serviceCategories'));
    }

    /**
     * expose Observable to MetaDataTypesState.modeOfTransportation
     * @returns {Observable<List<ServiceType>>}
     */
    modeOfTransportation() : Observable<List<ServiceType>> {
        return this.store.select(state => state.metaDataTypesState.get('modeOfTransportation'));
    }

    /**
     * expose Observable to MetaDataTypesState.treatmentType
     * @returns {Observable<List<ServiceType>>}
     */
    treatmentType() : Observable<List<ServiceType>> {
        return this.store.select(state => state.metaDataTypesState.get('treatmentType'));
    }

    /**
     * expose a parsed Observable to MetaDataTypesState.treatmentType
     * @returns {Observable<List<ServiceType>>}
     */
    treatmentTypeList() : Observable<List<ServiceType>> {
        return this.store.select(state => this.parseTreatmentTypes(state.metaDataTypesState.get('treatmentType')));
    }

    /**
     * expose Observable to MetaDataTypesState.queueTaskItemStatus
     * @returns {Observable<List<NameUuid>>}
     */
    queueTaskItemStatus() : Observable<List<NameUuid>> {
        return this.store.select(state => state.metaDataTypesState.get('queueTaskItemStatus'));
    }

    /**
     * expose Observable to MetaDataTypesState.queueTaskItemStatusReason
     * @returns {Observable<List<NameUuid>>}
     */
    queueTaskItemStatusReason() : Observable<List<NameUuid>> {
        return this.store.select(state => state.metaDataTypesState.get('queueTaskItemStatusReason'));
    }

    /**
     * expose Observable to MetaDataTypesState.queueTaskItemStatusReason
     * @returns {Observable<List<NameUuid>>}
     */
    reasonsByTaskItemType(taskItemTypeName : string) : Observable<any> {
        return this.store.select(state => this.getReasonsForTaskType(taskItemTypeName, state.metaDataTypesState.get('queueTaskItemStatusReason')));
    }

    /**
     * expose Observable to MetaDataTypesState.queueTaskItemType
     * @returns {Observable<List<NameUuid>>}
     */
    queueTaskItemType() : Observable<List<NameUuid>> {
        return this.store.select(state => state.metaDataTypesState.get('queueTaskItemType'));
    }

    /**
     * expose Observable to MetaDataTypesState.queueTaskStatus
     * @returns {Observable<List<NameUuid>>}
     */
    queueTaskStatus() : Observable<List<NameUuid>> {
        return this.store.select(state => state.metaDataTypesState.get('queueTaskStatus'));
    }

    /**
     * expose Observable to MetaDataTypesState.queueTaskType
     * @returns {Observable<List<NameUuid>>}
     */
    queueTaskType() : Observable<List<NameUuid>> {
        return this.store.select(state => state.metaDataTypesState.get('queueTaskType'));
    }

    /**
     * expose Observable to MetaDataTypesState.isLoaded
     * @returns {Observable<boolean>}
     */
    isLoaded() : Observable<boolean> {
        return Observable.create(observable => {
            this.serviceCoverageOrganizations().subscribe(organizations => {
                if (organizations.count() > 0) {
                    observable.next();
                }
            });
        });
    }

    /**
     * Returns a List of the modes of transportation
     * @returns {Observable<List<ServiceCategories>>}
     */
    getModesOfTransportation() : Observable<List<ServiceCategories>> {
        return this.store.select(state => state.metaDataTypesState
            .getIn(['modeOfTransportation', 0, 'serviceCategories'], List())
        );
    }

    /**
     * Returns a mode of transportation matching the uuid
     * @param {string} uuid
     * @returns {Observable<ServiceCategories>}
     */
    getModeOfTransportation(uuid : string) : Observable<ServiceCategories> {
        return this.getModesOfTransportation()
            .map(modes => modes.find(mode => mode.get('uuid') === uuid));
    }
}
