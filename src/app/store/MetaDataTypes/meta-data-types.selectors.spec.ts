import {async} from '@angular/core/testing';
import {Iterable} from 'immutable';

import {MetaDataTypesSelectors} from './meta-data-types.selectors';
import {MockRedux} from '../../../testing/ng2-redux-subs';
import {ROOT_REDUCER} from '../app-store';

describe('MetaData State Selectors', () => {
    let metaDataSelectors : MetaDataTypesSelectors,
        mockRedux         : MockRedux;

    // setup tasks to perform before each test
    beforeEach(() => {
        mockRedux = new MockRedux();

        mockRedux.configureStore(ROOT_REDUCER, {}, undefined, undefined);

        // Initialize mock NgRedux and create a new instance of the
        metaDataSelectors = new MetaDataTypesSelectors(mockRedux);
    });

    // test definitions
    it('genderTypes method should return MetaDataTypesState.genderTypes subscription', async(() => {
        // subscribe to Redux store updates
        metaDataSelectors.genderTypes().first().subscribe(val => {
            // check returned state for correct type
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('physicalCharacteristics method should return MetaDataTypesState.physicalCharacteristics subscription', async(() => {
        // subscribe to Redux store updates
        metaDataSelectors.physicalCharacteristics().first().subscribe(val => {
            // check returned state for correct type
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('personConnectionTypes method should return MetaDataTypesState.personConnectionTypes subscription', async(() => {
        // subscribe to Redux store updates
        metaDataSelectors.personConnectionTypes().first().subscribe(val => {
            // check returned state for correct type
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('specialRequirementTypes method should return MetaDataTypesState.specialRequirementTypes subscription', async(() => {
        // subscribe to Redux store updates
        metaDataSelectors.specialRequirementTypes().first().subscribe(val => {
            // check returned state for correct type
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('languages method should return MetaDataTypesState.languages subscription', async(() => {
        // subscribe to Redux store updates
        metaDataSelectors.languages().first().subscribe(val => {
            // check returned state for correct type
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('medicalConditionTypes method should return MetaDataTypesState.medicalConditionTypes subscription', async(() => {
        // subscribe to Redux store updates
        metaDataSelectors.medicalConditionTypes().first().subscribe(val => {
            // check returned state for correct type
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));
});
