import {INITIAL_META_DATA_TYPES_STATE} from './meta-data-types.initial-state';
import {MetaDataTypesActions} from './meta-data-types.actions';
import {IPayloadAction} from '../app-store';
import {MetaDataTypesState} from './types/meta-data-types-state.model';

export const META_DATA_TYPES_STATE_REDUCER = (state : MetaDataTypesState = INITIAL_META_DATA_TYPES_STATE, action : IPayloadAction) : MetaDataTypesState => {
    switch (action.type) {
        case MetaDataTypesActions.META_UPDATE_TYPES :
            // map metaDataTypes to state
            state = action.payload;

            break;
        default :
            return state;
    }

    return state;
};
