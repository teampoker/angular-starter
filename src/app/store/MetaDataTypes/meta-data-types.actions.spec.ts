import {async} from '@angular/core/testing';
import {Observable} from 'rxjs/Observable';
import {List} from 'immutable';

import {MetaDataTypesActions} from './meta-data-types.actions';
import {MockRedux} from '../../../testing/ng2-redux-subs';
import {MetaDataTypesService} from '../../shared/services/MetaDataTypes/meta-data-types.service';
import {NavActions} from '../Navigation/nav.actions';

// Mock the MetaDataTypesService with the methods we need to trigger
class MockMetaTypesService extends MetaDataTypesService {
    constructor() {
        super(undefined);
    }

    getMetaDataTypes() {
        return Observable.create(observer => {
            // update Redux store
            observer.next(List());
        });
    }
}

class MockNavActions extends NavActions {
    constructor() {
        super(undefined, undefined, undefined, undefined);
    }
}

describe('MetaDatatypes Action Creators', () => {
    let metaTypeActions     : MetaDataTypesActions,
        metaTypesService    : MockMetaTypesService,
        navActions          : MockNavActions,
        mockRedux           : MockRedux;

    // setup tasks to perform before each test
    beforeEach(() => {
        // Initialize mock NgRedux and create a new instance of the
        // ActionCreator Service to be tested.
        mockRedux           = new MockRedux();
        metaTypesService    = new MockMetaTypesService();
        navActions          = new MockNavActions();
        metaTypeActions     = new MetaDataTypesActions(
            mockRedux,
            metaTypesService,
            navActions
        );
    });

    // test definitions
    it('getMetaDataTypes method should dispatch META_UPDATE_TYPES action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : MetaDataTypesActions.META_UPDATE_TYPES,
            payload : List()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        metaTypeActions.getMetaDataTypes();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));
});
