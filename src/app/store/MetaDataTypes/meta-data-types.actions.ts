import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../app-store';
import {MetaDataTypesService} from '../../shared/services/MetaDataTypes/meta-data-types.service';
import {AlertItem, EnumAlertType} from '../Navigation/types/alert-item.model';
import {NavActions} from '../Navigation/nav.actions';
import {MetaDataTypesState} from './types/meta-data-types-state.model';

@Injectable()

/**
 * MetaDataTypes Action Creator Service
 */
export class MetaDataTypesActions {
    static META_UPDATE_TYPES : string = 'META_UPDATE_TYPES';

    /**
     * MetaDataTypesActions constructor
     * @param store
     * @param metaTypesService
     * @param navActions
     */
    constructor(
        private store               : NgRedux<IAppStore>,
        private metaTypesService    : MetaDataTypesService,
        private navActions          : NavActions
    ) {}

    /**
     * Sync Action that passes meta data types info to Redux store
     * @param metaTypes
     */
    private updateMetaDatatypes(metaTypes : MetaDataTypesState) {
        this.store.dispatch({
            type    : MetaDataTypesActions.META_UPDATE_TYPES,
            payload : metaTypes
        });
    }

    /**
     * Async Action that requests meta data types and updates Redux store with result
     */
    getMetaDataTypes() {
        this.metaTypesService.getMetaDataTypes().subscribe(response => {
            this.updateMetaDatatypes(response);
        }, error => {
            this.navActions.updateAlertMessageState(
                new AlertItem({
                    alertType   : EnumAlertType.ERROR,
                    message     : 'getMetaDataTypes Error: ' + JSON.stringify(error)
                })
            );
        });
    }
}
