import {META_DATA_TYPES_STATE_REDUCER} from './meta-data-types.reducer';
import {INITIAL_META_DATA_TYPES_STATE} from './meta-data-types.initial-state';
import {MetaDataTypesActions} from './meta-data-types.actions';
import {META_DATA_TYPES_MOCK} from '../../shared/services/MetaDataTypes/meta-data-types.service.mocks';
import {
    fromApi as metaDataTypesStateFromApi,
    MetaDataTypesState
} from './types/meta-data-types-state.model';

describe('MetaDataTypes State Reducer', () => {
    it('should init initial state', () => {
        expect(
            META_DATA_TYPES_STATE_REDUCER(undefined, {
                type    : undefined,
                payload : undefined
            })
        )
        .toEqual(INITIAL_META_DATA_TYPES_STATE);
    });

    it('should handle META_UPDATE_TYPES action', () => {
        const state = META_DATA_TYPES_STATE_REDUCER(undefined, {
            type    : MetaDataTypesActions.META_UPDATE_TYPES,
            payload : metaDataTypesStateFromApi(META_DATA_TYPES_MOCK.getCoreMetaDataTypes[0])
        });

        expect(state instanceof MetaDataTypesState).toEqual(true);
    });
});
