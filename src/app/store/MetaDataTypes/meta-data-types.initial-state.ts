import {MetaDataTypesState} from './types/meta-data-types-state.model';

export const INITIAL_META_DATA_TYPES_STATE = new MetaDataTypesState();
