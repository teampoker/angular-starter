
import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

export interface IQueueTaskItem {
    uuid                           : string;
    version                        : string;
    authorizationCode              : string;
    authorizedByName               : string;
    authorizedBy                   : string;
    verifiedWith                   : string;
    note                           : string;
    overrideReason                 : string;
    overrideReasonName             : string;
    phoneNumber                    : string;
    planFacilityName               : string;
    isApproved                     : boolean;
    queueTaskItemType              : INameUuid;
}

export const QUEUE_TASK_ITEM = Record({
    uuid                           : '',
    version                        : '',
    authorizationCode              : '',
    authorizedByName               : '',
    authorizedBy                   : '',
    verifiedWith                   : '',
    note                           : '',
    overrideReason                 : '',
    overrideReasonName             : '',
    phoneNumber                    : '',
    planFacilityName               : '',
    isApproved                     : false,
    queueTaskItemType              : new NameUuid()
});

export class QueueTaskItem extends QUEUE_TASK_ITEM {
    uuid                           : string;
    version                        : string;
    authorizationCode              : string;
    authorizedByName               : string;
    authorizedBy                   : string;
    verifiedWith                   : string;
    note                           : string;
    overrideReason                 : string;
    overrideReasonName             : string;
    phoneNumber                    : string;
    planFacilityName               : string;
    isApproved                     : boolean;
    queueTaskItemType              : NameUuid;

    constructor(values? : QueueTaskItem | IQueueTaskItem) {
        let convertedValues     : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof QueueTaskItem) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // queueTaskItemType
                convertedValues = convertedValues.set('queueTaskItemType', new NameUuid(convertedValues.get('queueTaskItemType')));

                // queueTaskItemContactMechanisms
                convertedValues = convertedValues.set('queueTaskItemContactMechanisms', List(convertedValues.get('queueTaskItemContactMechanisms', []).map(value => new NameUuid(value))));

            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * transform Immutable queueTaskItem model into
 * appropriate POJO for processing by API endpoints
 *
 * @param model Immutable model to transform
 *
 * @returns {any} POJO representation
 */
export function toApi(model : QueueTaskItem) : any {
    let newObject : any;

    // map immutable model to POJO
    newObject = {
        queueTaskItemUuid              : model.get('uuid', undefined),
        authorizationCode              : model.get('authorizationCode', undefined),
        note                           : model.get('note', undefined),
        overrideReason                 : model.get('overrideReason', undefined),
        overrideReasonName             : model.get('overrideReasonName', undefined),
        verifiedWith                   : model.get('verifiedWith', undefined),
        phoneNumber                    : model.get('phoneNumber', undefined),
        planFacilityName               : model.get('planFacilityName', undefined),
        isApproved                     : model.get('isApproved', undefined)
    };

    return newObject;
}
