import {
    fromJS,
    Map,
    Record
} from 'immutable';

export enum QueueTabType {
    UNASSIGNED,
    ASSIGNED_TO_ME,
    ASSIGNED_TO_OTHERS,
    COMPLETED
}

export enum CallType {
    CLAIM,
    UNASSIGN
}

export type SortColumn = 'appointmentOn' | 'beneficiaryName' | 'exceptionType' | 'planName' | 'referenceId' | 'state';

export type SortOrder = 'asc' | 'desc';

export interface IQueueQueryParams {
    sortColumn           : SortColumn;
    sortOrder            : SortOrder;
    page                 : string;
    pageSize             : string;
    search               : string;
}

export const QUEUE_QUERY_PARAMS = Record({
    sortColumn           : 'appointmentOn',
    sortOrder            : 'asc',
    page                 : '0',
    pageSize             : '25',
    search               : ''
});

export class QueueQueryParams extends QUEUE_QUERY_PARAMS {
    sortColumn           : SortColumn;
    sortOrder            : SortOrder;
    page                 : string;
    pageSize             : string;
    search               : string;

    constructor(values? : QueueQueryParams | IQueueQueryParams) {
        let convertedValues     : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof QueueQueryParams) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
