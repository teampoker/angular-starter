
import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

import {
    QueueTaskItem,
    IQueueTaskItem
} from './queue-task-item.model';

export interface IQueueTask {
    uuid                 : string;
    version              : string;
    reservationUuid      : string;
    assignedUser         : string;
    queueTaskType        : INameUuid;
    queueTaskStatus      : INameUuid;
    queueTaskItems       : Array<IQueueTaskItem>;
}

export const QUEUE_TASK = Record({
    uuid                 : '',
    version              : '',
    reservationUuid      : '',
    assignedUser         : '',
    queueTaskType        : new NameUuid(),
    queueTaskStatus      : new NameUuid(),
    queueTaskItems       : List<QueueTaskItem>()
});

export class QueueTask extends QUEUE_TASK {
    uuid                 : string;
    version              : string;
    reservationUuid      : string;
    assignedUser         : string;
    queueTaskType        : NameUuid;
    queueTaskStatus      : NameUuid;
    queueTaskItems       : List<QueueTaskItem>;

    constructor(values? : QueueTask | IQueueTask) {
        let convertedValues     : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof QueueTask) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // queueTaskType
                convertedValues = convertedValues.set('queueTaskType', new NameUuid(convertedValues.get('queueTaskType')));

                // queueTaskStatus
                convertedValues = convertedValues.set('queueTaskStatus', new NameUuid(convertedValues.get('queueTaskStatus')));

                // queueTaskItems
                convertedValues = convertedValues.set('queueTaskItems', List(convertedValues.get('queueTaskItems').map(value => new QueueTaskItem(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
