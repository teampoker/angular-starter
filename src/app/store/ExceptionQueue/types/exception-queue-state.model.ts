import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IException,
    Exception
} from './exception.model';

import {
    IQueueQueryParams,
    QueueQueryParams,
    QueueTabType
} from './queue-query-params.model';

import {
    ISearchResultsPaging,
    SearchResultsPaging
} from '../../Search/types/search-results-paging.model';
import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IExceptionQueueState {
    isFetching               : boolean;
    currentTab               : number;
    searchString             : string;
    startDate                : string;
    endDate                  : string;
    isSelectAll              : boolean;
    isReviewExceptionsActive : boolean;
    currentExceptionIndex    : number;
    params                   : IQueueQueryParams;
    pagingInfo               : ISearchResultsPaging;
    selectedExceptions       : Array<IException>;
    exceptionQueue           : Array<IException>;
    taskItemTypeFilter       : { [ type : string ] : boolean };
    planTypeFilter           : { [ type : string ] : boolean };
    prioritiesFilter         : { [ type : string ] : boolean };
    priorities               : Array<IKeyValuePair>;
}

export const EXCEPTION_QUEUE_STATE = Record({
    isFetching               : false,
    currentTab               : QueueTabType.ASSIGNED_TO_ME,
    searchString             : '',
    startDate                : '',
    endDate                  : '',
    isSelectAll              : false,
    isReviewExceptionsActive : false,
    currentExceptionIndex    : 0,
    params                   : new QueueQueryParams(),
    pagingInfo               : new SearchResultsPaging(),
    selectedExceptions       : List<Exception>(),
    exceptionQueue           : List<Exception>(),
    taskItemTypeFilter       : Map<string, boolean>(),
    planTypeFilter           : Map<string, boolean>(),
    prioritiesFilter         : Map<number, boolean>(),
    priorities               : List<KeyValuePair>([
        new KeyValuePair({ id : 1, value : 'Urgent' }),
        new KeyValuePair({ id : 2, value : 'High' }),
        new KeyValuePair({ id : 3, value : 'Low' })
    ])
});

export class ExceptionQueueState extends EXCEPTION_QUEUE_STATE {
    isFetching               : boolean;
    currentTab               : QueueTabType;
    searchString             : string;
    startDate                : string;
    endDate                  : string;
    isSelectAll              : boolean;
    isReviewExceptionsActive : boolean;
    currentExceptionIndex    : number;
    params                   : QueueQueryParams;
    pagingInfo               : SearchResultsPaging;
    selectedExceptions       : List<Exception>;
    exceptionQueue           : List<Exception>;
    taskItemTypeFilter       : Map<string, boolean>;
    planTypeFilter           : Map<string, boolean>;
    prioritiesFilter         : Map<number, boolean>;
    priorities               : List<KeyValuePair>;

    constructor(values? : ExceptionQueueState | IExceptionQueueState) {
        let convertedValues : Map<any, any>;
        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ExceptionQueueState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // params
                convertedValues = convertedValues.set('params', new QueueQueryParams(convertedValues.get('parms')));

                // pagingInfo
                convertedValues = convertedValues.set('pagingInfo', new SearchResultsPaging(convertedValues.get('pagingInfo')));

                // selectedExceptions
                convertedValues = convertedValues.set('selectedExceptions', List(convertedValues.get('selectedExceptions', []).map(value => new Exception(value))));

                // exceptionQueue
                convertedValues = convertedValues.set('exceptionQueue', List(convertedValues.get('exceptionQueue', []).map(value => new Exception(value))));

                // priorities
                convertedValues = convertedValues.set('priorities', convertedValues.get('priorities', List()).map(value => new KeyValuePair(value)));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
