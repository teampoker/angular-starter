import {
    fromJS,
    Map,
    Record,
    List
} from 'immutable';
import * as moment from 'moment';

import {
    IBeneficiary,
    Beneficiary
} from '../../Beneficiary/types/beneficiary.model';
import {
    IQueueTask,
    QueueTask
} from './queue-task.model';
import {
    IReservationState,
    ReservationState
} from '../../Reservation/types/reservation-state.model';
import {
    fromApi as beneficiaryPlanStateSummaryFromApi,
    IBeneficiaryPlansStateSummary,
    BeneficiaryPlansStateSummary
} from '../../Beneficiary/types/beneficiary-plans-state-summary.model';

import {
    IBeneficiaryInfo,
    BeneficiaryInfo
} from '../../Beneficiary/types/beneficiary-info';

import {
    BeneficiaryContactMechanismStateDetail,
    IBeneficiaryContactMechanismStateDetail
} from '../../Beneficiary/types/beneficiary-contact-mechanism-state-detail.model';

export interface IException {
    reservationUuid                        : string;
    referenceId                            : string;
    appointmentDate                        : string;
    state                                  : string;
    personUuid                             : string;
    priorityLevel                          : number;
    assignedUser                           : string;
    isSelected                             : boolean;
    beneficiary                            : IBeneficiary;
    reservation                            : IReservationState;
    queueTask                              : IQueueTask;
    plan                                   : IBeneficiaryPlansStateSummary;
    beneficiaryInfo                        : IBeneficiaryInfo;
    beneficiaryPlans                       : Array<IBeneficiaryPlansStateSummary>;
    beneficiaryContactMechanismStateDetail : IBeneficiaryContactMechanismStateDetail;

}

export const EXCEPTION = Record({
    reservationUuid                        : '',
    referenceId                            : '',
    appointmentDate                        : '',
    state                                  : '',
    personUuid                             : '',
    priorityLevel                          : 0,
    assignedUser                           : '',
    isSelected                             : false,
    beneficiary                            : new Beneficiary(),
    reservation                            : new ReservationState(),
    queueTask                              : new QueueTask(),
    beneficiaryPlans                       : List<BeneficiaryPlansStateSummary>(),
    plan                                   : new BeneficiaryPlansStateSummary(),
    beneficiaryInfo                        : new BeneficiaryInfo(),
    beneficiaryContactMechanismStateDetail : new BeneficiaryContactMechanismStateDetail()

});

export class Exception extends EXCEPTION {
    reservationUuid                        : string;
    referenceId                            : string;
    appointmentDate                        : string;
    state                                  : string;
    personUuid                             : string;
    priorityLevel                          : number;
    assignedUser                           : string;
    isSelected                             : boolean;
    beneficiary                            : Beneficiary;
    reservation                            : ReservationState;
    queueTask                              : QueueTask;
    beneficiaryPlans                       : List<BeneficiaryPlansStateSummary>;
    plan                                   : BeneficiaryPlansStateSummary;
    beneficiaryInfo                        : BeneficiaryInfo;
    beneficiaryContactMechanismStateDetail : BeneficiaryContactMechanismStateDetail;

    constructor(values? : Exception | IException) {
        let convertedValues     : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof Exception) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // person
                convertedValues = convertedValues.set('beneficiary', new Beneficiary(convertedValues.get('beneficiary')));

                // queueTask
                convertedValues = convertedValues.set('queueTask', new QueueTask(convertedValues.get('queueTask')));

                // reservation
                // convertedValues = convertedValues.set('reservation', new ReservationState(convertedValues.get('reservation')));

                // plan
                convertedValues = convertedValues.set('plan', new BeneficiaryPlansStateSummary(convertedValues.get('plan', [])));

                // beneficiaryInfo
                convertedValues = convertedValues.set('beneficiaryInfo', new BeneficiaryInfo(convertedValues.get('beneficiaryInfo', [])));

                // contact mechanism state detail
                convertedValues = convertedValues.set('beneficiaryContactMechanismStateDetail', new BeneficiaryContactMechanismStateDetail(convertedValues.get('beneficiaryContactMechanismStateDetail', [])));

                // beneficiaryPlans
                convertedValues = convertedValues.set('beneficiaryPlans', List(convertedValues.get('beneficiaryPlans', []).map(value => new BeneficiaryPlansStateSummary(value))));

            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * transform raw JSON into Exception Immutable Record type
 * @param rawObject
 * @returns {Exception}
 */
export function fromApi(rawObject : any) : Exception {
    return new Exception().withMutations(newException => newException
            .set('reservationUuid', rawObject.reservationUuid)
            .set('referenceId', rawObject.referenceId)
            .set('appointmentDate', moment(rawObject.appointmentDate).format('MM/DD/YYYY'))
            .set('state', rawObject.state)
            .set('personUuid', rawObject.personUuid)
            .set('priorityLevel', rawObject.priorityLevel)
            .set('assignedUser', rawObject.assignedUser)
            .set('beneficiary', new Beneficiary(rawObject.beneficiary))
            .set('reservation', rawObject.reservation)
            .set('queueTask', new QueueTask(rawObject.queueTask))
            .set('beneficiaryPlans', List(rawObject.beneficiary.serviceCoverages).map(plan => beneficiaryPlanStateSummaryFromApi(plan)))
            .set('plan', beneficiaryPlanStateSummaryFromApi(rawObject.plan))
            .set('beneficiaryInfo', new BeneficiaryInfo(rawObject.beneficiaryInfo))
            .set('beneficiaryContactMechanismStateDetail', new BeneficiaryContactMechanismStateDetail(rawObject.beneficiaryContactMechanismStateDetail))) as Exception;
}
