import {List, Map} from 'immutable';
import * as moment from 'moment';

import {INITIAL_EXCEPTION_QUEUE_STATE} from '../exception-queue.initial-state';
import {ExceptionQueueActions} from '../actions/exception-queue.actions';
import {IPayloadAction} from '../../app-store';
import {BeneficiaryInfo} from '../../Beneficiary/types/beneficiary-info';
import {BeneficiaryAddress} from '../../Beneficiary/types/beneficiary-address.model';
import {BeneficiaryEmail} from '../../Beneficiary/types/beneficiary-email.model';
import {BeneficiaryPhone} from '../../Beneficiary/types/beneficiary-phone.model';
import {BeneficiaryPhysicalCharacteristic} from '../../Beneficiary/types/beneficiary-physical-characteristic.model';
import {BeneficiaryContactMechanismStateDetail}  from '../../Beneficiary/types/beneficiary-contact-mechanism-state-detail.model';
import {BeneficiaryPlansStateSummary} from '../../Beneficiary/types/beneficiary-plans-state-summary.model';
import {BeneficiaryPersonServiceCoveragePlanClassification} from '../../Beneficiary/types/beneficiary-service-coverage-classification.model';
import {ExceptionQueueState} from '../types/exception-queue-state.model';

import {
    fromApi as exceptionFromApi,
    Exception
} from '../types/exception.model';
import {
    SearchResultsPaging,
    fromApi as searchResultsPagingFromApi
} from '../../Search/types/search-results-paging.model';

/**
 * Exception Queue State Reducer
 *
 * @param state
 * @param action
 * @returns {any}
 * @constructor
 */
export const EXCEPTION_QUEUE_STATE_REDUCER = (state : ExceptionQueueState = INITIAL_EXCEPTION_QUEUE_STATE, action : IPayloadAction): ExceptionQueueState => {
    switch (action.type) {
        case ExceptionQueueActions.EXCEPTION_QUEUE_SET_IS_FETCHING :
            state = state.set('isFetching', action.payload) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_EXCEPTION_QUEUE :
            // extract pagination configuration
            const pagingInfo = getPagingInfo(action.payload);

            // update list of exceptions and pagination configuration
            state = state.withMutations(record => {
                return record
                    .set('currentTab', state.get('currentTab', 1))
                    .set('searchString', state.get('searchString', ''))
                    .set('exceptionQueue', action.payload.results && action.payload.results.length ? List<Exception>(action.payload.results.map(value => exceptionFromApi(value))) : List<Exception>())
                    .set('pagingInfo', new SearchResultsPaging(updatePagination(pagingInfo)));
            }) as ExceptionQueueState;

            state = populateBeneficiaryViewModels(state) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_CURRENT_TAB:
            // set currentTab
            state = state.set('currentTab', action.payload) as ExceptionQueueState;

            state = state.withMutations(record => {
                return record
                    .set('currentTab', action.payload)
                    .set('selectedExceptions', List<string>());
            }) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_SEARCH_STRING:
            state = state.set('searchString', action.payload) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_SORT_PARAMS:

            // set sort params
            state = state.withMutations(record => {
                return record
                    .setIn(['params', 'sortColumn'], action.payload.sortKey)
                    .setIn(['params', 'sortOrder'], action.payload.direction);
            }) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PAGE_PARAM:
            // set page param
            if (action.payload.increment) {
                state = state.setIn(['params', 'page'], action.payload) as ExceptionQueueState;
            }
            else {
                const OFFSET = action.payload.value - 1 > 0 ? action.payload.value - 1 : 0;
                state = state.setIn(['params', 'page'], OFFSET) as ExceptionQueueState;
            }

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PAGE_SIZE:
            // set page size
            state = state.setIn(['params', 'pageSize'], action.payload) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_SELECT_ALL_EXCEPTIONS:
            // set isSelectAll flag
            state = state.set('isSelectAll', action.payload) as ExceptionQueueState;

            state = state.set('exceptionQueue', state.get('exceptionQueue').map(exception => exception.set('isSelected', action.payload))) as ExceptionQueueState;

            state = state.set('selectedExceptions', getAllExceptions(state.get('exceptionQueue'), action.payload)) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_SELECT_EXCEPTION : {
            const queueIndex = state.get('exceptionQueue').findKey(exception => exception.get('referenceId') === action.payload.get('referenceId'));
            const isSelected = !action.payload.get('isSelected');

            state = state.setIn(['exceptionQueue', queueIndex, 'isSelected'], isSelected) as ExceptionQueueState;

            if (isSelected) {
                state = state.set('selectedExceptions', state.get('selectedExceptions').push(action.payload.set('isSelected', true))) as ExceptionQueueState;
            }
            else {
                state = state.set(
                    'selectedExceptions',
                    state.get('selectedExceptions').filter(exception => exception.get('referenceId') !== action.payload.get('referenceId'))
                ) as ExceptionQueueState;
            }

            break;
        }
        case ExceptionQueueActions.EXCEPTION_QUEUE_EDIT_EXCEPTION : {
            // reset all exceptions
            state = state.set('exceptionQueue', state.get('exceptionQueue').map(exception => exception.set('isSelected', false))) as ExceptionQueueState;

            // find target exception and set to is selected
            const queueIndex = state.get('exceptionQueue').findKey(exception => exception.get('referenceId') === action.payload.get('referenceId'));
            state = state.setIn(['exceptionQueue', queueIndex, 'isSelected'], true) as ExceptionQueueState;

            // clear out selected exceptions
            state = state.set('selectedExceptions', List<Exception>()) as ExceptionQueueState;

            // add target exception to selected selectedExceptions
            state = state.set('selectedExceptions', state.get('selectedExceptions').push(action.payload.set('isSelected', true))) as ExceptionQueueState;

            break;
        }
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_START_DATE:
            // set dates
            if (action.payload === 'Invalid date') {
                state = state.set('startDate', '') as ExceptionQueueState;
            }
            else {
                state = state.set('startDate', action.payload) as ExceptionQueueState;
            }

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_END_DATE:
            // set dates
            if (action.payload === 'Invalid date') {
                state = state.set('endDate', '') as ExceptionQueueState;
            }
            else {
                state = state.set('endDate', action.payload) as ExceptionQueueState;
            }

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_IS_REVIEW_EXCEPTIONS_ACTIVE:
            // set is review exceptions active
            state = state.set('isReviewExceptionsActive', action.payload) as ExceptionQueueState;

            if (!action.payload) {
                state = state.set('currentExceptionIndex', 0) as ExceptionQueueState;
            }

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_CLEAR_DATE_RANGE :
            // clear date range values
            state = state.withMutations(record => record
                .set('startDate', '')
                .set('endDate', '')) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_CURRENT_EXCEPTION_INDEX :
            // set currentExceptionIndex
            // increment
            if (action.payload && state.get('currentExceptionIndex') + 1 <= state.get('selectedExceptions').size - 1) {
                state = state.set('currentExceptionIndex', state.get('currentExceptionIndex') + 1) as ExceptionQueueState;
            }
            // decrement
            else if (!action.payload && state.get('currentExceptionIndex') - 1 >= 0) {
                state = state.set('currentExceptionIndex', state.get('currentExceptionIndex') - 1) as ExceptionQueueState;
            }
            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_REVIEW_FORM_FIELDS:
            // set formfield
            state = state.setIn(['selectedExceptions', state.get('currentExceptionIndex'), 'queueTask', 'queueTaskItems', action.payload.primaryUpdateIndex, action.payload.fieldName], action.payload.fieldValue) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_TASK_ITEM_TYPE_FILTER:
            state = state.setIn(['taskItemTypeFilter', action.payload], !state.getIn(['taskItemTypeFilter', action.payload])) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PLAN_TYPE_FILTER:
            state = state.setIn(['planTypeFilter', action.payload], !state.getIn(['planTypeFilter', action.payload])) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_CLEAR_TASK_ITEM_TYPE_FILTER:
            state = state.set('taskItemTypeFilter', Map<string, boolean>()) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_CLEAR_PLAN_TYPE_FILTER:
            state = state.set('planTypeFilter', Map<string, boolean>()) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PRIORITIES_FILTER:
            state = state.setIn(
                ['prioritiesFilter', action.payload.get('id')],
                !state.getIn(['prioritiesFilter', action.payload.get('id')], false)
            ) as ExceptionQueueState;

            break;
        case ExceptionQueueActions.EXCEPTION_QUEUE_CLEAR_PRIORITIES_FILTER:
            state = state.set('prioritiesFilter', Map<number, boolean>()) as ExceptionQueueState;

            break;
        default:
            return state;
    }

    return state;
};

/**
 * returns all selected exceptions or a blank one depending on if selectAll is true or not
 *
 * @param exceptions : all exceptions in the exceptionQueue
 * @param selectAll : boolean value indicating if user selected all exceptions or not
 */
function getAllExceptions(exceptions : List<Exception>, selectAll : boolean) : any {
    if (selectAll) {
        return exceptions;
    }
    else {
        return List<Exception>();
    }
}

/**
 * returns paging info object parsing the response payload
 *
 * @param payload response payload of exception queue
 */
function getPagingInfo(payload : any) : any {
    return {
        page    : payload.page ? payload.page : 0,
        size    : payload.pageSize ? payload.pageSize : 25,
        total   : payload.totalResults ? payload.totalResults : 0
    };
}

/**
 * updates the current pagination info
 *
 * @param queuePaging search results pagination config
 */
function updatePagination(queuePaging : any) : SearchResultsPaging {
    // create immutable version of our new model data
    const paging : SearchResultsPaging = searchResultsPagingFromApi(queuePaging);

    return paging
        .set('numOfPages', paging.getTotalNumberOfPages())
        .set('displayedRecords', paging.getDisplayedRecords()) as SearchResultsPaging;
}

/**
 * copies raw beneficiary profile data from the API into a series of view models
 * that are more easily consumed by the UI templates
 *
 * @param state current ExceptionQueueState
 *
 * @returns {BeneficiaryState}
 */
function populateBeneficiaryViewModels(state : ExceptionQueueState) : ExceptionQueueState {
    if (state.get('exceptionQueue').size > 0) {
        const exceptions = state.get('exceptionQueue').map(exception => {
            const beneficiary = exception.get('beneficiary');

            let beneficiaryInfo                 : BeneficiaryInfo = new BeneficiaryInfo(),
                contactMechanismsStateDetail    : BeneficiaryContactMechanismStateDetail = new BeneficiaryContactMechanismStateDetail(),
                beneficiaryPlans                : List<BeneficiaryPlansStateSummary> = List<BeneficiaryPlansStateSummary>();

            // beneficiary info state
            beneficiaryInfo = beneficiaryInfo.withMutations(record => {
                let weight              : string = '0',
                    heightFeet          : string = '0',
                    heightInches        : string = '0',
                    beneficiaryWeight   : BeneficiaryPhysicalCharacteristic,
                    beneficiaryHeight   : BeneficiaryPhysicalCharacteristic;

                // pre-process physical characteristics
                if (beneficiary.get('physicalCharacteristics').size > 0) {
                    beneficiaryWeight = beneficiary.get('physicalCharacteristics').find(value => value.getIn(['type', 'value']) === 'Weight');
                    beneficiaryHeight = beneficiary.get('physicalCharacteristics').find(value => value.getIn(['type', 'value']) === 'Height');

                    beneficiaryWeight ? weight = beneficiaryWeight.get('value') : weight = '0';
                    beneficiaryHeight ? heightFeet = Math.floor(beneficiaryHeight.get('value') / 12).toString() : heightFeet = '0';
                    beneficiaryHeight ? heightInches = Math.floor(beneficiaryHeight.get('value') % 12).toString() : heightInches = '0';
                }

                // copy over the beneficiary demographic info
                return record
                    .set('birthDate', beneficiary.get('birthDate'))
                    .set('firstName', beneficiary.get('firstName'))
                    .set('lastName', beneficiary.get('lastName'))
                    .set('middleName', beneficiary.get('middleName'))
                    .set('nickname', beneficiary.get('nickname'))
                    .set('uuid', beneficiary.get('uuid'))
                    .set('personalTitle', beneficiary.get('personalTitle'))
                    .set('suffix', beneficiary.get('suffix'))
                    .set('deathDate', beneficiary.get('deathDate'))
                    .set('gender', beneficiary.getIn(['gender', 'value']))
                    .set('language', beneficiary.get('languages').size > 0 ? beneficiary.get('languages').find(value => value.get('ordinality') === 0).getIn(['language', 'value']) : '')
                    .set('weight', weight)
                    .set('heightFeet', heightFeet)
                    .set('heightInches', heightInches);
            }) as BeneficiaryInfo;

            // set isDeceased flag
            beneficiaryInfo.get('deathDate') && beneficiaryInfo.get('deathDate') !== '' ? beneficiaryInfo = beneficiaryInfo.set('isDeceased', true) as BeneficiaryInfo : beneficiaryInfo = beneficiaryInfo.set('isDeceased', false) as BeneficiaryInfo;

            // -------------------------------------------------------------------------------------------
            // service coverages state

            // check for null value first
            if (beneficiary.get('serviceCoverages') !== undefined) {
                if (beneficiary.get('serviceCoverages', List()).size > 0) {
                    beneficiaryPlans = beneficiary.get('serviceCoverages', List()).map(value => new BeneficiaryPlansStateSummary().withMutations(record => {
                        // search for a service coverage classification of type Id
                        const beneficiaryId : BeneficiaryPersonServiceCoveragePlanClassification = value.get('personServiceCoveragePlanClassifications', List())
                            .find(classification => classification.getIn(['serviceCoveragePlanClassification', 'serviceCoveragePlanClassificationType', 'name']) === 'Id');

                        const planStateRecord = record
                            .set('organizationUuid', value.getIn(['serviceCoveragePlan', 'serviceCoverageOrganization', 'uuid']))
                            .set('organization', value.getIn(['serviceCoveragePlan', 'serviceCoverageOrganization', 'name']))
                            .set('planDescription', value.getIn(['serviceCoveragePlan', 'name']))
                            .set('planUuid', value.getIn(['serviceCoveragePlan', 'uuid']))
                            .set('fromDate', value.get('fromDate') ? moment(value.get('fromDate')).format('MM/DD/YYYY') : '')
                            .set('thruDate', value.get('thruDate') ? moment(value.get('thruDate')).format('MM/DD/YYYY') : '')
                            .set('planIdLabel', beneficiaryId ? beneficiaryId.getIn(['serviceCoveragePlanClassification', 'name']) : '')
                            .set('planIdValue', beneficiaryId ? beneficiaryId.get('value') : 'N/A');

                        return planStateRecord;
                    }) as BeneficiaryPlansStateSummary);
                }
            }

            // -------------------------------------------------------------------------------------------
            // check for null value first
            if (beneficiary.get('contactMechanisms') !== undefined) {
                // any items to map?
                if (beneficiary.get('contactMechanisms').size > 0) {
                    const addressSize   : number = beneficiary.get('contactMechanisms').filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Address').size,
                          emailSize     : number = beneficiary.get('contactMechanisms').filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Electronic Address').size,
                          phoneSize     : number = beneficiary.get('contactMechanisms').filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Telecommunications Number').size;

                    contactMechanismsStateDetail = contactMechanismsStateDetail.withMutations(record => record
                        // addresses
                            .set('addresses', addressSize > 0 ? beneficiary.get('contactMechanisms').filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Address').map(value =>
                                new BeneficiaryAddress().withMutations(address => address
                                    .set('sequenceId', value.get('sequenceId'))
                                    .set('uuid', value.getIn(['contactMechanism', 'uuid']))
                                    .set('version', value.getIn(['contactMechanism', 'version']))
                                    .set('street', value.getIn(['contactMechanism', 'address1']))
                                    .set('apt', value.getIn(['contactMechanism', 'address2']))
                                    .set('city', value.getIn(['contactMechanism', 'city']))
                                    .set('state', value.getIn(['contactMechanism', 'state']))
                                    .set('postalCode', value.getIn(['contactMechanism', 'postalCode']))
                                    .set('geographicDetails', value.getIn(['contactMechanism', 'directions']))
                                    .set('addressNotifications', value.get('solicitationIndicatorType'))
                                    .set('addressType', value.get('purposeType'))
                                    .set('mechanismType', value.getIn(['contactMechanism', 'type']))) as BeneficiaryAddress) : record.get('addresses').push(new BeneficiaryAddress()))

                            // emails
                            .set('emails', emailSize > 0 ? beneficiary.get('contactMechanisms').filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Electronic Address').map(value =>
                                new BeneficiaryEmail().withMutations(email => email
                                    .set('sequenceId', value.get('sequenceId'))
                                    .set('uuid', value.getIn(['contactMechanism', 'uuid']))
                                    .set('version', value.getIn(['contactMechanism', 'version']))
                                    .set('email', value.getIn(['contactMechanism', 'electronicAddressString']))
                                    .set('emailType', value.get('purposeType'))
                                    .set('emailNotifications', value.get('solicitationIndicatorType'))
                                    .set('mechanismType', value.getIn(['contactMechanism', 'type']))) as BeneficiaryEmail) : record.get('emails').push(new BeneficiaryEmail()))

                            // phones
                            .set('phones', phoneSize > 0 ? beneficiary.get('contactMechanisms').filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Telecommunications Number').map(value =>
                                new BeneficiaryPhone().withMutations(phone => phone
                                    .set('sequenceId', value.get('sequenceId'))
                                    .set('uuid', value.getIn(['contactMechanism', 'uuid']))
                                    .set('version', value.getIn(['contactMechanism', 'version']))
                                    .set('phone', '(' + value.getIn(['contactMechanism', 'areaCode']) + ') ' + value.getIn(['contactMechanism', 'contactNumber']).substr(0, 3) + '-' + value.getIn(['contactMechanism', 'contactNumber']).substr(3, 7))
                                    .set('phoneType', value.get('purposeType'))
                                    .set('phoneNotifications', value.get('solicitationIndicatorType'))
                                    .set('mechanismType', value.getIn(['contactMechanism', 'type']))) as BeneficiaryPhone) : record.get('phones').push(new BeneficiaryPhone()))
                    ) as BeneficiaryContactMechanismStateDetail;
                }
                else {
                    contactMechanismsStateDetail = contactMechanismsStateDetail.withMutations(record => record
                    // addresses
                        .set('addresses', record.get('addresses').push(new BeneficiaryAddress()))

                        // emails
                        .set('emails', record.get('emails').push(new BeneficiaryEmail()))

                        // phones
                        .set('phones', record.get('phones').push(new BeneficiaryPhone()))) as BeneficiaryContactMechanismStateDetail;
                }
            }
            else {
                contactMechanismsStateDetail = contactMechanismsStateDetail.withMutations(record => record
                // addresses
                    .set('addresses', record.get('addresses').push(new BeneficiaryAddress()))

                    // emails
                    .set('emails', record.get('emails').push(new BeneficiaryEmail()))

                    // phones
                    .set('phones', record.get('phones').push(new BeneficiaryPhone()))) as BeneficiaryContactMechanismStateDetail;
            }

            return exception.withMutations(record => record
                .set('beneficiaryInfo', beneficiaryInfo)
                .set('beneficiaryContactMechanismStateDetail', contactMechanismsStateDetail)
                .set('beneficiaryPlans', beneficiaryPlans)
            );

        });
        return state.set('exceptionQueue', exceptions) as ExceptionQueueState;
    }
    else {
        return state;
    }
}
