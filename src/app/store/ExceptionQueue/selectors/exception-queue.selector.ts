import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NgRedux} from '@angular-redux/store';
import {List, Map} from 'immutable';
import * as moment from 'moment';

import {IAppStore} from '../../app-store';
import {
    QueueQueryParams,
    QueueTabType,
    SortColumn,
    SortOrder
} from '../types/queue-query-params.model';
import {Exception} from '../types/exception.model';
import {SearchResultsPaging} from '../../Search/types/search-results-paging.model';
import {KeyValuePair} from '../../types/key-value-pair.model';

@Injectable()

/**
 * implementation for ExceptionQueueStateSelectors: responsible for exposing custom state subscriptions to ExceptionQueueState
 */
export class ExceptionQueueStateSelectors {
    /**
     * ExceptionQueueStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    private exceptionTabFilters : { [key : number] : (exceptions : List<Exception>, username : string) => List<Exception> } = {
        [QueueTabType.ASSIGNED_TO_ME] : exceptionsAssignedToMeFilter,
        [QueueTabType.ASSIGNED_TO_OTHERS] : exceptionsAssignedToOthersFilter,
        [QueueTabType.COMPLETED] : completedExceptionsFilter,
        [QueueTabType.UNASSIGNED] : unassignedExceptionsFilter
    };

    private sortFunctions : any = {
        appointmentOn : {
            asc : sortByAppointmentOnAscending,
            desc : sortByAppointmentOnDescending
        },
        beneficiaryName : {
            asc : sortByBeneficiaryAscending,
            desc : sortByBeneficiaryDescending
        },
        exceptionType : {
            asc : sortByExceptionTypeAscending,
            desc : sortByExceptionTypeDescending
        },
        planName : {
            asc : sortByPlanNameAscending,
            desc : sortByPlanNameDescending
        },
        referenceId : {
            asc : sortByReferenceIdAscending,
            desc : sortByReferenceIdDescending
        },
        state : {
            asc : sortByStateAscending,
            desc : sortByStateDescending
        }
    };

    /**
     * private method for returning keys of filters that return true
     * @returns {string}
     */
    private getFilterString (filterObj : Map<string, boolean>) : any {
        return filterObj.filter((value, key, iter) => value).keySeq().toJS().toString();
    }

    getExceptionSortFunction(column : SortColumn, order : SortOrder) : any {
        let sortFunction;

        if (this.sortFunctions[column]) {
            sortFunction = this.sortFunctions[column][order];
        }

        return typeof sortFunction === 'function' ? sortFunction : sortByAppointmentOnAscending;
    }

    /**
     * expose Observable to ExceptionQueueState.exceptionQueue
     * @returns {Observable<List<Exception>>}
     */
    exceptionQueue() : Observable<List<Exception>> {
        return this.store.select(state => state.exceptionQueueState.get('exceptionQueue'));
    }

    /**
     * expose Observable to ExceptionQueueState.pagingInfo
     * @returns {Observable<SearchResultsPaging>}
     */
    pagingInfo() : Observable<SearchResultsPaging> {
        return this.store.select(state => state.exceptionQueueState.get('pagingInfo'));
    }

    /**
     * expose Observable to ExceptionQueueState.searchString
     * @returns {Observable<string>}
     */
    searchString() : Observable<string> {
        return this.store.select(state => state.exceptionQueueState.get('searchString'));
    }

    /**
     * expose Observable to ExceptionQueueState.currentTab
     * @returns {Observable<QueueTabType>}
     */
    currentTab() : Observable<QueueTabType> {
        return this.store.select(state => state.exceptionQueueState.get('currentTab'));
    }

    /**
     * expose Observable to ExceptionQueueState.params
     * @returns {Observable<QueueQueryParams>}
     */
    params() : Observable<QueueQueryParams> {
        return this.store.select(state => state.exceptionQueueState.get('params'));
    }

    /**
     * expose Observable to ExceptionQueueState.age
     * @returns {Observable<number>}
     */
    page() : Observable<number> {
        return this.store.select(state => state.exceptionQueueState.getIn(['params', 'page']));
    }

    /**
     * expose Observable to ExceptionQueueState.selectedExceptions
     * @returns {Observable<List<string>>}
     */
    selectedExceptions() : Observable<List<string>> {
        return this.store.select(state => state.exceptionQueueState.get('selectedExceptions'));
    }

    /**
     * expose Observable to ExceptionQueueState.startDate
     * @returns {Observable<string>}
     */
    startDate() : Observable<string> {
        return this.store.select(state => state.exceptionQueueState.get('startDate'));
    }

    /**
     * expose Observable to ExceptionQueueState.endDate
     * @returns {Observable<string>}
     */
     endDate() : Observable<string> {
        return this.store.select(state => state.exceptionQueueState.get('endDate'));
    }

    /**
     * expose Observable to ExceptionQueueState.isSelectAll
     * @returns {Observable<boolean>}
     */
    isSelectAll() : Observable<boolean> {
        return this.store.select(state => state.exceptionQueueState.get('isSelectAll'));
    }

    /**
     * expose Observable to ExceptionQueueState.reviewExceptions
     * @returns {Observable<boolean>}
     */
    isReviewExceptionsActive() : Observable<boolean> {
        return this.store.select(state => state.exceptionQueueState.get('isReviewExceptionsActive'));
    }

    /**
     * expose Observable to ExceptionQueueState.currentExceptionIndex
     * @returns {Observable<number>}
     */
    currentExceptionIndex() : Observable<number> {
        return this.store.select(state => state.exceptionQueueState.get('currentExceptionIndex'));
    }

    /**
     * expose Observable to ExceptionQueueState.taskItemTypeFilter
     * @returns {Observable<Map<string, boolean>>}
     */
    taskItemTypeFilter() : Observable<Map<string, boolean>> {
        return this.store.select(state => state.exceptionQueueState.get('taskItemTypeFilter'));
    }

    /**
     * expose Observable to ExceptionQueueState.planTypeFilter
     * @returns {Observable<Map<string, boolean>>}
     */
    planTypeFilter() : Observable<Map<string, boolean>> {
        return this.store.select(state => state.exceptionQueueState.get('planTypeFilter'));
    }

    /**
     * expose Observable to ExceptionQueueState.planTypeFilter
     * @returns {Observable<string>}
     */
    planTypeFilterString() : Observable<string> {
        return this.store.select(state => this.getFilterString(state.exceptionQueueState.get('planTypeFilter')));
    }

    /**
     * expose Observable to ExceptionQueueState.taskItemTypeFilter
     * @returns {Observable<string>}
     */
    taskItemTypeFilterString() : Observable<string> {
        return this.store.select(state => this.getFilterString(state.exceptionQueueState.get('taskItemTypeFilter')));
    }

    /**
     * expose Observable to ExceptionQueueState.isFetching
     * @returns {Observable<S>}
     */
    isFetching() : Observable<boolean> {
        return this.store.select(state => state.exceptionQueueState.get('isFetching'));
    }

    priorities() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.exceptionQueueState.get('priorities'));
    }

    prioritiesFilter() : Observable<Map<number, boolean>> {
        return this.store.select(state => state.exceptionQueueState.get('prioritiesFilter'));
    }

    getFilteredExceptions() : Observable<List<Exception>> {
        return this.store
            .select(state => state)
            .map(state => {
                const queueState = state.exceptionQueueState;
                let exceptions = queueState.get('exceptionQueue');
                const username = state.userState.getIn(['userInfo', 'name']);

                if (this.exceptionTabFilters[queueState.get('currentTab')]) {
                    exceptions = this.exceptionTabFilters[queueState.get('currentTab')](exceptions, username);
                }

                if (queueState.get('searchString', '') !== '') {
                    exceptions = searchExceptionsFilter(exceptions, queueState.get('searchString'));
                }

                if (queueState.get('startDate', '') !== '' && queueState.get('startDate') !== 'Invalid date') {
                    exceptions = filterByFromDate(exceptions, queueState.get('startDate'));
                }

                if (queueState.get('endDate', '') !== '' && queueState.get('endDate') !== 'Invalid date') {
                    exceptions = filterByThruDate(exceptions, queueState.get('endDate'));
                }

                if (queueState.get('planTypeFilter', Map()).filter(type => type).count() > 0) {
                    exceptions = filterByPlanType(exceptions, queueState.get('planTypeFilter'));
                }

                if (queueState.get('taskItemTypeFilter', Map()).filter(type => type).count() > 0) {
                    exceptions = filterByTaskItemType(exceptions, queueState.get('taskItemTypeFilter'));
                }

                if (queueState.get('prioritiesFilter', Map()).filter(priority => priority).count() > 0) {
                    exceptions = filterByPriority(exceptions, queueState.get('prioritiesFilter'));
                }

                return exceptions;
            });
    }
}

function completedExceptionsFilter(exceptions : List<Exception>) : List<Exception> {
    return exceptions
        .filter(exception => exception.getIn(['queueTask', 'queueTaskStatus', 'name']) !== 'Pending') as List<Exception>;
}

function exceptionsAssignedToMeFilter(exceptions : List<Exception>, username : string) : List<Exception> {
    exceptions = pendingExceptionsFilter(exceptions);

    return exceptions.filter(exception => exception.get('assignedUser', '') === username) as List<Exception>;
}

function exceptionsAssignedToOthersFilter(exceptions : List<Exception>, username : string) : List<Exception> {
    exceptions = pendingExceptionsFilter(exceptions);

    /* tslint:disable-next-line:ter-arrow-body-style */
    return exceptions.filter(exception => {
        // I'm not sure how this could come back undefined, but it can. Trust me.
        return exception.get('assignedUser', '') !== username && exception.get('assignedUser', '') !== '' && exception.get('assignedUser') !== undefined;
    }) as List<Exception>;
}

function filterByFromDate(exceptions : List<Exception>, date : string) : List<Exception> {
    return exceptions
        .filter(exception => moment.utc(exception.get('appointmentDate'), 'MM/DD/YYYY')
            .isSameOrAfter(moment.utc(date, 'MM/DD/YYYY'))) as List<Exception>;
}

function filterByPlanType(exceptions : List<Exception>, planTypes : Map<string, boolean>) : List<Exception> {
    return exceptions
        .filter(exception => planTypes.get(exception.getIn(['plan', 'planUuid'], false))) as List<Exception>;
}

function filterByPriority(exceptions : List<Exception>, priorities : Map<number, boolean>) : List<Exception> {
    return exceptions.filter(exception => priorities.get(
        parseInt(exception.get('priorityLevel', 0), 10),
        false
    )) as List<Exception>;
}

function filterByTaskItemType(exceptions : List<Exception>, planTypes : Map<string, boolean>) : List<Exception> {
    return exceptions
        .filter(exception => {
            let selected : boolean = false;

            exception
                .getIn(['queueTask', 'queueTaskItems'], List())
                .forEach(item => {
                    selected = planTypes.get(item.getIn(['queueTaskItemType', 'uuid'])) || selected;
                });

            return selected;
        }) as List<Exception>;
}

function filterByThruDate(exceptions : List<Exception>, date : string) : List<Exception> {
    return exceptions
        .filter(exception => moment.utc(exception.get('appointmentDate'), 'MM/DD/YYYY')
            .isSameOrBefore(moment.utc(date, 'MM/DD/YYYY'))) as List<Exception>;
}

function getExceptionTypes(exception : Exception) : string {
    return exception.getIn(['queueTask', 'queueTaskItems'])
        .map(item => item.getIn(['queueTaskItemType', 'name']))
        .sort()
        .join(', ');
}

function pendingExceptionsFilter(exceptions : List<Exception>) : List<Exception> {
    return exceptions
        .filter(exception => exception.getIn(['queueTask', 'queueTaskStatus', 'name']) === 'Pending') as List<Exception>;
}

function searchExceptionsFilter(exceptions : List<Exception>, text : string) : List<Exception> {
    text = text.toLowerCase();

    /* tslint:disable-next-line:ter-arrow-body-style */
    return exceptions.filter(exception => {
        return (exception.get('referenceId') || '').toLowerCase().indexOf(text) !== -1 ||
            (exception.getIn(['plan', 'planDescription']) || '').toLowerCase().indexOf(text) !== -1 ||
            (exception.getIn(['beneficiaryInfo', 'firstName']) || '').toLowerCase().indexOf(text) !== -1 ||
            (exception.getIn(['beneficiaryInfo', 'lastName']) || '').toLowerCase().indexOf(text) !== -1 ||
            (exception.get('state') || '').toLowerCase().indexOf(text) !== -1 ||
            (exception.get('appointmentDate') || '').toLowerCase().indexOf(text) !== -1 ||
            exception.getIn(['queueTask', 'queueTaskItems'])
                .map(item => item.getIn(['queueTaskItemType', 'name']))
                .filter(name => name.toLowerCase().indexOf(text) !== -1)
                .count() > 0;
    }) as List<Exception>;
}

/**
 * Sorts alphabetically ascending
 * @param {string} a
 * @param {string} b
 * @returns {number}
 */
function sortByAlphaAscending(a : string = '', b : string = '') : number {
    return a.localeCompare(b);
}

/**
 * Sorts alphabetically descending
 * @param {string} a
 * @param {string} b
 * @returns {number}
 */
function sortByAlphaDescending(a : string = '', b : string = '') : number {
    return b.localeCompare(a);
}

/**
 * Sorts by appointment date ascending
 * @param {Exception} a
 * @param {Exception} b
 * @returns {number}
 */
function sortByAppointmentOnAscending(a : Exception, b : Exception) : number {
    const aMoment = moment.utc(a.get('appointmentDate'), 'MM/DD/YYYY') as any;
    const bMoment = moment.utc(b.get('appointmentDate'), 'MM/DD/YYYY') as any;

    return aMoment - bMoment;
}

/**
 * Sorts by appointment date descending
 * @param {Exception} a
 * @param {Exception} b
 * @returns {number}
 */
function sortByAppointmentOnDescending(a : Exception, b : Exception) : number {
    const aMoment = moment.utc(a.get('appointmentDate'), 'MM/DD/YYYY') as any;
    const bMoment = moment.utc(b.get('appointmentDate'), 'MM/DD/YYYY') as any;

    return bMoment - aMoment;
}

function sortByBeneficiaryAscending(a : Exception, b : Exception) : number {
    return sortByAlphaAscending(
        `${a.getIn(['beneficiaryInfo', 'firstName'])} ${a.getIn(['beneficiaryInfo', 'lastName'])}`,
        `${b.getIn(['beneficiaryInfo', 'firstName'])} ${b.getIn(['beneficiaryInfo', 'lastName'])}`
    );
}

function sortByBeneficiaryDescending(a : Exception, b : Exception) : number {
    return sortByAlphaDescending(
        `${a.getIn(['beneficiaryInfo', 'firstName'])} ${a.getIn(['beneficiaryInfo', 'lastName'])}`,
        `${b.getIn(['beneficiaryInfo', 'firstName'])} ${b.getIn(['beneficiaryInfo', 'lastName'])}`
    );
}

function sortByExceptionTypeAscending(a : Exception, b : Exception) : number {
    return sortByAlphaAscending(getExceptionTypes(a), getExceptionTypes(b));
}

function sortByExceptionTypeDescending(a : Exception, b : Exception) : number {
    return sortByAlphaDescending(getExceptionTypes(a), getExceptionTypes(b));
}

function sortByPlanNameAscending(a : Exception, b : Exception) : number {
    return sortByAlphaAscending(a.getIn(['plan', 'planDescription']), b.getIn(['plan', 'planDescription']));
}

function sortByPlanNameDescending(a : Exception, b : Exception) : number {
    return sortByAlphaDescending(a.getIn(['plan', 'planDescription']), b.getIn(['plan', 'planDescription']));
}

function sortByReferenceIdAscending(a : Exception, b : Exception) : number {
    return sortByAlphaAscending(a.get('referenceId'), b.get('referenceId'));
}

function sortByReferenceIdDescending(a : Exception, b : Exception) : number {
    return sortByAlphaDescending(a.get('referenceId'), b.get('referenceId'));
}

function sortByStateAscending(a : Exception, b : Exception) : number {
    return sortByAlphaAscending(a.get('state'), b.get('state'));
}

function sortByStateDescending(a : Exception, b : Exception) : number {
    return sortByAlphaDescending(a.get('state'), b.get('state'));
}

function unassignedExceptionsFilter(exceptions : List<Exception>) : List<Exception> {
    exceptions = pendingExceptionsFilter(exceptions);

    /* tslint:disable-next-line:ter-arrow-body-style */
    return exceptions.filter(exception => {
        // I'm not sure how this could come back undefined, but it can. Trust me.
        return exception.get('assignedUser', '') === '' || exception.get('assignedUser') === undefined;
    }) as List<Exception>;
}
