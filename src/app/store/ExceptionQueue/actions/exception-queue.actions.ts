import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import * as moment from 'moment';

import {IAppStore} from '../../app-store';
import {NavActions} from '../../Navigation/nav.actions';
import {ExceptionQueueService} from '../../../areas/ExceptionQueue/services/ExceptionQueue/exception-queue.service';
import {ExceptionQueueState} from '../types/exception-queue-state.model';
import {
    QueueTabType,
    CallType,
    SortColumn,
    SortOrder
} from '../types/queue-query-params.model';
import {ExceptionQueueStateSelectors} from '../selectors/exception-queue.selector';
import {MetaDataTypesSelectors} from '../../MetaDataTypes/meta-data-types.selectors';
import {UserStateSelectors} from '../../User/user.selectors';
import {IBeneficiaryFieldUpdate} from '../../Beneficiary/types/beneficiary-field-update.model';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {Exception} from '../types/exception.model';

@Injectable()
/**
 * Implementation of ExceptionQueueActions: Redux Action Creator Service that exposes methods to mutate ExceptionQueue state
 */
export class ExceptionQueueActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static EXCEPTION_QUEUE_UPDATE_EXCEPTION_QUEUE             : string = 'EXCEPTION_QUEUE_UPDATE_EXCEPTION_QUEUE';
    static EXCEPTION_QUEUE_UPDATE_SEARCH_STRING               : string = 'EXCEPTION_QUEUE_UPDATE_SEARCH_STRING';
    static EXCEPTION_QUEUE_UPDATE_CURRENT_TAB                 : string = 'EXCEPTION_QUEUE_UPDATE_CURRENT_TAB';
    static EXCEPTION_QUEUE_UPDATE_PARAMS                      : string = 'EXCEPTION_QUEUE_UPDATE_PARAMS';
    static EXCEPTION_QUEUE_UPDATE_SORT_PARAMS                 : string = 'EXCEPTION_QUEUE_UPDATE_SORT_PARAMS';
    static EXCEPTION_QUEUE_UPDATE_PAGE_PARAM                  : string = 'EXCEPTION_QUEUE_UPDATE_PAGE_PARAM';
    static EXCEPTION_QUEUE_UPDATE_PAGE_SIZE                   : string = 'EXCEPTION_QUEUE_UPDATE_PAGE_SIZE';
    static EXCEPTION_QUEUE_UPDATE_SELECT_ALL_EXCEPTIONS       : string = 'EXCEPTION_QUEUE_UPDATE_SELECT_ALL_EXCEPTIONS';
    static EXCEPTION_QUEUE_UPDATE_SELECT_EXCEPTION            : string = 'EXCEPTION_QUEUE_UPDATE_SELECT_EXCEPTION';
    static EXCEPTION_QUEUE_UPDATE_START_DATE                  : string = 'EXCEPTION_QUEUE_UPDATE_START_DATE';
    static EXCEPTION_QUEUE_UPDATE_END_DATE                    : string = 'EXCEPTION_QUEUE_UPDATE_END_DATE';
    static EXCEPTION_QUEUE_CLEAR_DATE_RANGE                   : string = 'EXCEPTION_QUEUE_CLEAR_DATE_RANGE';
    static EXCEPTION_QUEUE_UPDATE_IS_REVIEW_EXCEPTIONS_ACTIVE : string = 'EXCEPTION_QUEUE_UPDATE_IS_REVIEW_EXCEPTIONS_ACTIVE';
    static EXCEPTION_QUEUE_UPDATE_IS_APPROVED                 : string = 'EXCEPTION_QUEUE_UPDATE_IS_APPROVED';
    static EXCEPTION_QUEUE_UPDATE_CURRENT_EXCEPTION_INDEX     : string = 'EXCEPTION_QUEUE_UPDATE_CURRENT_EXCEPTION_INDEX';
    static EXCEPTION_QUEUE_UPDATE_REVIEW_FORM_FIELDS          : string = 'EXCEPTION_QUEUE_UPDATE_REVIEW_FORM_FIELDS';
    static EXCEPTION_QUEUE_UPDATE_FILTER                      : string = 'EXCEPTION_QUEUE_UPDATE_FILTER';
    static EXCEPTION_QUEUE_UPDATE_TASK_ITEM_TYPE_FILTER       : string = 'EXCEPTION_QUEUE_UPDATE_TASK_ITEM_TYPE_FILTER';
    static EXCEPTION_QUEUE_UPDATE_PLAN_TYPE_FILTER            : string = 'EXCEPTION_QUEUE_UPDATE_PLAN_TYPE_FILTER';
    static readonly EXCEPTION_QUEUE_UPDATE_PRIORITIES_FILTER  : string = '[Exception Queue] Update Priorities Filter';
    static readonly EXCEPTION_QUEUE_CLEAR_PRIORITIES_FILTER   : string = '[Exception Queue] Clear Priorities Filter';
    static EXCEPTION_QUEUE_CLEAR_TASK_ITEM_TYPE_FILTER        : string = 'EXCEPTION_QUEUE_CLEAR_TASK_ITEM_TYPE_FILTER';
    static EXCEPTION_QUEUE_CLEAR_PLAN_TYPE_FILTER             : string = 'EXCEPTION_QUEUE_CLEAR_PLAN_TYPE_FILTER';
    static EXCEPTION_QUEUE_SET_IS_FETCHING                    : string = 'EXCEPTION_QUEUE_SET_IS_FETCHING';
    static EXCEPTION_QUEUE_EDIT_EXCEPTION                     : string = 'EXCEPTION_QUEUE_EDIT_EXCEPTION';

    /**
     * ExceptionQueueActions constructor
     * @param store
     * @param navActions
     * @param exceptionQueueService
     * @param exceptionQueueSelector
     * @param userSelector
     * @param metaDataTypeSelector
     */
    constructor(
        private store                   : NgRedux<IAppStore>,
        private navActions              : NavActions,
        private exceptionQueueService   : ExceptionQueueService,
        private exceptionQueueSelector  : ExceptionQueueStateSelectors,
        private userSelector            : UserStateSelectors,
        private metaDataTypeSelector    : MetaDataTypesSelectors
    ) {}

    /**
     * getTabFilter : returns filter string based on currentTab
     *
     * @param userName : user id of current user TODO : get this id once api is all set up
     * @param tab : number representing current tab
     */
    private getTabFilter (userName : string, tab : number) {
        switch (tab) {
            case 0: // unassigned tab
                return `unassigned:true&assignedUser:null`;
            case 1: // assigned to me
                return `assignedUser:${userName}`;
            case 2: // assigned to someone else
                return `notAssignedTo:${userName}`;
            case 3: // Completed
                // gets uuid for completed status from metaDataTypes and use that in the filter
                let completedStatus;
                this.metaDataTypeSelector.queueTaskStatus().subscribe(res => completedStatus = res.find(status => status.name === 'Completed')).unsubscribe();
                return `queueTaskStatusId:${ completedStatus ? completedStatus.get('uuid') : '22f698b1-f6cf-11e6-9155-125e1c46ef60'}`;
            default:
                return '';
        }
    }

    /**
     * requests the exception queue from the api
     */
    getExceptionQueue() {
        let isFetching : boolean = false;

        // query to see if exception queue GET is already active
        this.exceptionQueueSelector.isFetching().first().subscribe(value => isFetching = value);

        if (!isFetching) {
            // indicate loading
            this.updateIsFetching(true);

            let stateParams, stateSearch, stateCurrentTab, startDate, endDate, userName, tabFilter, taskItemTypeFilterString, planTypeFilterString;

            this.exceptionQueueSelector.params().subscribe(res => stateParams = res).unsubscribe();
            this.exceptionQueueSelector.currentTab().subscribe(res => stateCurrentTab = res).unsubscribe();
            this.exceptionQueueSelector.searchString().subscribe(res => stateSearch = res).unsubscribe();
            this.exceptionQueueSelector.startDate().subscribe(res => startDate = res).unsubscribe();
            this.exceptionQueueSelector.endDate().subscribe(res => endDate = res).unsubscribe();
            this.exceptionQueueSelector.taskItemTypeFilterString().subscribe(res => taskItemTypeFilterString = res).unsubscribe();
            this.exceptionQueueSelector.planTypeFilterString().subscribe(res => planTypeFilterString = res).unsubscribe();
            this.userSelector.userName.subscribe(res => userName = res).unsubscribe();

            // add fiter based on currentTab
            tabFilter = this.getTabFilter(userName, stateCurrentTab);
            stateParams = stateParams.set('search', `${tabFilter}`);

            // add date range filters
            if (startDate && endDate) {
                startDate = moment(startDate).format('YYYY-MM-DD');
                endDate = moment(endDate).format('YYYY-MM-DD');
                const dateRangeFilter = `|startDate:${startDate}|endDate:${endDate}`;
                stateParams = stateParams.set('search', `${stateParams.get('search')}${dateRangeFilter}`);
            }

            // add the beneficiary name search string if it exists
            if (stateSearch && stateSearch.length > 2) {
                const beneSearchFilter = `|beneficiaryName:${stateSearch}`;
                stateParams = stateParams.set('search', `${stateParams.get('search')}${beneSearchFilter}`);
            }

            // add taskItemTypeFilter to params
            if (taskItemTypeFilterString.length) {
                stateParams = stateParams.set('search', `${stateParams.get('search')}|queueTaskTypeUuid:${taskItemTypeFilterString}`);
            }
            // add planTypeFilter to params
            if (planTypeFilterString.length) {
                stateParams = stateParams.set('search', `${stateParams.get('search')}|serviceCoveragePlanUuid:${planTypeFilterString}`);
            }

            // clear select all selection
            this.updateSelectAllExceptions(false);

            this.exceptionQueueService.getExceptionQueue(stateParams).subscribe(response => {
                // hide loading
                this.updateIsFetching(false);

                // get exception queue data from api
                this.updateExceptionQueue(response);
            }, error => {
                // hide loading
                this.updateIsFetching(false);

                this.updateExceptionQueue(new ExceptionQueueState());
            });
        }
    }

    /**
     * request to assign or unassign exceptions
     */
    assignExceptions(type : CallType) {
        let selectedExceptions, targetUser;

        // get selectedExceptions and user id
        this.exceptionQueueSelector.selectedExceptions().first().subscribe(res => selectedExceptions = res);
        this.userSelector.userName.first().subscribe(res => targetUser = res);

        this.exceptionQueueService
            .assignExceptions(targetUser, selectedExceptions, type)
            .subscribe(() => this.getExceptionQueue(), error => console.error(error));
    }

    /**
     * seed the exception queue data in the store for later use
     *
     * @param exceptionQueueState is a list of exceptions returned from the api
     */
    updateExceptionQueue(exceptionQueueState : ExceptionQueueState) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_EXCEPTION_QUEUE,
            payload : exceptionQueueState
        });
    }

    /**
     * updateSearchString : send search string to store and filter exceptions based on the beneficiary name
     *
     * @param update : user entered beneficiary name string
     */
    updateSearchString(update : string) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_SEARCH_STRING,
            payload : update
        });
    }

    /**
     * updateCurrentTab : send currentTab to store and filter queues based on the new tab
     *
     * @param currentTab : user selected tab
     */
    updateCurrentTab(currentTab : QueueTabType) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_CURRENT_TAB,
            payload : currentTab
        });

        // refresh exception queue
        this.getExceptionQueue();
    }

    /**
     * updateParams : send params to store and filter queues based on the params
     *
     * @param updateParams : params for getExceptionQueue
     */
    updateParams(updateParams : string) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PARAMS,
            payload : updateParams
        });
    }

    /**
     * updateSortParams : send params to store and filter queues based on the params
     * @param sortKey
     * @param direction
     */
    updateSortParams(sortKey : SortColumn = 'appointmentOn', direction : SortOrder = 'asc') {
        const sortParams = {sortKey, direction};

        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_SORT_PARAMS,
            payload : sortParams
        });
    }

    /**
     * incrementPage : increments page and refresh results
     *
     */
    incrementPage() {
        let res;
        this.exceptionQueueSelector.page().subscribe(response => res = response).unsubscribe();

        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PAGE_PARAM,
            payload : {increment : true, value: res.toString()}
        });
    }

    /**
     * decrementPage : increments page and refresh results
     *
     */
    decrementPage() {
        let res;
        this.exceptionQueueSelector.page().subscribe(response => res = response).unsubscribe();

        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PAGE_PARAM,
            payload : {increment : false, value: res.toString()}
        });
    }

    /**
     * updatePageSize : send pageSize to store the size and update the results
     *
     * @param pageSize : page size
     */
    updatePageSize(pageSize : string) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PAGE_SIZE,
            payload : pageSize
        });
    }

    /**
     * dispatch action to update select all state of current list of exceptions
     * @param isAllSelected selected state of select all checkbox
     */
    updateSelectAllExceptions(isAllSelected : boolean) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_SELECT_ALL_EXCEPTIONS,
            payload : isAllSelected
        });
    }

    /**
     * dispatch action to update select state of individual exception
     * @param {Exception} exception exception in exceptionQueue
     */
    updateSelectException(exception : Exception) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_SELECT_EXCEPTION,
            payload : exception
        });
    }

    /**
     * dispatch action to update select state of individual exception when user clicks on pencil edit button
     * @param {Exception} exception exception in exceptionQueue
     */
    editException(exception : Exception) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_EDIT_EXCEPTION,
            payload : exception
        });

        this.updateIsReviewExceptionsActive(true);
    }

    /**
     * update startDate value on ExceptionQueueState
     *
     * @param startDate : date string
     */
    updateStartDate(startDate : string) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_START_DATE,
            payload : startDate
        });
    }

    /**
     * update endDate value on ExceptionQueueState
     *
     * @param endDate : date string
     */
    updateEndDate(endDate : string) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_END_DATE,
            payload : endDate
        });
    }

    /**
     * dispatch action to clear selected date range values
     */
    clearDateRangeFilter() {
        this.store.dispatch({ type : ExceptionQueueActions.EXCEPTION_QUEUE_CLEAR_DATE_RANGE });
    }

    /**
     * updateIsReviewExceptionsActive : update isReviewExceptionsActive boolean
     *
     * @param review : boolean
     */
    updateIsReviewExceptionsActive(review : boolean) {
        this.navActions.toggleIsPoppedNavState(review);

        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_IS_REVIEW_EXCEPTIONS_ACTIVE,
            payload : review
        });
    }

    /**
     * updateCurrentExceptionIndex : update index that points to the exception being reviewed in the exception queue
     *
     */
    updateCurrentExceptionIndex(increment : boolean) {
        let index;

        this.exceptionQueueSelector.currentExceptionIndex().subscribe(res => index = res).unsubscribe();

        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_CURRENT_EXCEPTION_INDEX,
            payload : increment
        });
    }

    /**
     * updateReviewFormFields : update exception task item field value
     *
     * @param fieldUpdate : IBeneficiaryFieldUpdate
     */
    updateReviewFormFields(fieldUpdate : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_REVIEW_FORM_FIELDS,
            payload : fieldUpdate
        });
    }

    /**
     * submitReviewForm : submit review form for a specific reservation
     *
     */
    submitReviewForm() {
        let index, targetException;
        this.exceptionQueueSelector.currentExceptionIndex().subscribe(res => index = res).unsubscribe();
        this.exceptionQueueSelector.selectedExceptions().subscribe(res => targetException = res).unsubscribe();

        this.exceptionQueueService.submitExceptionReview(targetException.getIn([index, 'queueTask', 'queueTaskItems'])).subscribe(response => {
            // get exception queue data from api
            this.updateExceptionQueue(response);
        }, error => {
            this.updateExceptionQueue(new ExceptionQueueState());
        });
    }

    /**
     * updateTaskItemTypeFilter : adds or removes task item type from the filter
     * @param {string} taskTypeUuid
     */
    updateTaskItemTypeFilter (taskTypeUuid : string) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_TASK_ITEM_TYPE_FILTER,
            payload : taskTypeUuid
        });
    }

    /**
     * clearTaskItemTypeFilter : clears task item type filter
     */
    clearTaskItemTypeFilter () {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_CLEAR_TASK_ITEM_TYPE_FILTER
        });
    }

    /**
     * clearPlanTypeFilter : clears task item type filter
     */
    clearPlanTypeFilter () {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_CLEAR_PLAN_TYPE_FILTER
        });
    }

    /**
     * updatePlanTypeFilter : adds or removes task item type from the filter
     * @param {string} planTypeUuid
     */
    updatePlanTypeFilter(planTypeUuid : string) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PLAN_TYPE_FILTER,
            payload : planTypeUuid
        });
    }

    /**
     * dispatch action to toggle state of isFetching flag
     * @param fetch
     */
    updateIsFetching(fetch : boolean) {
        this.store.dispatch({
            type    : ExceptionQueueActions.EXCEPTION_QUEUE_SET_IS_FETCHING,
            payload : fetch
        });
    }

    updatePrioritiesFilter(priority : KeyValuePair) {
        this.store.dispatch({
            type : ExceptionQueueActions.EXCEPTION_QUEUE_UPDATE_PRIORITIES_FILTER,
            payload : priority
        });
    }

    clearPrioritiesFilter() {
        this.store.dispatch({
            type : ExceptionQueueActions.EXCEPTION_QUEUE_CLEAR_PRIORITIES_FILTER
        });
    }
}
