import {ExceptionQueueState} from './types/exception-queue-state.model';

export const INITIAL_EXCEPTION_QUEUE_STATE = new ExceptionQueueState();
