import {
    fromJS,
    Map,
    Record
} from 'immutable';
import * as moment from 'moment';

import {ISpecialRequirement} from './special-requirement.interface';
import {NameUuid} from './name-uuid.model';

export interface ITransportationItemSpecialRequirement extends ISpecialRequirement {
    comment? : string;
    transportationReservationItemUuid : string;
}

export const TransportationItemSpecialRequirementRecord = Record({
    comment : null,
    createdBy : '',
    createdOn : '',
    sequenceId : -1,
    specialRequirementType : new NameUuid(),
    specialRequirementTypeUuid : '',
    transportationReservationItemUuid : -1,
    updatedBy : '',
    updatedOn : '',
    version : 0
});

export class TransportationItemSpecialRequirement extends TransportationItemSpecialRequirementRecord {
    comment? : string;
    createdBy : string;
    createdOn : string;
    sequenceId : number;
    specialRequirementType : NameUuid;
    specialRequirementTypeUuid : string;
    transportationReservationItemUuid : string;
    updatedBy : string;
    updatedOn : string;
    version : number;

    constructor(values? : TransportationItemSpecialRequirement | ITransportationItemSpecialRequirement) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof TransportationItemSpecialRequirement) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                convertedValues = convertedValues.set(
                    'specialRequirementType',
                    new NameUuid(convertedValues.get('specialRequirementType'))
                );
            }
        }

        super(convertedValues);
    }
}

export function fromApi(requirement : ITransportationItemSpecialRequirement) : TransportationItemSpecialRequirement {
    return new TransportationItemSpecialRequirement(requirement)
        .withMutations(model => model
            .set('createdOn', moment.utc(requirement.createdOn, 'YYYY-MM-DDTHH:mm:ssZ[UTC]').toISOString())
            .set('updatedOn', moment.utc(requirement.updatedOn, 'YYYY-MM-DDTHH:mm:ssZ[UTC]').toISOString())
        ) as TransportationItemSpecialRequirement;
}

export function toApi(requirement : TransportationItemSpecialRequirement) : ITransportationItemSpecialRequirement {
    const apiModel = requirement.toJS();

    apiModel.createdOn = moment.utc(requirement.get('createdOn')).format('YYYY-MM-DDTHH:mm:ssZ[UTC]');
    apiModel.updatedOn = moment.utc(requirement.get('updatedOn')).format('YYYY-MM-DDTHH:mm:ssZ[UTC]');

    return apiModel;
}
