import {
    fromJS,
    Map,
    Record
} from 'immutable';
import {isObject} from 'rxjs/util/isObject';

export interface IKeyValuePair {
    id    : number | string;
    value : string;
}

const KEY_VALUE_PAIR = Record({
    id    : '',
    value : 'SELECT'
});

const apiMap = Map({
    uuid  : 'id',
    name  : 'value'
});

export class KeyValuePair extends KEY_VALUE_PAIR {
    id    : number | string;
    value : string;

    constructor(values? : KeyValuePair | IKeyValuePair) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof KeyValuePair) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }

    static fromApi(rawObject : any) : KeyValuePair {
        const newObject : any = {};

        if (Map.isMap(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject.get(apiKey);
            });
        }
        else if (isObject(rawObject) && !Array.isArray(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject[apiKey];
            });
        }

        return new KeyValuePair(newObject);
    }

    static toApi(model : KeyValuePair) : any {
        const newObject : any = {};

        if (model !== undefined) {
            if (Map.isMap(model)) {
                apiMap.forEach((translatedKey, apiKey) => {
                    newObject[translatedKey] = model.get(apiKey);
                });
            }
            else {
                apiMap.forEach((translatedKey, apiKey) => {
                    newObject[apiKey] = model[translatedKey];
                });
            }

            return newObject;
        }
        else {
            return model;
        }
    }
}

export function fromStorage(pojo : IKeyValuePair) : KeyValuePair {
    return new KeyValuePair(pojo);
}
