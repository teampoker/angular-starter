import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';
import * as moment from 'moment';

import {
    fromApi as requirementFromApi,
    toApi as requirementToApi,
    IPassengerSpecialRequirement,
    PassengerSpecialRequirement
} from './passenger-special-requirement.model';
import {
    fromApi as personFromApi,
    toApi as personToApi,
    IPerson,
    Person
} from './person.model';

export interface ITransportationItemPassenger {
    createdOn : string;
    createdBy : string;
    passengerSpecialRequirements : List<IPassengerSpecialRequirement>;
    person : IPerson;
    personUuid : string;
    sequenceId : number;
    transportationReservationItemUuid : string;
    updatedBy : string;
    updatedOn : string;
    version : number;
}

export const TransportationItemPassengerRecord = Record({
    createdOn : '',
    createdBy : '',
    passengerSpecialRequirements : List<PassengerSpecialRequirement>(),
    person : new Person(),
    personUuid : '',
    sequenceId : -1,
    transportationReservationItemUuid : '',
    updatedBy : '',
    updatedOn : '',
    version : 0
});

export class TransportationItemPassenger extends TransportationItemPassengerRecord {
    createdOn : string;
    createdBy : string;
    passengerSpecialRequirements : List<PassengerSpecialRequirement>;
    person : Person;
    personUuid : string;
    sequenceId : number;
    transportationReservationItemUuid : string;
    updatedBy : string;
    updatedOn : string;
    version : number;

    constructor(values? : TransportationItemPassenger | ITransportationItemPassenger) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof TransportationItemPassenger) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                convertedValues = convertedValues.set(
                    'person',
                    new Person(convertedValues.get('person'))
                );

                convertedValues = convertedValues.set(
                    'passengerSpecialRequirements',
                    convertedValues
                        .get('passengerSpecialRequirements', List())
                        .map(requirement => new PassengerSpecialRequirement(requirement))
                );
            }
        }

        super(convertedValues);
    }
}

export function fromApi(passenger : ITransportationItemPassenger) : TransportationItemPassenger {
    let passengerModel = new TransportationItemPassenger(passenger);

    if (Array.isArray(passenger.passengerSpecialRequirements)) {
        passengerModel = passengerModel.set(
            'passengerSpecialRequirements',
            passenger.passengerSpecialRequirements.map(requirementFromApi)
        ) as TransportationItemPassenger;
    }

    return passengerModel.withMutations(model => model
        .set('person', personFromApi(passenger.person))
        .set('createdOn', moment.utc(passenger.createdOn, 'YYYY-MM-DDTHH:mm:ssZ[UTC]').toISOString())
        .set('updatedOn', moment.utc(passenger.updatedOn, 'YYYY-MM-DDTHH:mm:ssZ[UTC]').toISOString())
    ) as TransportationItemPassenger;
}

export function toApi(passenger : TransportationItemPassenger) : ITransportationItemPassenger {
    const apiModel = passenger.toJS();

    apiModel.createdOn = moment.utc(passenger.get('createdOn')).format('YYYY-MM-DDTHH:mm:ssZ[UTC]');
    apiModel.updatedOn = moment.utc(passenger.get('updatedOn')).format('YYYY-MM-DDTHH:mm:ssZ[UTC]');

    apiModel.passengerSpecialRequirements = passenger.get('passengerSpecialRequirements', List()).map(requirementToApi).toJS();
    apiModel.person = personToApi(passenger.get('person'));

    return apiModel;
}
