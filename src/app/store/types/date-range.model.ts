import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IDateRange {
    fromDate?  : string;
    thruDate?  : string;
}

const DATE_RANGE = Record({
    fromDate  : '',
    thruDate  : ''
});

export class DateRange extends DATE_RANGE {
    fromDate  : string;
    thruDate  : string;

    constructor(values? : DateRange | IDateRange) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof DateRange) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }
}

export function fromStorage(pojo : IDateRange) : DateRange {
    return new DateRange(pojo);
}
