import {Record} from 'immutable';

// http://googlemaps.github.io/google-maps-services-java/v0.1.3/javadoc/com/google/maps/model/TravelMode.html
export type TravelMode = 'BICYCLING' | 'DRIVING' | 'TRANSIT' | 'UNKOWN' | 'WALKING';

export interface IMassTransitRouteStep {
    distance : number;
    duration : number;
    instructions : string;
    travelMode : TravelMode;
}

export interface IMassTransitRouteStepApi {
    distanceForStep : number;
    durationForStep : number;
    instructionsForStep : string;
    travelModeForStep : TravelMode;
}

export const MassTransitRouteStepRecord = Record({
    distance : 0,
    duration : 0,
    instructions : '',
    travelMode : undefined
});

export class MassTransitRouteStep extends MassTransitRouteStepRecord {
    distance : number;
    duration : number;
    instructions : string;
    travelMode : TravelMode;
}

/**
 * Parses a mass transit step API response into a MassTransitRouteStep model
 *
 * @param {IMassTransitRouteStepApi} pojo
 *
 * @returns {MassTransitRouteStep}
 */
export function fromApi(pojo : IMassTransitRouteStepApi) : MassTransitRouteStep {
    return new MassTransitRouteStep({
        distance : pojo.distanceForStep,
        duration : pojo.durationForStep,
        instructions : pojo.instructionsForStep,
        travelMode : pojo.travelModeForStep
    });
}

/**
 * transform IMassTransitRouteStep model into a MassTransitRouteStep Record
 *
 * @param {IMassTransitRouteStep} pojo
 *
 * @returns {MassTransitRouteStep}
 */
export function fromStorage(pojo : IMassTransitRouteStep) : MassTransitRouteStep {
    return new MassTransitRouteStep(pojo);
}
