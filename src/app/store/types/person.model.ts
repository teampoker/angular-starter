import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';
import {isObject} from 'rxjs/util/isObject';

import {
    INameUuid,
    NameUuid
} from './name-uuid.model';

export interface IPerson {
    connections : Array<INameUuid>;
    firstName : string;
    gender : INameUuid;
    lastName : string;
    middleName : string;
    nickname : string;
    personalTitle : string;
    sourceUuid : string;
    suffix : string;
    targetUuid : string;
    uuid : string;
}

export const PersonRecord = Record({
    connections : List<NameUuid>(),
    firstName : '',
    gender : new NameUuid(),
    lastName : '',
    middleName : '',
    nickname : '',
    personalTitle : '',
    sourceUuid : '',
    suffix : '',
    targetUuid : '',
    uuid : ''
});

export class Person extends PersonRecord {
    connections : List<NameUuid>;
    firstName : string;
    gender : NameUuid;
    lastName : string;
    middleName : string;
    nickname : string;
    personalTitle : string;
    sourceUuid : string;
    suffix : string;
    targetUuid : string;
    uuid : string;

    constructor(values? : Person | IPerson) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof Person) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                convertedValues = convertedValues.set(
                    'connections',
                    convertedValues.get('connections', List()).map(connection => new NameUuid(connection))
                );
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

export function fromApi(person : any) : Person {
    const personState : any = {};

    if (isObject(person) && !Array.isArray(person)) {
        personState.firstName = person.firstName;
        personState.gender = new NameUuid(person.gender);
        personState.lastName = person.lastName;
        personState.middleName = person.middleName;
        personState.nickname = person.nickname;
        personState.personalTitle = person.personalTitle;
        personState.sourceUuid = person.sourceUuid;
        personState.suffix = person.suffix;
        personState.targetUuid = person.targetUuid;
        personState.uuid = person.uuid;

        if (Array.isArray(person.connections)) {
            personState.connections = List(person.connections.map(connection => new NameUuid(connection)));
        }
    }

    return new Person(personState);
}

export function toApi(person : Person) : any {
    return person.toJS();
}
