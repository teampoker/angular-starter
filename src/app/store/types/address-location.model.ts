import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    fromStorage as latLongFromStorage,
    ILatLong,
    LatLong
} from './latitude-longitude.model';
import {
    parsePhoneNumberAreaCode,
    parsePhoneNumberContactNumber
} from '../utils/parse-phone-numbers';

export interface IAddressLocation {
    isFormValid      : boolean;
    uuid?            : string;
    additionalInfo   : string;
    address1         : string;
    address2         : string;
    city             : string;
    latLong          : ILatLong;
    name             : string;
    phoneNumber      : string;
    state            : string;
    zipCode          : string;
    mileageDistance? : number;
    durationMinutes? : number;
}

export const ADDRESS_LOCATION = Record({
    isFormValid      : false,
    uuid             : null,
    additionalInfo   : '',
    address1         : '',
    address2         : '',
    city             : '',
    latLong          : new LatLong(),
    name             : '',
    phoneNumber      : '',
    state            : '',
    zipCode          : '',
    mileageDistance  : null,
    durationMinutes  : null
});

export class AddressLocation extends ADDRESS_LOCATION {
    isFormValid      : boolean;
    uuid?            : string;
    additionalInfo   : string;
    address1         : string;
    address2         : string;
    city             : string;
    latLong          : LatLong;
    name             : string;
    phoneNumber      : string;
    state            : string;
    zipCode          : string;
    mileageDistance? : number;
    durationMinutes? : number;

    constructor(values? : AddressLocation | IAddressLocation) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof AddressLocation) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // latLong
                convertedValues = convertedValues.set('latLong', new LatLong(convertedValues.get('latLong')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * Parses a leg API response into a Address Location model
 *
 * @param {any} pojo
 *
 * @returns {AddressLocation}
 */
export function fromApi(pojo : any) : AddressLocation {
    const address = pojo.address ? pojo.address : {};
    let phone = '';

    if (pojo.telecommunicationsNumber) {
        phone = pojo.telecommunicationsNumber.areaCode + '-' + pojo.telecommunicationsNumber.contactNumber;
    }

    return new AddressLocation({
        uuid : pojo.uuid,
        name : pojo.name,
        additionalInfo : pojo.directions,
        phoneNumber : phone,
        address1 : address.address1,
        address2 : address.address2,
        latLong : {
            lat : address.latitude,
            lng : address.longitude
        },
        city : address.city,
        state : address.state,
        zipCode : address.postalCode,
        mileageDistance : pojo.distance,
        durationMinutes : pojo.duration
    } as any);
}

export function fromStorage(pojo : IAddressLocation) : AddressLocation {
    let state = new AddressLocation(pojo);

    state = state.set('latLong', latLongFromStorage(pojo.latLong)) as AddressLocation;

    return state;
}

/**
 * transform Immutable Address Location model into
 * appropriate POJO for processing by API endpoints
 *
 * @param {AddressLocation} location
 *
 * @returns {any} POJO representation
 */
export function toApi(location : AddressLocation) : any {
    return {
        name : location.get('name'),
        uuid : location.get('uuid'),
        directions : location.get('additionalInfo'),
        address : {
            address1 : location.get('address1'),
            address2 : location.get('address2'),
            city : location.get('city'),
            postalCode : location.get('zipCode'),
            state : location.get('state'),
            latitude : location.getIn(['latLong', 'lat']),
            longitude : location.getIn(['latLong', 'lng'])
        },
        telecommunicationsNumber : {
            areaCode : parsePhoneNumberAreaCode(location.get('phoneNumber')),
            contactNumber : parsePhoneNumberContactNumber(location.get('phoneNumber'))
        }
    };
}
