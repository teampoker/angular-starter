import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IDateTime {
    date : string;
    time : string;
}

const DATE_TIME = Record({
    date : '',
    time : ''
});

export class DateTime extends DATE_TIME {
    date : string;
    time : string;

    constructor(values? : DateTime | IDateTime) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof DateTime) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }
}

export function fromStorage(pojo : IDateTime) : DateTime {
    return new DateTime(pojo);
}
