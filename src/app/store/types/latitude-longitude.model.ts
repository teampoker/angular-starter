import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ILatLong {
    lat      : string;
    lng      : string;
}

const LAT_LONG = Record({
    lat     : '',
    lng     : ''
});

export class LatLong extends LAT_LONG {
    lat      : string;
    lng      : string;

    constructor(values? : LatLong | ILatLong) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof LatLong) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }
}

export function fromStorage(pojo : ILatLong) : LatLong {
    return new LatLong(pojo);
}
