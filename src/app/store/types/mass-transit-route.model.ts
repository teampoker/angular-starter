import {
    List,
    Record
} from 'immutable';

import {
    fromApi as stepFromApi,
    fromStorage as stepFromStorage,
    IMassTransitRouteStep,
    IMassTransitRouteStepApi,
    MassTransitRouteStep
} from './mass-transit-route-step.model';

export interface IMassTransitRoute {
    arrivalTime : string;
    departureTime : string;
    distance : number;
    duration : number;
    steps : Array<IMassTransitRouteStep>;
}

export interface IMassTransitRouteApi {
    arrivalTimeForRoute : string;
    departureTimeForRoute : string;
    distanceForRoute : number;
    durationForRoute : number;
    massTransitRouteSteps : Array<IMassTransitRouteStepApi>;
    status : 'OK' | 'ZERO_RESULTS';
}

export const MassTransitRouteRecord = Record({
    arrivalTime : '',
    departureTime : '',
    distance : 0,
    duration : 0,
    steps : List<MassTransitRouteStep>()
});

export class MassTransitRoute extends MassTransitRouteRecord {
    arrivalTime : string;
    departureTime : string;
    distance : number;
    duration : number;
    steps : List<MassTransitRouteStep>;
}

/**
 * Parses a mass transit API response into a MassTransitRoute model
 *
 * @param {IMassTransitRouteApi} pojo
 *
 * @returns {MassTransitRoute}
 */
export function fromApi(pojo : IMassTransitRouteApi) : MassTransitRoute | undefined {
    if (pojo.status === 'OK') {
        return new MassTransitRoute({
            arrivalTime: pojo.arrivalTimeForRoute,
            departureTime: pojo.departureTimeForRoute,
            distance: pojo.distanceForRoute,
            duration: pojo.durationForRoute,
            steps: Array.isArray(pojo.massTransitRouteSteps) ? List(pojo.massTransitRouteSteps.map(stepFromApi)) : List()
        });
    }
    else {
        return undefined;
    }
}

/**
 * transform IMassTransitRoute model into a MassTransitRoute Record
 *
 * @param {IMassTransitRoute} pojo
 *
 * @returns {MassTransitRoute}
 */
export function fromStorage(pojo : IMassTransitRoute) : MassTransitRoute {
    return new MassTransitRoute({
        arrivalTime : pojo.arrivalTime,
        departureTime : pojo.departureTime,
        distance : pojo.distance,
        duration : pojo.duration,
        steps : List(pojo.steps.map(stepFromStorage))
    });
}
