import {DateRange} from './date-range.model';

describe('DateRange Model', () => {
    // test definitions
    it('should return an Immutable Record type of DateRange on init from POJO', () => {
        const dateRange : DateRange = new DateRange();

        expect(dateRange instanceof DateRange).toEqual(true);
    });

    it('should return an Immutable Record type of DateRange on init from Immutable', () => {
        const dateRange : DateRange = new DateRange(new DateRange());

        expect(dateRange instanceof DateRange).toEqual(true);
    });
});
