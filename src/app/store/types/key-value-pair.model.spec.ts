import {KeyValuePair} from './key-value-pair.model';

describe('KeyValuePair Model', () => {
    // test definitions
    it('should return an Immutable Record type of KeyValuePair on init from POJO', () => {
        const keyValuePair : KeyValuePair = new KeyValuePair();

        expect(keyValuePair instanceof KeyValuePair).toEqual(true);
    });

    it('should return an Immutable Record type of KeyValuePair on init from Immutable', () => {
        const keyValuePair : KeyValuePair = new KeyValuePair(new KeyValuePair());

        expect(keyValuePair instanceof KeyValuePair).toEqual(true);
    });

    it('should return an Immutable Record type of KeyValuePair with static fromApi method', () => {
        const keyValuePair : KeyValuePair = KeyValuePair.fromApi({
            uuid    : '0',
            name    : 'value'
        });

        expect(keyValuePair instanceof KeyValuePair).toEqual(true);
    });
});
