import {
    fromJS,
    Map,
    Record
} from 'immutable';
import * as moment from 'moment';

import {NameUuid} from './name-uuid.model';
import {ISpecialRequirement} from './special-requirement.interface';

export interface IPassengerSpecialRequirement extends ISpecialRequirement {
    transportationItemPassengerSequenceId : number;
}

export const PassengerSpecialRequirementRecord = Record({
    createdBy : '',
    createdOn : '',
    sequenceId : -1,
    specialRequirementType : new NameUuid(),
    specialRequirementTypeUuid : '',
    transportationItemPassengerSequenceId : -1,
    updatedBy : '',
    updatedOn : '',
    version : 0
});

export class PassengerSpecialRequirement extends PassengerSpecialRequirementRecord {
    createdBy : string;
    createdOn : string;
    sequenceId : number;
    specialRequirementType : NameUuid;
    specialRequirementTypeUuid : string;
    transportationItemPassengerSequenceId : number;
    updatedBy : string;
    updatedOn : string;
    version : number;

    constructor(values? : PassengerSpecialRequirement | IPassengerSpecialRequirement) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof PassengerSpecialRequirement) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                convertedValues = convertedValues.set(
                    'specialRequirementType',
                    new NameUuid(convertedValues.get('specialRequirementType'))
                );
            }
        }

        super(convertedValues);
    }
}

export function fromApi(requirement : IPassengerSpecialRequirement) : PassengerSpecialRequirement {
    return new PassengerSpecialRequirement(requirement)
        .withMutations(model => model
            .set('createdOn', moment.utc(requirement.createdOn, 'YYYY-MM-DDTHH:mm:ssZ[UTC]').toISOString())
            .set('updatedOn', moment.utc(requirement.updatedOn, 'YYYY-MM-DDTHH:mm:ssZ[UTC]').toISOString())
        ) as PassengerSpecialRequirement;
}

export function toApi(requirement : PassengerSpecialRequirement) : IPassengerSpecialRequirement {
    const apiModel = requirement.toJS();

    apiModel.createdOn = moment.utc(requirement.get('createdOn')).format('YYYY-MM-DDTHH:mm:ssZ[UTC]');
    apiModel.updatedOn = moment.utc(requirement.get('updatedOn')).format('YYYY-MM-DDTHH:mm:ssZ[UTC]');

    return apiModel;
}
