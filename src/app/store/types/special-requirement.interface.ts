import {INameUuid} from './name-uuid.model';

export interface ISpecialRequirement {
    createdBy : string;
    createdOn : string;
    sequenceId : number;
    specialRequirementType : INameUuid;
    specialRequirementTypeUuid : string;
    updatedBy : string;
    updatedOn : string;
    version : number;
}
