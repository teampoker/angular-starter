import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface INameUuid {
    name : string;
    uuid : string;
}

const NAME_UUID = Record({
    name : '',
    uuid : ''
});

export class NameUuid extends NAME_UUID {
    name : string;
    uuid : string;

    constructor(values? : NameUuid | INameUuid) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof NameUuid) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }

    /**
     * transform Immutable NameUuid model into
     * appropriate POJO for processing by API endpoints
     *
     * @param model Immutable model to transform
     *
     * @returns {any} POJO representation
     */
    static toApi(model : NameUuid) : any {
        let newObject : any;

        // map immutable model to POJO
        newObject = {
            uuid : model.get('uuid', undefined),
            name : model.get('name', undefined)
        };

        return newObject;
    }
}

/**
 * transform INameUuid model into a NameUuid Record
 *
 * @param {INameUuid} pojo
 *
 * @returns {NameUuid}
 */
export function fromStorage(pojo : INameUuid) : NameUuid {
    return new NameUuid(pojo);
}

/**
 * transform Immutable NameUuid model into
 * appropriate POJO for processing by API endpoints
 *
 * @param {NameUuid} nameUuid Immutable model to transform
 * @param {boolean} [uuidOnly=false] If the "name" property should be omitted
 *
 * @returns {INameUuid | { uuid : string }} POJO representation
 */
export function toApi(nameUuid : NameUuid, uuidOnly : boolean = false) : INameUuid | { uuid : string } {
    return uuidOnly ? { uuid : nameUuid.get('uuid') } : nameUuid.toJS();
}
