import {AppState} from './app-state.model';

/**
 * Defines the default structure of the AppState object.
 * @type {AppState}
 */
export const INITIAL_APP_STATE = new AppState();
