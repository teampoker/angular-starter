import {IPayloadAction} from '../app-store';
import {AppState} from './app-state.model';
import {INITIAL_APP_STATE} from './app-state.initial';
import {AppStateActions} from './app-state.actions';

/**
 * APP USER STORE
 *
 * @param state
 * @param action
 * @returns {any}
 * @constructor
 */
export const APP_STATE_REDUCER = (state : AppState = INITIAL_APP_STATE, action : IPayloadAction | any) : AppState => {
    switch (action.type) {
        case AppStateActions.APP_BUSY :
            state = state.set('isBusy', true) as AppState;
            break;
        case AppStateActions.APP_IDLE :
            state = state.set('isBusy', false) as AppState;
            break;
        default :
            return state;
    }

    return state;
};
