import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../app-store';

@Injectable()
export class AppStateActions {
    static APP_BUSY : string = 'APPLICATION_BUSY';
    static APP_IDLE : string = 'APPLICATION_IDLE';

    /**
     *
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {
    }

    /**
     * The app is tied up doing stuff.  Important stuff. Yuge things...things you wouldn't believe.
     */
    busy() {
        this.store.dispatch({
            type : AppStateActions.APP_BUSY
        });
    }

    /**
     * App is free to process more stuff.
     */
    idle() {
        this.store.dispatch({
            type : AppStateActions.APP_IDLE
        });
    }
}
