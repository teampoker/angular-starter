import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IAppState {
    isBusy : boolean;
}

export const APP_STATE = Record({
    isBusy : false
});

/**
 * type definition for Redux Store app state
 */
export class AppState extends APP_STATE implements IAppState {
    isBusy : boolean;

    constructor(values? : AppState | IAppState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof AppState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
