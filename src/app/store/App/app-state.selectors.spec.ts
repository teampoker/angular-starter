import {
    TestBed,
    inject
} from '@angular/core/testing';
import {NgReduxModule} from '@angular-redux/store';

import {AppStateSelectors} from './app-state.selectors';

describe('Selector: AppState', () => {
    let testSelector : AppStateSelectors;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports   : [
                NgReduxModule
            ],
            providers : [
                AppStateSelectors
            ]
        });
    });

    beforeEach(
        inject(
            [AppStateSelectors],
            selector => {
                testSelector = selector;
            }
        )
    );

    it('should return busy', () => {
        testSelector.isBusy.subscribe(value => {
            expect(typeof value === 'boolean').toEqual(true);
        });
    });
});
