import {Injectable} from '@angular/core';
import {select} from '@angular-redux/store';
import {Observable} from 'rxjs';

@Injectable()
/**
 * implementation for AppStateSelectors: responsible for exposing custom state subscriptions to AppState
 */
export class AppStateSelectors {
    /**
     * AppStateSelectors constructor
     *
     */
    constructor() {
    }

    /**
     * Flag that indicates if the app is busy doing something.
     * @returns {Observable<boolean>} true, if the app is unavailable for new requests. False, if idle.
     */
    @select(state => state.appState.isBusy)
    isBusy : Observable<boolean>;
}
