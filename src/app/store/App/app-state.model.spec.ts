import {AppState} from './app-state.model';

describe('Model: AppState', () => {

    it('should have a default constructor', () => {
        expect(new AppState()).toBeDefined();
    });

    it('should construct an immutable instance from JSON', () => {
        const expectedObject : any = {
            isBusy : true
        };
        expect(new AppState(expectedObject))
            .toEqual(jasmine.objectContaining(expectedObject));
    });

    it('should construct an immutable instance from JSON', () => {
        const expectedObject : any = {
            isBusy : true
        };
        const existing : AppState  = new AppState(expectedObject);

        expect(new AppState(existing))
            .toEqual(jasmine.objectContaining(expectedObject));
    });
});
