import {APP_STATE_REDUCER} from './app-state.reducer';
import {INITIAL_APP_STATE} from './app-state.initial';
import {AppState} from './app-state.model';
import {AppStateActions} from './app-state.actions';

describe('Reducer: AppState', () => {
    it('should return the initial state', () => {
        expect(
            APP_STATE_REDUCER(undefined, {})
        )
            .toEqual(INITIAL_APP_STATE);
    });

    it('should indicate app is busy when APP_BUSY', () => {
        expect(
            APP_STATE_REDUCER(new AppState(), {type : AppStateActions.APP_BUSY})
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        isBusy : true
                    }
                )
            );
    });

    it('should indicate app is idle when APP_IDLE', () => {
        expect(
            APP_STATE_REDUCER(new AppState(), {type : AppStateActions.APP_IDLE})
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        isBusy : false
                    }
                )
            );
    });

});
