import {
    TestBed,
    inject
} from '@angular/core/testing';
import {
    NgReduxModule,
    NgRedux
} from '@angular-redux/store';

import {AppStateActions} from './app-state.actions';
import {IAppStore} from '../app-store';

describe('Action: AppStateActions', () => {
    let testAction : AppStateActions,
        reduxStore : NgRedux<IAppStore>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports   : [
                NgReduxModule
            ],
            providers : [
                AppStateActions
            ]
        });
    });

    beforeEach(inject([AppStateActions, NgRedux], (action, store) => {
        testAction = action;
        reduxStore = store;

        // configure the redux store and ensure it's been initialized to our satisfaction
        expect(reduxStore).toBeDefined('redux store not injected or initialized');
        expect(reduxStore.dispatch).toBeDefined('redux store is missing dispatch()');

        // create the spy in case any tests need to sniff around
        spyOn(reduxStore, 'dispatch');
    }));

    it('should dispatch APP_BUSY action', () => {
        const expectedAction = {
            type : AppStateActions.APP_BUSY
        };

        testAction.busy();

        expect(reduxStore.dispatch).toHaveBeenCalled();
        expect(reduxStore.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('should dispatch APP_IDLE action', () => {
        const expectedAction = {
            type : AppStateActions.APP_IDLE
        };

        testAction.idle();

        expect(reduxStore.dispatch).toHaveBeenCalled();
        expect(reduxStore.dispatch).toHaveBeenCalledWith(expectedAction);
    });
});
