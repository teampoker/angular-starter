/**
 * This interface defines the contract of an object that implements the request-and-wait pattern.
 * Concrete classes should implement this if they need a way to indicate they are waiting on a resource.
 */
export interface IRequestable {
    /**
     * Indicates the object is waiting on something else.
     * Intended to be used by the UI.
     */
    isWaiting : boolean;
}
