import {combineReducers} from 'redux';
import {
    fromJS,
    Map,
    Record
} from 'immutable';
import {Action} from 'redux';
import * as persistState from 'redux-localstorage';

export interface IPayloadAction extends Action {
    payload? : any;
}

export * from './app-store';

export let middleware = [];

export let enhancers = [];

if (ENV.indexOf('debug') !== -1) {
    // sync with local storage for local debugging
    enhancers.push(persistState(
        '',
        {
            key             : 'angular-starter-web-ui',
            serialize       : store => JSON.stringify(deimmutify(store)),
            deserialize     : state => reimmutify(JSON.parse(state))
        }));

    const ENVIRONMENT : any = window || this;

    if (ENVIRONMENT.devToolsExtension) {
        enhancers.push(ENVIRONMENT.devToolsExtension());
    }
}
else {
    // clear any previous Redux store from local storage
    localStorage.removeItem('angular-starter-web-ui');
}

import {AppState} from './App/app-state.model';
import {UserState} from './User/types/user-state.model';
import {NavState} from './Navigation/types/nav-state.model';
import {DashboardState} from './Dashboard/types/dashboard-state.model';
import {EligibilityState} from './Eligibility/types/eligibility-state.model';
import {ReservationState} from './Reservation/types/reservation-state.model';
import {SearchState} from './Search/types/search-state.model';
import {BeneficiaryState} from './Beneficiary/types/beneficiary-state.model';
import {ExceptionQueueState} from './ExceptionQueue/types/exception-queue-state.model';
import {
    fromStorage as metaDataTypesStateFromStorage,
    MetaDataTypesState
} from './MetaDataTypes/types/meta-data-types-state.model';
import {SessionState} from './AppConfig/types/session-state.model';
import {FormsState} from './Forms/types/forms-state.model';
import {USER_STATE_REDUCER} from './User/user.reducer';
import {NAV_STATE_REDUCER} from './Navigation/nav.reducer';
import {DASHBOARD_STATE_REDUCER} from './Dashboard/dashboard.reducer';
import {SEARCH_STATE_REDUCER} from './Search/search.reducer';
import {RESERVATION_STATE_REDUCER} from './Reservation/reducers/reservation.reducer';
import {BENEFICIARY_STATE_REDUCER} from './Beneficiary/reducers/beneficiary.reducer';
import {META_DATA_TYPES_STATE_REDUCER} from './MetaDataTypes/meta-data-types.reducer';
import {ELIGIBILITY_STATE_REDUCER} from './Eligibility/eligibility.reducer';
import {EXCEPTION_QUEUE_STATE_REDUCER} from './ExceptionQueue/reducers/exception-queue.reducer';
import {SESSION_STATE_REDUCER} from './AppConfig/session.reducer';
import {FORMS_STATE_REDUCER} from './Forms/forms.reducer';
import {APP_STATE_REDUCER} from './App/app-state.reducer';

export interface IAppStore {
    appState?               : AppState;
    userState?              : UserState;
    navState?               : NavState;
    dashboardState?         : DashboardState;
    eligibilityState?       : EligibilityState;
    reservationState?       : ReservationState;
    searchState?            : SearchState;
    beneficiaryState?       : BeneficiaryState;
    metaDataTypesState?     : MetaDataTypesState;
    exceptionQueueState?    : ExceptionQueueState;
    sessionState?           : SessionState;
    formsState?             : FormsState;
}

const APP_STORE_STATE = Record({
    appState                : new AppState(),
    userState               : new UserState(),
    navState                : new NavState(),
    dashboardState          : new DashboardState(),
    eligibilityState        : new EligibilityState(),
    reservationState        : new ReservationState(),
    searchState             : new SearchState(),
    beneficiaryState        : new BeneficiaryState(),
    metaDataTypesState      : new MetaDataTypesState(),
    exceptionQueueState     : new ExceptionQueueState(),
    sessionState            : new SessionState(),
    formsState              : new FormsState()
});

export class AppStore extends APP_STORE_STATE {
    appState?               : AppState;
    userState?              : UserState;
    navState?               : NavState;
    dashboardState?         : DashboardState;
    eligibilityState?       : EligibilityState;
    reservationState?       : ReservationState;
    searchState?            : SearchState;
    beneficiaryState?       : BeneficiaryState;
    metaDataTypesState?     : MetaDataTypesState;
    exceptionQueueState?    : ExceptionQueueState;
    sessionState?           : SessionState;
    formsState?             : FormsState;

    constructor(values? : AppStore | IAppStore) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof AppStore) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // appState
                convertedValues = convertedValues.set('appState', new AppState(convertedValues.get('appState')));

                // userState
                convertedValues = convertedValues.set('userState', new UserState(convertedValues.get('userState')));

                // navState
                convertedValues = convertedValues.set('navState', new NavState(convertedValues.get('navState')));

                // dashboardState
                convertedValues = convertedValues.set('dashboardState', new DashboardState(convertedValues.get('dashboardState')));

                // eligibilityState
                convertedValues = convertedValues.set('eligibilityState', new EligibilityState(convertedValues.get('eligibilityState')));

                // reservationState
                convertedValues = convertedValues.set('reservationState', new ReservationState(convertedValues.get('reservationState')));

                // searchState
                convertedValues = convertedValues.set('searchState', new SearchState(convertedValues.get('searchState')));

                // beneficiaryState
                convertedValues = convertedValues.set('beneficiaryState', new BeneficiaryState(convertedValues.get('beneficiaryState')));

                // exceptionQueueState
                convertedValues = convertedValues.set('exceptionQueueState', new ExceptionQueueState(convertedValues.get('exceptionQueueState')));

                // session
                convertedValues = convertedValues.set('sessionState', new SessionState(convertedValues.get('sessionState')));

                // formsState
                convertedValues = convertedValues.set('formsState', new FormsState(convertedValues.get('formsState')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

export const ROOT_REDUCER = combineReducers<IAppStore>({
    appState                : APP_STATE_REDUCER,
    userState               : USER_STATE_REDUCER,
    navState                : NAV_STATE_REDUCER,
    dashboardState          : DASHBOARD_STATE_REDUCER,
    eligibilityState        : ELIGIBILITY_STATE_REDUCER,
    reservationState        : RESERVATION_STATE_REDUCER,
    searchState             : SEARCH_STATE_REDUCER,
    beneficiaryState        : BENEFICIARY_STATE_REDUCER,
    metaDataTypesState      : META_DATA_TYPES_STATE_REDUCER,
    exceptionQueueState     : EXCEPTION_QUEUE_STATE_REDUCER,
    sessionState            : SESSION_STATE_REDUCER,
    formsState              : FORMS_STATE_REDUCER
});

/**
 * converts the Immutable Application state into a POJO
 * @param store
 * @returns {{userState: any, navState: any, dashboardState: any, eligibilityState: any, reservationState: any, searchState: any, beneficiaryState: any, metaDataTypesState: any, exceptionQueueState: any, sessionState: any, formsState: any}}
 */
export function deimmutify(store : IAppStore) {
    return {
        appState                : store.appState.toJS(),
        userState               : store.userState.toJS(),
        navState                : store.navState.toJS(),
        dashboardState          : store.dashboardState.toJS(),
        eligibilityState        : store.eligibilityState.toJS(),
        reservationState        : store.reservationState.toJS(),
        searchState             : store.searchState.toJS(),
        beneficiaryState        : store.beneficiaryState.toJS(),
        metaDataTypesState      : store.metaDataTypesState.toJS(),
        exceptionQueueState     : store.exceptionQueueState.toJS(),
        sessionState            : store.sessionState.toJS(),
        formsState              : store.formsState.toJS()
    };
}

/**
 * convert the POJO data pulled from local storage into the correct Immutable Record types
 * @param POJO
 * @returns {{userState: UserState, navState: NavState, dashboardState: DashboardState, eligibilityState: EligibilityState, reservationState: ReservationState, searchState: SearchState, beneficiaryState: BeneficiaryState, metaDataTypesState: MetaDataTypesState, exceptionQueueState: ExceptionQueueState, sessionState: SessionState, formsState: FormsState}}
 */
export function reimmutify(POJO : any) {
    try {
        return {
            appState            : new AppState(POJO.appState),
            userState           : new UserState(POJO.userState),
            navState            : new NavState(POJO.navState),
            dashboardState      : new DashboardState(POJO.dashboardState),
            eligibilityState    : new EligibilityState(POJO.eligibilityState),
            reservationState    : new ReservationState(POJO.reservationState),
            searchState         : new SearchState(POJO.searchState),
            beneficiaryState    : new BeneficiaryState(POJO.beneficiaryState),
            metaDataTypesState  : metaDataTypesStateFromStorage(POJO.metaDataTypesState),
            exceptionQueueState : new ExceptionQueueState(POJO.exceptionQueueState),
            sessionState        : new SessionState(POJO.sessionState),
            formsState          : new FormsState(POJO.formsState)
        };
    }
    catch (err) {
        if (POJO) {
            console.error('Error rehydrating Redux Store from Local Storage: ' + err.message + ' with POJO: ' + JSON.stringify(POJO));
        }
    }
}
