import {INITIAL_ELIGIBILITY_STATE} from './eligibility.initial-state';
import {IPayloadAction} from '../app-store';
import {EligibilityState} from './types/eligibility-state.model';

/**
 * App Eligibility State Reducer
 *
 * @param state
 * @param action
 * @returns {any}
 * @constructor
 */
export const ELIGIBILITY_STATE_REDUCER = (state : EligibilityState = INITIAL_ELIGIBILITY_STATE, action : IPayloadAction) : EligibilityState => state;
