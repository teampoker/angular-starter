import {EligibilityState} from './types/eligibility-state.model';

export const INITIAL_ELIGIBILITY_STATE = new EligibilityState();
