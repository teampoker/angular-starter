import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IEligibilityInfo {
}

export const ELIGIBILITY_INFO = Record({
});

export class EligibilityInfo extends ELIGIBILITY_INFO {

    constructor(values? : EligibilityInfo | IEligibilityInfo) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof EligibilityInfo) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
