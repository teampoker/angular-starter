import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IEligibilityInfo,
    EligibilityInfo
} from './eligibility-info.model';

export interface IEligibilityState {
    eligibility : IEligibilityInfo;
}

export const ELIGIBILITY_STATE = Record({
    eligibility : new EligibilityInfo()
});

/**
 * type definition for Redux Store eligibility state
 */
export class EligibilityState extends ELIGIBILITY_STATE {
    eligibility : EligibilityInfo;

    constructor(values? : EligibilityState | IEligibilityState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof EligibilityState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // eligibility
                convertedValues = convertedValues.set('eligibility', new EligibilityInfo(convertedValues.get('eligibility')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
