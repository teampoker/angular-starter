import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../app-store';
import {EligibilityInfo} from './types/eligibility-info.model';

@Injectable()

export class EligibilityActions {
    static ELIGIBILITY_REFRESH          : string = 'ELIGIBILITY_REFRESH';

    constructor(private store : NgRedux<IAppStore>) {}

    eligibilityRefresh(data? : EligibilityInfo) {
        this.store.dispatch({
            type    : EligibilityActions.ELIGIBILITY_REFRESH,
            payload : data
        });
    }
}
