import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {Observable} from 'rxjs/Observable';

import {IAppStore} from '../app-store';
import {EligibilityState} from './types/eligibility-state.model';

@Injectable()

/**
 * implementation for EligibilityStateSelectors: responsible for exposing custom state subscriptions to EligibilityState
 */
export class EligibilityStateSelectors {
    /**
     * EligibilityStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * return observable to EligibilityState
     */
    getEligibilityState() : Observable<EligibilityState> {
        return this.store.select(state => state.eligibilityState);
    }
}
