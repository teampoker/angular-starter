import {List} from 'immutable';

import {INITIAL_RESERVATION_STATE} from '../reservation.initial-state';
import {IPayloadAction} from '../../app-store';
import {ReservationState} from '../types/reservation-state.model';
import {ReservationActions} from '../actions/reservation.actions';
import {BeneficiarySpecialRequirement} from '../../Beneficiary/types/beneficiary-special-requirement.model';
import {ReservationLegState} from '../types/reservation-leg-state.model';
import {NameUuid} from '../../types/name-uuid.model';
import {DateTime} from '../../types/date-time.model';
import {ServiceType} from '../../MetaDataTypes/types/service-type.model';
import {Beneficiary} from '../../Beneficiary/types/beneficiary.model';
import {ReservationRequestedByState} from '../types/reservation-requested-by-state.model';
import {ReservationPassengerState} from '../types/reservation-passenger';
import {ReservationPickupTimeState} from '../types/reservation-pickup-time-state.model';
import {ReservationAdditionalPassengersState} from '../types/reservation-additional-passengers-state.model';
import {ReservationModeOfTransportationState} from '../types/reservation-mode-of-transportation-state.model';
import {ReservationTripIndicatorState} from '../types/reservation-trip-indicator-state.model';
import {ReservationTreatmentTypeState} from '../types/reservation-treatment-type-state.model';
import {ReservationAccelerator} from '../types/reservation-accelerator.model';
import {ReservationAppointmentDateState} from '../types/reservation-appointment-date-state.model';
import {ServiceCategories} from '../../MetaDataTypes/types/service-categories.model';
import {BeneficiaryConnection} from '../../Beneficiary/types/beneficiary-connection.model';
import {AddressLocation} from '../../types/address-location.model';
import {reservationHistoryReducer} from './reservation-history.reducer';
import {ReservationBusinessRules} from '../types/reservation-business-rules-state.model';
import {Reservation} from '../types/reservation.model';
import {reservationCardsReducer} from './reservation-cards.reducer';

/**
 * App Reservation State Reducer
 *
 * @param state
 * @param action
 * @returns {any}
 * @constructor
 */
export const RESERVATION_STATE_REDUCER = (state : ReservationState = INITIAL_RESERVATION_STATE, action : IPayloadAction) : ReservationState => {
    state = reservationCardsReducer(state, action);
    state = reservationHistoryReducer(state, action);

    switch (action.type) {
        case ReservationActions.INIT_RESERVATION_UI_VIEW_STATE :
            state = populateViewModelsFromBeneficiary(state, action.payload.beneficiary, action.payload.modeOfTransportation, action.payload.treatmentType) as ReservationState;
            state = state.withMutations(record => record
                .set('showReasonSelection', false)
                .set('reservation', new Reservation())
            ) as ReservationState;

            break;
        case ReservationActions.INIT_RESERVATION_SPECIAL_REQUIREMENT_TEMP :
            state = state
                .set(
                    'reservationSpecialRequirementTempState',
                    state.get('reservationSpecialRequirementState', List<BeneficiarySpecialRequirement>())
            ) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_TREATMENT_TYPE :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationTreatmentTypeState', 'isFormValid'], true)
                        .setIn(['reservationTreatmentTypeState', 'treatmentType'], new NameUuid(action.payload));
                }) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_APPOINTMENT_DATE :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationAppointmentDateState', 'appointmentDateValid'], action.payload.fieldValue !== '')
                        .setIn(['reservationAppointmentDateState', action.payload.fieldName], action.payload.fieldValue);
                }) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_APPOINTMENT_TIME :
                state = state.setIn(['reservationAppointmentDateState', action.payload.fieldName], action.payload.fieldValue) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_APPOINTMENT_TIME_VALID :
                state = state.setIn(['reservationAppointmentDateState', action.payload.fieldName], action.payload.fieldValue) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_PICKUP_LOCATION :
                state = state.set('reservationPickupLocationState', action.payload) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_DROP_OFF_LOCATION :
                state = state.set('reservationDropOffLocationState', action.payload) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_ADDITIONAL_PASSENGERS :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationAdditionalPassengersState', 'isFormValid'], state.getIn(['reservationAdditionalPassengersState', 'passenger', 'name']) !== '')
                        .setIn(['reservationAdditionalPassengersState', action.payload.fieldName], action.payload.fieldValue);
                }) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_MODE_OF_TRANSPORTATION :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationModeOfTransportationState', 'isFormValid'], action.payload.get('name') !== '')
                        .setIn(['reservationModeOfTransportationState', 'modeOfTransportation'], action.payload);
                }) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_REQUESTED_BY :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationRequestedByState', 'isFormValid'], true)
                        .setIn(['reservationRequestedByState', 'requestedByInfo'], action.payload);
                }) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_MODE_OF_TRANSPORTATION_DRIVER :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationModeOfTransportationState', 'isFormValid'], true)
                        .setIn(['reservationModeOfTransportationState', 'driverInfo'], action.payload);
                }) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_PASSENGER :
            const size : number = state.getIn(['reservationAdditionalPassengersState', 'passengers']).count();

            state = state.withMutations(record => record
                .setIn(['reservationAdditionalPassengersState', 'isFormValid'], true)
                .setIn(['reservationAdditionalPassengersState', 'passenger'], action.payload)
                .setIn(['reservationAdditionalPassengersState', 'passengers', size], action.payload)) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_PREFERRED_TRANSPORTATION_PROVIDER :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationPreferredTransportationState', 'transportationProvider', 'id'], action.payload.fieldValue)
                        .setIn(['reservationPreferredTransportationState', 'transportationProvider', 'value'], action.payload.fieldName);
                }) as ReservationState;

            break;
        case ReservationActions.RESERVATION_CLEAR_PREFERRED_TRANSPORTATION_PROVIDER :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationPreferredTransportationState', 'transportationProvider', 'id'], '')
                        .setIn(['reservationPreferredTransportationState', 'transportationProvider', 'value'], '');
                }) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_PICKUP_TIME :
                state = state.setIn(['reservationPickupTimeState', action.payload.fieldName], action.payload.fieldValue) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_PICKUP_TIME_VALID :
                state = state.setIn(['reservationPickupTimeState', action.payload.fieldName], action.payload.fieldValue) as ReservationState;
            break;
        case ReservationActions.RESERVATION_REMOVE_TEMP_SPECIAL_REQUIREMENT :
            state = state.set(
                'reservationSpecialRequirementTempState',
                state.get('reservationSpecialRequirementTempState', List())
                    .filter(requirement => requirement.getIn(['type', 'id']) !== action.payload.get('id'))
            ) as ReservationState;
            break;
        case ReservationActions.RESERVATION_ADD_TEMP_SPECIAL_REQUIREMENT :
            state = state.set(
                'reservationSpecialRequirementTempState',
                state.get('reservationSpecialRequirementTempState', List()).push(action.payload)
            ) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_FROM_DATE : {
            const index = state.get('reservationSpecialRequirementTempState', List())
                .findKey(value => value.getIn(['type', 'id']) === action.payload.requirement.get('id'));

            if (state.hasIn(['reservationSpecialRequirementTempState', index])) {
                state = state.setIn(
                    ['reservationSpecialRequirementTempState', index, 'fromDate'],
                    action.payload.date
                )
                .setIn(
                    ['reservationSpecialRequirementTempState', index, 'permanentRequirement'],
                    false
                ) as ReservationState;
            }
            break;
        }
        case ReservationActions.RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_THRU_DATE : {
            const index = state.get('reservationSpecialRequirementTempState', List())
                .findKey(value => value.getIn(['type', 'id']) === action.payload.requirement.get('id'));

            if (state.hasIn(['reservationSpecialRequirementTempState', index])) {
                state = state
                    .setIn(
                        ['reservationSpecialRequirementTempState', index, 'thruDate'],
                        action.payload.date
                    )
                    .setIn(
                        ['reservationSpecialRequirementTempState', index, 'permanentRequirement'],
                        false
                    ) as ReservationState;
            }
            break;
        }
        case ReservationActions.RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_SET_PERMANENT : {
            const index = state.get('reservationSpecialRequirementTempState', List())
                .findKey(value => value.getIn(['type', 'id']) === action.payload.requirement.get('id'));

            if (state.hasIn(['reservationSpecialRequirementTempState', index])) {
                state = state
                    .setIn(
                        ['reservationSpecialRequirementTempState', index, 'permanentRequirement'],
                        action.payload.isPermanent
                    ) as ReservationState;

                if (action.payload.isPermanent) {
                    state = state
                        .deleteIn(['reservationSpecialRequirementTempState', index, 'fromDate'])
                        .deleteIn(['reservationSpecialRequirementTempState', index, 'thruDate']) as ReservationState;
                }
            }
            break;
        }
        case ReservationActions.RESERVATION_UPDATE_CONNECTION_REQUEST_ISSUED_BY :
                state = state.set('connectionRequestIssuedBy', action.payload) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_NUMBER_OF_LEGS :
                state = state.set(action.payload.fieldName, action.payload.fieldValue) as ReservationState;
            break;
        case ReservationActions.RESERVATION_ADD_ADDITIONAL_LEG :
                state = createAdditionalReservationLeg(state) as ReservationState;
            break;
        case ReservationActions.RESERVATION_SAVE_LEG :
                state = saveReservationLeg(state) as ReservationState;
            break;
        // case ReservationActions.RESERVATION_CANCEL_SAVE_LEG :
        //     state = state.set('saveLeg', false) as ReservationState;
        //     break;
        case ReservationActions.RESERVATION_UPDATE_LEG_MILEAGE :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationDropOffLocationState', 'mileageDistance'], action.payload.distance)
                        .setIn(['reservationDropOffLocationState', 'durationMinutes'], action.payload.duration);
                }) as ReservationState;

            break;
        case ReservationActions.CREATE_A_RESERVATION :
            state = state.set('confirmationNumber', action.payload) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_ROUND_TRIP_INIDICATOR :
                state = state.setIn(['reservationTripIndicatorState', 'roundTrip'], action.payload) as ReservationState;
            break;
        case ReservationActions.RESERVATION_UPDATE_WILL_CALL_INIDICATOR :
                state = state.setIn(['reservationTripIndicatorState', 'willCall'], action.payload) as ReservationState;
            break;
        case ReservationActions.RESERVATION_SAVE_TEMP_SPECIAL_REQUIREMENT :
            state = state.set(
                'reservationSpecialRequirementState',
                state.get('reservationSpecialRequirementTempState', List<BeneficiarySpecialRequirement>())
            ) as ReservationState;
            break;
        case ReservationActions.RESERVATION_SAVE_PASSENGER_TEMP_SPECIAL_REQUIREMENT : {
            const passengerIndex = getPassengerIndex(state, action.payload.get('uuid'));

            if (passengerIndex !== undefined) {
                state = state.setIn(
                    ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'specialRequirements'],
                    state.getIn(
                        ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements'],
                        List<BeneficiarySpecialRequirement>()
                    )
                ) as ReservationState;
            }

            break;
        }
        case ReservationActions.RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_SET_PERMANENT : {
            const passengerIndex = getPassengerIndex(state, action.payload.passenger.get('uuid'));
            const requirementIndex = getSpecialRequirementIndex(
                state.getIn(['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements']),
                action.payload.requirement.get('id')
            );

            if (requirementIndex !== undefined) {
                state = state.setIn(
                    ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements', requirementIndex, 'permanentRequirement'],
                    action.payload.isPermanent
                ) as ReservationState;
            }

            break;
        }
        case ReservationActions.RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_FROM_DATE : {
            const passengerIndex = getPassengerIndex(state, action.payload.passenger.get('uuid'));
            const requirementIndex = getSpecialRequirementIndex(
                state.getIn(['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements']),
                action.payload.requirement.get('id')
            );

            if (requirementIndex !== undefined) {
                state = state.setIn(
                    ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements', requirementIndex, 'fromDate'],
                    action.payload.date
                ) as ReservationState;
            }

            break;
        }
        case ReservationActions.RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_THRU_DATE : {
            const passengerIndex = getPassengerIndex(state, action.payload.passenger.get('uuid'));
            const requirementIndex = getSpecialRequirementIndex(
                state.getIn(['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements']),
                action.payload.requirement.get('id')
            );

            if (requirementIndex !== undefined) {
                state = state.setIn(
                    ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements', requirementIndex, 'thruDate'],
                    action.payload.date
                ) as ReservationState;
            }

            break;
        }
        case ReservationActions.RESERVATION_ADD_PASSENGER_TEMP_SPECIAL_REQUIREMENT : {
            const passengerIndex = getPassengerIndex(state, action.payload.passenger.get('uuid'));
            // const specialRequirements = state.getIn(
            //     ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements'],
            //     List<BeneficiarySpecialRequirement>()
            // );

            if (passengerIndex !== undefined) {
                const requirements = state.getIn(
                    ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements'],
                    List<BeneficiarySpecialRequirement>()
                );

                state = state.setIn(
                    ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements'],
                    requirements
                        .push(action.payload.requirement)
                ) as ReservationState;
            }

            break;
        }
        case ReservationActions.RESERVATION_REMOVE_PASSENGER_TEMP_SPECIAL_REQUIREMENT : {
            const passengerIndex = getPassengerIndex(state, action.payload.passenger.get('uuid'));

            if (passengerIndex !== undefined) {
                state = state.setIn(
                    ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements'],
                    state.getIn(
                        ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements']
                    ).filter(requirement => requirement.getIn(['type', 'id']) !== action.payload.requirement.get('id'))
                ) as ReservationState;
            }

            break;
        }
        case ReservationActions.INIT_RESERVATION_PASSENGER_SPECIAL_REQUIREMENT_TEMP : {
            const passengerIndex = getPassengerIndex(state, action.payload.get('uuid'));

            if (passengerIndex !== undefined) {
                state = state.setIn(
                    ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'tempSpecialRequirements'],
                    state.getIn(
                        ['reservationAdditionalPassengersState', 'passengers', passengerIndex, 'specialRequirements'],
                        List<BeneficiarySpecialRequirement>()
                    )
                ) as ReservationState;
            }

            break;
        }
        case ReservationActions.RESERVATION_REMOVE_PASSENGER :
            state = state.setIn(
                ['reservationAdditionalPassengersState', 'passengers'],
                state.getIn(['reservationAdditionalPassengersState', 'passengers'])
                    .filter(passenger => passenger.get('uuid') !== action.payload.get('uuid'))
            ) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_RESERVATIONS_LIST :
            state = state.set('reservations', List<Reservation>(action.payload)) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_TREATMENT_TYPE_MESSAGE :
                state = getServiceCoveragePlanFromRuleMsg(state, action.payload) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_LOCATION_MESSAGE :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationBusinessRules', 'location'],  action.payload)
                        .setIn(['reservationBusinessRules', 'locationOverrideReason'], List());
                }) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_ESCORT_MESSAGE :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationBusinessRules', 'escort'],  action.payload)
                        .setIn(['reservationBusinessRules', 'escortOverrideReason'], List());
                }) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_MOT_MESSAGE :
                state = state.withMutations(record => {
                    return record
                        .setIn(['reservationBusinessRules', 'modeOfTransportation'],  action.payload)
                        .setIn(['reservationBusinessRules', 'modeOfTransportationOverrideReason'], List());
                }) as ReservationState;

            break;
        case ReservationActions.RESERVATION_GET_PICKUP_LOCATIONS :
            state = state.set('recentPickupLocations', action.payload) as ReservationState;

            break;
        case ReservationActions.RESERVATION_GET_DROPOFF_LOCATIONS :
            state = state.set('recentDropoffLocations', action.payload) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_RESERVATION : {
            const index = state.get('reservations').findKey(reservation => reservation.get('uuid') === action.payload.get('uuid'));

            if (index >= 0) {
                state = state.setIn(['reservations', index], action.payload) as ReservationState;
            }
            else {
                state = state.set('reservations', state.get('reservations').push(action.payload)) as ReservationState;
            }

            break;
        }
        case ReservationActions.RESERVATION_GET_RESERVATION :
            state = state.set('reservation', action.payload) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_TREATMENT_TYPE_OVERRIDE_REASON :
            state = state.setIn(['reservationBusinessRules', 'treatmentTypeOverrideReason', action.payload.index], action.payload.choice) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_LOCATION_OVERRIDE_REASON :
            state = state.setIn(['reservationBusinessRules', 'locationOverrideReason', action.payload.index], action.payload.choice) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_ESCORT_OVERRIDE_REASON :
            state = state.setIn(['reservationBusinessRules', 'escortOverrideReason', action.payload.index], action.payload.choice) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_MOT_OVERRIDE_REASON :
            state = state.setIn(['reservationBusinessRules', 'modeOfTransportationOverrideReason', action.payload.index], action.payload.choice) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_EDIT_LEG :
            state = prepReservationLegForEditing(state, action.payload) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_DELETE_LEG :
            state = state.withMutations(record => record
                .set('deleteLeg', action.payload)
                .set('showReasonSelection', true)
            ) as ReservationState;

            break;
        case ReservationActions.RESERVATION_UPDATE_LEG :
            state = updateReservationLeg(state) as ReservationState;

            break;
        case ReservationActions.RESERVATION_CANCEL_EDIT_LEG :
            state = state.set('editLeg', null) as ReservationState;

            break;
        case ReservationActions.RESERVATION_GET_EDIT_RESERVATION :
            state = state.set('reservation', action.payload) as ReservationState;
                state = state.withMutations(record => {
                    return record
                        .set('isEditing', true)
                        .set('showReasonSelection', false)
                        .set('reservation', action.payload)
                        .set('confirmationNumber', action.payload.get('referenceId'))
                        .set('reservationLegState', action.payload.get('legs'));
                }) as ReservationState;

            break;
        case ReservationActions.RESERVATION_SET_EVENT_REASONS :
            state = state.set('reservationEventReasons', action.payload) as ReservationState;

            break;
        case ReservationActions.RESERVATION_SET_LEG_EVENT_REASON :
            state = state.withMutations(record => record
                .setIn(['reservationLegState', action.payload.legIndex, 'eventReasonUuid'], action.payload.reason.get('uuid'))
                .setIn(['reservationLegState', action.payload.legIndex, 'eventComment'], action.payload.comment)
            ) as ReservationState;

            break;
        case ReservationActions.RESERVATION_SET_RESERVATION_EVENT_REASON :
            state = state.withMutations(record => record
                .setIn(['reservation', 'eventReasonUuid'], action.payload.reason.get('uuid'))
                .setIn(['reservation', 'eventComment'], action.payload.comment)
            ) as ReservationState;

            break;
        case ReservationActions.RESERVATION_SET_LEG_MASS_TRANSIT_ROUTES :
            state = state.setIn(
                ['reservationModeOfTransportationState', 'massTransitRoutes'],
                action.payload
            ) as ReservationState;

            break;
        default :
            return state;
    }

    return state;
};

/**
 * updates the individual sections of the reservation state to allow for editing of a reservation leg
 *
 * @param {ReservationState} state current ReservationState
 * @param {number} index
 */
function prepReservationLegForEditing(state : ReservationState, index : number) {
    const leg : ReservationLegState = state.getIn(['reservationLegState', index]);

    state = state.withMutations(record => record
        .set('editLeg', index)
        .set('showReasonSelection', state.get('isEditing') ? true : false)
        .set('reservationSpecialRequirementState', leg.get('specialRequirements'))
        .set('reservationSpecialRequirementTempState', leg.get('specialRequirements'))
        .setIn(['reservationRequestedByState', 'isFormValid'], true)
        .setIn(['reservationRequestedByState', 'requestedByInfo'], leg.get('requestedBy'))
        .setIn(['reservationTreatmentTypeState', 'isFormValid'], true)
        .setIn(['reservationTreatmentTypeState', 'treatmentType'], leg.get('treatmentType'))
        .setIn(['reservationAppointmentDateState', 'appointmentDateValid'], true)
        .setIn(['reservationAppointmentDateState', 'appointmentDate'], leg.getIn(['reservationDateTime', 'date']))
        .setIn(['reservationAppointmentDateState', 'appointmentTimeValid'], true)
        .setIn(['reservationAppointmentDateState', 'appointmentTime'], leg.getIn(['reservationDateTime', 'time']))
        .set('reservationPickupLocationState', leg.get('pickupLocation'))
        .setIn(['reservationPickupLocationState', 'isFormValid'], true)
        .set('reservationDropOffLocationState', leg.get('dropOffLocation'))
        .setIn(['reservationDropOffLocationState', 'isFormValid'], true)
        .setIn(['reservationAdditionalPassengersState', 'additionalPassengers'], leg.get('additionalPassengers', List()).count() > 0)
        .setIn(['reservationAdditionalPassengersState', 'passengers'], leg.get('additionalPassengers'))
        .setIn(['reservationModeOfTransportationState', 'modeOfTransportation'], leg.get('modeOfTransportation'))
        .setIn(['reservationModeOfTransportationState', 'driverInfo'], leg.get('driverInfo'))
        .setIn(['reservationModeOfTransportationState', 'isFormValid'], true)
    ) as ReservationState;
    return state;
}

/**
 * looks for a service coverage plan uuid passed back from teh rules api
 *
 * @param state current ReservationState
 * @param payload api response
 */
function getServiceCoveragePlanFromRuleMsg(state : ReservationState, payload : any) {
    let serviceCoveragePlan : any = {},
        approved            : any = null,
        pending             : any = {};

    payload.forEach(record => {
        if (record.status === 'APPROVED') {
            approved = record.serviceCoveragePlan;
        }
        else if (record.status === 'PENDING') {
            pending = record.serviceCoveragePlan;
        }
    });

    serviceCoveragePlan = approved ? approved : pending;

    state = state.withMutations(record => record
        .set('serviceCoveragePlan', serviceCoveragePlan)
        .setIn(['reservationBusinessRules', 'treatmentType'],  payload)
        .setIn(['reservationBusinessRules', 'treatmentTypeOverrideReason'], List())
    ) as ReservationState;

    return state;
}

/**
 * marks state as saved and runs the create leg portion
 *
 * @param state current ReservationState
 */
function saveReservationLeg(state : ReservationState) {
    state = createAdditionalReservationLeg(state) as ReservationState;
    state = state.set('saveLeg', true) as ReservationState;
    return state;
}

/**
 * adds edited information back to the legs state and marks editing as null and saveLeg as true
 *
 * @param state current ReservationState
 */
function updateReservationLeg(state : ReservationState) {
    let reservationLegState : List<ReservationLegState> = state.get('reservationLegState') as List<ReservationLegState>;

    const leg                : number = state.get('editLeg'),
        requestedByInfo      : NameUuid = state.getIn(['reservationRequestedByState', 'requestedByInfo']),
        treatmentType        : NameUuid = state.getIn(['reservationTreatmentTypeState', 'treatmentType']),
        modeOfTransportation : NameUuid = state.getIn(['reservationModeOfTransportationState', 'modeOfTransportation']),
        driverInfo           : NameUuid = state.getIn(['reservationModeOfTransportationState', 'driverInfo']),
        additionalPassengers : List<ReservationPassengerState> = state.getIn(['reservationAdditionalPassengersState', 'passengers']),
        appointment          : DateTime = state.get('reservationAppointmentDateState'),
        specialRequirements  : List<BeneficiarySpecialRequirement> = state.get('reservationSpecialRequirementState'),
        pickupTime           : Date = state.getIn(['reservationPickupTimeState', 'pickupTime']),
        pickupLocation       : AddressLocation = state.get('reservationPickupLocationState'),
        dropOffLocation      : AddressLocation = state.get('reservationDropOffLocationState'),
        businessRules        : ReservationBusinessRules = state.get('reservationBusinessRules');

    let overrideReasons      : List<NameUuid> = List<NameUuid>(),
        treatmentTypeReasons : List<NameUuid> = List<NameUuid>(),
        locationReasons      : List<NameUuid> = List<NameUuid>(),
        escortReasons        : List<NameUuid> = List<NameUuid>(),
        motReasons           : List<NameUuid> = List<NameUuid>();

    // grab any exception queue reasons
    treatmentTypeReasons = List<NameUuid>(businessRules.get('treatmentTypeOverrideReason', [])
        .map(value => new NameUuid(value)));

    locationReasons = List<NameUuid>(businessRules.get('locationOverrideReason', [])
        .map(value => new NameUuid(value)));

    escortReasons = List<NameUuid>(businessRules.get('escortOverrideReason', [])
        .map(value => new NameUuid(value)));

    motReasons = List<NameUuid>(businessRules.get('modeOfTransportationOverrideReason', [])
        .map(value => new NameUuid(value)));

    overrideReasons = overrideReasons.concat(treatmentTypeReasons, locationReasons, escortReasons, motReasons) as List<NameUuid>;

    reservationLegState = reservationLegState.withMutations(record => record
            .setIn([leg, 'requestedBy'], requestedByInfo)
            .setIn([leg, 'treatmentType'], treatmentType)
            .setIn([leg, 'pickupLocation'], pickupLocation)
            .setIn([leg, 'dropOffLocation'], dropOffLocation)
            .setIn([leg, 'reservationDateTime', 'date'], appointment.get('appointmentDate'))
            .setIn([leg, 'reservationDateTime', 'time'], appointment.get('appointmentTime'))
            .setIn([leg, 'specialRequirements'], specialRequirements)
            .setIn([leg, 'modeOfTransportation'], modeOfTransportation)
            .setIn([leg, 'driverInfo'], driverInfo)
            .setIn([leg, 'additionalPassengers'], additionalPassengers)
            .setIn([leg, 'pickupTime'], pickupTime)
            .setIn([leg, 'legDistance'], dropOffLocation.get('mileageDistance'))
            .setIn([leg, 'legDuration'], dropOffLocation.get('durationMinutes'))
            .setIn([leg, 'exceptionReasons'], overrideReasons)
        ) as List<ReservationLegState>;

    state = state.withMutations(record => record
            .set('editLeg', null)
            .set('saveLeg', true)
            .set('reservationLegState', reservationLegState)
            .set('reservationBusinessRules', new ReservationBusinessRules())
    ) as ReservationState;

    return state;
}

/**
 * copies current reservation form data into a
 * seperate leg of the reservation state view model
 *
 * @param state current ReservationState
 */
function createAdditionalReservationLeg(state : ReservationState) {
    let reservationLegState  : List<ReservationLegState> = state.get('reservationLegState') as List<ReservationLegState>,
        pickupLocation       : AddressLocation = state.get('reservationPickupLocationState'),
        dropOffLocation      : AddressLocation = state.get('reservationDropOffLocationState'),
        overrideReasons      : List<NameUuid> = List<NameUuid>(),
        treatmentTypeReasons : List<NameUuid> = List<NameUuid>(),
        locationReasons      : List<NameUuid> = List<NameUuid>(),
        escortReasons        : List<NameUuid> = List<NameUuid>(),
        motReasons           : List<NameUuid> = List<NameUuid>();

    const size : number = state.get('reservationLegState', List()).count(),
        requestedByInfo      : NameUuid = state.getIn(['reservationRequestedByState', 'requestedByInfo']),
        treatmentType        : NameUuid = state.getIn(['reservationTreatmentTypeState', 'treatmentType']),
        modeOfTransportation : NameUuid = state.getIn(['reservationModeOfTransportationState', 'modeOfTransportation']),
        driverInfo           : NameUuid = state.getIn(['reservationModeOfTransportationState', 'driverInfo']),
        additionalPassengers : List<ReservationPassengerState> = state.getIn(['reservationAdditionalPassengersState', 'passengers']),
        appointment          : DateTime = state.get('reservationAppointmentDateState'),
        specialRequirements  : List<BeneficiarySpecialRequirement> = state.get('reservationSpecialRequirementState'),
        businessRules        : ReservationBusinessRules = state.get('reservationBusinessRules'),
        // if will call then pickup time is null
        pickupTime           : Date = state.getIn(['reservationTripIndicatorState', 'willCall'])
                                ? null : state.getIn(['reservationPickupTimeState', 'pickupTime']);

    // grab any exception queue reasons
    treatmentTypeReasons = List<NameUuid>(businessRules.get('treatmentTypeOverrideReason', [])
        .map(value => new NameUuid(value)));

    locationReasons = List<NameUuid>(businessRules.get('locationOverrideReason', [])
        .map(value => new NameUuid(value)));

    escortReasons = List<NameUuid>(businessRules.get('escortOverrideReason', [])
        .map(value => new NameUuid(value)));

    motReasons = List<NameUuid>(businessRules.get('modeOfTransportationOverrideReason', [])
        .map(value => new NameUuid(value)));

    overrideReasons = overrideReasons.concat(treatmentTypeReasons, locationReasons, escortReasons, motReasons) as List<NameUuid>;

    reservationLegState = reservationLegState.withMutations(record => record
            .setIn([size, 'requestedBy'], requestedByInfo)
            .setIn([size, 'treatmentType'], treatmentType)
            .setIn([size, 'pickupLocation'], pickupLocation)
            .setIn([size, 'dropOffLocation'], dropOffLocation)
            .setIn([size, 'reservationDateTime', 'date'], appointment.get('appointmentDate'))
            .setIn([size, 'reservationDateTime', 'time'], appointment.get('appointmentTime'))
            .setIn([size, 'specialRequirements'], specialRequirements)
            .setIn([size, 'modeOfTransportation'], modeOfTransportation)
            .setIn([size, 'driverInfo'], driverInfo)
            .setIn([size, 'additionalPassengers'], additionalPassengers)
            .setIn([size, 'pickupTime'], pickupTime)
            .setIn([size, 'legDistance'], dropOffLocation.get('mileageDistance'))
            .setIn([size, 'legDuration'], dropOffLocation.get('durationMinutes'))
            .setIn([size, 'exceptionReasons'], overrideReasons)
            .setIn([size, 'serviceCoveragePlan'], state.get('serviceCoveragePlan'))
        ) as List<ReservationLegState>;

    // is this a round trip only reservation?
    if (size < 1) {
        // pickupLocation
        const oldPickupLocation : AddressLocation = pickupLocation;
        pickupLocation  = dropOffLocation as AddressLocation;
        dropOffLocation = oldPickupLocation as AddressLocation;

        // dropOffLocation
        dropOffLocation = dropOffLocation.withMutations(record => record
                .set('mileageDistance', pickupLocation.get('mileageDistance'))
                .set('durationMinutes', pickupLocation.get('durationMinutes'))
        ) as AddressLocation;
    }
    else {
        pickupLocation  = dropOffLocation as AddressLocation;
        dropOffLocation = new AddressLocation();
    }

    // go clean pickup
    pickupLocation = pickupLocation.withMutations(record => record
            .set('mileageDistance', null)
            .set('durationMinutes', null)
    ) as AddressLocation;

    state = state.withMutations(record => record
            .set('saveLeg', false)
            .setIn(['reservationTripIndicatorState', 'roundTrip'], true)
            // get with services on will call
            .setIn(['reservationTripIndicatorState', 'willCall'], false)
            .set('reservationLegState', reservationLegState)
            // we need to reverse the drop off and pickup for round trip
            .set('reservationPickupLocationState', pickupLocation)
            .set('reservationDropOffLocationState', dropOffLocation)
            .set('reservationBusinessRules', new ReservationBusinessRules())
    ) as ReservationState;

    return state;
}

function getPassengerIndex(state : ReservationState, uuid : string) : number | undefined {
    return state.getIn(['reservationAdditionalPassengersState', 'passengers'])
        .findKey(passenger => passenger.get('uuid') === uuid);
}

function getSpecialRequirementIndex(requirements : List<BeneficiarySpecialRequirement>, uuid : string) : number | undefined {
    return requirements.findKey(requirement => requirement.getIn(['type', 'id']) === uuid);
}

/**
 * copies beneficiary state data into a series of view models
 * that are more easily consumed by the UI templates
 * @param state
 * @param beneficiary
 * @param modeOfTransportation
 * @param treatmentType
 * @returns {ReservationState}
 */
function populateViewModelsFromBeneficiary(state : ReservationState, beneficiary : Beneficiary, modeOfTransportation : List<ServiceType>, treatmentType : List<ServiceType>) : ReservationState {
    let requestedByState                  : ReservationRequestedByState              = new ReservationRequestedByState(),
        additionalPassengerState          : ReservationAdditionalPassengersState     = new ReservationAdditionalPassengersState(),
        modeOfTransportationState         : ReservationModeOfTransportationState     = new ReservationModeOfTransportationState(),
        reservationTreatmentTypeState     : ReservationTreatmentTypeState            = new ReservationTreatmentTypeState(),
        connectionsAcclerator             : List<ReservationAccelerator>             = List<ReservationAccelerator>(),
        treatmentTypesAccelerator         : List<NameUuid>                           = List<NameUuid>();

    const beneficiarySpecialRequirement   : List<BeneficiarySpecialRequirement> = beneficiary.get('specialRequirements', List<BeneficiarySpecialRequirement>()),
          treatmentTypeCategories         : List<ServiceCategories>             = treatmentType.getIn([0, 'serviceCategories']) as List<ServiceCategories>,
          transportationCategories        : List<ServiceCategories>             = modeOfTransportation.getIn([0, 'serviceCategories']) as List<ServiceCategories>,
          reservationLegState             : List<ReservationLegState>           = List<ReservationLegState>(),
          reservationAppointmentDateState : ReservationAppointmentDateState     = new ReservationAppointmentDateState(),
          reservationPickupLocationState  : AddressLocation                     = new AddressLocation(),
          reservationDropOffLocationState : AddressLocation                     = new AddressLocation(),
          reservationTripIndicatorState   : ReservationTripIndicatorState       = new ReservationTripIndicatorState(),
          reservationBusinessRules        : ReservationBusinessRules            = new ReservationBusinessRules(),
          recentPickupLocations           : List<NameUuid>                      = List<NameUuid>(),
          recentDropoffLocations          : List<NameUuid>                      = List<NameUuid>(),
          reservationPickupTimeState      : ReservationPickupTimeState          = new ReservationPickupTimeState(),
          reservationRules                : ReservationBusinessRules            = new ReservationBusinessRules();

    // ReservationRequestedByState
    if (beneficiary.get('connections') !== undefined) {
        const size        : number                      = beneficiary.get('connections', List()).count(),
              connections : List<BeneficiaryConnection> = beneficiary.get('connections', List()) as List<BeneficiaryConnection>;

        let accelerator : ReservationAccelerator = new ReservationAccelerator();

        if (size > 0) {
            accelerator = accelerator.withMutations(record => record
                    .set('uuid', beneficiary.get('uuid'))
                    .set('name', beneficiary.get('firstName') + ' ' + beneficiary.get('lastName'))
                    .set('connectionType', 'Self')
                    .set('addAConnection', false)) as ReservationAccelerator;

            connectionsAcclerator = connectionsAcclerator.push(accelerator);

            for (let i = 0; i < size; i++) {
                accelerator = accelerator.withMutations(record => record
                        .set('uuid', connections.getIn([i, 'uuid']))
                        .set('name', connections.getIn([i, 'firstName']) + ' ' + connections.getIn([i, 'lastName']))
                        .set('connectionType', connections.getIn([i, 'types', 0, 'value']))
                        .set('addAConnection', false)) as ReservationAccelerator;

                connectionsAcclerator = connectionsAcclerator.push(accelerator);
            }
        }
        else {
            accelerator = accelerator.withMutations(record => record
                    .set('uuid', beneficiary.get('uuid'))
                    .set('name', beneficiary.get('firstName') + ' ' + beneficiary.get('lastName'))
                    .set('connectionType', 'Self')
                    .set('addAConnection', false)) as ReservationAccelerator;

            connectionsAcclerator = connectionsAcclerator.push(accelerator);
        }

        // go back and add new requestedByAccelerator to results
        connectionsAcclerator = connectionsAcclerator.push(new ReservationAccelerator());

        requestedByState = requestedByState.set('requestedByAccelerator', connectionsAcclerator) as ReservationRequestedByState;
    }

    // ReservationTreatmentTypeState
    if (treatmentTypeCategories !== undefined) {
        let typesList : List<ServiceCategories> = List<ServiceCategories>();

        for (let i = 0; i < treatmentTypeCategories.size; i++) {
           typesList = typesList.concat(treatmentTypeCategories.getIn([i, 'serviceOfferings'])) as List<ServiceCategories>;
        }

        for (let i = 0; i < typesList.size; i++) {
            treatmentTypesAccelerator = treatmentTypesAccelerator.withMutations(type => type
                    .setIn([i, 'uuid'], typesList.getIn([i, 'uuid']))
                    .setIn([i, 'name'], typesList.getIn([i, 'name']))) as List<NameUuid>;
        }

        reservationTreatmentTypeState = reservationTreatmentTypeState
                .set('treatmentTypesAccelerator', treatmentTypesAccelerator) as ReservationTreatmentTypeState;
    }

    // ReservationModeOfTransportationState
    if (transportationCategories !== undefined) {
        modeOfTransportationState = modeOfTransportationState
                .set('modeOfTransportationDropdown', transportationCategories) as ReservationModeOfTransportationState;

        if (beneficiary.get('connections') !== undefined) {
            if (beneficiary.get('connections', List()).count() > 0) {
                connectionsAcclerator = beneficiary.get('connections', List()).filter(driver => driver.getIn(['types', 0, 'value']) === 'Driver').map(value =>
                    new ReservationAccelerator().withMutations(record => record
                                .set('uuid', value.get('uuid'))
                                .set('name', value.get('firstName') + ' ' + value.get('lastName'))
                                .set('connectionType', value.getIn(['types', 0, 'value']))
                                .set('addAConnection', false)));
            }

            // go back and add new modeOfTransportationAccelerator to results
            connectionsAcclerator = connectionsAcclerator.push(new ReservationAccelerator());

            // go back and new modeOfTransportationAccelerator to results
            modeOfTransportationState = modeOfTransportationState.set('modeOfTransportationAccelerator', connectionsAcclerator) as ReservationModeOfTransportationState;
        }
    }

    // ReservationAdditionalPassengersState
    if (beneficiary.get('connections') !== undefined) {
        if (beneficiary.get('connections').count() > 0) {
            connectionsAcclerator = beneficiary.get('connections', List()).map(value =>
                new ReservationAccelerator().withMutations(record => record
                        .set('uuid', value.get('uuid'))
                        .set('name', value.get('firstName') + ' ' + value.get('lastName'))
                        .set('connectionType', value.getIn(['types', 0, 'value']))
                        .set('addAConnection', false)));
        }

        // go back and add new additionalPassengerAccelerator to results
        connectionsAcclerator = connectionsAcclerator.push(new ReservationAccelerator());

        // filter out duplicate entries
        connectionsAcclerator = connectionsAcclerator.filter(
            (connection, key) => key === connectionsAcclerator.findKey(
                x => x.get('uuid') === connection.get('uuid')
            )
        ) as List<ReservationAccelerator>;

        // go back and new additionalPassengerAccelerator to results
        additionalPassengerState = additionalPassengerState.set('additionalPassengersAccelerator', connectionsAcclerator) as ReservationAdditionalPassengersState;
    }

    // update reservation demographics
    return state.withMutations(record => record
            .set('saveLeg', false)
            .set('reservationRequestedByState', requestedByState)
            .set('reservationAppointmentDateState', reservationAppointmentDateState)
            .set('reservationAdditionalPassengersState', additionalPassengerState)
            .set('reservationModeOfTransportationState', modeOfTransportationState)
            .set('reservationTreatmentTypeState', reservationTreatmentTypeState)
            .set('reservationSpecialRequirementState', beneficiarySpecialRequirement)
            .set('reservationPickupLocationState', reservationPickupLocationState)
            .set('reservationDropOffLocationState', reservationDropOffLocationState)
            .set('reservationLegState', reservationLegState)
            .set('reservationBusinessRules', reservationBusinessRules)
            .set('recentPickupLocations', recentPickupLocations)
            .set('recentDropoffLocations', recentDropoffLocations)
            .set('reservationTripIndicatorState', reservationTripIndicatorState)
            .set('reservationPickupTimeState', reservationPickupTimeState)
            .set('reservationBusinessRules', reservationRules)
            .set('confirmationNumber', '')
            .set('editLeg', null)
            .set('deleteLeg', null)
            .set('isEditing', false)
            .set('serviceCoveragePlan', new NameUuid())
    ) as ReservationState;
}
