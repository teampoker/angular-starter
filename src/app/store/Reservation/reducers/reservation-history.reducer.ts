import {Map} from 'immutable';

import {IPayloadAction} from '../../app-store';
import {ReservationState} from '../types/reservation-state.model';
import {ReservationHistoryActions} from '../actions/reservation-history.actions';

/**
 * App Reservation State Reducer
 *
 * @param state
 * @param action
 * @returns {any}
 * @constructor
 */
export function reservationHistoryReducer(state : ReservationState, action : IPayloadAction) : ReservationState {
    switch (action.type) {
        case ReservationHistoryActions.RESERVATION_HISTORY_CLEAR_STATUS_FILTER :
            state = state.setIn(
                ['reservationHistory', 'filters', 'tripStatus'],
                Map<string, boolean>()
            ) as ReservationState;

            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_CLEAR_TREATMENT_TYPE_FILTER :
            state = state.setIn(
                ['reservationHistory', 'filters', 'treatmentType'],
                Map<string, boolean>()
            ) as ReservationState;

            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_CLEAR_TRIP_TYPE_FILTER :
            state = state.setIn(
                ['reservationHistory', 'filters', 'tripType'],
                Map<string, boolean>()
            ) as ReservationState;

            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_NEXT_PAGE :
            state = state.setIn(
                ['reservationHistory', 'pageOffset'],
                state.getIn(['reservationHistory', 'pageOffset']) + 1
            ) as ReservationState;

            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_PREVIOUS_PAGE : {
            let page = state.getIn(['reservationHistory', 'pageOffset'], 0);

            // we don't want to decrement if they're already on offset 0
            page = page > 0 ? page - 1 : 0;

            state = state.setIn(['reservationHistory', 'pageOffset'], page) as ReservationState;

            break;
        }
        case ReservationHistoryActions.RESERVATION_HISTORY_SET_DATE_FILTER :
            state = state.setIn(['reservationHistory', 'filters', 'dateRange'], action.payload) as ReservationState;
            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_SET_ITEMS_PER_PAGE :
            state = state.setIn(['reservationHistory', 'itemsPerPage'], action.payload) as ReservationState;
            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_SET_SORT_OPTIONS :
            state = state.setIn(['reservationHistory', 'sortOption'], action.payload) as ReservationState;
            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_SET_STATUS_FILTER_VALUE :
            state = state.setIn(
                ['reservationHistory', 'filters', 'tripStatus', action.payload.type.get('id')],
                action.payload.active
            ) as ReservationState;
            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_SET_TEXT_FILTER :
            state = state.setIn(['reservationHistory', 'filters', 'text'], action.payload) as ReservationState;
            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_SET_TREATMENT_TYPE_FILTER_VALUE :
            state = state.setIn(
                ['reservationHistory', 'filters', 'treatmentType', action.payload.type.get('uuid')],
                action.payload.active
            ) as ReservationState;
            break;
        case ReservationHistoryActions.RESERVATION_HISTORY_SET_TRIP_TYPE_FILTER_VALUE :
            state = state.setIn(
                ['reservationHistory', 'filters', 'tripType', action.payload.type],
                action.payload.active
            ) as ReservationState;
            break;
        default :
            break;
    }

    return state;
}
