import {IPayloadAction} from '../../app-store';
import {ReservationCardState} from '../types/reservation-card-state.model';
import {ReservationState} from '../types/reservation-state.model';
import {ReservationCardsActions} from '../actions/reservation-cards.actions';

/**
 * Reservation Cards State Reducer
 *
 * @param state
 * @param action
 * @returns {any}
 * @constructor
 */
export function reservationCardsReducer(state : ReservationState, action : IPayloadAction) : ReservationState {
    switch (action.type) {
        case ReservationCardsActions.RESERVATION_CARDS_HISTORY_CLOSE_ITEM :
            state = state.setIn(['reservationCardsState', 'history'], new ReservationCardState()) as ReservationState;

            break;
        case ReservationCardsActions.RESERVATION_CARDS_HISTORY_SELECT_ITEM :
            state = state.setIn(['reservationCardsState', 'history', 'selectedItemUuid'], action.payload) as ReservationState;

            break;
        case ReservationCardsActions.RESERVATION_CARDS_HISTORY_TOGGLE_EXPAND_LEG :
            state = state.setIn(
                ['reservationCardsState', 'history', 'expandedItems', action.payload],
                !state.getIn(['reservationCardsState', 'history', 'expandedItems', action.payload], false)
            ) as ReservationState;

            break;
        case ReservationCardsActions.RESERVATION_CARDS_SCHEDULED_CLOSE_ITEM :
            state = state.setIn(['reservationCardsState', 'scheduled'], new ReservationCardState()) as ReservationState;

            break;
        case ReservationCardsActions.RESERVATION_CARDS_SCHEDULED_SELECT_ITEM :
            state = state.setIn(['reservationCardsState', 'scheduled', 'selectedItemUuid'], action.payload) as ReservationState;

            break;
        case ReservationCardsActions.RESERVATION_CARDS_SCHEDULED_TOGGLE_EXPAND_LEG :
            state = state.setIn(
                ['reservationCardsState', 'scheduled', 'expandedItems', action.payload],
                !state.getIn(['reservationCardsState', 'scheduled', 'expandedItems', action.payload], false)
            ) as ReservationState;

            break;
        default :
            break;
    }

    return state;
}
