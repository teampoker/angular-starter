import {ReservationState} from './types/reservation-state.model';

export const INITIAL_RESERVATION_STATE = new ReservationState();
