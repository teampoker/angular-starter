import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';
import * as moment from 'moment';

import {IAppStore} from '../../app-store';
import {NavActions} from '../../Navigation/nav.actions';
import {AlertItem, EnumAlertType} from '../../Navigation/types/alert-item.model';
import {MetaDataTypesSelectors} from '../../MetaDataTypes/meta-data-types.selectors';
import {BeneficiaryStateSelectors} from '../../Beneficiary/selectors/beneficiary.selectors';
import {BeneficiaryMedicalConditionsStateSelectors} from '../../Beneficiary/selectors/beneficiary-medical-conditions.selectors';
import {BeneficiaryContactMechanism} from '../../Beneficiary/types/beneficiary-contact-mechanism.model';
import {ReservationStateSelectors} from '../selectors/reservation.selectors';
import {Beneficiary} from '../../Beneficiary/types/beneficiary.model';
import {ServiceType} from '../../MetaDataTypes/types/service-type.model';
import {NameUuid} from '../../types/name-uuid.model';
import {IReservationFieldUpdate} from '../types/reservation-filed-update.model';
import {BeneficiarySpecialRequirement} from '../../Beneficiary/types/beneficiary-special-requirement.model';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {SpecialRequirementDateUpdate} from '../../Beneficiary/types/special-requirement-date-update.model';
import {AddressLocation} from '../../types/address-location.model';
import {ReservationService} from '../../../areas/Reservation/services/Reservation/reservation.service';
import {ReservationPassengerState} from '../types/reservation-passenger';
import {ReservationRuleResponse} from '../types/reservation-rule-response.model';
import {LatLong} from '../../types/latitude-longitude.model';
import {
    fromApi as reservationFromApi,
    Reservation
} from '../types/reservation.model';
import {BeneficiaryServiceCoverage} from '../../Beneficiary/types/beneficiary-service-coverage.model';
import {MassTransitRoute} from '../../types/mass-transit-route.model';

@Injectable()

/**
 * Implementation of ReservationActions: Redux Action Creator Service that exposes methods to mutate Reservation state
 */
export class ReservationActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static INIT_RESERVATION_UI_VIEW_STATE                        : string = 'INIT_RESERVATION_UI_VIEW_STATE';
    static CREATE_A_RESERVATION                                  : string = 'CREATE_A_RESERVATION';
    static RESERVATION_UPDATE_LEG                                : string = 'RESERVATION_UPDATE_LEG';
    static RESERVATION_UPDATE_EDIT_LEG                           : string = 'RESERVATION_UPDATE_EDIT_LEG';
    static RESERVATION_UPDATE_DELETE_LEG                         : string = 'RESERVATION_UPDATE_DELETE_LEG';
    static RESERVATION_GET_RESERVATION                           : string = 'RESERVATION_GET_RESERVATION';
    static RESERVATION_GET_EDIT_RESERVATION                      : string = 'RESERVATION_GET_EDIT_RESERVATION';
    static RESERVATION_UPDATE_REQUESTED_BY                       : string = 'RESERVATION_UPDATE_REQUESTED_BY';
    static RESERVATION_UPDATE_TREATMENT_TYPE                     : string = 'RESERVATION_UPDATE_TREATMENT_TYPE';
    static RESERVATION_UPDATE_APPOINTMENT_DATE                   : string = 'RESERVATION_UPDATE_APPOINTMENT_DATE';
    static RESERVATION_UPDATE_APPOINTMENT_TIME                   : string = 'RESERVATION_UPDATE_APPOINTMENT_TIME';
    static RESERVATION_UPDATE_APPOINTMENT_TIME_VALID             : string = 'RESERVATION_UPDATE_APPOINTMENT_TIME_VALID';
    static RESERVATION_UPDATE_PICKUP_TIME                        : string = 'RESERVATION_UPDATE_PICKUP_TIME';
    static RESERVATION_UPDATE_PICKUP_TIME_VALID                  : string = 'RESERVATION_UPDATE_PICKUP_TIME_VALID';
    static RESERVATION_UPDATE_PICKUP_LOCATION                    : string = 'RESERVATION_UPDATE_PICKUP_LOCATION';
    static RESERVATION_UPDATE_DROP_OFF_LOCATION                  : string = 'RESERVATION_UPDATE_DROP_OFF_LOCATION';
    static RESERVATION_UPDATE_PASSENGER                          : string = 'RESERVATION_UPDATE_PASSENGER';
    static RESERVATION_UPDATE_ADDITIONAL_PASSENGERS              : string = 'RESERVATION_UPDATE_ADDITIONAL_PASSENGERS';
    static RESERVATION_REMOVE_PASSENGER                          : string = 'RESERVATION_REMOVE_PASSENGER';
    static RESERVATION_UPDATE_MODE_OF_TRANSPORTATION             : string = 'RESERVATION_UPDATE_MODE_OF_TRANSPORTATION';
    static RESERVATION_UPDATE_MODE_OF_TRANSPORTATION_DRIVER      : string = 'RESERVATION_UPDATE_MODE_OF_TRANSPORTATION_DRIVER';
    static RESERVATION_CLEAR_PREFERRED_TRANSPORTATION_PROVIDER   : string = 'RESERVATION_CLEAR_PREFERRED_TRANSPORTATION_PROVIDER';
    static RESERVATION_UPDATE_PREFERRED_TRANSPORTATION_PROVIDER  : string = 'RESERVATION_UPDATE_PREFERRED_TRANSPORTATION_PROVIDER';
    static RESERVATION_UPDATE_CONNECTION_REQUEST_ISSUED_BY       : string = 'RESERVATION_UPDATE_CONNECTION_REQUEST_ISSUED_BY';
    static readonly RESERVATION_UPDATE_RESERVATION               : string = '[Reservation] Update Reservation';
    static readonly RESERVATION_SET_LEG_MASS_TRANSIT_ROUTES      : string = '[Reservation] Set Leg Mass Transit Routes';

    /* Event Reasons */
    static readonly RESERVATION_SET_EVENT_REASONS                : string = '[Reservation] Set Event Reasons';
    static readonly RESERVATION_SET_LEG_EVENT_REASON             : string = '[Reservation] Set Leg Event Reason';
    static readonly RESERVATION_SET_RESERVATION_EVENT_REASON     : string = '[Reservation] Set Reservation Event Reason';

    /* Special Requirements Form */
    static RESERVATION_SAVE_SPECIAL_REQUIREMENTS                    : string = 'RESERVATION_SAVE_SPECIAL_REQUIREMENTS';
    static RESERVATION_UPDATE_SPECIAL_REQUIREMENTS_EDITING          : string = 'RESERVATION_UPDATE_SPECIAL_REQUIREMENTS_EDITING';
    static RESERVATION_UPDATE_SPECIAL_REQUIREMENTS_EXPANDED         : string = 'RESERVATION_UPDATE_SPECIAL_REQUIREMENTS_EXPANDED';
    static RESERVATION_UPDATE_SPECIAL_REQUIREMENT_DROPDOWN_TYPES    : string = 'RESERVATION_UPDATE_SPECIAL_REQUIREMENT_DROPDOWN_TYPES';
    static RESERVATION_ADD_SPECIAL_REQUIREMENT                      : string = 'RESERVATION_ADD_SPECIAL_REQUIREMENT';
    static RESERVATION_REMOVE_SPECIAL_REQUIREMENT                   : string = 'RESERVATION_REMOVE_SPECIAL_REQUIREMENT';
    static RESERVATION_UPDATE_SPECIAL_REQUIREMENT_FROM_DATE         : string = 'RESERVATION_UPDATE_SPECIAL_REQUIREMENT_FROM_DATE';
    static RESERVATION_UPDATE_SPECIAL_REQUIREMENT_THRU_DATE         : string = 'RESERVATION_UPDATE_SPECIAL_REQUIREMENT_THRU_DATE';
    static RESERVATION_UPDATE_SPECIAL_REQUIREMENT_TYPES             : string = 'RESERVATION_UPDATE_SPECIAL_REQUIREMENT_TYPES';
    static INIT_RESERVATION_SPECIAL_REQUIREMENT_TEMP                 : string = 'INIT_RESERVATION_SPECIAL_REQUIREMENT_TEMP';
    static RESERVATION_ADD_TEMP_SPECIAL_REQUIREMENT                  : string = 'RESERVATION_ADD_TEMP_SPECIAL_REQUIREMENT';
    static RESERVATION_REMOVE_TEMP_SPECIAL_REQUIREMENT               : string = 'RESERVATION_REMOVE_TEMP_SPECIAL_REQUIREMENT';
    static RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_FROM_DATE     : string = 'RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_FROM_DATE';
    static RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_THRU_DATE     : string = 'RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_THRU_DATE';
    static RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_SET_PERMANENT : string = 'RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_SET_PERMANENT';
    static RESERVATION_SAVE_TEMP_SPECIAL_REQUIREMENT                 : string = 'RESERVATION_SAVE_TEMP_SPECIAL_REQUIREMENT';

    /* reservation legs */
    static RESERVATION_UPDATE_NUMBER_OF_LEGS                         : string = 'RESERVATION_UPDATE_NUMBER_OF_LEGS';
    static RESERVATION_ADD_ADDITIONAL_LEG                            : string = 'RESERVATION_ADD_ADDITIONAL_LEG';
    static RESERVATION_SAVE_LEG                                      : string = 'RESERVATION_SAVE_LEG';
    static RESERVATION_CANCEL_SAVE_LEG                               : string = 'RESERVATION_CANCEL_SAVE_LEG';
    static RESERVATION_CANCEL_EDIT_LEG                               : string = 'RESERVATION_CANCEL_EDIT_LEG';
    static RESERVATION_UPDATE_ROUND_TRIP_INIDICATOR                  : string = 'RESERVATION_UPDATE_ROUND_TRIP_INIDICATOR';
    static RESERVATION_UPDATE_WILL_CALL_INIDICATOR                   : string = 'RESERVATION_UPDATE_WILL_CALL_INIDICATOR';
    static RESERVATION_UPDATE_LEG_MILEAGE                            : string = 'RESERVATION_UPDATE_LEG_MILEAGE';

    /* reservation passengers */
    static INIT_RESERVATION_PASSENGER_SPECIAL_REQUIREMENT_TEMP                 : string = 'INIT_RESERVATION_PASSENGER_SPECIAL_REQUIREMENT_TEMP';
    static RESERVATION_ADD_PASSENGER_TEMP_SPECIAL_REQUIREMENT                  : string = 'RESERVATION_ADD_PASSENGER_TEMP_SPECIAL_REQUIREMENT';
    static RESERVATION_REMOVE_PASSENGER_TEMP_SPECIAL_REQUIREMENT               : string = 'RESERVATION_REMOVE_PASSENGER_TEMP_SPECIAL_REQUIREMENT';
    static RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_FROM_DATE     : string = 'RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_FROM_DATE';
    static RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_THRU_DATE     : string = 'RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_THRU_DATE';
    static RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_SET_PERMANENT : string = 'RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_SET_PERMANENT';
    static RESERVATION_SAVE_PASSENGER_TEMP_SPECIAL_REQUIREMENT                 : string = 'RESERVATION_SAVE_PASSENGER_TEMP_SPECIAL_REQUIREMENT';

    /* reservation passengers */
    static RESERVATION_UPDATE_RESERVATIONS_LIST                     : string = 'RESERVATION_UPDATE_RESERVATIONS_LIST';

    /* reservation rules messages */
    static RESERVATION_UPDATE_TREATMENT_TYPE_MESSAGE                : string = 'RESERVATION_UPDATE_TREATMENT_TYPE_MESSAGE';
    static RESERVATION_UPDATE_LOCATION_MESSAGE                      : string = 'RESERVATION_UPDATE_LOCATION_MESSAGE';
    static RESERVATION_UPDATE_ESCORT_MESSAGE                        : string = 'RESERVATION_UPDATE_ESCORT_MESSAGE';
    static RESERVATION_UPDATE_MOT_MESSAGE                           : string = 'RESERVATION_UPDATE_MOT_MESSAGE';
    static RESERVATION_UPDATE_TREATMENT_TYPE_OVERRIDE_REASON        : string = 'RESERVATION_UPDATE_TREATMENT_TYPE_OVERRIDE_REASON';
    static RESERVATION_UPDATE_LOCATION_OVERRIDE_REASON              : string = 'RESERVATION_UPDATE_LOCATION_OVERRIDE_REASON';
    static RESERVATION_UPDATE_ESCORT_OVERRIDE_REASON                : string = 'RESERVATION_UPDATE_ESCORT_OVERRIDE_REASON';
    static RESERVATION_UPDATE_MOT_OVERRIDE_REASON                   : string = 'RESERVATION_UPDATE_MOT_OVERRIDE_REASON';

    /* top 5 */
    static RESERVATION_GET_PICKUP_LOCATIONS                         : string = 'RESERVATION_GET_PICKUP_LOCATIONS';
    static RESERVATION_GET_DROPOFF_LOCATIONS                        : string = 'RESERVATION_GET_DROPOFF_LOCATIONS';

    /* exceptions */
    static RESERVATION_SAVE_DENIED_RESERVATION                      : string = 'RESERVATION_SAVE_DENIED_RESERVATION';

    /**
     * BeneficiaryInfoActions constructor
     * @param {NgRedux<IAppStore>} store
     * @param {NavActions} navActions
     * @param {ReservationService} reservationService
     * @param {MetaDataTypesSelectors} metaTypesSelectors
     * @param {BeneficiaryStateSelectors} beneficiaryStateSelectors
     * @param {ReservationStateSelectors} reservationStateSelectors
     * @param {BeneficiaryMedicalConditionsStateSelectors} beneficiaryMedicalConditionsStateSelectors
     */
    constructor(
        private store                                      : NgRedux<IAppStore>,
        private navActions                                 : NavActions,
        private reservationService                         : ReservationService,
        private metaTypesSelectors                         : MetaDataTypesSelectors,
        private beneficiaryStateSelectors                  : BeneficiaryStateSelectors,
        private reservationStateSelectors                  : ReservationStateSelectors,
        private beneficiaryMedicalConditionsStateSelectors : BeneficiaryMedicalConditionsStateSelectors
    ) {}

    /**
     * displays an error message
     * @param {any} error
     */
    private displayError(error : any) {
        this.navActions.updateAlertMessageState(
            new AlertItem({alertType: EnumAlertType.ERROR, message: error, durable: true})
        );
    }

    /**
     * displays a success message
     * @param {string} message
     */
    private displaySuccess(message : string) {
        this.navActions.updateAlertMessageState(
            new AlertItem({alertType: EnumAlertType.SUCCESS, message, durable: false})
        );
    }

    /**
     * dispatch action to populate reservation connections state in the redux store
     */
    initReservationState() {
        let beneficiary          : Beneficiary       = new Beneficiary(),
            modeOfTransportation : List<ServiceType> = List<ServiceType>(),
            treatmentType        : List<ServiceType> = List<ServiceType>(),
            uuid                 : string;

        this.beneficiaryStateSelectors.beneficiary().subscribe(value => beneficiary = value).unsubscribe();
        this.metaTypesSelectors.modeOfTransportation().subscribe(value => modeOfTransportation = value).unsubscribe();
        this.metaTypesSelectors.treatmentType().subscribe(value => treatmentType = value).unsubscribe();
        this.beneficiaryStateSelectors.beneficiaryUuid().subscribe(value => uuid = value).unsubscribe();

        this.store.dispatch({
            type    : ReservationActions.INIT_RESERVATION_UI_VIEW_STATE,
            payload : {
                beneficiary,
                modeOfTransportation,
                treatmentType
            }
        });

        // getReservations
        this.getReservations(uuid);

        // getReservationDropOffLocations
        this.getReservationDropOffLocations(uuid);

        // getReservationPickUpLocations
        this.getReservationPickUpLocations(uuid);
    }

    /**
     * @description dispatch action to get the pickup time for a reservation
     * @param uuid - reservation uuid
     */
    getReservation(uuid : string) {
        this.reservationService.getReservation(uuid).subscribe(response => {
            // update reservation
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_GET_RESERVATION,
                payload : response
            });

        }, error => this.displayError(error));
    }

    /**
     * dispatch action to get a reservation
     * @param uuid - beneficiary uuid
     */
    getReservations(uuid : string) {
        // getReservations
        this.reservationService.getReservations(uuid).subscribe(response => {
            // update reservations
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_UPDATE_RESERVATIONS_LIST,
                payload : response.map(reservation => reservationFromApi(reservation))
            });

        }, error => this.displayError(error));
    }

    /**
     * dispatch action to save denied reservation in redux store
     */
    saveDeniedReservation(reservation : Reservation) {
        // post for create reservation
        this.reservationService
            .createReservation(reservation)
            .first()
            .subscribe(response => {
                this.navActions.updateAlertMessageState(
                    new AlertItem({alertType: EnumAlertType.WARNING, message: 'RESERVATION_HAS_BEEN_DENIED', durable: false})
                );

                // retrieve new list of reservations for the beneficiary
                this.getReservations(reservation.get('personUuid'));
            }, error => this.displayError(error));
    }

    /**
     * dispatch action to create a reservation
     * @param {Reservation} reservation
     */
    createReservation(reservation : Reservation) {
        // post for create reservation
        this.reservationService
            .createReservation(reservation)
            .first()
            .subscribe(response => {
                // update reservation confirmationNumber
                this.store.dispatch({
                    type    : ReservationActions.CREATE_A_RESERVATION,
                    payload : response
                });

                this.displaySuccess('RESERVATION_HAS_BEEN_CREATED');

                // retrieve new list of reservations for the beneficiary
                this.getReservations(reservation.get('personUuid'));
            }, error => this.displayError(error));
    }

    /**
     * dispatch action to create a reservation
     * @param {Reservation} reservation
     */
    editReservation(reservation : Reservation) {
        // put for edit a reservation
        this.reservationService
            .editReservation(reservation)
            .first()
            .subscribe(() => {
                // retrieve new list of reservations for the beneficiary
                this.getReservations(reservation.get('personUuid'));

                this.displaySuccess('RESERVATION_HAS_BEEN_EDITED');
            }, error => this.displayError(error));
    }

    /**
     * dispatch action to edit a reservation
     * @param uuid reservation uuid
     */
    getEditReservation(uuid : string) {
        this.reservationService.getReservation(uuid).subscribe(response => {
            // update reservation
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_GET_EDIT_RESERVATION,
                payload : response
            });

        }, error => this.displayError(error));
    }

    /**
     * dispatch action to edit a leg of a reservation
     */
    updateLeg() {
        this.store.dispatch({ type : ReservationActions.RESERVATION_UPDATE_LEG });
    }

    /**
     * dispatch action to edit a reservation
     * @param leg
     */
    updateEditLeg(leg : number) {
        // update edit leg
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_EDIT_LEG,
            payload : leg
        });
    }

    /**
     * dispatch action to delete a leg of a reservation
     * @param leg
     */
    updateDeleteLeg(leg : number) {
        // update edit leg
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_DELETE_LEG,
            payload : leg
        });
    }

    /**
     * dispatch action to populate temp special requirement state in the redux store
     */
    initTempSpecialRequirementState() {
        this.store.dispatch({
            type    : ReservationActions.INIT_RESERVATION_SPECIAL_REQUIREMENT_TEMP
        });
    }

    /**
     * dispatch action to populate passenger temp special requirement state in the redux store
     */
    initTempPassengerSpecialRequirementState(passenger : KeyValuePair) {
        this.store.dispatch({
            type    : ReservationActions.INIT_RESERVATION_PASSENGER_SPECIAL_REQUIREMENT_TEMP,
            payload : passenger
        });
    }

    /**
     * dispatch action to update reservation Requested by in redux store
     */
    updateRequestedBy(update : NameUuid) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_REQUESTED_BY,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation treatment type in redux store
     */
    updateTreatmentType(update : NameUuid) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_TREATMENT_TYPE,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation appointment date in redux store
     */
    updateAppointmentDate(update : IReservationFieldUpdate) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_APPOINTMENT_DATE,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation appointment time in redux store
     */
    updateAppointmentTime(update : IReservationFieldUpdate) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_APPOINTMENT_TIME,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation appointment time valid state in redux store
     */
    updateAppointmentTimeValid(update : IReservationFieldUpdate) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_APPOINTMENT_TIME_VALID,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation pickup time in redux store
     */
    updatePickupTime(update : IReservationFieldUpdate) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_PICKUP_TIME,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation pickup time valid state in redux store
     */
    updatePickupTimeValid(update : IReservationFieldUpdate) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_PICKUP_TIME_VALID,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation pickup location in redux store
     */
    updatePickupLocation(payload : AddressLocation) {
        this.store.dispatch({
            type : ReservationActions.RESERVATION_UPDATE_PICKUP_LOCATION,
            payload
        });
    }

    /**
     * dispatch action to update reservation drop off location in redux store
     */
    updateDropOffLocation(payload : AddressLocation) {
        this.store.dispatch({
            type : ReservationActions.RESERVATION_UPDATE_DROP_OFF_LOCATION,
            payload
        });
    }

    /**
     * dispatch action to update reservation additional passengers in redux store
     */
    updateAdditionalPassengers(update : IReservationFieldUpdate) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_ADDITIONAL_PASSENGERS,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation additional passengers in redux store
     */
    updatePassenger(update : ReservationPassengerState) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_PASSENGER,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation mode of transportation in redux store
     */
    updateModeOfTransportation(update : NameUuid) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_MODE_OF_TRANSPORTATION,
            payload : update
        });

        if (update && update.get('name') === 'Mass Transit') {
            this.store.select(state => state.reservationState)
                .first()
                .subscribe(state => {
                    const appointmentTime = state.getIn(['reservationAppointmentDateState', 'appointmentTime']);
                    const dropOffLocation = state.getIn(['reservationDropOffLocationState', 'latLong']);
                    const pickupLocation = state.getIn(['reservationPickupLocationState', 'latLong']);

                    this.reservationService
                        .getMassTransitRoutes(appointmentTime, dropOffLocation, pickupLocation)
                        .subscribe(routes => this.setMassTransitRoutes(routes), error => this.displayError(error));
                });
        }
    }

    /**
     * dispatch action to update reservation transportation driver in redux store
     */
    updateModeOfTransportationDriver(update : NameUuid) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_MODE_OF_TRANSPORTATION_DRIVER,
            payload : update
        });
    }

    /**
     * dispatch action to update reservation preferred transportation provider in redux store
     */
    updatePreferredTransportationProvider(update : IReservationFieldUpdate) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_PREFERRED_TRANSPORTATION_PROVIDER,
            payload : update
        });
    }

    /**
     * dispatch action to clear reservation preferred transportation provider in redux store
     */
    clearTransportationProvider() {
        this.store.dispatch({ type : ReservationActions.RESERVATION_CLEAR_PREFERRED_TRANSPORTATION_PROVIDER });
    }

    /**
     * save the beneficiaries special requirements
     */
    saveSpecialRequirements() {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_SAVE_TEMP_SPECIAL_REQUIREMENT
        });
    }

    savePassengerSpecialRequirement(passenger : ReservationPassengerState) {
        this.store.dispatch({
            type : ReservationActions.RESERVATION_SAVE_PASSENGER_TEMP_SPECIAL_REQUIREMENT,
            payload : passenger
        });
    }

    /**
     * adds checked special requirement to beneficiary
     */
    addSpecialRequirement(requirement : BeneficiarySpecialRequirement) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_ADD_SPECIAL_REQUIREMENT,
            payload : requirement
        });
    }

    /**
     * adds checked special requirement to temp state
     */
    addTempSpecialRequirement(requirement : BeneficiarySpecialRequirement) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_ADD_TEMP_SPECIAL_REQUIREMENT,
            payload : requirement
        });
    }

    /**
     * adds checked special requirement to temp state
     */
    addPassengerTempSpecialRequirement(requirement : BeneficiarySpecialRequirement, passenger : ReservationPassengerState) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_ADD_PASSENGER_TEMP_SPECIAL_REQUIREMENT,
            payload : {
                passenger,
                requirement
            }
        });
    }

    removePassenger(passenger : ReservationPassengerState) {
        this.store.dispatch({
            type : ReservationActions.RESERVATION_REMOVE_PASSENGER,
            payload : passenger
        });
    }

    /**
     * remove unchecked special requirement to beneficiary
     */
    removeSpecialRequirement(requirement : BeneficiarySpecialRequirement) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_REMOVE_SPECIAL_REQUIREMENT,
            payload : requirement
        });
    }

    /**
     * remove unchecked special requirement to temp state
     */
    removeTempSpecialRequirement(requirement : KeyValuePair) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_REMOVE_TEMP_SPECIAL_REQUIREMENT,
            payload : requirement
        });
    }

    /**
     * remove unchecked special requirement to temp state
     */
    removeTempPassengerSpecialRequirement(requirement : KeyValuePair, passenger : ReservationPassengerState) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_REMOVE_PASSENGER_TEMP_SPECIAL_REQUIREMENT,
            payload : {
                passenger,
                requirement
            }
        });
    }

    /**
     * update selected special requirement thru date value
     * @param newDate
     * @param requirement
     */
    updateSpecialRequirementThruDate(newDate : string, requirement : BeneficiarySpecialRequirement) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_SPECIAL_REQUIREMENT_THRU_DATE,
            payload : new SpecialRequirementDateUpdate({
                newDate,
                requirement
            })
        });
    }

    /**
     * update selected temp special requirement thru date value
     * @param date
     * @param requirement
     */
    updateTempSpecialRequirementThruDate(date : string, requirement : KeyValuePair) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_THRU_DATE,
            payload : {
                date,
                requirement
            }
        });
    }

    /**
     * update selected temp special requirement thru date value
     * @param date
     * @param requirement
     * @param passenger
     */
    updateTempPassengerSpecialRequirementThruDate(date : string, requirement : KeyValuePair, passenger : ReservationPassengerState) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_THRU_DATE,
            payload : {
                date,
                passenger,
                requirement
            }
        });
    }

    /**
     * set permanence of a temporary special requirement
     * @param {boolean} isPermanent
     * @param {KeyValuePair} requirement
     */
    setTempSpecialRequirementPermanence(isPermanent : boolean, requirement : KeyValuePair) {
        this.store.dispatch({
            type : ReservationActions.RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_SET_PERMANENT,
            payload : {
                isPermanent,
                requirement
            }
        });
    }

    /**
     * set permanence of a passenger's temporary special requirement
     * @param {boolean} isPermanent
     * @param {KeyValuePair} requirement
     * @param {ReservationPassengerState} passenger
     */
    setTempPassengerSpecialRequirementPermanence(isPermanent : boolean, requirement : KeyValuePair, passenger : ReservationPassengerState) {
        this.store.dispatch({
            type : ReservationActions.RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_SET_PERMANENT,
            payload : {
                isPermanent,
                passenger,
                requirement
            }
        });
    }

    /**
     * update selected special requirement from date value
     * @param newDate
     * @param requirement
     */
    updateSpecialRequirementFromDate(newDate : string, requirement : BeneficiarySpecialRequirement) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_SPECIAL_REQUIREMENT_FROM_DATE,
            payload : new SpecialRequirementDateUpdate({
                newDate,
                requirement
            })
        });
    }

    /**
     * update selected special requirement from date value
     * @param date
     * @param requirement
     */
    updateTempSpecialRequirementFromDate(date : string, requirement : KeyValuePair) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_TEMP_SPECIAL_REQUIREMENT_FROM_DATE,
            payload : {
                date,
                requirement
            }
        });
    }

    /**
     * update selected special requirement from date value
     * @param date
     * @param requirement
     * @param passenger
     */
    updateTempPassengerSpecialRequirementFromDate(date : string, requirement : KeyValuePair, passenger : ReservationPassengerState) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_PASSENGER_TEMP_SPECIAL_REQUIREMENT_FROM_DATE,
            payload : {
                date,
                passenger,
                requirement
            }
        });
    }

    /**
     * dispatch action to update user beneficiary special requirements expanded flag on redux store
     */
    updateSpecialRequirementsInfoExpanded() {
        this.store.dispatch({ type : ReservationActions.RESERVATION_UPDATE_SPECIAL_REQUIREMENTS_EXPANDED });
    }

    /**
     * dispatch action to update user beneficiary special requirements editing flag on redux store
     * @param isEditing current edited state of beneficiary special requirements form
     */
    updateSpecialRequirementsInfoEditing(isEditing : boolean) {
        this.navActions.toggleIsPoppedNavState(isEditing);

        this.store.dispatch({
            type   : ReservationActions.RESERVATION_UPDATE_SPECIAL_REQUIREMENTS_EDITING,
            payload: isEditing
        });
    }

    /**
     * dispatch action to update reservation connection request issued by in redux store
     */
    connectionRequestIssuedBy(value : string) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_CONNECTION_REQUEST_ISSUED_BY,
            payload : value

        });
    }

    /**
     * dispatch action to update number of reservation legs in redux store
     */
    updateNumberOfLegs(value : number) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_NUMBER_OF_LEGS,
            payload : value
        });
    }

    /**
     * dispatch action to add additional reservation leg in redux store
     */
    addAdditionalLeg() {
        this.store.dispatch({ type : ReservationActions.RESERVATION_ADD_ADDITIONAL_LEG });
    }

    /**
     * dispatch action to save reservation leg in redux store
     */
    saveLeg() {
        this.store.dispatch({ type : ReservationActions.RESERVATION_SAVE_LEG });
    }

    /**
     * dispatch action to cancel save reservation leg in redux store
     */
    resetForm() {
        this.initReservationState();
    }

    /**
     * dispatch action to cancel save reservation leg in redux store
     */
    cancelSaveLeg() {
        this.initReservationState();
        // this.store.dispatch({ type : ReservationActions.RESERVATION_CANCEL_SAVE_LEG });
    }

    /**
     * dispatch action to cancel save reservation leg in redux store
     */
    cancelEditLeg() {
        this.store.dispatch({ type : ReservationActions.RESERVATION_CANCEL_EDIT_LEG });
    }

    /**
     * dispatch action to update reservation trip indicator flag on redux store
     * @param choice
     */
    updateRoundTrip(choice : boolean) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_ROUND_TRIP_INIDICATOR,
            payload : choice
        });
    }

    /**
     * dispatch action to update reservation will call indicator flag on redux store
     * @param choice
     */
    updateWillCall(choice : boolean) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_WILL_CALL_INIDICATOR,
            payload : choice
        });
    }

    /**
     * dispatch action to update reservation treatment type override reason in redux store
     * @param choice
     * @param index
     */
    updateTreatmentTypeOverrideReason(choice : NameUuid, index : number) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_TREATMENT_TYPE_OVERRIDE_REASON,
            payload : {
                choice,
                index
            }
        });
    }

    /**
     * dispatch action to update reservation location override reason in redux store
     * @param choice
     * @param index
     */
    updateLocationOverrideReason(choice : NameUuid, index : number) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_LOCATION_OVERRIDE_REASON,
            payload : {
                choice,
                index
            }
        });
    }

    /**
     * dispatch action to update reservation treatment type override reason in redux store
     * @param choice
     * @param index
     */
    updateEscortOverrideReason(choice : NameUuid, index : number) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_ESCORT_OVERRIDE_REASON,
            payload : {
                choice,
                index
            }
        });
    }

    /**
     * dispatch action to update reservation location override reason in redux store
     * @param choice
     * @param index
     */
    updateModeOfTransportationOverrideReason(choice : NameUuid, index : number) {
        this.store.dispatch({
            type    : ReservationActions.RESERVATION_UPDATE_MOT_OVERRIDE_REASON,
            payload : {
                choice,
                index
            }
        });
    }

    /**
     * dispatch action to get reservation leg milage
     * @param pickup
     * @param dropoff
     * @param dateTime
     */
    getReservationLegMileage(pickup : LatLong, dropoff : LatLong, dateTime : string) {
        const stringTime = moment.utc(dateTime).format(),
              query : string = '?pickUpLocationLat=' + pickup.get('lat') +
            '&pickUpLocationLong=' + pickup.get('lng') +
            '&dropOffLocationLat=' + dropoff.get('lat') +
            '&dropOffLocationLong=' + dropoff.get('lng') +
            '&appointmentTime=' + stringTime;

        // query for reservation leg mileage
        this.reservationService.getReservationLegMileage(query).subscribe(response => {
            // update reservation leg mileage
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_UPDATE_LEG_MILEAGE,
                payload : {
                    distance : response.distanceValue,
                    duration : response.durationValue
                }
            });

        }, error => this.displayError(error));
    }

    /**
     * @description dispatch action to get the pickup time for a reservation
     */
    getReservationPickupTime() {
        let durationValue            : number,
            appointmentTime          : string,
            modeOfTransportationUuid : string;

        const specialRequirementTypeUuids : Array<string> = [];

        // set the distance
        this.reservationStateSelectors.reservationDropOffLocationState()
                .subscribe(value => durationValue = value.get('durationMinutes', 0)).unsubscribe();

        // set the date and time
        this.reservationStateSelectors.reservationAppointmentDateState()
                .subscribe(value => appointmentTime = value.get('appointmentTime', '')).unsubscribe();

        // set the mod of transportation
        this.reservationStateSelectors.reservationModeOfTransportationState()
                .subscribe(value => modeOfTransportationUuid = value.getIn(['modeOfTransportation', 'uuid'], '')).unsubscribe();

        // map the special requirements for the reservation
        this.reservationStateSelectors.reservationSpecialRequirementState().subscribe(specialRequirements => {
            // push the special requirements for the beneficiary
            specialRequirements.forEach(requirement => {
                specialRequirementTypeUuids.push(requirement.getIn(['type', 'id']));
            });
        }).unsubscribe();

        // map the passengers of the reservation
        this.reservationStateSelectors.reservationAdditionalPassengersState().subscribe(additionalPassenger => {
            // find all the passengers
            additionalPassenger.get('passengers').forEach(passenger => {
                // push the special requirements for this passenger
                passenger.get('specialRequirements').forEach(requirement => {
                    specialRequirementTypeUuids.push(requirement.getIn(['type', 'id']));
                });
            });
        }).unsubscribe();

        // send to service
        this.reservationService.getReservationPickupTime({
                durationValue,
                appointmentTime,
                modeOfTransportationUuid,
                specialRequirementTypeUuids
            }).subscribe(response => {

            // update reservation pickuptime
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_UPDATE_PICKUP_TIME,
                payload : {
                    fieldName  : 'pickupTime',
                    fieldValue :  moment(response).format('YYYY-MM-DDTHH:mm:ssZ')
                }
            });

            // update reservation pickuptime isFormValid
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_UPDATE_PICKUP_TIME_VALID,
                payload : {
                    fieldName  : 'isFormValid',
                    fieldValue : true
                }
            });

        }, error => this.displayError(error));
    }

    /**
     * dispatch action to validate treatment types for a reservation
     */
    validateTreatmentType() {
        let addressMechanisms                    : List<BeneficiaryContactMechanism>  = List<BeneficiaryContactMechanism>(),
            birthDateString                      : string        = '',
            serviceCoverages                     : List<BeneficiaryServiceCoverage> = List<BeneficiaryServiceCoverage>(),
            callerWorkTitle                      : string        = '', // connection type uuid of requested by
            beneficiaryId                        : string        = '',
            serviceDateString                    : string        = '',
            treatmentType                        : string        = '',
            state                                : string        = '';

        const plansJoined                        : Array<string> = [],
            hasMedicalNecessityForm4Treatment    : boolean       = false,
            conditions                           : Array<string> = [],
            tempAttributes4Extensibility         : Array<string> = [],
            treatmentsPreviousLegs               : Array<string> = [];

        this.beneficiaryStateSelectors.beneficiary().subscribe(value => {
            birthDateString   = value.get('birthDate');
            beneficiaryId     = value.get('uuid');
            serviceCoverages  = value.get('serviceCoverages');
            addressMechanisms = value.get('contactMechanisms').filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'value']) === 'Address').sortBy(address =>  address.get('ordinality'));

            // we are hardcoding RI for now
            state             = addressMechanisms.get(0).getIn(['contactMechanism', 'state']) !== '' ? addressMechanisms.get(0).getIn(['contactMechanism', 'state']) : 'RI'; // remove RI at some point
        }).unsubscribe();

        this.reservationStateSelectors.reservationRequestedByState()
                .subscribe(value => callerWorkTitle = value.getIn(['requestedByInfo', 'uuid'])).unsubscribe();

        serviceCoverages.forEach(coverage => plansJoined.push(coverage.getIn(['serviceCoveragePlan', 'uuid'])));

        this.reservationStateSelectors.reservationAppointmentDateState()
                .subscribe(value => serviceDateString = value.get('appointmentTime')).unsubscribe();

        this.reservationStateSelectors.reservationTreatmentTypeState()
                .subscribe(value => treatmentType = value.getIn(['treatmentType', 'uuid'])).unsubscribe();

        // send this to the service
        this.reservationService.validateTreatmentType({
            birthDateString,
            callerWorkTitle,
            plansJoined,
            beneficiaryId,
            serviceDateString,
            hasMedicalNecessityForm4Treatment,
            treatmentType,
            state,
            conditions,
            tempAttributes4Extensibility,
            treatmentsPreviousLegs
        }).subscribe(response => {
            // update reservation treatmentType message
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_UPDATE_TREATMENT_TYPE_MESSAGE,
                payload : response
            });

        }, error => this.displayError(error));
    }

    /**
     * dispatch action to  validate Escort for a reservation
     */
    validateEscort() {
        let birthDateString          : string = '',
            addressMechanisms        : List<BeneficiaryContactMechanism>  = List<BeneficiaryContactMechanism>(),
            treatmentRuleRes         : List<ReservationRuleResponse>      = List<ReservationRuleResponse>(),
            state                    : string = '';

        const hasMedicalNecessityForm4Escorts4Treatment : boolean = false,
              additionalPassengerUuids    : Array<string> = [],
              planApproved4Treatment      : Array<any>    = [];

        this.beneficiaryStateSelectors.beneficiary().subscribe(value => {
            birthDateString   = value.get('birthDate');
            addressMechanisms = value.get('contactMechanisms').filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'value']) === 'Address').sortBy(address =>  address.get('ordinality'));

            // we are hardcoding RI for now
            state             = addressMechanisms.get(0).getIn(['contactMechanism', 'state']) !== '' ? addressMechanisms.get(0).getIn(['contactMechanism', 'state']) : 'RI'; // remove RI at some point
        }).unsubscribe();

        this.reservationStateSelectors.reservationBusinessRules()
                .subscribe(value => treatmentRuleRes = value.get('treatmentType')).unsubscribe();

        treatmentRuleRes.forEach(response => {
            planApproved4Treatment.push({
                name : response.getIn(['serviceCoveragePlan', 'name']),
                uuid : response.getIn(['serviceCoveragePlan', 'uuid'])
            });
        });

        // map the passengers of the reservation
        this.reservationStateSelectors.reservationAdditionalPassengersState().subscribe(additionalPassenger => {
            // find all the passengers
            additionalPassenger.get('passengers').forEach(passenger => {
                // push the uuid for this passenger
                additionalPassengerUuids.push(passenger.get('uuid'));
            });
        }).unsubscribe();

        const payload = {
            birthDateString,
            hasMedicalNecessityForm4Escorts4Treatment,
            serviceCoveragePlan : planApproved4Treatment[0],
            additionalPassengerUuids,
            state
        };

        this.reservationService.validateEscort(payload).subscribe(response => {
            // update reservation validateEscort message
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_UPDATE_ESCORT_MESSAGE,
                payload : response
            });

        }, error => this.displayError(error));
    }

    /**
     * dispatch action to validate Mode Of
     * Transportation rule for a leg of reservation
     */
    validateModeOfTransportation() {
        let addressMechanisms                    : List<BeneficiaryContactMechanism>  = List<BeneficiaryContactMechanism>(),
            treatmentRuleRes                     : List<ReservationRuleResponse>      = List<ReservationRuleResponse>(),
            birthDate                            : string        = '',
            serviceDate                          : string        = '',
            state                                : string        = '',
            pickupAddressState                   : string        = '',
            treatmentType                        : any           = null,
            modeOfTransportation                 : NameUuid      = new NameUuid(),
            pickupLocation                       : AddressLocation = new AddressLocation(),
            medicalConditions                    : List<KeyValuePair> = List<KeyValuePair>();

        const specialRequirementTypes            : Array<any> = [],
              medicalConditionTypes              : Array<any> = [],
              serviceCoveragePlan                : Array<any> = [];

        // get beneficiary specific information
        this.beneficiaryStateSelectors.beneficiary().subscribe(value => {
            birthDate         = value.get('birthDate');
            addressMechanisms = value.get('contactMechanisms').filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'value']) === 'Address').sortBy(address =>  address.get('ordinality'));

            // we are hardcoding RI for now
            state             = addressMechanisms.get(0).getIn(['contactMechanism', 'state']) !== '' ? addressMechanisms.get(0).getIn(['contactMechanism', 'state']) : 'RI'; // remove RI at some point
        }).unsubscribe();

        // set service date
        this.reservationStateSelectors.reservationAppointmentDateState()
                .subscribe(value => serviceDate = value.get('appointmentTime')).unsubscribe();

        // set pickup location
        this.reservationStateSelectors.reservationPickupLocationState()
                .subscribe(value => pickupLocation = value).unsubscribe();

        // set pickup location state
        pickupAddressState = pickupLocation.get('state');

        // map the treatmentType business rule response
        this.reservationStateSelectors.reservationBusinessRules()
                .subscribe(value => treatmentRuleRes = value.get('treatmentType')).unsubscribe();

        // go thru the treatment rules and find
        // the first uuid and use it as the plan for
        // now until the service can do this on its own
        treatmentRuleRes.forEach(response => {
            serviceCoveragePlan.push(response.getIn(['serviceCoveragePlan', 'uuid']));
        });

        // map the special requirements for the reservation
        this.reservationStateSelectors.reservationSpecialRequirementState().subscribe(specialRequirements => {
            // push the special requirements for the beneficiary
            specialRequirements.forEach(requirement => {
                specialRequirementTypes.push({
                    name : requirement.getIn(['type', 'value']),
                    uuid : requirement.getIn(['type', 'id'])
                });
            });
        }).unsubscribe();

        // map the passengers of the reservation
        this.reservationStateSelectors.reservationAdditionalPassengersState().subscribe(additionalPassenger => {
            // find all the passengers
            additionalPassenger.get('passengers').forEach(passenger => {
                // push the special requirements for this passenger
                passenger.get('specialRequirements').forEach(requirement => {
                    specialRequirementTypes.push({
                        name : requirement.getIn(['type', 'value']),
                        uuid : requirement.getIn(['type', 'id'])
                    });
                });
            });
        }).unsubscribe();

        // map mode of transportation
        this.reservationStateSelectors.reservationModeOfTransportationState()
                .subscribe(value => modeOfTransportation = value.get('modeOfTransportation')).unsubscribe();
        // map medical conditions
        this.beneficiaryMedicalConditionsStateSelectors.beneficiaryMedicalConditionsState()
                .subscribe(value => medicalConditions = value.get('medicalConditionTypes')).unsubscribe();

        // set medical conditions
        medicalConditions.forEach(response => {
            medicalConditionTypes.push({
                name : response.get('value'),
                uuid : response.get('id')
            });
        });

        // map treatmentType
        this.reservationStateSelectors.reservationTreatmentTypeState()
                .subscribe(value => treatmentType = value.get('treatmentType')).unsubscribe();

        // create a payload
        const payload = {
            serviceDateString : serviceDate,
            birthDateString : birthDate,
            pickupAddressState,
            state,
            serviceCoveragePlan : {
                uuid : serviceCoveragePlan[0],
                name : ''
            },
            computedModeOfTransportation : {},
            selectedModeOfTransportation : {
                name : modeOfTransportation.get('name'),
                uuid : modeOfTransportation.get('uuid')
            },
            specialRequirementTypes,
            medicalConditionTypes,
            treatmentType : {
                name : treatmentType.get('name'),
                uuid : treatmentType.get('uuid')
            }
        };

        // call the service
        this.reservationService.validateModeOfTransportation(payload).subscribe(response => {
            // update reservation validateModeOfTransportation message
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_UPDATE_MOT_MESSAGE,
                payload : response
            });

        }, error => this.displayError(error));
    }

    /**
     * dispatch action to validate locations for a reservation
     */
    validateLocation() {
        let addressMechanisms  : List<BeneficiaryContactMechanism>  = List<BeneficiaryContactMechanism>(),
            treatmentRuleRes   : List<ReservationRuleResponse>      = List<ReservationRuleResponse>(),
            serviceDateString  : string          = '',
            treatmentType      : string          = '',
            state              : string          = '',
            pickupLocation     : AddressLocation = new AddressLocation(),
            dropOffLocation    : AddressLocation = new AddressLocation();

        const planApproved4Treatment      : Array<string> = [];

        this.beneficiaryStateSelectors.beneficiary().subscribe(value => {
            addressMechanisms = value.get('contactMechanisms').filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'value']) === 'Address').sortBy(address =>  address.get('ordinality'));

            // if state is blank, hardcode RI for now
            state = addressMechanisms.get(0).getIn(['contactMechanism', 'state']) !== '' ? addressMechanisms.get(0).getIn(['contactMechanism', 'state']) : 'RI'; // remove RI at some point
        }).unsubscribe();

        this.reservationStateSelectors.reservationBusinessRules()
                .subscribe(value => treatmentRuleRes = value.get('treatmentType')).unsubscribe();
        treatmentRuleRes.forEach(response => {
            planApproved4Treatment.push(response.getIn(['serviceCoveragePlan', 'uuid']));
        });

        this.reservationStateSelectors.reservationAppointmentDateState()
                .subscribe(value => serviceDateString = value.get('appointmentTime')).unsubscribe();

        this.reservationStateSelectors.reservationTreatmentTypeState()
                .subscribe(value => treatmentType = value.getIn(['treatmentType', 'uuid'])).unsubscribe();

        this.reservationStateSelectors.reservationPickupLocationState()
                .subscribe(value => pickupLocation = value).unsubscribe();

        const fromForLeg = {
            facilityType : null,
            type : null,
            street : pickupLocation.get('address1'),
            medicaidApproved : false,
            beneficiaryHome : false,
            label : null,
            neighboringState : [],
            state : pickupLocation.get('state'),
            zip : pickupLocation.get('zipCode'),
            city : pickupLocation.get('city')
        };

        this.reservationStateSelectors.reservationDropOffLocationState()
                .subscribe(value => dropOffLocation = value).unsubscribe();

        const toForLeg = {
            facilityType : null,
            type : null,
            street : dropOffLocation.get('address1'),
            medicaidApproved : false,
            beneficiaryHome : false,
            label : null,
            neighboringState : [],
            state : dropOffLocation.get('state'),
            zip : dropOffLocation.get('zipCode'),
            city : dropOffLocation.get('city')
        };

        const payload = {
            // milesForLeg is a string??? and it can not handle 1oths of a mile
            milesForLeg : dropOffLocation.get('mileageDistance'),
            planApproved4Treatment : [planApproved4Treatment[0]],
            fromForLeg,
            serviceDateString,
            toForLeg,
            state,
            treatment : treatmentType
        };

        this.reservationService.validateLocation(payload).subscribe(response => {
            // update reservation treatmentType message
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_UPDATE_LOCATION_MESSAGE,
                payload : response
            });

        }, error => this.displayError(error));
    }

    /**
     * @description dispatch action to get the top 5 most recent pickup locations
     * @param uuid - beneficiary uuid
     */
    getReservationPickUpLocations(uuid : string) {
        this.reservationService.getReservationPickUpLocations(uuid).subscribe(response => {
            // update reservation pickUpLocations
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_GET_PICKUP_LOCATIONS,
                payload : response
            });

        }, error => this.displayError(error));
    }

    /**
     * @description dispatch action to get the top 5 most recent dropoff locations
     * @param uuid - beneficiary uuid
     */
    getReservationDropOffLocations(uuid : string) {
        this.reservationService.getReservationDropOffLocations(uuid).subscribe(response => {
            // update reservation dropOffLocations
            this.store.dispatch({
                type    : ReservationActions.RESERVATION_GET_DROPOFF_LOCATIONS,
                payload : response
            });

        }, error => this.displayError(error));
    }

    /**
     * Fetches a reservation matching the UUID and updates the store
     * @param {string} uuid
     */
    fetchReservation(uuid : string) {
        this.reservationService
            .getReservation(uuid)
            .first()
            .subscribe(response => {
                this.store.dispatch({
                    type: ReservationActions.RESERVATION_UPDATE_RESERVATION,
                    payload: response
                });
            }, error => this.displayError(error));
    }

    /**
     * Fetches the reservation event reasons and updates the store
     */
    fetchReservationEventReasons() {
        this.reservationService
            .getReservationEventReasons()
            .subscribe(reasons => {
                this.store.dispatch({
                    type : ReservationActions.RESERVATION_SET_EVENT_REASONS,
                    payload : reasons
                });
            }, error => this.displayError(error));
    }

    /**
     * Set the edit reason on a leg
     * @param {number} legIndex
     * @param {NameUuid} reason
     * @param {string} comment
     */
    setLegEditReason(legIndex : number, reason : NameUuid, comment : string) {
        this.store.dispatch({
            type : ReservationActions.RESERVATION_SET_LEG_EVENT_REASON,
            payload : {
                legIndex,
                reason,
                comment
            }
        });
    }

    /**
     * Set the edit reason on the reservation
     * @param {NameUuid} reason
     * @param {string} comment
     */
    setReservationEditReason(reason : NameUuid, comment : string) {
        this.store.dispatch({
            type : ReservationActions.RESERVATION_SET_RESERVATION_EVENT_REASON,
            payload : {
                reason,
                comment
            }
        });
    }

    /**
     * Sets the mass transit routes for a leg
     * @param {List<MassTransitRoute>} routes
     */
    setMassTransitRoutes(routes : List<MassTransitRoute>) {
        this.store.dispatch({
            type : ReservationActions.RESERVATION_SET_LEG_MASS_TRANSIT_ROUTES,
            payload : routes
        });
    }

    /**
     * cancels a reservation
     * @param {Reservation} reservation
     * @param {string} eventReasonUuid
     * @param {string} comment
     */
    cancelReservation(reservation : Reservation, eventReasonUuid : string, comment : string) {
        reservation = reservation.withMutations(record => record
            .set('eventComment', comment)
            .set('eventReasonUuid', eventReasonUuid)
        ) as Reservation;

        this.reservationService
            .cancelReservation(reservation)
            .subscribe(() => {
                // get updated reservation data
                this.fetchReservation(reservation.get('uuid'));

                this.displaySuccess('RESERVATION_HAS_BEEN_CANCELLED');
            }, error => this.displayError(error));
    }
}
