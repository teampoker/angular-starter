import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../../app-store';

@Injectable()
/**
 * Redux Actions for Reservation Cards
 * @export
 * @class ReservationCardsActions
 */
export class ReservationCardsActions {
    // History Widget Actions
    static readonly RESERVATION_CARDS_HISTORY_CLOSE_ITEM : string = '[Reservation Card - History] Close Item';
    static readonly RESERVATION_CARDS_HISTORY_TOGGLE_EXPAND_LEG : string = '[Reservation Card - History] Toggle Expand Leg';
    static readonly RESERVATION_CARDS_HISTORY_SELECT_ITEM : string = '[Reservation Card - History] Select Item';

    // Scheduled Rides Actions
    static readonly RESERVATION_CARDS_SCHEDULED_CLOSE_ITEM : string = '[Reservation Card - Scheduled] Close Item';
    static readonly RESERVATION_CARDS_SCHEDULED_TOGGLE_EXPAND_LEG : string = '[Reservation Card - Scheduled] Toggle Expand Leg';
    static readonly RESERVATION_CARDS_SCHEDULED_SELECT_ITEM : string = '[Reservation Card - Scheduled] Select Item';

    /**
     * @param {NgRedux<IAppStore>} store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * Dispatches action to close a selected item in the Reservation History Widget
     */
    closeHistoryItem() {
        this.store.dispatch({ type : ReservationCardsActions.RESERVATION_CARDS_HISTORY_CLOSE_ITEM });
    }

    /**
     * Dispatches action to close a selected item in the Scheduled Rides Widget
     */
    closeScheduledItem() {
        this.store.dispatch({ type : ReservationCardsActions.RESERVATION_CARDS_SCHEDULED_CLOSE_ITEM });
    }

    /**
     * Dispatches action to select an item in the Reservation History Widget
     * @param {string} uuid
     */
    selectHistoryItem(uuid : string) {
        this.store.dispatch({
            type : ReservationCardsActions.RESERVATION_CARDS_HISTORY_SELECT_ITEM,
            payload : uuid
        });
    }

    /**
     * Dispatches action to select an item in the Scheduled Rides Widget
     * @param {string} uuid
     */
    selectScheduledItem(uuid : string) {
        this.store.dispatch({
            type : ReservationCardsActions.RESERVATION_CARDS_SCHEDULED_SELECT_ITEM,
            payload : uuid
        });
    }

    /**
     * Dispatches action to toggle a leg in the Reservation History Widget
     * @param {number} index
     */
    toggleHistoryLeg(index : number) {
        this.store.dispatch({
            type : ReservationCardsActions.RESERVATION_CARDS_HISTORY_TOGGLE_EXPAND_LEG,
            payload : index
        });
    }

    /**
     * Dispatches action to toggle a leg in the Scheduled Rides Widget
     * @param {number} index
     */
    toggleScheduledLeg(index : number) {
        this.store.dispatch({
            type : ReservationCardsActions.RESERVATION_CARDS_SCHEDULED_TOGGLE_EXPAND_LEG,
            payload : index
        });
    }
}
