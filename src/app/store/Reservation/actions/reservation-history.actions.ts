import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../../app-store';
import {DateRange} from '../../types/date-range.model';
import {ServiceType} from '../../MetaDataTypes/types/service-type.model';
import {ReservationStatus} from '../types/reservation-status.model';
import {SortOptions} from '../types/reservation-history-state.model';

/**
 * Redux Actions for Reservation History
 * @export
 * @class ReservationHistoryActions
 */
@Injectable()
export class ReservationHistoryActions {
    static readonly RESERVATION_HISTORY_CLEAR_STATUS_FILTER : string = '[Reservation History] Clear Status Filter';
    static readonly RESERVATION_HISTORY_CLEAR_TREATMENT_TYPE_FILTER : string = '[Reservation History] Clear Treatment Type Filter';
    static readonly RESERVATION_HISTORY_CLEAR_TRIP_TYPE_FILTER : string = '[Reservation History] Clear Trip Type Filter';
    static readonly RESERVATION_HISTORY_NEXT_PAGE : string = '[Reservation History] Next Page';
    static readonly RESERVATION_HISTORY_PREVIOUS_PAGE : string = '[Reservation History] Previous Page';
    static readonly RESERVATION_HISTORY_SET_DATE_FILTER : string = '[Reservation History] Set Date Filter';
    static readonly RESERVATION_HISTORY_SET_ITEMS_PER_PAGE : string = '[Reservation History] Set Items Per Page';
    static readonly RESERVATION_HISTORY_SET_SORT_OPTIONS : string = '[Reservation History] Set Sort Options';
    static readonly RESERVATION_HISTORY_SET_STATUS_FILTER_VALUE : string = '[Reservation History] Set Status Filter Value';
    static readonly RESERVATION_HISTORY_SET_TEXT_FILTER : string = '[Reservation History] Set Text Filter';
    static readonly RESERVATION_HISTORY_SET_TREATMENT_TYPE_FILTER_VALUE : string = '[Reservation History] Set Treatment Type Filter Value';
    static readonly RESERVATION_HISTORY_SET_TRIP_TYPE_FILTER_VALUE : string = '[Reservation History] Set Trip Type Filter Value';

    /**
     * Creates an instance of ReservationHistoryActions.
     * @param {NgRedux<IAppState>} store
     *
     * @memberOf ReservationHistoryActions
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * Changes the date filter range
     * @param {DateRange} range
     */
    changeDateFilter(range : DateRange) {
        this.store.dispatch({
            type : ReservationHistoryActions.RESERVATION_HISTORY_SET_DATE_FILTER,
            payload : range
        });
    }

    /**
     * Sets the text filter's value
     * @param value
     *
     * @memberOf ReservationHistoryActions
     */
    changeTextFilter(value : string) {
        this.store.dispatch({
            type : ReservationHistoryActions.RESERVATION_HISTORY_SET_TEXT_FILTER,
            payload : value
        });
    }

    /**
     * Clears the status filter
     */
    clearStatusFilter() {
        this.store.dispatch({ type : ReservationHistoryActions.RESERVATION_HISTORY_CLEAR_STATUS_FILTER });
    }

    /**
     * Clears the treatment type filter
     */
    clearTreatmentTypeFilter() {
        this.store.dispatch({ type : ReservationHistoryActions.RESERVATION_HISTORY_CLEAR_TREATMENT_TYPE_FILTER });
    }

    /**
     * Clears the trip type filter
     */
    clearTripTypeFilter() {
        this.store.dispatch({ type : ReservationHistoryActions.RESERVATION_HISTORY_CLEAR_TRIP_TYPE_FILTER });
    }

    /**
     * Increments the current page
     *
     * @memberOf ReservationHistoryActions
     */
    goToNextPage() {
        this.store.dispatch({ type : ReservationHistoryActions.RESERVATION_HISTORY_NEXT_PAGE });
    }

    /**
     * Decrements the current page
     *
     * @memberOf ReservationHistoryActions
     */
    goToPreviousPage() {
        this.store.dispatch({ type : ReservationHistoryActions.RESERVATION_HISTORY_PREVIOUS_PAGE });
    }

    /**
     * Sets the number of items to display per page
     * @param {number} numberOfItems
     *
     * @memberOf ReservationHistoryActions
     */
    setItemsPerPage(numberOfItems : number) {
        this.store.dispatch({
            type : ReservationHistoryActions.RESERVATION_HISTORY_SET_ITEMS_PER_PAGE,
            payload : numberOfItems
        });
    }

    /**
     * Sets the sorting options
     * @param {SortOptions} sortOptions
     */
    setSortOptions(sortOptions : SortOptions) {
        this.store.dispatch({
            type : ReservationHistoryActions.RESERVATION_HISTORY_SET_SORT_OPTIONS,
            payload : sortOptions
        });
    }

    /**
     * Sets a status filter value to active/inactive
     * @param {ServiceType} type
     * @param {boolean} active
     */
    setStatusFilterValue(type : ReservationStatus, active : boolean) {
        this.store.dispatch({
            type : ReservationHistoryActions.RESERVATION_HISTORY_SET_STATUS_FILTER_VALUE,
            payload : {
                type,
                active
            }
        });
    }

    /**
     * Sets a treatment type filter value to active/inactive
     * @param {ServiceType} type
     * @param {boolean} active
     */
    setTreatmentTypeFilterValue(type : ServiceType, active : boolean) {
        this.store.dispatch({
            type : ReservationHistoryActions.RESERVATION_HISTORY_SET_TREATMENT_TYPE_FILTER_VALUE,
            payload : {
                type,
                active
            }
        });
    }

    /**
     * Sets a trip type filter value to active/inactive
     * @param {string} type
     * @param {boolean} active
     */
    setTripTypeFilterValue(type : string, active : boolean) {
        this.store.dispatch({
            type : ReservationHistoryActions.RESERVATION_HISTORY_SET_TRIP_TYPE_FILTER_VALUE,
            payload : {
                type,
                active
            }
        });
    }
}
