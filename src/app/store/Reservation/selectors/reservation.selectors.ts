import {Injectable} from '@angular/core';
import {List} from 'immutable';
import {NgRedux} from '@angular-redux/store';
import {Observable} from 'rxjs/Observable';

import {IAppStore} from '../../app-store';
import {ReservationRequestedByState} from '../types/reservation-requested-by-state.model';
import {ReservationTreatmentTypeState} from '../types/reservation-treatment-type-state.model';
import {ReservationAppointmentDateState} from '../types/reservation-appointment-date-state.model';
import {ReservationAdditionalPassengersState} from '../types/reservation-additional-passengers-state.model';
import {ReservationModeOfTransportationState} from '../types/reservation-mode-of-transportation-state.model';
import {ReservationPreferredTransportationState} from '../types/reservation-preferred-transportation-state.model';
import {ReservationPickupTimeState} from '../types/reservation-pickup-time-state.model';
import {BeneficiarySpecialRequirement} from '../../Beneficiary/types/beneficiary-special-requirement.model';
import {ReservationLegState} from '../types/reservation-leg-state.model';
import {AddressLocation} from '../../types/address-location.model';
import {ReservationTripIndicatorState} from '../types/reservation-trip-indicator-state.model';
import {Reservation} from '../types/reservation.model';
import {ReservationBusinessRules} from '../types/reservation-business-rules-state.model';
import {NameUuid} from '../../types/name-uuid.model';
import {ReservationCardState} from '../types/reservation-card-state.model';

@Injectable()

/**
 * implementation for ReservationStateSelectors: responsible for exposing custom state subscriptions to ReservationState
 */
export class ReservationStateSelectors {
    /**
     * ReservationStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to ReservationState.reservationRequestedByState
     * @returns {Observable<ReservationRequestedByState>}
     */
    reservationRequestedByState() : Observable<ReservationRequestedByState> {
        return this.store.select(state => state.reservationState.get('reservationRequestedByState'));
    }

    /**
     * expose Observable to ReservationState.reservationTreatmentTypeState
     * @returns {Observable<ReservationTreatmentTypeState>}
     */
    reservationTreatmentTypeState() : Observable<ReservationTreatmentTypeState> {
        return this.store.select(state => state.reservationState.get('reservationTreatmentTypeState'));
    }

    /**
     * expose Observable to ReservationState.reservationAppointmentDateState
     * @returns {Observable<ReservationAppointmentDateState>}
     */
    reservationAppointmentDateState() : Observable<ReservationAppointmentDateState> {
        return this.store.select(state => state.reservationState.get('reservationAppointmentDateState'));
    }

    /**
     * expose Observable to ReservationState.reservationPickupLocationState
     * @returns {Observable<AddressLocation>}
     */
    reservationPickupLocationState() : Observable<AddressLocation> {
        return this.store.select(state => state.reservationState.get('reservationPickupLocationState'));
    }

    /**
     * expose Observable to ReservationState.reservationDropOffLocationState
     * @returns {Observable<AddressLocation>}
     */
    reservationDropOffLocationState() : Observable<AddressLocation> {
        return this.store.select(state => state.reservationState.get('reservationDropOffLocationState'));
    }

    /**
     * expose Observable to ReservationState.reservationDropOffLocationState.mileageDistance
     * @returns {Observable<string>}
     */
    reservationMileageDistance() : Observable<string> {
        return this.store.select(state => state.reservationState.getIn(['reservationDropOffLocationState', 'mileageDistance']));
    }

    /**
     * expose Observable to ReservationState.reservationAdditionalPassengersState
     * @returns {Observable<ReservationAdditionalPassengersState>}
     */
    reservationAdditionalPassengersState() : Observable<ReservationAdditionalPassengersState> {
        return this.store.select(state => state.reservationState.get('reservationAdditionalPassengersState'));
    }

    /**
     * expose Observable to ReservationState.reservationModeOfTransportationState
     * @returns {Observable<ReservationModeOfTransportationState>}
     */
    reservationModeOfTransportationState() : Observable<ReservationModeOfTransportationState> {
        return this.store.select(state => state.reservationState.get('reservationModeOfTransportationState'));
    }

    /**
     * expose Observable to ReservationState.reservationPreferredTransportationState
     * @returns {Observable<ReservationPreferredTransportationState>}
     */
    reservationPreferredTransportationState() : Observable<ReservationPreferredTransportationState> {
        return this.store.select(state => state.reservationState.get('reservationPreferredTransportationState'));
    }

    /**
     * expose Observable to ReservationState.reservationPickupTimeState
     * @returns {Observable<ReservationPickupTimeState>}
     */
    reservationPickupTimeState() : Observable<ReservationPickupTimeState> {
        return this.store.select(state => state.reservationState.get('reservationPickupTimeState'));
    }
    /**
     * expose Observable to ReservationState.reservationSpecialRequirementState
     * @returns {Observable<BeneficiarySpecialRequirement>}
     */
    reservationSpecialRequirementState() : Observable<List<BeneficiarySpecialRequirement>> {
        return this.store.select(state => state.reservationState.get('reservationSpecialRequirementState'));
    }

    /**
     * expose Observable to ReservationState.reservationSpecialRequirementTempState
     * @returns {Observable<BeneficiarySpecialRequirement>}
     */
    reservationTempRequirementState() : Observable<List<BeneficiarySpecialRequirement>> {
        return this.store.select(state => state.reservationState.get('reservationSpecialRequirementTempState'));
    }

    /**
     * expose Observable to ReservationState.connectionRequestIssuedBy
     * @returns {Observable<string>}
     */
    connectionRequestIssuedBy() : Observable<string> {
        return this.store.select(state => state.reservationState.get('connectionRequestIssuedBy'));
    }

    /**
     * expose Observable to ReservationState.reservationLegState
     * @returns {Observable<ReservationLegState>}
     */
    reservationLegState() : Observable<ReservationLegState> {
        return this.store.select(state => state.reservationState.get('reservationLegState'));
    }

    /**
     * expose Observable to ReservationState.saveLeg
     * @returns {Observable<boolean>}
     */
    reservationSaveLegIndicator() : Observable<string> {
        return this.store.select(state => state.reservationState.get('saveLeg'));
    }

    /**
     * expose Observable to ReservationState.reservationTripIndicatorState
     * @returns {Observable<ReservationTripIndicatorState>}
     */
    reservationTripIndicatorState() : Observable<ReservationTripIndicatorState> {
        return this.store.select(state => state.reservationState.get('reservationTripIndicatorState'));
    }

    /**
     * expose Observable to ReservationState.confirmationNumber
     * @returns {Observable<string>}
     */
    reservationConfirmationNumber() : Observable<string> {
        return this.store.select(state => state.reservationState.get('confirmationNumber'));
    }

    /**
     * expose Observable to ReservationState.reservation
     * @returns {Observable<Reservation>}
     */
    reservation() : Observable<Reservation> {
        return this.store.select(state => state.reservationState.get('reservation'));
    }

    /**
     * expose Observable to ReservationState.reservations
     * @returns {Observable<List<Reservation>>}
     */
    reservations() : Observable<List<Reservation>> {
        return this.store.select(state => state.reservationState.get('reservations'));
    }

    /**
     * expose Observable to ReservationState.ReservationBusinessRules
     * @returns {Observable<ReservationBusinessRules>}
     */
    reservationBusinessRules() : Observable<ReservationBusinessRules> {
        return this.store.select(state => state.reservationState.get('reservationBusinessRules'));
    }

    /**
     * expose Observable to ReservationState.recentPickupLocations
     * @returns {Observable<List<NameUuid>>}
     */
    recentPickupLocations() : Observable<List<NameUuid>> {
        return this.store.select(state => state.reservationState.get('recentPickupLocations'));
    }

    /**
     * expose Observable to ReservationState.recentDropoffLocations
     * @returns {Observable<List<NameUuid>>}
     */
    recentDropoffLocations() : Observable<List<NameUuid>> {
        return this.store.select(state => state.reservationState.get('recentDropoffLocations'));
    }

    /**
     * Returns a reservation with the given uuid
     * @param uuid
     * @returns {Observable<Reservation>}
     */
    getReservation(uuid : string) : Observable<Reservation> {
        return this.store
            .select(state => state.reservationState.get('reservations'))
            .map(reservations => reservations.find(reservation => reservation.get('uuid') === uuid));
    }

    /**
     * Returns the reservation history card state
     * @returns {Observable<ReservationCardState>}
     */
    getHistoryCardState() : Observable<ReservationCardState> {
        return this.store.select(state => state.reservationState.getIn(['reservationCardsState', 'history']));
    }

    /**
     * Returns the scheduled rides card state
     * @returns {Observable<ReservationCardState>}
     */
    getScheduledCardState() : Observable<ReservationCardState> {
        return this.store.select(state => state.reservationState.getIn(['reservationCardsState', 'scheduled']));
    }

    /**
     * expose Observable to ReservationState.isEditing
     * @returns {Observable<ReservationCardState>}
     */
    isEditing() : Observable<boolean> {
        return this.store.select(state => state.reservationState.get('isEditing'));
    }

    /**
     * expose Observable to ReservationState.editLeg
     * @returns {Observable<number>}
     */
    editLeg() : Observable<number> {
        return this.store.select(state => state.reservationState.get('editLeg'));
    }

    /**
     * expose Observable to ReservationState.deleteLeg
     * @returns {Observable<number>}
     */
    deleteLeg() : Observable<number> {
        return this.store.select(state => state.reservationState.get('deleteLeg'));
    }

    /**
     * returns edit reasons
     * @returns {Observable<List<NameUuid>>}
     */
    getEditReasons() : Observable<List<NameUuid>> {
        return this.store.select(state => state.reservationState.getIn(['reservationEventReasons', 'updateReasons'], List()));
    }

    /**
     * returns the edit reason matching the uuid provided
     * @param {string} uuid
     * @returns {Observable<NameUuid>}
     */
    getEditReason(uuid : string) : Observable<NameUuid> {
        return this.getEditReasons()
            .map(reasons => reasons
                .find(reason => reason.get('uuid') === uuid, new NameUuid())
            );
    }

    /**
     * returns cancellation reasons
     * @returns {Observable<List<NameUuid>>}
     */
    getCancelReasons() : Observable<List<NameUuid>> {
        return this.store.select(state => state.reservationState.getIn(['reservationEventReasons', 'cancelReasons'], List()));
    }

    /**
     * returns the cancellation reason matching the uuid provided
     * @param {string} uuid
     * @returns {Observable<NameUuid>}
     */
    getCancelReason(uuid : string) : Observable<NameUuid> {
        return this.getCancelReasons()
            .map(reasons => reasons
                .find(reason => reason.get('uuid') === uuid, new NameUuid())
            );
    }
}
