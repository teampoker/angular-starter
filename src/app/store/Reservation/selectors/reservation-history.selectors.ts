import {NgRedux} from '@angular-redux/store';
import {Injectable} from '@angular/core';
import {
    List,
    Map
} from 'immutable';
import * as moment from 'moment';
import {Observable} from 'rxjs';

import {IAppStore} from '../../app-store';
import {ReservationHistoryFiltersState} from '../types/reservation-history-filters-state.model';
import {
    ReservationHistoryState,
    SortOptions
} from '../types/reservation-history-state.model';
import {ReservationStatus} from '../types/reservation-status.model';
import {Reservation} from '../types/reservation.model';

/**
 * Selectors for reservation history
 * @export
 * @class ReservationHistorySelectors
 */
@Injectable()
export class ReservationHistorySelectors {
    /**
     * Creates an instance of ReservationHistory.
     * @param {NgRedux<IAppState>} store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    private readonly sortFunctions : { [key : number] : (a : Reservation, b : Reservation) => number } = {
        [SortOptions.APPOINTMENT_TIME_ASC] : sortByAppointmentTimeAscending,
        [SortOptions.APPOINTMENT_TIME_DESC] : sortByAppointmentTimeDescending,
        [SortOptions.TREATMENT_TYPE_ASC] : sortByTreatmentTypeAscending,
        [SortOptions.TREATMENT_TYPE_DESC] : sortByTreatmentTypeDescending,
        [SortOptions.TRIP_DATE_ASC] : sortByTripDateAscending,
        [SortOptions.TRIP_DATE_DESC] : sortByTripDateDescending,
        [SortOptions.TRIP_ID_ASC] : sortByTripIdAscending,
        [SortOptions.TRIP_ID_DESC] : sortByTripIdDescending,
        [SortOptions.TRIP_STATUS_ASC] : sortByTripStatusAscending,
        [SortOptions.TRIP_STATUS_DESC] : sortByTripStatusDescending,
        [SortOptions.TRIP_TYPE_ASC] : sortByTripTypeAscending,
        [SortOptions.TRIP_TYPE_DESC] : sortByTripTypeDescending
    };

    private filter(reservations : List<Reservation>, filters : ReservationHistoryFiltersState) : List<Reservation> {
        // filter by text
        reservations = this.filterByText(reservations, filters.get('text', ''));

        // filter by treatment type
        if (filters.get('treatmentType').filter(enabled => enabled).count() > 0) {
            reservations = this.filterByTreatmentType(reservations, filters.get('treatmentType', Map()));
        }

        // filter by status
        if (filters.get('tripStatus').filter(enabled => enabled).count() > 0) {
            reservations = this.filterByTripStatus(reservations, filters.get('tripStatus'));
        }

        // filter by trip type
        if (filters.get('tripType').filter(enabled => enabled).count() > 0) {
            reservations = this.filterByTripType(reservations, filters.get('tripType'));
        }

        // filter by date
        if (filters.getIn(['dateRange', 'fromDate'])) {
            reservations = this.filterByFromDate(reservations, filters.getIn(['dateRange', 'fromDate']));
        }

        if (filters.getIn(['dateRange', 'thruDate'])) {
            reservations = this.filterByThruDate(reservations, filters.getIn(['dateRange', 'thruDate']));
        }

        return reservations;
    }

    private filterByFromDate(reservations : List<Reservation>, date : string) : List<Reservation> {
        return reservations
            .filter(reservation => moment.utc(
                    reservation.getIn(['legs', 0, 'reservationDateTime', 'date']),
                    'YYYY-MM-DDTHH:mm:ssZ[UTC]'
                ).isSameOrAfter(moment.utc(date, 'MM/DD/YYYY'))) as List<Reservation>;
    }

    private filterByThruDate(reservations : List<Reservation>, date : string) : List<Reservation> {
        return reservations
            .filter(reservation => moment.utc(
                    reservation.getIn(['legs', 0, 'reservationDateTime', 'date']),
                    'YYYY-MM-DDTHH:mm:ssZ[UTC]'
                ).isSameOrBefore(moment.utc(date, 'MM/DD/YYYY'))) as List<Reservation>;
    }

    private filterByText(reservations : List<Reservation>, text : string) : List<Reservation> {
        return reservations.filter(reservation => reservation.get('referenceId', '').indexOf(text) !== -1) as List<Reservation>;
    }

    private filterByTreatmentType(reservations : List<Reservation>, treatmentTypes : Map<string, boolean>) : List<Reservation> {
        return reservations
            .filter(reservation => {
                let selected : boolean = false;

                reservation
                    .get('legs')
                    .forEach(leg => {
                        selected = treatmentTypes.get(leg.getIn(['treatmentType', 'uuid'])) || selected;
                    });

                return selected;
            }) as List<Reservation>;
    }

    private filterByTripStatus(reservations : List<Reservation>, statuses : Map<string, boolean>) : List<Reservation> {
        return reservations.filter(reservation => statuses.get(reservation.getIn(['status', 'id']))) as List<Reservation>;
    }

    private filterByTripType(reservations : List<Reservation>, types : Map<string, boolean>) : List<Reservation> {
        return reservations.filter(reservation => types.get(reservation.getIn(['legs', 0, 'type']))) as List<Reservation>;
    }

    /**
     * Returns a sort function that fulfills the selected option if valid
     * @param sortOption
     * @returns {(a:Reservation, b:Reservation)=>number}
     */
    getSortFunction(sortOption : SortOptions) : (a : Reservation, b : Reservation) => number {
        const sortFunction = this.sortFunctions[sortOption];

        return typeof sortFunction === 'function' ? sortFunction : sortByAppointmentTimeAscending;
    }

    /**
     * Returns an Observable of reservation history settings
     * @returns {Observable<ReservationHistoryState>}
     */
    getSettings() : Observable<ReservationHistoryState> {
        return this.store.select(state => state.reservationState.get('reservationHistory'));
    }

    /**
     * Returns a Observable containing a List of a beneficiary's reservations
     * @param {string} beneficiaryUuid
     * @param {boolean} applyFilters
     * @returns {Observable<List<Reservation>>}
     */
    getReservations(beneficiaryUuid : string, applyFilters : boolean = false) : Observable<List<Reservation>> {
        return this.store
            .select(state => state.reservationState)
            .map(reservationState => {
                let reservations = reservationState.get('reservations')
                        .filter(reservation => reservation.get('personUuid') === beneficiaryUuid);
                const filters = reservationState.getIn(['reservationHistory', 'filters']);

                // restrict to past reservations
                reservations = reservations
                    .filter(reservation => {
                        return moment.utc(
                            reservation.getIn(['legs', 0, 'reservationDateTime', 'date']),
                            'YYYY-MM-DDTHH:mm:ssZ[UTC]'
                        ).isBefore(moment.utc());
                    });

                if (applyFilters) {
                    reservations = this.filter(reservations, filters);
                }

                return reservations;
            });
    }

    /**
     * Returns a Observable containing a List of a beneficiary's scheduled rides
     * @param {string} beneficiaryUuid
     * @param {boolean} applyFilters
     * @returns {Observable<List<Reservation>>}
     */
    getScheduledRides(beneficiaryUuid : string, applyFilters : boolean = false) : Observable<List<Reservation>> {
        return this.store
            .select(state => state.reservationState)
            .map(reservationState => {
                let reservations = reservationState.get('reservations')
                    .filter(reservation => reservation.get('personUuid') === beneficiaryUuid);
                const filters = reservationState.getIn(['reservationHistory', 'filters']);

                // restrict to future reservations
                reservations = reservations
                    .filter(reservation => {
                        return moment.utc(
                            reservation.getIn(['legs', 0, 'reservationDateTime', 'date']),
                            'YYYY-MM-DDTHH:mm:ssZ[UTC]'
                        ).isSameOrAfter(moment.utc());
                    });

                if (applyFilters) {
                    reservations = this.filter(reservations, filters);
                }

                return reservations;
            });
    }

    /**
     * Returns a Map of all reservation statuses
     * @param {string} beneficiaryUuid
     * @returns {Observable<Map<string, ReservationStatus>>}
     */
    getStatuses(beneficiaryUuid : string) : Observable<Map<string, ReservationStatus>> {
        return this.store
            .select(state => state.reservationState.get('reservations'))
            .map(reservations => reservations.filter(reservation => reservation.get('personUuid') === beneficiaryUuid))
            .map(reservations => reservations.map(reservation => reservation.get('status')))
            .map(statuses => statuses.reduce((acc, status) => acc.set(status.get('id'), status), Map()));
    }

    /**
     * Returns a list of all reservation trip types
     * @param {string} beneficiaryUuid
     * @returns {Observable<List<string>>}
     */
    getTripTypes(beneficiaryUuid : string) : Observable<List<string>> {
        return this.store
            .select(state => state.reservationState.get('reservations'))
            .map(reservations => reservations
                .filter(reservation => reservation.get('personUuid') === beneficiaryUuid)
                .map(reservation => reservation.getIn(['legs', 0, 'type']))
                .filter(type => type && type !== '')
                .map(type => type)
                .groupBy(type => type)
                .map(typeGroup => typeGroup.first())
            )
            .map(types => types.toList());
    }
}

/**
 * Sorts alphabetically ascending
 * @param {string} a
 * @param {string} b
 * @returns {number}
 */
function sortByAlphaAscending(a : string, b : string) : number {
    return a.localeCompare(b);
}

/**
 * Sorts alphabetically descending
 * @param {string} a
 * @param {string} b
 * @returns {number}
 */
function sortByAlphaDescending(a : string, b : string) : number {
    return b.localeCompare(a);
}

/**
 * Sorts by appointment time ascending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByAppointmentTimeAscending(a : Reservation, b : Reservation) : number {
    const aMoment = moment.utc(a.getIn(['legs', 0, 'reservationDateTime', 'time']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]').local();
    const bMoment = moment.utc(b.getIn(['legs', 0, 'reservationDateTime', 'time']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]').local();

    return (moment.utc({ h : aMoment.hour(), m : aMoment.minute(), s : aMoment.second()}) as any) -
        (moment.utc({ h : bMoment.hour(), m : bMoment.minute(), s : bMoment.second()}) as any);
}

/**
 * Sorts by appointment time descending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByAppointmentTimeDescending(a : Reservation, b : Reservation) : number {
    const aMoment = moment.utc(a.getIn(['legs', 0, 'reservationDateTime', 'time']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]').local();
    const bMoment = moment.utc(b.getIn(['legs', 0, 'reservationDateTime', 'time']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]').local();

    return (moment.utc({ h : bMoment.hour(), m : bMoment.minute(), s : bMoment.second()}) as any) -
        (moment.utc({ h : aMoment.hour(), m : aMoment.minute(), s : aMoment.second()}) as any);
}

/**
 * Sorts by treatment type ascending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTreatmentTypeAscending(a : Reservation, b : Reservation) : number {
    const aType = a.getIn(['legs', 0, 'treatmentType', 'name'], '');
    const bType = b.getIn(['legs', 0, 'treatmentType', 'name'], '');

    return sortByAlphaAscending(aType, bType);
}

/**
 * Sorts by treatment type descending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTreatmentTypeDescending(a : Reservation, b : Reservation) : number {
    const aType = a.getIn(['legs', 0, 'treatmentType', 'name'], '');
    const bType = b.getIn(['legs', 0, 'treatmentType', 'name'], '');

    return sortByAlphaDescending(aType, bType);
}

/**
 * Sorts by trip date ascending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTripDateAscending(a : Reservation, b : Reservation) : number {
    return (moment.utc(a.getIn(['legs', 0, 'reservationDateTime', 'date']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]') as any) -
        (moment.utc(b.getIn(['legs', 0, 'reservationDateTime', 'date']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]') as any);
}

/**
 * Sorts by trip date descending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTripDateDescending(a : Reservation, b : Reservation) : number {
    return (moment.utc(b.getIn(['legs', 0, 'reservationDateTime', 'date']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]') as any) -
        (moment.utc(a.getIn(['legs', 0, 'reservationDateTime', 'date']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]') as any);
}

/**
 * Sorts by trip id ascending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTripIdAscending(a : Reservation, b : Reservation) : number {
    const aId = a.get('referenceId', '');
    const bId = b.get('referenceId', '');

    return sortByAlphaAscending(aId, bId);
}

/**
 * Sorts by trip id descending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTripIdDescending(a : Reservation, b : Reservation) : number {
    const aId = a.get('referenceId', '');
    const bId = b.get('referenceId', '');

    return sortByAlphaDescending(aId, bId);
}

/**
 * Sorts by trip status ascending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTripStatusAscending(a : Reservation, b : Reservation) : number {
    const aStatus = a.getIn(['status', 'name'], '');
    const bStatus = b.getIn(['status', 'name'], '');

    return sortByAlphaAscending(aStatus, bStatus);
}

/**
 * Sorts by trip status descending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTripStatusDescending(a : Reservation, b : Reservation) : number {
    const aStatus = a.getIn(['status', 'name'], '');
    const bStatus = b.getIn(['status', 'name'], '');

    return sortByAlphaDescending(aStatus, bStatus);
}

/**
 * Sorts by trip type ascending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTripTypeAscending(a : Reservation, b : Reservation) : number {
    const aStatus = a.getIn(['legs', 0, 'type'], '');
    const bStatus = b.getIn(['legs', 0, 'type'], '');

    return sortByAlphaAscending(aStatus, bStatus);
}

/**
 * Sorts by trip type descending
 * @param {Reservation} a
 * @param {Reservation} b
 * @returns {number}
 */
function sortByTripTypeDescending(a : Reservation, b : Reservation) : number {
    const aStatus = a.getIn(['legs', 0, 'type'], '');
    const bStatus = b.getIn(['legs', 0, 'type'], '');

    return sortByAlphaDescending(aStatus, bStatus);
}
