import {
    fromApi,
    toApi,
    ReservationLegState
} from './reservation-leg-state.model';
import {RESERVATION_MOCK} from '../../../areas/Reservation/services/Reservation/reservation.service.mock';
import * as AddressLocation from '../../types/address-location.model';
import {Person} from '../../types/person.model';
import {TransportationItemPassenger} from '../../types/transportation-item-passenger.model';
import {ReservationPassengerState} from './reservation-passenger';
import {BeneficiarySpecialRequirement} from '../../Beneficiary/types/beneficiary-special-requirement.model';
import {ReservationStatus} from './reservation-status.model';
import {DateTime} from '../../types/date-time.model';
import {TransportationItemSpecialRequirement} from '../../types/transportation-item-special-requirement.model';
import * as NameUuid from '../../types/name-uuid.model';

describe('Model: ReservationLegState', () => {
    it('should not throw when supplied an empty object at instantiation', () => {
        expect(() => new (ReservationLegState as any)({})).not.toThrow();
    });

    describe('fromApi', () => {
        describe('Invalid Input', () => {
            const invalidFromApiCalls = [
                () => fromApi(undefined),
                () => fromApi(null),
                () => fromApi(true),
                () => fromApi('string'),
                () => fromApi(123),
                () => fromApi(() => {}),
                () => fromApi([1, 2, 3])
            ];
            it('should not throw when an invalid argument is passed', () => {
                // since this function is handling API responses we cannot rely on
                // type checking to catch errors when transpiling, so we have to
                // do runtime input validation
                invalidFromApiCalls.forEach(call => expect(call).not.toThrow());
            });
            it('should return a default leg state on invalid input', () => {
                const controlLegState = new ReservationLegState();

                invalidFromApiCalls.forEach(call => {
                    const testLegState = call();

                    expect(controlLegState.equals(testLegState)).toEqual(true);
                });
            });
        });
        describe('Valid Input', () => {
            describe('getReservation Response', () => {
                const mockLeg = RESERVATION_MOCK['getReservation'][0]['items'][0];
                const result  = fromApi(mockLeg);

                it('should return a ReservationLegState instance', () => {
                    expect(result instanceof ReservationLegState).toBe(true);
                });
                it('should have the correct uuid', () => {
                    expect(result.get('uuid')).toEqual(mockLeg.uuid);
                });
                xit('should have the correct requested by information', () => {
                    expect(result.get('requestedBy') instanceof NameUuid.NameUuid).toEqual(true);
                    expect(result.getIn(['requestedBy', 'uuid'])).toEqual(mockLeg.requestedByPersonUuid);
                });
                it('should have the correct treatment type information', () => {
                    expect(result.get('treatmentType') instanceof NameUuid.NameUuid).toEqual(true);
                    expect(result.getIn(['treatmentType', 'uuid'])).toEqual(mockLeg.treatmentType.uuid);
                });
                it('should have the correct appointment time', () => {
                    expect(result.getIn(['reservationDateTime', 'date'])).toEqual(mockLeg.appointmentOn);
                    expect(result.getIn(['reservationDateTime', 'time'])).toEqual(mockLeg.appointmentOn);
                });
                xit('should have the correct pickup location', () => {
                    expect(result.get('pickupLocation') instanceof AddressLocation.AddressLocation).toEqual(true);
                });
                xit('should have the correct drop off location', () => {
                    expect(result.get('dropOffLocation') instanceof AddressLocation.AddressLocation).toEqual(true);
                });
                xit('should have the correct driver information', () => {
                    expect(result.get('driverInfo') instanceof NameUuid.NameUuid).toEqual(true);
                    expect(result.getIn(['driverInfo', 'name'])).toEqual(mockLeg.driver.name);
                    expect(result.getIn(['driverInfo', 'uuid'])).toEqual(mockLeg.driver.uuid);
                });
                it('should have the correct additional passengers info', () => {
                    expect(result.get('transportationItemPassengers').count()).toEqual(mockLeg.transportationItemPassengers.length);

                    mockLeg.transportationItemPassengers.forEach(passenger => {
                        const resultPassenger = result.get('transportationItemPassengers').find(x => x.get('personUuid') === passenger.personUuid);

                        expect(resultPassenger).toBeDefined();
                        expect(resultPassenger instanceof TransportationItemPassenger).toEqual(true);
                    });
                });
            });
            describe('getReservations Response', () => {
                const mockLeg = RESERVATION_MOCK['getReservations'][0]['items'][0];
                const result  = fromApi(mockLeg);

                it('should return a ReservationLegState instance', () => {
                    expect(result instanceof ReservationLegState).toBe(true);
                });
                it('should have the correct uuid', () => {
                    expect(result.get('uuid')).toEqual(mockLeg.uuid);
                });
                xit('should have the correct requested by information', () => {
                    expect(result.get('requestedBy') instanceof NameUuid.NameUuid).toEqual(true);
                    expect(result.getIn(['requestedBy', 'uuid'])).toEqual(mockLeg.requestedByPersonUuid);
                });
                it('should have the correct treatment type information', () => {
                    expect(result.get('treatmentType') instanceof NameUuid.NameUuid).toEqual(true);
                    expect(result.getIn(['treatmentType', 'uuid'])).toEqual(mockLeg.treatmentType.uuid);
                });
                it('should have the correct appointment time', () => {
                    expect(result.getIn(['reservationDateTime', 'date'])).toEqual(mockLeg.appointmentOn);
                    expect(result.getIn(['reservationDateTime', 'time'])).toEqual(mockLeg.appointmentOn);
                });
                xit('should have the correct pickup location', () => {
                    expect(result.get('pickupLocation') instanceof AddressLocation.AddressLocation).toEqual(true);
                });
                xit('should have the correct drop off location', () => {
                    expect(result.get('dropOffLocation') instanceof AddressLocation.AddressLocation).toEqual(true);
                });
                // Have to update the mocks before re-implementing this test
                xit('should have the correct driver information', () => {
                    expect(result.get('driver') instanceof Person).toEqual(true);
                    expect(result.getIn(['driver', 'firstName'])).toEqual(mockLeg.driver.name);
                    expect(result.getIn(['driverInfo', 'uuid'])).toEqual(mockLeg.driver.uuid);
                });
                it('should have the correct additional passengers info', () => {
                    mockLeg.transportationItemPassengers.forEach(passenger => {
                        const resultPassenger = result.get('transportationItemPassengers').find(x => x.get('personUuid') === passenger.personUuid);

                        expect(resultPassenger).toBeDefined();
                        expect(resultPassenger instanceof TransportationItemPassenger).toEqual(true);
                    });
                });
            });
        });
    });

    describe('toApi', () => {
        const leg = new ReservationLegState({
            uuid : '70a75bc5-76ba-48d6-bd84-402eea5a4a31',
            requestedBy : new ReservationPassengerState({ uuid : '42d864dc-3b86-48c5-8944-7b3e95848c3b' } as any),
            treatmentType : new NameUuid.NameUuid(),
            reservationDateTime : new DateTime(),
            pickupLocation : new AddressLocation.AddressLocation(),
            dropOffLocation : new AddressLocation.AddressLocation(),
            modeOfTransportation : new NameUuid.NameUuid(),
            driver : new Person(),
            driverInfo : new ReservationPassengerState(),
            additionalPassengers : [
                new ReservationPassengerState(),
                new ReservationPassengerState()
            ],
            transportationItemPassengers : [
                new TransportationItemPassenger(),
                new TransportationItemPassenger()
            ],
            transportationItemSpecialRequirements : [
                new TransportationItemSpecialRequirement(),
                new TransportationItemSpecialRequirement()
            ],
            specialRequirements : [
                new BeneficiarySpecialRequirement(),
                new BeneficiarySpecialRequirement()
            ],
            pickupTime : '2017-03-21T04:03:00Z',
            legDistance : 787,
            legDuration : 710,
            version : 1,
            status : new ReservationStatus(),
            type : 'Transportation',
            createdOn : '2017-03-16T22:42:02.000Z',
            createdBy : 'Create-Reservation-User-Placeholder',
            updatedOn : '2017-03-16T22:42:02.000Z',
            updatedBy : 'Create-Reservation-User-Placeholder',
            ordinality : 1
        } as any);
        const result = toApi(leg);

        it('should have the correct uuid', () => {
            expect(result.uuid).toBe(leg.get('uuid'));
        });
        it('should have the correct requestedByPersonUuid', () => {
            expect(result.requestedByPersonUuid).toBe(leg.getIn(['requestedBy', 'uuid']));
        });
        it('should have the correct version', () => {
            expect(result.version).toBe(leg.get('version'));
        });
        it('should have the correct distance', () => {
            expect(result.distance).toBe(leg.get('legDistance'));
        });
        it('should have the correct duration', () => {
            expect(result.duration).toBe(leg.get('legDuration'));
        });
        it('should have the correct type', () => {
            expect(result.type).toBe(leg.get('type'));
        });
        it('should have the treatment type', () => {
            spyOn(NameUuid, 'toApi');

            toApi(leg);

            expect(result.treatmentType).toBeDefined();
            expect(NameUuid.toApi).toHaveBeenCalledWith(leg.get('treatmentType'), true);
        });
        it('should have the mode of transportation', () => {
            spyOn(NameUuid, 'toApi');

            toApi(leg);

            expect(result.modeOfTransportation).toBeDefined();
            expect(NameUuid.toApi).toHaveBeenCalledWith(leg.get('modeOfTransportation'), true);
        });
        it('should have the drop off location', () => {
            spyOn(AddressLocation, 'toApi');

            toApi(leg);

            expect(result.dropOffLocation).toBeDefined();
            expect(AddressLocation.toApi).toHaveBeenCalledWith(leg.get('dropOffLocation'));
        });
        it('should have the pickup location', () => {
            spyOn(AddressLocation, 'toApi');

            toApi(leg);

            expect(result.pickUpLocation).toBeDefined();
            expect(AddressLocation.toApi).toHaveBeenCalledWith(leg.get('pickupLocation'));
        });
    });
});
