import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IReservationCardState {
    expandedItems : { [index : number] : boolean };
    selectedItemUuid : string;
}

export const RESERVATION_CARD_STATE = Record({
    expandedItems : Map<string, boolean>(),
    selectedItemUuid : undefined
});

export class ReservationCardState extends RESERVATION_CARD_STATE {
    expandedItems : Map<string, boolean>;
    selectedItemUuid : string;

    constructor(values? : ReservationCardState | IReservationCardState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationCardState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
