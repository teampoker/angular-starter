import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';
import {isObject} from 'rxjs/util/isObject';

import {
    IReservationLegState,
    ReservationLegState,
    fromApi as reservationLegFromApi,
    toApi as reservationLegToApi
} from './reservation-leg-state.model';
import {
    IReservationStatus,
    ReservationStatus
} from './reservation-status.model';

export interface IReservation {
    legs                 : Array<IReservationLegState>;
    personUuid?          : string;
    referenceId?         : string;
    eventComment?        : string;
    eventReasonUuid?     : string;
    status?              : IReservationStatus;
    uuid?                : string;
    version?             : number;
}

export const RESERVATION = Record({
    legs                 : List<ReservationLegState>(),
    personUuid           : '',
    referenceId          : '',
    eventComment         : undefined,
    eventReasonUuid      : undefined,
    status               : new ReservationStatus(),
    uuid                 : '',
    version              : null
});

export class Reservation extends RESERVATION {
    legs                 : List<ReservationLegState>;
    personUuid           : string;
    referenceId?         : string;
    eventComment?        : string;
    eventReasonUuid?     : string;
    status?              : ReservationStatus;
    uuid?                : string;
    version?             : number;

    constructor(values? : Reservation | IReservation) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof Reservation) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // status
                convertedValues = convertedValues.set('status', new ReservationStatus(convertedValues.get('status')));

                // legs
                convertedValues = convertedValues.set(
                    'legs',
                    List(
                        convertedValues.get('legs', [])
                            .map(value => new ReservationLegState(value))
                    )
                );
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * Parses a reservation API response into a Reservation model
 *
 * @param {any} reservation reservation domain model
 *
 * @returns {Reservation}
 */
export function fromApi(reservation : any) : Reservation {
    const reservationState : any = {};

    if (isObject(reservation) && !Array.isArray(reservation)) {
        reservationState.personUuid = reservation.personUuid;
        reservationState.referenceId = reservation.referenceId;
        reservationState.eventComment = reservation.eventComment;
        reservationState.eventReasonUuid = reservation.eventReasonUuid;
        reservationState.status = reservation.status;
        reservationState.uuid = reservation.uuid;
        reservationState.version = reservation.version;

        if (Array.isArray(reservation.items)) {
            reservationState.legs = List<ReservationLegState>(reservation.items.map(leg => reservationLegFromApi(leg)));
        }
    }

    return new Reservation(reservationState);
}

/**
 * transform Immutable Reservation model into
 * appropriate POJO for processing by API endpoints
 *
 * @param {Reservation} reservation
 *
 * @returns {any} POJO representation
 */
export function toApi(reservation : Reservation) : any {
        const newObject : any = {
            personUuid : reservation.get('personUuid'),
            referenceId : reservation.get('referenceId'),
            eventComment : reservation.get('eventComment'),
            eventReasonUuid : reservation.get('eventReasonUuid'),
            status : reservation.get('status') ? reservation.get('status').toJS() : undefined,
            version : reservation.get('version') ? reservation.get('version') : undefined
        };

        newObject.items = reservation
            .get('legs', List())
            .map((leg, index) => {
                leg = leg.set('ordinality', index);

                return reservationLegToApi(leg);
            })
            .toJS();

    return newObject;
}
