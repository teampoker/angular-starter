import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IReservationCardState,
    ReservationCardState
} from './reservation-card-state.model';

export interface IReservationCardsState {
    history : IReservationCardState;
    scheduled : IReservationCardState;
}

export const ReservationCardsStateRecord = Record({
    history : new ReservationCardState(),
    scheduled : new ReservationCardState()
});

export class ReservationCardsState extends ReservationCardsStateRecord {
    history : ReservationCardState;
    scheduled : ReservationCardState;

    constructor(values? : ReservationCardState | IReservationCardState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationCardsState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
