import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IReservationFieldUpdate {
    fieldName               : string;
    fieldValue              : string | boolean | number;
    primaryUpdateIndex?     : number;
    secondaryUpdateIndex?   : number;
    tertiaryUpdateIndex?    : number;
}

const RESERVATION_FIELD_UPDATE = Record({
    fieldName               : '',
    fieldValue              : '',
    primaryUpdateIndex      : 0,
    secondaryUpdateIndex    : 0,
    tertiaryUpdateIndex     : 0
});

export class ReservationFieldUpdate extends RESERVATION_FIELD_UPDATE {
    fieldName               : string;
    fieldValue              : string | boolean | number;
    primaryUpdateIndex?     : number;
    secondaryUpdateIndex?   : number;
    tertiaryUpdateIndex?    : number;

    constructor(values? : ReservationFieldUpdate | IReservationFieldUpdate) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationFieldUpdate) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }
}
