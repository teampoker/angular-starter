import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IReservationAccelerator,
    ReservationAccelerator
} from './reservation-accelerator.model';

export interface IReservationConnectionsState {
    requestedByAccelerator  : Array<IReservationAccelerator>;
}

export const RESERVATION_CONNECTIONS_STATE = Record({
    requestedByAccelerator  : List<ReservationAccelerator>()
});

export class ReservationConnectionsState extends RESERVATION_CONNECTIONS_STATE {
    requestedByAccelerator  : Array<ReservationAccelerator>;

    constructor(values? : ReservationConnectionsState | IReservationConnectionsState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationConnectionsState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
