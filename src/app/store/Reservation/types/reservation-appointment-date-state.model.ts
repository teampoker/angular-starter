import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IReservationAppointmentDateState {
    appointmentDate            : string;
    appointmentTime            : string;
    appointmentDateValid       : boolean;
    appointmentTimeValid       : boolean;
}

export const RESERVATION_APPOINTMENT_DATE_STATE = Record({
    appointmentDate            : '',
    appointmentTime            : '',
    appointmentDateValid       : false,
    appointmentTimeValid       : false
});

export class ReservationAppointmentDateState extends RESERVATION_APPOINTMENT_DATE_STATE {
    appointmentDate            : string;
    appointmentTime            : string;
    appointmentDateValid       : boolean;
    appointmentTimeValid       : boolean;

    constructor(values? : ReservationAppointmentDateState | IReservationAppointmentDateState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationAppointmentDateState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
