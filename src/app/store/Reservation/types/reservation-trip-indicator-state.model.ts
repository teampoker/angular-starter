import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IReservationTripIndicatorState {
    pickupTime  : Date;
    roundTrip   : boolean;
    willCall    : boolean;
    isFormValid : boolean;
}

export const RESERVATION_TRIP_INDICATOR_STATE = Record({
    pickupTime  : null,
    roundTrip   : true,
    willCall    : false,
    isFormValid : false
});

export class ReservationTripIndicatorState extends RESERVATION_TRIP_INDICATOR_STATE {
    pickupTime  : Date;
    roundTrip   : boolean;
    willCall    : boolean;
    isFormValid : boolean;

    constructor(values? : ReservationTripIndicatorState | IReservationTripIndicatorState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationTripIndicatorState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
