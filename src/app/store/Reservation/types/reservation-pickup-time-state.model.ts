import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IReservationPickupTimeState {
    pickupTime  : Date;
    isFormValid : boolean;
}

export const RESERVATION_PICKUP_TIME_STATE = Record({
    pickupTime  : null,
    isFormValid : false
});

export class ReservationPickupTimeState extends RESERVATION_PICKUP_TIME_STATE {
    pickupTime  : Date;
    isFormValid : boolean;

    constructor(values? : ReservationPickupTimeState | IReservationPickupTimeState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationPickupTimeState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
