import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IReservationAccelerator,
    ReservationAccelerator
} from './reservation-accelerator.model';
import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

export interface IReservationRequestedByState {
    isFormValid                 : boolean;
    requestedByInfo             : INameUuid;
    requestedByAccelerator      : Array<IReservationAccelerator>;
}

export const RESERVATION_REQUESTED_BY_STATE = Record({
    isFormValid             : false,
    requestedByInfo         : new NameUuid(),
    requestedByAccelerator  : List<ReservationAccelerator>()
});

export class ReservationRequestedByState extends RESERVATION_REQUESTED_BY_STATE {
    isFormValid                 : boolean;
    requestedByInfo             : NameUuid;
    requestedByAccelerator      : List<ReservationAccelerator>;

    constructor(values? : ReservationRequestedByState | IReservationRequestedByState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationRequestedByState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // requestedByInfo
                convertedValues = convertedValues.set('requestedByInfo', new NameUuid(convertedValues.get('requestedByInfo')));

                // requestedByAccelerator
                convertedValues = convertedValues.set('requestedByAccelerator', List(convertedValues.get('requestedByAccelerator', []).map(value => new ReservationAccelerator(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
