import {
    fromJS,
    Map,
    List,
    Record
} from 'immutable';

import {
    IReservationAccelerator,
    ReservationAccelerator
} from './reservation-accelerator.model';
import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';
import {
    IReservationPassengerState,
    ReservationPassengerState
} from './reservation-passenger';

export interface IReservationAdditionalPassengersState {
    additionalPassengers            : boolean;
    isFormValid                     : boolean;
    passenger                       : INameUuid;
    passengers                      : Array<IReservationPassengerState>;
    additionalPassengersAccelerator : Array<IReservationAccelerator>;
}

export const RESERVATION_ADDITIONAL_PASSENGERS_STATE = Record({
    additionalPassengers            : false,
    isFormValid                     : true,
    passenger                       : new NameUuid(),
    passengers                      : List<ReservationPassengerState>(),
    additionalPassengersAccelerator : List<ReservationAccelerator>()
});

export class ReservationAdditionalPassengersState extends RESERVATION_ADDITIONAL_PASSENGERS_STATE {
    additionalPassengers            : boolean;
    isFormValid                     : boolean;
    passenger                       : NameUuid;
    passengers                      : List<ReservationPassengerState>;
    additionalPassengersAccelerator : List<ReservationAccelerator>;

    constructor(values? : ReservationAdditionalPassengersState | IReservationAdditionalPassengersState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationAdditionalPassengersState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // passenger
                convertedValues = convertedValues.set('passenger', new NameUuid(convertedValues.get('passenger')));

                // passengers
                convertedValues = convertedValues.set('passengers', List(convertedValues.get('passengers', []).map(value => new ReservationPassengerState(value))));

                // additionalPassengersAccelerator
                convertedValues = convertedValues.set('additionalPassengersAccelerator', new ReservationAccelerator(convertedValues.get('additionalPassengersAccelerator')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
