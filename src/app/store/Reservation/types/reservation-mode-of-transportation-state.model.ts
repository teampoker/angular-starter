import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IReservationAccelerator,
    ReservationAccelerator
} from './reservation-accelerator.model';
import {ServiceCategories} from '../../MetaDataTypes/types/service-categories.model';
import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';
import {
    fromStorage as routeFromStorage,
    IMassTransitRoute,
    MassTransitRoute
} from '../../types/mass-transit-route.model';

export interface IReservationModeOfTransportationState {
    driverInfo                        : INameUuid;
    isFormValid                       : boolean;
    massTransitRoutes                 : Array<IMassTransitRoute>;
    modeOfTransportation              : INameUuid;
    modeOfTransportationAccelerator   : Array<IReservationAccelerator>;
    modeOfTransportationDropdown      : Array<ServiceCategories>;
}

export const RESERVATION_MODE_OF_TRANSPORTATION_STATE = Record({
    driverInfo                        : new NameUuid(),
    isFormValid                       : false,
    massTransitRoutes                 : List<MassTransitRoute>(),
    modeOfTransportation              : new NameUuid(),
    modeOfTransportationAccelerator   : List<ReservationAccelerator>(),
    modeOfTransportationDropdown      : List<ServiceCategories>()
});

export class ReservationModeOfTransportationState extends RESERVATION_MODE_OF_TRANSPORTATION_STATE {
    driverInfo                        : NameUuid;
    isFormValid                       : boolean;
    massTransitRoutes                 : List<MassTransitRoute>;
    modeOfTransportation              : NameUuid;
    modeOfTransportationAccelerator   : List<ReservationAccelerator>;
    modeOfTransportationDropdown      : List<ServiceCategories>;

    constructor(values? : ReservationModeOfTransportationState | IReservationModeOfTransportationState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationModeOfTransportationState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // driverInfo
                convertedValues = convertedValues.set('driverInfo', new NameUuid(convertedValues.get('driverInfo')));

                // massTransitRoutes
                convertedValues = convertedValues.set(
                    'massTransitRoutes',
                    convertedValues.get('massTransitRoutes', List()).map(routeFromStorage)
                );

                // modeOfTransportation
                convertedValues = convertedValues.set('modeOfTransportation', new NameUuid(convertedValues.get('modeOfTransportation')));

                // modeOfTransportationDropdown
                convertedValues = convertedValues.set('modeOfTransportationDropdown', List(convertedValues.get('modeOfTransportationDropdown', []).map(value => new ServiceCategories(value))));

                // modeOfTransportationAccelerator
                convertedValues = convertedValues.set('modeOfTransportationAccelerator', List(convertedValues.get('modeOfTransportationAccelerator', []).map(value => new ReservationAccelerator(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
