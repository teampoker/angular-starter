import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

export interface IReservationPreferredTransportationState {
    transportationProvider  : INameUuid;
    isFormValid             : boolean;
    transportationProviders : Array<INameUuid>;
}

export const RESERVATION_PREFERRED_TRANSPORTATION_STATE = Record({
    transportationProvider   : new NameUuid(),
    isFormValid              : false,
    transportationProviders  : List<NameUuid>()
});

export class ReservationPreferredTransportationState extends RESERVATION_PREFERRED_TRANSPORTATION_STATE {
    transportationProvider   : NameUuid;
    isFormValid              : boolean;
    transportationProviders  : List<NameUuid>;

    constructor(values? : ReservationPreferredTransportationState | IReservationPreferredTransportationState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationPreferredTransportationState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // transportationProvider
                convertedValues = convertedValues.set('transportationProvider', new NameUuid(convertedValues.get('transportationProvider')));

                // transportationProviders
                convertedValues = convertedValues.set('transportationProviders', List(convertedValues.get('transportationProviders', []).map(value => new NameUuid(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
