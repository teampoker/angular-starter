import {
    fromJS,
    Map,
    Record
} from 'immutable';
import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

export interface IReservationRuleResponse {
    message             : string;
    status              : string;
    queueTaskItemType   : INameUuid;
    serviceCoveragePlan : INameUuid;
}

export const RESERVATION_RULE_RESPONSE = Record({
    message             : '',
    status              : '',
    queueTaskItemType   : new NameUuid(),
    serviceCoveragePlan : new NameUuid()
});

export class ReservationRuleResponse extends RESERVATION_RULE_RESPONSE {
    message             : string;
    status              : string;
    queueTaskItemType   : NameUuid;
    serviceCoveragePlan : NameUuid;

    constructor(values? : ReservationRuleResponse | IReservationRuleResponse) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationRuleResponse) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // queueTaskItemType
                convertedValues = convertedValues.set('queueTaskItemType', new NameUuid(convertedValues.get('queueTaskItemType')));

                // serviceCoveragePlan
                convertedValues = convertedValues.set('serviceCoveragePlan', new NameUuid(convertedValues.get('serviceCoveragePlan')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
