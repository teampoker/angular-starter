import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryAddress,
    BeneficiaryAddress
} from '../../Beneficiary/types/beneficiary-address.model';
import {
    IBeneficiaryEmail,
    BeneficiaryEmail
} from '../../Beneficiary/types/beneficiary-email.model';
import {
    IBeneficiaryPhone,
    BeneficiaryPhone
} from '../../Beneficiary/types/beneficiary-phone.model';
import {
    IBeneficiarySpecialRequirement,
    BeneficiarySpecialRequirement
} from '../../Beneficiary/types/beneficiary-special-requirement.model';

export enum EnumReservationHeaderType {
    MAKE_A_RESERVATION
}

export interface IReservationHeader {
    title                  : string;
    headerType             : EnumReservationHeaderType;
    firstName              : string;
    middleName             : string;
    lastName               : string;
    birthDate              : string;
    primaryAddress         : IBeneficiaryAddress;
    primaryEmail           : IBeneficiaryEmail;
    primaryPhone           : IBeneficiaryPhone;
    specialRequirements    : Array<IBeneficiarySpecialRequirement>;

}

export const BENEFICIARY_HEADER = Record({
    title                  : '',
    headerType             : EnumReservationHeaderType.MAKE_A_RESERVATION,
    firstName              : '',
    middleName             : '',
    lastName               : '',
    birthDate              : '',
    primaryAddress         : new BeneficiaryAddress(),
    primaryEmail           : new BeneficiaryEmail(),
    primaryPhone           : new BeneficiaryPhone(),
    specialRequirements    : List<BeneficiarySpecialRequirement>()
});

export class ReservationHeader extends BENEFICIARY_HEADER {
    title                  : string;
    headerType             : EnumReservationHeaderType;
    firstName              : string;
    middleName             : string;
    lastName               : string;
    birthDate              : string;
    primaryAddress         : BeneficiaryAddress;
    primaryEmail           : BeneficiaryEmail;
    primaryPhone           : BeneficiaryPhone;
    specialRequirements    : Array<IBeneficiarySpecialRequirement>;

    constructor(values? : ReservationHeader | IReservationHeader) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationHeader) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // primaryAddress
                convertedValues = convertedValues.set('primaryAddress', new BeneficiaryAddress(convertedValues.get('primaryAddress')));

                // primaryEmail
                convertedValues = convertedValues.set('primaryEmail', new BeneficiaryEmail(convertedValues.get('primaryEmail')));

                // primaryPhone
                convertedValues = convertedValues.set('primaryPhone', new BeneficiaryPhone(convertedValues.get('primaryPhone')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
