import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    fromStorage as requirementFromStorage,
    toApi as requirementToApi,
    BeneficiarySpecialRequirement,
    IBeneficiarySpecialRequirement
} from '../../Beneficiary/types/beneficiary-special-requirement.model';

export interface IReservationPassengerState {
    connectionType? : string;
    name : string;
    specialRequirements : List<IBeneficiarySpecialRequirement>;
    tempSpecialRequirements? : List<IBeneficiarySpecialRequirement>;
    uuid : string;
}

export const RESERVATION_PASSENGER_STATE = Record({
    connectionType : null,
    name : null,
    specialRequirements : List<BeneficiarySpecialRequirement>(),
    tempSpecialRequirements : List<BeneficiarySpecialRequirement>(),
    uuid : null
});

export class ReservationPassengerState extends RESERVATION_PASSENGER_STATE {
    connectionType? : string;
    name : string;
    specialRequirements : List<BeneficiarySpecialRequirement>;
    tempSpecialRequirements? : List<BeneficiarySpecialRequirement>;
    uuid : string;

    constructor(values? : ReservationPassengerState | IReservationPassengerState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationPassengerState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * transform IReservationPassengerState model into a ReservationPassengerState Record
 *
 * @param {IReservationPassengerState} pojo
 *
 * @returns {ReservationPassengerState}
 */
export function fromStorage(pojo : IReservationPassengerState) : ReservationPassengerState {
    let state = new ReservationPassengerState(pojo);

    state = state.set(
        'specialRequirements',
        List(Array.isArray(pojo.specialRequirements) ? pojo.specialRequirements.map(requirementFromStorage) : [])
    ) as ReservationPassengerState;

    state = state.set(
        'tempSpecialRequirements',
        List(Array.isArray(pojo.tempSpecialRequirements) ? pojo.tempSpecialRequirements.map(requirementFromStorage) : [])
    ) as ReservationPassengerState;

    return state;
}

/**
 * transform Immutable ReservationPassengerState model into
 * appropriate POJO for processing by API endpoints
 *
 * @param {ReservationPassengerState} passenger Immutable model to transform
 *
 * @returns {{ personUuid : string, passengerSpecialRequirements : Array<{ specialRequirementTypeUuid : string }> }} POJO representation
 */
export function toApi(passenger : ReservationPassengerState) : { personUuid : string, passengerSpecialRequirements : Array<{ specialRequirementTypeUuid : string }> } {
    return {
        personUuid : passenger.get('uuid'),
        passengerSpecialRequirements : passenger
            .get('specialRequirements')
            .map(requirementToApi)
            .toJS()
    };
}
