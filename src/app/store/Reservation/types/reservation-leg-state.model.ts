import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';
import {isObject} from 'rxjs/util/isObject';
import * as moment from 'moment';
import * as timezone from 'moment-timezone';

import {
    fromApi as beneficiaryRequirementFromApi,
    toApi as beneficiaryRequirementToApi,
    BeneficiarySpecialRequirement,
    IBeneficiarySpecialRequirement
} from '../../Beneficiary/types/beneficiary-special-requirement.model';
import {
    fromApi as addressLocationFromApi,
    toApi as addressLocationToApi,
    AddressLocation,
    IAddressLocation
} from '../../types/address-location.model';
import {
    DateTime,
    IDateTime
} from '../../types/date-time.model';
import {
    toApi as nameUuidToApi,
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';
import {
    toApi as passengerToApi,
    IReservationPassengerState,
    ReservationPassengerState
} from './reservation-passenger';
import {
    fromApi as passengerFromApi,
    ITransportationItemPassenger,
    TransportationItemPassenger
} from '../../types/transportation-item-passenger.model';
import {
    fromApi as requirementFromApi,
    ITransportationItemSpecialRequirement,
    TransportationItemSpecialRequirement
} from '../../types/transportation-item-special-requirement.model';
import {
    fromApi as personFromApi,
    IPerson,
    Person
} from '../../types/person.model';
import {
    IReservationStatus,
    ReservationStatus
} from './reservation-status.model';
import {
    fromStorage as routeFromStorage,
    IMassTransitRoute,
    MassTransitRoute
} from '../../types/mass-transit-route.model';

export interface IReservationLegState {
    uuid?                : string;
    requestedBy          : IReservationPassengerState;
    treatmentType        : INameUuid;
    reservationDateTime  : IDateTime;
    pickupLocation       : IAddressLocation;
    dropOffLocation      : IAddressLocation;
    modeOfTransportation : INameUuid;
    driver?              : IPerson;
    driverInfo           : IReservationPassengerState;
    additionalPassengers : Array<IReservationPassengerState>;
    serviceCoveragePlan? : INameUuid;
    transportationItemPassengers? : Array<ITransportationItemPassenger>;
    transportationItemSpecialRequirements? : Array<ITransportationItemSpecialRequirement>;
    specialRequirements  : Array<IBeneficiarySpecialRequirement>;
    pickupTime           : Date;
    legDistance          : number;
    legDuration          : number;
    version?             : number;
    status?              : IReservationStatus;
    type                 : string;
    createdOn            : string;
    createdBy            : string;
    updatedOn            : string;
    updatedBy            : string;
    ordinality           : number;
    eventComment         : string | null;
    eventReasonUuid      : string | null;
    massTransitRoutes    : Array<IMassTransitRoute>;
    exceptionReasons?    : Array<INameUuid>;
}

export const RESERVATION_LEG_STATE = Record({
    uuid                 : '',
    requestedBy          : new ReservationPassengerState(),
    treatmentType        : new NameUuid(),
    reservationDateTime  : new DateTime(),
    pickupLocation       : new AddressLocation(),
    dropOffLocation      : new AddressLocation(),
    modeOfTransportation : new NameUuid(),
    driver               : new Person(),
    driverInfo           : new ReservationPassengerState(),
    additionalPassengers : List<ReservationPassengerState>(),
    serviceCoveragePlan  : new NameUuid(),
    transportationItemPassengers : List<TransportationItemPassenger>(),
    transportationItemSpecialRequirements : List<TransportationItemSpecialRequirement>(),
    specialRequirements  : List<BeneficiarySpecialRequirement>(),
    pickupTime           : new Date(),
    legDistance          : 0,
    legDuration          : 0,
    version              : null,
    status               : new ReservationStatus(),
    type                 : '',
    createdOn            : '',
    createdBy            : '',
    updatedOn            : '',
    updatedby            : '',
    ordinality           : 0,
    eventComment         : undefined,
    eventReasonUuid      : undefined,
    massTransitRoutes    : List<MassTransitRoute>(),
    exceptionReasons     : List<NameUuid>()
});

export class ReservationLegState extends RESERVATION_LEG_STATE {
    uuid?                : string;
    requestedBy          : ReservationPassengerState;
    treatmentType        : NameUuid;
    reservationDateTime  : DateTime;
    pickupLocation       : AddressLocation;
    dropOffLocation      : AddressLocation;
    modeOfTransportation : NameUuid;
    driver?              : Person;
    driverInfo           : ReservationPassengerState;
    additionalPassengers : List<ReservationPassengerState>;
    serviceCoveragePlan? : NameUuid;
    transportationItemPassengers? : List<TransportationItemPassenger>;
    transportationItemSpecialRequirements? : List<TransportationItemSpecialRequirement>;
    specialRequirements  : List<BeneficiarySpecialRequirement>;
    pickupTime           : Date;
    legDistance          : number;
    legDuration          : number;
    version?             : number;
    status?              : ReservationStatus;
    type                 : string;
    createdOn            : string;
    createdBy            : string;
    updatedOn            : string;
    updatedBy            : string;
    ordinality           : number;
    eventComment         : string | null;
    eventReasonUuid      : string | null;
    massTransitRoutes    : List<MassTransitRoute>;
    exceptionReasons?    : List<NameUuid>;

    constructor(values? : ReservationLegState | IReservationLegState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationLegState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // requestedBy
                convertedValues = convertedValues.set('requestedBy', new ReservationPassengerState(convertedValues.get('requestedBy')));

                // treatmentType
                convertedValues = convertedValues.set('treatmentType', new NameUuid(convertedValues.get('treatmentType')));

                // reservationDateTime
                convertedValues = convertedValues.set('reservationDateTime', new DateTime(convertedValues.get('reservationDateTime')));

                // pickupLocation
                convertedValues = convertedValues.set('pickupLocation', new AddressLocation(convertedValues.get('pickupLocation')));

                // dropOff
                convertedValues = convertedValues.set('dropOffLocation', new AddressLocation(convertedValues.get('dropOffLocation')));

                // modeOfTransportation
                convertedValues = convertedValues.set('modeOfTransportation', new NameUuid(convertedValues.get('modeOfTransportation')));

                // driver
                convertedValues = convertedValues.set('driver', new Person(convertedValues.get('driver')));

                // driverInfo
                convertedValues = convertedValues.set('driverInfo', new ReservationPassengerState(convertedValues.get('driverInfo')));

                // additionalPassengers
                convertedValues = convertedValues.set('additionalPassengers', List(convertedValues.get('additionalPassengers', []).map(value => new ReservationPassengerState(value))));

                // serviceCoveragePlan
                convertedValues = convertedValues.set('serviceCoveragePlan', new NameUuid(convertedValues.get('serviceCoveragePlan')));

                // transportationItemPassengers
                convertedValues = convertedValues.set(
                    'transportationItemPassengers',
                    convertedValues.get('transportationItemPassengers', List())
                        .map(value => new TransportationItemPassenger(value))
                );

                // transportationItemSpecialRequirements
                convertedValues = convertedValues.set(
                    'transportationItemSpecialRequirements',
                    convertedValues.get('transportationItemSpecialRequirements', List())
                        .map(value => new TransportationItemSpecialRequirement(value))
                );

                // specialRequirements
                convertedValues = convertedValues.set('specialRequirements', List(convertedValues.get('specialRequirements', []).map(value => new BeneficiarySpecialRequirement(value))));

                // status
                convertedValues = convertedValues.set('status', new ReservationStatus(convertedValues.get('status')));

                convertedValues = convertedValues.set(
                    'massTransitRoutes',
                    convertedValues.get('massTransitRoutes', List()).map(routeFromStorage)
                );

                // exceptionReasons
                convertedValues = convertedValues.set('exceptionReasons',  List(convertedValues.get('exceptionReasons', []).map(value => new NameUuid(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * Parses a leg API response into a Leg state model
 *
 * @param {any} leg reservation leg
 *
 * @returns {ReservationLegState}
 */
export function fromApi(leg : any) : ReservationLegState {
    const legState : any = {};

    if (isObject(leg) && !Array.isArray(leg)) {
        legState.uuid        = leg.uuid;
        legState.legDistance = leg.distance;
        legState.legDuration = leg.duration;
        legState.version     = leg.version;
        legState.status      = new ReservationStatus(leg.status);
        legState.type        = leg.type;
        legState.createdOn   = moment.utc(leg.createdOn, 'YYYY-MM-DDTHH:mm:ssZ[UTC]').toISOString();
        legState.createdBy   = leg.createdBy;
        legState.updatedOn   = moment.utc(leg.updatedOn, 'YYYY-MM-DDTHH:mm:ssZ[UTC]').toISOString();
        legState.updatedBy   = leg.updatedBy;
        legState.ordinality  = leg.ordinality;
        legState.eventComment = leg.eventComment;
        legState.eventReasonUuid = leg.eventReasonUuid;
        legState.pickupLocation = addressLocationFromApi(leg.pickUpLocation);
        legState.dropOffLocation = addressLocationFromApi(leg.dropOffLocation);

        if (leg.pickUpOn) {
            legState.pickupTime = leg.pickUpOn.replace('[UTC]', '');
        }

        if (leg.requestedBy) {
            legState.requestedBy = new ReservationPassengerState({
                connectionType : leg.requestedBy.connections && Array.isArray(leg.requestedBy.connections) && leg.requestedBy.connections[0] ? leg.requestedBy.connections[0].name : '',
                name: leg.requestedBy.firstName ? `${leg.requestedBy.firstName} ${leg.requestedBy.lastName}` : '',
                specialRequirements : List<BeneficiarySpecialRequirement>(),
                uuid: leg.requestedBy.uuid
            });
        }
        else {
            legState.requestedBy = new NameUuid({
                name : 'Self',
                uuid : leg.requestedByPersonUuid
            });
        }

        if (leg.treatmentType) {
            legState.treatmentType = new NameUuid({
                name : leg.treatmentType.name,
                uuid : leg.treatmentType.uuid
            });
        }

        if (leg.modeOfTransportation) {
            legState.modeOfTransportation = new NameUuid({
                name : leg.modeOfTransportation.name,
                uuid : leg.modeOfTransportation.uuid
            });
        }

        legState.additionalPassengers = [];
        legState.transportationItemPassengers = [];

        if (Array.isArray(leg.transportationItemPassengers)) {
            legState.additionalPassengers = leg.transportationItemPassengers
                .map(passenger => new ReservationPassengerState({
                    connectionType : passenger.person.connections && Array.isArray(passenger.person.connections) ? passenger.person.connections[0].name : '',
                    name : `${passenger.person.firstName} ${passenger.person.lastName}`,
                    specialRequirements : passenger.passengerSpecialRequirements.map(beneficiaryRequirementFromApi),
                    uuid : passenger.personUuid
                }));

            legState.transportationItemPassengers = List(
                leg.transportationItemPassengers.map(passengerFromApi)
            );
        }

        legState.specialRequirements = [];
        legState.transportationItemSpecialRequirements = [];

        if (Array.isArray(leg.transportationItemSpecialRequirements)) {
            legState.specialRequirements = leg.transportationItemSpecialRequirements
                .map(beneficiaryRequirementFromApi);

            legState.transportationItemSpecialRequirements = List(
                leg.transportationItemSpecialRequirements.map(requirementFromApi)
            );
        }

        if (leg.appointmentOn) {
            legState.reservationDateTime = new DateTime({
                date : leg.appointmentOn,
                time : leg.appointmentOn
            });
        }

        legState.reservationDistance = leg.distance;

        if (leg.driver) {
            legState.driverInfo = new ReservationPassengerState({
                connectionType : leg.driver.connections && Array.isArray(leg.driver.connections) && leg.driver.connections[0] ? leg.driver.connections[0].name : '',
                name : leg.driver.firstName ? `${leg.driver.firstName} ${leg.driver.lastName}` : '',
                specialRequirements : List<BeneficiarySpecialRequirement>(),
                uuid : leg.driver.uuid
            });

            legState.driver = personFromApi(leg.driver);
        }
    }

    return new ReservationLegState(legState);
}

/**
 * transform Immutable Reservation Leg model into
 * appropriate POJO for processing by API endpoints
 *
 * @param {ReservationLegState} leg
 *
 * @returns {any} POJO representation
 */
export function toApi(leg : ReservationLegState) : any {
    const pojo : any = {};

    pojo.exceptionQueueTaskItemTypes = leg.get('exceptionReasons').map(nameUuidToApi).toJS();
    pojo.distance = leg.get('legDistance');
    pojo.driver = leg.get('driverInfo');
    pojo.dropOffLocation = addressLocationToApi(leg.get('dropOffLocation'));
    pojo.duration = leg.get('legDuration');
    pojo.eventComment = leg.get('eventComment');
    pojo.eventReasonUuid = leg.get('eventReasonUuid');
    pojo.modeOfTransportation = nameUuidToApi(leg.get('modeOfTransportation'), true);
    pojo.ordinality = leg.get('ordinality');
    pojo.pickUpLocation = addressLocationToApi(leg.get('pickupLocation'));
    pojo.requestedByPersonUuid = leg.getIn(['requestedBy', 'uuid']);
    pojo.treatmentType = nameUuidToApi(leg.get('treatmentType'), true);
    pojo.transportationItemPassengers = leg.get('additionalPassengers').map(passengerToApi).toJS();
    pojo.transportationItemSpecialRequirements = leg.get('specialRequirements').map(beneficiaryRequirementToApi).toJS();
    pojo.type = 'Transportation';
    pojo.uuid = leg.get('uuid');
    pojo.version = leg.get('version');
    pojo.serviceCoveragePlanUuid = leg.getIn(['serviceCoveragePlan', 'uuid']);

    pojo.pickUpOnTime = {
        dateTime : leg.get('pickupTime'),
        timezone : timezone.tz(timezone.tz.guess()).format('z')
    };

    pojo.appointmentOnTime = {
        dateTime : leg.getIn(['reservationDateTime', 'time']),
        timezone : timezone.tz(timezone.tz.guess()).format('z')
    };

    return pojo;
}
