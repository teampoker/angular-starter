import {
    fromApi,
    Reservation
} from './reservation.model';
import * as ReservationLegState from './reservation-leg-state.model';
import {RESERVATION_MOCK} from '../../../areas/Reservation/services/Reservation/reservation.service.mock';

describe('Model: Reservation', () => {
    it('should not throw when supplied an empty object at instantiation', () => {
        expect(() => new (Reservation as any)({})).not.toThrow();
    });

    describe('fromApi', () => {
        describe('Invalid Input', () => {
            const invalidFromApiCalls = [
                () => fromApi(undefined),
                () => fromApi(null),
                () => fromApi(true),
                () => fromApi('string'),
                () => fromApi(123),
                () => fromApi(() => {}),
                () => fromApi([1, 2, 3])
            ];
            it('should not throw when an invalid argument is passed', () => {
                // since this function is handling API responses we cannot rely on
                // type checking to catch errors when transpiling, so we have to
                // do runtime input validation
                invalidFromApiCalls.forEach(call => expect(call).not.toThrow());
            });
            it('should return a default reservation state on invalid input', () => {
                const controlState = new Reservation();

                invalidFromApiCalls.forEach(call => {
                    const testState = call();

                    expect(controlState.equals(testState)).toEqual(true);
                });
            });
        });
        describe('Valid Input', () => {
            describe('getReservations Response', () => {
                const mockReservation = RESERVATION_MOCK['getReservations'][0];
                const result = fromApi(mockReservation);

                it('should return a Reservation instance', () => {
                    expect(result instanceof Reservation).toBe(true);
                });
                it('should have the correct uuid', () => {
                    expect(result.get('uuid')).toEqual(mockReservation.uuid);
                });
                it('should have the correct personUuid', () => {
                    expect(result.get('personUuid')).toEqual(mockReservation.personUuid);
                });
                it('should have the correct status', () => {
                    expect(result.getIn(['status', 'id'])).toEqual(mockReservation.status.id);
                    expect(result.getIn(['status', 'name'])).toEqual(mockReservation.status.name);
                });
                it('should call the Reservation Leg State fromApi method', () => {
                    spyOn(ReservationLegState, 'fromApi');

                    fromApi(mockReservation);

                    expect(ReservationLegState.fromApi).toHaveBeenCalled();
                    expect((ReservationLegState.fromApi as any).calls.count()).toEqual(mockReservation.items.length);
                });
            });

            describe('getReservation Response', () => {
                const mockReservation = RESERVATION_MOCK['getReservation'][0];
                const result = fromApi(mockReservation);

                it('should return a Reservation instance', () => {
                    expect(result instanceof Reservation).toBe(true);
                });
                it('should have the correct uuid', () => {
                    expect(result.get('uuid')).toEqual(mockReservation.uuid);
                });
                it('should have the correct personUuid', () => {
                    expect(result.get('personUuid')).toEqual(mockReservation.personUuid);
                });
                it('should have the correct status', () => {
                    expect(result.getIn(['status', 'id'])).toEqual(mockReservation.status.id);
                    expect(result.getIn(['status', 'name'])).toEqual(mockReservation.status.name);
                });
                it('should call the Reservation Leg State fromApi method', () => {
                    spyOn(ReservationLegState, 'fromApi');

                    fromApi(mockReservation);

                    expect(ReservationLegState.fromApi).toHaveBeenCalled();
                    expect((ReservationLegState.fromApi as any).calls.count()).toEqual(mockReservation.items.length);
                });
            });
        });
    });
});
