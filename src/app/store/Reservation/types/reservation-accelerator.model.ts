import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IReservationAccelerator {
    uuid            : string;
    name            : string;
    connectionType  : string;
    addAConnection  : boolean;
}

export const RESERVATION_ACCELERATOR = Record({
    uuid            : '',
    name            : 'PLUS_ADD_A_CONNECTION',
    connectionType  : '',
    addAConnection  : true
});

export class ReservationAccelerator extends RESERVATION_ACCELERATOR {
    uuid            : string;
    name            : string;
    connectionType  : string;
    addAConnection  : boolean;

    constructor(values? : ReservationAccelerator | IReservationAccelerator) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationAccelerator) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
