import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IReservationStatus {
    id    : number;
    name  : string;
}

const RESERVATION_STATUS = Record({
    id    : 0,
    name  : ''
});

export class ReservationStatus extends RESERVATION_STATUS {
    id    : number;
    name  : string;

    constructor(values? : ReservationStatus | IReservationStatus) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationStatus) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }
}
