import {
    fromJS,
    Map,
    List,
    Record
} from 'immutable';
import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

import {
    IReservationRuleResponse,
    ReservationRuleResponse
} from './reservation-rule-response.model';

export interface IReservationBusinessRules {
    treatmentType                      : Array<IReservationRuleResponse>;
    location                           : Array<IReservationRuleResponse>;
    escort                             : Array<IReservationRuleResponse>;
    modeOfTransportation               : Array<IReservationRuleResponse>;
    treatmentTypeOverrideReason        : Array<INameUuid>;
    locationOverrideReason             : Array<INameUuid>;
    escortOverrideReason               : Array<INameUuid>;
    modeOfTransportationOverrideReason : Array<INameUuid>;
}

export const RESERVATION_BUSINESS_RULES_STATE = Record({
    treatmentType                      : List<ReservationRuleResponse>(),
    location                           : List<ReservationRuleResponse>(),
    escort                             : List<ReservationRuleResponse>(),
    modeOfTransportation               : List<ReservationRuleResponse>(),
    treatmentTypeOverrideReason        : List<NameUuid>(),
    locationOverrideReason             : List<NameUuid>(),
    escortOverrideReason               : List<NameUuid>(),
    modeOfTransportationOverrideReason : List<NameUuid>()
});

export class ReservationBusinessRules extends RESERVATION_BUSINESS_RULES_STATE {
    treatmentType                      : List<ReservationRuleResponse>;
    location                           : List<ReservationRuleResponse>;
    escort                             : List<ReservationRuleResponse>;
    modeOfTransportation               : List<ReservationRuleResponse>;
    treatmentTypeOverrideReason        : List<NameUuid>;
    locationOverrideReason             : List<NameUuid>;
    escortOverrideReason               : List<NameUuid>;
    modeOfTransportationOverrideReason : List<NameUuid>;

    constructor(values? : ReservationBusinessRules | IReservationBusinessRules) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationBusinessRules) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // treatmentType
                convertedValues = convertedValues.set('treatmentType', List(convertedValues.get('treatmentType', []).map(value => new ReservationRuleResponse(value))));

                // location
                convertedValues = convertedValues.set('location', List(convertedValues.get('location', []).map(value => new ReservationRuleResponse(value))));

                // escort
                convertedValues = convertedValues.set('escort', List(convertedValues.get('escort', []).map(value => new ReservationRuleResponse(value))));

                // modeOfTransportation
                convertedValues = convertedValues.set('modeOfTransportation', List(convertedValues.get('modeOfTransportation', []).map(value => new ReservationRuleResponse(value))));

                // treatmentTypeOverrideReason
                convertedValues = convertedValues.set('treatmentTypeOverrideReason', List(convertedValues.get('treatmentTypeOverrideReason', []).map(value => new NameUuid(value))));

                // locationOverrideReason
                convertedValues = convertedValues.set('locationOverrideReason', List(convertedValues.get('locationOverrideReason', []).map(value => new NameUuid(value))));

                // escortOverrideReason
                convertedValues = convertedValues.set('escortOverrideReason', List(convertedValues.get('escortOverrideReason', []).map(value => new NameUuid(value))));

                // modeOfTransportationOverrideReason
                convertedValues = convertedValues.set('modeOfTransportationOverrideReason', List(convertedValues.get('modeOfTransportationOverrideReason', []).map(value => new NameUuid(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
