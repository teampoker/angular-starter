import {
    List,
    Record
} from 'immutable';

import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

export interface IReservationEventReasons {
    cancelReasons : Array<INameUuid>;
    updateReasons : Array<INameUuid>;
}

export const ReservationEventReasonsRecord = Record({
    cancelReasons : List<NameUuid>(),
    updateReasons : List<NameUuid>()
});

export class ReservationEventReasons extends ReservationEventReasonsRecord {
    cancelReasons : List<NameUuid>;
    updateReasons : List<NameUuid>;
}

/**
 * transform raw JSON from an api response into
 * ReservationEventReasons Immutable Record type
 *
 * @param {IReservationEventReasons} pojo
 *
 * @returns {ReservationEventReasons}
 */
export function fromApi(pojo : IReservationEventReasons) : ReservationEventReasons {
    return fromStorage(pojo);
}

/**
 * transform raw JSON from local storage into
 * ReservationEventReasons Immutable Record type
 *
 * @param {IReservationEventReasons} pojo
 *
 * @returns {ReservationEventReasons}
 */
export function fromStorage(pojo : IReservationEventReasons) : ReservationEventReasons {
    return new ReservationEventReasons(pojo).withMutations(record => record
        .set('cancelReasons', List(record.get('cancelReasons', []).map(reason => new NameUuid(reason))))
        .set('updateReasons', List(record.get('updateReasons', []).map(reason => new NameUuid(reason))))
    ) as ReservationEventReasons;
}
