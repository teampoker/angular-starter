import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

export interface IReservationTreatmentTypeState {
    isFormValid               : boolean;
    treatmentType             : INameUuid;
    treatmentTypesAccelerator : Array<INameUuid>;
}

export const RESERVATION_TREATMENT_TYPE_STATE = Record({
    isFormValid               : false,
    treatmentType             : new NameUuid(),
    treatmentTypesAccelerator : List<NameUuid>()
});

export class ReservationTreatmentTypeState extends RESERVATION_TREATMENT_TYPE_STATE {
    isFormValid               : boolean;
    treatmentType             : NameUuid;
    treatmentTypesAccelerator : List<NameUuid>;

    constructor(values? : ReservationTreatmentTypeState | IReservationTreatmentTypeState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationTreatmentTypeState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // treatmentType
                convertedValues = convertedValues.set('treatmentType', new NameUuid(convertedValues.get('treatmentType')));

                // treatmentTypesAccelerator
                convertedValues = convertedValues.set('treatmentTypesAccelerator', List(convertedValues.get('treatmentTypesAccelerator', []).map(value => new NameUuid(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
