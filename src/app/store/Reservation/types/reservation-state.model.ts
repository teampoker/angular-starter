import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';
import {
    IReservationRequestedByState,
    ReservationRequestedByState
} from './reservation-requested-by-state.model';
import {
    IReservationTreatmentTypeState,
    ReservationTreatmentTypeState
} from './reservation-treatment-type-state.model';
import {
    IReservationAppointmentDateState,
    ReservationAppointmentDateState
} from './reservation-appointment-date-state.model';
import {
    IReservationAdditionalPassengersState,
    ReservationAdditionalPassengersState
} from './reservation-additional-passengers-state.model';
import {
    IReservationModeOfTransportationState,
    ReservationModeOfTransportationState
} from './reservation-mode-of-transportation-state.model';
import {
    IReservationPreferredTransportationState,
    ReservationPreferredTransportationState
} from './reservation-preferred-transportation-state.model';
import {
    IReservationPickupTimeState,
    ReservationPickupTimeState
} from './reservation-pickup-time-state.model';
import {
    IReservationLegState,
    ReservationLegState
} from './reservation-leg-state.model';
import {
    IAddressLocation,
    AddressLocation
} from '../../types/address-location.model';
import {
    IBeneficiarySpecialRequirement,
    BeneficiarySpecialRequirement
} from '../../Beneficiary/types/beneficiary-special-requirement.model';
import {
    IReservationTripIndicatorState,
    ReservationTripIndicatorState
} from './reservation-trip-indicator-state.model';
import {
    IReservation,
    Reservation
} from './reservation.model';
import {
    IReservationCardsState,
    ReservationCardsState
} from './reservation-cards-state.model';
import {
    IReservationHistoryState,
    ReservationHistoryState
} from './reservation-history-state.model';
import {
    IReservationBusinessRules,
    ReservationBusinessRules
} from './reservation-business-rules-state.model';
import {
    fromStorage as eventReasonsFromStorage,
    IReservationEventReasons,
    ReservationEventReasons
} from './reservation-event-reasons.model';

export interface IReservationState {
    isEditing                                : boolean;
    showReasonSelection                      : boolean;
    saveLeg                                  : boolean;
    editLeg                                  : number;
    deleteLeg                                : number;
    serviceCoveragePlan?                     : INameUuid;
    confirmationNumber                       : string;
    connectionRequestIssuedBy                : 'REQUESTED_BY' | 'PASSENGER' | 'DRIVER';
    reservationEventReasons                  : IReservationEventReasons;
    reservationHistory                       : IReservationHistoryState;
    reservationTreatmentTypeState            : IReservationTreatmentTypeState;
    reservationRequestedByState              : IReservationRequestedByState;
    reservationAppointmentDateState          : IReservationAppointmentDateState;
    reservationPickupLocationState           : IAddressLocation;
    reservationDropOffLocationState          : IAddressLocation;
    reservationAdditionalPassengersState     : IReservationAdditionalPassengersState;
    reservationModeOfTransportationState     : IReservationModeOfTransportationState;
    reservationPreferredTransportationState  : IReservationPreferredTransportationState;
    reservationPickupTimeState               : IReservationPickupTimeState;
    reservationLegState                      : Array<IReservationLegState>;
    reservationSpecialRequirementState       : Array<IBeneficiarySpecialRequirement>;
    reservationSpecialRequirementTempState   : Array<IBeneficiarySpecialRequirement>;
    reservationTripIndicatorState            : IReservationTripIndicatorState;
    reservation                              : IReservation;
    reservations                             : Array<IReservation>;
    reservationCardsState                    : IReservationCardsState;
    reservationBusinessRules                 : IReservationBusinessRules;
    recentPickupLocations                    : Array<IAddressLocation>;
    recentDropoffLocations                   : Array<IAddressLocation>;
}

export const RESERVATION_STATE = Record({
    isEditing                                : false,
    showReasonSelection                      : false,
    saveLeg                                  : false,
    editLeg                                  : null,
    deleteLeg                                : null,
    serviceCoveragePlan                      : null,
    confirmationNumber                       : '',
    connectionRequestIssuedBy                : '',
    reservationEventReasons                  : new ReservationEventReasons(),
    reservationHistory                       : new ReservationHistoryState(),
    reservationTreatmentTypeState            : new ReservationTreatmentTypeState(),
    reservationRequestedByState              : new ReservationRequestedByState(),
    reservationAppointmentDateState          : new ReservationAppointmentDateState(),
    reservationPickupLocationState           : new AddressLocation(),
    reservationDropOffLocationState          : new AddressLocation(),
    reservationAdditionalPassengersState     : new ReservationAdditionalPassengersState(),
    reservationModeOfTransportationState     : new ReservationModeOfTransportationState(),
    reservationPreferredTransportationState  : new ReservationPreferredTransportationState(),
    reservationPickupTimeState               : new ReservationPickupTimeState(),
    reservationLegState                      : List<ReservationLegState>(),
    reservationSpecialRequirementState       : List<BeneficiarySpecialRequirement>(),
    reservationSpecialRequirementTempState   : List<BeneficiarySpecialRequirement>(),
    reservationTripIndicatorState            : new ReservationTripIndicatorState(),
    reservation                              : new Reservation(),
    reservations                             : List<Reservation>(),
    reservationCardsState                    : new ReservationCardsState(),
    reservationBusinessRules                 : new ReservationBusinessRules(),
    recentPickupLocations                    : List<AddressLocation>(),
    recentDropoffLocations                   : List<AddressLocation>()
});

/**
 * type definition for Redux Store reservation state
 */
export class ReservationState extends RESERVATION_STATE {
    isEditing                                : boolean;
    showReasonSelection                      : boolean;
    saveLeg                                  : boolean;
    editLeg                                  : number;
    deleteLeg                                : number;
    serviceCoveragePlan?                     : NameUuid;
    confirmationNumber                       : string;
    connectionRequestIssuedBy                : 'REQUESTED_BY' | 'PASSENGER' | 'DRIVER';
    reservationEventReasons                  : ReservationEventReasons;
    reservationHistory                       : ReservationHistoryState;
    reservationTreatmentTypeState            : ReservationTreatmentTypeState;
    reservationRequestedByState              : ReservationRequestedByState;
    reservationAppointmentDateState          : ReservationAppointmentDateState;
    reservationPickupLocationState           : AddressLocation;
    reservationDropOffLocationState          : AddressLocation;
    reservationAdditionalPassengersState     : ReservationAdditionalPassengersState;
    reservationModeOfTransportationState     : ReservationModeOfTransportationState;
    reservationPreferredTransportationState  : ReservationPreferredTransportationState;
    reservationPickupTimeState               : ReservationPickupTimeState;
    reservationLegState                      : List<ReservationLegState>;
    reservationSpecialRequirementState       : List<BeneficiarySpecialRequirement>;
    reservationSpecialRequirementTempState   : List<BeneficiarySpecialRequirement>;
    reservationTripIndicatorState            : ReservationTripIndicatorState;
    reservation                              : Reservation;
    reservations                             : List<Reservation>;
    reservationCardsState                    : ReservationCardsState;
    reservationBusinessRules                 : ReservationBusinessRules;
    recentPickupLocations                    : List<AddressLocation>;
    recentDropoffLocations                   : List<AddressLocation>;

    constructor(values? : ReservationState | IReservationState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoveragePlan
                convertedValues = convertedValues.set('serviceCoveragePlan', new NameUuid(convertedValues.get('serviceCoveragePlan')));

                // reservationHistory
                convertedValues = convertedValues.set(
                    'reservationHistory',
                    new ReservationHistoryState(convertedValues.get('reservationHistory'))
                );

                // reservationEventReasons
                convertedValues = convertedValues.set(
                    'reservationEventReasons',
                    eventReasonsFromStorage(convertedValues.get('reservationEventReasons'))
                );

                // reservationTreatmentTypeState
                convertedValues = convertedValues.set('reservationTreatmentTypeState', new ReservationTreatmentTypeState(convertedValues.get('reservationTreatmentTypeState')));

                // reservationRequestedByState
                convertedValues = convertedValues.set('reservationRequestedByState', new ReservationRequestedByState(convertedValues.get('reservationRequestedByState')));

                // reservationAppointmentDateState
                convertedValues = convertedValues.set('reservationAppointmentDateState', new ReservationAppointmentDateState(convertedValues.get('reservationAppointmentDateState')));

                // reservationPickupLocationState
                convertedValues = convertedValues.set('reservationPickupLocationState', new AddressLocation(convertedValues.get('reservationPickupLocationState')));

                // reservationDropOffLocationState
                convertedValues = convertedValues.set('reservationDropOffLocationState', new AddressLocation(convertedValues.get('reservationDropOffLocationState')));

                // reservationAdditionalPassengersState
                convertedValues = convertedValues.set('reservationAdditionalPassengersState', new ReservationAdditionalPassengersState(convertedValues.get('reservationAdditionalPassengersState')));

                // reservationModeOfTransportationState
                convertedValues = convertedValues.set('reservationModeOfTransportationState', new ReservationModeOfTransportationState(convertedValues.get('reservationModeOfTransportationState')));

                // reservationPreferredTransportationState
                convertedValues = convertedValues.set('reservationPreferredTransportationState', new ReservationPreferredTransportationState(convertedValues.get('reservationPreferredTransportationState')));

                // reservationLegState
                convertedValues = convertedValues.set('reservationLegState', List(convertedValues.get('reservationLegState', []).map(value => new ReservationLegState(value))));

                // reservationSpecialRequirementState
                convertedValues = convertedValues.set('reservationSpecialRequirementState', List(convertedValues.get('reservationSpecialRequirementState', []).map(value => new BeneficiarySpecialRequirement(value))));

                // reservationSpecialRequirementTempState
                convertedValues = convertedValues.set('reservationSpecialRequirementTempState', List(convertedValues.get('reservationSpecialRequirementTempState', []).map(value => new BeneficiarySpecialRequirement(value))));

                // reservationTripIndicatorState
                convertedValues = convertedValues.set('reservationTripIndicatorState', new ReservationTripIndicatorState(convertedValues.get('reservationTripIndicatorState')));

                // reservation
                convertedValues = convertedValues.set('reservation', new Reservation(convertedValues.get('reservation')));

                // reservations
                convertedValues = convertedValues.set('reservations', List(convertedValues.get('reservations', []).map(value => new Reservation(value))));

                // reservationCardState
                convertedValues = convertedValues.set('reservationCardsState', new ReservationCardsState(convertedValues.get('reservationCardsState')));

                // reservationBusinessRules
                convertedValues = convertedValues.set('reservationBusinessRules', new ReservationBusinessRules(convertedValues.get('reservationBusinessRules')));

                // recentPickupLocations
                convertedValues = convertedValues.set('recentPickupLocations', List(convertedValues.get('recentPickupLocations', []).map(value => new AddressLocation(value))));

                // recentDropoffLocations
                convertedValues = convertedValues.set('recentDropoffLocations', List(convertedValues.get('recentDropoffLocations', []).map(value => new AddressLocation(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
