import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    fromStorage as historyFilterFromStorage,
    IReservationHistoryFiltersState,
    ReservationHistoryFiltersState
} from './reservation-history-filters-state.model';

export enum SortOptions {
    APPOINTMENT_TIME_ASC,
    APPOINTMENT_TIME_DESC,
    TREATMENT_TYPE_ASC,
    TREATMENT_TYPE_DESC,
    TRIP_DATE_ASC,
    TRIP_DATE_DESC,
    TRIP_ID_ASC,
    TRIP_ID_DESC,
    TRIP_STATUS_ASC,
    TRIP_STATUS_DESC,
    TRIP_TYPE_ASC,
    TRIP_TYPE_DESC
}

export interface IReservationHistoryState {
    filters : IReservationHistoryFiltersState;
    itemsPerPage : number;
    itemsPerPageChoices : Array<number>;
    pageOffset : number;
    sortOption : SortOptions;
}

export const RESERVATION_HISTORY = Record({
    filters : new ReservationHistoryFiltersState(),
    itemsPerPage : 20,
    itemsPerPageChoices : [20, 40, 60, 80, 100],
    pageOffset : 0,
    sortOption : SortOptions.APPOINTMENT_TIME_ASC
});

export class ReservationHistoryState extends RESERVATION_HISTORY {
    filters : ReservationHistoryFiltersState;
    itemsPerPage : number;
    itemsPerPageChoices : Array<number>;
    pageOffset : number;
    sortOption : SortOptions;

    constructor(values? : IReservationHistoryState | ReservationHistoryState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationHistoryState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                convertedValues = convertedValues.set(
                    'filters',
                    new ReservationHistoryFiltersState(convertedValues.get('filters'))
                );
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

export function fromStorage(pojo : IReservationHistoryState) : ReservationHistoryState {
    let state = new ReservationHistoryState(pojo);

    state = state.set('filters', historyFilterFromStorage(state.get('filters'))) as ReservationHistoryState;

    return state;
}
