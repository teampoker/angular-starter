import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    fromStorage as dateRangeFromStorage,
    DateRange,
    IDateRange
} from '../../types/date-range.model';

export interface IReservationHistoryFiltersState {
    dateRange : IDateRange;
    text : string;
    treatmentType : { [ uuid : string ] : boolean };
    tripStatus : { [ uuid : string ] : boolean };
    tripType : { [ type : string ] : boolean };
}

export const RESERVATION_HISTORY_FILTERS = Record({
    dateRange : new DateRange(),
    text : '',
    treatmentType : Map<string, boolean>(),
    tripStatus : Map<string, boolean>(),
    tripType : Map<string, boolean>()
});

export class ReservationHistoryFiltersState extends RESERVATION_HISTORY_FILTERS {
    dateRange : DateRange;
    text : string;
    treatmentType : Map<string, boolean>;
    tripStatus : Map<string, boolean>;
    tripType : Map<string, boolean>;

    constructor(values? : IReservationHistoryFiltersState | ReservationHistoryFiltersState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof ReservationHistoryFiltersState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                convertedValues = convertedValues.set(
                    'dateRange',
                    new DateRange(convertedValues.get('dateRange'))
                );
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

export function fromStorage(pojo : IReservationHistoryFiltersState) : ReservationHistoryFiltersState {
    let state = new ReservationHistoryFiltersState(pojo);

    state = state.set('dateRange', dateRangeFromStorage(state.get('dateRange'))) as ReservationHistoryFiltersState;

    return state;
}
