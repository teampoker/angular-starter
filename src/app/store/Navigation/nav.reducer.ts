import {INITIAL_NAV_STATE} from './nav.initial-state';
import {IPayloadAction} from '../app-store';
import {NavState} from './types/nav-state.model';
import {NavActions} from './nav.actions';
import {TaskItem} from './types/task-item.model';

export const NAV_STATE_REDUCER = (state : NavState = INITIAL_NAV_STATE, action : IPayloadAction) : NavState => {
    switch (action.type) {
        case NavActions.UPDATE_NAV :
            state = state.merge({ activeNavState : action.payload }) as NavState;

            break;
        case NavActions.TOGGLE_SIDE_NAV :
            state = state.merge({ extendSideMenu : action.payload }) as NavState;

            break;
        case NavActions.TOGGLE_IS_POPPED :
            state = state.merge({ isPopped : action.payload }) as NavState;

            break;
        case NavActions.TASK_LIST_TOGGLE :
            state = state.merge({ toggleTasks : action.payload }) as NavState;

            break;
        case NavActions.TASK_LIST_UPDATE :
            state = state.setIn(['taskList', action.payload.index], action.payload.data) as NavState;

            break;
        case NavActions.TASK_LIST_ADD_ITEM :
            state = state.update('taskList', value => value.push(new TaskItem(action.payload))) as NavState;

            break;
        case NavActions.ALERT_MESSAGE_UPDATE :
            state = state.set('alertMessage', action.payload) as NavState;

            break;
        default :
            return state;
    }

    return state;
};
