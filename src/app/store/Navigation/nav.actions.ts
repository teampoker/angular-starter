import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../app-store';
import {NavStateSelectors} from './nav.selectors';
import {SearchActions} from '../Search/search.actions';
import {AlertItem} from './types/alert-item.model';
import {TaskItem} from './types/task-item.model';
import {
    EnumNavOption,
    NavOption
} from './types/nav-option.model';

@Injectable()

/**
 * NavActions Action Creator Service
 */
export class NavActions {
    static UPDATE_NAV           : string = 'UPDATE_NAV';
    static TOGGLE_IS_POPPED     : string = 'TOGGLE_IS_POPPED';
    static TOGGLE_SIDE_NAV      : string = 'TOGGLE_SIDE_NAV';
    static TASK_LIST_TOGGLE     : string = 'TASK_LIST_TOGGLE';
    static TASK_LIST_ADD_ITEM   : string = 'TASK_LIST_ADD_ITEM';
    static TASK_LIST_UPDATE     : string = 'TASK_LIST_UPDATE';
    static ALERT_MESSAGE_UPDATE : string = 'ALERT_MESSAGE_UPDATE';

    /**
     * NavActions constructor
     * @param store
     * @param router
     * @param navSelectors
     * @param searchActions
     */
    constructor(
        private store           : NgRedux<IAppStore>,
        private router          : Router,
        private navSelectors    : NavStateSelectors,
        private searchActions   : SearchActions
    ) {}

    /**
     * dispatches UPDATE_NAV Redux action to update NavState.activeNavState value
     * @param link
     */
    updateActiveNavState(link : EnumNavOption) {
        this.store.dispatch({
            type    : NavActions.UPDATE_NAV,
            payload : link
        });
    }

    /**
     * dispatches UPDATE_NAV Redux action to update NavState.activeNavState value
     * @param message
     */
    updateAlertMessageState(message : AlertItem) {
        this.store.dispatch({
            type    : NavActions.ALERT_MESSAGE_UPDATE,
            payload : message
        });
    }

    /**
     * toggle show/hide the taskList state
     */
    toggleTaskList(data : boolean) {
        data = !data;
        this.store.dispatch({
            type    : NavActions.TASK_LIST_TOGGLE,
            payload : data
        });
    }

    /**
     * add to the taskList state
     */
    addTaskToList(data : TaskItem) {
        this.store.dispatch({
            type    : NavActions.TASK_LIST_ADD_ITEM,
            payload : data
        });
    }

    /**
     * update item in taskList state
     */
    updateTaskList(data : TaskItem, index : number) {
        this.store.dispatch({
            type    : NavActions.TASK_LIST_UPDATE,
            payload : {
                data,
                index
            }
        });
    }

    /**
     * dispatches TOGGLE_SIDE_NAV Redux action to toggle expanded state of side navigation
     * @param sideMenuToggle
     */
    toggleSideNavigation(sideMenuToggle : boolean) {
        this.store.dispatch({
            type    : NavActions.TOGGLE_SIDE_NAV,
            payload : sideMenuToggle
        });
    }

    /**
     * dispatches TOGGLE_IS_POPPED Redux action to toggle expanded state of side navigation
     * @param isPoppedToggle visible state of lightbox styling
     */
    toggleIsPoppedNavState(isPoppedToggle : boolean) {
        this.store.dispatch({
            type    : NavActions.TOGGLE_IS_POPPED,
            payload : isPoppedToggle
        });
    }

    /**
     * 1. updates activeNavState value on NavState
     * 2. toggles expanded state of side nav menu (if provided)
     * 3. triggers Angular Router navigation step
     *
     * @param navItem
     * @param extendSideMenu
     */
    doMenuNavigation(navItem : NavOption, extendSideMenu? : boolean) {
        // determine current active link
        const link : EnumNavOption = navItem.baseNavOption;

        // check current navigation state
        if (this.navSelectors.currentState() === EnumNavOption.SEARCH) {
            // clear search state
            this.searchActions.resetHeaderSearchState();

        }

        // trigger nav action
        this.updateActiveNavState(link);

        // toggle side nav expanded state?
        if (extendSideMenu !== undefined) {
            this.toggleSideNavigation(extendSideMenu);
        }

        // do routing
        switch (link) {
            case EnumNavOption.DASHBOARD :
                this.router.navigate(['/Dashboard']);

                break;

            case EnumNavOption.ELIGIBILITY :
                this.router.navigate(['/Eligibility']);

            case EnumNavOption.EXCEPTION_QUEUE:
                this.router.navigate(['/ExceptionQueue']);

                break;
            default :
                this.router.navigate(['/Dashboard']);
        }
    }
}
