import {async} from '@angular/core/testing';
import {Router} from '@angular/router';

import {NavActions} from './nav.actions';
import {NavStateSelectors} from './nav.selectors';
import {MockRouter} from '../../../testing/router-stubs';
import {MockRedux} from '../../../testing/ng2-redux-subs';
import {SearchActions} from '../Search/search.actions';
import {EnumNavOption, NavOption} from './types/nav-option.model';
import {TaskItem} from './types/task-item.model';

// Mock the SearchActions with the methods we need to trigger
class MockSearchActions extends SearchActions {
    constructor() {
        super(undefined, undefined, undefined, undefined);
    }

    resetHeaderSearchState() {
        return undefined;
    }
}

// Mock the NavStateSelectors with the methods we need to trigger
class MockNavStateSelectors extends NavStateSelectors {
    constructor() {
        super(undefined);
    }

    currentState() : EnumNavOption {
        return EnumNavOption.DASHBOARD;
    }
}

describe('Navigation Action Creators', () => {
    let navActions          : NavActions,
        mockRouter          : MockRouter,
        mockSearchActions   : MockSearchActions,
        mockNavSelectors    : MockNavStateSelectors,
        mockRedux           : MockRedux;

    // setup tasks to perform before each test
    beforeEach(() => {
        // Initialize mock NgRedux and create a new instance of the
        // ActionCreator Service to be tested.
        mockRedux           = new MockRedux();
        mockSearchActions   = new MockSearchActions();
        mockNavSelectors    = new MockNavStateSelectors();
        mockRouter          = new MockRouter();
        navActions          = new NavActions(
            mockRedux,
            mockRouter as Router,
            mockNavSelectors,
            mockSearchActions
        );
    });

    // test definitions
    it('updateActiveNavState method should dispatch UPDATE_NAV action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : NavActions.UPDATE_NAV,
            payload : EnumNavOption.DASHBOARD
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        navActions.updateActiveNavState(EnumNavOption.DASHBOARD);

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('toggleSideNavigation method should dispatch TOGGLE_SIDE_NAV action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : NavActions.TOGGLE_SIDE_NAV,
            payload : true
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        navActions.toggleSideNavigation(true);

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('doMenuNavigation method should dispatch UPDATE_NAV action', async(() => {
        const navOption = new NavOption({
            baseNavOption : EnumNavOption.USER,
            title         : 'USER',
            style         : 'fa fa-user fa-2x',
            subNav        : []
        });

        // action expected to be dispatched
        const expectedAction = {
            type    : NavActions.UPDATE_NAV,
            payload : EnumNavOption.USER
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        navActions.doMenuNavigation(navOption);

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('doMenuNavigation method should dispatch TOGGLE_SIDE_NAV action when extendSideMenu parameter is supplied', async(() => {
        const navOption = new NavOption({
            baseNavOption : EnumNavOption.USER,
            title         : 'USER',
            style         : 'fa fa-user fa-2x',
            subNav        : []
        });

        // action expected to be dispatched
        const expectedAction = {
            type    : NavActions.TOGGLE_SIDE_NAV,
            payload : true
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        navActions.doMenuNavigation(navOption, true);

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('doMenuNavigation method should route to the correct location based on supplied EnumNavOption', async(() => {
        const navOption = new NavOption({
            baseNavOption : EnumNavOption.ELIGIBILITY,
            title         : 'USER',
            style         : 'fa fa-user fa-2x',
            subNav        : []
        });

        // spy on mock navigate method
        spyOn(mockRouter, 'navigate');

        navActions.doMenuNavigation(navOption);

        expect(mockRouter.navigate).toHaveBeenCalledWith(['/Eligibility']);
    }));

    it('addTaskToList method should dispatch TASK_LIST_ADD_ITEM action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : NavActions.TASK_LIST_ADD_ITEM,
            payload : new TaskItem()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        navActions.addTaskToList(new TaskItem());

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('updateTaskList method should dispatch TASK_LIST_UPDATE action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : NavActions.TASK_LIST_UPDATE,
            payload : {
                data    : new TaskItem(),
                index   : 0
            }
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        navActions.updateTaskList(new TaskItem(), 0);

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('toggleTaskList method should dispatch TASK_LIST_TOGGLE action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : NavActions.TASK_LIST_TOGGLE,
            payload : false
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        navActions.toggleTaskList(true);

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));
});
