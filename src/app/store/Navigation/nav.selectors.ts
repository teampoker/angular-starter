import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../app-store';
import {EnumNavOption, NavOption} from './types/nav-option.model';
import {AlertItem} from './types/alert-item.model';
import {TaskItem} from './types/task-item.model';

@Injectable()

/**
 * implementation for NavStateSelectors: responsible for exposing custom state subscriptions to NavState
 */
export class NavStateSelectors {
    /**
     * NavStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to NavState.isPopped
     * @returns {Observable<boolean>}
     */
    isPopped() : Observable<boolean> {
        return this.store.select(state => state.navState.get('isPopped'));
    }

    /**
     * expose Observable to NavState.activeNavState
     * @returns {Observable<EnumNavOption>}
     */
    activeNavState() : Observable<EnumNavOption> {
        return this.store.select(state => state.navState.get('activeNavState'));
    }

    /**
     * expose Observable to NavState.topNavConfig
     * @returns {Observable<List<NavOption>>}
     */
    topNavConfig() : Observable<List<NavOption>> {
        return this.store.select(state => state.navState.get('topNavConfig'));
    }

    /**
     * expose Observable to NavState.alertMessage
     * @returns {Observable<AlertItem>}
     */
    alertMessage() : Observable<AlertItem> {
        return this.store.select(state => state.navState.get('alertMessage'));
    }

    /**
     * expose Observable to NavState.leftNavConfig
     * @returns {Observable<List<NavOption>>}
     */
    leftNavConfig() : Observable<List<NavOption>> {
        return this.store.select(state => state.navState.get('leftNavConfig'));
    }

    /**
     * expose Observable to NavState.extendSideMenu
     * @returns {Observable<boolean>}
     */
    extendSideMenu() : Observable<boolean> {
        return this.store.select(state => state.navState.get('extendSideMenu'));
    }

    /**
     * return current value of activeNavState
     * @returns EnumNavOptions
     */
    currentState() : EnumNavOption {
        let currentState : EnumNavOption = EnumNavOption.DASHBOARD;

        this.store.select(state => state.navState.get('activeNavState')).subscribe(value => currentState = value).unsubscribe();

        return currentState;
    }

    /**
     * expose Observable to taskList
     * @returns {Observable<List<TaskItem>>}
     */
    taskList() : Observable<List<TaskItem>> {
        return this.store.select(state => state.navState.get('taskList'));
    }

    /**
     * expose Observable to toggleTasks
     * @returns {Observable<boolean>}
     */
    toggleTaskList() : Observable<boolean> {
        return this.store.select(state => state.navState.get('toggleTasks'));
    }
}
