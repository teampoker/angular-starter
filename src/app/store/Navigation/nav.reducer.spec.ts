import {NAV_STATE_REDUCER} from './nav.reducer';
import {INITIAL_NAV_STATE} from './nav.initial-state';
import {NavActions} from './nav.actions';
import {EnumNavOption} from './types/nav-option.model';
import {TaskItem} from './types/task-item.model';

describe('Navigation State Reducer', () => {
    it('should init initial state', () => {
        expect(
            NAV_STATE_REDUCER(undefined, {
                type    : undefined,
                payload : undefined
            })
        )
        .toEqual(INITIAL_NAV_STATE);
    });

    it('should handle UPDATE_NAV action', () => {
        expect(
            NAV_STATE_REDUCER(undefined, {
                type    : NavActions.UPDATE_NAV,
                payload : EnumNavOption.DASHBOARD
            })
            .get('activeNavState')
        )
        .toEqual(EnumNavOption.DASHBOARD);
    });

    it('should handle TOGGLE_SIDE_NAV action', () => {
        expect(
            NAV_STATE_REDUCER(undefined, {
                type    : NavActions.TOGGLE_SIDE_NAV,
                payload : true
            })
                .get('extendSideMenu')
        )
        .toEqual(true);
    });

    it('should handle TASK_LIST_UPDATE action', () => {
        expect(
            NAV_STATE_REDUCER(undefined, {
                type    : NavActions.TASK_LIST_UPDATE,
                payload : new TaskItem()
            })
            .getIn(['taskList', 0]) instanceof TaskItem
        )
        .toEqual(true);
    });

    it('should handle TASK_LIST_ADD_ITEM action', () => {
        expect(
            NAV_STATE_REDUCER(undefined, {
                type    : NavActions.TASK_LIST_ADD_ITEM,
                payload : new TaskItem()
            })
            .getIn(['taskList', 0]) instanceof TaskItem
        )
        .toEqual(true);
    });
});
