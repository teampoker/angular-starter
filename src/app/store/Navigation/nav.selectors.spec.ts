import {async} from '@angular/core/testing';
import {Iterable} from 'immutable';

import {NavStateSelectors} from './nav.selectors';
import {MockRedux} from '../../../testing/ng2-redux-subs';
import {ROOT_REDUCER} from '../app-store';
import {EnumNavOption, NavOption} from './types/nav-option.model';
import {AlertItem} from './types/alert-item.model';
import {TaskItem} from './types/task-item.model';

describe('Navigation Selectors', () => {
    let navStateSelectors : NavStateSelectors,
        mockRedux         : MockRedux;

    // setup tasks to perform before each test
    beforeEach(() => {
        mockRedux = new MockRedux();

        mockRedux.configureStore(ROOT_REDUCER, {}, undefined, undefined);

        // Initialize mock NgRedux and create a new instance of the
        navStateSelectors = new NavStateSelectors(mockRedux);
    });

    // test definitions
    it('isPopped method should return NavState.isPopped subscription', async(() => {
        // subscribe to Redux store updates
        navStateSelectors.isPopped().first().subscribe(val => {
            // check returned state for correct type
            expect(typeof val === 'boolean').toEqual(true);
        });
    }));

    it('activeNavState method should return activeNavState subscription', async(() => {
        // subscribe to Redux store updates
        navStateSelectors.activeNavState().first().subscribe(val => {
            // check returned state for correct type
            expect(val).toEqual(EnumNavOption.LOGIN);
        });
    }));

    it('toggleTaskList method should return NavState.toggleTaskList subscription', async(() => {
        // subscribe to Redux store updates
        navStateSelectors.toggleTaskList().first().subscribe(val => {
            // check returned state for correct type
            expect(typeof val === 'boolean').toEqual(true);
        });
    }));

    it('topNavConfig method should return NavState.topNavConfig subscription', async(() => {
        // subscribe to Redux store updates
        navStateSelectors.topNavConfig().first().subscribe(val => {
            // check returned state for correct type
            expect(Iterable.isIndexed(val)).toEqual(true);
            expect(val.get(0) instanceof NavOption).toEqual(true);
        });
    }));

    it('leftNavConfig method should return NavState.leftNavConfig subscription', async(() => {
        // subscribe to Redux store updates
        navStateSelectors.leftNavConfig().first().subscribe(val => {
            // check returned state for correct type
            expect(Iterable.isIndexed(val)).toEqual(true);
            expect(val.get(0) instanceof NavOption).toEqual(true);
        });
    }));

    it('alertMessage method should return NavState.alertMessage subscription', async(() => {
        // subscribe to Redux store updates
        navStateSelectors.alertMessage().first().subscribe(val => {
            // check returned state for correct type
            expect(val instanceof AlertItem).toEqual(true);
        });
    }));

    it('extendSideMenu method should return NavState.extendSideMenu subscription', async(() => {
        // subscribe to Redux store updates
        navStateSelectors.extendSideMenu().first().subscribe(val => {
            // check returned state for correct type
            expect(typeof val === 'boolean').toEqual(true);
        });
    }));

    it('currentState method should return NavState.activeNavState value', async(() => {
        // check returned state for correct type
        expect(typeof navStateSelectors.currentState() === 'number').toEqual(true);
    }));

    it('taskList method should return NavState.taskList subscription', async(() => {
        // subscribe to Redux store updates
        navStateSelectors.taskList().first().subscribe(val => {
            // check returned state for correct type
            expect(Iterable.isIndexed(val)).toEqual(true);
            expect(val.get(0) instanceof TaskItem).toEqual(true);
        });
    }));
});
