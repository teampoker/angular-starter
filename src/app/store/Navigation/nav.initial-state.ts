import {List} from 'immutable';

import {NavState} from './types/nav-state.model';
import {EnumNavOption, NavOption} from './types/nav-option.model';
import {TaskItem} from './types/task-item.model';

export const INITIAL_NAV_STATE = new NavState().withMutations(state => {
    state.set('topNavConfig', List([
        {
            baseNavOption : EnumNavOption.USER,
            title         : 'USER',
            style         : 'fa fa-user fa-2x',
            subNav        : [
                {
                   baseNavOption : EnumNavOption.LOGOUT,
                   title         : 'LOGOUT',
                   style         : 'fa fa-sign-out fa-2x',
                   subNav        : []
                }
            ]
        },
        {
            baseNavOption : EnumNavOption.TASKS,
            title         : 'TASKS',
            style         : 'fa fa-th-list fa-2x',
            subNav        : []
        }
    ]).map(value => new NavOption(value)))

    .set('leftNavConfig', List([
        {
            baseNavOption : EnumNavOption.DASHBOARD,
            title         : 'DASHBOARD',
            style         : 'fa fa-dashboard fa-2x',
            subNav        : []
        },
        {
            baseNavOption : EnumNavOption.EXCEPTION_QUEUE,
            title         : 'EXCEPTION_QUEUE',
            style         : 'fa fa-check-square-o fa-2x',
            subNav        : []
        }
    ]).map(value => new NavOption(value)))

    .set('taskList', List([
        {
            task        : '',
            status      : false
        }
    ]).map(value => new TaskItem(value)));
}) as NavState;
