import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ITaskItem {
    task        : string;
    status      : boolean;
}

export const TASK_ITEM = Record({
    task        : '',
    status      : false
});

/**
 * type definition for Redux Store taskItem state
 */
export class TaskItem extends TASK_ITEM {
    task        : string;
    status      : boolean;

    constructor(values? : TaskItem | ITaskItem) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof TaskItem) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
