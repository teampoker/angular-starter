import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    EnumNavOption,
    INavOption,
    NavOption
} from './nav-option.model';
import {
    ITaskItem,
    TaskItem
} from './task-item.model';
import {
    AlertItem,
    IAlertItem
} from './alert-item.model';

export interface INavState {
    extendSideMenu  : boolean;
    isPopped        : boolean;
    activeNavState  : EnumNavOption;
    topNavConfig    : Array<INavOption>;
    leftNavConfig   : Array<INavOption>;
    taskList        : Array<ITaskItem>;
    toggleTasks     : boolean;
    alertMessage    : IAlertItem;
}

export const NAV_STATE = Record({
    extendSideMenu : false,
    isPopped       : false,
    activeNavState : EnumNavOption.LOGIN,
    topNavConfig   : List<NavOption>(),
    leftNavConfig  : List<NavOption>(),
    taskList       : List<TaskItem>(),
    toggleTasks    : false,
    alertMessage   : new AlertItem()
});

export class NavState extends NAV_STATE {
    extendSideMenu  : boolean;
    isPopped        : boolean;
    activeNavState  : EnumNavOption;
    topNavConfig    : List<NavOption>;
    leftNavConfig   : List<NavOption>;
    taskList        : List<TaskItem>;
    toggleTasks     : boolean;
    alertMessage    : AlertItem;

    constructor(values? : NavState | INavState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof NavState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // topNavConfig
                convertedValues = convertedValues.set('topNavConfig', List(convertedValues.get('topNavConfig', []).map(value => new NavOption(value))));

                // leftNavConfig
                convertedValues = convertedValues.set('leftNavConfig', List(convertedValues.get('leftNavConfig', []).map(value => new NavOption(value))));

                // taskList
                convertedValues = convertedValues.set('taskList', List(convertedValues.get('taskList', []).map(value => new TaskItem(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
