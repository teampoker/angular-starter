import {
    fromJS,
    Map,
    Record
} from 'immutable';

export enum EnumAlertType {
    INFO,
    WARNING,
    ERROR,
    SUCCESS,
    LOCKED,
    INTERNAL
}

export interface IAlertItem {
    alertType   : EnumAlertType;
    message     : string;
    durable?    : boolean;
}

export const ALERT_ITEM = Record({
    alertType   : EnumAlertType.INFO,
    message     : '',
    durable     : false
});

/**
 * type definition for Redux Store taskItem state
 */
export class AlertItem extends ALERT_ITEM {
    /**
     * The alert type - instilling some level of confusion in our users. Life choices that lead to this point...
     */
    alertType : EnumAlertType;
    /**
     * The message intended to be displayed .
     */
    message : string;
    /**
     * An alert is 'durable' if it needs to stay on the screen for the user.
     * A durable alert must be dismissed (closed) by the user.
     */
    durable : boolean;

    constructor(values? : AlertItem | IAlertItem) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof AlertItem) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
