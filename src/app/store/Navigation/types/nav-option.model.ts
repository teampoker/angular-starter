import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

export enum EnumNavOption {
    BENEFICIARY,
    DASHBOARD,
    LOGIN,
    LOGOUT,
    ELIGIBILITY,
    PROFILE,
    SEARCH,
    USER,
    RESERVATION,
    TASKS,
    EXCEPTION_QUEUE
}

export interface INavOption {
    baseNavOption   : EnumNavOption;
    title           : string;
    style           : string;
    subNav          : Array<any>;
}

const NAV_OPTION = Record({
    baseNavOption   : undefined,
    title           : '',
    style           : '',
    subNav          : List<any>()
});

export class NavOption extends NAV_OPTION {
    baseNavOption   : EnumNavOption;
    title           : string;
    style           : string;
    subNav          : List<any>;

    constructor(values? : NavOption | INavOption) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof NavOption) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }

            convertedValues = convertedValues.set(
                'subNav',
                List(
                    convertedValues.get('subNav', [])
                        .map(value => new NavOption(value))
                )
            );
        }

        super(convertedValues);
    }
}
