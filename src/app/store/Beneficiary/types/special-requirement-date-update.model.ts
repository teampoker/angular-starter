import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    BeneficiarySpecialRequirement,
    IBeneficiarySpecialRequirement
} from './beneficiary-special-requirement.model';

export interface ISpecialRequirementDateUpdate {
    newDate     : string;
    requirement : IBeneficiarySpecialRequirement;
}

export const SPECIAL_REQUIREMENT_DATE_UPDATE = Record({
    newDate     : '',
    requirement : new BeneficiarySpecialRequirement()
});

export class SpecialRequirementDateUpdate extends SPECIAL_REQUIREMENT_DATE_UPDATE {
    newDate     : string;
    requirement : BeneficiarySpecialRequirement;

    constructor(values? : SpecialRequirementDateUpdate | ISpecialRequirementDateUpdate) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SpecialRequirementDateUpdate) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // requirement
                convertedValues = convertedValues.set(
                    'requirement',
                    new BeneficiarySpecialRequirement(convertedValues.get('requirement'))
                );
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
