import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryContactMechanismPurposeType {
    id                          : string;
    value                       : string;
    contactMechanismType        : IKeyValuePair;
}

export const BENEFICIARY_CONTACT_MECHANISM_PURPOSE_TYPE = Record({
    id                          : '',
    value                       : '',
    contactMechanismType        : new KeyValuePair()
});

const apiMap = Map({
    uuid                    : 'id',
    name                    : 'value',
    contactMechanismType    : 'contactMechanismType'
});

export class BeneficiaryContactMechanismPurposeType extends BENEFICIARY_CONTACT_MECHANISM_PURPOSE_TYPE {
    id                          : string;
    value                       : string;
    contactMechanismType        : KeyValuePair;

    constructor(values? : BeneficiaryContactMechanismPurposeType | IBeneficiaryContactMechanismPurposeType) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryContactMechanismPurposeType) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // contactMechanismType
                if (convertedValues.getIn(['contactMechanismType', 'uuid'])) {
                    convertedValues = convertedValues.set('contactMechanismType', KeyValuePair.fromApi(convertedValues.get('contactMechanismType')));
                }
                else {
                    convertedValues = convertedValues.set('contactMechanismType', new KeyValuePair(convertedValues.get('contactMechanismType')));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }

    static fromApi(rawObject : any) : BeneficiaryContactMechanismPurposeType {
        const newObject : any = {};

        if (Map.isMap(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject.get(apiKey);
            });
        }
        else {
            apiMap.forEach((translatedKey, apiKey) => {
                newObject[translatedKey] = rawObject[apiKey];
            });
        }

        return new BeneficiaryContactMechanismPurposeType(newObject);
    }
}
