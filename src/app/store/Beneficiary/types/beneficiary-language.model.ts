import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    ILanguageType,
    LanguageType
} from '../../MetaDataTypes/types/language-type.model';

export interface IBeneficiaryLanguage {
    ordinality  : number;
    personUuid? : string;
    sequenceId? : number;
    version?    : number;
    language    : ILanguageType;
    type        : ILanguageType;
}

export const BENEFICIARY_LANGUAGE = Record({
    ordinality  : 0,
    personUuid  : '',
    sequenceId  : undefined,
    version     : undefined,
    language    : new LanguageType(),
    type        : new LanguageType()
});

export class BeneficiaryLanguage extends BENEFICIARY_LANGUAGE {
    ordinality  : number;
    personUuid? : string;
    sequenceId? : number;
    version?    : number;
    language    : LanguageType;
    type        : LanguageType;

    constructor(values? : BeneficiaryLanguage | IBeneficiaryLanguage) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryLanguage) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // language
                if (convertedValues.getIn(['language', 'uuid'])) {
                    convertedValues = convertedValues.set('language', LanguageType.fromApi(convertedValues.get('language')));
                }
                else {
                    convertedValues = convertedValues.set('language', new LanguageType(convertedValues.get('language')));
                }

                // type
                if (convertedValues.getIn(['type', 'uuid'])) {
                    convertedValues = convertedValues.set('type', LanguageType.fromApi(convertedValues.get('type')));
                }
                else {
                    convertedValues = convertedValues.set('type', new LanguageType(convertedValues.get('type')));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
