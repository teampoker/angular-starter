import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IBeneficiaryInfo {
    birthDate               : string;
    deathDate               : string;
    createdOn               : string;
    firstName               : string;
    lastName                : string;
    middleName              : string;
    nickname                : string;
    personalTitle           : string;
    suffix                  : string;
    updatedOn               : string;
    uuid                    : string;
    version                 : number;
    gender                  : string;
    language                : string;
    heightFeet              : string;
    heightInches            : string;
    weight                  : string;
    isDeceased              : boolean;
}

export const BENEFICIARY_INFO = Record({
    birthDate               : '',
    deathDate               : '',
    createdOn               : '',
    firstName               : '',
    lastName                : '',
    middleName              : '',
    nickname                : '',
    personalTitle           : 'SELECT',
    suffix                  : '',
    updatedOn               : '',
    uuid                    : undefined,
    version                 : undefined,
    gender                  : 'SELECT',
    language                : 'SELECT',
    heightFeet              : 'SELECT',
    heightInches            : 'SELECT',
    weight                  : '',
    isDeceased              : false
});

export class BeneficiaryInfo extends BENEFICIARY_INFO {
    birthDate               : string;
    deathDate               : string;
    createdOn               : string;
    firstName               : string;
    lastName                : string;
    middleName              : string;
    nickname                : string;
    personalTitle           : string;
    suffix                  : string;
    updatedOn               : string;
    uuid                    : string;
    version                 : number;
    gender                  : string;
    language                : string;
    heightFeet              : string;
    heightInches            : string;
    weight                  : string;
    isDeceased              : boolean;

    constructor(values?  : BeneficiaryInfo | IBeneficiaryInfo) {
        let convertedValues  : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryInfo) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
