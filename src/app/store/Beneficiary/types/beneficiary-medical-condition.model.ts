import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryMedicalCondition {
    personUuid?         : string;
    sequenceId?         : number;
    version?            : number;
    fromDate?           : string;
    thruDate?           : string;
    type                : IKeyValuePair;
}

const BENEFICIARY_MEDICAL_CONDITION = Record({
    personUuid         : '',
    sequenceId         : undefined,
    version            : undefined,
    fromDate           : '',
    thruDate           : '',
    type               : new KeyValuePair()
});

export class BeneficiaryMedicalCondition extends BENEFICIARY_MEDICAL_CONDITION {
    personUuid?         : string;
    sequenceId?         : number;
    version?            : number;
    fromDate?           : string;
    thruDate?           : string;
    type                : KeyValuePair;

    constructor(values? : BeneficiaryMedicalCondition | IBeneficiaryMedicalCondition) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryMedicalCondition) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // type
                if (convertedValues.getIn(['type', 'uuid'])) {
                    convertedValues = convertedValues.set('type', KeyValuePair.fromApi(convertedValues.get('type')));
                }
                else {
                    convertedValues = convertedValues.set('type', new KeyValuePair(convertedValues.get('type')));
                }
            }
        }

        super(convertedValues);
    }
}
