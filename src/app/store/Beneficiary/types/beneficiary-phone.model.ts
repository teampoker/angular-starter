import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryPhone {
    sequenceId?         : number;
    uuid?               : string;
    version?            : number;
    phone               : string;
    phoneType           : IKeyValuePair;
    phoneNotifications  : IKeyValuePair;
    mechanismType       : IKeyValuePair;
}

export const BENEFICIARY_PHONE = Record({
    sequenceId          : undefined,
    uuid                : undefined,
    version             : undefined,
    phone               : '',
    phoneType           : new KeyValuePair(),
    phoneNotifications  : new KeyValuePair(),
    mechanismType       : new KeyValuePair()
});

export class BeneficiaryPhone extends BENEFICIARY_PHONE {
    sequenceId?         : number;
    uuid?               : string;
    version?            : number;
    phone               : string;
    phoneType           : KeyValuePair;
    phoneNotifications  : KeyValuePair;
    mechanismType       : KeyValuePair;

    constructor(values? : BeneficiaryPhone | IBeneficiaryPhone) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryPhone) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
