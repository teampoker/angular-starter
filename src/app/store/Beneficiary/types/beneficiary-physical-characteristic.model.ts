import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryPhysicalCharacteristic {
    value       : string;
    personUuid  : string;
    sequenceId  : number;
    version     : number;
    type        : IKeyValuePair;
}

export const BENEFICIARY_PHYSICAL_CHARACTERISTIC = Record({
    value       : 0,
    personUuid  : '',
    sequenceId  : undefined,
    version     : undefined,
    type        : new KeyValuePair()
});

export class BeneficiaryPhysicalCharacteristic extends BENEFICIARY_PHYSICAL_CHARACTERISTIC {
    value       : string;
    personUuid  : string;
    sequenceId  : number;
    version     : number;
    type        : KeyValuePair;

    constructor(values? : BeneficiaryPhysicalCharacteristic | IBeneficiaryPhysicalCharacteristic) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryPhysicalCharacteristic) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // type
                if (convertedValues.getIn(['type', 'uuid'])) {
                    convertedValues = convertedValues.set('type', KeyValuePair.fromApi(convertedValues.get('type')));
                }
                else {
                    convertedValues = convertedValues.set('type', new KeyValuePair(convertedValues.get('type')));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
