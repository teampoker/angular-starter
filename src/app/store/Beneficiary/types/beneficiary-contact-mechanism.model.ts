import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryContactMechanismDetail,
    BeneficiaryContactMechanismDetail
} from './beneficiary-contact-mechanism-detail.model';
import {
    IBeneficiaryContactMechanismPurposeType,
    BeneficiaryContactMechanismPurposeType
} from './beneficiary-contact-mechanism-purpose-type.model';
import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryContactMechanism {
    version                     : string;
    personUuid                  : string;
    sequenceId                  : number;
    ordinality                  : number;
    comments                    : string;
    extension                   : string;
    contactMechanism            : IBeneficiaryContactMechanismDetail;
    purposeType                 : IBeneficiaryContactMechanismPurposeType;
    solicitationIndicatorType   : IKeyValuePair;
}

export const BENEFICIARY_CONTACT_MECHANISM = Record({
    version                     : undefined,
    personUuid                  : undefined,
    sequenceId                  : undefined,
    ordinality                  : 0,
    comments                    : '',
    extension                   : '',
    contactMechanism            : new BeneficiaryContactMechanismDetail(),
    purposeType                 : new BeneficiaryContactMechanismPurposeType(),
    solicitationIndicatorType   : new KeyValuePair()
});

export class BeneficiaryContactMechanism extends BENEFICIARY_CONTACT_MECHANISM {
    version                     : string;
    personUuid                  : string;
    sequenceId                  : number;
    ordinality                  : number;
    comments                    : string;
    extension                   : string;
    contactMechanism            : BeneficiaryContactMechanismDetail;
    purposeType                 : BeneficiaryContactMechanismPurposeType;
    solicitationIndicatorType   : KeyValuePair;

    constructor(values? : BeneficiaryContactMechanism | IBeneficiaryContactMechanism) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryContactMechanism) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // contactMechanism
                convertedValues = convertedValues.set('contactMechanism', new BeneficiaryContactMechanismDetail(convertedValues.get('contactMechanism')));

                // purposeType
                if (convertedValues.getIn(['purposeType', 'uuid'])) {
                    convertedValues = convertedValues.set('purposeType', BeneficiaryContactMechanismPurposeType.fromApi(convertedValues.get('purposeType')));
                }
                else {
                    convertedValues = convertedValues.set('purposeType', new BeneficiaryContactMechanismPurposeType(convertedValues.get('purposeType')));
                }

                // solicitationIndicatorType
                if (convertedValues.getIn(['solicitationIndicatorType', 'uuid'])) {
                    convertedValues = convertedValues.set('solicitationIndicatorType', KeyValuePair.fromApi(convertedValues.get('solicitationIndicatorType')));
                }
                else {
                    convertedValues = convertedValues.set('solicitationIndicatorType', new KeyValuePair(convertedValues.get('solicitationIndicatorType')));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
