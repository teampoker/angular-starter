import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryMedicalConditionState {
    personUuid?         : string;
    sequenceId?         : number;
    version?            : number;
    fromDate?           : string;
    thruDate?           : string;
    type                : IKeyValuePair;
}

const BENEFICIARY_MEDICAL_CONDITION_STATE = Record({
    personUuid         : '',
    sequenceId         : undefined,
    version            : undefined,
    fromDate           : '',
    thruDate           : '',
    type               : new KeyValuePair()
});

export class BeneficiaryMedicalConditionState extends BENEFICIARY_MEDICAL_CONDITION_STATE {
    personUuid?         : string;
    sequenceId?         : number;
    version?            : number;
    fromDate?           : string;
    thruDate?           : string;
    type                : KeyValuePair;

    constructor(values? : BeneficiaryMedicalConditionState | IBeneficiaryMedicalConditionState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryMedicalConditionState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }
}
