import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryInfoDropdownTypes,
    BeneficiaryInfoDropdownTypes
} from './beneficiary-info-dropdown-types.model';
import {
    IBeneficiaryInfo,
    BeneficiaryInfo
} from './beneficiary-info';
import {IRequestable} from '../../App/requestable.interface';

export interface IBeneficiaryInfoState {
    isWaiting                   : boolean;
    isBeneficiaryInfoEditing    : boolean;
    beneficiaryInfo             : IBeneficiaryInfo;
    dropdownTypes               : IBeneficiaryInfoDropdownTypes;
}

export const BENEFICIARY_INFO_STATE = Record({
    isWaiting                   : false,
    isBeneficiaryInfoEditing    : false,
    beneficiaryInfo             : new BeneficiaryInfo(),
    dropdownTypes               : new BeneficiaryInfoDropdownTypes()
});

export class BeneficiaryInfoState extends BENEFICIARY_INFO_STATE implements IRequestable {
    isWaiting                   : boolean;
    isBeneficiaryInfoEditing    : boolean;
    beneficiaryInfo             : BeneficiaryInfo;
    dropdownTypes               : BeneficiaryInfoDropdownTypes;

    constructor(values? : BeneficiaryInfoState | IBeneficiaryInfoState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryInfoState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // beneficiaryInfo
                convertedValues = convertedValues.set('beneficiaryInfo', new BeneficiaryInfo(convertedValues.get('beneficiaryInfo')));

                // dropdownTypes
                convertedValues = convertedValues.set('dropdownTypes', new BeneficiaryInfoDropdownTypes(convertedValues.get('dropdownTypes')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
