import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryContactMechanismDetail {
    uuid                        : string;
    version?                    : number;
    contactNumber?              : string;
    countryCode?                : string;
    areaCode?                   : string;
    electronicAddressString?    : string;
    address1?                   : string;
    address2?                   : string;
    city?                       : string;
    state?                      : string;
    postalCode?                 : string;
    directions?                 : string;
    type                        : IKeyValuePair;
}

export const BENEFICIARY_CONTACT_MECHANISM_DETAIL = Record({
    uuid                        : undefined,
    version                     : undefined,
    contactNumber               : '',
    countryCode                 : '',
    areaCode                    : '',
    electronicAddressString     : '',
    address1                    : '',
    address2                    : '',
    city                        : '',
    state                       : '',
    postalCode                  : '',
    directions                  : '',
    type                        : new KeyValuePair()
});

export class BeneficiaryContactMechanismDetail extends BENEFICIARY_CONTACT_MECHANISM_DETAIL {
    uuid                        : string;
    version?                    : number;
    contactNumber?              : string;
    countryCode?                : string;
    areaCode?                   : string;
    electronicAddressString?    : string;
    address1?                   : string;
    address2?                   : string;
    city?                       : string;
    state?                      : string;
    postalCode?                 : string;
    directions?                 : string;
    type                        : KeyValuePair;

    constructor(values? : BeneficiaryContactMechanismDetail | IBeneficiaryContactMechanismDetail) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryContactMechanismDetail) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // type
                if (convertedValues.getIn(['type', 'uuid'])) {
                    convertedValues = convertedValues.set('type', KeyValuePair.fromApi(convertedValues.get('type')));
                }
                else {
                    convertedValues = convertedValues.set('type', new KeyValuePair(convertedValues.get('type')));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
