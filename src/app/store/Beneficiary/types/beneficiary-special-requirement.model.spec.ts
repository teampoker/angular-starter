import {
    fromApi,
    BeneficiarySpecialRequirement
} from './beneficiary-special-requirement.model';
import {RESERVATION_MOCK} from '../../../areas/Reservation/services/Reservation/reservation.service.mock';

describe('Model: BeneficiarySpecialRequirement', () => {
    it('should not throw when supplied an empty object at instantiation', () => {
        expect(() => new (BeneficiarySpecialRequirement as any)({})).not.toThrow();
    });

    describe('fromApi', () => {
        describe('Invalid Input', () => {
            const invalidFromApiCalls = [
                () => fromApi(undefined),
                () => fromApi(null),
                () => fromApi(true),
                () => fromApi('string'),
                () => fromApi(123),
                () => fromApi(() => {}),
                () => fromApi([1, 2, 3])
            ];
            it('should not throw when an invalid argument is passed', () => {
                // since this function is handling API responses we cannot rely on
                // type checking to catch errors when transpiling, so we have to
                // do runtime input validation
                invalidFromApiCalls.forEach(call => expect(call).not.toThrow());
            });
            it('should return a default leg state on invalid input', () => {
                const controlLegState = new BeneficiarySpecialRequirement();

                invalidFromApiCalls.forEach(call => {
                    const testLegState = call();

                    expect(controlLegState.equals(testLegState)).toEqual(true);
                });
            });
        });
        describe('Valid Input', () => {
            describe('getReservation Response', () => {
                const mockRequirement = RESERVATION_MOCK['getReservation'][0]['items'][0]['transportationItemSpecialRequirements'][0];
                const result  = fromApi(mockRequirement);

                it('should return a BeneficiarySpecialRequirement instance', () => {
                    expect(result instanceof BeneficiarySpecialRequirement).toBe(true);
                });
            });
            describe('getReservations Response', () => {
                const mockRequirement = RESERVATION_MOCK['getReservations'][0]['items'][0]['transportationItemSpecialRequirements'][0];
                const result  = fromApi(mockRequirement);

                it('should return a BeneficiarySpecialRequirement instance', () => {
                    expect(result instanceof BeneficiarySpecialRequirement).toBe(true);
                });
            });
        });
    });
});
