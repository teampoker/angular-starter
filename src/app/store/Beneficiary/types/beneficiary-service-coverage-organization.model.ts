import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IBeneficiaryServiceCoverageOrganization {
    createdBy                   : string;
    createdOn                   : string;
    uuid                        : string;
    name                        : string;
    updatedBy                   : string;
    updatedOn                   : string;
    nationalProviderIdentifier  : string;
    version                     : number;
}

export const BENEFICIARY_SERVICE_COVERAGE_ORGANIZATION = Record({
    createdBy                   : '',
    createdOn                   : '',
    uuid                        : undefined,
    name                        : '',
    updatedBy                   : '',
    updatedOn                   : '',
    nationalProviderIdentifier  : '',
    version                     : undefined
});

export class BeneficiaryServiceCoverageOrganization extends BENEFICIARY_SERVICE_COVERAGE_ORGANIZATION {
    createdBy                   : string;
    createdOn                   : string;
    uuid                        : string;
    name                        : string;
    updatedBy                   : string;
    updatedOn                   : string;
    nationalProviderIdentifier  : string;
    version                     : number;

    constructor(values? : BeneficiaryServiceCoverageOrganization | IBeneficiaryServiceCoverageOrganization) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryServiceCoverageOrganization) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
