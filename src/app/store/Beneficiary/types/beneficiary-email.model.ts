import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryEmail {
    sequenceId?          : number;
    uuid?                : string;
    version?             : number;
    email                : string;
    emailType            : IKeyValuePair;
    emailNotifications   : IKeyValuePair;
    mechanismType        : IKeyValuePair;
}

export const BENEFICIARY_EMAIL = Record({
    sequenceId           : undefined,
    uuid                 : undefined,
    version              : undefined,
    email                : '',
    emailType            : new KeyValuePair(),
    emailNotifications   : new KeyValuePair(),
    mechanismType        : new KeyValuePair()
});

export class BeneficiaryEmail extends BENEFICIARY_EMAIL {
    sequenceId?          : number;
    uuid?                : string;
    version?             : number;
    email                : string;
    emailType            : KeyValuePair;
    emailNotifications   : KeyValuePair;
    mechanismType        : KeyValuePair;

    constructor(values? : BeneficiaryEmail | IBeneficiaryEmail) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryEmail) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
