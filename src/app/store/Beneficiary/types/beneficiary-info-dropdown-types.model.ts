import {
    fromJS,
    Map,
    List,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';
import {
    ILanguageType,
    LanguageType
} from '../../MetaDataTypes/types/language-type.model';

export interface IBeneficiaryInfoDropdownTypes {
    genderTypes                 : Array<IKeyValuePair>;
    languageTypes               : Array<ILanguageType>;
    salutationTypes             : Array<IKeyValuePair>;
    heightInFeetTypes           : Array<IKeyValuePair>;
    heightInInchesTypes         : Array<IKeyValuePair>;
    physicalCharacteristicTypes : Array<IKeyValuePair>;
}

export const BENEFICIARY_INFO_DROPDOWN_TYPES = Record({
    genderTypes                 : List<KeyValuePair>(),
    languageTypes               : List<LanguageType>(),
    salutationTypes             : List<KeyValuePair>(),
    heightInFeetTypes           : List<KeyValuePair>(),
    heightInInchesTypes         : List<KeyValuePair>(),
    physicalCharacteristicTypes : List<KeyValuePair>()
});

export class BeneficiaryInfoDropdownTypes extends BENEFICIARY_INFO_DROPDOWN_TYPES {
    genderTypes                 : List<KeyValuePair>;
    languageTypes               : List<LanguageType>;
    salutationTypes             : List<KeyValuePair>;
    heightInFeetTypes           : List<KeyValuePair>;
    heightInInchesTypes         : List<KeyValuePair>;
    physicalCharacteristicTypes : List<KeyValuePair>;

    constructor(values? : BeneficiaryInfoDropdownTypes | IBeneficiaryInfoDropdownTypes) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryInfoDropdownTypes) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // genderTypes
                convertedValues = convertedValues.set('genderTypes', List(convertedValues.get('genderTypes', []).map(value => new KeyValuePair(value))));

                // languageTypes
                convertedValues = convertedValues.set('languageTypes', List(convertedValues.get('languageTypes', []).map(value => new LanguageType(value))));

                // salutationTypes
                convertedValues = convertedValues.set('salutationTypes', List(convertedValues.get('salutationTypes', []).map(value => new KeyValuePair(value))));

                // heightInFeetTypes
                convertedValues = convertedValues.set('heightInFeetTypes', List(convertedValues.get('heightInFeetTypes', []).map(value => new KeyValuePair(value))));

                // heightInInchesTypes
                convertedValues = convertedValues.set('heightInInchesTypes', List(convertedValues.get('heightInInchesTypes', []).map(value => new KeyValuePair(value))));

                // physicalCharacteristicTypes
                convertedValues = convertedValues.set('physicalCharacteristicTypes', List(convertedValues.get('physicalCharacteristicTypes', []).map(value => new KeyValuePair(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
