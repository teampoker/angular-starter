import {
    fromJS,
    Map,
    List,
    Record
} from 'immutable';

import {
    IContactMechanismType,
    ContactMechanismType
} from '../../MetaDataTypes/types/contact-mechanism-type.model';
import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryContactMechanismsDropdownTypes {
    solicitationIndicatorTypes  : Array<IKeyValuePair>;
    addressTypes                : Array<IKeyValuePair>;
    emailTypes                  : Array<IKeyValuePair>;
    phoneTypes                  : Array<IKeyValuePair>;
    contactMechanismTypes       : Array<IContactMechanismType>;
}

export const BENEFICIARY_CONTACT_MECHANISMS_DROPDOWN_TYPES = Record({
    solicitationIndicatorTypes  : List<KeyValuePair>(),
    addressTypes                : List<KeyValuePair>(),
    emailTypes                  : List<KeyValuePair>(),
    phoneTypes                  : List<KeyValuePair>(),
    contactMechanismTypes       : List<ContactMechanismType>()
});

export class BeneficiaryContactMechanismsDropdownTypes extends BENEFICIARY_CONTACT_MECHANISMS_DROPDOWN_TYPES {
    solicitationIndicatorTypes  : List<KeyValuePair>;
    addressTypes                : List<KeyValuePair>;
    emailTypes                  : List<KeyValuePair>;
    phoneTypes                  : List<KeyValuePair>;
    contactMechanismTypes       : List<ContactMechanismType>;

    constructor(values? : BeneficiaryContactMechanismsDropdownTypes | IBeneficiaryContactMechanismsDropdownTypes) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryContactMechanismsDropdownTypes) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // solicitationIndicatorTypes
                convertedValues = convertedValues.set('solicitationIndicatorTypes', List(convertedValues.get('solicitationIndicatorTypes', []).map(value => new KeyValuePair(value))));

                // addressTypes
                convertedValues = convertedValues.set('addressTypes', List(convertedValues.get('addressTypes', []).map(value => new KeyValuePair(value))));

                // emailTypes
                convertedValues = convertedValues.set('emailTypes', List(convertedValues.get('emailTypes', []).map(value => new KeyValuePair(value))));

                // phoneTypes
                convertedValues = convertedValues.set('phoneTypes', List(convertedValues.get('phoneTypes', []).map(value => new KeyValuePair(value))));

                // contactMechanismTypes
                convertedValues = convertedValues.set('contactMechanismTypes', List(convertedValues.get('contactMechanismTypes', []).map(value => new ContactMechanismType(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
