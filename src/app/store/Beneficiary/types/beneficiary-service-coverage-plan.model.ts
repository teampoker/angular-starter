import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryServiceCoverageOrganization,
    BeneficiaryServiceCoverageOrganization
} from './beneficiary-service-coverage-organization.model';

export interface IBeneficiaryServiceCoveragePlan {
    uuid                        : string;
    createdOn                   : string;
    createdBy                   : string;
    fromDate                    : string;
    thruDate                    : string;
    updatedOn                   : string;
    updatedBy                   : string;
    name                        : string;
    identifier                  : string;
    version                     : number;
    serviceCoverageOrganization : IBeneficiaryServiceCoverageOrganization;
}

export const BENEFICIARY_SERVICE_COVERAGE_PLAN = Record({
    uuid                        : undefined,
    createdOn                   : '',
    createdBy                   : '',
    fromDate                    : '',
    thruDate                    : '',
    updatedOn                   : '',
    updatedBy                   : '',
    name                        : '',
    identifier                  : '',
    version                     : undefined,
    serviceCoverageOrganization : new BeneficiaryServiceCoverageOrganization()
});

export class BeneficiaryServiceCoveragePlan extends BENEFICIARY_SERVICE_COVERAGE_PLAN {
    uuid                        : string;
    createdOn                   : string;
    createdBy                   : string;
    fromDate                    : string;
    thruDate                    : string;
    updatedOn                   : string;
    updatedBy                   : string;
    name                        : string;
    identifier                  : string;
    version                     : number;
    serviceCoverageOrganization : BeneficiaryServiceCoverageOrganization;

    constructor(values? : BeneficiaryServiceCoveragePlan | IBeneficiaryServiceCoveragePlan) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryServiceCoveragePlan) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoverageOrganization
                convertedValues = convertedValues.set('serviceCoverageOrganization', new BeneficiaryServiceCoverageOrganization(convertedValues.get('serviceCoverageOrganization')));

            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
