import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryPersonServiceCoveragePlanClassification,
    BeneficiaryPersonServiceCoveragePlanClassification
} from './beneficiary-service-coverage-classification.model';
import {
    IBeneficiaryServiceCoveragePlan,
    BeneficiaryServiceCoveragePlan
} from './beneficiary-service-coverage-plan.model';

export interface IBeneficiaryServiceCoverage {
    createdBy                                   : string;
    createdOn                                   : string;
    personUuid                                  : string;
    sequenceId                                  : number;
    fromDate                                    : string;
    thruDate                                    : string;
    ordinality                                  : number;
    updatedOn                                   : string;
    updatedBy                                   : string;
    version                                     : number;
    serviceCoveragePlan                         : IBeneficiaryServiceCoveragePlan;
    personServiceCoveragePlanClassifications    : Array<IBeneficiaryPersonServiceCoveragePlanClassification>;
}

export const BENEFICIARY_SERVICE_COVERAGE = Record({
    createdBy                                   : '',
    createdOn                                   : '',
    personUuid                                  : '',
    sequenceId                                  : undefined,
    fromDate                                    : '',
    thruDate                                    : '',
    ordinality                                  : 0,
    updatedOn                                   : '',
    updatedBy                                   : '',
    version                                     : undefined,
    serviceCoveragePlan                         : new BeneficiaryServiceCoveragePlan(),
    personServiceCoveragePlanClassifications    : List<BeneficiaryPersonServiceCoveragePlanClassification>()
});

export class BeneficiaryServiceCoverage extends BENEFICIARY_SERVICE_COVERAGE {
    createdBy                                   : string;
    createdOn                                   : string;
    personUuid                                  : string;
    sequenceId                                  : number;
    fromDate                                    : string;
    thruDate                                    : string;
    ordinality                                  : number;
    updatedOn                                   : string;
    updatedBy                                   : string;
    version                                     : number;
    serviceCoveragePlan                         : BeneficiaryServiceCoveragePlan;
    personServiceCoveragePlanClassifications    : List<BeneficiaryPersonServiceCoveragePlanClassification>;

    constructor(values? : BeneficiaryServiceCoverage | IBeneficiaryServiceCoverage) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryServiceCoverage) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoveragePlan
                convertedValues = convertedValues.set('serviceCoveragePlan', new BeneficiaryServiceCoveragePlan(convertedValues.get('serviceCoveragePlan')));

                // personServiceCoveragePlanClassifications
                convertedValues = convertedValues.set('personServiceCoveragePlanClassifications', List(convertedValues.get('personServiceCoveragePlanClassifications', []).map(value => new BeneficiaryPersonServiceCoveragePlanClassification(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
