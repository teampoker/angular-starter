import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    BeneficiaryServiceCoveragePlanClassification,
    IBeneficiaryServiceCoveragePlanClassification
} from './beneficiary-service-coverage-plan-classification.model';

export interface IBeneficiaryPersonServiceCoveragePlanClassification {
    createdBy                           : string;
    createdOn                           : string;
    updatedBy                           : string;
    updatedOn                           : string;
    value                               : string;
    fromDate                            : string;
    thruDate                            : string;
    sequenceId                          : number;
    version                             : number;
    serviceCoveragePlanClassification   : IBeneficiaryServiceCoveragePlanClassification;
}

export const BENEFICIARY_PERSON_SERVICE_COVERAGE_PLAN_CLASSIFICATION = Record({
    createdBy                           : '',
    createdOn                           : '',
    updatedBy                           : '',
    updatedOn                           : '',
    value                               : '',
    fromDate                            : '',
    thruDate                            : '',
    sequenceId                          : undefined,
    version                             : undefined,
    serviceCoveragePlanClassification   : new BeneficiaryServiceCoveragePlanClassification()
});

export class BeneficiaryPersonServiceCoveragePlanClassification extends BENEFICIARY_PERSON_SERVICE_COVERAGE_PLAN_CLASSIFICATION {
    createdBy                           : string;
    createdOn                           : string;
    updatedBy                           : string;
    updatedOn                           : string;
    value                               : string;
    fromDate                            : string;
    thruDate                            : string;
    sequenceId                          : number;
    version                             : number;
    serviceCoveragePlanClassification   : BeneficiaryServiceCoveragePlanClassification;

    constructor(values? : BeneficiaryPersonServiceCoveragePlanClassification | IBeneficiaryPersonServiceCoveragePlanClassification) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryPersonServiceCoveragePlanClassification) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoveragePlanClassification
                convertedValues = convertedValues.set('serviceCoveragePlanClassification',  new BeneficiaryServiceCoveragePlanClassification(convertedValues.get('serviceCoveragePlanClassification')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
