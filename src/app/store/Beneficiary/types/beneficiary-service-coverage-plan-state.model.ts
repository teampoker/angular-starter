import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryServiceCoveragePlanClassificationState,
    BeneficiaryServiceCoveragePlanClassificationState
} from './beneficiary-service-coverage-plan-classification-state.model';

export interface IBeneficiaryServiceCoveragePlanState {
    id                                  : string;
    value                               : string;
    serviceCoveragePlanClassifications  : Array<IBeneficiaryServiceCoveragePlanClassificationState>;
}

export const BENEFICIARY_SERVICE_COVERAGE_PLAN_STATE = Record({
    id                                  : '',
    value                               : '',
    serviceCoveragePlanClassifications  : List<BeneficiaryServiceCoveragePlanClassificationState>()
});

export class BeneficiaryServiceCoveragePlanState extends BENEFICIARY_SERVICE_COVERAGE_PLAN_STATE {
    id                                  : string;
    value                               : string;
    serviceCoveragePlanClassifications  : List<BeneficiaryServiceCoveragePlanClassificationState>;

    constructor(values? : BeneficiaryServiceCoveragePlanState | IBeneficiaryServiceCoveragePlanState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryServiceCoveragePlanState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // coveragePlanClassifications
                convertedValues = convertedValues.set('serviceCoveragePlanClassifications', List(convertedValues.get('serviceCoveragePlanClassifications', [])
                                                                                                                .sort((a, b) => a.get('ordinality') > b.get('ordinality'))
                                                                                                                .map(value => BeneficiaryServiceCoveragePlanClassificationState.fromApi(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
