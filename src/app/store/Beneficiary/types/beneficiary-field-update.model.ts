import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryFieldUpdate {
    fieldName               : string;
    fieldValue              : string | boolean | number | IKeyValuePair;
    primaryUpdateIndex?     : number;
    secondaryUpdateIndex?   : number;
    tertiaryUpdateIndex?    : number;
}

const BENEFICIARY_FIELD_UPDATE = Record({
    fieldName               : '',
    fieldValue              : undefined,
    primaryUpdateIndex      : 0,
    secondaryUpdateIndex    : 0,
    tertiaryUpdateIndex     : 0
});

export class BeneficiaryFieldUpdate extends BENEFICIARY_FIELD_UPDATE {
    fieldName               : string;
    fieldValue              : string | boolean | number | KeyValuePair;
    primaryUpdateIndex?     : number;
    secondaryUpdateIndex?   : number;
    tertiaryUpdateIndex?    : number;

    constructor(values? : BeneficiaryFieldUpdate | IBeneficiaryFieldUpdate) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryFieldUpdate) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }
}
