import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryAddress {
    sequenceId?             : number;
    uuid?                   : string;
    version?                : number;
    street                  : string;
    city                    : string;
    state                   : string;
    postalCode              : string;
    apt                     : string;
    geographicDetails       : string;
    addressNotifications    : IKeyValuePair;
    addressType             : IKeyValuePair;
    mechanismType           : IKeyValuePair;
}

export const BENEFICIARY_ADDRESS = Record({
    sequenceId              : undefined,
    uuid                    : undefined,
    version                 : undefined,
    street                  : '',
    city                    : '',
    state                   : '',
    postalCode              : '',
    apt                     : '',
    geographicDetails       : '',
    addressNotifications    : new KeyValuePair(),
    addressType             : new KeyValuePair(),
    mechanismType           : new KeyValuePair()
});

export class BeneficiaryAddress extends BENEFICIARY_ADDRESS {
    sequenceId?             : number;
    uuid?                   : string;
    version?                : number;
    street                  : string;
    city                    : string;
    state                   : string;
    postalCode              : string;
    apt                     : string;
    geographicDetails       : string;
    addressNotifications    : KeyValuePair;
    addressType             : KeyValuePair;
    mechanismType           : KeyValuePair;

    constructor(values? : BeneficiaryAddress | IBeneficiaryAddress) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryAddress) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
