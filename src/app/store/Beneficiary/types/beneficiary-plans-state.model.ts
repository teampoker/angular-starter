import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryPlansStateSummary,
    BeneficiaryPlansStateSummary
} from './beneficiary-plans-state-summary.model';

export interface IBeneficiaryPlansState {
    isAddPlanActive         : boolean;
    isEditPlanActive        : boolean;
    addPlanIndex            : number;
    editPlanIndex           : number;
    organizationIndex       : number;
    coveragePlanIndex       : number;
    isPlanFormValid         : boolean;
    plansStateSummary       : Array<IBeneficiaryPlansStateSummary>;
    newPlanStateSummary     : BeneficiaryPlansStateSummary;
}

export const BENEFICIARY_PLANS_STATE = Record({
    isAddPlanActive         : false,
    isEditPlanActive        : false,
    addPlanIndex            : -1,
    editPlanIndex           : -1,
    organizationIndex       : -1,
    coveragePlanIndex       : -1,
    isPlanFormValid         : false,
    plansStateSummary       : List<BeneficiaryPlansStateSummary>(),
    newPlanStateSummary     : new BeneficiaryPlansStateSummary()
});

export class BeneficiaryPlansState extends BENEFICIARY_PLANS_STATE {
    isAddPlanActive         : boolean;
    isEditPlanActive        : boolean;
    addPlanIndex            : number;
    editPlanIndex           : number;
    organizationIndex       : number;
    coveragePlanIndex       : number;
    isPlanFormValid         : boolean;
    plansStateSummary       : List<BeneficiaryPlansStateSummary>;
    /**
     * Used for an active add; temporary space for working a new plan.
     */
    newPlanStateSummary     : BeneficiaryPlansStateSummary;

    constructor(values? : BeneficiaryPlansState | IBeneficiaryPlansState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryPlansState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // plansStateSummary
                convertedValues = convertedValues.set('plansStateSummary', List(convertedValues.get('plansStateSummary', []).map(value => new BeneficiaryPlansStateSummary(value))));

                // newPlanStateSummary
                convertedValues = convertedValues.set('newPlansStateSummary', convertedValues.get('newPlansStateSummary', List()).map(value => new BeneficiaryPlansStateSummary(value)));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
