import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryContactMechanismsDropdownTypes,
    BeneficiaryContactMechanismsDropdownTypes
} from './beneficiary-contact-mechanisms-dropdown-types.model';
import {
    IBeneficiaryContactMechanismStateDetail,
    BeneficiaryContactMechanismStateDetail
} from './beneficiary-contact-mechanism-state-detail.model';

export interface IBeneficiaryContactMechanismsState {
    isContactMechanismsEditing  : boolean;
    contactMechanismsState      : IBeneficiaryContactMechanismStateDetail;
    dropdownTypes               : IBeneficiaryContactMechanismsDropdownTypes;
}

export const BENEFICIARY_CONTACT_MECHANISMS_STATE = Record({
    isContactMechanismsEditing  : false,
    contactMechanismsState      : new BeneficiaryContactMechanismStateDetail(),
    dropdownTypes               : new BeneficiaryContactMechanismsDropdownTypes()
});

export class BeneficiaryContactMechanismsState extends BENEFICIARY_CONTACT_MECHANISMS_STATE {
    isContactMechanismsEditing  : boolean;
    contactMechanismsState      : BeneficiaryContactMechanismStateDetail;
    dropdownTypes               : BeneficiaryContactMechanismsDropdownTypes;

    constructor(values? : BeneficiaryContactMechanismsState | IBeneficiaryContactMechanismsState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryContactMechanismsState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // contactMechanismsState
                convertedValues = convertedValues.set('contactMechanismsState', new BeneficiaryContactMechanismStateDetail(convertedValues.get('contactMechanismsState')));

                // dropdownTypes
                convertedValues = convertedValues.set('dropdownTypes', new BeneficiaryContactMechanismsDropdownTypes(convertedValues.get('dropdownTypes')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
