import {
    fromJS,
    Map,
    List,
    Record
} from 'immutable';

import {
    IBeneficiaryAddress,
    BeneficiaryAddress
} from './beneficiary-address.model';
import {
    IBeneficiaryEmail,
    BeneficiaryEmail
} from './beneficiary-email.model';
import {
    IBeneficiaryPhone,
    BeneficiaryPhone
} from './beneficiary-phone.model';

export interface IBeneficiaryContactMechanismStateDetail {
    addresses   : Array<IBeneficiaryAddress>;
    emails      : Array<IBeneficiaryEmail>;
    phones      : Array<IBeneficiaryPhone>;
    tempAddress : IBeneficiaryAddress;
    tempEmail   : IBeneficiaryEmail;
    tempPhone   : IBeneficiaryPhone;
}

export const BENEFICIARY_CONTACT_MECHANISM_STATE_DETAIL = Record({
    addresses   : List<BeneficiaryAddress>(),
    emails      : List<BeneficiaryEmail>(),
    phones      : List<BeneficiaryPhone>(),
    tempAddress : new BeneficiaryAddress(),
    tempEmail   : new BeneficiaryEmail(),
    tempPhone   : new BeneficiaryPhone()
});

export class BeneficiaryContactMechanismStateDetail extends BENEFICIARY_CONTACT_MECHANISM_STATE_DETAIL {
    addresses   : List<BeneficiaryAddress>;
    emails      : List<BeneficiaryEmail>;
    phones      : List<BeneficiaryPhone>;
    tempAddress : BeneficiaryAddress;
    tempEmail   : BeneficiaryEmail;
    tempPhone   : BeneficiaryPhone;

    constructor(values? : BeneficiaryContactMechanismStateDetail | IBeneficiaryContactMechanismStateDetail) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryContactMechanismStateDetail) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // emails
                convertedValues = convertedValues.set('emails', List(convertedValues.get('emails', []).map(value => new BeneficiaryEmail(value))));

                // phones
                convertedValues = convertedValues.set('phones', List(convertedValues.get('phones', []).map(value => new BeneficiaryPhone(value))));

                // addresses
                convertedValues = convertedValues.set('addresses', List(convertedValues.get('addresses', []).map(value => new BeneficiaryAddress(value))));

                // tempAddress
                convertedValues = convertedValues.set('tempAddress', new BeneficiaryAddress(convertedValues.get('tempAddress')));

                // tempEmail
                convertedValues = convertedValues.set('tempEmail', new BeneficiaryAddress(convertedValues.get('tempEmail')));

                // tempPhone
                convertedValues = convertedValues.set('tempPhone', new BeneficiaryAddress(convertedValues.get('tempPhone')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
