import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryServiceCoveragePlanClassificationState {
    id                              : string;                                   // classification identifier
    value                           : string;                                   // hard-coded field label to display
    maximum                         : number;                                   // number of times to display this field in UI
    isRequired                      : boolean;                                  // is user required to enter a value?
    validationFormat?               : string;                                   // validation RegEx
    selectedValue                   : string | boolean | IKeyValuePair;         // user entered value for this field
    classificationType              : IKeyValuePair;                            // basically what "type" of field this is
    benefitClassificationValues     : Array<IKeyValuePair>;                     // if this is populated then the field is a dropdown selection
}

export const BENEFICIARY_SERVICE_COVERAGE_PLAN_CLASSIFICATION_STATE = Record({
    id                              : '',
    value                           : '',
    maximum                         : 0,
    isRequired                      : false,
    validationFormat                : '',
    selectedValue                   : '',
    classificationType              : new KeyValuePair(),
    benefitClassificationValues     : List<KeyValuePair>()
});

const apiMap = Map({
    id                          : 'id',
    value                       : 'value',
    minimum                     : 'isRequired',
    maximum                     : 'maximum',
    validationFormat            : 'validationFormat',
    classificationType          : 'classificationType',
    benefitClassificationValues : 'benefitClassificationValues'
});

export class BeneficiaryServiceCoveragePlanClassificationState extends BENEFICIARY_SERVICE_COVERAGE_PLAN_CLASSIFICATION_STATE {
    id                              : string;
    value                           : string;
    maximum                         : number;
    isRequired                      : boolean;
    validationFormat?               : string;
    selectedValue                   : string | boolean | KeyValuePair;
    classificationType              : KeyValuePair;
    benefitClassificationValues     : List<KeyValuePair>;

    constructor(values? : BeneficiaryServiceCoveragePlanClassificationState | IBeneficiaryServiceCoveragePlanClassificationState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryServiceCoveragePlanClassificationState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // beneficiary
                convertedValues = convertedValues.set('classificationType', new KeyValuePair(convertedValues.get('classificationType')));

                // benefitClassificationValues
                convertedValues = convertedValues.set('benefitClassificationValues', List(convertedValues.get('benefitClassificationValues', []).map(value => new KeyValuePair(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }

    static fromApi(rawObject : any) : BeneficiaryServiceCoveragePlanClassificationState {
        const newObject : any = {};

        if (Map.isMap(rawObject)) {
            apiMap.forEach((translatedKey, apiKey) => {

                if (apiKey === 'minimum') {
                    newObject[translatedKey] = rawObject.get(apiKey) === 1;
                }
                else {
                    newObject[translatedKey] = rawObject.get(apiKey);
                }
            });
        }
        else {
            apiMap.forEach((translatedKey, apiKey) => {
                if (apiKey === 'minimum') {
                    newObject[translatedKey] = rawObject[apiKey] === 1;
                }
                else {
                    newObject[translatedKey] = rawObject[apiKey];
                }
            });
        }

        return new BeneficiaryServiceCoveragePlanClassificationState(newObject);
    }
}
