import {
    fromJS,
    Map,
    List,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';
import {
    IBeneficiaryMedicalConditionState,
    BeneficiaryMedicalConditionState
} from './beneficiary-medical-condition-state.model';

export interface IBeneficiaryMedicalConditionsState {
    isAddMedicalConditionActive         : boolean;
    isAddMedicalConditionValid          : boolean;
    tempMedicalCondition                : IBeneficiaryMedicalConditionState;
    conditionsState                     : Array<IBeneficiaryMedicalConditionState>;
    medicalConditionTypes               : Array<IKeyValuePair>;
}

export const BENEFICIARY_MEDICAL_CONDITIONS_STATE = Record({
    isAddMedicalConditionActive         : false,
    isAddMedicalConditionValid          : true,
    tempMedicalCondition                : new BeneficiaryMedicalConditionState(),
    conditionsState                     : List<BeneficiaryMedicalConditionState>(),
    medicalConditionTypes               : List<KeyValuePair>()
});

export class BeneficiaryMedicalConditionsState extends BENEFICIARY_MEDICAL_CONDITIONS_STATE {
    isAddMedicalConditionActive         : boolean;
    isAddMedicalConditionValid          : boolean;
    tempMedicalCondition                : BeneficiaryMedicalConditionState;
    conditionsState                     : List<BeneficiaryMedicalConditionState>;
    medicalConditionTypes               : List<KeyValuePair>;

    constructor(values? : BeneficiaryMedicalConditionsState | IBeneficiaryMedicalConditionsState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryMedicalConditionsState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // tempMedicalCondition
                convertedValues = convertedValues.set('tempMedicalCondition', new BeneficiaryMedicalConditionState(convertedValues.get('tempMedicalCondition')));

                // conditionsState
                convertedValues = convertedValues.set('conditionsState', List(convertedValues.get('conditionsState', []).map(value => new BeneficiaryMedicalConditionState(value))));

                // medicalConditionTypes
                convertedValues = convertedValues.set('medicalConditionTypes', List(convertedValues.get('medicalConditionTypes', []).map(value => new KeyValuePair(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
