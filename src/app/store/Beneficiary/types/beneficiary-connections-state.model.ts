import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryConnectionsDropdownTypes,
    BeneficiaryConnectionsDropdownTypes
} from './beneficiary-connections-dropdown-types.model';
import {
    IBeneficiaryConnectionsStateDetail,
    BeneficiaryConnectionsStateDetail
} from './beneficiary-connections-state-detail.model';
import {
    IBeneficiary,
    Beneficiary
} from './beneficiary.model';

export interface IBeneficiaryConnectionsState {
    isAddConnectionActive       : boolean;
    isEditConnectionActive      : boolean;
    isAddPhoneActive            : boolean;
    addConnectionIndex          : number;
    editConnectionIndex         : number;
    isAddConnectionExpanded     : boolean;
    isConnectionAddedManually   : boolean;
    isConnectionAddedFromSearch : boolean;
    manuallyAddedConnection     : IBeneficiary;
    connectionsStateDetail      : Array<IBeneficiaryConnectionsStateDetail>;
    dropdownTypes               : IBeneficiaryConnectionsDropdownTypes;
}

export const BENEFICIARY_CONNECTIONS_STATE = Record({
    isAddConnectionActive       : false,
    isEditConnectionActive      : false,
    isAddPhoneActive            : false,
    addConnectionIndex          : -1,
    editConnectionIndex         : -1,
    isAddConnectionExpanded     : false,
    isConnectionAddedManually   : false,
    isConnectionAddedFromSearch : false,
    manuallyAddedConnection     : new Beneficiary(),
    connectionsStateDetail      : List<BeneficiaryConnectionsStateDetail>(),
    dropdownTypes               : new BeneficiaryConnectionsDropdownTypes()
});

export class BeneficiaryConnectionsState extends BENEFICIARY_CONNECTIONS_STATE {
    isAddConnectionActive       : boolean;
    isEditConnectionActive      : boolean;
    isAddPhoneActive            : boolean;
    addConnectionIndex          : number;
    editConnectionIndex         : number;
    isAddConnectionExpanded     : boolean;
    isConnectionAddedManually   : boolean;
    isConnectionAddedFromSearch : boolean;
    manuallyAddedConnection     : Beneficiary;
    connectionsStateDetail      : List<BeneficiaryConnectionsStateDetail>;
    dropdownTypes               : BeneficiaryConnectionsDropdownTypes;

    constructor(values? : BeneficiaryConnectionsState | IBeneficiaryConnectionsState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryConnectionsState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // connectionsStateDetail
                convertedValues = convertedValues.set('connectionsStateDetail', List(convertedValues.get('connectionsStateDetail', []).map(value => new BeneficiaryConnectionsStateDetail(value))));

                // dropdownTypes
                convertedValues = convertedValues.set('dropdownTypes', new BeneficiaryConnectionsDropdownTypes(convertedValues.get('dropdownTypes')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
