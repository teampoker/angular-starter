import {
    fromJS,
    Map,
    Record
} from 'immutable';

export enum EnumConnectionTypes {
    PARENT_FAMILY,
    SOCIAL_WORKER,
    NURSE_DOCTOR,
    PLAN_CLIENT,
    EMERGENCY_CONTACT,
    DRIVER
}

export interface IBeneficiaryConnectionTypes {
    parentFamily     : boolean;
    socialWorker     : boolean;
    nurseDoctor      : boolean;
    planClient       : boolean;
    emergencyContact : boolean;
    driver           : boolean;
}

export const BENEFICIARY_CONNECTION_TYPES = Record({
    parentFamily     : false,
    socialWorker     : false,
    nurseDoctor      : false,
    planClient       : false,
    emergencyContact : false,
    driver           : false
});

export class BeneficiaryConnectionTypes extends BENEFICIARY_CONNECTION_TYPES {
    parentFamily     : boolean;
    socialWorker     : boolean;
    nurseDoctor      : boolean;
    planClient       : boolean;
    emergencyContact : boolean;
    driver           : boolean;

    constructor(values? : BeneficiaryConnectionTypes | IBeneficiaryConnectionTypes) {
        let convertedValues  : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryConnectionTypes) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
