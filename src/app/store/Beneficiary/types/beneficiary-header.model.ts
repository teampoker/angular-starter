import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryAddress,
    BeneficiaryAddress
} from './beneficiary-address.model';
import {
    IBeneficiaryEmail,
    BeneficiaryEmail
} from './beneficiary-email.model';
import {
    IBeneficiaryPhone,
    BeneficiaryPhone
} from './beneficiary-phone.model';

export enum EnumBeneficiaryHeaderType {
    BENEFICIARY_PROFILE,
    CREATE_NEW_PROFILE,
    BENEFICIARY_MAIN
}

export interface IBeneficiaryHeader {
    title           : string;
    headerType      : EnumBeneficiaryHeaderType;
    firstName       : string;
    middleName      : string;
    lastName        : string;
    birthDate       : string;
    primaryAddress  : IBeneficiaryAddress;
    primaryEmail    : IBeneficiaryEmail;
    primaryPhone    : IBeneficiaryPhone;
}

export const BENEFICIARY_HEADER = Record({
    title           : '',
    headerType      : EnumBeneficiaryHeaderType.BENEFICIARY_MAIN,
    firstName       : '',
    middleName      : '',
    lastName        : '',
    birthDate       : '',
    primaryAddress  : new BeneficiaryAddress(),
    primaryEmail    : new BeneficiaryEmail(),
    primaryPhone    : new BeneficiaryPhone()
});

export class BeneficiaryHeader extends BENEFICIARY_HEADER {
    title           : string;
    headerType      : EnumBeneficiaryHeaderType;
    firstName       : string;
    middleName      : string;
    lastName        : string;
    birthDate       : string;
    primaryAddress  : BeneficiaryAddress;
    primaryEmail    : BeneficiaryEmail;
    primaryPhone    : BeneficiaryPhone;

    constructor(values? : BeneficiaryHeader | IBeneficiaryHeader) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryHeader) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // primaryAddress
                convertedValues = convertedValues.set('primaryAddress', new BeneficiaryAddress(convertedValues.get('primaryAddress')));

                // primaryEmail
                convertedValues = convertedValues.set('primaryEmail', new BeneficiaryEmail(convertedValues.get('primaryEmail')));

                // primaryPhone
                convertedValues = convertedValues.set('primaryPhone', new BeneficiaryPhone(convertedValues.get('primaryPhone')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
