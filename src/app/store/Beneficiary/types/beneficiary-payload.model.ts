import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';
import {
    IServiceCoverageOrganizationType,
    ServiceCoverageOrganizationType
} from '../../MetaDataTypes/types/service-coverage-organization-type.model';

export interface IBeneficiaryPayload {
    beneficiary         : any;
    serviceCoverages    : Array<IServiceCoverageOrganizationType>;
    classificationTypes : Array<IKeyValuePair>;
}

export const BENEFICIARY_PAYLOAD = Record({
    beneficiary         : undefined,
    serviceCoverages    : List<ServiceCoverageOrganizationType>(),
    classificationTypes : List<KeyValuePair>()
});

export class BeneficiaryPayload extends BENEFICIARY_PAYLOAD {
    beneficiary         : any;
    serviceCoverages    : List<ServiceCoverageOrganizationType>;
    classificationTypes : List<KeyValuePair>;

    constructor(values? : BeneficiaryPayload | IBeneficiaryPayload) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryPayload) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoverages
                convertedValues = convertedValues.set('serviceCoverages', List(convertedValues.get('serviceCoverages', []).map(value => new ServiceCoverageOrganizationType(value))));

                // classificationTypes
                convertedValues = convertedValues.set('classificationTypes', List(convertedValues.get('classificationTypes', []).map(value => new KeyValuePair(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
