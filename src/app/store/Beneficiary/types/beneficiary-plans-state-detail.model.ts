import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryServiceCoveragePlanState,
    BeneficiaryServiceCoveragePlanState
} from './beneficiary-service-coverage-plan-state.model';
import {ServiceCoverageOrganizationType} from '../../MetaDataTypes/types/service-coverage-organization-type.model';

export interface IBeneficiaryPlansStateDetail {
    id                      : string;
    value                   : string;
    serviceCoveragePlans    : Array<IBeneficiaryServiceCoveragePlanState>;
}

export const BENEFICIARY_PLANS_STATE_DETAIL = Record({
    id                      : '',
    value                   : '',
    serviceCoveragePlans    : List<BeneficiaryServiceCoveragePlanState>()
});

export class BeneficiaryPlansStateDetail extends BENEFICIARY_PLANS_STATE_DETAIL {
    id                      : string;
    value                   : string;
    serviceCoveragePlans    : List<BeneficiaryServiceCoveragePlanState>;

    constructor(values? : BeneficiaryPlansStateDetail | IBeneficiaryPlansStateDetail | ServiceCoverageOrganizationType) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryPlansStateDetail) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // coveragePlans
                convertedValues = convertedValues.set('serviceCoveragePlans', List(convertedValues.get('serviceCoveragePlans', []).map(value => new BeneficiaryServiceCoveragePlanState(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * transform raw JSON into BeneficiaryPlansStateDetail Immutable Record type
 * @param rawObject
 * @returns {Exception}
 */
export function fromApi(rawObject : any) : BeneficiaryPlansStateDetail {
    return new BeneficiaryPlansStateDetail().withMutations(newPlanSummary => newPlanSummary
        .set('id', rawObject.id)
        .set('value', rawObject.value)
        .set('serviceCoveragePlans', rawObject.serviceCoveragePlans ? List<BeneficiaryServiceCoveragePlanState>(rawObject.serviceCoveragePlans.map(value => new BeneficiaryServiceCoveragePlanState(value))) : undefined)) as BeneficiaryPlansStateDetail;
}
