import {
    fromJS,
    Map,
    List,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';
import {
    IContactMechanismType,
    ContactMechanismType
} from '../../MetaDataTypes/types/contact-mechanism-type.model';

export interface IBeneficiaryConnectionsDropdownTypes {
    solicitationIndicatorTypes  : Array<IKeyValuePair>;
    salutations                 : Array<IKeyValuePair>;
    phoneTypes                  : Array<IKeyValuePair>;
    personConnectionTypes       : Array<IKeyValuePair>;
    contactMechanismTypes       : Array<IContactMechanismType>;
}

export const BENEFICIARY_CONNECTIONS_DROPDOWN_TYPES = Record({
    solicitationIndicatorTypes  : List<KeyValuePair>(),
    salutations                 : List<KeyValuePair>(),
    phoneTypes                  : List<KeyValuePair>(),
    personConnectionTypes       : List<KeyValuePair>(),
    contactMechanismTypes       : List<ContactMechanismType>()
});

export class BeneficiaryConnectionsDropdownTypes extends BENEFICIARY_CONNECTIONS_DROPDOWN_TYPES {
    solicitationIndicatorTypes  : List<KeyValuePair>;
    salutations                 : List<KeyValuePair>;
    phoneTypes                  : List<KeyValuePair>;
    personConnectionTypes       : List<KeyValuePair>;
    contactMechanismTypes       : List<ContactMechanismType>;

    constructor(values? : BeneficiaryConnectionsDropdownTypes | IBeneficiaryConnectionsDropdownTypes) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryConnectionsDropdownTypes) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // solicitationIndicatorTypes
                convertedValues = convertedValues.set('solicitationIndicatorTypes', List(convertedValues.get('solicitationIndicatorTypes', []).map(value => new KeyValuePair(value))));

                // salutations
                convertedValues = convertedValues.set('salutations', List(convertedValues.get('salutations', []).map(value => new KeyValuePair(value))));

                // phoneTypes
                convertedValues = convertedValues.set('phoneTypes', List(convertedValues.get('phoneTypes', []).map(value => new KeyValuePair(value))));

                // personConnectionTypes
                convertedValues = convertedValues.set('personConnectionTypes', List(convertedValues.get('personConnectionTypes', []).map(value => new KeyValuePair(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
