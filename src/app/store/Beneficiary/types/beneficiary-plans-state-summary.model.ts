import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    fromApi as beneficiaryPlansStateDetailFromApi,
    IBeneficiaryPlansStateDetail,
    BeneficiaryPlansStateDetail
} from './beneficiary-plans-state-detail.model';

export interface IBeneficiaryPlansStateSummary {
    organization            : string;       // serviceCoveragePlan.serviceCoverageOrganization.name
    organizationUuid        : string;       // serviceCoveragePlan.serviceCoverageOrganization.uuid
    planDescription         : string;       // serviceCoveragePlan.name
    planUuid                : string;       // serviceCoveragePlan.uuid
    fromDate                : string;
    thruDate                : string;
    planIdLabel             : string;
    planIdValue             : string;
    plansStateDetail?       : Array<IBeneficiaryPlansStateDetail>;
}

export const BENEFICIARY_PLANS_STATE_SUMMARY = Record({
    organization            : 'SELECT',
    organizationUuid        : '',
    planDescription         : 'SELECT',
    planUuid                : '',
    fromDate                : '',
    thruDate                : '',
    planIdLabel             : '',
    planIdValue             : 'N/A',
    plansStateDetail        : List<BeneficiaryPlansStateDetail>()
});

export class BeneficiaryPlansStateSummary extends BENEFICIARY_PLANS_STATE_SUMMARY {
    organization            : string;
    organizationUuid        : string;
    planDescription         : string;
    planUuid                : string;
    fromDate                : string;
    thruDate                : string;
    planIdLabel             : string;
    planIdValue             : string;
    plansStateDetail?       : List<BeneficiaryPlansStateDetail>;

    constructor(values? : BeneficiaryPlansStateSummary | IBeneficiaryPlansStateSummary) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryPlansStateSummary) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // plansStateDetail
                convertedValues = convertedValues.set('plansStateDetail', List(convertedValues.get('plansStateDetail', []).map(value => new BeneficiaryPlansStateDetail(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * transform raw JSON into BeneficiaryPlansStateSummary Immutable Record type
 * @param rawObject
 * @returns {Exception}
 */
export function fromApi(rawObject : any) : BeneficiaryPlansStateSummary {
    return new BeneficiaryPlansStateSummary().withMutations(newPlanSummary => newPlanSummary
        .set('organization', rawObject.organization)
        .set('organizationUuid', rawObject.organizationUuid)
        .set('planDescription', rawObject.planDescription)
        .set('planUuid', rawObject.planUuid)
        .set('fromDate', rawObject.fromDate)
        .set('thruDate', rawObject.thruDate)
        .set('planIdLabel', rawObject.planIdLabel)
        .set('planIdValue', rawObject.planIdValue)
        .set('plansStateDetail', rawObject.plansStateDetail ? List<BeneficiaryPlansStateDetail>(rawObject.plansStateDetail.map(value => beneficiaryPlansStateDetailFromApi(value))) : undefined)) as BeneficiaryPlansStateSummary;
}
