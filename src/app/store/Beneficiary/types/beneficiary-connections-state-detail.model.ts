import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryPhone,
    BeneficiaryPhone
} from './beneficiary-phone.model';
import {
    IBeneficiaryConnectionTypes,
    BeneficiaryConnectionTypes
} from './beneficiary-connection-types.model';

export interface IBeneficiaryConnectionsStateDetail {
    uuid                : string;
    firstName           : string;
    middleName          : string;
    lastName            : string;
    personalTitle       : string;
    suffix              : string;
    gender              : string;
    fromDate            : string;
    thruDate            : string;
    birthDate           : string;
    birthDateEnabled    : boolean;
    phones              : Array<IBeneficiaryPhone>;
    connectionTypes     : IBeneficiaryConnectionTypes;
}

export const BENEFICIARY_CONNECTIONS_STATE_DETAIL = Record({
    uuid                : undefined,
    firstName           : '',
    middleName          : '',
    lastName            : '',
    personalTitle       : 'SELECT',
    suffix              : '',
    gender              : '',
    fromDate            : '',
    thruDate            : '',
    birthDate           : '',
    birthDateEnabled    : false,
    phones              : List<BeneficiaryPhone>(),
    connectionTypes     : new BeneficiaryConnectionTypes()
});

export class BeneficiaryConnectionsStateDetail extends BENEFICIARY_CONNECTIONS_STATE_DETAIL {
    uuid                : string;
    firstName           : string;
    middleName          : string;
    lastName            : string;
    personalTitle       : string;
    suffix              : string;
    gender              : string;
    fromDate            : string;
    thruDate            : string;
    birthDate           : string;
    birthDateEnabled    : boolean;
    phones              : List<BeneficiaryPhone>;
    connectionTypes     : BeneficiaryConnectionTypes;

    constructor(values? : BeneficiaryConnectionsStateDetail | IBeneficiaryConnectionsStateDetail) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryConnectionsStateDetail) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // phones
                convertedValues = convertedValues.set('phones', List(convertedValues.get('phones', []).map(value => new BeneficiaryPhone(value))));

                // connectionTypes
                convertedValues = convertedValues.set('connectionTypes', new BeneficiaryConnectionTypes(convertedValues.get('connectionTypes')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
