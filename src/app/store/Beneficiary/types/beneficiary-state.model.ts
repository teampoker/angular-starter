import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiary,
    Beneficiary
} from './beneficiary.model';
import {
    IBeneficiaryPlansState,
    BeneficiaryPlansState
} from './beneficiary-plans-state.model';
import {
    IBeneficiaryConnectionsState,
    BeneficiaryConnectionsState
} from './beneficiary-connections-state.model';
import {
    IBeneficiaryMedicalConditionsState,
    BeneficiaryMedicalConditionsState
} from './beneficiary-medical-conditions-state.model';
import {
    IBeneficiaryContactMechanismsState,
    BeneficiaryContactMechanismsState
} from './beneficiary-contact-mechanisms-state.model';
import {
    IBeneficiaryInfoState,
    BeneficiaryInfoState
} from './beneficiary-info-state.model';
import {
    IBeneficiarySpecialRequirementsState,
    BeneficiarySpecialRequirementsState
} from './beneficiary-special-requirements-state.model';
import {
    IBeneficiaryBackup,
    BeneficiaryBackup
} from './beneficiary-backup.model';
import {IRequestable} from '../../App/requestable.interface';

export interface IBeneficiaryState {
    isWaiting                           : boolean;
    isFetching                          : boolean;
    newProfileActive                    : boolean;
    isCreateProfileMappingFinished      : boolean;
    isCreateProfileSaveActive           : boolean;
    beneficiary                         : IBeneficiary;
    beneficiaryBackup                   : IBeneficiaryBackup;
    beneficiaryInfoState                : IBeneficiaryInfoState;
    beneficiarySpecialRequirementsState : IBeneficiarySpecialRequirementsState;
    beneficiaryContactMechanismsState   : IBeneficiaryContactMechanismsState;
    beneficiaryPlansState               : IBeneficiaryPlansState;
    beneficiaryMedicalConditionsState   : IBeneficiaryMedicalConditionsState;
    beneficiaryConnectionsState         : IBeneficiaryConnectionsState;
}

export const BENEFICIARY_STATE = Record({
    isWaiting                           : false,
    isFetching                          : false,
    newProfileActive                    : false,
    isCreateProfileMappingFinished      : false,
    isCreateProfileSaveActive           : false,
    beneficiary                         : new Beneficiary(),
    beneficiaryBackup                   : new BeneficiaryBackup(),
    beneficiaryInfoState                : new BeneficiaryInfoState(),
    beneficiarySpecialRequirementsState : new BeneficiarySpecialRequirementsState(),
    beneficiaryContactMechanismsState   : new BeneficiaryContactMechanismsState(),
    beneficiaryPlansState               : new BeneficiaryPlansState(),
    beneficiaryMedicalConditionsState   : new BeneficiaryMedicalConditionsState(),
    beneficiaryConnectionsState         : new BeneficiaryConnectionsState()
});

/**
 * type definition for Redux Store beneficiary state
 */
export class BeneficiaryState extends BENEFICIARY_STATE implements IRequestable {
    isWaiting                           : boolean;
    isFetching                          : boolean;
    newProfileActive                    : boolean;
    isCreateProfileMappingFinished      : boolean;
    isCreateProfileSaveActive           : boolean;
    beneficiary                         : Beneficiary;
    beneficiaryBackup                   : BeneficiaryBackup;
    beneficiaryInfoState                : BeneficiaryInfoState;
    beneficiarySpecialRequirementsState : BeneficiarySpecialRequirementsState;
    beneficiaryContactMechanismsState   : BeneficiaryContactMechanismsState;
    beneficiaryPlansState               : BeneficiaryPlansState;
    beneficiaryMedicalConditionsState   : BeneficiaryMedicalConditionsState;
    beneficiaryConnectionsState         : BeneficiaryConnectionsState;

    constructor(values? : BeneficiaryState | IBeneficiaryState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // beneficiary
                convertedValues = convertedValues.set('beneficiary', new Beneficiary(convertedValues.get('beneficiary')));

                // beneficiaryBackup
                convertedValues = convertedValues.set('beneficiaryBackup', new BeneficiaryBackup(convertedValues.get('beneficiaryBackup')));

                // beneficiaryInfoState
                convertedValues = convertedValues.set('beneficiaryInfoState', new BeneficiaryInfoState(convertedValues.get('beneficiaryInfoState')));

                // beneficiarySpecialRequirementsState
                convertedValues = convertedValues.set('beneficiarySpecialRequirementsState', new BeneficiarySpecialRequirementsState(convertedValues.get('beneficiarySpecialRequirementsState')));

                // beneficiaryContactMechanismsState
                convertedValues = convertedValues.set('beneficiaryContactMechanismsState', new BeneficiaryContactMechanismsState(convertedValues.get('beneficiaryContactMechanismsState')));

                // beneficiaryPlansState
                convertedValues = convertedValues.set('beneficiaryPlansState', new BeneficiaryPlansState(convertedValues.get('beneficiaryPlansState')));

                // beneficiaryMedicalConditionsState
                convertedValues = convertedValues.set('beneficiaryMedicalConditionsState', new BeneficiaryMedicalConditionsState(convertedValues.get('beneficiaryMedicalConditionsState')));

                // beneficiaryConnectionsState
                convertedValues = convertedValues.set('beneficiaryConnectionsState', new BeneficiaryConnectionsState(convertedValues.get('beneficiaryConnectionsState')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
