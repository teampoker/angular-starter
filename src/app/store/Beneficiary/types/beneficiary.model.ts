import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';
import {
    BeneficiaryLanguage,
    IBeneficiaryLanguage
} from './beneficiary-language.model';
import {
    BeneficiaryMedicalCondition,
    IBeneficiaryMedicalCondition
} from './beneficiary-medical-condition.model';
import {
    BeneficiarySpecialRequirement,
    IBeneficiarySpecialRequirement
} from './beneficiary-special-requirement.model';
import {
    IBeneficiaryPhysicalCharacteristic,
    BeneficiaryPhysicalCharacteristic
} from './beneficiary-physical-characteristic.model';
import {
    IBeneficiaryContactMechanism,
    BeneficiaryContactMechanism
} from './beneficiary-contact-mechanism.model';
import {
    BeneficiaryConnection,
    IBeneficiaryConnection
} from './beneficiary-connection.model';
import {
    IBeneficiaryServiceCoverage,
    BeneficiaryServiceCoverage
} from './beneficiary-service-coverage.model';

export interface IBeneficiary {
    birthDate               : string;
    deathDate               : string;
    createdOn               : string;
    firstName               : string;
    lastName                : string;
    middleName              : string;
    nickname                : string;
    personalTitle           : string;
    suffix                  : string;
    updatedOn               : string;
    uuid                    : string;
    version                 : number;
    gender                  : IKeyValuePair;
    languages               : Array<IBeneficiaryLanguage>;
    physicalCharacteristics : Array<IBeneficiaryPhysicalCharacteristic>;
    specialRequirements     : Array<IBeneficiarySpecialRequirement>;
    medicalConditions       : Array<IBeneficiaryMedicalCondition>;
    contactMechanisms       : Array<IBeneficiaryContactMechanism>;
    connections             : Array<IBeneficiaryConnection>;
    serviceCoverages        : Array<IBeneficiaryServiceCoverage>;

}

export const BENEFICIARY = Record({
    birthDate               : '',
    deathDate               : '',
    createdOn               : '',
    firstName               : '',
    lastName                : '',
    middleName              : '',
    nickname                : '',
    personalTitle           : '',
    suffix                  : '',
    updatedOn               : '',
    uuid                    : undefined,
    version                 : undefined,
    gender                  : undefined,
    languages               : List<BeneficiaryLanguage>(),
    physicalCharacteristics : List<BeneficiaryPhysicalCharacteristic>(),
    specialRequirements     : List<BeneficiarySpecialRequirement>(),
    medicalConditions       : List<BeneficiaryMedicalCondition>(),
    contactMechanisms       : List<BeneficiaryContactMechanism>(),
    connections             : List<BeneficiaryConnection>(),
    serviceCoverages        : List<BeneficiaryServiceCoverage>()
});

export class Beneficiary extends BENEFICIARY {
    birthDate               : string;
    deathDate               : string;
    createdOn               : string;
    firstName               : string;
    lastName                : string;
    middleName              : string;
    nickname                : string;
    personalTitle           : string;
    suffix                  : string;
    updatedOn               : string;
    uuid                    : string;
    version                 : number;
    gender                  : KeyValuePair;
    languages               : List<BeneficiaryLanguage>;
    physicalCharacteristics : List<BeneficiaryPhysicalCharacteristic>;
    specialRequirements     : List<BeneficiarySpecialRequirement>;
    medicalConditions       : List<BeneficiaryMedicalCondition>;
    contactMechanisms       : List<BeneficiaryContactMechanism>;
    connections             : List<BeneficiaryConnection>;
    serviceCoverages        : List<BeneficiaryServiceCoverage>;

    constructor(values?  : Beneficiary | IBeneficiary) {
        let convertedValues  : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof Beneficiary) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // gender
                if (convertedValues.getIn(['gender', 'uuid'])) {
                    convertedValues = convertedValues.set('gender', KeyValuePair.fromApi(convertedValues.get('gender')));
                }
                else {
                    convertedValues = convertedValues.set('gender', new KeyValuePair(convertedValues.get('gender')));
                }

                // languages
                convertedValues = convertedValues.set('languages', List(convertedValues.get('languages', []).map(value => new BeneficiaryLanguage(value))));

                // physicalCharacteristics
                convertedValues = convertedValues.set('physicalCharacteristics', List(convertedValues.get('physicalCharacteristics', []).map(value => new BeneficiaryPhysicalCharacteristic(value))));

                // specialRequirements
                convertedValues = convertedValues.set('specialRequirements', List(convertedValues.get('specialRequirements', []).map(value => new BeneficiarySpecialRequirement(value))));

                // medicalConditions
                convertedValues = convertedValues.set('medicalConditions', List(convertedValues.get('medicalConditions', []).map(value => new BeneficiaryMedicalCondition(value))));

                // connections
                convertedValues = convertedValues.set('connections', List(convertedValues.get('connections', []).map(value => new BeneficiaryConnection(value))));

                // contactMechanisms
                convertedValues = convertedValues.set('contactMechanisms', List(convertedValues.get('contactMechanisms', []).map(value => new BeneficiaryContactMechanism(value))));

                // serviceCoverages
                convertedValues = convertedValues.set('serviceCoverages', List(convertedValues.get('serviceCoverages', []).map(value => new BeneficiaryServiceCoverage(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
