import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IBeneficiaryContactMechanism,
    BeneficiaryContactMechanism
} from './beneficiary-contact-mechanism.model';
import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryConnection {
    uuid                : string;
    personalTitle       : string;
    firstName           : string;
    middleName          : string;
    lastName            : string;
    suffix              : string;
    birthDate           : string;
    contactMechanisms   : Array<IBeneficiaryContactMechanism>;
    types               : Array<IKeyValuePair>;
}

export const BENEFICIARY_CONNECTION = Record({
    uuid                : undefined,
    personalTitle       : '',
    firstName           : '',
    middleName          : '',
    lastName            : '',
    suffix              : '',
    birthDate           : '',
    contactMechanisms   : List<BeneficiaryContactMechanism>(),
    types               : List<KeyValuePair>()
});

export class BeneficiaryConnection extends BENEFICIARY_CONNECTION {
    uuid                : string;
    personalTitle       : string;
    firstName           : string;
    middleName          : string;
    lastName            : string;
    suffix              : string;
    birthDate           : string;
    contactMechanisms   : List<BeneficiaryContactMechanism>;
    types               : List<KeyValuePair>;

    constructor(values? : BeneficiaryConnection | IBeneficiaryConnection) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryConnection) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // contactMechanisms
                if (convertedValues.get('contactMechanisms')) {
                    convertedValues = convertedValues.set('contactMechanisms', List(convertedValues.get('contactMechanisms', []).map(value => new BeneficiaryContactMechanism(value))));
                }

                // types
                if (convertedValues.getIn(['types', 0, 'uuid'])) {
                    convertedValues = convertedValues.set('types', List(convertedValues.get('types', []).map(value => KeyValuePair.fromApi(value))));
                }
                else if (convertedValues.get('types')) {
                    convertedValues = convertedValues.set('types', List(convertedValues.get('types', []).map(value => new KeyValuePair(value))));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
