import {
    fromJS,
    Map,
    Record
} from 'immutable';
import {isObject} from 'rxjs/util/isObject';

import {
    fromStorage as keyValuePairFromStorage,
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiarySpecialRequirement {
    fromDate?             : string;
    permanentRequirement? : boolean;
    personUuid?           : string;
    sequenceId?           : number;
    thruDate?             : string;
    type                  : IKeyValuePair;
    version?              : number;
}

const BENEFICIARY_SPECIAL_REQUIREMENT = Record({
    fromDate             : null,
    permanentRequirement : true,
    personUuid           : '',
    sequenceId           : undefined,
    thruDate             : null,
    type                 : new KeyValuePair(),
    version              : undefined
});

export class BeneficiarySpecialRequirement extends BENEFICIARY_SPECIAL_REQUIREMENT {
    fromDate?             : string;
    permanentRequirement? : boolean;
    personUuid?           : string;
    sequenceId?           : number;
    thruDate?             : string;
    type                  : KeyValuePair;
    version?              : number;

    constructor(values? : BeneficiarySpecialRequirement | IBeneficiarySpecialRequirement) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiarySpecialRequirement) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // type
                if (convertedValues.getIn(['type', 'uuid'])) {
                    convertedValues = convertedValues.set('type', KeyValuePair.fromApi(convertedValues.get('type')));
                }
                else {
                    convertedValues = convertedValues.set('type', new KeyValuePair(convertedValues.get('type')));
                }
            }

            convertedValues = convertedValues.set(
                'permanentRequirement',
                isPermanent(convertedValues)
            );
        }

        super(convertedValues);
    }
}

/**
 * Parses a leg API response into a Leg state model
 *
 * @param {any} requirement reservation leg
 *
 * @returns {BeneficiarySpecialRequirement}
 */
export function fromApi(requirement : any) : BeneficiarySpecialRequirement {
    const requirementState : any = {};

    if (isObject(requirement) && !Array.isArray(requirement)) {
        requirementState.sequenceId = requirement.sequenceId;
        requirementState.version = requirement.version;

        if (requirement.specialRequirementType) {
            requirementState.type = KeyValuePair.fromApi(requirement.specialRequirementType);
        }
        else {
            requirementState.type = new KeyValuePair({
                id : requirement.specialRequirementTypeUuid,
                value : ''
            });
        }
    }

    return new BeneficiarySpecialRequirement(requirementState);
}

/**
 * transform IBeneficiarySpecialRequirement model into a BeneficiarySpecialRequirement Record
 *
 * @param {IBeneficiarySpecialRequirement} pojo
 *
 * @returns {BeneficiarySpecialRequirement}
 */
export function fromStorage(pojo : IBeneficiarySpecialRequirement) : BeneficiarySpecialRequirement {
    let state = new BeneficiarySpecialRequirement(pojo);

    state = state.set('type', keyValuePairFromStorage(pojo.type)) as BeneficiarySpecialRequirement;

    return state;
}

/**
 * transform Immutable BeneficiarySpecialRequirement model into
 * appropriate POJO for processing by API endpoints
 *
 * @param {BeneficiarySpecialRequirement} requirement Immutable model to transform
 *
 * @returns {{ specialRequirementTypeUuid : string }} POJO representation
 */
export function toApi(requirement : BeneficiarySpecialRequirement) : { specialRequirementTypeUuid : string } {
    return {
        specialRequirementTypeUuid : requirement.getIn(['type', 'id'])
    };
}

/**
 * inspects the fromDate and thruDate values.  If both undefined then permanentRequirement is set to true
 * if either is defined, permanentRequirement will be false
 * @param values
 * @returns {boolean}
 */
function isPermanent(values : Map<any, any>) {
    return values.get('fromDate', null) === null &&
        values.get('thruDate', null) === null;
}
