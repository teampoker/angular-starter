import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

export interface IBeneficiaryServiceCoveragePlanClassification {
    uuid                                    : string;
    updatedBy                               : string;
    updatedOn                               : string;
    createdBy                               : string;
    createdOn                               : string;
    name                                    : string;
    validationFormat                        : string;
    maximum                                 : number;
    minimum                                 : number;
    ordinality                              : number;
    version                                 : number;
    serviceCoveragePlanClassificationType   : INameUuid;
}

export const BENEFICIARY_SERVICE_COVERAGE_PLAN_CLASSIFICATION = Record({
    uuid                                    : undefined,
    updatedBy                               : '',
    updatedOn                               : '',
    createdBy                               : '',
    createdOn                               : '',
    name                                    : '',
    validationFormat                        : '',
    maximum                                 : 0,
    minimum                                 : 0,
    ordinality                              : 0,
    version                                 : undefined,
    serviceCoveragePlanClassificationType   : new NameUuid()
});

export class BeneficiaryServiceCoveragePlanClassification extends BENEFICIARY_SERVICE_COVERAGE_PLAN_CLASSIFICATION {
    uuid                                    : string;
    updatedBy                               : string;
    updatedOn                               : string;
    createdBy                               : string;
    createdOn                               : string;
    name                                    : string;
    validationFormat                        : string;
    maximum                                 : number;
    minimum                                 : number;
    ordinality                              : number;
    version                                 : number;
    serviceCoveragePlanClassificationType   : NameUuid;

    constructor(values? : BeneficiaryServiceCoveragePlanClassification | IBeneficiaryServiceCoveragePlanClassification) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryServiceCoveragePlanClassification) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoveragePlanClassificationType
                convertedValues = convertedValues.set('serviceCoveragePlanClassificationType',  new NameUuid(convertedValues.get('serviceCoveragePlanClassificationType')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
