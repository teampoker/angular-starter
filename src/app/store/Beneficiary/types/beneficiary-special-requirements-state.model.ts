import {
    fromJS,
    Map,
    List,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';
import {
    BeneficiarySpecialRequirement,
    IBeneficiarySpecialRequirement
} from './beneficiary-special-requirement.model';

export interface IBeneficiarySpecialRequirementsState {
    isAddSpecialRequirementActive         : boolean;
    isAddSpecialRequirementValid          : boolean;
    tempSpecialRequirement                : IBeneficiarySpecialRequirement;
    requirementsState                     : Array<IBeneficiarySpecialRequirement>;
    specialRequirementTypes               : Array<IKeyValuePair>;
}

export const BENEFICIARY_SPECIAL_REQUIREMENTS_STATE = Record({
    isAddSpecialRequirementActive         : false,
    isAddSpecialRequirementValid          : true,
    tempSpecialRequirement                : new BeneficiarySpecialRequirement(),
    requirementsState                     : List<BeneficiarySpecialRequirement>(),
    specialRequirementTypes               : List<KeyValuePair>()
});

export class BeneficiarySpecialRequirementsState extends BENEFICIARY_SPECIAL_REQUIREMENTS_STATE {
    isAddSpecialRequirementActive         : boolean;
    isAddSpecialRequirementValid          : boolean;
    tempSpecialRequirement                : BeneficiarySpecialRequirement;
    requirementsState                     : List<BeneficiarySpecialRequirement>;
    specialRequirementTypes               : List<KeyValuePair>;

    constructor(values? : BeneficiarySpecialRequirementsState | IBeneficiarySpecialRequirementsState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiarySpecialRequirementsState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // tempSpecialRequirement
                convertedValues = convertedValues.set(
                    'tempSpecialRequirement',
                    new BeneficiarySpecialRequirement(convertedValues.get('tempSpecialRequirement'))
                );

                // requirementsState
                convertedValues = convertedValues.set(
                    'requirementsState',
                    List(convertedValues.get('requirementsState', []).map(value => new BeneficiarySpecialRequirement(value)))
                );

                // specialRequirementTypes
                convertedValues = convertedValues.set(
                    'specialRequirementTypes',
                    List(convertedValues.get('specialRequirementTypes', [])
                        .map(value => new KeyValuePair(value))
                    )
                );
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
