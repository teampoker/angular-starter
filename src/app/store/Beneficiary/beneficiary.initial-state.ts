import {BeneficiaryState} from './types/beneficiary-state.model';

export const INITIAL_BENEFICIARY_STATE = new BeneficiaryState();
