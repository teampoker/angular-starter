import {IPayloadAction} from '../../app-store';
import {BeneficiaryMedicalConditionsState} from '../types/beneficiary-medical-conditions-state.model';
import {BeneficiaryMedicalConditionsActions} from '../actions/beneficiary-medical-conditions.actions';
import {BeneficiaryMedicalConditionState} from '../types/beneficiary-medical-condition-state.model';

/**
 * Beneficiary Medical Conditions state reducer
 *
 * @param state
 * @param action
 * @returns {BeneficiaryMedicalConditionsState}
 * @constructor
 */
export const BENEFICIARY_MEDICAL_CONDITIONS_REDUCER = (state : BeneficiaryMedicalConditionsState, action : IPayloadAction) : BeneficiaryMedicalConditionsState => {
    switch (action.type) {
        case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_DROPDOWN_TYPES :
            state = state.set('medicalConditionTypes', action.payload) as BeneficiaryMedicalConditionsState;

            break;
        case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_REMOVE_MEDICAL_CONDITION :
            // remove selected medical condition
            state = state.deleteIn(['conditionsState', action.payload]) as BeneficiaryMedicalConditionsState;

            break;
        case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_MEDICAL_CONDITION_FIELD_VALUE :
            // update field on tempMedicalCondition
            state = state.setIn(['tempMedicalCondition', action.payload.fieldName], action.payload.fieldValue) as BeneficiaryMedicalConditionsState;

            break;
        case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_MEDICAL_CONDITION_TYPE :
            // update the condition on tempMedicalCondition
            state = state.withMutations(record => {
                return record
                    .setIn(['tempMedicalCondition', 'type'], action.payload)
                    .set('isAddMedicalConditionValid', true);
            }) as BeneficiaryMedicalConditionsState;

            break;
        case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_ADD_MEDICAL_CONDITION_ACTIVE :
            state = state.set('isAddMedicalConditionActive', action.payload) as BeneficiaryMedicalConditionsState;

            break;
        case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_CANCEL_ADD_MEDICAL_CONDITION :
            state = state.withMutations(record => {
                return record
                    .set('tempMedicalCondition', new BeneficiaryMedicalConditionState())
                    .set('isAddMedicalConditionActive', false)
                    .set('isAddMedicalConditionValid', true)
                    .setIn(['tempMedicalCondition', 'type', 'value'], 'SELECT');
            }) as BeneficiaryMedicalConditionsState;

            break;
        case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_SAVE_MEDICAL_CONDITIONS :
            // make sure user has specified a medical condition name
            if (state.getIn(['tempMedicalCondition', 'type', 'value']) !== 'SELECT') {
                // build the new medical condition record to add
                const tempMedicalCondition    : BeneficiaryMedicalConditionState = state.get('tempMedicalCondition'),
                      newCondition            : BeneficiaryMedicalConditionState = new BeneficiaryMedicalConditionState().withMutations(record => {
                        return record
                            .set('personUuid', tempMedicalCondition.get('personUuid'))
                            .set('fromDate', tempMedicalCondition.get('fromDate'))
                            .set('thruDate', tempMedicalCondition.get('thruDate'))
                            .set('sequenceId', tempMedicalCondition.get('sequenceId'))
                            .set('version', tempMedicalCondition.get('version'))
                            .set('type', tempMedicalCondition.get('type'));
                    }) as BeneficiaryMedicalConditionState;

                state = state.withMutations(record => {
                    return record
                        .set('isAddMedicalConditionActive', false)
                        .set('conditionsState', state.get('conditionsState').push(newCondition))
                        .set('tempMedicalCondition', new BeneficiaryMedicalConditionState())
                        .setIn(['tempMedicalCondition', 'type', 'value'], 'SELECT');
                }) as BeneficiaryMedicalConditionsState;
            }
            else {
                // set validation flag to false to notify user of problem
                state = state.set('isAddMedicalConditionValid', false) as BeneficiaryMedicalConditionsState;
            }

            break;
        default :
            return state;
    }

    return state;
};
