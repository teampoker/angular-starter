import {List} from 'immutable';

import {IPayloadAction} from '../../app-store';
import {BeneficiaryPlansState} from '../types/beneficiary-plans-state.model';
import {BeneficiaryPlansActions} from '../actions/beneficiary-plans.actions';
import {BeneficiaryPlansStateDetail} from '../types/beneficiary-plans-state-detail.model';
import {BeneficiaryPlansStateSummary} from '../types/beneficiary-plans-state-summary.model';

/**
 * service coverages metadata cached locally in this value
 */
let serviceCoverageOrganizationsMetaData : List<BeneficiaryPlansStateDetail>;

/**
 * Beneficiary Plans state reducer
 * @param state
 * @param action
 * @returns {BeneficiaryPlansState}
 * @constructor
 */
export const BENEFICIARY_PLANS_REDUCER = (state : BeneficiaryPlansState, action : IPayloadAction) : BeneficiaryPlansState => {
    switch (action.type) {
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_ADD_BENEFICIARY_PLAN :
            // cache service coverages metadata
            serviceCoverageOrganizationsMetaData = List<BeneficiaryPlansStateDetail>(action.payload.map(coverageValue => new BeneficiaryPlansStateDetail(coverageValue)));

            state = state.withMutations(record => {
                return record
                    // update add index
                    .set('addPlanIndex', state.get('plansStateSummary').size)

                    // add new plan instance
                    .set('newPlanStateSummary',
                        new BeneficiaryPlansStateSummary()
                            .set('plansStateDetail', serviceCoverageOrganizationsMetaData)
                    );
            }) as BeneficiaryPlansState;

            // set flags/indexes
            state = state.withMutations(record => {
                return record
                    .set('isPlanFormValid', false)
                    .set('isEditPlanActive', false)
                    .set('isAddPlanActive', true)
                    .set('editPlanIndex', -1)
                    .set('organizationIndex', -1)
                    .set('coveragePlanIndex', -1);
            }) as BeneficiaryPlansState;

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_EDIT_BENEFICIARY_PLAN :
            state = state.withMutations(record => {
                return record
                    .set('editPlanIndex', action.payload)
                    .set('isEditPlanActive', true)
                    .set('isAddPlanActive', false)
                    .set('addPlanIndex', -1)
                    .set('organizationIndex', -1)
                    .set('coveragePlanIndex', -1);
            }) as BeneficiaryPlansState;

            // now update organizationIndex and coveragePlanIndex
            state = state.set('organizationIndex', state.getIn(['plansStateSummary', state.get('editPlanIndex'), 'plansStateDetail']).findIndex(value => value.get('id') === state.getIn(['plansStateSummary', state.get('editPlanIndex'), 'organizationUuid']))) as BeneficiaryPlansState;

            state = state.set('coveragePlanIndex', state.getIn(['plansStateSummary', state.get('editPlanIndex'), 'plansStateDetail', state.get('organizationIndex'), 'serviceCoveragePlans']).findIndex(plan => plan.get('id') === state.getIn(['plansStateSummary', state.get('editPlanIndex'), 'planUuid']))) as BeneficiaryPlansState;

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_CANCEL_ADD_PLAN :
            state = state.delete('newPlanStateSummary') as BeneficiaryPlansState;

            state = state.withMutations(record => {
                return record
                    .set('isPlanFormValid', false)
                    .set('isEditPlanActive', false)
                    .set('isAddPlanActive', false)
                    .set('editPlanIndex', -1)
                    .set('addPlanIndex', -1)
                    .set('organizationIndex', -1)
                    .set('coveragePlanIndex', -1);
            }) as BeneficiaryPlansState;

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_CLOSE_ADD_PLAN :
            state = state.withMutations(record => {
                return record
                    .set('isPlanFormValid', false)
                    .set('isEditPlanActive', false)
                    .set('isAddPlanActive', false)
                    .set('editPlanIndex', -1)
                    .set('addPlanIndex', -1)
                    .set('organizationIndex', -1)
                    .set('coveragePlanIndex', -1);
            }) as BeneficiaryPlansState;

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_CANCEL_EDIT_PLAN :
            state = state.withMutations(record => {
                return record
                    .set('isPlanFormValid', false)
                    .set('isEditPlanActive', false)
                    .set('isAddPlanActive', false)
                    .set('editPlanIndex', -1)
                    .set('addPlanIndex', -1)
                    .set('organizationIndex', -1)
                    .set('coveragePlanIndex', -1);
            }) as BeneficiaryPlansState;

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_CLOSE_EDIT_PLAN :
            state = state.withMutations(record => {
                return record
                    .set('isPlanFormValid', false)
                    .set('isEditPlanActive', false)
                    .set('isAddPlanActive', false)
                    .set('editPlanIndex', -1)
                    .set('addPlanIndex', -1)
                    .set('organizationIndex', -1)
                    .set('coveragePlanIndex', -1);
            }) as BeneficiaryPlansState;

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_PLAN_FIELD_VALUE :
            if (state.get('isEditPlanActive')) {
                state = state.setIn(['plansStateSummary', state.get('editPlanIndex'), action.payload.fieldName], action.payload.fieldValue) as BeneficiaryPlansState;
            }
            else {
                state = state.setIn(['newPlanStateSummary', action.payload.fieldName], action.payload.fieldValue) as BeneficiaryPlansState;
            }

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_PLAN_DYNAMIC_FIELD_VALUE :
            if (state.get('isEditPlanActive')) {
                // figure out the index of the field to update
                const updateIndex = state.getIn(['plansStateSummary', state.get('editPlanIndex'), 'plansStateDetail', state.get('organizationIndex'), 'serviceCoveragePlans', state.get('coveragePlanIndex'), 'serviceCoveragePlanClassifications']).findIndex(fieldType => fieldType.get('value') === action.payload.fieldName);

                if (updateIndex !== -1) {
                    state = state.setIn(['plansStateSummary', state.get('editPlanIndex'), 'plansStateDetail', state.get('organizationIndex'), 'serviceCoveragePlans', state.get('coveragePlanIndex'), 'serviceCoveragePlanClassifications', updateIndex, 'selectedValue'], action.payload.fieldValue) as BeneficiaryPlansState;
                }

                // if index was zero...
                if (updateIndex === 0) {
                    // update the summary planIdValue as well
                    state = state.setIn(['plansStateSummary', state.get('editPlanIndex'), 'planIdValue'], action.payload.fieldValue) as BeneficiaryPlansState;
                }
            }
            else {
                // figure out the index of the field to update
                const updateIndex = state.getIn(['newPlanStateSummary', 'plansStateDetail', state.get('organizationIndex'), 'serviceCoveragePlans', state.get('coveragePlanIndex'), 'serviceCoveragePlanClassifications']).findIndex(fieldType => fieldType.get('value') === action.payload.fieldName);

                if (updateIndex !== -1) {
                    state = state.setIn(['newPlanStateSummary', 'plansStateDetail', state.get('organizationIndex'), 'serviceCoveragePlans', state.get('coveragePlanIndex'), 'serviceCoveragePlanClassifications', updateIndex, 'selectedValue'], action.payload.fieldValue) as BeneficiaryPlansState;
                }

                // if index was zero...
                if (updateIndex === 0) {
                    // update the summary planIdValue as well
                    state = state.setIn(['newPlanStateSummary', 'planIdValue'], action.payload.fieldValue) as BeneficiaryPlansState;
                }
            }

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_REMOVE_BENEFICIARY_PLAN :
            state = state.deleteIn(['plansStateSummary', action.payload]) as BeneficiaryPlansState;

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_SAVE_PLANS :
            // was a new plan being added?
            if (state.get('isAddPlanActive')) {
                // copy the new plan to the collection of plans
                state = state
                    .set('plansStateSummary',
                        state
                            .get('plansStateSummary')
                            .push(state.get('newPlanStateSummary'))
                    ) as BeneficiaryPlansState;

                // clear out the new item
                state = state.delete('newPlanStateSummary') as BeneficiaryPlansState;
            }

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_ORGANIZATION_INDEX :
            state = state.set('organizationIndex', action.payload) as BeneficiaryPlansState;

            break;
        case BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_COVERAGE_PLAN_INDEX :
            state = state.set('coveragePlanIndex', action.payload) as BeneficiaryPlansState;

            break;
        default :
            return state;
    }

    return state;
};
