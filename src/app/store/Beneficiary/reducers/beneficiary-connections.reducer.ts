import {List} from 'immutable';

import {IPayloadAction} from '../../app-store';
import {BeneficiaryConnectionsState} from '../types/beneficiary-connections-state.model';
import {BeneficiaryConnectionsActions} from '../actions/beneficiary-connections.actions';
import {ContactMechanismType} from '../../MetaDataTypes/types/contact-mechanism-type.model';
import {BeneficiaryConnectionsStateDetail} from '../types/beneficiary-connections-state-detail.model';
import {BeneficiaryPhone} from '../types/beneficiary-phone.model';
import {Beneficiary} from '../types/beneficiary.model';

/**
 * Beneficiary Connections state reducer
 *
 * @param state
 * @param action
 * @returns {BeneficiaryConnectionsState}
 * @constructor
 */
export const BENEFICIARY_CONNECTIONS_REDUCER = (state : BeneficiaryConnectionsState, action : IPayloadAction) : BeneficiaryConnectionsState => {
    switch (action.type) {
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_UPDATE_CONNECTION_DROPDOWN_TYPES :
            state = state.set('dropdownTypes', action.payload) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_TOGGLE_ADD_PHONE_ACTIVE :
            state = state.set('isAddPhoneActive', !state.get('isAddPhoneActive')) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_TOGGLE_ADD_CONNECTION_EXPANDED :
            state = state.set('isAddConnectionExpanded', !state.get('isAddConnectionExpanded')) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_TOGGLE_CONNECTION_ADDED_MANUALLY :
            state = state.set('isConnectionAddedManually', action.payload) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_TOGGLE_CONNECTION_ADDED_FROM_SEARCH :
            state = state.set('isConnectionAddedFromSearch', action.payload) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_ADD_BENEFICIARY_CONNECTION : {
            // grab the contact mechanism types for phones
            const phoneType : ContactMechanismType = state.getIn(['dropdownTypes', 'contactMechanismTypes']).find(mechanism => mechanism.get('value') === 'Telecommunications Number');

            state = state.withMutations(record => {
                return record
                    // update add index
                    .set('addConnectionIndex', state.get('connectionsStateDetail').size)

                    // add new connection instance
                    .set('connectionsStateDetail', state.get('connectionsStateDetail').push(new BeneficiaryConnectionsStateDetail().withMutations(connection => {
                        return connection
                            .set('phones', connection.get('phones').push(new BeneficiaryPhone().withMutations(phoneRecord => {
                                return phoneRecord
                                    .setIn(['mechanismType', 'id'], phoneType.get('id'))
                                    .setIn(['mechanismType', 'value'], phoneType.get('value'));
                            })));
                    })));
            }) as BeneficiaryConnectionsState;

            // set flags/indexes
            state = state.withMutations(record => {
                return record
                    .set('isAddConnectionActive', true)
                    .set('isAddPhoneActive', false)
                    .set('isEditConnectionActive', false)
                    .set('isConnectionAddedManually', false)
                    .set('isConnectionAddedFromSearch', false)
                    .set('editConnectionIndex', -1);
            }) as BeneficiaryConnectionsState;

            break;
        }
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_REMOVE_BENEFICIARY_CONNECTION :
            state = state.deleteIn(['connectionsStateDetail', action.payload]) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_ADD_CONNECTION_FROM_PEOPLE_SEARCH : {
            const addedBeneficiary : Beneficiary = new Beneficiary(action.payload),

                // store contact mechanism type for phones
                phoneType : ContactMechanismType = state.getIn(['dropdownTypes', 'contactMechanismTypes']).find(mechanism => mechanism.get('value') === 'Telecommunications Number'),

                // calculate index to insert this newly added connection at
                addedConnectionIndex : number = state.get('connectionsStateDetail').size - 1,

                // map the selected connection to the dynamically geneerated FormGroup that is bound to the Add a Connection Form
                addedConnection : BeneficiaryConnectionsStateDetail = new BeneficiaryConnectionsStateDetail().withMutations(addedConnectionRecord => {
                    return addedConnectionRecord
                        .set('uuid', addedBeneficiary.get('uuid'))
                        .set('firstName', addedBeneficiary.get('firstName'))
                        .set('middleName', addedBeneficiary.get('middleName'))
                        .set('lastName', addedBeneficiary.get('lastName'))
                        .set('personalTitle', addedBeneficiary.get('personalTitle'))
                        .set('suffix', addedBeneficiary.get('suffix'))
                        .set('phones', List<BeneficiaryPhone>(addedBeneficiary.get('contactMechanisms').filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'id']) === phoneType.get('id'))
                            .map(phoneMechanism => new BeneficiaryPhone().withMutations(phoneRecord => phoneRecord
                                .set('sequenceId', phoneMechanism.get('sequenceId'))
                                .set('uuid', phoneMechanism.getIn(['contactMechanism', 'id']))
                                .set('version', phoneMechanism.getIn(['contactMechanism', 'version']))
                                .set('phone', '(' + phoneMechanism.getIn(['contactMechanism', 'areaCode']) + ') ' + phoneMechanism.getIn(['contactMechanism', 'contactNumber']).substr(0, 3) + '-' + phoneMechanism.getIn(['contactMechanism', 'contactNumber']).substr(3, 7))
                                .set('phoneType', phoneMechanism.get('purposeType'))
                                .set('phoneNotifications', phoneMechanism.get('solicitationIndicatorType'))
                                .set('mechanismType', phoneMechanism.getIn(['contactMechanism', 'type'])
                                )))));
                }) as BeneficiaryConnectionsStateDetail;

            state = state.withMutations(record => record
                // add this data to our viewmodel to map to the Add a Connection form
                    .setIn(['connectionsStateDetail', addedConnectionIndex], addedConnection)

                    // make sure Add a Connection section is expanded
                    .set('isAddConnectionExpanded', true)
                    .set('isConnectionAddedManually', false)
                    .set('isConnectionAddedFromSearch', true)
            ) as BeneficiaryConnectionsState;

            break;
        }
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_EDIT_BENEFICIARY_CONNECTION :
            state = state.withMutations(record => {
                return record
                    .set('editConnectionIndex', action.payload)
                    .set('isAddConnectionActive', false)
                    .set('isAddPhoneActive', false)
                    .set('isEditConnectionActive', true)
                    .set('addConnectionIndex', -1);
            }) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_UPDATE_CONNECTION_FIELD_VALUE : {
            // check for connection type values
            if (
                action.payload.fieldName === 'parentFamily' ||
                action.payload.fieldName === 'socialWorker' ||
                action.payload.fieldName === 'nurseDoctor' ||
                action.payload.fieldName === 'planClient' ||
                action.payload.fieldName === 'emergencyContact' ||
                action.payload.fieldName === 'driver'
            ) {
                state = state.setIn(['connectionsStateDetail', action.payload.primaryUpdateIndex, 'connectionTypes', action.payload.fieldName], action.payload.fieldValue) as BeneficiaryConnectionsState;
            }
            // check for connection phone values
            else if (
                action.payload.fieldName === 'phone' ||
                action.payload.fieldName === 'phoneType' ||
                action.payload.fieldName === 'phoneNotifications'
            ) {
                state = state.setIn(['connectionsStateDetail', action.payload.primaryUpdateIndex, 'phones', action.payload.secondaryUpdateIndex, action.payload.fieldName], action.payload.fieldValue) as BeneficiaryConnectionsState;
            }
            else {
                state = state.setIn(['connectionsStateDetail', action.payload.primaryUpdateIndex, action.payload.fieldName], action.payload.fieldValue) as BeneficiaryConnectionsState;
            }

            break;
        }
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_CANCEL_ADD_CONNECTION :
            // remove the data that was mapped to the temporary Add a Connection form
            state = state.deleteIn(['connectionsStateDetail', state.get('addConnectionIndex')]) as BeneficiaryConnectionsState;

            state = state.withMutations(record => {
                return record
                    .set('isAddConnectionActive', false)
                    .set('isAddPhoneActive', false)
                    .set('isAddConnectionExpanded', false)
                    .set('isConnectionAddedManually', false)
                    .set('isConnectionAddedFromSearch', false)
                    .set('addConnectionIndex', -1);
            }) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_CLOSE_ADD_CONNECTION :
            state = state.withMutations(record => {
                return record
                    .set('isAddConnectionActive', false)
                    .set('isAddPhoneActive', false)
                    .set('isAddConnectionExpanded', false)
                    .set('addConnectionIndex', -1);
            }) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_CANCEL_EDIT_CONNECTION :
            state = state.withMutations(record => {
                return record
                    .set('isEditConnectionActive', false)
                    .set('isAddPhoneActive', false)
                    .set('editConnectionIndex', -1);
            }) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_CLOSE_EDIT_CONNECTION :
            state = state.withMutations(record => {
                return record
                    .set('isEditConnectionActive', false)
                    .set('isAddPhoneActive', false)
                    .set('editConnectionIndex', -1);
            }) as BeneficiaryConnectionsState;

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_ADD_ADDITIONAL_PHONE :
            if (state.get('isAddConnectionActive')) {
                state = state.updateIn(['connectionsStateDetail', state.get('addConnectionIndex'), 'phones'], value => value.push(new BeneficiaryPhone())) as BeneficiaryConnectionsState;
            }
            else {
                state = state.updateIn(['connectionsStateDetail', state.get('editConnectionIndex'), 'phones'], value => value.push(new BeneficiaryPhone())) as BeneficiaryConnectionsState;
            }

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_REMOVE_ADDITIONAL_PHONE :
            if (state.get('isAddConnectionActive')) {
                state = state.deleteIn(['connectionsStateDetail', state.get('addConnectionIndex'), 'phones', action.payload]) as BeneficiaryConnectionsState;
            }
            else {
                state = state.deleteIn(['connectionsStateDetail', state.get('editConnectionIndex'), 'phones', action.payload]) as BeneficiaryConnectionsState;
            }

            break;
        case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_UPDATE_NEW_CONNECTION_UUID :
            state = state.setIn(['connectionsStateDetail', state.get('connectionsStateDetail').size - 1, 'uuid'], action.payload) as BeneficiaryConnectionsState;

            break;
        default :
            return state;
    }

    return state;
};
