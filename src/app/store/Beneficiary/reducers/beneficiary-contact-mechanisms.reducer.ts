import {IPayloadAction} from '../../app-store';
import {BeneficiaryContactMechanismsState} from '../types/beneficiary-contact-mechanisms-state.model';
import {BeneficiaryContactMechanismActions} from '../actions/beneficiary-contact-mechanisms.actions';
import {BeneficiaryAddress} from '../types/beneficiary-address.model';
import {ContactMechanismType} from '../../MetaDataTypes/types/contact-mechanism-type.model';
import {BeneficiaryEmail} from '../types/beneficiary-email.model';
import {BeneficiaryPhone} from '../types/beneficiary-phone.model';

/**
 * Beneficiary Contact Mechanisms state reducer
 *
 * @param state
 * @param action
 * @returns {BeneficiaryContactMechanismsState}
 * @constructor
 */
export const BENEFICIARY_CONTACT_MECHANISMS_REDUCER = (state : BeneficiaryContactMechanismsState, action : IPayloadAction) : BeneficiaryContactMechanismsState => {
    switch (action.type) {
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_CONTACT_INFO_DROPDOWN_TYPES :
            state = state.set('dropdownTypes', action.payload) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_SAVE_CONTACT_MECHANISMS :
            state = state.set('isContactMechanismsEditing', false) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_CONTACT_INFO_EDITING :
            state = state.set('isContactMechanismsEditing', action.payload) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_FIELD_VALUE :
            state = state.setIn(['contactMechanismsState', 'addresses', action.payload.primaryUpdateIndex, action.payload.fieldName], action.payload.fieldValue) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_ADDRESS :
            state = state.updateIn(['contactMechanismsState', 'addresses'], value => value.push(new BeneficiaryAddress().withMutations(addressRecord => {
                const addressType : ContactMechanismType = state.getIn(['dropdownTypes', 'contactMechanismTypes']).find(mechanism => mechanism.get('value') === 'Address');

                return addressRecord
                    .setIn(['mechanismType', 'id'], addressType.get('id'))
                    .setIn(['mechanismType', 'value'], addressType.get('value'));
            }))) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_ADDRESS :
            state = state.deleteIn(['contactMechanismsState', 'addresses', action.payload]) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_TYPE :
            state = state.withMutations(record => {
                return record
                    .setIn(['contactMechanismsState', 'addresses', action.payload.primaryUpdateIndex, 'addressType', 'value'], action.payload.fieldName)
                    .setIn(['contactMechanismsState', 'addresses', action.payload.primaryUpdateIndex, 'addressType', 'id'], action.payload.fieldValue);
            }) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_NOTIFICATION_TYPE :
            state = state.withMutations(record => {
                return record
                    .setIn(['contactMechanismsState', 'addresses', action.payload.primaryUpdateIndex, 'addressNotifications', 'value'], action.payload.fieldName)
                    .setIn(['contactMechanismsState', 'addresses', action.payload.primaryUpdateIndex, 'addressNotifications', 'id'], action.payload.fieldValue);
            }) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_FIELD_VALUE :
            state = state.setIn(['contactMechanismsState', 'emails', action.payload.primaryUpdateIndex, action.payload.fieldName], action.payload.fieldValue) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_TYPE :
            state = state.withMutations(record => {
                return record
                    .setIn(['contactMechanismsState', 'emails', action.payload.primaryUpdateIndex, 'emailType', 'value'], action.payload.fieldName)
                    .setIn(['contactMechanismsState', 'emails', action.payload.primaryUpdateIndex, 'emailType', 'id'], action.payload.fieldValue);
            }) as BeneficiaryContactMechanismsState;
            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_NOTIFICATION_TYPE :
            state = state.withMutations(record => {
                return record
                    .setIn(['contactMechanismsState', 'emails', action.payload.primaryUpdateIndex, 'emailNotifications', 'value'], action.payload.fieldName)
                    .setIn(['contactMechanismsState', 'emails', action.payload.primaryUpdateIndex, 'emailNotifications', 'id'], action.payload.fieldValue);
            }) as BeneficiaryContactMechanismsState;
            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_EMAIL :
            state = state.updateIn(['contactMechanismsState', 'emails'], value => value.push(new BeneficiaryEmail().withMutations(emailRecord => {
                const emailType : ContactMechanismType = state.getIn(['dropdownTypes', 'contactMechanismTypes']).find(mechanism => mechanism.get('value') === 'Electronic Address');

                return emailRecord
                    .setIn(['mechanismType', 'id'], emailType.get('id'))
                    .setIn(['mechanismType', 'value'], emailType.get('value'));
            }))) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_EMAIL :
            state = state.deleteIn(['contactMechanismsState', 'emails', action.payload]) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_FIELD_VALUE :
            state = state.setIn(['contactMechanismsState', 'phones', action.payload.primaryUpdateIndex, action.payload.fieldName], action.payload.fieldValue) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_TYPE :
            state = state.withMutations(record => {
                return record
                    .setIn(['contactMechanismsState', 'phones', action.payload.primaryUpdateIndex, 'phoneType', 'value'], action.payload.fieldName)
                    .setIn(['contactMechanismsState', 'phones', action.payload.primaryUpdateIndex, 'phoneType', 'id'], action.payload.fieldValue);
            }) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_NOTIFICATION_TYPE :
            state = state.withMutations(record => {
                return record
                    .setIn(['contactMechanismsState', 'phones', action.payload.primaryUpdateIndex, 'phoneNotifications', 'value'], action.payload.fieldName)
                    .setIn(['contactMechanismsState', 'phones', action.payload.primaryUpdateIndex, 'phoneNotifications', 'id'], action.payload.fieldValue);
            }) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_PHONE :
            state = state.updateIn(['contactMechanismsState', 'phones'], value => value.push(new BeneficiaryPhone().withMutations(phoneRecord => {
                const phoneType : ContactMechanismType = state.getIn(['dropdownTypes', 'contactMechanismTypes']).find(mechanism => mechanism.get('value') === 'Telecommunications Number');

                return phoneRecord
                    .setIn(['mechanismType', 'id'], phoneType.get('id'))
                    .setIn(['mechanismType', 'value'], phoneType.get('value'));
            }))) as BeneficiaryContactMechanismsState;

            break;
        case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_PHONE :
            state = state.deleteIn(['contactMechanismsState', 'phones', action.payload]) as BeneficiaryContactMechanismsState;

            break;
        default :
            return state;
    }

    return state;
};
