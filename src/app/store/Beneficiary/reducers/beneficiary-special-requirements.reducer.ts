import {IPayloadAction} from '../../app-store';
import {BeneficiarySpecialRequirementsState} from '../types/beneficiary-special-requirements-state.model';
import {BeneficiarySpecialRequirementsActions} from '../actions/beneficiary-special-requirements.actions';
import {BeneficiarySpecialRequirement} from '../types/beneficiary-special-requirement.model';

/**
 * Beneficiary Special Requirements state reducer
 *
 * @param state
 * @param action
 * @returns {BeneficiarySpecialRequirementsState}
 * @constructor
 */
export const BENEFICIARY_SR_REDUCER = (state : BeneficiarySpecialRequirementsState, action : IPayloadAction) : BeneficiarySpecialRequirementsState => {
    switch (action.type) {
        case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_SR_DROPDOWN_TYPES :
            state = state.set('specialRequirementTypes', action.payload) as BeneficiarySpecialRequirementsState;

            break;
        case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_REMOVE_SR :
            // remove selected special requirement
            state = state.deleteIn(['requirementsState', action.payload]) as BeneficiarySpecialRequirementsState;

            break;
        case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_SR_FIELD_VALUE :
            // update field on tempSpecialRequirement

            // if the field is fromDate or thruDate and permanentRequirement is true ignore...
            if (state.getIn(['tempSpecialRequirement', 'permanentRequirement'])) {
                // check for fromDate/thruDate
                if (action.payload.fieldName === 'fromDate' || action.payload.fieldName === 'thruDate') {
                    action.payload.fieldValue = '';
                }
            }

            state = state.setIn(['tempSpecialRequirement', action.payload.fieldName], action.payload.fieldValue) as BeneficiarySpecialRequirementsState;

            // if permanentRequirement was set
            if (action.payload.fieldName === 'permanentRequirement' && action.payload.fieldValue) {
                // blank the date values also
                state = state.withMutations(record => {
                    return record
                        .setIn(['tempSpecialRequirement', 'fromDate'], '')
                        .setIn(['tempSpecialRequirement', 'thruDate'], '');
                }) as BeneficiarySpecialRequirementsState;
            }

            break;
        case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_SR_TYPE :
            // update field on tempSpecialRequirement
            state = state.withMutations(record => {
                return record
                    .setIn(['tempSpecialRequirement', 'type'], action.payload)
                    .set('isAddSpecialRequirementValid', true);
            }) as BeneficiarySpecialRequirementsState;

            break;
        case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_ADD_SR_ACTIVE :
            state = state.set('isAddSpecialRequirementActive', action.payload) as BeneficiarySpecialRequirementsState;

            break;
        case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_CANCEL_ADD_SR :
            state = state.withMutations(record => {
                return record
                    .set('tempSpecialRequirement', new BeneficiarySpecialRequirement())
                    .set('isAddSpecialRequirementActive', false)
                    .set('isAddSpecialRequirementValid', true)
                    .setIn(['tempSpecialRequirement', 'type', 'value'], 'SELECT');
            }) as BeneficiarySpecialRequirementsState;

            break;
        case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_SAVE_SR :
            // make sure user has specified a special requirement name
            if (state.getIn(['tempSpecialRequirement', 'type', 'value']) !== 'SELECT') {
                // build the new special requirement record to add
                const tempSpecialRequirement  : BeneficiarySpecialRequirement = state.get('tempSpecialRequirement'),
                      newRequirement          : BeneficiarySpecialRequirement = new BeneficiarySpecialRequirement().withMutations(record => {
                          return record
                            .set('personUuid', tempSpecialRequirement.get('personUuid'))
                            .set('fromDate', tempSpecialRequirement.get('fromDate'))
                            .set('thruDate', tempSpecialRequirement.get('thruDate'))
                            .set('sequenceId', tempSpecialRequirement.get('sequenceId'))
                            .set('version', tempSpecialRequirement.get('version'))
                            .set('type', tempSpecialRequirement.get('type'))
                            .set('permanentRequirement', tempSpecialRequirement.get('permanentRequirement'));
                    }) as BeneficiarySpecialRequirement;

                state = state.withMutations(record => {
                    return record
                        .set('isAddSpecialRequirementActive', false)
                        .set('requirementsState', state.get('requirementsState').push(newRequirement))
                        .set('tempSpecialRequirement', new BeneficiarySpecialRequirement())
                        .setIn(['tempSpecialRequirement', 'type', 'value'], 'SELECT');
                }) as BeneficiarySpecialRequirementsState;
            }
            else {
                // set validation flag to false to notify user of problem
                state = state.set('isAddSpecialRequirementValid', false) as BeneficiarySpecialRequirementsState;
            }

            break;
        case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_SET_SR :
            state = state.set('requirementsState', action.payload) as BeneficiarySpecialRequirementsState;
            break;
        default :
            return state;
    }

    return state;
};
