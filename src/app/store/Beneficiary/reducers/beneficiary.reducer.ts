import {
    List,
    Map
} from 'immutable';
import * as moment from 'moment';

import {IPayloadAction} from '../../app-store';
import {INITIAL_BENEFICIARY_STATE} from '../beneficiary.initial-state';
import {BENEFICIARY_INFO_REDUCER} from './beneficiary-info.reducer';
import {BENEFICIARY_CONTACT_MECHANISMS_REDUCER} from './beneficiary-contact-mechanisms.reducer';
import {BENEFICIARY_SR_REDUCER} from './beneficiary-special-requirements.reducer';
import {BENEFICIARY_CONNECTIONS_REDUCER} from './beneficiary-connections.reducer';
import {BENEFICIARY_MEDICAL_CONDITIONS_REDUCER} from './beneficiary-medical-conditions.reducer';
import {BENEFICIARY_PLANS_REDUCER} from './beneficiary-plans.reducer';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {BeneficiaryState} from '../types/beneficiary-state.model';
import {ServiceCoverageOrganizationType} from '../../MetaDataTypes/types/service-coverage-organization-type.model';
import {BeneficiaryInfoActions} from '../actions/beneficiary-info.actions';
import {BeneficiaryBackup} from '../types/beneficiary-backup.model';
import {Beneficiary} from '../types/beneficiary.model';
import {BeneficiaryContactMechanismActions} from '../actions/beneficiary-contact-mechanisms.actions';
import {BeneficiarySpecialRequirementsActions} from '../actions/beneficiary-special-requirements.actions';
import {BeneficiaryConnectionsActions} from '../actions/beneficiary-connections.actions';
import {ContactMechanismType} from '../../MetaDataTypes/types/contact-mechanism-type.model';
import {BeneficiaryContactMechanism} from '../types/beneficiary-contact-mechanism.model';
import {BeneficiaryMedicalConditionsActions} from '../actions/beneficiary-medical-conditions.actions';
import {BeneficiaryPlansActions} from '../actions/beneficiary-plans.actions';
import {BeneficiaryActions} from '../actions/beneficiary.actions';
import {BeneficiaryInfo} from '../types/beneficiary-info';
import {LanguageType} from '../../MetaDataTypes/types/language-type.model';
import {BeneficiaryPhysicalCharacteristic} from '../types/beneficiary-physical-characteristic.model';
import {BeneficiaryLanguage} from '../types/beneficiary-language.model';
import {BeneficiaryContactMechanismStateDetail} from '../types/beneficiary-contact-mechanism-state-detail.model';
import {BeneficiaryMedicalConditionState} from '../types/beneficiary-medical-condition-state.model';
import {BeneficiaryMedicalCondition} from '../types/beneficiary-medical-condition.model';
import {BeneficiarySpecialRequirement} from '../types/beneficiary-special-requirement.model';
import {BeneficiaryConnectionsStateDetail} from '../types/beneficiary-connections-state-detail.model';
import {BeneficiaryConnection} from '../types/beneficiary-connection.model';
import {BeneficiaryServiceCoverage} from '../types/beneficiary-service-coverage.model';
import {BeneficiaryPlansStateSummary} from '../types/beneficiary-plans-state-summary.model';
import {BeneficiaryServiceCoveragePlanClassificationState} from '../types/beneficiary-service-coverage-plan-classification-state.model';
import {BeneficiaryPersonServiceCoveragePlanClassification} from '../types/beneficiary-service-coverage-classification.model';
import {BeneficiaryPlansStateDetail} from '../types/beneficiary-plans-state-detail.model';
import {BeneficiaryAddress} from '../types/beneficiary-address.model';
import {BeneficiaryEmail} from '../types/beneficiary-email.model';
import {BeneficiaryPhone} from '../types/beneficiary-phone.model';
import {BeneficiaryConnectionTypes} from '../types/beneficiary-connection-types.model';
import {
    parsePhoneNumberAreaCode,
    parsePhoneNumberContactNumber
} from '../../utils/parse-phone-numbers';

/**
 * Redux Action Prefixes
 * @type {string}
 */
const   beneficiaryInfoPrefix                 : string = 'BENEFICIARY_INFO',
        beneficiaryContactMechanismsPrefix    : string = 'BENEFICIARY_CONTACT_MECHANISMS',
        beneficiarySpecialRequirementsPrefix  : string = 'BENEFICIARY_SR',
        beneficiaryConnectionsPrefix          : string = 'BENEFICIARY_CONNECTIONS',
        beneficiaryMedicalConditionsPrefix    : string = 'BENEFICIARY_MEDICAL_CONDITIONS',
        beneficiaryPlansPrefix                : string = 'BENEFICIARY_PLANS';

/**
 * service coverages metadata cached locally in these values
 */
let serviceCoverageOrganizationsMetaData        : List<ServiceCoverageOrganizationType>,
    serviceCoveragePlanClassificationsMetaData  : List<KeyValuePair>;

/**
 * App Beneficiary State Reducer
 *
 * @param state
 * @param action
 * @returns {any}
 * @constructor
 */
export const BENEFICIARY_STATE_REDUCER = (state : BeneficiaryState = INITIAL_BENEFICIARY_STATE, action : IPayloadAction) : BeneficiaryState => {
    /**
     * check for action prefix type
     */

    /**
     * Beneficiary Information
     */
    if (action.type.indexOf(beneficiaryInfoPrefix) !== -1) {
        // pass action to beneficiary info reducer
        state = state.set('beneficiaryInfoState', BENEFICIARY_INFO_REDUCER(state.get('beneficiaryInfoState'), action)) as BeneficiaryState;

        // perform any extra work on BeneficiaryState as needed
        switch (action.type) {
            case BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_INFO_EDITING :
                // backup beneficiary data
                state = doBeneficiaryBackup(state);

                break;
            case BeneficiaryInfoActions.BENEFICIARY_INFO_SAVE_BENEFICIARY_INFO :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary info to the beneficiary domain model
                state = saveBeneficiaryInfo(state);

                break;
            case BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_BENEFICIARY :
                // update beneficiary
                state = state.set('beneficiary', new Beneficiary(action.payload)) as BeneficiaryState;

                // update view models
                state = populateViewModels(state) as BeneficiaryState;

                break;
            default :
                return state;
        }
    }
    /**
     * Beneficiary Contact Mechanisms
     */
    else if (action.type.indexOf(beneficiaryContactMechanismsPrefix) !== -1) {
        // pass action to beneficiary address & contacts reducer
        state = state.set('beneficiaryContactMechanismsState', BENEFICIARY_CONTACT_MECHANISMS_REDUCER(state.get('beneficiaryContactMechanismsState'), action)) as BeneficiaryState;

        // perform any extra work on BeneficiaryState as needed
        switch (action.type) {
            case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_BENEFICIARY :
                // update the beneficiary
                state = state.set('beneficiary', new Beneficiary(action.payload)) as BeneficiaryState;

                // update view models
                state = populateViewModels(state) as BeneficiaryState;

                break;
            case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_CONTACT_INFO_EDITING :
                // backup beneficiary data
                state = doBeneficiaryBackup(state);

                break;
            case BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_SAVE_CONTACT_MECHANISMS :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary info to the beneficiary domain model
                state = saveBeneficiaryContactMechanisms(state);

                break;
            default :
                return state;
        }
    }
    /**
     * Beneficiary Special Requirements
     */
    else if (action.type.indexOf(beneficiarySpecialRequirementsPrefix) !== -1) {
        // pass action to beneficiary special requirements reducer
        state = state.set('beneficiarySpecialRequirementsState', BENEFICIARY_SR_REDUCER(state.get('beneficiarySpecialRequirementsState'), action)) as BeneficiaryState;
        // perform any extra work on BeneficiaryState as needed
        switch (action.type) {
            case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_SAVE_SR :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary info to the beneficiary domain model
                state = saveBeneficiarySpecialRequirements(state);

                break;
            case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_REMOVE_SR :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary info to the beneficiary domain model
                state = saveBeneficiarySpecialRequirements(state);

                break;
            case BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_BENEFICIARY :
                // update beneficiary
                state = state.set('beneficiary', new Beneficiary(action.payload)) as BeneficiaryState;

                // update view models
                state = populateViewModels(state) as BeneficiaryState;

                break;
            default :
                return state;
        }
    }
    /**
     * Beneficiary Connections
     */
    else if (action.type.indexOf(beneficiaryConnectionsPrefix) !== -1) {
        // pass action to beneficiary connections reducer
        state = state.set('beneficiaryConnectionsState', BENEFICIARY_CONNECTIONS_REDUCER(state.get('beneficiaryConnectionsState'), action)) as BeneficiaryState;

        // perform any extra work on BeneficiaryState as needed
        switch (action.type) {
            case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_EDIT_BENEFICIARY_CONNECTION :
                state = doBeneficiaryBackup(state);

                break;
            case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_CANCEL_EDIT_CONNECTION :
                state = doBeneficiaryBackup(state);

                break;
            case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_SAVE_BENEFICIARY_CONNECTIONS :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary connections to the beneficiary domain model
                state = saveBeneficiaryConnections(state);

                break;
            case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_UPDATE_BENEFICIARY :
                // update beneficiary
                state = state.set('beneficiary', new Beneficiary(action.payload)) as BeneficiaryState;

                // update view models
                state = populateViewModels(state);

                break;
            case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_REMOVE_BENEFICIARY_CONNECTION :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary connections to the beneficiary domain model
                state = saveBeneficiaryConnections(state);

                break;
            case BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_POPULATE_MANUALLY_ADDED_CONNECTION :
                const phoneType : ContactMechanismType = state.getIn(['beneficiaryConnectionsState', 'dropdownTypes', 'contactMechanismTypes'])
                        .find(mechanism => mechanism.get('value') === 'Telecommunications Number'),
                    defaultSolicitationIndicatorType    : KeyValuePair = state.getIn(['beneficiaryConnectionsState', 'dropdownTypes', 'solicitationIndicatorTypes'])
                        .find(value => value.get('value') === 'No');

                state = state.setIn(['beneficiaryConnectionsState', 'manuallyAddedConnection'], new Beneficiary().withMutations(record => record
                        .set('personalTitle', action.payload.get('personalTitle'))
                        .set('firstName', action.payload.get('firstName'))
                        .set('middleName', action.payload.get('middleName'))
                        .set('lastName', action.payload.get('lastName'))
                        .set('suffix', action.payload.get('suffix'))
                        .set('contactMechanisms', action.payload.get('phones').size > 0 ? action.payload.get('phones').map((phoneRecord, phoneRecordIndex) => {
                                return new BeneficiaryContactMechanism().withMutations(contactMechanismRecord => contactMechanismRecord
                                    .set('ordinality', phoneRecordIndex)
                                    .setIn(['purposeType', 'id'], phoneRecord.getIn(['phoneType', 'id']) !== 0 ? phoneRecord.getIn(['phoneType', 'id']) : 0)
                                    .setIn(['purposeType', 'value'], phoneRecord.getIn(['phoneType', 'value']) !== '' ? phoneRecord.getIn(['phoneType', 'value']) : '')
                                    .setIn(['purposeType', 'contactMechanismType', 'id'], phoneType.get('id'))
                                    .setIn(['purposeType', 'contactMechanismType', 'value'], phoneType.get('value'))
                                    .set('solicitationIndicatorType', phoneRecord.getIn(['phoneNotifications', 'id']) !== 0 ? phoneRecord.get('phoneNotifications') : defaultSolicitationIndicatorType.get('id'))
                                    .setIn(['contactMechanism', 'type', 'id'], phoneType.get('id'))
                                    .setIn(['contactMechanism', 'type', 'value'], phoneType.get('value'))
                                    .setIn(['contactMechanism', 'areaCode'], parsePhoneNumberAreaCode(phoneRecord.get('phone')))
                                    .setIn(['contactMechanism', 'contactNumber'], parsePhoneNumberContactNumber(phoneRecord.get('phone'))));
                            }) : undefined)
                )) as BeneficiaryState;

                break;
            default :
                return state;
        }
    }
    /**
     * Beneficiary Medical Conditions
     */
    else if (action.type.indexOf(beneficiaryMedicalConditionsPrefix) !== -1) {
        // pass action to beneficiary medical conditions reducer
        state = state.set('beneficiaryMedicalConditionsState', BENEFICIARY_MEDICAL_CONDITIONS_REDUCER(state.get('beneficiaryMedicalConditionsState'), action)) as BeneficiaryState;
        // perform any extra work on BeneficiaryState as needed
        switch (action.type) {
            case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_SAVE_MEDICAL_CONDITIONS :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary info to the beneficiary domain model
                state = saveBeneficiaryMedicalConditions(state);

                break;
            case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_REMOVE_MEDICAL_CONDITION :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary info to the beneficiary domain model
                state = saveBeneficiaryMedicalConditions(state);

                break;
            case BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_BENEFICIARY :
                // update beneficiary
                state = state.set('beneficiary', new Beneficiary(action.payload)) as BeneficiaryState;

                // update view models
                state = populateViewModels(state) as BeneficiaryState;

                break;
            default :
                return state;
        }
    }
    /**
     * Beneficiary Plans
     */
    else if (action.type.indexOf(beneficiaryPlansPrefix) !== -1) {
        // pass action to beneficiary plans reducer
        state = state.set('beneficiaryPlansState', BENEFICIARY_PLANS_REDUCER(state.get('beneficiaryPlansState'), action)) as BeneficiaryState;

        // perform any extra work on BeneficiaryState as needed
        switch (action.type) {
            case BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_BENEFICIARY :
                // update the beneficiary
                state = state.set('beneficiary', new Beneficiary(action.payload)) as BeneficiaryState;

                // update view models
                state = populateViewModels(state) as BeneficiaryState;

                break;
            case BeneficiaryPlansActions.BENEFICIARY_PLANS_EDIT_BENEFICIARY_PLAN :
                state = doBeneficiaryBackup(state);

                break;
            case BeneficiaryPlansActions.BENEFICIARY_PLANS_CANCEL_EDIT_PLAN :
                state = doBeneficiaryBackup(state);

                break;
            case BeneficiaryPlansActions.BENEFICIARY_PLANS_SAVE_PLANS :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary service coverages to the beneficiary domain model
                state = saveBeneficiaryServiceCoverages(state);

                break;
            case BeneficiaryPlansActions.BENEFICIARY_PLANS_REMOVE_BENEFICIARY_PLAN :
                // reset beneficiary backup state
                state = state.set('beneficiaryBackup', new BeneficiaryBackup()) as BeneficiaryState;

                // map updated beneficiary service coverages to the beneficiary domain model
                state = saveBeneficiaryServiceCoverages(state);

                break;
            default :
                return state;
        }
    }
    /**
     * Redux Actions that are not specific to a piece of the Beneficiary Profile form
     */
    else {
        switch (action.type) {
            case BeneficiaryActions.BENEFICIARY_WAIT:
                state = state.set('isWaiting', action.payload) as BeneficiaryState;
                break;
            case BeneficiaryActions.BENEFICIARY_UPDATE_BENEFICIARY :
                // update beneficiary
                let beneficiary : any = action.payload.get('beneficiary');

                if (!beneficiary) {
                    beneficiary = new Beneficiary();
                }

                state = state.set('beneficiary', new Beneficiary(beneficiary)) as BeneficiaryState;

                // store metatdata
                serviceCoverageOrganizationsMetaData        = action.payload.get('serviceCoverages');
                serviceCoveragePlanClassificationsMetaData  = action.payload.get('classificationTypes');

                // update view models
                state = populateViewModels(state) as BeneficiaryState;

                break;
            case BeneficiaryActions.BENEFICIARY_UPDATE_CREATE_PROFILE_SAVE_ACTIVE :
                state = state.set('isCreateProfileSaveActive', action.payload) as BeneficiaryState;

                break;
            case BeneficiaryActions.BENEFICIARY_UPDATE_NEW_PROFILE_ACTIVE :
                state = state.set('newProfileActive', action.payload) as BeneficiaryState;

                break;
            case BeneficiaryActions.BENEFICIARY_INIT_BENEFICIARY_DASHBOARD :
                // init beneficiary dashboard data
                state = state.withMutations(record => record
                    // set newProfileActive flag
                    .set('newProfileActive', false)) as BeneficiaryState;

                break;
            case BeneficiaryActions.BENEFICIARY_INIT_CREATE_BENEFICIARY_PROFILE :
                // init newProfile data
                state = state.withMutations(record => record
                    // reset beneficiary model
                    .set('beneficiary', new Beneficiary())

                    // set newProfileActive flag
                    .set('newProfileActive', true)

                    // set saving/mapping flags false
                    .set('isCreateProfileMappingFinished', false)

                    // reset form specific state

                    // beneficiary info
                    .setIn(['beneficiaryInfoState', 'isBeneficiaryInfoEditing'], false)

                    // beneficiary address and contact
                    .setIn(['beneficiaryContactMechanismsState', 'isContactMechanismsEditing'], false)

                    // beneficiary connections
                    .setIn(['beneficiaryConnectionsState', 'isEditConnectionActive'], false)
                    .setIn(['beneficiaryConnectionsState', 'editConnectionIndex'], -1)
                    .setIn(['beneficiaryConnectionsState', 'isAddConnectionActive'], false)
                    .setIn(['beneficiaryConnectionsState', 'isConnectionAddedManually'], false)
                    .setIn(['beneficiaryConnectionsState', 'isConnectionAddedFromSearch'], false)
                    .setIn(['beneficiaryConnectionsState', 'isAddConnectionExpanded'], false)
                    .setIn(['beneficiaryConnectionsState', 'addConnectionIndex'], -1)

                    // beneficiary medical conditions

                    // beneficiary special requirements

                    // beneficiary plans
                    .setIn(['beneficiaryPlansState', 'isPlanFormValid'], false)
                    .setIn(['beneficiaryPlansState', 'isEditPlanActive'], false)
                    .setIn(['beneficiaryPlansState', 'isAddPlanActive'], false)
                    .setIn(['beneficiaryPlansState', 'editPlanIndex'], -1)
                    .setIn(['beneficiaryPlansState', 'addPlanIndex'], -1)
                ) as BeneficiaryState;

                // update view models
                state = populateViewModels(state) as BeneficiaryState;

                break;
            case BeneficiaryActions.BENEFICIARY_INIT_VIEW_BENEFICIARY_PROFILE :
                // init newProfile data
                state = state.withMutations(record => record
                    // set newProfileActive flag
                    .set('newProfileActive', false)

                    // reset form specific state

                    // beneficiary info
                    .setIn(['beneficiaryInfoState', 'isBeneficiaryInfoEditing'], false)

                    // beneficiary address and contact
                    .setIn(['beneficiaryContactMechanismsState', 'isContactMechanismsEditing'], false)

                    // beneficiary connections
                    .setIn(['beneficiaryConnectionsState', 'isEditConnectionActive'], false)
                    .setIn(['beneficiaryConnectionsState', 'editConnectionIndex'], -1)
                    .setIn(['beneficiaryConnectionsState', 'isAddConnectionActive'], false)
                    .setIn(['beneficiaryConnectionsState', 'isConnectionAddedManually'], false)
                    .setIn(['beneficiaryConnectionsState', 'isConnectionAddedFromSearch'], false)
                    .setIn(['beneficiaryConnectionsState', 'isAddConnectionExpanded'], false)
                    .setIn(['beneficiaryConnectionsState', 'addConnectionIndex'], -1)

                    // beneficiary medical conditions

                    // beneficiary special requirements

                    // beneficiary plans
                    .setIn(['beneficiaryPlansState', 'isPlanFormValid'], false)
                    .setIn(['beneficiaryPlansState', 'isEditPlanActive'], false)
                    .setIn(['beneficiaryPlansState', 'isAddPlanActive'], false)
                    .setIn(['beneficiaryPlansState', 'editPlanIndex'], -1)
                    .setIn(['beneficiaryPlansState', 'addPlanIndex'], -1)
                ) as BeneficiaryState;

                break;
            case BeneficiaryActions.BENEFICIARY_CREATE_NEW_BENEFICIARY :
                // set the flag indicating a save is occurring
                state = state.withMutations(record => record
                    .set('isCreateProfileMappingFinished', false)
                ) as BeneficiaryState;

                // update the beneficiary domain model
                state = saveBeneficiaryInfo(state);

                state = saveBeneficiaryContactMechanisms(state);

                state = saveBeneficiaryMedicalConditions(state);

                state = saveBeneficiarySpecialRequirements(state);

                state = saveBeneficiaryConnections(state);

                state = saveBeneficiaryServiceCoverages(state);

                // set mapping finished flag true
                state = state.set('isCreateProfileMappingFinished', true) as BeneficiaryState;

                break;
            default :
                return state;
        }
    }

    return state;
};

/**
 * map the Beneficiary Info portion of view model to the
 * Beneficiary Domain Model
 * @param state
 */
function saveBeneficiaryInfo(state : BeneficiaryState) : BeneficiaryState {
    try {
        const beneficiaryInfoVM : BeneficiaryInfo   = state.getIn(['beneficiaryInfoState', 'beneficiaryInfo']);

        // are we creating a new beneficiary profile?
        if (state.get('newProfileActive')) {
            // begin mapping view model data to Beneficiary Information Domain Model
            return state.withMutations(record => {
                // pull the language metadata info for the selected language
                const languageType : LanguageType = record.getIn(['beneficiaryInfoState', 'dropdownTypes', 'languageTypes']).find(lang => lang.get('value') === beneficiaryInfoVM.get('language'));

                return record
                    .setIn(['beneficiary', 'personalTitle'], beneficiaryInfoVM.get('personalTitle') !== 'SELECT' ? beneficiaryInfoVM.get('personalTitle') : '')
                    .setIn(['beneficiary', 'firstName'], beneficiaryInfoVM.get('firstName'))
                    .setIn(['beneficiary', 'middleName'], beneficiaryInfoVM.get('middleName'))
                    .setIn(['beneficiary', 'lastName'], beneficiaryInfoVM.get('lastName'))
                    .setIn(['beneficiary', 'suffix'], beneficiaryInfoVM.get('suffix'))
                    .setIn(['beneficiary', 'birthDate'], beneficiaryInfoVM.get('birthDate') ? moment(beneficiaryInfoVM.get('birthDate')).format('YYYY-MM-DD') : '')
                    .setIn(['beneficiary', 'deathDate'], beneficiaryInfoVM.get('deathDate') ? moment(beneficiaryInfoVM.get('deathDate')).format('YYYY-MM-DD') : '')

                    /**
                     * gender value needs to be mapped to a the correct metaDataTypes.genderTypes entity
                     */
                    .setIn(['beneficiary', 'gender'], beneficiaryInfoVM.get('deathDate') !== 'SELECT' ? state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'genderTypes']).find(gender => gender.value === beneficiaryInfoVM.get('gender')) : undefined)

                    /**
                     * weight value needs to be mapped to a the correct metaDataTypes.physicalCharacteristicTypes entity
                     */
                    .setIn(['beneficiary', 'physicalCharacteristics'], beneficiaryInfoVM.get('weight') !== '' ? record.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().withMutations(stuff => stuff
                            .set('value', beneficiaryInfoVM.get('weight'))
                            .set('type', record.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Weight'))
                        )) : record.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().set('type', record.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Weight'))))

                    /**
                     * height value needs to be mapped to a the correct metaDataTypes.physicalCharacteristicTypes entity
                     */
                    .setIn(['beneficiary', 'physicalCharacteristics'], beneficiaryInfoVM.get('heightFeet') !== 'SELECT' ? record.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().withMutations(stuff => {
                            // make sure they entered a value for inches
                            if (beneficiaryInfoVM.get('heightInches') !== 'SELECT') {
                                return stuff
                                    .set('value', parseInt(beneficiaryInfoVM.get('heightFeet'), 10) * 12 + parseInt(beneficiaryInfoVM.get('heightInches'), 10))
                                    .set('type', record.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Height'));
                            }
                            else {
                                return stuff
                                    .set('value', parseInt(beneficiaryInfoVM.get('heightFeet'), 10) * 12)
                                    .set('type', record.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Height'));
                            }
                        })) : record.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().set('type', record.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Height'))))

                    /**
                     * primary language value needs to be mapped to a the correct metaDataTypes.languageTypes entity
                     */
                    .setIn(['beneficiary', 'languages'], beneficiaryInfoVM.get('language') !== 'SELECT' && beneficiaryInfoVM.get('language') !== 'Select' && beneficiaryInfoVM.get('language') !== '' ? state.getIn(['beneficiary', 'languages']).push(new BeneficiaryLanguage().withMutations(stuff => stuff
                            .set('language', languageType)
                            .set('type', languageType)
                            .set('ordinality', 0)
                        )) : List<BeneficiaryLanguage>());
            }) as BeneficiaryState;
        }
        // user edited existing Beneficiary's Profile Information section
        else {
            // store index of height and weight in beneficiary physical characteristics along with index of beneficiary's current primary selected language
            const   beneficiaryWeightIndex    : number = state.getIn(['beneficiary', 'physicalCharacteristics']).findIndex(value => value.getIn(['type', 'value']) === 'Weight'),
                    beneficiaryHeightIndex    : number = state.getIn(['beneficiary', 'physicalCharacteristics']).findIndex(value => value.getIn(['type', 'value']) === 'Height'),
                    existingLangIndex         : number = state.getIn(['beneficiary', 'languages']).findIndex(language => language.getIn(['type', 'name']) === beneficiaryInfoVM.get('language') && language.get('ordinality') !== 0),

                // store index of beneficiary's current primary selected language
                primaryLanguageIndex : number = state.getIn(['beneficiary', 'languages']).findIndex(language => language.get('ordinality') === 0);

            // store new selected primary language, if available, here
            let newSelectedLanguage : BeneficiaryLanguage,

                // store calculated beneficiary height values here
                beneficiaryHeightFeet       : string,
                beneficiaryHeightInches     : string,
                beneficiaryHeightCalculated : number;

            // delete any existing presence of a beneficiary language selection that matches the newly selected primary language(if any)
            if (existingLangIndex !== -1) {
                state = state.deleteIn(['beneficiary', 'languages', existingLangIndex]) as BeneficiaryState;
            }

            // check for a language selection before mapping the record
            if (beneficiaryInfoVM.get('language') !== 'SELECT' && beneficiaryInfoVM.get('language') !== '') {
                newSelectedLanguage = new BeneficiaryLanguage().withMutations(primaryLanguage => {
                    const oldLanguage   : BeneficiaryLanguage   = state.getIn(['beneficiary', 'languages']).find(language => language.get('ordinality') === 0),
                          languageType  : LanguageType          = state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'languageTypes']).find(lang => lang.get('value') === state.getIn(['beneficiaryInfoState', 'beneficiaryInfo', 'language']));

                    // was there a previous primary language selection?
                    return primaryLanguage
                        .set('ordinality', oldLanguage ? oldLanguage.get('ordinality') : 0)
                        .set('personUuid', oldLanguage ? oldLanguage.get('personUuid') : state.getIn(['beneficiary', 'uuid']))
                        .set('sequenceId', oldLanguage ? oldLanguage.get('sequenceId') : undefined)
                        .set('version', oldLanguage ? oldLanguage.get('version') : undefined)
                        .set('language', languageType)
                        .set('type', languageType);
                }) as BeneficiaryLanguage;
            }

            // make sure they entered a value for feet
            beneficiaryInfoVM.get('heightFeet') !== 'SELECT' ? beneficiaryHeightFeet = beneficiaryInfoVM.get('heightFeet') : beneficiaryHeightFeet = '0';

            // make sure they entered a value for inches
            beneficiaryInfoVM.get('heightInches') !== 'SELECT' ? beneficiaryHeightInches = beneficiaryInfoVM.get('heightInches') : beneficiaryHeightInches = '0';

            beneficiaryHeightCalculated = parseInt(beneficiaryHeightFeet, 10) * 12 + parseInt(beneficiaryHeightInches, 10);

            // begin mapping view model data to Beneficiary Information Domain Model
            let newInfoState : BeneficiaryState = state.withMutations(record => record
                    .setIn(['beneficiary', 'personalTitle'], beneficiaryInfoVM.get('personalTitle') !== 'SELECT' ? beneficiaryInfoVM.get('personalTitle') : '')
                    .setIn(['beneficiary', 'firstName'], beneficiaryInfoVM.get('firstName'))
                    .setIn(['beneficiary', 'middleName'], beneficiaryInfoVM.get('middleName'))
                    .setIn(['beneficiary', 'lastName'], beneficiaryInfoVM.get('lastName'))
                    .setIn(['beneficiary', 'suffix'], beneficiaryInfoVM.get('suffix'))
                    .setIn(['beneficiary', 'birthDate'], beneficiaryInfoVM.get('birthDate') ? moment(beneficiaryInfoVM.get('birthDate')).format('YYYY-MM-DD') : undefined)
                    .setIn(['beneficiary', 'deathDate'], beneficiaryInfoVM.get('deathDate') ? moment(beneficiaryInfoVM.get('deathDate')).format('YYYY-MM-DD') : undefined)

                    /**
                     * this value needs to be mapped to a the correct metaDataTypes.genderTypes entity
                     */
                    .setIn(['beneficiary', 'gender'], beneficiaryInfoVM.get('deathDate') !== 'SELECT' ? state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'genderTypes']).find(gender => gender.value === beneficiaryInfoVM.get('gender')) : undefined)
            ) as BeneficiaryState;

            // update physical characteristics separately in case they did not have an existing height or weight
            if (beneficiaryWeightIndex !== -1) {
                newInfoState = newInfoState.withMutations(newInfoStateRecord =>
                    newInfoStateRecord
                        .setIn(['beneficiary', 'physicalCharacteristics', beneficiaryWeightIndex, 'value'], beneficiaryInfoVM.get('weight') !== '' ? beneficiaryInfoVM.get('weight') : 0)
                        .setIn(['beneficiary', 'physicalCharacteristics', beneficiaryWeightIndex, 'type'], state.getIn(['beneficiary', 'physicalCharacteristics', beneficiaryWeightIndex, 'type']))
                ) as BeneficiaryState;
            }
            else {
                newInfoState = newInfoState.setIn(['beneficiary', 'physicalCharacteristics'], beneficiaryInfoVM.get('weight') !== '' ? state.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().withMutations(stuff =>
                        stuff
                            .set('value', beneficiaryInfoVM.get('weight'))
                            .set('type', state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Weight'))
                    )) : state.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().set('type', state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Weight')))) as BeneficiaryState;
            }

            if (beneficiaryHeightIndex !== -1) {
                newInfoState = newInfoState.withMutations(newInfoStateRecord => newInfoStateRecord
                        .setIn(['beneficiary', 'physicalCharacteristics', beneficiaryHeightIndex, 'value'], beneficiaryHeightCalculated)
                        .setIn(['beneficiary', 'physicalCharacteristics', beneficiaryHeightIndex, 'type'], state.getIn(['beneficiary', 'physicalCharacteristics', beneficiaryHeightIndex, 'type']))
                ) as BeneficiaryState;
            }
            else {
                newInfoState = newInfoState.withMutations(newInfoStateRecord => newInfoStateRecord
                        .setIn(['beneficiary', 'physicalCharacteristics'], beneficiaryInfoVM.get('weight') !== '' ? state.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().withMutations(stuff => stuff
                                    .set('value', beneficiaryInfoVM.get('weight'))
                                    .set('type', state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Weight'))
                            )) : state.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().set('type', state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Weight'))))
                        .setIn(['beneficiary', 'physicalCharacteristics'], beneficiaryInfoVM.get('heightFeet') !== 'SELECT' ? state.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().withMutations(stuff => {
                                // make sure they entered a value for inches
                                if (beneficiaryInfoVM.get('heightInches') !== 'SELECT') {
                                    return stuff
                                        .set('value', parseInt(beneficiaryInfoVM.get('heightFeet'), 10) * 12 + parseInt(beneficiaryInfoVM.get('heightInches'), 10))
                                        .set('type', state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Height'));
                                }
                                else {
                                    return stuff
                                        .set('value', parseInt(beneficiaryInfoVM.get('heightFeet'), 10) * 12)
                                        .set('type', state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Height'));
                                }
                            })) : state.getIn(['beneficiary', 'physicalCharacteristics']).push(new BeneficiaryPhysicalCharacteristic().set('type', state.getIn(['beneficiaryInfoState', 'dropdownTypes', 'physicalCharacteristicTypes']).find(temp => temp.get('value') === 'Height'))))
                ) as BeneficiaryState;
            }

            // now we can try and set the updated primary language if available
            if (newSelectedLanguage) {
                /**
                 * this value needs to be mapped to a the correct metaDataTypes.languageTypes entity
                 * we need to look for the language with ordinality of zero.  if it does not match the new
                 * primary language selection we need to go find the correct value from languageTypes and
                 * update the beneficiary.languages collection with the new primary language
                 */

                if (primaryLanguageIndex !== -1) {
                    newInfoState = newInfoState.setIn(['beneficiary', 'languages', primaryLanguageIndex], newSelectedLanguage) as BeneficiaryState;
                }
                else {
                    newInfoState = newInfoState.updateIn(['beneficiary', 'languages'], value => value.push(newSelectedLanguage)) as BeneficiaryState;
                }
            }

            return newInfoState;
        }
    }
    catch (err) {
        console.error('error: ' + err + ' during saveBeneficiaryInfo');
    }
}

/**
 * map the Beneficiary Contact Mechanisms portion of view model to the
 * Beneficiary Domain Model
 * @param state
 */
function saveBeneficiaryContactMechanisms(state : BeneficiaryState) : BeneficiaryState {
    try {
        // grab all the metadata for the various contact mechanism types
        const addressType   : ContactMechanismType = state.getIn(['beneficiaryConnectionsState', 'dropdownTypes', 'contactMechanismTypes']).find(mechanism => mechanism.get('value') === 'Address'),
              emailType     : ContactMechanismType = state.getIn(['beneficiaryConnectionsState', 'dropdownTypes', 'contactMechanismTypes']).find(mechanism => mechanism.get('value') === 'Electronic Address'),
              phoneType     : ContactMechanismType = state.getIn(['beneficiaryConnectionsState', 'dropdownTypes', 'contactMechanismTypes']).find(mechanism => mechanism.get('value') === 'Telecommunications Number'),

            // set a series of reasonable defaults to use on the various dropdown fields so we don't send across a null value
            defaultAddressType                  : KeyValuePair = addressType.get('purposeTypes').find(value => value.get('value') === 'Residence'),
            defaultEmailType                    : KeyValuePair = emailType.get('purposeTypes').find(value => value.get('value') === 'Personal Email'),
            defaultPhoneType                    : KeyValuePair = phoneType.get('purposeTypes').find(value => value.get('value') === 'Home Phone'),
            defaultSolicitationIndicatorType    : KeyValuePair = state.getIn(['beneficiaryConnectionsState', 'dropdownTypes', 'solicitationIndicatorTypes'])
                                                                      .find(value => value.get('value') === 'No'),

            // grab contact mechanism state
            contactMechanismsState : BeneficiaryContactMechanismStateDetail = state.getIn(['beneficiaryContactMechanismsState', 'contactMechanismsState']);

        let contactMechanisms : List<BeneficiaryContactMechanism> = List<BeneficiaryContactMechanism>();

        // are we creating a new beneficiary profile?
        if (state.get('newProfileActive')) {
            // just map the values over to the contactMechanisms state

            // addresses first
            contactMechanisms = contactMechanisms.concat(List<BeneficiaryContactMechanism>(contactMechanismsState.get('addresses').map((contactMechanismState, contactMechanismStateIndex) =>
                new BeneficiaryContactMechanism().withMutations(contactMechanismRecord => contactMechanismRecord
                    .set('ordinality', contactMechanismStateIndex)
                    .setIn(['purposeType', 'id'], contactMechanismState.getIn(['addressType', 'id']) !== 0 ? contactMechanismState.getIn(['addressType', 'id']) : defaultAddressType.get('id'))
                    .setIn(['purposeType', 'value'], contactMechanismState.getIn(['addressType', 'value']) !== '' ? contactMechanismState.getIn(['addressType', 'value']) : defaultAddressType.get('value'))
                    .setIn(['purposeType', 'contactMechanismType', 'id'], addressType.get('id'))
                    .setIn(['purposeType', 'contactMechanismType', 'value'], addressType.get('value'))
                    .set('solicitationIndicatorType', contactMechanismState.getIn(['addressNotifications', 'id']) !== 0 ? contactMechanismState.get('addressNotifications') : defaultSolicitationIndicatorType.get('id'))
                    .setIn(['contactMechanism', 'type', 'id'], addressType.get('id'))
                    .setIn(['contactMechanism', 'type', 'value'], addressType.get('value'))
                    .setIn(['contactMechanism', 'address1'], contactMechanismState.get('street'))
                    .setIn(['contactMechanism', 'address2'], contactMechanismState.get('apt'))
                    .setIn(['contactMechanism', 'city'], contactMechanismState.get('city'))
                    .setIn(['contactMechanism', 'state'], contactMechanismState.get('state'))
                    .setIn(['contactMechanism', 'postalCode'], contactMechanismState.get('postalCode'))
                    .setIn(['contactMechanism', 'directions'], contactMechanismState.get('geographicDetails')))
            ))) as List<BeneficiaryContactMechanism>;

            // emails next
            contactMechanisms = contactMechanisms.concat(List<BeneficiaryContactMechanism>(contactMechanismsState.get('emails').map((contactMechanismState, contactMechanismStateIndex) =>
                new BeneficiaryContactMechanism().withMutations(contactMechanismRecord => contactMechanismRecord
                    .set('ordinality', contactMechanismStateIndex)
                    .setIn(['purposeType', 'id'], contactMechanismState.getIn(['emailType', 'id']) !== 0 ? contactMechanismState.getIn(['emailType', 'id']) : defaultEmailType.get('id'))
                    .setIn(['purposeType', 'value'], contactMechanismState.getIn(['emailType', 'value']) !== '' ? contactMechanismState.getIn(['emailType', 'value']) : defaultEmailType.get('value'))
                    .setIn(['purposeType', 'contactMechanismType', 'id'], emailType.get('id'))
                    .setIn(['purposeType', 'contactMechanismType', 'value'], emailType.get('value'))
                    .set('solicitationIndicatorType', contactMechanismState.getIn(['emailNotifications', 'id']) !== 0 ? contactMechanismState.get('emailNotifications') : defaultSolicitationIndicatorType.get('id'))
                    .setIn(['contactMechanism', 'type', 'id'], emailType.get('id'))
                    .setIn(['contactMechanism', 'type', 'value'], emailType.get('value'))
                    .setIn(['contactMechanism', 'electronicAddressString'], contactMechanismState.get('email')))
            ))) as List<BeneficiaryContactMechanism>;

            // phones last
            contactMechanisms = contactMechanisms.concat(List<BeneficiaryContactMechanism>(contactMechanismsState.get('phones').map((contactMechanismState, contactMechanismStateIndex) =>
                new BeneficiaryContactMechanism().withMutations(contactMechanismRecord => contactMechanismRecord
                    .set('ordinality', contactMechanismStateIndex)
                    .setIn(['purposeType', 'id'], contactMechanismState.getIn(['phoneType', 'id']) !== 0 ? contactMechanismState.getIn(['phoneType', 'id']) : defaultPhoneType.get('id'))
                    .setIn(['purposeType', 'value'], contactMechanismState.getIn(['phoneType', 'value']) !== '' ? contactMechanismState.getIn(['phoneType', 'value']) : defaultPhoneType.get('value'))
                    .setIn(['purposeType', 'contactMechanismType', 'id'], phoneType.get('id'))
                    .setIn(['purposeType', 'contactMechanismType', 'value'], phoneType.get('value'))
                    .set('solicitationIndicatorType', contactMechanismState.getIn(['phoneNotifications', 'id']) !== 0 ? contactMechanismState.get('phoneNotifications') : defaultSolicitationIndicatorType.get('id'))
                    .setIn(['contactMechanism', 'type', 'id'], phoneType.get('id'))
                    .setIn(['contactMechanism', 'type', 'value'], phoneType.get('value'))
                    .setIn(['contactMechanism', 'areaCode'], parsePhoneNumberAreaCode(contactMechanismState.get('phone')))
                    .setIn(['contactMechanism', 'contactNumber'], parsePhoneNumberContactNumber(contactMechanismState.get('phone'))))
            ))) as List<BeneficiaryContactMechanism>;
        }
        else {
            // go ahead and grab the existing contact mechanisms for the beneficiary by type
            const addressMechanisms : List<BeneficiaryContactMechanism> = state.getIn(['beneficiary', 'contactMechanisms']).filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'id']) === addressType.get('id')),
                  emailMechanisms   : List<BeneficiaryContactMechanism> = state.getIn(['beneficiary', 'contactMechanisms']).filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'id']) === emailType.get('id')),
                  phoneMechanisms   : List<BeneficiaryContactMechanism> = state.getIn(['beneficiary', 'contactMechanisms']).filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'id']) === phoneType.get('id'));

            // addresses first
            contactMechanismsState.get('addresses').forEach(contactMechanismState => {
                // look for a matching beneficiary mechanism
                const addressMatch : BeneficiaryContactMechanism = addressMechanisms.find(address => address.getIn(['contactMechanism', 'uuid']) === contactMechanismState.get('uuid'));

                if (addressMatch) {
                    // this address was edited
                    contactMechanisms = contactMechanisms.push(addressMatch.withMutations(contactMechanismRecord => contactMechanismRecord
                        .setIn(['purposeType', 'id'], contactMechanismState.getIn(['addressType', 'id']) !== 0 ? contactMechanismState.getIn(['addressType', 'id']) : defaultAddressType.get('id'))
                        .setIn(['purposeType', 'value'], contactMechanismState.getIn(['addressType', 'value']) !== '' ? contactMechanismState.getIn(['addressType', 'value']) : defaultAddressType.get('value'))
                        .set('solicitationIndicatorType', contactMechanismState.getIn(['addressNotifications', 'id']) !== 0 ? contactMechanismState.get('addressNotifications') : defaultSolicitationIndicatorType.get('id'))
                        .setIn(['contactMechanism', 'address1'], contactMechanismState.get('street'))
                        .setIn(['contactMechanism', 'address2'], contactMechanismState.get('apt'))
                        .setIn(['contactMechanism', 'city'], contactMechanismState.get('city'))
                        .setIn(['contactMechanism', 'state'], contactMechanismState.get('state'))
                        .setIn(['contactMechanism', 'postalCode'], contactMechanismState.get('postalCode'))
                        .setIn(['contactMechanism', 'directions'], contactMechanismState.get('geographicDetails'))) as BeneficiaryContactMechanism);
                }
                else {
                    // this address was added
                    contactMechanisms = contactMechanisms.push(new BeneficiaryContactMechanism().withMutations(contactMechanismRecord => contactMechanismRecord
                        .set('ordinality', addressMechanisms.size)
                        .set('personUuid', state.getIn(['beneficiary', 'uuid']))
                        .setIn(['purposeType', 'id'], contactMechanismState.getIn(['addressType', 'id']) !== 0 ? contactMechanismState.getIn(['addressType', 'id']) : defaultAddressType.get('id'))
                        .setIn(['purposeType', 'value'], contactMechanismState.getIn(['addressType', 'value']) !== '' ? contactMechanismState.getIn(['addressType', 'value']) : defaultAddressType.get('value'))
                        .setIn(['purposeType', 'contactMechanismType', 'id'], addressType.get('id'))
                        .setIn(['purposeType', 'contactMechanismType', 'value'], addressType.get('value'))
                        .set('solicitationIndicatorType', contactMechanismState.getIn(['addressNotifications', 'id']) !== 0 ? contactMechanismState.get('addressNotifications') : defaultSolicitationIndicatorType.get('id'))
                        .setIn(['contactMechanism', 'type', 'id'], addressType.get('id'))
                        .setIn(['contactMechanism', 'type', 'value'], addressType.get('value'))
                        .setIn(['contactMechanism', 'address1'], contactMechanismState.get('street'))
                        .setIn(['contactMechanism', 'address2'], contactMechanismState.get('apt'))
                        .setIn(['contactMechanism', 'city'], contactMechanismState.get('city'))
                        .setIn(['contactMechanism', 'state'], contactMechanismState.get('state'))
                        .setIn(['contactMechanism', 'postalCode'], contactMechanismState.get('postalCode'))
                        .setIn(['contactMechanism', 'directions'], contactMechanismState.get('geographicDetails'))) as BeneficiaryContactMechanism);
                }
            });

            // emails next
            contactMechanismsState.get('emails').forEach(contactMechanismState => {
                // look for a matching beneficiary mechanism
                const emailMatch : BeneficiaryContactMechanism = emailMechanisms.find(email => email.getIn(['contactMechanism', 'uuid']) === contactMechanismState.get('uuid'));

                if (emailMatch) {
                    // this email was edited
                    contactMechanisms = contactMechanisms.push(emailMatch.withMutations(contactMechanismRecord => contactMechanismRecord
                            .setIn(['purposeType', 'id'], contactMechanismState.getIn(['emailType', 'id']) !== 0 ? contactMechanismState.getIn(['emailType', 'id']) : defaultEmailType.get('id'))
                            .setIn(['purposeType', 'value'], contactMechanismState.getIn(['emailType', 'value']) !== '' ? contactMechanismState.getIn(['emailType', 'value']) : defaultEmailType.get('value'))
                            .set('solicitationIndicatorType', contactMechanismState.getIn(['emailNotifications', 'id']) !== 0 ? contactMechanismState.get('emailNotifications') : defaultSolicitationIndicatorType.get('id'))
                            .setIn(['contactMechanism', 'electronicAddressString'], contactMechanismState.get('email'))
                        ) as BeneficiaryContactMechanism
                    );
                }
                else {
                    // this email was added
                    contactMechanisms = contactMechanisms.push(new BeneficiaryContactMechanism().withMutations(contactMechanismRecord => contactMechanismRecord
                            .set('ordinality', emailMechanisms.size)
                            .set('personUuid', state.getIn(['beneficiary', 'uuid']))
                            .setIn(['purposeType', 'id'], contactMechanismState.getIn(['emailType', 'id']) !== 0 ? contactMechanismState.getIn(['emailType', 'id']) : defaultEmailType.get('id'))
                            .setIn(['purposeType', 'value'], contactMechanismState.getIn(['emailType', 'value']) !== '' ? contactMechanismState.getIn(['emailType', 'value']) : defaultEmailType.get('value'))
                            .setIn(['purposeType', 'contactMechanismType', 'id'], emailType.get('id'))
                            .setIn(['purposeType', 'contactMechanismType', 'value'], emailType.get('value'))
                            .set('solicitationIndicatorType', contactMechanismState.getIn(['emailNotifications', 'id']) !== 0 ? contactMechanismState.get('emailNotifications') : defaultSolicitationIndicatorType.get('id'))
                            .setIn(['contactMechanism', 'type', 'id'], emailType.get('id'))
                            .setIn(['contactMechanism', 'type', 'value'], emailType.get('value'))
                            .setIn(['contactMechanism', 'electronicAddressString'], contactMechanismState.get('email'))
                    ) as BeneficiaryContactMechanism);
                }
            });

            // phones last
            contactMechanismsState.get('phones').forEach(contactMechanismState => {
                // look for a matching beneficiary mechanism
                const phoneMatch : BeneficiaryContactMechanism = phoneMechanisms.find(phone => phone.getIn(['contactMechanism', 'uuid']) === contactMechanismState.get('uuid'));

                if (phoneMatch) {
                    contactMechanisms = contactMechanisms.push(phoneMatch.withMutations(contactMechanismRecord => contactMechanismRecord
                            .setIn(['purposeType', 'id'], contactMechanismState.getIn(['phoneType', 'id']) !== 0 ? contactMechanismState.getIn(['phoneType', 'id']) : defaultPhoneType.get('id'))
                            .setIn(['purposeType', 'value'], contactMechanismState.getIn(['phoneType', 'value']) !== '' ? contactMechanismState.getIn(['phoneType', 'value']) : defaultPhoneType.get('value'))
                            .set('solicitationIndicatorType', contactMechanismState.getIn(['phoneNotifications', 'id']) !== 0 ? contactMechanismState.get('phoneNotifications') : defaultSolicitationIndicatorType.get('id'))
                            .setIn(['contactMechanism', 'areaCode'], parsePhoneNumberAreaCode(contactMechanismState.get('phone')))
                            .setIn(['contactMechanism', 'contactNumber'], parsePhoneNumberContactNumber(contactMechanismState.get('phone')))
                    )as BeneficiaryContactMechanism);
                }
                else {
                    // this phone was added
                    contactMechanisms = contactMechanisms.push(new BeneficiaryContactMechanism().withMutations(contactMechanismRecord => contactMechanismRecord
                            .set('ordinality', phoneMechanisms.size)
                            .set('personUuid', state.getIn(['beneficiary', 'uuid']))
                            .setIn(['purposeType', 'id'], contactMechanismState.getIn(['phoneType', 'id']) !== 0 ? contactMechanismState.getIn(['phoneType', 'id']) : defaultPhoneType.get('id'))
                            .setIn(['purposeType', 'value'], contactMechanismState.getIn(['phoneType', 'value']) !== '' ? contactMechanismState.getIn(['phoneType', 'value']) : defaultPhoneType.get('value'))
                            .setIn(['purposeType', 'contactMechanismType', 'id'], phoneType.get('id'))
                            .setIn(['purposeType', 'contactMechanismType', 'value'], phoneType.get('value'))
                            .set('solicitationIndicatorType', contactMechanismState.getIn(['phoneNotifications', 'id']) !== 0 ? contactMechanismState.get('phoneNotifications') : defaultSolicitationIndicatorType.get('id'))
                            .setIn(['contactMechanism', 'type', 'id'], phoneType.get('id'))
                            .setIn(['contactMechanism', 'type', 'value'], phoneType.get('value'))
                            .setIn(['contactMechanism', 'areaCode'], parsePhoneNumberAreaCode(contactMechanismState.get('phone')))
                            .setIn(['contactMechanism', 'contactNumber'], parsePhoneNumberContactNumber(contactMechanismState.get('phone')))
                    ) as BeneficiaryContactMechanism);
                }
            });
        }

        // update contactMechanisms on beneficiary domain model
        return state.setIn(['beneficiary', 'contactMechanisms'], contactMechanisms) as BeneficiaryState;
    }
    catch (err) {
        console.error('error: ' + err + ' during saveBeneficiaryContactMechanisms');
    }
}

/**
 * map the Beneficiary MedicalConditions portion of view model to the
 * Beneficiary Domain Model
 * @param state
 */
function saveBeneficiaryMedicalConditions(state : BeneficiaryState) : BeneficiaryState {
    try {
        const medicalConditionsState : BeneficiaryMedicalConditionState = state.getIn(['beneficiaryMedicalConditionsState', 'conditionsState']);

        // map medical conditions state to beneficiary domain model medical conditions
        state = state.setIn(['beneficiary', 'medicalConditions'], List<BeneficiaryMedicalCondition>(medicalConditionsState.map(conditionState =>
            new BeneficiaryMedicalCondition().withMutations(record => record
                .set('personUuid', state.getIn(['beneficiary', 'uuid']))
                .set('fromDate', conditionState.get('fromDate') ? moment(conditionState.get('fromDate')).format('YYYY-MM-DD') : undefined)
                .set('thruDate', conditionState.get('thruDate') ? moment(conditionState.get('thruDate')).format('YYYY-MM-DD') : undefined)
                .setIn(['type', 'id'], conditionState.getIn(['type', 'id']))
                .setIn(['type', 'value'], conditionState.getIn(['type', 'value']))) as BeneficiaryMedicalCondition
        ))) as BeneficiaryState;

        return state;
    }
    catch (err) {
        console.error('error: ' + err + ' during saveBeneficiaryMedicalConditions');
    }
}

/**
 * map the Beneficiary SpecialRequirements portion of view model to the
 * Beneficiary Domain Model
 * @param state
 */
function saveBeneficiarySpecialRequirements(state : BeneficiaryState) : BeneficiaryState {
    try {
        const specialRequirementsState : BeneficiarySpecialRequirement = state.getIn(['beneficiarySpecialRequirementsState', 'requirementsState']);

        // map special requirements state to beneficiary domain model special requirements
        return state.setIn(['beneficiary', 'specialRequirements'], List<BeneficiarySpecialRequirement>(specialRequirementsState.map(requirementState =>
            new BeneficiarySpecialRequirement().withMutations(record => record
                .set('personUuid', state.getIn(['beneficiary', 'uuid']))
                .set('fromDate', requirementState.get('fromDate') ? moment(requirementState.get('fromDate')).format('YYYY-MM-DD') : undefined)
                .set('thruDate', requirementState.get('thruDate') ? moment(requirementState.get('thruDate')).format('YYYY-MM-DD') : undefined)
                .setIn(['type', 'id'], requirementState.getIn(['type', 'id']))
                .setIn(['type', 'value'], requirementState.getIn(['type', 'value']))) as BeneficiarySpecialRequirement
        ))) as BeneficiaryState;
    }
    catch (err) {
        console.error('error: ' + err + ' during saveBeneficiarySpecialRequirements');
    }
}

/**
 * map the Beneficiary Connections portion of view model to the
 * Beneficiary Domain Model
 * @param state
 */
function saveBeneficiaryConnections(state : BeneficiaryState) : BeneficiaryState {
    const beneficiaryConnectionsVM  : List<BeneficiaryConnectionsStateDetail>   = state.getIn(['beneficiaryConnectionsState', 'connectionsStateDetail']),
          personConnectionTypes     : List<KeyValuePair>                        = state.getIn(['beneficiaryConnectionsState', 'dropdownTypes', 'personConnectionTypes']);

    let beneficiaryConnections : List<BeneficiaryConnection> = List<BeneficiaryConnection>();

    // begin mapping view model data to Beneficiary Connections Domain Model
    beneficiaryConnectionsVM.forEach(beneficiaryConnectionState => {
        // look for a matching beneficiary connection
        const currentBeneficiaryConnection : BeneficiaryConnection = state.getIn(['beneficiary', 'connections']).find(connection => connection.get('uuid') === beneficiaryConnectionState.get('uuid'));

        // only data the CSR can change is the connection type(s)
        let connectionTypes : List<KeyValuePair> = List<KeyValuePair>();

        // any types that are set to value of true should be added to connectionTypes list
        beneficiaryConnectionState.get('connectionTypes').forEach((connectionTypeValue, connectionTypeKey) => {
            if (connectionTypeValue) {
                // which value is this?
                if (connectionTypeKey === 'parentFamily') {
                    connectionTypes = connectionTypes.push(personConnectionTypes.find(connectionType => connectionType.get('value') === 'Parent / Family'));
                }
                else if (connectionTypeKey === 'socialWorker') {
                    connectionTypes = connectionTypes.push(personConnectionTypes.find(connectionType => connectionType.get('value') === 'Social / Case Worker / Case Manager'));
                }
                else if (connectionTypeKey === 'nurseDoctor') {
                    connectionTypes = connectionTypes.push(personConnectionTypes.find(connectionType => connectionType.get('value') === 'Nurse / Doctor / Counselor'));
                }
                else if (connectionTypeKey === 'planClient') {
                    connectionTypes = connectionTypes.push(personConnectionTypes.find(connectionType => connectionType.get('value') === 'Plan / Client'));
                }
                else if (connectionTypeKey === 'emergencyContact') {
                    connectionTypes = connectionTypes.push(personConnectionTypes.find(connectionType => connectionType.get('value') === 'Emergency Contact'));
                }
                else if (connectionTypeKey === 'driver') {
                    connectionTypes = connectionTypes.push(personConnectionTypes.find(connectionType => connectionType.get('value') === 'Driver'));
                }
            }
        });

        // existing beneficiary connection record??
        if (currentBeneficiaryConnection) {
            // update connection types on this beneficiary
            beneficiaryConnections = beneficiaryConnections.push(currentBeneficiaryConnection.set('types', connectionTypes) as BeneficiaryConnection);
        }
        // looks like we added a new connection somewhere...hang on to something...
        else {
            // make sure this isn't a dummy record that's just been added to keep a Beneficiary Connection form from blowing up
            if (beneficiaryConnectionState.get('uuid')) {
                // update uuid and connection types on this new beneficiary connection
                beneficiaryConnections = beneficiaryConnections.push(new BeneficiaryConnection().withMutations(connectionRecord => connectionRecord
                        .set('uuid', beneficiaryConnectionState.get('uuid'))
                        .set('types', connectionTypes)) as BeneficiaryConnection);
            }
        }
    });

    // update beneficiary's connections for saving
    return state.setIn(['beneficiary', 'connections'], beneficiaryConnections) as BeneficiaryState;
}

/**
 * map the Beneficiary Service Coverages portion of view model to the
 * Beneficiary Domain Model
 * @param state
 */
function saveBeneficiaryServiceCoverages(state : BeneficiaryState) : BeneficiaryState {
    const serviceCoverages      : List<BeneficiaryServiceCoverage>      = state.getIn(['beneficiary', 'serviceCoverages']),
          serviceCoveragesState : List<BeneficiaryPlansStateSummary>    = state.getIn(['beneficiaryPlansState', 'plansStateSummary']);

    // map service coverages state to beneficiary domain model service coverages
    return state.setIn(['beneficiary', 'serviceCoverages'], serviceCoveragesState.map((coverageState, coverageStateIndex) => {
        // grab the data that stores the values for all the dynamic plan fields (Beneficiary ID, sub plan, etc)
        // that corresponds to the organization/plan selections in the UI
        const matchingPlanData : List<BeneficiaryServiceCoveragePlanClassificationState> = coverageState.get('plansStateDetail')
                                                                                                        .find(org => org.get('id') === coverageState.get('organizationUuid'))
                                                                                                        .get('serviceCoveragePlans')
                                                                                                        .find(plan => plan.get('id') === coverageState.get('planUuid'))
                                                                                                        .get('serviceCoveragePlanClassifications');

        // flag to track if user has changed plans or not
        // for this plan index
        let isSamePlan : boolean,

            // grab current service coverage record at this index from beneficiary model's serviceCoverages
            serviceCoverage : BeneficiaryServiceCoverage = serviceCoverages.get(coverageStateIndex);

        // is there an existing service coverage record at this index?
        if (serviceCoverage) {
            // did they select a new Plan different from the previous selection??
            serviceCoverage.getIn(['serviceCoveragePlan', 'uuid']) === coverageState.get('planUuid') ? isSamePlan = true : isSamePlan = false;

            // if CSR selected a new plan for the beneficiary, remove all pre-existing classifications
            if (!isSamePlan) {
                serviceCoverage = serviceCoverage.set('personServiceCoveragePlanClassifications', List<BeneficiaryPersonServiceCoveragePlanClassification>()) as BeneficiaryServiceCoverage;
            }

            return serviceCoverage.withMutations(coverageRecord => coverageRecord
                // organization data
                .setIn(['serviceCoveragePlan', 'serviceCoverageOrganization', 'name'], coverageState.get('organization'))
                .setIn(['serviceCoveragePlan', 'serviceCoverageOrganization', 'uuid'], coverageState.get('organizationUuid'))

                // plan data
                .setIn(['serviceCoveragePlan', 'name'], coverageState.get('planDescription'))
                .setIn(['serviceCoveragePlan', 'uuid'], coverageState.get('planUuid'))

                // coverage dates
                .set('fromDate', coverageState.get('fromDate') ? moment(coverageState.get('fromDate')).format('YYYY-MM-DD') : '')
                .set('thruDate', coverageState.get('thruDate') ? moment(coverageState.get('thruDate')).format('YYYY-MM-DD') : '')

                // plan dynamic fields data (this data was found earlier above and assigned to matchingPlansData)
                .set('personServiceCoveragePlanClassifications', matchingPlanData.map(planDetail => {
                    // updating an existing plan?
                    if (isSamePlan) {
                        // match against existing classification fields by classification type
                        const planClassificationOld : BeneficiaryPersonServiceCoveragePlanClassification = serviceCoverage.get('personServiceCoveragePlanClassifications')
                                    .find(classification => classification.getIn(['serviceCoveragePlanClassification', 'serviceCoveragePlanClassificationType', 'uuid']) === planDetail.getIn(['classificationType', 'id']));

                        // was there already data for this field?
                        if (planClassificationOld) {
                            // just updated the existing record's value
                            return planClassificationOld.set('value', planDetail.get('selectedValue'));
                        }
                        // user entered data for a field that was not previously captured
                        else {
                            // return new entry for this field
                            return new BeneficiaryPersonServiceCoveragePlanClassification().withMutations(coveragePlanClassificationRecord => coveragePlanClassificationRecord
                                .set('value', planDetail.get('selectedValue'))
                                .setIn(['serviceCoveragePlanClassification', 'uuid'], planDetail.get('id')));
                        }
                    }
                    // or adding dynamic plan field values for a newly selected plan?
                    else {
                        return new BeneficiaryPersonServiceCoveragePlanClassification().withMutations(coveragePlanClassificationRecord => coveragePlanClassificationRecord
                            .set('value', planDetail.get('selectedValue'))
                            .setIn(['serviceCoveragePlanClassification', 'uuid'], planDetail.get('id')));
                    }
                })));
        }
        else {
            // this is a new added service coverage record
            isSamePlan = false;

            // create new beneficiary service coverage record at this index
            return new BeneficiaryServiceCoverage().withMutations(coverageRecord => coverageRecord
                // beneficiary uuid
                .set('personUuid', state.getIn(['beneficiary', 'uuid']))

                // organization data
                .setIn(['serviceCoveragePlan', 'serviceCoverageOrganization', 'name'], coverageState.get('organization'))
                .setIn(['serviceCoveragePlan', 'serviceCoverageOrganization', 'uuid'], coverageState.get('organizationUuid'))

                // plan data
                .setIn(['serviceCoveragePlan', 'name'], coverageState.get('planDescription'))
                .setIn(['serviceCoveragePlan', 'uuid'], coverageState.get('planUuid'))

                // coverage dates
                .set('fromDate', coverageState.get('fromDate') ? moment(coverageState.get('fromDate')).format('YYYY-MM-DD') : '')
                .set('thruDate', coverageState.get('thruDate') ? moment(coverageState.get('thruDate')).format('YYYY-MM-DD') : '')

                // plan dynamic fields data (this data was found earlier above and assigned to matchingPlansData)
                .set('personServiceCoveragePlanClassifications', matchingPlanData.map(planDetail =>
                    new BeneficiaryPersonServiceCoveragePlanClassification().withMutations(coveragePlanClassificationRecord => coveragePlanClassificationRecord
                        .set('value', planDetail.get('selectedValue'))
                        .setIn(['serviceCoveragePlanClassification', 'uuid'], planDetail.get('id'))))));
        }
    })) as BeneficiaryState;
}

/**
 * copy Beneficiary Information segment of Beneficiary data to beneficiaryBackup or vice versa
 * @param state
 * @returns {BeneficiaryState}
 */
function doBeneficiaryBackup(state : BeneficiaryState) : BeneficiaryState {
    // is create new profile active?
    if (!state.get('newProfileActive')) {
        // if editing was enabled, backup form data
        if (
            state.getIn(['beneficiaryInfoState', 'isBeneficiaryInfoEditing'])                   ||
            state.getIn(['beneficiaryContactMechanismsState', 'isContactMechanismsEditing'])    ||
            state.getIn(['beneficiaryConnectionsState', 'isEditConnectionActive'])              ||
            state.getIn(['beneficiaryPlansState', 'isEditPlanActive'])
        ) {
            // backup beneficiary information
            state = state.set('beneficiaryBackup', new BeneficiaryBackup().withMutations(record => record
                        .set('backupActive', true)
                        .setIn(['beneficiaryInfoState', 'beneficiaryInfo'], state.getIn(['beneficiaryInfoState', 'beneficiaryInfo']))
                        .setIn(['beneficiarySpecialRequirementsState', 'requirementsState'], state.getIn(['beneficiarySpecialRequirementsState', 'requirementsState']))
                        .setIn(['beneficiaryContactMechanismsState', 'contactMechanismsState'], state.getIn(['beneficiaryContactMechanismsState', 'contactMechanismsState']))
                        .setIn(['beneficiaryPlansState', 'plansStateSummary'], state.getIn(['beneficiaryPlansState', 'plansStateSummary']))
                        .setIn(['beneficiaryMedicalConditionsState', 'conditionsState'], state.getIn(['beneficiaryMedicalConditionsState', 'conditionsState']))
                        .setIn(['beneficiaryConnectionsState', 'connectionsStateDetail'], state.getIn(['beneficiaryConnectionsState', 'connectionsStateDetail']))) as BeneficiaryBackup
            ) as BeneficiaryState;
        }
        else {
            // check for existence of any prior beneficiary backup data
            if (state.getIn(['beneficiaryBackup', 'backupActive'])) {
                // copy backed up data over to beneficiary
                state = state.withMutations(record => record
                        // set backup flag to false
                        .setIn(['beneficiaryBackup', 'backupActive'], false)

                        // re-populate view models
                        .setIn(['beneficiaryInfoState', 'beneficiaryInfo'], state.getIn(['beneficiaryBackup', 'beneficiaryInfoState', 'beneficiaryInfo']))
                        .setIn(['beneficiarySpecialRequirementsState', 'requirementsState'], state.getIn(['beneficiaryBackup', 'beneficiarySpecialRequirementsState', 'requirementsState']))
                        .setIn(['beneficiaryContactMechanismsState', 'contactMechanismsState'], state.getIn(['beneficiaryBackup', 'beneficiaryContactMechanismsState', 'contactMechanismsState']))
                        .setIn(['beneficiaryPlansState', 'plansStateSummary'], state.getIn(['beneficiaryBackup', 'beneficiaryPlansState', 'plansStateSummary']))
                        .setIn(['beneficiaryMedicalConditionsState', 'conditionsState'], state.getIn(['beneficiaryBackup', 'beneficiaryMedicalConditionsState', 'conditionsState']))
                        .setIn(['beneficiaryConnectionsState', 'connectionsStateDetail'], state.getIn(['beneficiaryBackup', 'beneficiaryConnectionsState', 'connectionsStateDetail']))

                        // null backup
                        .set('beneficiaryBackup', new BeneficiaryBackup())) as BeneficiaryState;
            }
        }
    }

    return state;
}

/**
 * copies raw beneficiary profile data from the API into a series of view models
 * that are more easily consumed by the UI templates
 *
 * @param state current BeneficiaryState
 *
 * @returns {BeneficiaryState}
 */
function populateViewModels(state : BeneficiaryState) : BeneficiaryState {
    let beneficiaryInfo         : BeneficiaryInfo                         = new BeneficiaryInfo(),
        requirementsState       : List<BeneficiarySpecialRequirement>     = List<BeneficiarySpecialRequirement>(),
        conditionsState         : List<BeneficiaryMedicalConditionState>  = List<BeneficiaryMedicalConditionState>(),
        contactMechanismsState  : BeneficiaryContactMechanismStateDetail  = new BeneficiaryContactMechanismStateDetail(),
        connectionsState        : List<BeneficiaryConnectionsStateDetail> = List<BeneficiaryConnectionsStateDetail>(),
        plansStateSummary       : List<BeneficiaryPlansStateSummary>      = List<BeneficiaryPlansStateSummary>(),
        plansStateDetail        : List<BeneficiaryPlansStateDetail>       = List<BeneficiaryPlansStateDetail>();

    const beneficiary       : BeneficiaryState = state.get('beneficiary'),
        isNewProfileActive  : boolean          = state.get('newProfileActive');

    // is new profile active?
    if (isNewProfileActive) {
        // contact info state
        contactMechanismsState = contactMechanismsState.withMutations(record => record
                // addresses
                .set('addresses', record.get('addresses', List()).push(new BeneficiaryAddress().withMutations(addressRecord => {
                    const addressType : ContactMechanismType = state.getIn(
                        ['beneficiaryConnectionsState', 'dropdownTypes', 'contactMechanismTypes'],
                        List()
                    )
                        .find(mechanism => mechanism.get('value') === 'Address', this, Map());

                    return addressRecord
                        .setIn(['mechanismType', 'id'], addressType.get('id'))
                        .setIn(['mechanismType', 'value'], addressType.get('value'));
                })))

                // emails
                .set('emails', record.get('emails').push(new BeneficiaryEmail().withMutations(emailRecord => {
                    const emailType : ContactMechanismType = state.getIn(
                        ['beneficiaryConnectionsState', 'dropdownTypes', 'contactMechanismTypes'],
                        List()
                    )
                        .find(mechanism => mechanism.get('value') === 'Electronic Address', this, Map());

                    return emailRecord
                        .setIn(['mechanismType', 'id'], emailType.get('id'))
                        .setIn(['mechanismType', 'value'], emailType.get('value'));
                })))

                // phones
                .set('phones', record.get('phones').push(new BeneficiaryPhone().withMutations(phoneRecord => {
                    const phoneType : ContactMechanismType = state.getIn(
                        ['beneficiaryConnectionsState', 'dropdownTypes', 'contactMechanismTypes'],
                        List()
                    )
                        .find(mechanism => mechanism.get('value') === 'Telecommunications Number', this, Map());

                    return phoneRecord
                        .setIn(['mechanismType', 'id'], phoneType.get('id'))
                        .setIn(['mechanismType', 'value'], phoneType.get('value'));
                })))) as BeneficiaryContactMechanismStateDetail;
    }
    else {
        // beneficiary info state
        beneficiaryInfo = beneficiaryInfo.withMutations(record => {
            let weight            : string = '0',
                heightFeet        : string = '0',
                heightInches      : string = '0',
                beneficiaryWeight : BeneficiaryPhysicalCharacteristic,
                beneficiaryHeight : BeneficiaryPhysicalCharacteristic;

            // pre-process physical characteristics
            if (beneficiary.get('physicalCharacteristics').size > 0) {
                beneficiaryWeight   = beneficiary.get('physicalCharacteristics').find(value => value.getIn(['type', 'value']) === 'Weight');
                beneficiaryHeight   = beneficiary.get('physicalCharacteristics').find(value => value.getIn(['type', 'value']) === 'Height');

                beneficiaryWeight ? weight = beneficiaryWeight.get('value') : weight = '0';
                beneficiaryHeight ? heightFeet = Math.floor(beneficiaryHeight.get('value') / 12).toString() : heightFeet = '0';
                beneficiaryHeight ? heightInches = Math.floor(beneficiaryHeight.get('value') % 12).toString() : heightInches = '0';
            }

            // copy over the beneficiary demographic info
            return record
                .set('birthDate', beneficiary.get('birthDate'))
                .set('firstName', beneficiary.get('firstName'))
                .set('lastName', beneficiary.get('lastName'))
                .set('middleName', beneficiary.get('middleName'))
                .set('nickname', beneficiary.get('nickname'))
                .set('uuid', beneficiary.get('uuid'))
                .set('personalTitle', beneficiary.get('personalTitle'))
                .set('suffix', beneficiary.get('suffix'))
                .set('deathDate', beneficiary.get('deathDate'))
                .set('gender', beneficiary.getIn(['gender', 'value']))
                .set('language', beneficiary.get('languages').size > 0 ? beneficiary.get('languages').find(value => value.get('ordinality') === 0).getIn(['language', 'value']) : '')
                .set('weight', weight)
                .set('heightFeet', heightFeet)
                .set('heightInches', heightInches);
        }) as BeneficiaryInfo;

        // set isDeceased flag
        beneficiaryInfo.get('deathDate') && beneficiaryInfo.get('deathDate') !== '' ? beneficiaryInfo = beneficiaryInfo.set('isDeceased', true) as BeneficiaryInfo : beneficiaryInfo = beneficiaryInfo.set('isDeceased', false) as BeneficiaryInfo;

        // -------------------------------------------------------------------------------------------
        // special requirements state
        requirementsState = state.getIn(['beneficiary', 'specialRequirements'], List()).map(value =>
            new BeneficiarySpecialRequirement().withMutations(record => record
                    .set('personUuid', value.get('personUuid'))
                    .set('fromDate', value.get('fromDate'))
                    .set('thruDate', value.get('thruDate'))
                    .set('sequenceId', value.get('sequenceId'))
                    .set('version', value.get('version'))
                    .set('type', value.get('type'))
                    .set('permanentRequirement', value.get('fromDate') === undefined && value.get('thruDate') === undefined)));

        // -------------------------------------------------------------------------------------------
        // medical conditions state
        conditionsState = state.getIn(['beneficiary', 'medicalConditions'], List())
            .map(value =>
                new BeneficiaryMedicalConditionState()
                    .withMutations(record => record
                        .set('personUuid', value.get('personUuid'))
                        .set('fromDate', value.get('fromDate'))
                        .set('thruDate', value.get('thruDate'))
                        .set('sequenceId', value.get('sequenceId'))
                        .set('version', value.get('version'))
                        .set('type', value.get('type'))
                    )
            );

        // -------------------------------------------------------------------------------------------
        // contact info state

        // check for null value first
        if (state.getIn(['beneficiary', 'contactMechanisms']) !== undefined) {
            // any items to map?
            if (state.getIn(['beneficiary', 'contactMechanisms'], List()).count() > 0) {
                const addressSize : number = state.getIn(['beneficiary', 'contactMechanisms'], List())
                          .filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Address').count();
                const emailSize : number = state.getIn(['beneficiary', 'contactMechanisms'], List())
                          .filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Electronic Address').count();
                const phoneSize : number = state.getIn(['beneficiary', 'contactMechanisms'], List())
                          .filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Telecommunications Number').count();

                contactMechanismsState = contactMechanismsState.withMutations(record => record
                        // addresses
                        .set('addresses', addressSize > 0 ? state.getIn(['beneficiary', 'contactMechanisms']).filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Address').map(value =>
                                new BeneficiaryAddress().withMutations(address => address
                                        .set('sequenceId', value.get('sequenceId'))
                                        .set('uuid', value.getIn(['contactMechanism', 'uuid']))
                                        .set('version', value.getIn(['contactMechanism', 'version']))
                                        .set('street', value.getIn(['contactMechanism', 'address1']))
                                        .set('apt', value.getIn(['contactMechanism', 'address2']))
                                        .set('city', value.getIn(['contactMechanism', 'city']))
                                        .set('state', value.getIn(['contactMechanism', 'state']))
                                        .set('postalCode', value.getIn(['contactMechanism', 'postalCode']))
                                        .set('geographicDetails', value.getIn(['contactMechanism', 'directions']))
                                        .set('addressNotifications', value.get('solicitationIndicatorType'))
                                        .set('addressType', value.get('purposeType'))
                                        .set('mechanismType', value.getIn(['contactMechanism', 'type']))) as BeneficiaryAddress) : record.get('addresses').push(new BeneficiaryAddress()))

                        // emails
                        .set('emails', emailSize > 0 ? state.getIn(['beneficiary', 'contactMechanisms']).filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Electronic Address').map(value =>
                                new BeneficiaryEmail().withMutations(email => email
                                        .set('sequenceId', value.get('sequenceId'))
                                        .set('uuid', value.getIn(['contactMechanism', 'uuid']))
                                        .set('version', value.getIn(['contactMechanism', 'version']))
                                        .set('email', value.getIn(['contactMechanism', 'electronicAddressString']))
                                        .set('emailType', value.get('purposeType'))
                                        .set('emailNotifications', value.get('solicitationIndicatorType'))
                                        .set('mechanismType', value.getIn(['contactMechanism', 'type']))) as BeneficiaryEmail) : record.get('emails').push(new BeneficiaryEmail()))

                        // phones
                        .set('phones', phoneSize > 0 ? state.getIn(['beneficiary', 'contactMechanisms']).filter(stuff => stuff.getIn(['contactMechanism', 'type', 'value']) === 'Telecommunications Number').map(value =>
                                new BeneficiaryPhone().withMutations(phone => phone
                                        .set('sequenceId', value.get('sequenceId'))
                                        .set('uuid', value.getIn(['contactMechanism', 'uuid']))
                                        .set('version', value.getIn(['contactMechanism', 'version']))
                                        .set('phone', '(' + value.getIn(['contactMechanism', 'areaCode']) + ') ' + value.getIn(['contactMechanism', 'contactNumber']).substr(0, 3) + '-' + value.getIn(['contactMechanism', 'contactNumber']).substr(3, 7))
                                        .set('phoneType', value.get('purposeType'))
                                        .set('phoneNotifications', value.get('solicitationIndicatorType'))
                                        .set('mechanismType', value.getIn(['contactMechanism', 'type']))) as BeneficiaryPhone) : record.get('phones').push(new BeneficiaryPhone()))
                ) as BeneficiaryContactMechanismStateDetail;
            }
            else {
                contactMechanismsState = contactMechanismsState.withMutations(record => record
                        // addresses
                        .set('addresses', record.get('addresses', List()).push(new BeneficiaryAddress()))

                        // emails
                        .set('emails', record.get('emails', List()).push(new BeneficiaryEmail()))

                        // phones
                        .set('phones', record.get('phones', List()).push(new BeneficiaryPhone()))) as BeneficiaryContactMechanismStateDetail;
            }
        }
        else {
            contactMechanismsState = contactMechanismsState.withMutations(record => record
                    // addresses
                    .set('addresses', record.get('addresses', List()).push(new BeneficiaryAddress()))

                    // emails
                    .set('emails', record.get('emails', List()).push(new BeneficiaryEmail()))

                    // phones
                    .set('phones', record.get('phones', List()).push(new BeneficiaryPhone()))) as BeneficiaryContactMechanismStateDetail;
        }

        // -------------------------------------------------------------------------------------------
        // connections state

        // check for null value first
        if (state.getIn(['beneficiary', 'connections']) !== undefined) {
            if (state.getIn(['beneficiary', 'connections'], List()).count() > 0) {
                connectionsState = state.getIn(['beneficiary', 'connections'], List()).map(connection =>
                    new BeneficiaryConnectionsStateDetail().withMutations(connectionStateRecord => connectionStateRecord
                        /**
                         * map the connection's person
                         */
                        .set('uuid', connection.get('uuid'))
                        .set('firstName', connection.get('firstName'))
                        .set('middleName', connection.get('middleName'))
                        .set('lastName', connection.get('lastName'))
                        .set('personalTitle', connection.get('personalTitle'))
                        .set('suffix', connection.get('suffix'))
                        .set('birthDate', connection.get('birthDate', ''))
                        .set('gender', connection.get('gender'))

                        /**
                         * map only phone contact mechanisms
                         */
                        .set('phones', connection.get('contactMechanisms', List()).count() > 0 ? connection.get('contactMechanisms')
                                .filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'value']) === 'Telecommunications Number')
                                .map(connectionPhone =>
                                    new BeneficiaryPhone().withMutations(phone => phone
                                            .set('uuid', connectionPhone.getIn(['contactMechanism', 'uuid']))
                                            .set('phone', '(' + connectionPhone.getIn(['contactMechanism', 'areaCode']) + ') ' + connectionPhone.getIn(['contactMechanism', 'contactNumber']).substr(0, 3) + '-' + connectionPhone.getIn(['contactMechanism', 'contactNumber']).substr(3, 7))
                                            .set('phoneType', connectionPhone.get('purposeType'))
                                            .set('phoneNotifications', connectionPhone.get('solicitationIndicatorType'))
                                            .set('mechanismType', connectionPhone.getIn(['contactMechanism', 'type'])))) : connectionStateRecord.get('phones', List()).push(new BeneficiaryPhone()))

                        /**
                         * map the connection's type(s)
                         */
                        .set('connectionTypes', new BeneficiaryConnectionTypes().withMutations(stuff => stuff
                                .set('parentFamily', !!connection.get('types', List()).find(connectionType => connectionType.get('value') === 'Parent / Family'))
                                .set('socialWorker', !!connection.get('types', List()).find(connectionType => connectionType.get('value') === 'Social / Case Worker / Case Manager'))
                                .set('nurseDoctor', !!connection.get('types', List()).find(connectionType => connectionType.get('value') === 'Nurse / Doctor / Counselor'))
                                .set('planClient', !!connection.get('types', List()).find(connectionType => connectionType.get('value') === 'Plan / Client'))
                                .set('emergencyContact', !!connection.get('types', List()).find(connectionType => connectionType.get('value') === 'Emergency Contact'))
                                .set('driver', !!connection.get('types', List()).find(connectionType => connectionType.get('value') === 'Driver'))))));

                // check for existing connection here.  If a connection has multiple types i.e. Parent/Family etc we will receive a different connection
                // entry in the connections collection.  Each entry will have the same person/metadata information but the type info will differ
                // for each connection type that is associated with the beneficiary connection.
                // we use target.uuid to determine if we have multiple records that correspond to the same beneficiary connection with multiple connection types
                let tempConnections : List<BeneficiaryConnectionsStateDetail> = List<BeneficiaryConnectionsStateDetail>();

                connectionsState.forEach(value => {
                    // look for multiple entries in our connectionState for this beneficiary connection by uuid
                    const connectionsMatch : List<BeneficiaryConnectionsStateDetail> = List<BeneficiaryConnectionsStateDetail>(connectionsState.filter(stuff => stuff.get('uuid') === value.get('uuid')));

                    // did we find duplicate entries for this beneficiary connection's uuid?
                    if (connectionsMatch.size > 1) {
                        // we have duplicate entries, this signifies a beneficiary connection with multiple connection types

                        // have we already processed this entry?
                        if (tempConnections.findIndex(omega => omega.get('uuid') === value.get('uuid')) === -1) {
                            // aggregate the resulting aggregated beneficiary connection data from all the duplicates here
                            let tempConnection : BeneficiaryConnectionsStateDetail = new BeneficiaryConnectionsStateDetail(connectionsMatch.get(0));

                            // go through all the duplicate records
                            connectionsMatch.forEach(alpha => {
                                tempConnection = tempConnection.withMutations(tempConnectionRecord => tempConnectionRecord
                                        .setIn(['connectionTypes', 'parentFamily'], alpha.getIn(['connectionTypes', 'parentFamily']) ? alpha.getIn(['connectionTypes', 'parentFamily']) : tempConnectionRecord.getIn(['connectionTypes', 'parentFamily']))
                                        .setIn(['connectionTypes', 'socialWorker'], alpha.getIn(['connectionTypes', 'socialWorker']) ? alpha.getIn(['connectionTypes', 'socialWorker']) : tempConnectionRecord.getIn(['connectionTypes', 'socialWorker']))
                                        .setIn(['connectionTypes', 'nurseDoctor'], alpha.getIn(['connectionTypes', 'nurseDoctor']) ? alpha.getIn(['connectionTypes', 'nurseDoctor']) : tempConnectionRecord.getIn(['connectionTypes', 'nurseDoctor']))
                                        .setIn(['connectionTypes', 'planClient'], alpha.getIn(['connectionTypes', 'planClient']) ? alpha.getIn(['connectionTypes', 'planClient']) : tempConnectionRecord.getIn(['connectionTypes', 'planClient']))
                                        .setIn(['connectionTypes', 'emergencyContact'], alpha.getIn(['connectionTypes', 'emergencyContact']) ? alpha.getIn(['connectionTypes', 'emergencyContact']) : tempConnectionRecord.getIn(['connectionTypes', 'emergencyContact']))
                                        .setIn(['connectionTypes', 'driver'], alpha.getIn(['connectionTypes', 'driver']) ? alpha.getIn(['connectionTypes', 'driver']) : tempConnectionRecord.getIn(['connectionTypes', 'driver']))
                                ) as BeneficiaryConnectionsStateDetail;
                            });

                            // add aggregate beneficiary connection entity to temporary List of beneficiary connections
                            tempConnections = tempConnections.push(tempConnection);
                        }
                        else {
                            // skip this one, we've already processed it
                        }
                    }
                    else {
                        // add this to our temporary connections List
                        tempConnections = tempConnections.push(value);
                    }
                });

                // update connectionsState with aggregated beneficiary connections data
                connectionsState = tempConnections;
            }
        }

        // -------------------------------------------------------------------------------------------
        // service coverages state

        // check for null value first
        if (state.getIn(['beneficiary', 'serviceCoverages']) !== undefined) {
            if (state.getIn(['beneficiary', 'serviceCoverages'], List()).count() > 0) {
                plansStateSummary = state.getIn(['beneficiary', 'serviceCoverages'], List()).map(value => new BeneficiaryPlansStateSummary().withMutations(record => {
                    // search for a service coverage classification of type Id
                    const beneficiaryId : BeneficiaryPersonServiceCoveragePlanClassification = value.get('personServiceCoveragePlanClassifications', List())
                                                                                                    .find(classification => classification.getIn(['serviceCoveragePlanClassification', 'serviceCoveragePlanClassificationType', 'name']) === 'Id');
                    let planStateRecord = record
                        .set('organizationUuid', value.getIn(['serviceCoveragePlan', 'serviceCoverageOrganization', 'uuid']))
                        .set('organization', value.getIn(['serviceCoveragePlan', 'serviceCoverageOrganization', 'name']))
                        .set('planDescription', value.getIn(['serviceCoveragePlan', 'name']))
                        .set('planUuid', value.getIn(['serviceCoveragePlan', 'uuid']))
                        .set('fromDate', value.get('fromDate') ? moment(value.get('fromDate')).format('MM/DD/YYYY') : '')
                        .set('thruDate', value.get('thruDate') ? moment(value.get('thruDate')).format('MM/DD/YYYY') : '')
                        .set('planIdLabel', beneficiaryId ? beneficiaryId.getIn(['serviceCoveragePlanClassification', 'name']) : '')
                        .set('planIdValue', beneficiaryId ? beneficiaryId.get('value') : 'N/A');

                    // service coverages/plans state detail

                    // build plansStateDetail
                    plansStateDetail = List<BeneficiaryPlansStateDetail>(serviceCoverageOrganizationsMetaData.map(coverageValue => new BeneficiaryPlansStateDetail(coverageValue)));

                    // store on base plansState
                    planStateRecord.set('plansStateDetail', plansStateDetail);

                    // mark the indexes of the coverage organization and coverage plan to make updating below easier
                    const coverageOrgIndex  : number = planStateRecord.get('plansStateDetail', List()).findIndex(coverage => coverage.get('id') === planStateRecord.get('organizationUuid')),
                          coveragePlanIndex : number = planStateRecord.getIn(['plansStateDetail', coverageOrgIndex, 'serviceCoveragePlans'], List()).findIndex(plan => plan.get('id') === planStateRecord.get('planUuid'));

                    // update data structure with current beneficiary plan values
                    value.get('personServiceCoveragePlanClassifications', List())
                        .sort((a, b) => a.getIn(['serviceCoveragePlanClassification', 'ordinality']) > b.getIn(['serviceCoveragePlanClassification', 'ordinality']))
                        .forEach(planField => {
                            planStateRecord = planStateRecord.updateIn(['plansStateDetail', coverageOrgIndex, 'serviceCoveragePlans', coveragePlanIndex, 'serviceCoveragePlanClassifications',
                                planStateRecord.getIn(['plansStateDetail', coverageOrgIndex, 'serviceCoveragePlans', coveragePlanIndex, 'serviceCoveragePlanClassifications'], List())
                                    .findIndex(classification => classification.get('id') === planField.getIn(['serviceCoveragePlanClassification', 'uuid']))], classificationRecord => classificationRecord.set('selectedValue', planField.get('value')));
                        });

                    return planStateRecord;
                }) as BeneficiaryPlansStateSummary);
            }
        }
    }

    // update beneficiary demographics
    return state.withMutations(record => record
            .setIn(['beneficiaryInfoState', 'beneficiaryInfo'], beneficiaryInfo)
            .setIn(['beneficiarySpecialRequirementsState', 'requirementsState'], requirementsState)
            .setIn(['beneficiarySpecialRequirementsState', 'tempSpecialRequirement', 'type', 'value'], 'SELECT')
            .setIn(['beneficiarySpecialRequirementsState', 'tempSpecialRequirement', 'personUuid'], state.getIn(['beneficiary', 'uuid']))
            .setIn(['beneficiaryMedicalConditionsState', 'conditionsState'], conditionsState)
            .setIn(['beneficiaryMedicalConditionsState', 'tempMedicalCondition', 'type', 'value'], 'SELECT')
            .setIn(['beneficiaryMedicalConditionsState', 'tempMedicalCondition', 'personUuid'], state.getIn(['beneficiary', 'uuid']))
            .setIn(['beneficiaryContactMechanismsState', 'contactMechanismsState'], contactMechanismsState)
            .setIn(['beneficiaryConnectionsState', 'connectionsStateDetail'], connectionsState)
            .setIn(['beneficiaryPlansState', 'plansStateSummary'], plansStateSummary)) as BeneficiaryState;
}
