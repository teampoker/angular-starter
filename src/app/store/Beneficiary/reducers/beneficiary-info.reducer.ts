import {IPayloadAction} from '../../app-store';
import {BeneficiaryInfoState} from '../types/beneficiary-info-state.model';
import {BeneficiaryInfoActions} from '../actions/beneficiary-info.actions';

/**
 * Beneficiary Information state reducer
 *
 * @param state
 * @param action
 * @returns {BeneficiaryInfoState}
 * @constructor
 */
export const BENEFICIARY_INFO_REDUCER = (state : BeneficiaryInfoState, action : IPayloadAction) : BeneficiaryInfoState => {
    switch (action.type) {
        case BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_INFO_EDITING :
            state = state.set('isBeneficiaryInfoEditing', action.payload) as BeneficiaryInfoState;

            break;
        case BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_INFO_FIELD_VALUE :
            // update beneficiary and beneficiary demographics state
            state = state.setIn(['beneficiaryInfo', action.payload.fieldName], action.payload.fieldValue) as BeneficiaryInfoState;

            break;
        case BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_INFO_DROPDOWN_TYPES :
            state = state.set('dropdownTypes', action.payload) as BeneficiaryInfoState;

            break;
        case BeneficiaryInfoActions.BENEFICIARY_INFO_SAVE_BENEFICIARY_INFO :
            if (!state.get('newProfileActive')) {
                // set editing flag to false
                state = state.withMutations(record => record
                    .set('isBeneficiaryInfoEditing', false)
                    .set('isWaiting', true)
                ) as BeneficiaryInfoState;
            }

            break;
        case BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_BENEFICIARY:
            // update to bene info has been received, we can stop waiting
            state = state.set('isWaiting', false) as BeneficiaryInfoState;
            break;
        default :
            return state;
    }

    return state;
};
