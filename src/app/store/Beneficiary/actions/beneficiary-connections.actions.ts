import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../../app-store';
import {NavActions} from '../../Navigation/nav.actions';
import {MetaDataTypesSelectors} from '../../MetaDataTypes/meta-data-types.selectors';
import {BeneficiaryService} from '../../../areas/Beneficiary/services/Beneficiary/beneficiary.service';
import {BeneficiaryStateSelectors} from '../selectors/beneficiary.selectors';
import {BeneficiaryConnectionsStateSelectors} from '../selectors/beneficiary-connections.selectors';
import {ReservationActions} from '../../Reservation/actions/reservation.actions';
import {ReservationStateSelectors} from '../../Reservation/selectors/reservation.selectors';
import {BeneficiaryConnectionsStateDetail} from '../types/beneficiary-connections-state-detail.model';
import {NameUuid} from '../../types/name-uuid.model';
import {BeneficiaryConnectionsState} from '../types/beneficiary-connections-state.model';
import {BeneficiaryConnectionsDropdownTypes} from '../types/beneficiary-connections-dropdown-types.model';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {ContactMechanismType} from '../../MetaDataTypes/types/contact-mechanism-type.model';
import {IBeneficiaryFieldUpdate} from '../types/beneficiary-field-update.model';
import {PeopleSearchResult} from '../../Search/types/people-search-result';
import {AlertItem, EnumAlertType} from '../../Navigation/types/alert-item.model';
import {Beneficiary} from '../types/beneficiary.model';
import {BeneficiaryConnection} from '../types/beneficiary-connection.model';
import {ReservationPassengerState} from '../../Reservation/types/reservation-passenger';
import {BeneficiarySpecialRequirement} from '../types/beneficiary-special-requirement.model';
import {BeneficiaryActions} from './beneficiary.actions';

@Injectable()

/**
 * Implementation of BeneficiaryConnectionsActions: Redux Action Creator Service that exposes methods to mutate Beneficiary Connections state
 */
export class BeneficiaryConnectionsActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static BENEFICIARY_CONNECTIONS_UPDATE_CONNECTION_DROPDOWN_TYPES     : string = 'BENEFICIARY_CONNECTIONS_UPDATE_CONNECTION_DROPDOWN_TYPES';
    static BENEFICIARY_CONNECTIONS_ADD_BENEFICIARY_CONNECTION           : string = 'BENEFICIARY_CONNECTIONS_ADD_BENEFICIARY_CONNECTION';
    static BENEFICIARY_CONNECTIONS_ADD_ADDITIONAL_PHONE                 : string = 'BENEFICIARY_CONNECTIONS_ADD_ADDITIONAL_PHONE';
    static BENEFICIARY_CONNECTIONS_REMOVE_ADDITIONAL_PHONE              : string = 'BENEFICIARY_CONNECTIONS_REMOVE_ADDITIONAL_PHONE';
    static BENEFICIARY_CONNECTIONS_EDIT_BENEFICIARY_CONNECTION          : string = 'BENEFICIARY_CONNECTIONS_EDIT_BENEFICIARY_CONNECTION';
    static BENEFICIARY_CONNECTIONS_REMOVE_BENEFICIARY_CONNECTION        : string = 'BENEFICIARY_CONNECTIONS_REMOVE_BENEFICIARY_CONNECTION';
    static BENEFICIARY_CONNECTIONS_CANCEL_ADD_CONNECTION                : string = 'BENEFICIARY_CONNECTIONS_CANCEL_ADD_CONNECTION';
    static BENEFICIARY_CONNECTIONS_CLOSE_ADD_CONNECTION                 : string = 'BENEFICIARY_CONNECTIONS_CLOSE_ADD_CONNECTION';
    static BENEFICIARY_CONNECTIONS_CANCEL_EDIT_CONNECTION               : string = 'BENEFICIARY_CONNECTIONS_CANCEL_EDIT_CONNECTION';
    static BENEFICIARY_CONNECTIONS_CLOSE_EDIT_CONNECTION                : string = 'BENEFICIARY_CONNECTIONS_CLOSE_EDIT_CONNECTION';
    static BENEFICIARY_CONNECTIONS_UPDATE_CONNECTION_FIELD_VALUE        : string = 'BENEFICIARY_CONNECTIONS_UPDATE_CONNECTION_FIELD_VALUE';
    static BENEFICIARY_CONNECTIONS_TOGGLE_ADD_PHONE_ACTIVE              : string = 'BENEFICIARY_CONNECTIONS_TOGGLE_ADD_PHONE_ACTIVE';
    static BENEFICIARY_CONNECTIONS_TOGGLE_ADD_CONNECTION_EXPANDED       : string = 'BENEFICIARY_CONNECTIONS_TOGGLE_ADD_CONNECTION_EXPANDED';
    static BENEFICIARY_CONNECTIONS_ADD_CONNECTION_FROM_PEOPLE_SEARCH    : string = 'BENEFICIARY_CONNECTIONS_ADD_CONNECTION_FROM_PEOPLE_SEARCH';
    static BENEFICIARY_CONNECTIONS_SAVE_BENEFICIARY_CONNECTIONS         : string = 'BENEFICIARY_CONNECTIONS_SAVE_BENEFICIARY_CONNECTIONS';
    static BENEFICIARY_CONNECTIONS_UPDATE_BENEFICIARY                   : string = 'BENEFICIARY_CONNECTIONS_UPDATE_BENEFICIARY';
    static BENEFICIARY_CONNECTIONS_TOGGLE_CONNECTION_ADDED_MANUALLY     : string = 'BENEFICIARY_CONNECTIONS_TOGGLE_CONNECTION_ADDED_MANUALLY';
    static BENEFICIARY_CONNECTIONS_TOGGLE_CONNECTION_ADDED_FROM_SEARCH  : string = 'BENEFICIARY_CONNECTIONS_TOGGLE_CONNECTION_ADDED_FROM_SEARCH';
    static BENEFICIARY_CONNECTIONS_POPULATE_MANUALLY_ADDED_CONNECTION   : string = 'BENEFICIARY_CONNECTIONS_POPULATE_MANUALLY_ADDED_CONNECTION';
    static BENEFICIARY_CONNECTIONS_UPDATE_NEW_CONNECTION_UUID           : string = 'BENEFICIARY_CONNECTIONS_UPDATE_NEW_CONNECTION_UUID';

    /**
     * BeneficiaryConnectionsActions constructor
     * @param store
     * @param navActions
     * @param metaTypesSelectors
     * @param beneficiaryService
     * @param beneficiarySelectors
     * @param beneficiaryConnectionsSelectors
     * @param reservationActions
     * @param reservationsSelectors
     * @param beneficiaryActions
     */
    constructor(
        private store                           : NgRedux<IAppStore>,
        private navActions                      : NavActions,
        private metaTypesSelectors              : MetaDataTypesSelectors,
        private beneficiaryService              : BeneficiaryService,
        private beneficiarySelectors            : BeneficiaryStateSelectors,
        private beneficiaryConnectionsSelectors : BeneficiaryConnectionsStateSelectors,
        private reservationActions              : ReservationActions,
        private reservationsSelectors           : ReservationStateSelectors,
        private beneficiaryActions              : BeneficiaryActions
    ) {}

    /**
     * dispatch action to update beneficiary model on Redux store with results
     * of a Beneficiary Person PUT operation
     * @param response
     */
    private updateBeneficiaryFromAPIResponse(response : any) {
        this.store.dispatch({
            type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_UPDATE_BENEFICIARY,
            payload : response
        });
    }

    /**
     * checks to see if the most recent Add a Connection workflow was triggered from Reservations screen
     * and if so, dispatches the appropriate action, based on where the Add a Connection workflow was triggered from in the Reservations UI,
     * to update Reservations State with the newly added connection info
     * @param addedConnection newly added connection
     */
    private updateReservationsStateWithAddedConnection(addedConnection : BeneficiaryConnectionsStateDetail) {
        let reservationsTrigger : string;       // a connection can be added from reservations, and this value indicates what Reservation Action we should dispatch after connection is added successfully

        // look for trigger that indicates where the add a connection workflow was triggered from
        this.reservationsSelectors.connectionRequestIssuedBy().subscribe(value => reservationsTrigger = value).unsubscribe();

        // now update Reservations state with this value
        if (reservationsTrigger) {
            if (reservationsTrigger === 'REQUESTED_BY') {
                const requestedBy = new NameUuid().withMutations(accelerator => accelerator
                    .set('uuid', addedConnection.get('uuid'))
                    .set('name', addedConnection.get('firstName') + ' ' + addedConnection.get('lastName'))) as NameUuid;

                this.reservationActions.updateRequestedBy(requestedBy);
            }
            else if (reservationsTrigger === 'PASSENGER') {
                const additionalPassenger = new ReservationPassengerState({
                    uuid : addedConnection.get('uuid'),
                    name : `${addedConnection.get('firstName')} ${addedConnection.get('lastName')}`,
                    specialRequirements : List<BeneficiarySpecialRequirement>()
                });

                this.reservationActions.updatePassenger(additionalPassenger);
            }
            else if (reservationsTrigger === 'DRIVER') {
                const driver = new NameUuid().withMutations(accelerator => accelerator
                    .set('uuid', addedConnection.get('uuid'))
                    .set('name', addedConnection.get('firstName') + ' ' + addedConnection.get('lastName'))) as NameUuid;

                this.reservationActions.updateModeOfTransportationDriver(driver);
            }
        }
    }

    /**
     * determine if a connection already exists locally
     * @returns {boolean}
     */
    private checkForDuplicateConnection(uuid : string) : boolean {
        let isDuplicate         : boolean = false,
            isNewProfileActive  : boolean = false,
            connections         : List<BeneficiaryConnection> = List<BeneficiaryConnection>(),
            connectionsVM       : List<BeneficiaryConnectionsStateDetail> = List<BeneficiaryConnectionsStateDetail>();

        // if editing a connection just bail on this step
        if (!this.beneficiaryConnectionsSelectors.isEditConnectionActive()) {
            this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();
            this.beneficiaryConnectionsSelectors.beneficiaryConnections().subscribe(value => connections = value).unsubscribe();
            this.beneficiaryConnectionsSelectors.beneficiaryConnectionsState().subscribe(value => connectionsVM = value.get('connectionsStateDetail', List())).unsubscribe();

            // is create profile active?
            if (isNewProfileActive) {
                // this is a new profile so we have to check against our connections viewmodel

                // if there's only one record then they are adding the beneficiary's very first connection and we can ignore this
                if (connectionsVM.size > 1) {
                    if (connectionsVM.find(connection => connection.get('uuid') === uuid)) {
                        // okay so we have a match but we need to look and see how many records there are total
                        // in the list of connections.  If there's only one, then this connection is currently being added
                        // however if there's two of this particular uuid, then the connection truly is being added a second time
                        if (connectionsVM.filter(connection => connection.get('uuid') === uuid).size > 1) {
                            isDuplicate = true;
                        }
                    }
                }
            }
            else {
                // check against the beneficiary domain model
                if (connections.size > 0) {
                    if (connections.find(connection => connection.get('uuid') === uuid)) {
                        isDuplicate = true;
                    }
                }
            }

        }

        return isDuplicate;
    }

    /**
     * updates necessary dropdown types for beneficiary connections form
     */
    updateBeneficiaryConnectionDropdownTypes() {
        let connectionsState : BeneficiaryConnectionsState = new BeneficiaryConnectionsState();

        // are the necessary dropdown types already populated??
        this.beneficiaryConnectionsSelectors.beneficiaryConnectionsState().subscribe(value => connectionsState = value).unsubscribe();

        if (connectionsState.getIn(['dropdownTypes', 'solicitationIndicatorTypes']).size === 0) {
            let connectionTypes             : BeneficiaryConnectionsDropdownTypes = new BeneficiaryConnectionsDropdownTypes(),
                solicitationIndicatorTypes  : List<KeyValuePair> = List<KeyValuePair>(),
                salutationTypes             : List<KeyValuePair> = List<KeyValuePair>(),
                phoneTypes                  : List<KeyValuePair> = List<KeyValuePair>(),
                personConnectionTypes       : List<KeyValuePair> = List<KeyValuePair>(),
                contactMechanismTypes       : List<ContactMechanismType> = List<ContactMechanismType>();

            this.metaTypesSelectors.salutations().subscribe(value => salutationTypes = value).unsubscribe();
            this.metaTypesSelectors.solicitationIndicatorTypes().subscribe(value => solicitationIndicatorTypes = value).unsubscribe();
            this.metaTypesSelectors.phoneTypes().subscribe(value => phoneTypes = value).unsubscribe();
            this.metaTypesSelectors.personConnectionTypes().subscribe(value => personConnectionTypes = value).unsubscribe();
            this.metaTypesSelectors.contactMechanismTypes().subscribe(value => contactMechanismTypes = value).unsubscribe();

            connectionTypes = connectionTypes.withMutations(record => record
                    .set('solicitationIndicatorTypes', solicitationIndicatorTypes)
                    .set('salutations', salutationTypes)
                    .set('phoneTypes', phoneTypes)
                    .set('personConnectionTypes', personConnectionTypes)
                    .set('contactMechanismTypes', contactMechanismTypes)
            ) as BeneficiaryConnectionsDropdownTypes;

            this.store.dispatch({
                type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_UPDATE_CONNECTION_DROPDOWN_TYPES,
                payload : connectionTypes
            });
        }
    }

    /**
     * dispatch action to update add beneficiary connection index on redux store
     */
    addBeneficiaryConnection() {
        let isNewProfileActive : boolean;

        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        if (!isNewProfileActive) {
            this.navActions.toggleIsPoppedNavState(true);
        }

        this.store.dispatch({ type : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_ADD_BENEFICIARY_CONNECTION });
    }

    /**
     * dispatch action to add additional beneficiary phone
     */
    addAdditionalConnectionPhone() {
        this.store.dispatch({ type : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_ADD_ADDITIONAL_PHONE });
    }

    /**
     * dispatch action to remove additional beneficiary phone
     * @param removeIndex
     */
    removeAdditionalConnectionPhone(removeIndex : number) {
        this.store.dispatch({
            type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_REMOVE_ADDITIONAL_PHONE,
            payload : removeIndex
        });
    }

    /**
     * dispatch action to update edit beneficiary connection index on redux store
     * @param index index of edited connection
     */
    editBeneficiaryConnection(index : number) {
        let isNewProfileActive : boolean;

        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        if (!isNewProfileActive) {
            this.navActions.toggleIsPoppedNavState(true);
        }

        this.store.dispatch({
            type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_EDIT_BENEFICIARY_CONNECTION,
            payload : index
        });
    }

    /**
     * dispatch action to cancel beneficiary add connection workflow
     */
    cancelAddBeneficiaryConnection() {
        this.navActions.toggleIsPoppedNavState(false);

        this.store.dispatch({ type : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_CANCEL_ADD_CONNECTION });
    }

    /**
     * dispatch action to close beneficiary add connection screen
     */
    closeAddBeneficiaryConnection() {
        this.navActions.toggleIsPoppedNavState(false);

        this.store.dispatch({ type : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_CLOSE_ADD_CONNECTION });
    }

    /**
     * dispatch action to cancel beneficiary edit connection workflow
     */
    cancelEditBeneficiaryConnection() {
        this.navActions.toggleIsPoppedNavState(false);

        this.store.dispatch({ type : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_CANCEL_EDIT_CONNECTION });
    }

    /**
     * dispatch action to close beneficiary edit connection screen
     */
    closeEditBeneficiaryConnection() {
        this.navActions.toggleIsPoppedNavState(false);

        this.store.dispatch({ type : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_CLOSE_EDIT_CONNECTION });
    }

    /**
     * dispatch action with updated input data for beneficiary connection field
     * @param update
     */
    updateConnectionField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_UPDATE_CONNECTION_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action to toggle add phone active state
     */
    toggleAddPhoneActive() {
        this.store.dispatch({ type : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_TOGGLE_ADD_PHONE_ACTIVE });
    }

    /**
     * dispatch action to toggle add connection expanded state
     */
    toggleAddConnectionExpanded() {
        this.store.dispatch({ type : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_TOGGLE_ADD_CONNECTION_EXPANDED });
    }

    /**
     * dispatch action to toggle isConnectionAddedManually flag which indicates
     * if a CSR is entering a new connection by hand
     * @param toggleState
     */
    toggleConnectionAddedManually(toggleState : boolean) {
        this.store.dispatch({
            type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_TOGGLE_CONNECTION_ADDED_MANUALLY,
            payload : toggleState
        });
    }

    /**
     * dispatch action to toggle isConnectionAddedFromSearch flag which indicates
     * if a CSR is entering a new connection by using the search
     * @param toggleState
     */
    toggleConnectionAddedFromSearch(toggleState : boolean) {
        this.store.dispatch({
            type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_TOGGLE_CONNECTION_ADDED_FROM_SEARCH,
            payload : toggleState
        });
    }

    /**
     * dispatch action to update add beneficiary connection from a list of people search results
     * @param connection
     */
    addBeneficiaryConnectionFromPeopleSearch(connection : PeopleSearchResult) {
        // this connection has the uuid of the selected connection so we need to take this
        // uuid and query for the full person/beneficiary record
        this.beneficiaryService.getBeneficiary(connection.get('uuid')).subscribe(response => {
            // now we can add this new connection
            this.store.dispatch({
                type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_ADD_CONNECTION_FROM_PEOPLE_SEARCH,
                payload : response
            });
        }, error => {
            // well that sucked...
            this.navActions.updateAlertMessageState(
                new AlertItem({alertType : EnumAlertType.ERROR, message : error})
            );
        });
    }

    /**
     * dispatch action to map the beneficiary info portion of client view model
     * to the current Beneficiary's Domain Model
     * this ONLY gets used during Edit Beneficiary Profile workflow
     */
    saveBeneficiaryConnections() {
        let isNewProfileActive          : boolean       = false,
            beneficiaryModel            : Beneficiary   = new Beneficiary(),
            modifiedConnection          : BeneficiaryConnectionsStateDetail,
            manuallyAddedConnection     : Beneficiary;

        // grab the most recently added connection
        modifiedConnection = this.beneficiaryConnectionsSelectors.modifiedBeneficiaryConnection();

        // check for duplicate connection being addded
        if (!this.checkForDuplicateConnection(modifiedConnection.get('uuid'))) {
            // was a connection added manually by the CSR?
            const isConnectionAddedManually : boolean = this.beneficiaryConnectionsSelectors.isConnectionAddedManually();

            if (!isConnectionAddedManually) {
                // dispatch action to update the beneficiary model
                this.store.dispatch({ type: BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_SAVE_BENEFICIARY_CONNECTIONS });

                // toggle modal styles
                this.navActions.toggleIsPoppedNavState(false);

                // is Create Profile Active? if so we don't do a save operation to the API here
                this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

                if (!isNewProfileActive) {
                    this.beneficiaryActions.lockBeneficiary(true);

                    // grab Beneficiary Domain Model
                    this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

                    // save the updated Beneficiary Person data
                    this.beneficiaryService.updateBeneficiaryPerson(beneficiaryModel).subscribe(response => {
                        // update the beneficiary domain modal
                        this.updateBeneficiaryFromAPIResponse(response);

                        // if this add a connection emanated from the Reservations workflow we need to dispatch
                        // an action here to ensure this newly added connection is injected into the Reservations State properly
                        this.updateReservationsStateWithAddedConnection(modifiedConnection);
                        this.beneficiaryActions.lockBeneficiary(false);
                    }, error => {
                        console.debug('UPDATE BENEFICIARY FAILED', error);
                        this.beneficiaryActions.lockBeneficiary(false);
                        this.navActions.updateAlertMessageState(
                            new AlertItem({alertType: EnumAlertType.ERROR, message: error})
                        );
                    });
                }
            }
            else {
                // map it to the manuallyAddedConnection value on our connection state
                this.store.dispatch({
                    type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_POPULATE_MANUALLY_ADDED_CONNECTION,
                    payload : modifiedConnection
                });

                // now grab the manuallyAddedConnection value that was just mapped so we can use it to create a new beneficiary
                manuallyAddedConnection = this.beneficiaryConnectionsSelectors.manuallyAddedConnection();

                // create new beneficiary with this data
                this.beneficiaryService.createBeneficiary(manuallyAddedConnection).subscribe(response => {
                    // so now that we have a valid person record for this manually added connection we can
                    // FINALLY update the viewmodel for this new connection with the uuid that was generated
                    // during this successful save operation
                    this.store.dispatch({
                        type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_UPDATE_NEW_CONNECTION_UUID,
                        payload : response.uuid
                    });

                    // dispatch action to update the beneficiary model
                    this.store.dispatch({type: BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_SAVE_BENEFICIARY_CONNECTIONS});

                    // is Create Profile Active? if so we don't do a save operation to the API here
                    this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

                    if (!isNewProfileActive) {
                        // grab Beneficiary Domain Model
                        this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

                        // save the updated Beneficiary Person data
                        this.beneficiaryService.updateBeneficiaryPerson(beneficiaryModel).subscribe(personResponse => {
                            // update the beneficiary domain modal
                            this.updateBeneficiaryFromAPIResponse(personResponse);

                            // set manually added flag to false now that we're done with this entire journey
                            this.toggleConnectionAddedManually(false);

                            // if this add a connection emanated from the Reservations workflow we need to dispatch
                            // an action here to ensure this newly added connection is injected into the Reservations State properly
                            this.updateReservationsStateWithAddedConnection(modifiedConnection);
                        }, error => {
                            this.navActions.updateAlertMessageState(
                                new AlertItem({alertType: EnumAlertType.ERROR, message: error}));
                        });
                    }
                }, error => {
                    this.navActions.updateAlertMessageState(
                        new AlertItem({alertType: EnumAlertType.ERROR, message: error})
                    );
                });
            }
        }
        else {
            // display warning message to user
            this.navActions.updateAlertMessageState(
                new AlertItem({
                    alertType   : EnumAlertType.ERROR,
                    message     : 'DUPLICATE_CONNECTION_ERROR'
                })
            );

            // delete this connection from the view model
            this.store.dispatch({
                type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_REMOVE_BENEFICIARY_CONNECTION,
                payload : this.beneficiaryConnectionsSelectors.mostRecentlyAddedBeneficiaryConnectionIndex()
            });
        }
    }

    /**
     * remove beneficiary connection from Beneficiary state
     * @param index index of removed connection
     */
    removeBeneficiaryConnection(index : number) {
        let isNewProfileActive  : boolean       = false,
            beneficiaryModel    : Beneficiary   = new Beneficiary();

        // dispatch action to update the beneficiary model
        this.store.dispatch({
            type    : BeneficiaryConnectionsActions.BENEFICIARY_CONNECTIONS_REMOVE_BENEFICIARY_CONNECTION,
            payload : index
        });

        // is Create Profile Active? if so we don't do a save operation to the API here
        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        if (!isNewProfileActive) {
            this.beneficiaryActions.lockBeneficiary(true);
            // grab Beneficiary Domain Model
            this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

            // save the updated Beneficiary Person data
            this.beneficiaryService.updateBeneficiaryPerson(beneficiaryModel).subscribe(response => {
                // update the beneficiary domain modal
                this.updateBeneficiaryFromAPIResponse(response);
                this.beneficiaryActions.lockBeneficiary(false);
            }, error => {
                this.beneficiaryActions.lockBeneficiary(false);
                this.navActions.updateAlertMessageState(
                    new AlertItem({ alertType : EnumAlertType.ERROR, message : error.message }));
            });
        }
    }
}
