import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../../app-store';
import {NavActions} from '../../Navigation/nav.actions';
import {MetaDataTypesSelectors} from '../../MetaDataTypes/meta-data-types.selectors';
import {BeneficiaryService} from '../../../areas/Beneficiary/services/Beneficiary/beneficiary.service';
import {BeneficiaryStateSelectors} from '../selectors/beneficiary.selectors';
import {BeneficiaryMedicalConditionsStateSelectors} from '../selectors/beneficiary-medical-conditions.selectors';
import {BeneficiaryMedicalConditionsState} from '../types/beneficiary-medical-conditions-state.model';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {IBeneficiaryFieldUpdate} from '../types/beneficiary-field-update.model';
import {Beneficiary} from '../types/beneficiary.model';
import {AlertItem, EnumAlertType} from '../../Navigation/types/alert-item.model';
import {BeneficiaryActions} from './beneficiary.actions';

@Injectable()

/**
 * Implementation of BeneficiaryMedicalConditionsActions: Redux Action Creator Service that exposes methods to mutate Beneficiary Medical Conditions state
 */
export class BeneficiaryMedicalConditionsActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_BENEFICIARY                    : string = 'BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_BENEFICIARY';
    static BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_DROPDOWN_TYPES                 : string = 'BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_DROPDOWN_TYPES';
    static BENEFICIARY_MEDICAL_CONDITIONS_REMOVE_MEDICAL_CONDITION              : string = 'BENEFICIARY_MEDICAL_CONDITIONS_REMOVE_MEDICAL_CONDITION';
    static BENEFICIARY_MEDICAL_CONDITIONS_SAVE_MEDICAL_CONDITIONS               : string = 'BENEFICIARY_MEDICAL_CONDITIONS_SAVE_MEDICAL_CONDITIONS';
    static BENEFICIARY_MEDICAL_CONDITIONS_CANCEL_ADD_MEDICAL_CONDITION          : string = 'BENEFICIARY_MEDICAL_CONDITIONS_CANCEL_ADD_MEDICAL_CONDITION';
    static BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_MEDICAL_CONDITION_FIELD_VALUE  : string = 'BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_MEDICAL_CONDITION_FIELD_VALUE';
    static BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_ADD_MEDICAL_CONDITION_ACTIVE   : string = 'BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_ADD_MEDICAL_CONDITION_ACTIVE';
    static BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_MEDICAL_CONDITION_TYPE         : string = 'BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_MEDICAL_CONDITION_TYPE';

    /**
     * BeneficiaryMedicalConditionsActions constructor
     * @param store
     * @param navActions
     * @param metaTypesSelectors
     * @param beneficiaryService
     * @param beneficiarySelectors
     * @param beneficiaryMedicalConditionsStateSelectors
     * @param beneficiaryActions
     */
    constructor(
        private store                                       : NgRedux<IAppStore>,
        private navActions                                  : NavActions,
        private metaTypesSelectors                          : MetaDataTypesSelectors,
        private beneficiaryService                          : BeneficiaryService,
        private beneficiarySelectors                        : BeneficiaryStateSelectors,
        private beneficiaryMedicalConditionsStateSelectors  : BeneficiaryMedicalConditionsStateSelectors,
        private beneficiaryActions                          : BeneficiaryActions
    ) {}

    /**
     * dispatch action to update beneficiary model on Redux store with results
     * of a Beneficiary Person PUT operation
     * @param response
     */
    private updateBeneficiaryFromAPIResponse(response : any) {
        this.store.dispatch({
            type    : BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_BENEFICIARY,
            payload : response
        });
    }

    /**
     * update dropdown types used by beneficiary special requirements form
     */
    updateMedicalConditionDropdownTypes() {
        let medicalConditionsState : BeneficiaryMedicalConditionsState = new BeneficiaryMedicalConditionsState();

        // are the necessary dropdown types already populated??
        this.beneficiaryMedicalConditionsStateSelectors.beneficiaryMedicalConditionsState().subscribe(value => medicalConditionsState = value).unsubscribe();

        if (medicalConditionsState.get('medicalConditionTypes').size === 0) {
            let medicalConditionTypes : List<KeyValuePair> = List<KeyValuePair>();

            this.metaTypesSelectors.medicalConditionTypes().subscribe(value => medicalConditionTypes = value).unsubscribe();

            this.store.dispatch({
                type    : BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_DROPDOWN_TYPES,
                payload : medicalConditionTypes
            });
        }
    }

    /**
     * dispatch action with updated input data for beneficiary medical condition field
     * @param update
     */
    updateMedicalConditionField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_MEDICAL_CONDITION_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for beneficiary medical condition type
     * @param newType selected medical condition type
     */
    updateTempMedicalConditionType(newType : KeyValuePair) {
        this.store.dispatch({
            type    : BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_MEDICAL_CONDITION_TYPE,
            payload : newType
        });
    }

    /**
     * dispatch action to update add a new medical condition in the ui
     */
    updateIsAddMedicalCondition(value : boolean) {
        this.store.dispatch({
            type    : BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_UPDATE_ADD_MEDICAL_CONDITION_ACTIVE,
            payload : value
        });
    }

    /**
     * dispatch action to update add a new medical condition in the ui
     */
    cancelNewMedicalCondition() {
        this.store.dispatch({ type : BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_CANCEL_ADD_MEDICAL_CONDITION });
    }

    /**
     * dispatch action to map the beneficiary medical condition portion of client view model
     * to the current Beneficiary's Domain Model
     * this ONLY gets used during Edit Beneficiary Profile workflow
     */
    saveMedicalConditions() {
        let isNewProfileActive  : boolean       = false,
            beneficiaryModel    : Beneficiary   = new Beneficiary();

        // dispatch action to update the beneficiary model
        this.store.dispatch({ type : BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_SAVE_MEDICAL_CONDITIONS });

        // is Create Profile Active? if so we don't do a save operation to the API here
        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        if (!isNewProfileActive) {
            this.beneficiaryActions.lockBeneficiary(true);

            // grab Beneficiary Domain Model
            this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

            // save the updated Beneficiary Person data
            this.beneficiaryService.updateBeneficiaryPerson(beneficiaryModel).subscribe(response => {
                // update beneficiary domain model with updated response data
                this.updateBeneficiaryFromAPIResponse(response);
                this.beneficiaryActions.lockBeneficiary(false);
            }, error => {
                this.beneficiaryActions.lockBeneficiary(false);
                this.navActions.updateAlertMessageState(
                    new AlertItem({ alertType : EnumAlertType.ERROR, message : error }));
            });
        }
    }

    /**
     * remove medical condition from Beneficiary state
     * @param index index of medical condition to remove
     */
    removeMedicalCondition(index : number) {
        let isNewProfileActive  : boolean     = false,
            beneficiaryModel    : Beneficiary = new Beneficiary();

        this.store.dispatch({
            type    : BeneficiaryMedicalConditionsActions.BENEFICIARY_MEDICAL_CONDITIONS_REMOVE_MEDICAL_CONDITION,
            payload : index
        });

        // is Create Profile Active? if so we don't do a save operation to the API here
        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        if (!isNewProfileActive) {
            this.beneficiaryActions.lockBeneficiary(true);
            // grab Beneficiary Domain Model
            this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

            // save the updated Beneficiary Person data
            this.beneficiaryService.updateBeneficiaryPerson(beneficiaryModel).subscribe(response => {
                // update beneficiary domain model with updated response data
                this.updateBeneficiaryFromAPIResponse(response);
                this.beneficiaryActions.lockBeneficiary(false);
            }, error => {
                this.beneficiaryActions.lockBeneficiary(false);
                this.navActions.updateAlertMessageState(
                    new AlertItem({ alertType : EnumAlertType.ERROR, message : error }));
            });
        }
    }
}
