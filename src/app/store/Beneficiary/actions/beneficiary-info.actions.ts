import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../../app-store';
import {NavActions} from '../../Navigation/nav.actions';
import {BeneficiaryService} from '../../../areas/Beneficiary/services/Beneficiary/beneficiary.service';
import {BeneficiaryStateSelectors} from '../selectors/beneficiary.selectors';
import {BeneficiaryInfoStateSelectors} from '../selectors/beneficiary-info.selectors';
import {MetaDataTypesSelectors} from '../../MetaDataTypes/meta-data-types.selectors';
import {BeneficiaryInfoState} from '../types/beneficiary-info-state.model';
import {BeneficiaryInfoDropdownTypes} from '../types/beneficiary-info-dropdown-types.model';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {LanguageType} from '../../MetaDataTypes/types/language-type.model';
import {IBeneficiaryFieldUpdate} from '../types/beneficiary-field-update.model';
import {Beneficiary} from '../types/beneficiary.model';
import {AlertItem, EnumAlertType} from '../../Navigation/types/alert-item.model';
import {BeneficiaryActions} from './beneficiary.actions';

@Injectable()

/**
 * Implementation of BeneficiaryInfoActions: Redux Action Creator Service that exposes methods to mutate Beneficiary Information state
 */
export class BeneficiaryInfoActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static BENEFICIARY_INFO_UPDATE_BENEFICIARY          : string = 'BENEFICIARY_INFO_UPDATE_BENEFICIARY';
    static BENEFICIARY_INFO_UPDATE_INFO_FIELD_VALUE     : string = 'BENEFICIARY_INFO_UPDATE_INFO_FIELD_VALUE';
    static BENEFICIARY_INFO_UPDATE_INFO_EDITING         : string = 'BENEFICIARY_INFO_UPDATE_INFO_EDITING';
    static BENEFICIARY_INFO_UPDATE_INFO_DROPDOWN_TYPES  : string = 'BENEFICIARY_INFO_UPDATE_INFO_DROPDOWN_TYPES';
    static BENEFICIARY_INFO_SAVE_BENEFICIARY_INFO       : string = 'BENEFICIARY_INFO_SAVE_BENEFICIARY_INFO';

    /**
     * BeneficiaryInfoActions constructor
     * @param store
     * @param navActions
     * @param beneficiaryService
     * @param beneficiarySelectors
     * @param beneficiaryInfoSelectors
     * @param metaTypesSelectors
     * @param beneficiaryActions Beneficiary Actions to be able to mark it as waiting for resources.
     */
    constructor(
        private store                       : NgRedux<IAppStore>,
        private navActions                  : NavActions,
        private beneficiaryService          : BeneficiaryService,
        private beneficiarySelectors        : BeneficiaryStateSelectors,
        private beneficiaryInfoSelectors    : BeneficiaryInfoStateSelectors,
        private metaTypesSelectors          : MetaDataTypesSelectors,
        private beneficiaryActions          : BeneficiaryActions
    ) {}

    /**
     * dispatch action to update beneficiary model on Redux store with results
     * of a Beneficiary Person PUT operation
     * @param response
     */
    private updateBeneficiaryFromAPIResponse(response : any) {
        this.store.dispatch({
            type    : BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_BENEFICIARY,
            payload : response
        });
    }

    /**
     * updates necessary dropdown types for beneficiary info form
     */
    updateBeneficiaryInfoDropdownTypes() {
        let infoState : BeneficiaryInfoState = new BeneficiaryInfoState();

        // are the necessary dropdown types already populated??
        this.beneficiaryInfoSelectors.beneficiaryInfoState().subscribe(value => infoState = value).unsubscribe();

        if (infoState.getIn(['dropdownTypes', 'genderTypes']).size === 0) {
            let infoTypes                   : BeneficiaryInfoDropdownTypes  = new BeneficiaryInfoDropdownTypes(),
                genderTypes                 : List<KeyValuePair>            = List<KeyValuePair>(),
                languageTypes               : List<LanguageType>            = List<LanguageType>(),
                salutationTypes             : List<KeyValuePair>            = List<KeyValuePair>(),
                heightInFeetTypes           : List<KeyValuePair>            = List<KeyValuePair>(),
                heightInInchesTypes         : List<KeyValuePair>            = List<KeyValuePair>(),
                physicalCharacteristicTypes : List<KeyValuePair>            = List<KeyValuePair>();

            this.metaTypesSelectors.genderTypes().subscribe(value => genderTypes = value).unsubscribe();
            this.metaTypesSelectors.languages().subscribe(value => languageTypes = value).unsubscribe();
            this.metaTypesSelectors.salutations().subscribe(value => salutationTypes = value).unsubscribe();
            this.metaTypesSelectors.heightInFeetTypes().subscribe(value => heightInFeetTypes = value).unsubscribe();
            this.metaTypesSelectors.heightInInchesTypes().subscribe(value => heightInInchesTypes = value).unsubscribe();
            this.metaTypesSelectors.physicalCharacteristics().subscribe(value => physicalCharacteristicTypes = value).unsubscribe();

            infoTypes = infoTypes.withMutations(record => record
                    .set('genderTypes', genderTypes)
                    .set('languageTypes', languageTypes)
                    .set('salutationTypes', salutationTypes)
                    .set('heightInFeetTypes', heightInFeetTypes)
                    .set('heightInInchesTypes', heightInInchesTypes)
                    .set('physicalCharacteristicTypes', physicalCharacteristicTypes)
            ) as BeneficiaryInfoDropdownTypes;

            this.store.dispatch({
                type    : BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_INFO_DROPDOWN_TYPES,
                payload : infoTypes
            });
        }
    }
    /**
     * dispatch action with updated input data for beneficiary info field
     * @param update
     */
    updateBeneficiaryInfoField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_INFO_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action to update user beneficiary info editing flag on redux store
     * @param isEditing current edited state of beneficiary info form
     */
    updateBeneficiaryInfoEditing(isEditing : boolean) {
        this.navActions.toggleIsPoppedNavState(isEditing);

        this.store.dispatch({
            type    : BeneficiaryInfoActions.BENEFICIARY_INFO_UPDATE_INFO_EDITING,
            payload : isEditing
        });
    }

    /**
     * dispatch action to map the beneficiary info portion of client view model
     * to the current Beneficiary's Domain Model
     * this ONLY gets used during Edit Beneficiary Profile workflow
     */
    saveBeneficiaryInfo() {
        let beneficiaryModel : Beneficiary = new Beneficiary();

        // dispatch action to update the beneficiary model
        this.store.dispatch({ type : BeneficiaryInfoActions.BENEFICIARY_INFO_SAVE_BENEFICIARY_INFO });
        this.beneficiaryActions.lockBeneficiary(true);

        // toggle modal styles
        this.navActions.toggleIsPoppedNavState(false);

        // grab Beneficiary Domain Model
        this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

        // save the updated Beneficiary Info data
        this.beneficiaryService.updateBeneficiaryPerson(beneficiaryModel).subscribe(response => {
            // update beneficiary domain model with updated response data
            this.updateBeneficiaryFromAPIResponse(response);
            this.beneficiaryActions.lockBeneficiary(false);
        }, error => {
            this.beneficiaryActions.lockBeneficiary(false);
            this.navActions.updateAlertMessageState(
                    new AlertItem({ alertType : EnumAlertType.ERROR, message : error }));
        });
    }
}
