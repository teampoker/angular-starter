import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';
import * as moment from 'moment';

import {IAppStore} from '../../app-store';
import {BeneficiaryService} from '../../../areas/Beneficiary/services/Beneficiary/beneficiary.service';
import {MetaDataTypesSelectors} from '../../MetaDataTypes/meta-data-types.selectors';
import {BeneficiaryStateSelectors} from '../selectors/beneficiary.selectors';
import {BeneficiarySpecialRequirementsSelectors} from '../selectors/beneficiary-special-requirements.selectors';
import {BeneficiarySpecialRequirementsState} from '../types/beneficiary-special-requirements-state.model';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {IBeneficiaryFieldUpdate} from '../types/beneficiary-field-update.model';
import {Beneficiary} from '../types/beneficiary.model';
import {BeneficiarySpecialRequirement} from '../types/beneficiary-special-requirement.model';
import {AlertItem, EnumAlertType} from '../../Navigation/types/alert-item.model';
import {NavActions} from '../../Navigation/nav.actions';
import {BeneficiaryActions} from './beneficiary.actions';

@Injectable()

/**
 * Implementation of BeneficiarySpecialRequirementsActions: Redux Action Creator Service that exposes methods to mutate Beneficiary Special Requirements state
 */
export class BeneficiarySpecialRequirementsActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static BENEFICIARY_SR_UPDATE_BENEFICIARY            : string = 'BENEFICIARY_SR_UPDATE_BENEFICIARY';
    static BENEFICIARY_SR_UPDATE_SR_DROPDOWN_TYPES      : string = 'BENEFICIARY_SR_UPDATE_SR_DROPDOWN_TYPES';
    static BENEFICIARY_SR_REMOVE_SR                     : string = 'BENEFICIARY_SR_REMOVE_SR';
    static BENEFICIARY_SR_SAVE_SR                       : string = 'BENEFICIARY_SR_SAVE_SR';
    static BENEFICIARY_SR_CANCEL_ADD_SR                 : string = 'BENEFICIARY_SR_CANCEL_ADD_SR';
    static BENEFICIARY_SR_UPDATE_SR_FIELD_VALUE         : string = 'BENEFICIARY_SR_UPDATE_SR_FIELD_VALUE';
    static BENEFICIARY_SR_UPDATE_ADD_SR_ACTIVE          : string = 'BENEFICIARY_SR_UPDATE_ADD_SR_ACTIVE';
    static BENEFICIARY_SR_UPDATE_SR_TYPE                : string = 'BENEFICIARY_SR_UPDATE_SR_TYPE';
    static BENEFICIARY_SR_SET_SR                        : string = 'BENEFICIARY_SR_SET_SR';

    /**
     * BeneficiarySpecialRequirementsActions constructor
     * @param store
     * @param navActions
     * @param beneficiaryService
     * @param metaTypesSelectors
     * @param beneficiarySelectors
     * @param beneficiarySpecialRequirementsSelectors
     * @param beneficiaryActions
     */
    constructor(
        private store                                   : NgRedux<IAppStore>,
        private navActions                              : NavActions,
        private beneficiaryService                      : BeneficiaryService,
        private metaTypesSelectors                      : MetaDataTypesSelectors,
        private beneficiarySelectors                    : BeneficiaryStateSelectors,
        private beneficiarySpecialRequirementsSelectors : BeneficiarySpecialRequirementsSelectors,
        private beneficiaryActions                      : BeneficiaryActions
    ) {}

    /**
     * dispatch action to update beneficiary model on Redux store with results
     * of a Beneficiary Person PUT operation
     * @param response
     */
    private updateBeneficiaryFromAPIResponse(response : any) {
        this.store.dispatch({
            type    : BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_BENEFICIARY,
            payload : response
        });
    }

    /**
     * inspect the eligibilty dates for a given special requirement and make
     * sure the start date is not after the end date and vice versa
     *
     * @param newSpecialRequirement newly added special requirement
     *
     * @returns {boolean}
     */
    private checkSpecialRequirementDatesOrder(newSpecialRequirement : BeneficiarySpecialRequirement) : boolean {
        // verify start date is before end date
        if (moment(newSpecialRequirement.get('fromDate')).isAfter(moment(newSpecialRequirement.get('thruDate')))) {
            return false;
        }
        // verify end date is after start date
        else if (moment(newSpecialRequirement.get('thruDate')).isBefore(moment(newSpecialRequirement.get('fromDate')))) {
            return false;
        }
        // verify start and end date aren't the same
        else if (moment(newSpecialRequirement.get('thruDate')).isSame(moment(newSpecialRequirement.get('fromDate')))) {
            return false;
        }

        return true;
    }

    /**
     * determine if a special requirement can be added based on previous additions
     *
     * @param newSpecialRequirement newly added special requirement
     *
     * @returns {boolean}
     */
    private checkSpecialRequirementCompatibility(newSpecialRequirement : BeneficiarySpecialRequirement) : boolean {
        let isCompatible : boolean = true,
            specialRequirements     : List<BeneficiarySpecialRequirement> = List<BeneficiarySpecialRequirement>(),
            specialRequirementsVM   : BeneficiarySpecialRequirementsState = new BeneficiarySpecialRequirementsState();

        this.beneficiarySpecialRequirementsSelectors.beneficiarySpecialRequirements().subscribe(value => specialRequirements = value).unsubscribe();
        this.beneficiarySpecialRequirementsSelectors.beneficiarySpecialRequirementsState().first().subscribe(value => specialRequirementsVM = value.get('requirementsState'));

        switch (newSpecialRequirement.getIn(['type', 'value'])) {
            case 'Door to Door' :
                // No Door through Door, Hand to Hand, Stretcher
                specialRequirementsVM.forEach(req => {
                    switch (req.getIn(['type', 'value'])) {
                        case 'Door through Door' :
                        case 'Hand to Hand' :
                        case 'Stretcher' :
                            isCompatible = false;
                            break;
                        default:
                            break;
                    }
                });
                break;

            case 'Door through Door' :
                // No Door to Door, Hand to Hand
                specialRequirementsVM.forEach(req => {
                    switch (req.getIn(['type', 'value'])) {
                        case 'Door to Door' :
                        case 'Hand to Hand' :
                            isCompatible = false;
                            break;
                        default:
                            break;
                    }
                });
                break;

            case 'Hand to Hand' :
                // No Door to Door, Door through Door, Wheelchair, Wheelchair Lift
                specialRequirementsVM.forEach(req => {
                    switch (req.getIn(['type', 'value'])) {
                        case 'Door to Door' :
                        case 'Door through Door' :
                        case 'Wheelchair' :
                        case 'Wheelchair Lift' :
                            isCompatible = false;
                            break;
                        default:
                            break;
                    }
                });
                break;

            case 'Stretcher' :
                // No Ambulatory, Door to Door, Hand to Hand, Wheelchair, Wheelchair Lift
                specialRequirementsVM.forEach(req => {
                    switch (req.getIn(['type', 'value'])) {
                        case 'Ambulatory' :
                        case 'Door to Door' :
                        case 'Hand to Hand' :
                        case 'Wheelchair' :
                        case 'Wheelchair Lift' :
                            isCompatible = false;
                            break;
                        default:
                            break;
                    }
                });
                break;

            case 'Wheelchair' :
                // No Ambulatory, Hand to Hand, Stretcher
                specialRequirementsVM.forEach(req => {
                    switch (req.getIn(['type', 'value'])) {
                        case 'Ambulatory' :
                        case 'Hand to Hand' :
                        case 'Stretcher' :
                            isCompatible = false;
                            break;
                        default:
                            break;
                    }
                });
                break;

            case 'Wheelchair Lift' :
                // No Ambulatory, Hand to Hand, Stretcher
                specialRequirementsVM.forEach(req => {
                    switch (req.getIn(['type', 'value'])) {
                        case 'Ambulatory' :
                        case 'Hand to Hand' :
                        case 'Stretcher' :
                            isCompatible = false;
                            break;
                        default:
                            break;
                    }
                });
                break;

            default:
                break;
        }

        return isCompatible;
    }

    /**
     * determine if a special requirement already exists locally
     * @returns {boolean}
     */
    private checkForDuplicateSpecialRequirement(newSpecialRequirement : BeneficiarySpecialRequirement) : boolean {
        let isDuplicate             : boolean = false,
            specialRequirements     : List<BeneficiarySpecialRequirement> = List<BeneficiarySpecialRequirement>(),
            specialRequirementsVM   : BeneficiarySpecialRequirementsState = new BeneficiarySpecialRequirementsState();

        this.beneficiarySpecialRequirementsSelectors.beneficiarySpecialRequirements().subscribe(value => specialRequirements = value).unsubscribe();
        this.beneficiarySpecialRequirementsSelectors.beneficiarySpecialRequirementsState().subscribe(value => specialRequirementsVM = value.get('requirementsState')).unsubscribe();

        // are they adding the very first special requirement and we can ignore this?
        if (specialRequirementsVM.size > 0) {
            const matchedRequirement : BeneficiarySpecialRequirement = specialRequirementsVM.find(requirement => requirement.getIn(['type', 'id']) === newSpecialRequirement.getIn(['type', 'id']));

            // look for match with previously added special requirement
            if (matchedRequirement) {
                // now check to see if it is permanent or not??
                if (newSpecialRequirement.get('permanentRequirement') || matchedRequirement.get('permanentRequirement')) {
                    // permanent will overlap with any previous requirement's date range
                    isDuplicate = true;
                }
                else {
                    // verify the date range selected for the new requirement DO NOT overlap
                    // the previous existing special requirement's data range
                    if (
                        // end of new range is same as start of matched range
                        moment(newSpecialRequirement.get('thruDate')).isSame(moment(matchedRequirement.get('fromDate'))) ||

                        // end of new range is after start of matched range
                        moment(newSpecialRequirement.get('thruDate')).isAfter(moment(matchedRequirement.get('fromDate')))  &&

                        // start of new range is before start of matched range
                        moment(newSpecialRequirement.get('fromDate')).isBefore(moment(matchedRequirement.get('fromDate')))
                    ) {
                        // cannot add a special requirement with an overlapping date range
                        isDuplicate = true;
                    }
                    else if (
                        // start of new range is same as end of matched range
                        moment(newSpecialRequirement.get('fromDate')).isSame(moment(matchedRequirement.get('thruDate')))  ||

                        // start of new range is before end of matched range
                        moment(newSpecialRequirement.get('fromDate')).isBefore(moment(matchedRequirement.get('thruDate'))) &&

                        // end of new range is after end of matched range
                        moment(newSpecialRequirement.get('thruDate')).isAfter(moment(matchedRequirement.get('thruDate')))
                    ) {
                        // cannot add a special requirement with an overlapping date range
                        isDuplicate = true;
                    }
                    else if (
                        // start of new range is same as end of matched range
                        moment(newSpecialRequirement.get('fromDate')).isSame(moment(matchedRequirement.get('fromDate'))) &&

                        // end of new range is same as end of matched range
                        moment(newSpecialRequirement.get('thruDate')).isSame(moment(matchedRequirement.get('thruDate'))) ||

                        // start of new range is same as start of matched range
                        moment(newSpecialRequirement.get('fromDate')).isSame(moment(matchedRequirement.get('fromDate'))) ||

                        // end of new range is same as end of matched range
                        moment(newSpecialRequirement.get('thruDate')).isSame(moment(matchedRequirement.get('thruDate')))
                    ) {
                        // cannot add a special requirement with an overlapping date range
                        isDuplicate = true;
                    }
                }
            }
        }

        return isDuplicate;
    }

    /**
     * update dropdown types used by beneficiary special requirements form
     */
    updateSpecialRequirementDropdownTypes() {
        let state : BeneficiarySpecialRequirementsState = new BeneficiarySpecialRequirementsState();

        // are the necessary dropdown types already populated??
        this.beneficiarySpecialRequirementsSelectors.beneficiarySpecialRequirementsState().subscribe(value => state = value).unsubscribe();

        if (state.get('specialRequirementTypes').size === 0) {
            let specialRequirementTypes : List<KeyValuePair> = List<KeyValuePair>();

            this.metaTypesSelectors.specialRequirementTypes().subscribe(value => specialRequirementTypes = value).unsubscribe();

            this.store.dispatch({
                type    : BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_SR_DROPDOWN_TYPES,
                payload : specialRequirementTypes
            });
        }
    }

    /**
     * dispatch action with updated input data for beneficiary special requirement field
     * @param update
     */
    updateSpecialRequirementField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_SR_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for beneficiary special requirement Type
     * @param update
     */
    updateSpecialRequirementType(update : KeyValuePair) {
        this.store.dispatch({
            type    : BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_SR_TYPE,
            payload : update
        });
    }

    /**
     * dispatch action to update add a new special requirement in the ui
     */
    updateIsAddSpecialRequirement(value : boolean) {
        this.store.dispatch({
            type    : BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_UPDATE_ADD_SR_ACTIVE,
            payload : value
        });
    }

    /**
     * dispatch action to update add a new special requirement in the ui
     */
    cancelNewSpecialRequirement() {
        this.store.dispatch({ type : BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_CANCEL_ADD_SR });
    }

    /**
     * dispatch action to map the beneficiary special requirements portion of client view model
     * to the current Beneficiary's Domain Model
     * this ONLY gets used during Edit Beneficiary Profile workflow
     */
    saveSpecialRequirement() {
        let isNewProfileActive  : boolean       = false,
            beneficiaryModel    : Beneficiary   = new Beneficiary(),
            mostRecentlyAddedSR : BeneficiarySpecialRequirement;

        // grab the most recently added connection
        mostRecentlyAddedSR = this.beneficiarySpecialRequirementsSelectors.mostRecentlyAddedBeneficiarySpecialRequirement();

        // verify start date is not after end date and vice versa
        if (this.checkSpecialRequirementDatesOrder(mostRecentlyAddedSR)) {
            // check for duplicate connection being addded
            if (!this.checkForDuplicateSpecialRequirement(mostRecentlyAddedSR)) {
                // check if requirement can be added based on previous requirements
                if (this.checkSpecialRequirementCompatibility(mostRecentlyAddedSR)) {
                    this.beneficiaryActions.lockBeneficiary(true);
                    // dispatch action to update the beneficiary model
                    this.store.dispatch({type: BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_SAVE_SR});

                    // is Create Profile Active? if so we don't do a save operation to the API here
                    this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

                    if (!isNewProfileActive) {
                        // grab Beneficiary Domain Model
                        this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

                        // save the updated Beneficiary Person data
                        this.beneficiaryService.updateBeneficiaryPerson(beneficiaryModel).subscribe(response => {
                            // update beneficiary domain model with updated response data
                            this.updateBeneficiaryFromAPIResponse(response);
                            this.beneficiaryActions.lockBeneficiary(false);
                        }, error => {
                            this.beneficiaryActions.lockBeneficiary(false);
                            // display warning message to user
                            this.navActions.updateAlertMessageState(
                                new AlertItem({
                                    alertType   : EnumAlertType.ERROR,
                                    message     : 'error'
                                })
                            );
                        });
                    }
                    else {
                        // unlock the beneficiary since we're in a 'create new' scenario
                        this.beneficiaryActions.lockBeneficiary(false);
                    }
                }
                else {
                    // display warning message to user
                    this.navActions.updateAlertMessageState(
                        new AlertItem({
                            alertType   : EnumAlertType.ERROR,
                            message     : 'INCOMPATIBLE_SR_ERROR'
                        })
                    );
                }

            }
            else {
                // display warning message to user
                this.navActions.updateAlertMessageState(
                    new AlertItem({
                        alertType   : EnumAlertType.ERROR,
                        message     : 'DUPLICATE_SR_ERROR'
                    })
                );
            }
        }
        else {
            // display warning message to user
            this.navActions.updateAlertMessageState(
                new AlertItem({
                    alertType   : EnumAlertType.ERROR,
                    message     : 'SR_DATE_ORDER_ERROR'
                })
            );
        }
    }

    /**
     * remove special requirement from Beneficiary state
     * @param index index of special requirement to remove
     */
    removeSpecialRequirement(index : number) {
        let isNewProfileActive  : boolean     = false,
            beneficiaryModel    : Beneficiary = new Beneficiary();

        this.store.dispatch({
            type    : BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_REMOVE_SR,
            payload : index
        });

        // is Create Profile Active? if so we don't do a save operation to the API here
        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        if (!isNewProfileActive) {
            this.beneficiaryActions.lockBeneficiary(true);
            // grab Beneficiary Domain Model
            this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

            // save the updated Beneficiary Person data
            this.beneficiaryService.updateBeneficiaryPerson(beneficiaryModel).subscribe(response => {
                // update beneficiary domain model with updated response data
                this.updateBeneficiaryFromAPIResponse(response);
                this.beneficiaryActions.lockBeneficiary(false);
            }, error => {
                this.navActions.updateAlertMessageState(
                    new AlertItem({ alertType : EnumAlertType.ERROR, message : error }));
            });
        }
    }

    setSpecialRequirements(payload : BeneficiarySpecialRequirementsState) {
        this.store.dispatch({
            type : BeneficiarySpecialRequirementsActions.BENEFICIARY_SR_SET_SR,
            payload
        });
    }
}
