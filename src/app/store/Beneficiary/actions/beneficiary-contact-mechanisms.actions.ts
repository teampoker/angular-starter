import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../../app-store';
import {NavActions} from '../../Navigation/nav.actions';
import {MetaDataTypesSelectors} from '../../MetaDataTypes/meta-data-types.selectors';
import {BeneficiaryService} from '../../../areas/Beneficiary/services/Beneficiary/beneficiary.service';
import {BeneficiaryStateSelectors} from '../selectors/beneficiary.selectors';
import {BeneficiaryContactMechanismsStateSelectors} from '../selectors/beneficiary-contact-mechanisms.selectors';
import {BeneficiaryContactMechanismsState} from '../types/beneficiary-contact-mechanisms-state.model';
import {BeneficiaryContactMechanismsDropdownTypes} from '../types/beneficiary-contact-mechanisms-dropdown-types.model';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {ContactMechanismType} from '../../MetaDataTypes/types/contact-mechanism-type.model';
import {IBeneficiaryFieldUpdate} from '../types/beneficiary-field-update.model';
import {Beneficiary} from '../types/beneficiary.model';
import {AlertItem, EnumAlertType} from '../../Navigation/types/alert-item.model';

@Injectable()

/**
 * Implementation of BeneficiaryContactMechanismActions: Redux Action Creator Service that exposes methods to mutate Beneficiary Contact Mechanisms state
 */
export class BeneficiaryContactMechanismActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_CONTACT_INFO_DROPDOWN_TYPES           : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_CONTACT_INFO_DROPDOWN_TYPES';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_CONTACT_INFO_EDITING                  : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_CONTACT_INFO_EDITING';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_FIELD_VALUE                   : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_FIELD_VALUE';
    static BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_ADDRESS                       : string = 'BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_ADDRESS';
    static BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_ADDRESS                    : string = 'BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_ADDRESS';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_FIELD_VALUE                     : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_FIELD_VALUE';
    static BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_EMAIL                         : string = 'BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_EMAIL';
    static BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_EMAIL                      : string = 'BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_EMAIL';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_FIELD_VALUE                     : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_FIELD_VALUE';
    static BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_PHONE                         : string = 'BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_PHONE';
    static BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_PHONE                      : string = 'BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_PHONE';
    static BENEFICIARY_CONTACT_MECHANISMS_SAVE_CONTACT_MECHANISMS                      : string = 'BENEFICIARY_CONTACT_MECHANISMS_SAVE_CONTACT_MECHANISMS';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_TYPE                          : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_TYPE';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_NOTIFICATION_TYPE             : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_NOTIFICATION_TYPE';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_TYPE                            : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_TYPE';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_NOTIFICATION_TYPE               : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_NOTIFICATION_TYPE';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_TYPE                            : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_TYPE';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_NOTIFICATION_TYPE               : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_NOTIFICATION_TYPE';
    static BENEFICIARY_CONTACT_MECHANISMS_UPDATE_BENEFICIARY                           : string = 'BENEFICIARY_CONTACT_MECHANISMS_UPDATE_BENEFICIARY';

    /**
     * BeneficiaryContactMechanismActions constructor
     * @param store
     * @param navActions
     * @param metaTypesSelectors
     * @param beneficiaryService
     * @param beneficiarySelectors
     * @param beneficiaryContactInfoSelectors
     */
    constructor(
        private store                           : NgRedux<IAppStore>,
        private navActions                      : NavActions,
        private metaTypesSelectors              : MetaDataTypesSelectors,
        private beneficiaryService              : BeneficiaryService,
        private beneficiarySelectors            : BeneficiaryStateSelectors,
        private beneficiaryContactInfoSelectors : BeneficiaryContactMechanismsStateSelectors
    ) {}

    /**
     * dispatch action to update beneficiary model on Redux store with results
     * of a Beneficiary Person PUT operation
     * @param response
     */
    private updateBeneficiaryFromAPIResponse(response : any) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_BENEFICIARY,
            payload : response
        });
    }

    /**
     * updates necessary dropdown types for beneficiary contact info form
     */
    updateContactInfoDropdownTypes() {
        let contactMechanismsState : BeneficiaryContactMechanismsState = new BeneficiaryContactMechanismsState();

        // are the necessary dropdown types already populated??
        this.beneficiaryContactInfoSelectors.beneficiaryContactMechanismsState().subscribe(value => contactMechanismsState = value).unsubscribe();

        if (contactMechanismsState.getIn(['dropdownTypes', 'addressTypes'], []).size === 0) {
            let contactTypes                : BeneficiaryContactMechanismsDropdownTypes   = new BeneficiaryContactMechanismsDropdownTypes(),
                solicitationIndicatorTypes  : List<KeyValuePair>                    = List<KeyValuePair>(),
                addressTypes                : List<KeyValuePair>                    = List<KeyValuePair>(),
                emailTypes                  : List<KeyValuePair>                    = List<KeyValuePair>(),
                phoneTypes                  : List<KeyValuePair>                    = List<KeyValuePair>(),
                contactMechanismTypes       : List<ContactMechanismType>            = List<ContactMechanismType>();

            this.metaTypesSelectors.solicitationIndicatorTypes().subscribe(value => solicitationIndicatorTypes = value).unsubscribe();
            this.metaTypesSelectors.addressTypes().subscribe(value => addressTypes = value).unsubscribe();
            this.metaTypesSelectors.emailTypes().subscribe(value => emailTypes = value).unsubscribe();
            this.metaTypesSelectors.phoneTypes().subscribe(value => phoneTypes = value).unsubscribe();
            this.metaTypesSelectors.contactMechanismTypes().subscribe(value => contactMechanismTypes = value).unsubscribe();

            contactTypes = contactTypes.withMutations(record => record
                    .set('solicitationIndicatorTypes', solicitationIndicatorTypes)
                    .set('addressTypes', addressTypes)
                    .set('emailTypes', emailTypes)
                    .set('phoneTypes', phoneTypes)
                    .set('contactMechanismTypes', contactMechanismTypes)
            ) as BeneficiaryContactMechanismsDropdownTypes;

            this.store.dispatch({
                type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_CONTACT_INFO_DROPDOWN_TYPES,
                payload : contactTypes
            });
        }
    }

    /**
     * dispatch action to update user beneficiary contact info editing flag on redux store
     * @param isEditing current edited state of beneficiary contact info form
     */
    updateContactInfoEditing(isEditing : boolean) {
        this.navActions.toggleIsPoppedNavState(isEditing);

        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_CONTACT_INFO_EDITING,
            payload : isEditing
        });
    }

    /**
     * dispatch action with updated input data for beneficiary address field
     * @param update
     */
    updateAddressField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for beneficiary address type selection
     * @param update
     */
    updateAddressType(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_TYPE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for beneficiary address notification type selection
     * @param update
     */
    updateAddressNotificationType(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_ADDRESS_NOTIFICATION_TYPE,
            payload : update
        });
    }

    /**
     * dispatch action to add additional beneficiary address
     */
    addAdditionalBeneficiaryAddress() {
        this.store.dispatch({ type : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_ADDRESS });
    }

    /**
     * dispatch action to remove additional beneficiary address
     * @param removeIndex
     */
    removeAdditionalBeneficiaryAddress(removeIndex : number) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_ADDRESS,
            payload : removeIndex
        });
    }

    /**
     * dispatch action with updated input data for beneficiary email field
     * @param update
     */
    updateEmailField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for beneficiary email type selection
     * @param update
     */
    updateEmailType(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_TYPE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for beneficiary email notification type selection
     * @param update
     */
    updateEmailNotificationType(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_EMAIL_NOTIFICATION_TYPE,
            payload : update
        });
    }

    /**
     * dispatch action to add additional beneficiary email
     */
    addAdditionalBeneficiaryEmail() {
        this.store.dispatch({ type : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_EMAIL });
    }

    /**
     * dispatch action to remove additional beneficiary email
     * @param removeIndex
     */
    removeAdditionalBeneficiaryEmail(removeIndex : number) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_EMAIL,
            payload : removeIndex
        });
    }

    /**
     * dispatch action with updated input data for beneficiary phone field
     * @param update
     */
    updatePhoneField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for beneficiary phone type selection
     * @param update
     */
    updatePhoneType(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_TYPE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for beneficiary phone notification type selection
     * @param update
     */
    updatePhoneNotificationType(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_UPDATE_PHONE_NOTIFICATION_TYPE,
            payload : update
        });
    }

    /**
     * dispatch action to add additional beneficiary phone
     */
    addAdditionalBeneficiaryPhone() {
        this.store.dispatch({ type : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_ADD_ADDITIONAL_PHONE });
    }

    /**
     * dispatch action to remove additional beneficiary phone
     * @param removeIndex
     */
    removeAdditionalBeneficiaryPhone(removeIndex : number) {
        this.store.dispatch({
            type    : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_REMOVE_ADDITIONAL_PHONE,
            payload : removeIndex
        });
    }

    /**
     * dispatch action to save beneficiary contact mechanisms data to beneficiary domain model
     * and then to the API
     * this ONLY gets used during Edit Beneficiary Profile workflow
     */
    saveBeneficiaryContactMechanisms() {
        let isNewProfileActive  : boolean       = false,
            beneficiaryModel    : Beneficiary   = new Beneficiary();

        // dispatch action to update the beneficiary model
        this.store.dispatch({ type : BeneficiaryContactMechanismActions.BENEFICIARY_CONTACT_MECHANISMS_SAVE_CONTACT_MECHANISMS });

        // toggle modal styles
        this.navActions.toggleIsPoppedNavState(false);

        // is Create Profile Active? if so we don't do a save operation to the API here
        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        if (!isNewProfileActive) {
            // grab Beneficiary Domain Model
            this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

            // save the updated Beneficiary Person data
            this.beneficiaryService.updateBeneficiaryContactMechanisms(beneficiaryModel).subscribe(response => {
                console.debug('UPDATE BENEFICIARY CONTACT MECHANISMS SUCCESSFUL');
                // update beneficiary domain model with updated response data
                this.updateBeneficiaryFromAPIResponse(response);
            }, error => {
                console.debug('UPDATE BENEFICIARY CONTACT MECHANISMS FAILED', error);

                this.navActions.updateAlertMessageState(
                    new AlertItem({alertType : EnumAlertType.ERROR, message : error }));
            });
        }
    }
}
