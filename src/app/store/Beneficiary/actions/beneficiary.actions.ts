import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../../app-store';
import {NavActions} from '../../Navigation/nav.actions';
import {BeneficiaryService} from '../../../areas/Beneficiary/services/Beneficiary/beneficiary.service';
import {BeneficiaryStateSelectors} from '../selectors/beneficiary.selectors';
import {MetaDataTypesSelectors} from '../../MetaDataTypes/meta-data-types.selectors';
import {ServiceCoverageOrganizationType} from '../../MetaDataTypes/types/service-coverage-organization-type.model';
import {KeyValuePair} from '../../types/key-value-pair.model';
import {BeneficiaryPayload} from '../types/beneficiary-payload.model';
import {Beneficiary} from '../types/beneficiary.model';
import {EnumAlertType, AlertItem} from '../../Navigation/types/alert-item.model';
import {FormsActions} from '../../Forms/forms.actions';

@Injectable()

/**
 * Implementation of BeneficiaryActions: Redux Action Creator Service that exposes methods to mutate Beneficiary state
 */
export class BeneficiaryActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static BENEFICIARY_UPDATE_BENEFICIARY                           : string = 'BENEFICIARY_UPDATE_BENEFICIARY';
    static BENEFICIARY_CREATE_NEW_BENEFICIARY                       : string = 'BENEFICIARY_CREATE_NEW_BENEFICIARY';
    static BENEFICIARY_INIT_BENEFICIARY_DASHBOARD                   : string = 'BENEFICIARY_INIT_BENEFICIARY_DASHBOARD';
    static BENEFICIARY_INIT_VIEW_BENEFICIARY_PROFILE                : string = 'BENEFICIARY_INIT_VIEW_BENEFICIARY_PROFILE';
    static BENEFICIARY_INIT_CREATE_BENEFICIARY_PROFILE              : string = 'BENEFICIARY_INIT_CREATE_BENEFICIARY_PROFILE';
    static BENEFICIARY_UPDATE_NEW_PROFILE_ACTIVE                    : string = 'BENEFICIARY_UPDATE_NEW_PROFILE_ACTIVE';
    static BENEFICIARY_UPDATE_CREATE_PROFILE_SAVE_ACTIVE            : string = 'BENEFICIARY_UPDATE_CREATE_PROFILE_SAVE_ACTIVE';
    static BENEFICIARY_WAIT                                         : string = 'BENEFICIARY_WAIT';

    /**
     * BeneficiaryActions constructor
     * @param store
     * @param navActions
     * @param beneficiaryService
     * @param beneficiarySelectors
     * @param formsActions
     * @param metaTypesSelectors
     * @param router
     */
    constructor(
        private store                   : NgRedux<IAppStore>,
        private navActions              : NavActions,
        private beneficiaryService      : BeneficiaryService,
        private beneficiarySelectors    : BeneficiaryStateSelectors,
        private formsActions            : FormsActions,
        private metaTypesSelectors      : MetaDataTypesSelectors,
        private router                  : Router
    ) {}

    /**
     * service coverages metadata
     */
    private serviceCoverages : List<ServiceCoverageOrganizationType>;

    /**
     * service coverage classification types
     */
    private classifications : List<KeyValuePair>;

    /**
     * requests the entire beneficiary domain model for the given uuid
     * @param uuid uuid of selected beneficiary
     * @param redirect flag indicates if we redirect user to the Beneficiary Dashboard after HTTP operation completes successfully
     */
    getBeneficiary(uuid : string, redirect : boolean) {
        this.metaTypesSelectors.isLoaded().subscribe(() => {
            // make sure newProfileActive flag is false
            this.updateNewProfileActive(false);

            // lock it
            this.lockBeneficiary(true);

            // grab the serviceCoverageOrganizations metadata
            this.metaTypesSelectors.serviceCoverageOrganizations().subscribe(value => this.serviceCoverages = value).unsubscribe();
            this.metaTypesSelectors.serviceCoveragePlanClassificationTypes().subscribe(value => this.classifications = value).unsubscribe();

            // query for beneficiary's forms
            this.formsActions.getForms(uuid);

            // query for beneficiary's information
            this.beneficiaryService.getBeneficiary(uuid).subscribe(response => {
                // update beneficiary
                this.updateBeneficiaryDomainModel(new BeneficiaryPayload().withMutations(record => {
                    return record
                        .set('beneficiary', response)
                        .set('serviceCoverages', this.serviceCoverages)
                        .set('classificationTypes', this.classifications);
                }) as BeneficiaryPayload);

                // unlock
                this.lockBeneficiary(false);

                // navigate user to beneficiary view?
                if (redirect) {
                    this.router.navigate(['/Beneficiary']);
                }
            }, error => {
                // clear beneficiary results
                this.updateBeneficiaryDomainModel(new BeneficiaryPayload().withMutations(record => {
                    return record
                        .set('beneficiary', new Beneficiary()) // need to clear out the current beneficiary record
                        .set('serviceCoverages', this.serviceCoverages)
                        .set('classificationTypes', this.classifications);
                }) as BeneficiaryPayload);

                // unlock
                this.lockBeneficiary(false);
            });
        });
    }

    /**
     * update the beneficiary domain model mapped to BeneficiaryState
     * with new beneficiary's data
     *
     * @param beneficiaryPayload combination of raw Beneficiary GET response and metaDataTypes.serviceCoverageOrganizations data
     */
    updateBeneficiaryDomainModel(beneficiaryPayload : BeneficiaryPayload) {
        this.store.dispatch({
            type    : BeneficiaryActions.BENEFICIARY_UPDATE_BENEFICIARY,
            payload : beneficiaryPayload
        });
        this.lockBeneficiary(false);
    }

    /**
     * Sync Action that initializes View Profile state
     */
    initBeneficiaryDashboard() {
        this.store.dispatch({ type : BeneficiaryActions.BENEFICIARY_INIT_BENEFICIARY_DASHBOARD });
    }

    /**
     * Sync Action that initializes View Profile state
     */
    initViewBeneficiaryProfile() {
        this.store.dispatch({ type : BeneficiaryActions.BENEFICIARY_INIT_VIEW_BENEFICIARY_PROFILE });
    }

    /**
     * Sync Action that initializes Create Profile state
     */
    initNewBeneficiaryProfile() {
        this.store.dispatch({ type : BeneficiaryActions.BENEFICIARY_INIT_CREATE_BENEFICIARY_PROFILE });
        this.lockBeneficiary(false);
    }

    /**
     * Sync action that sets the BeneficiaryState.isNewProfileActive flag
     * @param active
     */
    updateNewProfileActive(active : boolean) {
        this.store.dispatch({
            type    : BeneficiaryActions.BENEFICIARY_UPDATE_NEW_PROFILE_ACTIVE,
            payload : active
        });
    }

    /**
     * Sync action that sets the BeneficiaryState.isCreateProfileSaveActive flag
     * @param active
     */
    updateCreateProfileSaveActive(active : boolean) {
        this.store.dispatch({
            type    : BeneficiaryActions.BENEFICIARY_UPDATE_CREATE_PROFILE_SAVE_ACTIVE,
            payload : active
        });
    }

    /**
     * Async Action that saves beneficiary to API and then disables edit
     * status of Beneficiary Profile form
     */
    createNewBeneficiary() {
        // is a save already active?
        let isCreateProfileSaveActive : boolean = false;

        // toggle modal style
        this.navActions.toggleIsPoppedNavState(false);

        // grab Beneficiary Domain Model
        this.beneficiarySelectors.createProfileSaveActive().subscribe(value => isCreateProfileSaveActive = value).unsubscribe();
        // this.beneficiarySelectors.isWaiting.subscribe(value => isCreateProfileSaveActive = value).unsubscribe();

        if (!isCreateProfileSaveActive) {
            let beneficiaryModel : Beneficiary = new Beneficiary();

            // indicate save is active
            this.updateCreateProfileSaveActive(true);

            // indicate we're mapping data from viewmodel to domain model
            // this.updateCreateProfileMappingFinished(false);

            // trigger the update of the beneficiary domain model; also saves a backup of the beneficiary state
            this.store.dispatch({type: BeneficiaryActions.BENEFICIARY_CREATE_NEW_BENEFICIARY});

            // verify data mapping is completed before grabbing updated beneficiary model to send with POST
            this.beneficiarySelectors.createProfileMappingFinished().subscribe(isMappingFinished => {
                if (isMappingFinished) {
                    // grab updated Beneficiary Domain Model
                    this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

                    // lock the beneficiary
                    this.lockBeneficiary(true);

                    // save new beneficiary profile
                    this.beneficiaryService.createBeneficiary(beneficiaryModel).subscribe(response => {
                        console.debug('CREATE BENEFICIARY SUCCESSFUL');
                        // set the new Profile active flag to false
                        this.updateNewProfileActive(false);

                        // grab the serviceCoverageOrganizations metadata
                        this.metaTypesSelectors.serviceCoverageOrganizations().subscribe(value => this.serviceCoverages = value).unsubscribe();
                        this.metaTypesSelectors.serviceCoveragePlanClassificationTypes().subscribe(value => this.classifications = value).unsubscribe();

                        // update beneficiary domain model with results
                        this.updateBeneficiaryDomainModel(new BeneficiaryPayload().withMutations(record => {
                            return record
                                .set('beneficiary', response)
                                .set('serviceCoverages', this.serviceCoverages)
                                .set('classificationTypes', this.classifications);
                        }) as BeneficiaryPayload);

                        // unlock the beneficiary
                        this.lockBeneficiary(false);

                        // indicate save is finished
                        this.updateCreateProfileSaveActive(false);

                        // navigate user to beneficiary dashboard
                        this.router.navigate(['/Beneficiary/', response.uuid]);
                    }, error => {
                        // unlock the beneficiary
                        this.lockBeneficiary(false);

                        // indicate save is finished
                        this.updateCreateProfileSaveActive(false);

                        console.debug('CREATE BENEFICIARY FAILED', error);

                        this.navActions.updateAlertMessageState(
                            new AlertItem({alertType : EnumAlertType.ERROR, message : error }));
                    });
                }
            });
        }
        else {
            console.debug('WARNING MULTIPLE CREATE BENEFICIARY REQUESTS WERE ATTEMPTED!');
        }
    }

    /**
     *
     * @param lock A value of true marks the beneficiary state as waiting on another operation or action; false unlocks it.
     */
    lockBeneficiary(lock : boolean) {
        this.store.dispatch({
            type: BeneficiaryActions.BENEFICIARY_WAIT,
            payload : lock
        });
    }
}
