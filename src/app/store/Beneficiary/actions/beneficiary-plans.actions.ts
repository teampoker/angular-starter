import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';
import * as moment from 'moment';

import {IAppStore} from '../../app-store';
import {NavActions} from '../../Navigation/nav.actions';
import {MetaDataTypesSelectors} from '../../MetaDataTypes/meta-data-types.selectors';
import {BeneficiaryService} from '../../../areas/Beneficiary/services/Beneficiary/beneficiary.service';
import {BeneficiaryStateSelectors} from '../selectors/beneficiary.selectors';
import {ServiceCoverageOrganizationType} from '../../MetaDataTypes/types/service-coverage-organization-type.model';
import {IBeneficiaryFieldUpdate} from '../types/beneficiary-field-update.model';
import {Beneficiary} from '../types/beneficiary.model';
import {AlertItem, EnumAlertType} from '../../Navigation/types/alert-item.model';
import {BeneficiaryPlansStateSummary} from '../types/beneficiary-plans-state-summary.model';
import {BeneficiaryPlansStateSelectors} from '../selectors/beneficiary-plans.selectors';
import {BeneficiaryServiceCoverage} from '../types/beneficiary-service-coverage.model';
import {BeneficiaryActions} from './beneficiary.actions';

@Injectable()

/**
 * Implementation of BeneficiaryPlansActions: Redux Action Creator Service that exposes methods to mutate Beneficiary Plans state
 */
export class BeneficiaryPlansActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static BENEFICIARY_PLANS_UPDATE_BENEFICIARY                           : string = 'BENEFICIARY_PLANS_UPDATE_BENEFICIARY';
    static BENEFICIARY_PLANS_ADD_BENEFICIARY_PLAN                         : string = 'BENEFICIARY_PLANS_ADD_BENEFICIARY_PLAN';
    static BENEFICIARY_PLANS_EDIT_BENEFICIARY_PLAN                        : string = 'BENEFICIARY_PLANS_EDIT_BENEFICIARY_PLAN';
    static BENEFICIARY_PLANS_REMOVE_BENEFICIARY_PLAN                      : string = 'BENEFICIARY_PLANS_REMOVE_BENEFICIARY_PLAN';
    static BENEFICIARY_PLANS_CANCEL_ADD_PLAN                              : string = 'BENEFICIARY_PLANS_CANCEL_ADD_PLAN';
    static BENEFICIARY_PLANS_CLOSE_ADD_PLAN                               : string = 'BENEFICIARY_PLANS_CLOSE_ADD_PLAN';
    static BENEFICIARY_PLANS_CANCEL_EDIT_PLAN                             : string = 'BENEFICIARY_PLANS_CANCEL_EDIT_PLAN';
    static BENEFICIARY_PLANS_CLOSE_EDIT_PLAN                              : string = 'BENEFICIARY_PLANS_CLOSE_EDIT_PLAN';
    static BENEFICIARY_PLANS_UPDATE_PLAN_FIELD_VALUE                      : string = 'BENEFICIARY_PLANS_UPDATE_PLAN_FIELD_VALUE';
    static BENEFICIARY_PLANS_UPDATE_PLAN_DYNAMIC_FIELD_VALUE              : string = 'BENEFICIARY_PLANS_UPDATE_PLAN_DYNAMIC_FIELD_VALUE';
    static BENEFICIARY_PLANS_UPDATE_ORGANIZATION_INDEX                    : string = 'BENEFICIARY_PLANS_UPDATE_ORGANIZATION_INDEX';
    static BENEFICIARY_PLANS_UPDATE_COVERAGE_PLAN_INDEX                   : string = 'BENEFICIARY_PLANS_UPDATE_COVERAGE_PLAN_INDEX';
    static BENEFICIARY_PLANS_SAVE_PLANS                                   : string = 'BENEFICIARY_PLANS_SAVE_PLANS';

    /**
     * BeneficiaryInfoActions constructor
     * @param store
     * @param navActions
     * @param metaTypesSelectors
     * @param beneficiaryService
     * @param beneficiarySelectors
     * @param beneficiaryPlansSelectors
     * @param beneficiaryActions
     */
    constructor(
        private store                       : NgRedux<IAppStore>,
        private navActions                  : NavActions,
        private metaTypesSelectors          : MetaDataTypesSelectors,
        private beneficiaryService          : BeneficiaryService,
        private beneficiarySelectors        : BeneficiaryStateSelectors,
        private beneficiaryPlansSelectors   : BeneficiaryPlansStateSelectors,
        private beneficiaryActions          : BeneficiaryActions
    ) {}

    /**
     * dispatch action to update beneficiary model on Redux store with results
     * of a Beneficiary Person PUT operation
     * @param response
     */
    private updateBeneficiaryFromAPIResponse(response : any) {
        this.store.dispatch({
            type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_BENEFICIARY,
            payload : response
        });
    }

    /**
     * inspect the eligibilty dates for a given service coverage and make
     * sure the start date is not after the end date and vice versa
     *
     * @param addedCoverage newly added service coverage
     *
     * @returns {boolean}
     */
    private checkServiceCoverageDatesOrder(addedCoverage : BeneficiaryPlansStateSummary) : boolean {
        if (addedCoverage) {
            // verify start date is before end date
            if (moment(addedCoverage.get('fromDate')).isAfter(moment(addedCoverage.get('thruDate')))) {
                return false;
            }
            // verify end date is after start date
            else if (moment(addedCoverage.get('thruDate')).isBefore(moment(addedCoverage.get('fromDate')))) {
                return false;
            }
            // verify start and end date aren't the same
            else if (moment(addedCoverage.get('thruDate')).isSame(moment(addedCoverage.get('fromDate')))) {
                return false;
            }
        }

        return true;
    }

    /**
     * checks to see if newly added service coverage date ranges
     * overlap with any of the existing beneficiary service coverages
     * @param addedCoverage
     * @returns {boolean}
     */
    private checkForDuplicateServiceCoverageDateRange(addedCoverage : BeneficiaryPlansStateSummary) : boolean {
        let isDuplicate          : boolean = false,
            isNewProfileActive   : boolean = false,
            serviceCoverages     : List<BeneficiaryServiceCoverage> = List<BeneficiaryServiceCoverage>(),
            serviceCoveragesVM   : List<BeneficiaryPlansStateSummary> = List<BeneficiaryPlansStateSummary>();

        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();
        this.beneficiaryPlansSelectors.beneficiaryServiceCoverages().subscribe(value => serviceCoverages = value).unsubscribe();
        this.beneficiaryPlansSelectors.beneficiaryPlansState().subscribe(value => serviceCoveragesVM = value.get('plansStateSummary', List())).unsubscribe();

        // is create profile active?
        if (isNewProfileActive) {
            // this is a new profile so we have to check against our service coverages viewmodel
            // if there's only one record then they are adding the beneficiary's very first service coverage and we can ignore this
            if (serviceCoveragesVM.size > 1) {
                // scan any previous service coverages and determine if the coverage dates of the
                // newly added service coverage overlaps with any of them
                serviceCoveragesVM.forEach((previousCoverage, previousCoverageIndex) => {
                    // verify the date range selected for the new service coverage DO NOT overlap
                    // the previous existing service coverage's data range
                    if (
                        // end of new range is same as start of matched range
                        moment(addedCoverage.get('thruDate')).isSame(moment(previousCoverage.get('fromDate'))) ||

                        // end of new range is after start of matched range
                        moment(addedCoverage.get('thruDate')).isAfter(moment(previousCoverage.get('fromDate')))  &&

                        // start of new range is before start of matched range
                        moment(addedCoverage.get('fromDate')).isBefore(moment(previousCoverage.get('fromDate')))
                    ) {
                        // check for CSR editing duplicate plan
                        if (this.beneficiaryPlansSelectors.isEditPlanActive()) {
                            if (this.beneficiaryPlansSelectors.editPlanIndex() !== previousCoverageIndex) {
                                // cannot add a service coverage with an overlapping date range
                                isDuplicate = true;
                            }
                        }
                        else {
                            if (this.beneficiaryPlansSelectors.addPlanIndex() !== previousCoverageIndex) {
                                // cannot add a service coverage with an overlapping date range
                                isDuplicate = true;
                            }
                        }
                    }
                    else if (
                        // start of new range is same as end of matched range
                        moment(addedCoverage.get('fromDate')).isSame(moment(previousCoverage.get('thruDate')))  ||

                        // start of new range is before end of matched range
                        moment(addedCoverage.get('fromDate')).isBefore(moment(previousCoverage.get('thruDate'))) &&

                        // end of new range is after end of matched range
                        moment(addedCoverage.get('thruDate')).isAfter(moment(previousCoverage.get('thruDate')))
                    ) {
                        // check for CSR editing duplicate plan
                        if (this.beneficiaryPlansSelectors.isEditPlanActive()) {
                            if (this.beneficiaryPlansSelectors.editPlanIndex() !== previousCoverageIndex) {
                                // cannot add a service coverage with an overlapping date range
                                isDuplicate = true;
                            }
                        }
                        else {
                            if (this.beneficiaryPlansSelectors.addPlanIndex() !== previousCoverageIndex) {
                                // cannot add a service coverage with an overlapping date range
                                isDuplicate = true;
                            }
                        }
                    }
                    else if (
                        // start of new range is same as start of matched range
                        moment(addedCoverage.get('fromDate')).isSame(moment(previousCoverage.get('fromDate'))) &&

                        // end of new range is same as end of matched range
                        moment(addedCoverage.get('thruDate')).isSame(moment(previousCoverage.get('thruDate'))) ||

                        // start of new range is same as start of matched range
                        moment(addedCoverage.get('fromDate')).isSame(moment(previousCoverage.get('fromDate'))) ||

                        // end of new range is same as end of matched range
                        moment(addedCoverage.get('thruDate')).isSame(moment(previousCoverage.get('thruDate')))
                    ) {
                        // check for CSR editing duplicate plan
                        if (this.beneficiaryPlansSelectors.isEditPlanActive()) {
                            if (this.beneficiaryPlansSelectors.editPlanIndex() !== previousCoverageIndex) {
                                // cannot add a service coverage with an overlapping date range
                                isDuplicate = true;
                            }
                        }
                        else {
                            if (this.beneficiaryPlansSelectors.addPlanIndex() !== previousCoverageIndex) {
                                // cannot add a service coverage with an overlapping date range
                                isDuplicate = true;
                            }
                        }
                    }
                });
            }
        }
        else {
            // check against the beneficiary domain model
            if (serviceCoverages.size > 0) {
                // scan any previous service coverages and determine if the coverage dates of the
                // newly added service coverage overlaps with any of them
                serviceCoverages.forEach((previousCoverage, previousCoverageIndex) => {
                    // verify the date range selected for the new requirement DO NOT overlap
                    // the previous existing special requirement's data range
                    if (
                        // end of new range is same as start of matched range
                        moment(addedCoverage.get('thruDate')).isSame(moment(previousCoverage.get('fromDate'))) ||

                        // end of new range is after start of matched range
                        moment(addedCoverage.get('thruDate')).isAfter(moment(previousCoverage.get('fromDate')))  &&

                        // start of new range is before start of matched range
                        moment(addedCoverage.get('fromDate')).isBefore(moment(previousCoverage.get('fromDate')))
                    ) {
                        // check for CSR editing duplicate plan
                        if (this.beneficiaryPlansSelectors.editPlanIndex() !== previousCoverageIndex) {
                            // cannot add a service coverage with an overlapping date range
                            isDuplicate = true;
                        }
                    }
                    else if (
                        // start of new range is same as end of matched range
                        moment(addedCoverage.get('fromDate')).isSame(moment(previousCoverage.get('thruDate')))  ||

                        // start of new range is before end of matched range
                        moment(addedCoverage.get('fromDate')).isBefore(moment(previousCoverage.get('thruDate'))) &&

                        // end of new range is after end of matched range
                        moment(addedCoverage.get('thruDate')).isAfter(moment(previousCoverage.get('thruDate')))
                    ) {
                        // check for CSR editing duplicate plan
                        if (this.beneficiaryPlansSelectors.editPlanIndex() !== previousCoverageIndex) {
                            // cannot add a service coverage with an overlapping date range
                            isDuplicate = true;
                        }
                    }
                    else if (
                        // start of new range is same as start of matched range
                        moment(addedCoverage.get('fromDate')).isSame(moment(previousCoverage.get('fromDate'))) &&

                        // end of new range is same as end of matched range
                        moment(addedCoverage.get('thruDate')).isSame(moment(previousCoverage.get('thruDate'))) ||

                        // start of new range is same as start of matched range
                        moment(addedCoverage.get('fromDate')).isSame(moment(previousCoverage.get('fromDate'))) ||

                        // end of new range is same as end of matched range
                        moment(addedCoverage.get('thruDate')).isSame(moment(previousCoverage.get('thruDate')))
                    ) {
                        // check for CSR editing duplicate plan
                        if (this.beneficiaryPlansSelectors.editPlanIndex() !== previousCoverageIndex) {
                            // cannot add a service coverage with an overlapping date range
                            isDuplicate = true;
                        }
                    }
                });
            }
        }

        return isDuplicate;
    }

    /**
     * dispatch action to update add beneficiary plan index on redux store
     */
    addBeneficiaryPlan() {
        let isNewProfileActive  : boolean,
            serviceCoverages    : List<ServiceCoverageOrganizationType>;

        // grab isNewProfile flag state
        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        // grab the serviceCoverageOrganizations metadata
        this.metaTypesSelectors.serviceCoverageOrganizations().subscribe(value => serviceCoverages = value).unsubscribe();

        if (!isNewProfileActive) {
            this.navActions.toggleIsPoppedNavState(true);
        }

        this.store.dispatch({
            type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_ADD_BENEFICIARY_PLAN,
            payload : serviceCoverages
        });
    }

    /**
     * dispatch action to update edit beneficiary plan index on redux store
     * @param index index of edited plan
     */
    editBeneficiaryPlan(index : number) {
        let isNewProfileActive  : boolean,
            serviceCoverages    : List<ServiceCoverageOrganizationType>;

        // grab isNewProfile flag state
        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        // grab the serviceCoverageOrganizations metadata
        this.metaTypesSelectors.serviceCoverageOrganizations().subscribe(value => serviceCoverages = value).unsubscribe();

        if (!isNewProfileActive) {
            this.navActions.toggleIsPoppedNavState(true);
        }

        this.store.dispatch({
            type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_EDIT_BENEFICIARY_PLAN,
            payload : index
        });
    }

    /**
     * dispatch action to cancel beneficiary add plan workflow
     */
    cancelAddBeneficiaryPlan() {
        this.navActions.toggleIsPoppedNavState(false);

        this.store.dispatch({ type : BeneficiaryPlansActions.BENEFICIARY_PLANS_CANCEL_ADD_PLAN });
    }

    /**
     * dispatch action to close beneficiary add plan screen
     */
    closeAddBeneficiaryPlan() {
        this.navActions.toggleIsPoppedNavState(false);

        this.store.dispatch({ type : BeneficiaryPlansActions.BENEFICIARY_PLANS_CLOSE_ADD_PLAN });
    }

    /**
     * dispatch action to cancel beneficiary edit plan workflow
     */
    cancelEditBeneficiaryPlan() {
        this.navActions.toggleIsPoppedNavState(false);

        this.store.dispatch({ type : BeneficiaryPlansActions.BENEFICIARY_PLANS_CANCEL_EDIT_PLAN });
    }

    /**
     * dispatch action to close beneficiary edit plan screen
     */
    closeEditBeneficiaryPlan() {
        this.navActions.toggleIsPoppedNavState(false);

        this.store.dispatch({ type : BeneficiaryPlansActions.BENEFICIARY_PLANS_CLOSE_EDIT_PLAN });
    }

    /**
     * dispatch action with updated input data for beneficiary plan field
     * @param update
     */
    updatePlanField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_PLAN_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for dynamic beneficiary plan field
     * @param update
     */
    updateDynamicPlanField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_PLAN_DYNAMIC_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action to update beneficiary plans organization index in redux store
     * @param index index of edited plan
     */
    updateOrganizationIndex(index : number) {
        this.store.dispatch({
            type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_ORGANIZATION_INDEX,
            payload : index
        });
    }

    /**
     * dispatch action to update beneficiary plans coverage plan index in redux store
     * @param index index of edited plan
     */
    updateCoveragePlanIndex(index : number) {
        this.store.dispatch({
            type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_UPDATE_COVERAGE_PLAN_INDEX,
            payload : index
        });
    }

    /**
     * dispatch action to map the beneficiary plans portion of client view model
     * to the current Beneficiary's Domain Model
     * this ONLY gets used during Edit Beneficiary Profile workflow
     */
    saveBeneficiaryPlans() {
        let isNewProfileActive          : boolean       = false,
            modifiedServiceCoverage     : BeneficiaryPlansStateSummary;

        // grab the most recently added service coverage
        modifiedServiceCoverage = this.beneficiaryPlansSelectors.modifiedBeneficiaryServiceCoverage();

        // verify start date is not after end date and vice versa
        if (this.checkServiceCoverageDatesOrder(modifiedServiceCoverage)) {
            // check for duplicate service coverage date range first
            if (!this.checkForDuplicateServiceCoverageDateRange(modifiedServiceCoverage)) {
                // dispatch action to update the beneficiary model
                this.store.dispatch({type: BeneficiaryPlansActions.BENEFICIARY_PLANS_SAVE_PLANS});
                this.beneficiaryActions.lockBeneficiary(true);

                // is Create Profile Active? if so we don't do a save operation to the API here
                this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

                if (!isNewProfileActive) {
                    this.pushServiceCoverages();
                }
                else {
                    // unlock beneficiary as we're in a 'create new' scenario
                    this.beneficiaryActions.lockBeneficiary(false);
                }
            }
            else {
                // display warning message to user
                this.navActions.updateAlertMessageState(
                    new AlertItem({
                        alertType   : EnumAlertType.ERROR,
                        message     : 'DUPLICATE_PLAN_ERROR'
                    })
                );

                // if not editing...
                if (!this.beneficiaryPlansSelectors.isEditPlanActive()) {
                    // delete this connection from the view model
                    this.store.dispatch({
                        type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_REMOVE_BENEFICIARY_PLAN,
                        payload : this.beneficiaryPlansSelectors.modifiedBeneficiaryServiceCoverageIndex()
                    });
                }
                else {
                    // roll back the edit operation
                    this.cancelEditBeneficiaryPlan();
                }
            }
        }
        else {
            // display warning message to user
            this.navActions.updateAlertMessageState(
                new AlertItem({
                    alertType   : EnumAlertType.ERROR,
                    message     : 'PLAN_DATE_ORDER_ERROR'
                })
            );

            // if not editing...
            if (!this.beneficiaryPlansSelectors.isEditPlanActive()) {
                // delete this connection from the view model
                this.store.dispatch({
                    type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_REMOVE_BENEFICIARY_PLAN,
                    payload : this.beneficiaryPlansSelectors.modifiedBeneficiaryServiceCoverageIndex()
                });
            }
            else {
                // roll back the edit operation
                this.cancelEditBeneficiaryPlan();
            }
        }
    }

    /**
     * remove service coverage from Beneficiary state
     * @param index index of removed plan
     */
    removeBeneficiaryPlan(index : number) {
        let isNewProfileActive  : boolean     = false,
            beneficiaryModel    : Beneficiary = new Beneficiary();

        this.store.dispatch({
            type    : BeneficiaryPlansActions.BENEFICIARY_PLANS_REMOVE_BENEFICIARY_PLAN,
            payload : index
        });

        // is Create Profile Active? if so we don't do a save operation to the API here
        this.beneficiarySelectors.newProfileActive().subscribe(value => isNewProfileActive = value).unsubscribe();

        if (!isNewProfileActive) {
            this.beneficiaryActions.lockBeneficiary(true);
            // grab Beneficiary Domain Model
            this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

            // save the updated Beneficiary Person data
            this.beneficiaryService.updateBeneficiaryServiceCoverages(beneficiaryModel).subscribe(response => {
                // update beneficiary domain model with updated response data
                this.updateBeneficiaryFromAPIResponse(response);
                this.beneficiaryActions.lockBeneficiary(false);
            }, error => {
                this.beneficiaryActions.lockBeneficiary(false);
                this.navActions.updateAlertMessageState(
                    new AlertItem({ alertType : EnumAlertType.ERROR, message : error.message }));
            });
        }
    }

    /**
     * Command to push the service coverages back to the API (server-side).
     *
     */
    pushServiceCoverages() {
        let beneficiaryModel : Beneficiary;

        // grab Beneficiary Domain Model
        this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

        if (beneficiaryModel) {
            this.beneficiaryActions.lockBeneficiary(true);

            // save the updated Beneficiary Person data
            this.beneficiaryService.updateBeneficiaryServiceCoverages(beneficiaryModel).subscribe(response => {
                // update beneficiary domain model with updated response data
                this.updateBeneficiaryFromAPIResponse(response);
                this.beneficiaryActions.lockBeneficiary(false);
            }, error => {
                this.beneficiaryActions.lockBeneficiary(false);
                this.navActions.updateAlertMessageState(
                    new AlertItem({ alertType : EnumAlertType.ERROR, message : error }));
            });
        }
        else {
            // no beneficiary was provided
            console.warn('Nothing to push - no beneficiary provided.');
        }
    }
}
