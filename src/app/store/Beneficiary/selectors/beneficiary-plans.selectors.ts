import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../../app-store';
import {BeneficiaryPlansState} from '../types/beneficiary-plans-state.model';
import {BeneficiaryPlansStateSummary} from '../types/beneficiary-plans-state-summary.model';
import {BeneficiaryServiceCoverage} from '../types/beneficiary-service-coverage.model';

@Injectable()

/**
 * implementation for BeneficiaryPlansStateSelectors: responsible for exposing custom state subscriptions to Beneficiary Plans state
 */
export class BeneficiaryPlansStateSelectors {
    /**
     * BeneficiaryPlansStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to BeneficiaryState.beneficiaryPlansState
     * @returns {Observable<BeneficiaryPlansState>}
     */
    beneficiaryPlansState() : Observable<BeneficiaryPlansState> {
        return this.store.select(state => state.beneficiaryState.get('beneficiaryPlansState'));
    }

    /**
     * expose Observable to BeneficiaryState.beneficiary.serviceCoverages
     * @returns {Observable<BeneficiaryServiceCoverage>}
     */
    beneficiaryServiceCoverages() : Observable<List<BeneficiaryServiceCoverage>> {
        return this.store.select(state => state.beneficiaryState.getIn(['beneficiary', 'serviceCoverages']));
    }

    /**
     * return recently added beneficiary service coverage from beneficiaryPlansState.plansStateSummary collection
     * @returns {BeneficiaryPlansStateSummary}
     */
    modifiedBeneficiaryServiceCoverage() : BeneficiaryPlansStateSummary {
        let modifiedServiceCoverage : BeneficiaryPlansStateSummary = new BeneficiaryPlansStateSummary();

        // adding or editing a service coverage?
        if (this.isEditPlanActive()) {
            this.store.select(state => state.beneficiaryState.getIn(['beneficiaryPlansState', 'plansStateSummary', this.editPlanIndex()])).subscribe(coverage => modifiedServiceCoverage = coverage).unsubscribe();
        }
        else {
            this.store.select(state => state.beneficiaryState.getIn(['beneficiaryPlansState', 'newPlanStateSummary'])).subscribe(coverage => modifiedServiceCoverage = coverage).unsubscribe();
        }

        return modifiedServiceCoverage;
    }

    /**
     * return index of most recently added beneficiary service coverage
     * @returns {number}
     */
    modifiedBeneficiaryServiceCoverageIndex() : number {
        let serviceCoverageIndex : number = -1;

        // adding or editing a service coverage?
        if (this.isEditPlanActive()) {
            serviceCoverageIndex = this.editPlanIndex();
        }
        else {
            serviceCoverageIndex = this.addPlanIndex();
        }

        return serviceCoverageIndex;
    }

    /**
     * return current state of beneficiaryPlansState.isEditPlanActive flag
     * @returns {boolean}
     */
    isEditPlanActive() : boolean {
        let isEditActive : boolean = false;

        this.store.select(state => state.beneficiaryState.getIn(['beneficiaryPlansState', 'isEditPlanActive'])).subscribe(value => isEditActive = value).unsubscribe();

        return isEditActive;
    }

    /**
     * return current state of beneficiaryPlansState.editPlanIndex flag
     * @returns {number}
     */
    editPlanIndex() : number {
        let planIndex : number = -1;

        this.store.select(state => state.beneficiaryState.getIn(['beneficiaryPlansState', 'editPlanIndex'])).subscribe(value => planIndex = value).unsubscribe();

        return planIndex;
    }

    /**
     * return current state of beneficiaryPlansState.addPlanIndex flag
     * @returns {number}
     */
    addPlanIndex() : number {
        let planIndex : number = -1;

        this.store.select(state => state.beneficiaryState.getIn(['beneficiaryPlansState', 'addPlanIndex'])).subscribe(value => planIndex = value).unsubscribe();

        return planIndex;
    }
}
