import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../../app-store';
import {BeneficiaryConnectionsState} from '../types/beneficiary-connections-state.model';
import {BeneficiaryConnectionsStateDetail} from '../types/beneficiary-connections-state-detail.model';
import {Beneficiary} from '../types/beneficiary.model';
import {BeneficiaryConnection} from '../types/beneficiary-connection.model';

@Injectable()

/**
 * implementation for BeneficiaryConnectionsStateSelectors: responsible for exposing custom state subscriptions to Beneficiary Connections state
 */
export class BeneficiaryConnectionsStateSelectors {
    /**
     * BeneficiaryConnectionsStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to BeneficiaryState.beneficiaryConnectionsState
     * @returns {Observable<BeneficiaryConnectionsState>}
     */
    beneficiaryConnectionsState() : Observable<BeneficiaryConnectionsState> {
        return this.store.select(state => state.beneficiaryState.get('beneficiaryConnectionsState'));
    }

    /**
     * expose Observable to BeneficiaryState.beneficiary.connections
     * @returns {Observable<BeneficiaryConnectionsState>}
     */
    beneficiaryConnections() : Observable<List<BeneficiaryConnection>> {
        return this.store.select(state => state.beneficiaryState.getIn(['beneficiary', 'connections']));
    }

    /**
     * return current value of the BeneficiaryState.beneficiaryConnectionsState.isConnectionAddedManually flag
     */
    isConnectionAddedManually() : boolean {
        let isAddedManually : boolean = false;

        this.store.select(state => state.beneficiaryState.getIn(['beneficiaryConnectionsState', 'isConnectionAddedManually'])).subscribe(value => isAddedManually = value).unsubscribe();

        return isAddedManually;
    }

    /**
     * return recently manually added beneficiary connection from beneficiaryConnectionsState.connectionsStateDetail collection
     * @returns {BeneficiaryConnectionsStateDetail}
     */
    modifiedBeneficiaryConnection() : BeneficiaryConnectionsStateDetail {
        let addedConnection : BeneficiaryConnectionsStateDetail = new BeneficiaryConnectionsStateDetail();

        if (this.isEditConnectionActive()) {
            this.store.select(state => state.beneficiaryState.getIn(['beneficiaryConnectionsState', 'connectionsStateDetail', this.editConnectionIndex()])).subscribe(connection => addedConnection = connection).unsubscribe();
        }
        else {
            this.store.select(state => state.beneficiaryState.getIn(['beneficiaryConnectionsState', 'connectionsStateDetail', this.addConnectionIndex()])).subscribe(connection => addedConnection = connection).unsubscribe();
        }

        return addedConnection;
    }

    /**
     * return index of most recently added beneficiary connection
     * @returns {number}
     */
    mostRecentlyAddedBeneficiaryConnectionIndex() : number {
        let connectionIndex : number = 0;

        this.store.select(state => state.beneficiaryState.getIn(['beneficiaryConnectionsState', 'connectionsStateDetail']).size).subscribe(value => connectionIndex = value - 1).unsubscribe();

        return connectionIndex;
    }

    /**
     * return recently manually added beneficiary connection from beneficiaryConnectionsState.manuallyAddedConnection
     * @returns {Beneficiary}
     */
    manuallyAddedConnection() : Beneficiary {
        let manuallyAddedConnection : Beneficiary = new Beneficiary();

        this.store.select(state => state.beneficiaryState.getIn(['beneficiaryConnectionsState', 'manuallyAddedConnection'])).subscribe(value => manuallyAddedConnection = value).unsubscribe();

        return manuallyAddedConnection;
    }

    /**
     * return current state of beneficiaryConnectionState.isEditConnectionActive flag
     * @returns {boolean}
     */
    isEditConnectionActive() : boolean {
        let isEditActive : boolean = false;

        this.store.select(state => state.beneficiaryState.getIn(['beneficiaryConnectionsState', 'isEditConnectionActive'])).subscribe(value => isEditActive = value).unsubscribe();

        return isEditActive;
    }

    /**
     * return current state of beneficiaryConnectionState.editConnectionIndex flag
     * @returns {number}
     */
    editConnectionIndex() : number {
        let planIndex : number = -1;

        this.store.select(state => state.beneficiaryState.getIn(['beneficiaryConnectionsState', 'editConnectionIndex'])).subscribe(value => planIndex = value).unsubscribe();

        return planIndex;
    }

    /**
     * return current state of beneficiaryConnectionState.addConnectionIndex flag
     * @returns {number}
     */
    addConnectionIndex() : number {
        let planIndex : number = -1;

        this.store.select(state => state.beneficiaryState.getIn(['beneficiaryConnectionsState', 'addConnectionIndex'])).subscribe(value => planIndex = value).unsubscribe();

        return planIndex;
    }
}
