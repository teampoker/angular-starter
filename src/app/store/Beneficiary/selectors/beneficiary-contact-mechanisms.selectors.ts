import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../../app-store';
import {BeneficiaryContactMechanismsState} from '../types/beneficiary-contact-mechanisms-state.model';

@Injectable()

/**
 * implementation for BeneficiaryContactMechanismsStateSelectors: responsible for exposing custom state subscriptions to Beneficiary Contact Mechanisms state
 */
export class BeneficiaryContactMechanismsStateSelectors {
    /**
     * BeneficiaryContactMechanismsStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /* BENEFICIARY ADDRESS & CONTACT INFO */

    /**
     * expose Observable to BeneficiaryState.beneficiaryContactMechanismsState
     * @returns {Observable<BeneficiaryContactMechanismsState>}
     */
    beneficiaryContactMechanismsState() : Observable<BeneficiaryContactMechanismsState> {
        return this.store.select(state => state.beneficiaryState.get('beneficiaryContactMechanismsState'));
    }
}
