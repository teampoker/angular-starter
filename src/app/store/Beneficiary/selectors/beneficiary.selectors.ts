import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {NgRedux, select} from '@angular-redux/store';

import {IAppStore} from '../../app-store';
import {Beneficiary} from '../types/beneficiary.model';
import {
    EnumBeneficiaryHeaderType,
    BeneficiaryHeader
} from '../types/beneficiary-header.model';

@Injectable()

/**
 * implementation for BeneficiaryStateSelectors: responsible for exposing custom state subscriptions to BeneficiaryState
 */
export class BeneficiaryStateSelectors {
    /**
     * BeneficiaryStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to beneficiary
     * @returns {Observable<Beneficiary>}
     */
    beneficiary() : Observable<Beneficiary> {
        return this.store.select(state => state.beneficiaryState.get('beneficiary'));
    }

    /**
     * expose Observable to beneficiary.uuid
     * @returns {Observable<string>}
     */
    beneficiaryUuid() : Observable<string> {
        return this.store.select(state => state.beneficiaryState.getIn(['beneficiary', 'uuid']));
    }

    /**
     * expose Observable to beneficiaryState.newProfileActive
     * @returns {Observable<boolean>}
     */
    newProfileActive() : Observable<boolean> {
        return this.store.select(state => state.beneficiaryState.get('newProfileActive'));
    }

    /**
     * expose Observable to beneficiaryState.isCreateProfileSaveActive
     * @returns {Observable<boolean>}
     */
    createProfileSaveActive() : Observable<boolean> {
        return this.store.select(state => state.beneficiaryState.get('isCreateProfileSaveActive'));
    }

    /**
     * expose Observable to beneficiaryState.isCreateProfileMappingFinished
     * @returns {Observable<boolean>}
     */
    createProfileMappingFinished() : Observable<boolean> {
        return this.store.select(state => state.beneficiaryState.get('isCreateProfileMappingFinished'));
    }

    /**
     * exposes the beneficiaryState.isFetching property of redux store
     * @returns {Observable<boolean>} true, if the beneficiary is actively interacting with API; false if not.
     */
    @select(state => state.beneficiaryState.isFetching)
    isFetching : Observable<boolean>;

    @select(state => state.beneficiaryState.isWaiting)
    isWaiting : Observable<boolean>;

    /**
     * expose Observable to beneficiary Header info
     * @param headerType
     * @returns {Observable<BeneficiaryHeader>}
     */
    beneficiaryHeader(headerType : EnumBeneficiaryHeaderType) : Observable<BeneficiaryHeader> {
        let header              : BeneficiaryHeader = new BeneficiaryHeader(),
            headerSubscription  : Subscription;

        // setup Observable
        return Observable.create(observer => {
            switch (headerType) {
                case EnumBeneficiaryHeaderType.BENEFICIARY_MAIN :
                    // set header title and type
                    header = header.withMutations(record => {
                        return record
                            .set('title', 'BENEFICIARY')
                            .set('headerType', headerType) as BeneficiaryHeader;
                    }) as BeneficiaryHeader;

                    break;
                case EnumBeneficiaryHeaderType.CREATE_NEW_PROFILE :
                    // set header title and type
                    header = header.withMutations(record => {
                        return record
                            .set('title', 'CREATE_NEW_PROFILE')
                            .set('headerType', headerType) as BeneficiaryHeader;
                    }) as BeneficiaryHeader;

                    break;
                case EnumBeneficiaryHeaderType.BENEFICIARY_PROFILE :
                    // set header title and type
                    header = header.withMutations(record => {
                        return record
                            .set('title', 'PROFILE')
                            .set('headerType', headerType) as BeneficiaryHeader;
                    }) as BeneficiaryHeader;

                    break;
                default :
                    header = header.withMutations(record => {
                        return record
                            .set('title', 'BENEFICIARY')
                            .set('headerType', headerType) as BeneficiaryHeader;
                    }) as BeneficiaryHeader;

                    // emit this value
                    observer.next(header);
            }

            // pull necessary beneficiaryInfo data
            headerSubscription = Observable.combineLatest(
                this.store.select(state => state.beneficiaryState.get('beneficiary')),
                this.store.select(state => state.beneficiaryState.get('beneficiaryContactMechanismsState'))
            )
            .subscribe(val => {
                // update local state
                header = header.withMutations(record => {
                    return record
                        .set('firstName', val[0].get('firstName'))
                        .set('middleName', val[0].get('middleName'))
                        .set('lastName', val[0].get('lastName'))
                        .set('birthDate', val[0].get('birthDate'))
                        .set('primaryAddress', val[1].getIn(['contactMechanismsState', 'addresses', 0, 'street']))
                        .set('primaryEmail', val[1].getIn(['contactMechanismsState', 'emails', 0, 'email']))
                        .set('primaryPhone', val[1].getIn(['contactMechanismsState', 'phones', 0, 'phone']));
                }) as BeneficiaryHeader;

                // emit updated header info
                observer.next(header);
            });

            // onComplete handler
            return () => {
                // unsubscribe from Redux store
                headerSubscription.unsubscribe();
            };
        });

    }
}
