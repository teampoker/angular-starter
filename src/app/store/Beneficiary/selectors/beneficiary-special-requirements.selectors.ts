import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../../app-store';
import {BeneficiarySpecialRequirementsState} from '../types/beneficiary-special-requirements-state.model';
import {BeneficiarySpecialRequirement} from '../types/beneficiary-special-requirement.model';

@Injectable()

/**
 * implementation for BeneficiarySpecialRequirementsSelectors: responsible for exposing custom state subscriptions to Beneficiary Special Requirements state
 */
export class BeneficiarySpecialRequirementsSelectors {
    /**
     * BeneficiarySpecialRequirementsSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to beneficiaryState.beneficiarySpecialRequirementsState
     * @returns {Observable<BeneficiarySpecialRequirementsState>}
     */
    beneficiarySpecialRequirementsState() : Observable<BeneficiarySpecialRequirementsState> {
        return this.store.select(state => state.beneficiaryState.get('beneficiarySpecialRequirementsState'));
    }

    /**
     * expose Observable to beneficiaryState.beneficiarySpecialRequirementsState
     * @returns {Observable<BeneficiarySpecialRequirementsState>}
     */
    beneficiarySpecialRequirements() : Observable<List<BeneficiarySpecialRequirement>> {
        return this.store.select(state => state.beneficiaryState.getIn(['beneficiary', 'specialRequirements']));
    }

    /**
     * return recently manually added beneficiary special requirement from beneficiarySpecialRequirementsState.requirementsState collection
     * @returns {BeneficiarySpecialRequirement}
     */
    mostRecentlyAddedBeneficiarySpecialRequirement() : BeneficiarySpecialRequirement {
        let addedSpecialRequirement : BeneficiarySpecialRequirement = new BeneficiarySpecialRequirement();

        this.store.select(state => state.beneficiaryState.getIn(['beneficiarySpecialRequirementsState', 'tempSpecialRequirement'])).subscribe(requirement => addedSpecialRequirement = requirement).unsubscribe();

        return addedSpecialRequirement;
    }
}
