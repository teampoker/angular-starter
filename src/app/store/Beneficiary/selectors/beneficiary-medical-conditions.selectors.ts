import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../../app-store';
import {BeneficiaryMedicalConditionsState} from '../types/beneficiary-medical-conditions-state.model';

@Injectable()

/**
 * implementation for BeneficiaryMedicalConditionsStateSelectors: responsible for exposing custom state subscriptions to Beneficiary Medical Conditions state
 */
export class BeneficiaryMedicalConditionsStateSelectors {
    /**
     * BeneficiaryMedicalConditionsStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to BeneficiaryState.beneficiaryMedicalConditionsState
     * @returns {Observable<BeneficiaryMedicalConditionsState>}
     */
    beneficiaryMedicalConditionsState() : Observable<BeneficiaryMedicalConditionsState> {
        return this.store.select(state => state.beneficiaryState.get('beneficiaryMedicalConditionsState'));
    }
}
