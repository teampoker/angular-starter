import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {
    NgRedux,
    select
} from '@angular-redux/store';

import {IAppStore} from '../../app-store';
import {BeneficiaryInfoState} from '../types/beneficiary-info-state.model';

@Injectable()

/**
 * implementation for BeneficiaryInfoStateSelectors: responsible for exposing custom state subscriptions to Beneficiary Information state
 */
export class BeneficiaryInfoStateSelectors {
    /**
     * BeneficiaryInfoStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to BeneficiaryState.beneficiaryInfoState
     * @returns {Observable<BeneficiaryInfoState>}
     */
    beneficiaryInfoState() : Observable<BeneficiaryInfoState> {
        return this.store.select(state => state.beneficiaryState.get('beneficiaryInfoState'));
    }

    /**
     * exposes a waiting indicator on the state object
     */
    @select(state => state.beneficiaryState.beneficiaryInfoState.isWaiting)
    isWaiting : Observable<boolean>;
}
