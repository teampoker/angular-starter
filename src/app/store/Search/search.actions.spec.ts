import {async} from '@angular/core/testing';
import {Router} from '@angular/router';
import {List} from 'immutable';
import {Observable} from 'rxjs/Observable';

import {SearchActions} from './search.actions';
import {SearchStateSelectors} from './search.selectors';
import {MockRouter} from '../../../testing/router-stubs';
import {MockRedux} from '../../../testing/ng2-redux-subs';
import {SearchService} from '../../shared/services/Search/search.service';
import {SearchParameters} from './types/search-parameters.model';
import {SearchType} from './types/search-type.model';
import {KeyValuePair} from '../types/key-value-pair.model';
import {SearchResultsOrderByConfig} from './types/search-results-order-by-config.model';

// Mock the SearchService with the methods we need to trigger
class MockStatementsService extends SearchService {
    constructor() {
        super(undefined);
    }

    getSearchTypes() {
        return Observable.create(observer => {
            // update Redux store
            observer.next(List());
        });
    }

    getHeaderSearchResults() {
        return Observable.create(observer => {
            // update Redux store
            observer.next(List());
        });
    }

    getPeopleSearchResults() {
        return Observable.create(observer => {
            // update Redux store
            observer.next(List());
        });
    }
}

// Mock the SearchStateSelectors with the methods we need to trigger
class MockSearchStateSelectors extends SearchStateSelectors {
    constructor() {
        super(undefined);
    }

    headerSearchParameters() : SearchParameters {
        return new SearchParameters({
            searchText  : 'user entered text',
            pageSize    : '10',
            pageOffset  : 0,
            sortColumn  : '',
            sortOrder   : 'asc'
        });
    }

    peopleSearchParameters() : SearchParameters {
        return new SearchParameters({
            searchText  : 'user entered text',
            pageSize    : '10',
            pageOffset  : 0,
            sortColumn  : '',
            sortOrder   : 'asc'
        });
    }

    selectedHeaderSearchType() : KeyValuePair {
        return new KeyValuePair({
            id    : '1',
            value : 'Beneficiary'
        });
    }

    searchTypes() : Observable<List<SearchType>> {
        return Observable.create(observer => {
            const searchTypes : List<SearchType> = List<SearchType>();

            // update Redux store
            observer.next(searchTypes);
        });
    }

    isHeaderSearchActive() : boolean {
        return false;
    }
}

describe('Search Action Creators', () => {
    let searchActions       : SearchActions,
        searchService       : SearchService,
        searchSelectors     : SearchStateSelectors,
        mockRouter          : MockRouter,
        mockRedux           : MockRedux;

    // setup tasks to perform before each test
    beforeEach(() => {
        // Initialize mock NgRedux and create a new instance of the
        // ActionCreator Service to be tested.
        mockRedux           = new MockRedux();
        searchService       = new MockStatementsService();
        searchSelectors     = new MockSearchStateSelectors();
        mockRouter          = new MockRouter();
        searchActions       = new SearchActions(
            mockRedux,
            searchService,
            searchSelectors,
            mockRouter as Router
        );
    });

    // test definitions
    it('getSearchTypes method should dispatch SEARCH_UPDATE_SEARCH_TYPES action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_SEARCH_TYPES,
            payload : List()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.getSearchTypes();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('getHeaderSearchResults method should dispatch SEARCH_UPDATE_HEADER_SEARCH_RESULTS action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS,
            payload : List()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.getHeaderSearchResults();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('getPeopleSearchResults method should dispatch SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS,
            payload : List()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.getPeopleSearchResults();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('getHeaderSearchResults method should navigate Router to /Search url', async(() => {
        // spy on mock navigate method
        spyOn(mockRouter, 'navigate');

        searchActions.getHeaderSearchResults();

        expect(mockRouter.navigate).toHaveBeenCalledWith(['/Search']);
    }));

    it('updateSearchTypes method should dispatch SEARCH_UPDATE_SEARCH_TYPES action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_SEARCH_TYPES,
            payload : []
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updateSearchTypes([]);

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updateHeaderSearchResults method should dispatch SEARCH_UPDATE_HEADER_SEARCH_RESULTS action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS,
            payload : List<any>()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updateHeaderSearchResults(List<any>());

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updatePeopleSearchResults method should dispatch SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS,
            payload : List<any>()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updatePeopleSearchResults(List<any>());

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updateSelectedHeaderSearchType method should dispatch SEARCH_UPDATE_HEADER_SEARCH_TYPE action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_TYPE,
            payload : new KeyValuePair({
                id      : '0',
                value   : 'test category'
            })
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updateSelectedHeaderSearchType(new KeyValuePair({
            id      : '0',
            value   : 'test category'
        }));

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updateSelectedPeopleSearchType method should dispatch SEARCH_UPDATE_PEOPLE_SEARCH_TYPE action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_TYPE,
            payload : new KeyValuePair({
                id      : '0',
                value   : 'test category'
            })
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updateSelectedPeopleSearchType(new KeyValuePair({
            id      : '0',
            value   : 'test category'
        }));

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updateHeaderSearchSelectedPageSize method should dispatch SEARCH_UPDATE_HEADER_PAGE_SIZE action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_HEADER_PAGE_SIZE,
            payload : new KeyValuePair({
                id      : '0',
                value   : 'test view count'
            })
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updateHeaderSearchSelectedPageSize(new KeyValuePair({
            id      : '0',
            value   : 'test view count'
        }));

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updatePeopleSearchSelectedPageSize method should dispatch SEARCH_UPDATE_PEOPLE_PAGE_SIZE action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_PAGE_SIZE,
            payload : new KeyValuePair({
                id      : '0',
                value   : 'test view count'
            })
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updatePeopleSearchSelectedPageSize(new KeyValuePair({
            id      : '0',
            value   : 'test view count'
        }));

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updateHeaderSearchText method should dispatch SEARCH_UPDATE_HEADER_SEARCH_TEXT action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_TEXT,
            payload : 'some search text'
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updateHeaderSearchText('some search text');

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updatePeopleSearchText method should dispatch SEARCH_UPDATE_PEOPLE_SEARCH_TEXT action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_TEXT,
            payload : 'some search text'
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updatePeopleSearchText('some search text');

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updateHeaderSearchOrderByConfig method should dispatch SEARCH_UPDATE_HEADER_SEARCH_RESULTS_ORDER_BY action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS_ORDER_BY,
            payload : new SearchResultsOrderByConfig({
                sortColumn  : 'lastName',
                sortOrder   : 'LastName__ASC'
            })
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updateHeaderSearchOrderByConfig(new SearchResultsOrderByConfig({
            sortColumn  : 'lastName',
            sortOrder   : 'LastName__ASC'
        }));

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('updatePeopleSearchOrderByConfig method should dispatch SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS_ORDER_BY action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS_ORDER_BY,
            payload : new SearchResultsOrderByConfig({
                sortColumn  : 'lastName',
                sortOrder   : 'LastName__ASC'
            })
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.updatePeopleSearchOrderByConfig(new SearchResultsOrderByConfig({
            sortColumn  : 'lastName',
            sortOrder   : 'LastName__ASC'
        }));

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('incrementHeaderPageIndex method should dispatch SEARCH_UPDATE_HEADER_SEARCH_TEXT action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_INCREMENT_HEADER_SEARCH_PAGE_INDEX
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.incrementHeaderPageIndex();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('incrementPeoplePageIndex method should dispatch SEARCH_UPDATE_PEOPLE_SEARCH_TEXT action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_INCREMENT_PEOPLE_SEARCH_PAGE_INDEX
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.incrementPeoplePageIndex();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('decrementHeaderPageIndex method should dispatch SEARCH_UPDATE_HEADER_SEARCH_TEXT action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_DECREMENT_HEADER_SEARCH_PAGE_INDEX
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.decrementHeaderPageIndex();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('decrementPeoplePageIndex method should dispatch SEARCH_UPDATE_PEOPLE_SEARCH_TEXT action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_DECREMENT_PEOPLE_SEARCH_PAGE_INDEX
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.decrementPeoplePageIndex();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('resetHeaderSearchState method should dispatch SEARCH_RESET_HEADER_SEARCH_STATE action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_RESET_HEADER_SEARCH_STATE
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.resetHeaderSearchState();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('resetPeopleSearchState method should dispatch SEARCH_RESET_PEOPLE_SEARCH_STATE action', () => {
        // action expected to be dispatched
        const expectedAction = {
            type    : SearchActions.SEARCH_RESET_PEOPLE_SEARCH_STATE
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        searchActions.resetPeopleSearchState();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    });
});
