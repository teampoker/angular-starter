import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NgRedux, select} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../app-store';
import {SearchType} from './types/search-type.model';
import {SearchBoxState} from './types/search-box-state.model';
import {KeyValuePair} from '../types/key-value-pair.model';
import {SearchResultState} from './types/search-result-state.model';
import {PeopleSearchResult} from './types/people-search-result';
import {SearchResultsOrderByConfig} from './types/search-results-order-by-config.model';
import {SearchResultsPaging} from './types/search-results-paging.model';
import {SearchParameters} from './types/search-parameters.model';

@Injectable()

/**
 * implementation for SearchStateSelectors: responsible for exposing custom state subscriptions to SearchState
 */
export class SearchStateSelectors {
    /**
     * SearchStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to SearchState searchTypes
     * @returns {Observable<List<SearchType>>}
     */
    searchTypes() : Observable<List<SearchType>> {
        return this.store.select(state => state.searchState.get('searchTypes'));
    }

    /**
     * expose isSearchActive value
     */
    isHeaderSearchActive() : boolean {
        let isSearchActive : boolean = false;

        this.store.select(state => state.searchState.getIn(['headerSearchState', 'isSearchActive'])).subscribe(value => isSearchActive = value).unsubscribe();

        return isSearchActive;
    }

    /**
     * expose the header fetching indicator as observable
     */
    @select(state => state.searchState.headerSearchState.isFetching)
    isHeaderSearchFetching : Observable<boolean>;

    /**
     * expose isSearchActive value
     */
    isPeopleSearchActive() : boolean {
        let isSearchActive : boolean = false;

        this.store.select(state => state.searchState.getIn(['peopleSearchState', 'isSearchActive'])).subscribe(value => isSearchActive = value).unsubscribe();

        return isSearchActive;
    }

    /**
     * expose the header fetching indicator as observable
     */
    @select(state => state.searchState.peopleSearchState.isFetching)
    isPeopleSearchFetching : Observable<boolean>;

    /**
     * /**
     * expose Observable to SearchState.peopleSearchState.isShowAllActive
     * @returns {Observable<boolean>}
     */
    isPeopleSearchShowAllActive() : Observable<boolean> {
        return this.store.select(state => state.searchState.getIn(['peopleSearchState', 'isShowAllActive']));
    }

    /**
     * expose Observable to SearchState.headerSearchState.searchBoxState.searchText
     * @returns {Observable<string>}
     */
    headerSearchText() : Observable<string> {
        return this.store.select(state => state.searchState.getIn(['headerSearchState', 'searchBoxState', 'searchText']));
    }

    /**
     * expose Observable to SearchState.peopleSearchState.searchBoxState.searchText
     * @returns {Observable<string>}
     */
    peopleSearchText() : Observable<string> {
        return this.store.select(state => state.searchState.getIn(['peopleSearchState', 'searchBoxState', 'searchText']));
    }

    /**
     * expose Observable to SearchState.headerSearchState.searchBoxState
     * @returns {Observable<SearchBoxState>}
     */
    headerSearchBoxState() : Observable<SearchBoxState> {
        return this.store.select(state => state.searchState.getIn(['headerSearchState', 'searchBoxState']));
    }

    /**
     * expose Observable to SearchState.peopleSearchState.searchBoxState
     * @returns {Observable<SearchBoxState>}
     */
    peopleSearchBoxState() : Observable<SearchBoxState> {
        return this.store.select(state => state.searchState.getIn(['peopleSearchState', 'searchBoxState']));
    }

    /**
     * expose Observable to SearchState.headerSearchState.searchBoxState.selectedSearchType
     * @returns {KeyValuePair}
     */
    selectedHeaderSearchType() : KeyValuePair {
        let selectedSearchType  : any = KeyValuePair;

        // get selected beneficiary search type
        this.store.select(state => state.searchState.getIn(['headerSearchState', 'searchBoxState', 'selectedSearchType'])).first().subscribe(value => selectedSearchType = value);

        return selectedSearchType;
    }

    /**
     * expose Observable to SearchState.headerSearchState.searchResultsState
     * @returns {Observable<List<SearchResultState>>}
     */
    headerSearchResults() : Observable<List<SearchResultState>> {
        return this.store.select(state => state.searchState.getIn(['headerSearchState', 'searchResultsState']));
    }

    /**
     * expose Observable to SearchState.peopleSearchState.searchResults
     * @returns {Observable<List<PeopleSearchResult>>}
     */
    peopleSearchResults() : Observable<List<PeopleSearchResult>> {
        return this.store.select(state => state.searchState.getIn(['peopleSearchState', 'searchResults']));
    }

    /**
     * expose Observable to SearchState.headerSearchState.orderByConfig
     * @returns {Observable<SearchResultsOrderByConfig>}
     */
    headerSearchOrderByConfig() : Observable<SearchResultsOrderByConfig> {
        return this.store.select(state => state.searchState.getIn(['headerSearchState', 'orderByConfig']));
    }

    /**
     * expose Observable to SearchState.peopleSearchState.peopleSearchOrderByConfig
     * @returns {Observable<SearchResultsOrderByConfig>}
     */
    peopleSearchOrderByConfig() : Observable<SearchResultsOrderByConfig> {
        return this.store.select(state => state.searchState.getIn(['peopleSearchState', 'orderByConfig']));
    }

    /**
     * expose Observable to SearchState.headerSearchState.searchResultsPaging
     * @returns {Observable<SearchResultsPaging>}
     */
    headerSearchResultsPaging() : Observable<SearchResultsPaging> {
        return this.store.select(state => state.searchState.getIn(['headerSearchState', 'searchResultsPaging']));
    }

    /**
     * expose Observable to SearchState.peopleSearchState.searchResultsPaging
     * @returns {Observable<SearchResultsPaging>}
     */
    peopleSearchResultsPaging() : Observable<SearchResultsPaging> {
        return this.store.select(state => state.searchState.getIn(['peopleSearchState', 'searchResultsPaging']));
    }

    /**
     * expose Observable to SearchState.headerSearchState.selectedPageSize
     * @returns {Observable<KeyValuePair>}
     */
    headerSearchSelectedPageSize() : Observable<KeyValuePair> {
        return this.store.select(state => state.searchState.getIn(['headerSearchState', 'selectedPageSize']));
    }

    /**
     * expose Observable to SearchState.peopleSearchState.peopleSearchSelectedPageSize
     * @returns {Observable<KeyValuePair>}
     */
    peopleSearchSelectedPageSize() : Observable<KeyValuePair> {
        return this.store.select(state => state.searchState.getIn(['peopleSearchState', 'selectedPageSize']));
    }

    /**
     * expose Observable to SearchState.headerSearchState.searchResultsPaging.pageSizeOptions
     * @returns {Observable<List<KeyValuePair>>}
     */
    headerSearchPageSizeOptions() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.searchState.getIn(['headerSearchState', 'searchResultsPaging', 'pageSizeOptions']));
    }

    /**
     * expose Observable to SearchState.headerSearchState.searchResultsPaging.pageSizeOptions
     * @returns {Observable<List<KeyValuePair>>}
     */
    peopleSearchPageSizeOptions() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.searchState.getIn(['peopleSearchState', 'searchResultsPaging', 'pageSizeOptions']));
    }

    /**
     * grab all the search parameter stuff needed for getHeaderSearchResults method
     * @returns {SearchParameters}
     */
    headerSearchParameters() : SearchParameters {
        let searchParams : SearchParameters = new SearchParameters();

        this.headerSearchText().first().subscribe(value => searchParams = searchParams.set('searchText', value) as SearchParameters);
        this.headerSearchResultsPaging().first().subscribe(value => searchParams = searchParams.set('pageOffset', value.get('pageOffset')) as SearchParameters);
        this.headerSearchSelectedPageSize().first().subscribe(value => searchParams = searchParams.set('pageSize', value.get('value')) as SearchParameters);

        this.headerSearchOrderByConfig().subscribe(value => {
            searchParams = searchParams.withMutations(record => record
                .set('sortColumn', value.get('sortColumn'))
                .set('sortOrder', value.get('sortOrder'))) as SearchParameters;
        });

        return searchParams;
    }

    /**
     * grab all the search parameter stuff needed for getPeopleSearchResults method
     * @returns {SearchParameters}
     */
    peopleSearchParameters() : SearchParameters {
        let searchParams : SearchParameters = new SearchParameters();

        this.peopleSearchText().first().subscribe(value => searchParams = searchParams.set('searchText', value) as SearchParameters);
        this.peopleSearchResultsPaging().first().subscribe(value => searchParams = searchParams.set('pageOffset', value.get('pageOffset')) as SearchParameters);
        this.peopleSearchSelectedPageSize().first().subscribe(value => searchParams = searchParams.set('pageSize', value.get('value')) as SearchParameters);

        this.peopleSearchOrderByConfig().subscribe(value => {
            searchParams = searchParams.withMutations(record => record
                .set('sortColumn', value.get('sortColumn'))
                .set('sortOrder', value.get('sortOrder'))) as SearchParameters;
        });

        return searchParams;
    }
}
