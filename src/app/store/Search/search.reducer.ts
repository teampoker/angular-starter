import {List} from 'immutable';
import * as moment from 'moment';

import {INITIAL_SEARCH_STATE} from './search.initial-state';
import {IPayloadAction} from '../app-store';
import {SearchState} from './types/search-state.model';
import {SearchActions} from './search.actions';
import {
    SearchResultsPaging,
    fromApi as searchResultsPagingFromApi
} from './types/search-results-paging.model';
import {KeyValuePair} from '../types/key-value-pair.model';
import {SearchResult} from './types/search-result.model';
import {SearchResultState} from './types/search-result-state.model';
import {SearchContactMechanism} from './types/search-contact-mechanism.model';
import {SearchServiceCoverage} from './types/search-service-coverage.model';
import {SearchBeneficiaryPlanState} from './types/search-beneficiary-plan-state.model';
import {SearchServiceCoverageClassification} from './types/search-service-coverage-classification.model';
import {SearchType} from './types/search-type.model';
import {PeopleSearchResult} from './types/people-search-result';

export const SEARCH_STATE_REDUCER = (state : SearchState = INITIAL_SEARCH_STATE, action : IPayloadAction) : SearchState => {
    switch (action.type) {
        case SearchActions.SEARCH_UPDATE_SEARCH_TYPES :
            state = state.set('searchTypes', List<SearchType>(action.payload.map(value => new SearchType(value)))) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS :
            /**
             * Search results have been received at this point.
             */

            // indicate that app is no longer fetching data
            state = state.setIn(['headerSearchState', 'isFetching'], false) as SearchState;

                // store list of matching search results
            state = state.setIn(['headerSearchState', 'searchResults'], List<SearchResult>(action.payload.results.map(value => new SearchResult(value)))) as SearchState;

                // store pagination info
            state = state.setIn(['headerSearchState', 'searchResultsPaging'], updatePagination(action.payload)) as SearchState;

            // update view count options based on total # of results
            state = state.setIn(['headerSearchState', 'searchResultsPaging', 'pageSizeOptions'], updatePageSizeOptions(state.getIn(['headerSearchState', 'searchResultsPaging']))) as SearchState;

            // update view models
            state = populateViewModels(state) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS :
            // store list of matching search results
            state = state.setIn(['peopleSearchState', 'searchResults'], List<PeopleSearchResult>(action.payload.results.map(value => new PeopleSearchResult(value)))) as SearchState;

            // store pagination info
            state = state.setIn(['peopleSearchState', 'searchResultsPaging'], updatePagination(action.payload)) as SearchState;

            // update view count options based on total # of results
            state = state.setIn(['peopleSearchState', 'searchResultsPaging', 'pageSizeOptions'], updatePageSizeOptions(state.getIn(['peopleSearchState', 'searchResultsPaging']))) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_HEADER_SEARCH_TEXT :
            state = state.setIn(['headerSearchState', 'searchBoxState', 'searchText'], action.payload) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_TEXT :
            state = state.setIn(['peopleSearchState', 'searchBoxState', 'searchText'], action.payload) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_HEADER_SEARCH_TYPE :
            state = state.setIn(['headerSearchState', 'searchBoxState', 'selectedSearchType'], action.payload) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_TYPE :
            state = state.setIn(['peopleSearchState', 'searchBoxState', 'selectedSearchType'], action.payload) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_HEADER_PAGE_SIZE :
            state = state.withMutations(record => {
                return record
                    .setIn(['headerSearchState', 'selectedPageSize'], action.payload)
                    // reset the paging back to the first page of results
                    .setIn(['headerSearchState', 'searchResultsPaging', 'pageOffset'], 0)
                    .setIn(['headerSearchState', 'searchResultsPaging', 'pageSize'], action.payload.value);
            }) as SearchState;
            break;
        case SearchActions.SEARCH_UPDATE_PEOPLE_PAGE_SIZE :
            state = state.setIn(['peopleSearchState', 'selectedPageSize'], action.payload) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS_ORDER_BY :
            state = state.setIn(['headerSearchState', 'orderByConfig'], action.payload) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS_ORDER_BY :
            state = state.setIn(['peopleSearchState', 'orderByConfig'], action.payload) as SearchState;

            break;
        case SearchActions.SEARCH_INCREMENT_HEADER_SEARCH_PAGE_INDEX :
            state = state.setIn(['headerSearchState', 'searchResultsPaging', 'pageOffset'], state.getIn(['headerSearchState', 'searchResultsPaging', 'pageOffset']) + 1) as SearchState;

            break;
        case SearchActions.SEARCH_INCREMENT_PEOPLE_SEARCH_PAGE_INDEX :
            state = state.setIn(['peopleSearchState', 'searchResultsPaging', 'pageOffset'], state.getIn(['peopleSearchState', 'searchResultsPaging', 'pageOffset']) + 1) as SearchState;

            break;
        case SearchActions.SEARCH_DECREMENT_HEADER_SEARCH_PAGE_INDEX :
            state = state.setIn(['headerSearchState', 'searchResultsPaging', 'pageOffset'], state.getIn(['headerSearchState', 'searchResultsPaging', 'pageOffset']) - 1) as SearchState;

            break;
        case SearchActions.SEARCH_DECREMENT_PEOPLE_SEARCH_PAGE_INDEX :
            state = state.setIn(['peopleSearchState', 'searchResultsPaging', 'pageOffset'], state.getIn(['peopleSearchState', 'searchResultsPaging', 'pageOffset']) - 1) as SearchState;

            break;
        case SearchActions.SEARCH_TOGGLE_HEADER_SEARCH_ACTIVE :
            state = state.setIn(['headerSearchState', 'isSearchActive'], !state.getIn(['headerSearchState', 'isSearchActive'])) as SearchState;

            break;
        case SearchActions.SEARCH_TOGGLE_PEOPLE_SEARCH_ACTIVE :
            state = state.setIn(['peopleSearchState', 'isSearchActive'], !state.getIn(['peopleSearchState', 'isSearchActive'])) as SearchState;

            break;
        case SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_SHOW_ALL_RESULTS :
            // was a boolean value specified?
            if (action.payload !== undefined) {
                state = state.setIn(['peopleSearchState', 'isShowAllActive'], action.payload) as SearchState;
            }
            else {
                // no supplied value, just toggle it
                state = state.setIn(['peopleSearchState', 'isShowAllActive'], !state.getIn(['peopleSearchState', 'isShowAllActive'])) as SearchState;
            }

            break;
        case SearchActions.SEARCH_RESET_HEADER_SEARCH_STATE :
            state = state.withMutations(record => {
                return record
                    .setIn(['headerSearchState', 'isFetching'], false)
                    .setIn(['headerSearchState', 'isSearchActive'], false)
                    .setIn(['headerSearchState', 'searchBoxState', 'searchText'], '')
                    .setIn(['headerSearchState', 'orderByConfig', 'sortColumn'], 'lastName')
                    .setIn(['headerSearchState', 'orderByConfig', 'sortOrder'], 'asc')
                    .setIn(['headerSearchState', 'searchResultsPaging', 'pageOffset'], 0)
                    .setIn(['headerSearchState', 'searchResultsPaging', 'pageSize'], 10);
            }) as SearchState;

            break;
        case SearchActions.SEARCH_RESET_PEOPLE_SEARCH_STATE :
            state = state.withMutations(record => {
                return record
                    .setIn(['peopleSearchState', 'isFetching'], false)
                    .setIn(['peopleSearchState', 'isSearchActive'], false)
                    .setIn(['peopleSearchState', 'isShowAllActive'], false)
                    .setIn(['peopleSearchState', 'searchBoxState', 'searchText'], '')
                    .setIn(['peopleSearchState', 'orderByConfig', 'sortColumn'], 'lastName')
                    .setIn(['peopleSearchState', 'orderByConfig', 'sortOrder'], 'asc');
            }) as SearchState;

            break;
        case SearchActions.SEARCH_RESET_SEARCH_PARAMETERS :
            state = state.withMutations(record => {
                return record
                    .setIn(['headerSearchState', 'orderByConfig', 'sortColumn'], 'lastName')
                    .setIn(['headerSearchState', 'orderByConfig', 'sortOrder'], 'asc')
                    .setIn(['headerSearchState', 'searchResultsPaging', 'pageOffset'], 0)
                    .setIn(['headerSearchState', 'searchResultsPaging', 'pageSize'], 10);
            }) as SearchState;

            break;
        case SearchActions.SEARCH_HEADER_FETCHING:
            /**
             * store the change in entity state that the search is fetching a resource
             */
            state = state.setIn(['headerSearchState', 'isFetching'], true) as SearchState;

            break;
        case SearchActions.SEARCH_PEOPLE_FETCHING:
            /**
             * store the change in entity state that the search is fetching a resource
             */
            state = state.setIn(['peopleSearchState', 'isFetching'], true) as SearchState;

            break;
        default :
            return state;
    }
    return state;
};

/**
 * updates the current pagination info
 *
 * @param results search results
 */
function updatePagination(results : any) : SearchResultsPaging {
    // did we get any results?
    if (results._paging) {
        // create immutable version of our new model data
        const paging : SearchResultsPaging = searchResultsPagingFromApi(results._paging);

        return paging
            .set('numOfPages', paging.getDisplayedRecords())
            .set('displayedRecords', paging.getTotalNumberOfPages()) as SearchResultsPaging;
    }
    else {
        return new SearchResultsPaging({
            pageOffset          : 0,
            pageSize            : 0,
            totalRecords        : 0,
            numOfPages          : 0,
            displayedRecords    : 0,
            pageSizeOptions     : []
        });
    }
}

/**
 * inspects the pagination configuration data returned from the API and
 * determines the page size options to display to the CSR
 * @param paginationConfig
 * @returns {List<KeyValuePair>}
 */
function updatePageSizeOptions(paginationConfig : SearchResultsPaging) : List<KeyValuePair> {
    /**
     * store page size options here
     */
    let pageSizeOptions : List<KeyValuePair> = List<KeyValuePair>();

    /**
     * grab total # of records returned
     * @type {number}
     */
    const totalRecords : number = paginationConfig.get('totalRecords');

    // always have 10 as an option
    pageSizeOptions = pageSizeOptions.push(new KeyValuePair({
        id      : '1',
        value   : '10'
    }));

    // can we display 25?
    if (totalRecords >= 25) {
        pageSizeOptions = pageSizeOptions.push(new KeyValuePair({
            id      : '2',
            value   : '25'
        }));
    }

    // can we display 50?
    if (totalRecords >= 50) {
        pageSizeOptions = pageSizeOptions.push(new KeyValuePair({
            id      : '3',
            value   : '50'
        }));
    }

    // can we display 75?
    if (totalRecords >= 75) {
        pageSizeOptions = pageSizeOptions.push(new KeyValuePair({
            id      : '4',
            value   : '75'
        }));
    }

    // can we display 100?
    if (totalRecords >= 100) {
        pageSizeOptions = pageSizeOptions.push(new KeyValuePair({
            id      : '5',
            value   : '100'
        }));
    }

    return pageSizeOptions;
}

/**
 * copies raw search results data from the API into a series of view models
 * that are more easily consumed by the UI templates
 *
 * @param state current SearchState
 */
function populateViewModels(state : SearchState) {
    const searchResultsModel : List<SearchResult> = state.getIn(['headerSearchState', 'searchResults']);

    let searchResults : List<SearchResultState> = List<SearchResultState>();

    if (searchResultsModel !== undefined) {
        if (searchResultsModel.size > 0) {
            searchResults = List<SearchResultState>(searchResultsModel.map(value => {
                let addressMechanisms   : List<SearchContactMechanism>  = List<SearchContactMechanism>(),
                    phoneMechanisms     : List<SearchContactMechanism>  = List<SearchContactMechanism>(),
                    serviceCoverages    : List<SearchServiceCoverage>   = List<SearchServiceCoverage>();

                return new SearchResultState().withMutations(record => {
                    // look in contactMechanisms for phone, address info
                    addressMechanisms   = value.get('contactMechanisms').filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'value']) === 'Address').sortBy(address =>  address.get('ordinality'));
                    phoneMechanisms     = value.get('contactMechanisms').filter(mechanism => mechanism.getIn(['contactMechanism', 'type', 'value']) === 'Telecommunications Number').sortBy(phone =>  phone.get('ordinality'));
                    serviceCoverages    = value.get('serviceCoverages').sortBy(coverage =>  coverage.get('ordinality'));

                    return record
                        .set('beneficiaryUUID', value.get('uuid'))
                        .set('beneficiaryName', value.get('firstName') + ' ' + value.get('lastName'))
                        .set('beneficiaryDOB', value.get('birthDate'))
                        .set('beneficiaryAddress', addressMechanisms.size > 0 ? addressMechanisms.get(0).getIn(['contactMechanism', 'address1']) : 'N/A')
                        .set('beneficiaryCity', addressMechanisms.size > 0 ? addressMechanisms.get(0).getIn(['contactMechanism', 'city']) : 'N/A')
                        .set('beneficiaryState', addressMechanisms.size > 0 ? addressMechanisms.get(0).getIn(['contactMechanism', 'state']) : 'N/A')
                        .set('beneficiaryPostalCode', addressMechanisms.size > 0 ? addressMechanisms.get(0).getIn(['contactMechanism', 'postalCode']) : 'N/A')
                        .set('beneficiaryPhone', phoneMechanisms.size > 0 ? phoneMechanisms.get(0).getIn(['contactMechanism', 'areaCode']) + phoneMechanisms.get(0).getIn(['contactMechanism', 'contactNumber']) : 'N/A')
                        .set('beneficiaryPlans', serviceCoverages.map(coverage => new SearchBeneficiaryPlanState().withMutations(coverageRecord => {
                            // see if a classification type of Id is associated with any of the classifications for this coverage plan, if so, the value will be our Member Id
                            const planId : SearchServiceCoverageClassification = coverage.get('classifications').find(classification => classification.getIn(['serviceCoveragePlanClassification', 'serviceCoveragePlanClassificationType', 'name']) === 'Id');

                            return coverageRecord
                                .set('organization', coverage.getIn(['serviceCoveragePlan', 'serviceCoverageOrganization', 'name']) + ' ' + coverage.getIn(['serviceCoveragePlan', 'name']))
                                .set('fromDate', coverage.get('fromDate') ? moment(coverage.get('fromDate')).format('MM/DD/YYYY') : 'N/A')
                                .set('thruDate', coverage.get('thruDate') ? moment(coverage.get('thruDate')).format('MM/DD/YYYY') : 'N/A')
                                .set('planIdLabel', planId ? planId.getIn(['serviceCoveragePlanClassification', 'name']) : 'N/A')
                                .set('planIdValue', planId ? planId.get('value') : 'N/A');
                        })));
                }) as SearchResultState;
            }));
        }
    }

    // update beneficiary demographics
    return state.setIn(['headerSearchState', 'searchResultsState'], searchResults) as SearchState;
}
