import {INITIAL_SEARCH_STATE} from './search.initial-state';
import {SEARCH_STATE_REDUCER} from './search.reducer';
import {SearchActions} from './search.actions';
import {SEARCH_MOCK} from '../../shared/services/Search/search.service.mocks';
import {SearchState} from './types/search-state.model';
import {PeopleSearchResult} from './types/people-search-result';
import {KeyValuePair} from '../types/key-value-pair.model';
import {SearchResultsOrderByConfig} from './types/search-results-order-by-config.model';
import {SearchType} from './types/search-type.model';
import {SearchResult} from './types/search-result.model';

describe('Search State Reducer', () => {
    it('should init initial state', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : undefined,
                payload : undefined
            })
        )
            .toEqual(INITIAL_SEARCH_STATE);
    });

    it('should handle SEARCH_UPDATE_SEARCH_TYPES action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_SEARCH_TYPES,
                payload : SEARCH_MOCK.getSearchTypes
            })
                .getIn(['searchTypes', 0]) instanceof SearchType
        )
            .toEqual(true);
    });

    it('should extract Search Types from SEARCH_UPDATE_SEARCH_TYPES action payload if present', () => {
        let state : SearchState;

        state = SEARCH_STATE_REDUCER(undefined, {
            type    : SearchActions.SEARCH_UPDATE_SEARCH_TYPES,
            payload : SEARCH_MOCK.getSearchTypes
        });

        expect(state.getIn(['searchTypes', 0, 'name'])).toEqual(SEARCH_MOCK.getSearchTypes[0].name);
        expect(state.getIn(['searchTypes', 1, 'name'])).toEqual(SEARCH_MOCK.getSearchTypes[1].name);
    });

    it('should handle SEARCH_UPDATE_HEADER_SEARCH_RESULTS action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS,
                payload : SEARCH_MOCK.getHeaderSearchResults[0]
            })
                .getIn(['headerSearchState', 'searchResults', 0]) instanceof SearchResult
        )
            .toEqual(true);
    });

    xit('should extract Search Results from SEARCH_UPDATE_HEADER_SEARCH_RESULTS action payload if present', () => {
        let state : SearchState;

        state = SEARCH_STATE_REDUCER(undefined, {
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS,
            payload : SEARCH_MOCK.getHeaderSearchResults[0]
        });

        expect(state.getIn(['headerSearchState', 'searchResults', 'results', 0]) instanceof PeopleSearchResult).toEqual(true);

    });

    it('should extract Pagination info from SEARCH_UPDATE_HEADER_SEARCH_RESULTS action payload if present', () => {
        let state : SearchState;

        state = SEARCH_STATE_REDUCER(undefined, {
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS,
            payload : SEARCH_MOCK.getHeaderSearchResults[0]
        });

        expect(state.getIn(['headerSearchState', 'searchResultsPaging', 'pageOffset'])).toEqual(SEARCH_MOCK.getHeaderSearchResults[0]._paging.page);
        expect(state.getIn(['headerSearchState', 'searchResultsPaging', 'pageSize'])).toEqual(SEARCH_MOCK.getHeaderSearchResults[0]._paging.size);
        expect(state.getIn(['headerSearchState', 'searchResultsPaging', 'totalRecords'])).toEqual(SEARCH_MOCK.getHeaderSearchResults[0]._paging.total);
    });

    it('should set available pageSizeOptions from SEARCH_UPDATE_HEADER_SEARCH_RESULTS action payload if present', () => {
        let state : SearchState;

        state = SEARCH_STATE_REDUCER(undefined, {
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS,
            payload : SEARCH_MOCK.getHeaderSearchResults[0]
        });

        expect(state.getIn(['headerSearchState', 'searchResultsPaging', 'pageSizeOptions']).size).toBeGreaterThan(0);
    });

    xit('should extract Search Results from SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS action payload if present', () => {
        let state : SearchState;

        state = SEARCH_STATE_REDUCER(undefined, {
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS,
            payload : SEARCH_MOCK.getPeopleSearchResults[0]
        });

        expect(state.getIn(['peopleSearchState', 'searchResults', 'results', 0]) instanceof PeopleSearchResult).toEqual(true);

    });

    it('should set available pageSizeOptions from SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS action payload if present', () => {
        let state : SearchState;

        state = SEARCH_STATE_REDUCER(undefined, {
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS,
            payload : SEARCH_MOCK.getHeaderSearchResults[0]
        });

        expect(state.getIn(['headerSearchState', 'searchResultsPaging', 'pageSizeOptions']).size).toBeGreaterThan(0);
    });

    it('should handle SEARCH_UPDATE_HEADER_SEARCH_TEXT action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_TEXT,
                payload : 'search text'
            })
                .getIn(['headerSearchState', 'searchBoxState', 'searchText'])
        )
            .toEqual('search text');
    });

    it('should handle SEARCH_UPDATE_PEOPLE_SEARCH_TEXT action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_TEXT,
                payload : 'search text'
            })
                .getIn(['peopleSearchState', 'searchBoxState', 'searchText'])
        )
            .toEqual('search text');
    });

    it('should handle SEARCH_UPDATE_HEADER_SEARCH_TYPE action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_TYPE,
                payload : new KeyValuePair({ id : '2', value : 'BENEFICIARY' })
            })
                .getIn(['headerSearchState', 'searchBoxState', 'selectedSearchType', 'value'])
        )
            .toEqual('BENEFICIARY');
    });

    it('should handle SEARCH_UPDATE_PEOPLE_SEARCH_TYPE action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_TYPE,
                payload : new KeyValuePair({ id : '2', value : 'PEOPLE' })
            })
                .getIn(['peopleSearchState', 'searchBoxState', 'selectedSearchType', 'value'])
        )
            .toEqual('PEOPLE');
    });

    it('should handle SEARCH_UPDATE_HEADER_PAGE_SIZE action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_HEADER_PAGE_SIZE,
                payload : new KeyValuePair({ id : '1', value : '10' })
            })
                .getIn(['headerSearchState', 'selectedPageSize', 'value'])
        )
            .toEqual('10');
    });

    it('should handle SEARCH_UPDATE_PEOPLE_PAGE_SIZE action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_PEOPLE_PAGE_SIZE,
                payload : new KeyValuePair({ id : '1', value : '10' })
            })
                .getIn(['peopleSearchState', 'selectedPageSize', 'value'])
        )
            .toEqual('10');
    });

    it('should handle SEARCH_UPDATE_HEADER_SEARCH_RESULTS_ORDER_BY action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS_ORDER_BY,
                payload : new SearchResultsOrderByConfig()
            })
                .getIn(['headerSearchState', 'orderByConfig'])
        )
            .toEqual(new SearchResultsOrderByConfig());
    });

    it('should handle SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS_ORDER_BY action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS_ORDER_BY,
                payload : new SearchResultsOrderByConfig()
            })
                .getIn(['peopleSearchState', 'orderByConfig'])
        )
            .toEqual(new SearchResultsOrderByConfig());
    });

    it('should handle SEARCH_INCREMENT_HEADER_SEARCH_PAGE_INDEX action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type : SearchActions.SEARCH_INCREMENT_HEADER_SEARCH_PAGE_INDEX
            })
                .getIn(['headerSearchState', 'searchResultsPaging', 'pageOffset'])
        )
            .toEqual(1);
    });

    it('should handle SEARCH_INCREMENT_PEOPLE_SEARCH_PAGE_INDEX action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type : SearchActions.SEARCH_INCREMENT_PEOPLE_SEARCH_PAGE_INDEX
            })
                .getIn(['peopleSearchState', 'searchResultsPaging', 'pageOffset'])
        )
            .toEqual(1);
    });

    it('should handle SEARCH_DECREMENT_HEADER_SEARCH_PAGE_INDEX action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type : SearchActions.SEARCH_DECREMENT_HEADER_SEARCH_PAGE_INDEX
            })
                .getIn(['headerSearchState', 'searchResultsPaging', 'pageOffset'])
        )
            .toEqual(-1);
    });

    it('should handle SEARCH_DECREMENT_PEOPLE_SEARCH_PAGE_INDEX action', () => {
        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type : SearchActions.SEARCH_DECREMENT_PEOPLE_SEARCH_PAGE_INDEX
            })
                .getIn(['peopleSearchState', 'searchResultsPaging', 'pageOffset'])
        )
            .toEqual(-1);
    });

    it('should handle SEARCH_TOGGLE_HEADER_SEARCH_ACTIVE action', () => {
        const isSearchActive = SEARCH_STATE_REDUCER(undefined, {
            type    : undefined,
            payload : undefined
        }).getIn(['headerSearchState', 'isSearchActive']);

        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type : SearchActions.SEARCH_TOGGLE_HEADER_SEARCH_ACTIVE
            })
                .getIn(['headerSearchState', 'isSearchActive'])
        )
            .toEqual(!isSearchActive);
    });

    it('should handle SEARCH_TOGGLE_PEOPLE_SEARCH_ACTIVE action', () => {
        const isSearchActive = SEARCH_STATE_REDUCER(undefined, {
            type    : undefined,
            payload : undefined
        }).getIn(['peopleSearchState', 'isSearchActive']);

        expect(
            SEARCH_STATE_REDUCER(undefined, {
                type : SearchActions.SEARCH_TOGGLE_PEOPLE_SEARCH_ACTIVE
            })
                .getIn(['peopleSearchState', 'isSearchActive'])
        )
            .toEqual(!isSearchActive);
    });

    it('should handle SEARCH_RESET_HEADER_SEARCH_STATE action', () => {
        const state = SEARCH_STATE_REDUCER(undefined, {
            type : SearchActions.SEARCH_RESET_HEADER_SEARCH_STATE
        })
            .get('headerSearchState');

        expect(state.get('isSearchActive')).toEqual(false);
        expect(state.getIn(['searchBoxState', 'searchText'])).toEqual('');
        expect(state.getIn(['orderByConfig', 'sortColumn'])).toEqual('lastName');
        expect(state.getIn(['orderByConfig', 'sortOrder'])).toEqual('asc');
    });

    it('should handle SEARCH_RESET_PEOPLE_SEARCH_STATE action', () => {
        const state = SEARCH_STATE_REDUCER(undefined, {
            type : SearchActions.SEARCH_RESET_PEOPLE_SEARCH_STATE
        })
            .get('peopleSearchState');

        expect(state.get('isSearchActive')).toEqual(false);
        expect(state.getIn(['searchBoxState', 'searchText'])).toEqual('');
        expect(state.getIn(['orderByConfig', 'sortColumn'])).toEqual('lastName');
        expect(state.getIn(['orderByConfig', 'sortOrder'])).toEqual('asc');
    });
});
