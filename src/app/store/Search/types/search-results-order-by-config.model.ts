import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ISearchResultsOrderByConfig {
    sortColumn   : string;
    sortOrder    : string;
}

export const SEARCH_RESULTS_ORDER_BY_CONFIG = Record({
    sortColumn   : '',
    sortOrder    : 'asc'
});

export class SearchResultsOrderByConfig extends SEARCH_RESULTS_ORDER_BY_CONFIG {
    sortColumn   : string;
    sortOrder    : string;

    constructor(values? : SearchResultsOrderByConfig | ISearchResultsOrderByConfig) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchResultsOrderByConfig) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
