import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    ISearchResultsOrderByConfig,
    SearchResultsOrderByConfig
} from './search-results-order-by-config.model';
import {
    ISearchResultsPaging,
    SearchResultsPaging
} from './search-results-paging.model';
import {
    ISearchBoxState,
    SearchBoxState
} from './search-box-state.model';
import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';
import {
    ISearchResultState,
    SearchResultState
} from './search-result-state.model';
import {
    ISearchResult,
    SearchResult
} from './search-result.model';

export interface IHeaderSearchState {
    isFetching          : boolean;
    isSearchActive      : boolean;
    searchBoxState      : ISearchBoxState;
    searchResults       : Array<ISearchResult>;
    searchResultsState  : Array<ISearchResultState>;
    searchResultsPaging : ISearchResultsPaging;
    selectedPageSize    : IKeyValuePair;
    orderByConfig       : ISearchResultsOrderByConfig;
}

export const HEADER_SEARCH_STATE = Record({
    isFetching          : false,
    isSearchActive      : false,
    searchBoxState      : new SearchBoxState(),
    searchResults       : List<SearchResult>(),
    searchResultsState  : List<SearchResultState>(),
    searchResultsPaging : new SearchResultsPaging(),
    selectedPageSize    : new KeyValuePair(),
    orderByConfig       : new SearchResultsOrderByConfig()
});

export class HeaderSearchState extends HEADER_SEARCH_STATE {
    isFetching          : boolean;
    isSearchActive      : boolean;
    searchBoxState      : SearchBoxState;
    searchResults       : List<SearchResult>;
    searchResultsState  : List<SearchResultState>;
    searchResultsPaging : SearchResultsPaging;
    selectedPageSize    : KeyValuePair;
    orderByConfig       : SearchResultsOrderByConfig;

    constructor(values? : HeaderSearchState | IHeaderSearchState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof HeaderSearchState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // searchBoxState
                convertedValues = convertedValues.set('searchBoxState', new SearchBoxState(convertedValues.get('searchBoxState')));

                // searchResults
                convertedValues = convertedValues.set('searchResults', List(convertedValues.get('searchResults', []).map(value => new SearchResult(value))));

                // searchResultsState
                convertedValues = convertedValues.set('searchResultsState', List(convertedValues.get('searchResultsState', []).map(value => new SearchResultState(value))));

                // searchResultsPaging
                convertedValues = convertedValues.set('searchResultsPaging', new SearchResultsPaging(convertedValues.get('searchResultsPaging')));

                // selectedPageSize
                convertedValues = convertedValues.set('selectedPageSize', new KeyValuePair(convertedValues.get('selectedPageSize')));

                // orderByConfig
                convertedValues = convertedValues.set('orderByConfig', new SearchResultsOrderByConfig(convertedValues.get('orderByConfig')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
