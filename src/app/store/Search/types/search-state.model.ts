import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IHeaderSearchState,
    HeaderSearchState
} from './header-search-state.model';
import {
    IPeopleSearchState,
    PeopleSearchState
} from './people-search-state.model';
import {
    ISearchType,
    SearchType
} from './search-type.model';

export interface ISearchState {
    searchTypes         : Array<ISearchType>;
    headerSearchState   : IHeaderSearchState;
    peopleSearchState   : IPeopleSearchState;
}

export const SEARCH_STATE = Record({
    searchTypes         : List<SearchType>(),
    headerSearchState   : new HeaderSearchState(),
    peopleSearchState   : new PeopleSearchState()
});

/**
 * type definition for Redux Store search state
 */
export class SearchState extends SEARCH_STATE {
    searchTypes         : List<SearchType>;
    headerSearchState   : HeaderSearchState;
    peopleSearchState   : PeopleSearchState;

    constructor(values? : SearchState | ISearchState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // searchTypes
                convertedValues = convertedValues.set('searchTypes', List(convertedValues.get('searchTypes', []).map(value => new SearchType(value))));

                // headerSearchState
                convertedValues = convertedValues.set('headerSearchState', new HeaderSearchState(convertedValues.get('headerSearchState')));

                // peopleSearchState
                convertedValues = convertedValues.set('peopleSearchState', new PeopleSearchState(convertedValues.get('peopleSearchState')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
