import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    ISearchContactMechanismDetail,
    SearchContactMechanismDetail
} from './search-contact-mechanism-detail.model';
import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface ISearchContactMechanism {
    personUuid                  : string;
    ordinality                  : number;
    contactMechanism            : ISearchContactMechanismDetail;
    contactMechanismPurposeType : IKeyValuePair;
}

export const SEARCH_CONTACT_MECHANISM = Record({
    personUuid                  : '',
    ordinality                  : 0,
    contactMechanism            : new SearchContactMechanismDetail(),
    contactMechanismPurposeType : new KeyValuePair()
});

export class SearchContactMechanism extends SEARCH_CONTACT_MECHANISM {
    personUuid                  : string;
    ordinality                  : number;
    contactMechanism            : SearchContactMechanismDetail;
    contactMechanismPurposeType : KeyValuePair;

    constructor(values? : SearchContactMechanism | ISearchContactMechanism) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchContactMechanism) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // contactMechanism
                convertedValues = convertedValues.set('contactMechanism', new SearchContactMechanismDetail(convertedValues.get('contactMechanism')));

                // contactMechanismPurposeType
                if (convertedValues.getIn(['contactMechanismPurposeType', 'uuid'])) {
                    convertedValues = convertedValues.set('contactMechanismPurposeType', KeyValuePair.fromApi(convertedValues.get('contactMechanismPurposeType')));
                }
                else {
                    convertedValues = convertedValues.set('contactMechanismPurposeType', new KeyValuePair(convertedValues.get('contactMechanismPurposeType')));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
