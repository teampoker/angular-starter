import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    ISearchServiceCoverageOrganization,
    SearchServiceCoverageOrganization
} from './search-service-coverage-organization.model';

export interface ISearchServiceCoveragePlan {
    uuid                        : string;
    name                        : string;
    serviceCoverageOrganization : ISearchServiceCoverageOrganization;
}

export const SEARCH_SERVICE_COVERAGE_PLAN = Record({
    uuid                        : undefined,
    name                        : '',
    serviceCoverageOrganization : new SearchServiceCoverageOrganization()
});

export class SearchServiceCoveragePlan extends SEARCH_SERVICE_COVERAGE_PLAN {
    uuid                        : string;
    name                        : string;
    serviceCoverageOrganization : SearchServiceCoverageOrganization;

    constructor(values? : SearchServiceCoveragePlan | ISearchServiceCoveragePlan) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchServiceCoveragePlan) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoverageOrganization
                convertedValues = convertedValues.set('serviceCoverageOrganization', new SearchServiceCoverageOrganization(convertedValues.get('serviceCoverageOrganization')));

            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
