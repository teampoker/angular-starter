import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ISearchParameters {
    searchText  : string;
    pageSize    : string;
    pageOffset  : number;
    sortColumn  : string;
    sortOrder   : string;
}

export const SEARCH_PARAMETERS = Record({
    searchText  : '',
    pageSize    : '',
    pageOffset  : 0,
    sortColumn  : '',
    sortOrder   : ''
});

export class SearchParameters extends SEARCH_PARAMETERS {
    searchText  : string;
    pageSize    : string;
    pageOffset  : number;
    sortColumn  : string;
    sortOrder   : string;

    constructor(values? : SearchParameters | ISearchParameters) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchParameters) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
