import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface ISearchBoxState {
    searchText           : string;
    selectedSearchType   : IKeyValuePair;
    searchTypeOptions    : Array<IKeyValuePair>;
}

export const SEARCH_BOX_STATE = Record({
    searchText           : '',
    selectedSearchType   : new KeyValuePair(),
    searchTypeOptions    : List<KeyValuePair>()
});

export class SearchBoxState extends SEARCH_BOX_STATE {
    searchText           : string;
    selectedSearchType   : KeyValuePair;
    searchTypeOptions    : List<KeyValuePair>;

    constructor(values? : SearchBoxState | ISearchBoxState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchBoxState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // selectedSearchType
                convertedValues = convertedValues.set('selectedSearchType', new KeyValuePair(convertedValues.get('selectedSearchType')));

                // searchTypeOptions
                convertedValues = convertedValues.set('searchTypeOptions', List(convertedValues.get('searchTypeOptions', []).map(value => new KeyValuePair(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
