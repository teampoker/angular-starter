import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface ISearchContactMechanismDetail {
    uuid                        : string;
    contactNumber?              : string;
    countryCode?                : string;
    areaCode?                   : string;
    electronicAddressString?    : string;
    address1?                   : string;
    address2?                   : string;
    address3?                   : string;
    city?                       : string;
    state?                      : string;
    postalCode?                 : string;
    directions?                 : string;
    type                        : IKeyValuePair;
}

export const SEARCH_CONTACT_MECHANISM_DETAIL = Record({
    uuid                        : undefined,
    contactNumber               : '',
    countryCode                 : '',
    areaCode                    : '',
    electronicAddressString     : '',
    address1                    : '',
    address2                    : '',
    address3                    : '',
    city                        : '',
    state                       : '',
    postalCode                  : '',
    directions                  : '',
    type                        : new KeyValuePair()
});

export class SearchContactMechanismDetail extends SEARCH_CONTACT_MECHANISM_DETAIL {
    uuid                        : string;
    contactNumber?              : string;
    countryCode?                : string;
    areaCode?                   : string;
    electronicAddressString?    : string;
    address1?                   : string;
    address2?                   : string;
    address3?                   : string;
    city?                       : string;
    state?                      : string;
    postalCode?                 : string;
    directions?                 : string;
    type                        : KeyValuePair;

    constructor(values? : SearchContactMechanismDetail | ISearchContactMechanismDetail) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchContactMechanismDetail) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // contactMechanismType
                if (convertedValues.getIn(['type', 'uuid'])) {
                    convertedValues = convertedValues.set('type', KeyValuePair.fromApi(convertedValues.get('type')));
                }
                else {
                    convertedValues = convertedValues.set('type', new KeyValuePair(convertedValues.get('type')));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
