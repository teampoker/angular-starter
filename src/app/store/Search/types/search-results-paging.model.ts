import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface ISearchResultsPaging {
    pageOffset          : number;                   // current active page
    pageSize            : number;                   // selected # of items to display per page
    totalRecords        : number;                   // total # of records returned
    numOfPages          : number;                   // total # of pages of records returned
    displayedRecords    : number;                   // current # of records being displayed on current active page
    pageSizeOptions     : Array<IKeyValuePair>;
}

export const SEARCH_RESULTS_PAGING = Record({
    pageOffset          : 0,
    pageSize            : 0,
    totalRecords        : 0,
    numOfPages          : 0,
    displayedRecords    : 0,
    pageSizeOptions     : List<KeyValuePair>()
});

const apiMap = Map({
    page    : 'pageOffset',
    size    : 'pageSize',
    total   : 'totalRecords'
});

export class SearchResultsPaging extends SEARCH_RESULTS_PAGING {
    pageOffset          : number;
    pageSize            : number;
    totalRecords        : number;
    numOfPages          : number;
    displayedRecords    : number;
    pageSizeOptions     : List<KeyValuePair>;

    constructor(values? : SearchResultsPaging | ISearchResultsPaging) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchResultsPaging) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // pageSizeOptions
                if (convertedValues.get('pageSizeOptions')) {
                    convertedValues = convertedValues.set('pageSizeOptions', List(convertedValues.get('pageSizeOptions', []).map(value => new KeyValuePair(value))));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }

    /**
     * The total number of records intended to be viewed or displayed to user.
     * @returns {number}
     */
    getDisplayedRecords() : number {
        const startItem : number = (this.pageOffset * this.pageSize) + 1;

        if ((this.totalRecords - startItem) >= this.pageSize) {
            // we intend to display a full page of results
            return this.pageSize;
        }
        else {
            // the remaining few items are left to display
            return this.totalRecords % this.pageSize;
        }
    }

    /**
     * Answers the age-old question: how many total pages of results are there in this pagination exercise?
     * @returns {number} 1 if the total number of records is less than the page limit; otherwise the number of pages
     */
    getTotalNumberOfPages() : number {
        if (this.totalRecords > this.pageSize) {
            // rounding up to the next whole number should always give us the num of pages needed to hold results
            return Math.ceil(this.totalRecords / this.pageSize);
        }
        else {
            // minimum of 1 page is always needed
            return 1;
        }
    }
}

/**
 * converts the raw JSON included with the search results API response into a SearchResultsPaging Immutable Record
 * @param rawObject
 * @returns {SearchResultsPaging}
 */
export function fromApi(rawObject : any) : SearchResultsPaging {
    const newObject : any = {};

    apiMap.forEach((translatedKey, apiKey) => {
        newObject[translatedKey] = rawObject[apiKey];
    });

    return new SearchResultsPaging(newObject);
}
