import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ISearchType {
    name : string;
}

export const SEARCH_TYPE = Record({
    name : ''
});

export class SearchType extends SEARCH_TYPE {
    name : string;

    constructor(values? : SearchType | ISearchType) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchType) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
