import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    ISearchBoxState,
    SearchBoxState
} from './search-box-state.model';
import {
    ISearchResultsPaging,
    SearchResultsPaging
} from './search-results-paging.model';
import {
    ISearchResultsOrderByConfig,
    SearchResultsOrderByConfig
} from './search-results-order-by-config.model';
import {
    IPeopleSearchResult,
    PeopleSearchResult
} from './people-search-result';
import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IPeopleSearchState {
    isFetching          : boolean;
    isSearchActive      : boolean;
    isShowAllActive     : boolean;
    searchBoxState      : ISearchBoxState;
    searchResults       : Array<IPeopleSearchResult>;
    searchResultsPaging : ISearchResultsPaging;
    selectedPageSize    : IKeyValuePair;
    orderByConfig       : ISearchResultsOrderByConfig;
}

export const PEOPLE_SEARCH_STATE = Record({
    isFetching          : false,
    isSearchActive      : false,
    isShowAllActive     : false,
    searchBoxState      : new SearchBoxState(),
    searchResults       : List<PeopleSearchResult>(),
    searchResultsPaging : new SearchResultsPaging(),
    selectedPageSize    : new KeyValuePair(),
    orderByConfig       : new SearchResultsOrderByConfig()
});

/**
 * type definition for Redux Store people state
 */
export class PeopleSearchState extends PEOPLE_SEARCH_STATE {
    isFetching          : boolean;
    isSearchActive      : boolean;
    isShowAllActive     : boolean;
    searchBoxState      : SearchBoxState;
    searchResults       : List<PeopleSearchResult>;
    searchResultsPaging : SearchResultsPaging;
    selectedPageSize    : KeyValuePair;
    orderByConfig       : SearchResultsOrderByConfig;

    constructor(values? : PeopleSearchState | IPeopleSearchState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof PeopleSearchState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // searchBoxState
                convertedValues = convertedValues.set('searchBoxState', new SearchBoxState(convertedValues.get('searchBoxState')));

                // searchResults
                convertedValues = convertedValues.set('searchResults', List(convertedValues.get('searchResults', []).map(value => new PeopleSearchResult(value))));

                // searchResultsPaging
                convertedValues = convertedValues.set('searchResultsPaging', new SearchResultsPaging(convertedValues.get('searchResultsPaging')));

                // selectedPageSize
                convertedValues = convertedValues.set('selectedPageSize', new KeyValuePair(convertedValues.get('selectedPageSize')));

                // orderByConfig
                convertedValues = convertedValues.set('orderByConfig', new SearchResultsOrderByConfig(convertedValues.get('orderByConfig')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
