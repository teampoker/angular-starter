import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IPeopleSearchResult {
    uuid                    : string;
    firstName               : string;
    lastName                : string;
    phone                   : string;
    address                 : string;
    score                   : number;
}

export const PEOPLE_SEARCH_RESULT = Record({
    uuid                    : undefined,
    firstName               : '',
    lastName                : '',
    phone                   : '',
    address                 : '',
    score                   : 0
});

export class PeopleSearchResult extends PEOPLE_SEARCH_RESULT {
    uuid                    : string;
    firstName               : string;
    lastName                : string;
    phone                   : string;
    address                 : string;
    score                   : number;

    constructor(values? : PeopleSearchResult | IPeopleSearchResult) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof PeopleSearchResult) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
