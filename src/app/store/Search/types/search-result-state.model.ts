import {
    fromJS,
    Map,
    Record,
    List
} from 'immutable';

import {
    ISearchBeneficiaryPlanState,
    SearchBeneficiaryPlanState
} from './search-beneficiary-plan-state.model';

export interface ISearchResultState {
    beneficiaryUUID         : string;
    beneficiaryName         : string;
    beneficiaryDOB          : string;
    beneficiaryAddress      : string;
    beneficiaryCity         : string;
    beneficiaryState        : string;
    beneficiaryPostalCode   : string;
    beneficiaryPhone        : string;
    beneficiaryPlans        : Array<ISearchBeneficiaryPlanState>;
}

export const SEARCH_RESULT_STATE = Record({
    beneficiaryUUID         : '',
    beneficiaryName         : '',
    beneficiaryDOB          : '',
    beneficiaryAddress      : '',
    beneficiaryCity         : '',
    beneficiaryState        : '',
    beneficiaryPostalCode   : '',
    beneficiaryPhone        : '',
    beneficiaryPlans        : List<SearchBeneficiaryPlanState>()
});

export class SearchResultState extends SEARCH_RESULT_STATE {
    beneficiaryUUID         : string;
    beneficiaryName         : string;
    beneficiaryDOB          : string;
    beneficiaryAddress      : string;
    beneficiaryCity         : string;
    beneficiaryState        : string;
    beneficiaryPostalCode   : string;
    beneficiaryPhone        : string;
    beneficiaryPlans        : List<SearchBeneficiaryPlanState>;

    constructor(values? : SearchResultState | ISearchResultState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchResultState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // beneficiaryPlan
                convertedValues = convertedValues.set('beneficiaryPlans', List(convertedValues.get('beneficiaryPlans', []).map(value => new SearchBeneficiaryPlanState(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
