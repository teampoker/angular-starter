import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ISearchServiceCoverageOrganization {
    uuid                        : string;
    name                        : string;
}

export const SEARCH_SERVICE_COVERAGE_ORGANIZATION = Record({
    uuid                        : undefined,
    name                        : ''
});

export class SearchServiceCoverageOrganization extends SEARCH_SERVICE_COVERAGE_ORGANIZATION {
    uuid                        : string;
    name                        : string;

    constructor(values? : SearchServiceCoverageOrganization | ISearchServiceCoverageOrganization) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchServiceCoverageOrganization) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
