import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ISearchBeneficiaryPlanState {
    organization        : string;
    planIdLabel         : string;
    planIdValue         : string;
    fromDate            : string;
    thruDate            : string;
}

export const SEARCH_BENEFICIARY_PLAN_STATE = Record({
    organization        : '',
    planIdLabel         : '',
    planIdValue         : '',
    fromDate            : '',
    thruDate            : ''
});

export class SearchBeneficiaryPlanState extends SEARCH_BENEFICIARY_PLAN_STATE {
    organization        : string;
    planIdLabel         : string;
    planIdValue         : string;
    fromDate            : string;
    thruDate            : string;

    constructor(values? : SearchBeneficiaryPlanState | ISearchBeneficiaryPlanState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchBeneficiaryPlanState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
