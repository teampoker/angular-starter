import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    INameUuid,
    NameUuid
} from '../../types/name-uuid.model';

export interface ISearchServiceCoveragePlanClassification {
    uuid                                    : string;
    name                                    : string;
    serviceCoveragePlanClassificationType   : INameUuid;
}

export const SEARCH_SERVICE_COVERAGE_PLAN_CLASSIFICATION = Record({
    uuid                                    : undefined,
    name                                    : '',
    serviceCoveragePlanClassificationType   : new NameUuid()
});

export class SearchServiceCoveragePlanClassification extends SEARCH_SERVICE_COVERAGE_PLAN_CLASSIFICATION {
    uuid                                    : string;
    name                                    : string;
    serviceCoveragePlanClassificationType   : NameUuid;

    constructor(values? : SearchServiceCoveragePlanClassification | ISearchServiceCoveragePlanClassification) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchServiceCoveragePlanClassification) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoveragePlanClassification
                convertedValues = convertedValues.set('serviceCoveragePlanClassificationType', new NameUuid(convertedValues.get('serviceCoveragePlanClassificationType')));

            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
