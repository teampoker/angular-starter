import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    ISearchContactMechanism,
    SearchContactMechanism
} from './search-contact-mechanism.model';
import {
    ISearchServiceCoverage,
    SearchServiceCoverage
} from './search-service-coverage.model';

export interface ISearchResult {
    uuid                        : string;
    firstName                   : string;
    middleName                  : string;
    lastName                    : string;
    birthDate                   : string;
    score                       : number;
    contactMechanisms           : Array<ISearchContactMechanism>;
    serviceCoverages            : Array<ISearchServiceCoverage>;
}

export const SEARCH_RESULT = Record({
    uuid                        : undefined,
    firstName                   : '',
    middleName                  : '',
    lastName                    : '',
    birthDate                   : '',
    score                       : 0,
    contactMechanisms           : List<SearchContactMechanism>(),
    serviceCoverages            : List<SearchServiceCoverage>()
});

export class SearchResult extends SEARCH_RESULT {
    uuid                        : string;
    firstName                   : string;
    middleName                  : string;
    lastName                    : string;
    birthDate                   : string;
    score                       : number;
    contactMechanisms           : List<SearchContactMechanism>;
    serviceCoverages            : List<SearchServiceCoverage>;

    constructor(values? : SearchResult | ISearchResult) {
        let convertedValues     : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchResult) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // contactMechanisms
                convertedValues = convertedValues.set('contactMechanisms', List(convertedValues.get('contactMechanisms', []).map(value => new SearchContactMechanism(value))));

                // serviceCoverages
                convertedValues = convertedValues.set('serviceCoverages', List(convertedValues.get('serviceCoverages', []).map(value => new SearchServiceCoverage(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
