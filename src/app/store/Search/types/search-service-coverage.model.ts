import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    ISearchServiceCoverageClassification,
    SearchServiceCoverageClassification
} from './search-service-coverage-classification.model';
import {
    ISearchServiceCoveragePlan,
    SearchServiceCoveragePlan
} from './search-service-coverage-plan.model';

export interface ISearchServiceCoverage {
    uuid                : string;
    personUuid          : string;
    beneficiaryId       : string;
    fromDate            : string;
    thruDate            : string;
    ordinality          : number;
    serviceCoveragePlan : ISearchServiceCoveragePlan;
    classifications     : Array<ISearchServiceCoverageClassification>;
}

export const SEARCH_SERVICE_COVERAGE = Record({
    uuid                : undefined,
    personUuid          : '',
    beneficiaryId       : '',
    fromDate            : '',
    thruDate            : '',
    ordinality          : 0,
    serviceCoveragePlan : new SearchServiceCoveragePlan(),
    classifications     : List<SearchServiceCoverageClassification>()
});

export class SearchServiceCoverage extends SEARCH_SERVICE_COVERAGE {
    uuid                : string;
    personUuid          : string;
    beneficiaryId       : string;
    fromDate            : string;
    thruDate            : string;
    ordinality          : number;
    serviceCoveragePlan : SearchServiceCoveragePlan;
    classifications     : List<SearchServiceCoverageClassification>;

    constructor(values? : SearchServiceCoverage | ISearchServiceCoverage) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchServiceCoverage) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoveragePlan
                convertedValues = convertedValues.set('serviceCoveragePlan', new SearchServiceCoveragePlan(convertedValues.get('serviceCoveragePlan')));

                // personServiceCoveragePlanClassifications
                convertedValues = convertedValues.set('classifications', List(convertedValues.get('classifications', []).map(value => new SearchServiceCoverageClassification(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
