import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IPaginationConfig {
    pageOffset      : number;
    pageSize        : number;
    totalRecords    : number;
    numOfPages      : number;
    items           : number;
}

export const PAGINATION_CONFIG = Record({
    pageOffset      : 0,
    pageSize        : 0,
    totalRecords    : 0,
    numOfPages      : 0,
    items           : 0
});

export class PaginationConfig extends PAGINATION_CONFIG {
    pageOffset      : number;
    pageSize        : number;
    totalRecords    : number;
    numOfPages      : number;
    items           : number;

    constructor(values? : PaginationConfig | IPaginationConfig) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof PaginationConfig) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
