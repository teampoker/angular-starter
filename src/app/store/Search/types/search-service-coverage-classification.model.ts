import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    ISearchServiceCoveragePlanClassification,
    SearchServiceCoveragePlanClassification
} from './search-service-coverage-plan-classification';

export interface ISearchServiceCoverageClassification {
    value                               : string;
    serviceCoveragePlanClassification   : ISearchServiceCoveragePlanClassification;
}

export const SEARCH_SERVICE_COVERAGE_CLASSIFICATION = Record({
    value                               : '',
    serviceCoveragePlanClassification   : new SearchServiceCoveragePlanClassification()
});

export class SearchServiceCoverageClassification extends SEARCH_SERVICE_COVERAGE_CLASSIFICATION {
    value                               : string;
    serviceCoveragePlanClassification   : SearchServiceCoveragePlanClassification;

    constructor(values? : SearchServiceCoverageClassification | ISearchServiceCoverageClassification) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SearchServiceCoverageClassification) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // serviceCoveragePlanClassification
                convertedValues = convertedValues.set('serviceCoveragePlanClassification', new SearchServiceCoveragePlanClassification(convertedValues.get('serviceCoveragePlanClassification')));

            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
