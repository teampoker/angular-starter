import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../app-store';
import {SearchStateSelectors} from './search.selectors';
import {SearchService} from '../../shared/services/Search/search.service';
import {SearchType, ISearchType} from './types/search-type.model';
import {SearchParameters} from './types/search-parameters.model';
import {KeyValuePair} from '../types/key-value-pair.model';
import {SearchResultsOrderByConfig} from './types/search-results-order-by-config.model';

@Injectable()

/**
 * SearchActions Action Creator Service
 */
export class SearchActions {
    static SEARCH_UPDATE_SEARCH_TYPES                       : string = 'SEARCH_UPDATE_SEARCH_TYPES';

    /* Header Search Actions */
    static SEARCH_UPDATE_HEADER_SEARCH_TEXT                 : string = 'SEARCH_UPDATE_HEADER_SEARCH_TEXT';
    static SEARCH_UPDATE_HEADER_SEARCH_TYPE                 : string = 'SEARCH_UPDATE_HEADER_SEARCH_TYPE';
    static SEARCH_UPDATE_HEADER_PAGE_SIZE                   : string = 'SEARCH_UPDATE_HEADER_PAGE_SIZE';
    static SEARCH_UPDATE_HEADER_SEARCH_RESULTS              : string = 'SEARCH_UPDATE_HEADER_SEARCH_RESULTS';
    static SEARCH_UPDATE_HEADER_SEARCH_RESULTS_ORDER_BY     : string = 'SEARCH_UPDATE_HEADER_SEARCH_RESULTS_ORDER_BY';
    static SEARCH_INCREMENT_HEADER_SEARCH_PAGE_INDEX        : string = 'SEARCH_INCREMENT_HEADER_SEARCH_PAGE_INDEX';
    static SEARCH_DECREMENT_HEADER_SEARCH_PAGE_INDEX        : string = 'SEARCH_DECREMENT_HEADER_SEARCH_PAGE_INDEX';
    static SEARCH_TOGGLE_HEADER_SEARCH_ACTIVE               : string = 'SEARCH_TOGGLE_HEADER_SEARCH_ACTIVE';
    static SEARCH_RESET_HEADER_SEARCH_STATE                 : string = 'SEARCH_RESET_HEADER_SEARCH_STATE';
    static SEARCH_RESET_SEARCH_PARAMETERS                   : string = 'SEARCH_RESET_SEARCH_PARAMETERS';
    static SEARCH_HEADER_FETCHING                           : string = 'SEARCH_HEADER_FETCHING';

    /* People Search Actions */
    static SEARCH_UPDATE_PEOPLE_SEARCH_TEXT                 : string = 'SEARCH_UPDATE_PEOPLE_SEARCH_TEXT';
    static SEARCH_UPDATE_PEOPLE_SEARCH_TYPE                 : string = 'SEARCH_UPDATE_PEOPLE_SEARCH_TYPE';
    static SEARCH_UPDATE_PEOPLE_PAGE_SIZE                   : string = 'SEARCH_UPDATE_PEOPLE_PAGE_SIZE';
    static SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS              : string = 'SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS';
    static SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS_ORDER_BY     : string = 'SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS_ORDER_BY';
    static SEARCH_INCREMENT_PEOPLE_SEARCH_PAGE_INDEX        : string = 'SEARCH_INCREMENT_PEOPLE_SEARCH_PAGE_INDEX';
    static SEARCH_DECREMENT_PEOPLE_SEARCH_PAGE_INDEX        : string = 'SEARCH_DECREMENT_PEOPLE_SEARCH_PAGE_INDEX';
    static SEARCH_TOGGLE_PEOPLE_SEARCH_ACTIVE               : string = 'SEARCH_TOGGLE_PEOPLE_SEARCH_ACTIVE';
    static SEARCH_UPDATE_PEOPLE_SEARCH_SHOW_ALL_RESULTS     : string = 'SEARCH_UPDATE_PEOPLE_SEARCH_SHOW_ALL_RESULTS';
    static SEARCH_RESET_PEOPLE_SEARCH_STATE                 : string = 'SEARCH_RESET_PEOPLE_SEARCH_STATE';
    static SEARCH_PEOPLE_FETCHING                           : string = 'SEARCH_PEOPLE_FETCHING';

    /**
     * SearchActions constructor
     * @param store
     * @param searchService
     * @param searchSelectors
     * @param router
     */
    constructor(
        private store           : NgRedux<IAppStore>,
        private searchService   : SearchService,
        private searchSelectors : SearchStateSelectors,
        private router          : Router
    ) {}

    /**
     * Async Action that queries API for available search types
     * user can select from
     */
    getSearchTypes() {
        let searchTypes : List<SearchType> = List<SearchType>();

        // check for existing types
        this.searchSelectors.searchTypes().subscribe(value => searchTypes = value).unsubscribe();

        if (searchTypes.size === 0) {
            // query for search types
            this.searchService.getSearchTypes().subscribe(response => {
                // update search types
                this.updateSearchTypes(response);
            }, error => {
                // clear search results
                this.updateHeaderSearchResults([]);
            });
        }
    }

    /**
     * Async Action that queries API for search results for a given search type
     * and updates Redux store with those results, then navigates the user
     * to the "/Search" route
     */
    getHeaderSearchResults() {
        let searchParams : SearchParameters;

        // is a search request already active?
        if (!this.searchSelectors.isHeaderSearchActive()) {
            this.headerSearchIsFetching();

            // grab any search parameters
            searchParams = this.searchSelectors.headerSearchParameters();

            // set search active flag
            this.toggleHeaderSearchActive();

            // query for search results
            this.searchService.getHeaderSearchResults(searchParams, undefined).subscribe(response => {
                // update search results
                this.updateHeaderSearchResults(response);

                // set search active flag
                this.toggleHeaderSearchActive();

                // navigate user to search view
                this.router.navigate(['/Search']);
            }, error => {
                // clear search results
                this.updateHeaderSearchResults({
                    results : []
                });

                // set search active flag
                this.toggleHeaderSearchActive();
            });
        }
    }

    /**
     * Async Action that queries API for people results
     * and updates Redux store with those results
     */
    getPeopleSearchResults() {
        let peopleParams : SearchParameters;

        this.peopleSearchIsFetching();

        // grab any people parameters
        peopleParams = this.searchSelectors.peopleSearchParameters();

        // set people search active flag
        this.togglePeopleSearchActive();

        // query for people results
        this.searchService.getPeopleSearchResults(peopleParams, undefined).subscribe(response => {
            // update people results
            this.updatePeopleSearchResults(response);

            // set people search active flag
            this.togglePeopleSearchActive();

            // navigate user to people view
            // this.router.navigate(['/People']);
        }, error => {
            // clear people results
            this.updatePeopleSearchResults({
                results : []
            });

            // set people search active flag
            this.togglePeopleSearchActive();
        });
    }

    /**
     * Sync Action that updates search types in Redux store
     */
    updateSearchTypes(searchTypes : Array<ISearchType>) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_SEARCH_TYPES,
            payload : searchTypes
        });
    }

    /**
     * Sync Action that updates header search results in Redux store
     * @param searchResults
     */
    updateHeaderSearchResults(searchResults : any) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS,
            payload : searchResults
        });
    }

    /**
     * Sync Action that updates people search results in Redux store
     * @param searchResults
     */
    updatePeopleSearchResults(searchResults : any) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS,
            payload : searchResults
        });
    }

    /**
     * Sync Action that updates user's selected header search type in Redux store
     * @param searchType
     */
    updateSelectedHeaderSearchType(searchType : KeyValuePair) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_TYPE,
            payload : searchType
        });
    }

    /**
     * Sync Action that updates user's selected people search type in Redux store
     * @param searchType
     */
    updateSelectedPeopleSearchType(searchType : KeyValuePair) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_TYPE,
            payload : searchType
        });
    }

    /**
     * Sync Action that updates selected header page size setting in Redux store
     * @param pageSize
     */
    updateHeaderSearchSelectedPageSize(pageSize : KeyValuePair) {
        // update the current maximum number of records to show per page setting
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_HEADER_PAGE_SIZE,
            payload : pageSize
        });

        // refresh results with this new value
        this.getHeaderSearchResults();
    }

    /**
     * Sync Action that updates selected people search page size setting in Redux store
     * @param pageSize
     */
    updatePeopleSearchSelectedPageSize(pageSize : KeyValuePair) {
        // update the current maximum number of records to show per page setting
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_PAGE_SIZE,
            payload : pageSize
        });

        // refresh results with this new value
        this.getPeopleSearchResults();
    }

    /**
     * Sync Action that updates entered header search text in Redux store
     * @param searchText
     */
    updateHeaderSearchText(searchText : string) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_TEXT,
            payload : searchText
        });
    }

    /**
     * Sync Action that updates entered people search text in Redux store
     * @param searchText
     */
    updatePeopleSearchText(searchText : string) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_TEXT,
            payload : searchText
        });

        // refresh results with this new value
        this.getPeopleSearchResults();
    }

    /**
     * Sync Action that updates header search results order by configuration in Redux store
     * @param config
     */
    updateHeaderSearchOrderByConfig(config : SearchResultsOrderByConfig) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_HEADER_SEARCH_RESULTS_ORDER_BY,
            payload : config
        });
    }

    /**
     * Sync Action that updates people search results order by configuration in Redux store
     * @param config
     */
    updatePeopleSearchOrderByConfig(config : SearchResultsOrderByConfig) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_RESULTS_ORDER_BY,
            payload : config
        });
    }

    /**
     * Sync Action that increments selected page index for header search in Redux store
     */
    incrementHeaderPageIndex() {
        this.store.dispatch({ type : SearchActions.SEARCH_INCREMENT_HEADER_SEARCH_PAGE_INDEX });
    }

    /**
     * Sync Action that increments selected page index for people search in Redux store
     */
    incrementPeoplePageIndex() {
        this.store.dispatch({ type : SearchActions.SEARCH_INCREMENT_PEOPLE_SEARCH_PAGE_INDEX });
    }

    /**
     * Sync Action that decrements selected page index for header search in Redux store
     */
    decrementHeaderPageIndex() {
        this.store.dispatch({ type : SearchActions.SEARCH_DECREMENT_HEADER_SEARCH_PAGE_INDEX });
    }

    /**
     * Sync Action that decrements selected page index for people search in Redux store
     */
    decrementPeoplePageIndex() {
        this.store.dispatch({ type : SearchActions.SEARCH_DECREMENT_PEOPLE_SEARCH_PAGE_INDEX });
    }

    /**
     * Sync Action that toggles the state of the isSearchActive flag
     */
    toggleHeaderSearchActive() {
        this.store.dispatch({ type : SearchActions.SEARCH_TOGGLE_HEADER_SEARCH_ACTIVE });
    }

    /**
     * Sync Action that toggles the state of the isSearchActive flag
     */
    togglePeopleSearchActive() {
        this.store.dispatch({ type : SearchActions.SEARCH_TOGGLE_PEOPLE_SEARCH_ACTIVE });
    }

    /**
     * Sync Action that toggles the state of the isShowAllActive flag
     * @param showAll expanded state of show all results section
     */
    updatePeopleSearchShowAllResults(showAll? : boolean) {
        this.store.dispatch({
            type    : SearchActions.SEARCH_UPDATE_PEOPLE_SEARCH_SHOW_ALL_RESULTS,
            payload : showAll
        });
    }

    /**
     * Sync Action that clears relevant Search State values
     */
    resetHeaderSearchState() {
        this.store.dispatch({ type : SearchActions.SEARCH_RESET_HEADER_SEARCH_STATE });
    }

    /**
     * Sync Action that clears relevant People State values
     */
    resetPeopleSearchState() {
        this.store.dispatch({ type : SearchActions.SEARCH_RESET_PEOPLE_SEARCH_STATE });
    }

    /**
     * Sync Action that resets the search parameters only
     */
    resetSearchParameters() {
        this.store.dispatch({ type : SearchActions.SEARCH_RESET_SEARCH_PARAMETERS });
    }

    /**
     * Indicate that the header search entity is entering into a fetch operation - waiting on another resource.
     */
    headerSearchIsFetching() {
        this.store.dispatch({ type : SearchActions.SEARCH_HEADER_FETCHING });
    }

    /**
     * Indicate that the people search entity is entering into a fetch operation - waiting on another resource.
     */
    peopleSearchIsFetching() {
        this.store.dispatch({ type : SearchActions.SEARCH_PEOPLE_FETCHING });
    }
}
