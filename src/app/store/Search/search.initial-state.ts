import {List} from 'immutable';

import {KeyValuePair} from '../types/key-value-pair.model';
import {SearchState} from './types/search-state.model';
import {SearchResultsOrderByConfig} from './types/search-results-order-by-config.model';

export const INITIAL_SEARCH_STATE = new SearchState().withMutations(state => {
    state.setIn(['headerSearchState', 'searchResultsPaging', 'pageSizeOptions'], List([{
        id      : '1',
        value   : '10'
    }, {
        id      : '2',
        value   : '25'
    }, {
        id      : '3',
        value   : '50'
    }, {
        id      : '4',
        value   : '100'
    }]).map(value => new KeyValuePair(value)))

        .setIn(['headerSearchState', 'selectedPageSize'], new KeyValuePair({
            id      : '1',
            value   : '10'
        }))

        .setIn(['headerSearchState', 'searchBoxState', 'searchTypeOptions'], List([
            {   id      : '2',
                value   : 'BENEFICIARY'
            }
        ]).map(value => new KeyValuePair(value)))

        .setIn(['headerSearchState', 'searchBoxState', 'selectedSearchType'], new KeyValuePair({
            id      : '2',
            value   : 'BENEFICIARY'
        }))

        .setIn(['headerSearchState', 'orderByConfig'], new SearchResultsOrderByConfig({
            sortColumn   : 'lastName',
            sortOrder    : 'asc'
        }))

        .setIn(['peopleSearchState', 'searchResultsPaging', 'pageSizeOptions'], List([{
            id      : '1',
            value   : '10'
        }, {
            id      : '2',
            value   : '25'
        }, {
            id      : '3',
            value   : '50'
        }, {
            id      : '4',
            value   : '100'
        }]).map(value => new KeyValuePair(value)))

        .setIn(['peopleSearchState', 'selectedPageSize'], new KeyValuePair({
            id      : '1',
            value   : '10'
        }))

        .setIn(['peopleSearchState', 'searchBoxState', 'searchTypeOptions'], List([
            {   id      : '2',
                value   : 'BENEFICIARY'
            }
        ]).map(value => new KeyValuePair(value)))

        .setIn(['peopleSearchState', 'searchBoxState', 'selectedSearchType'], new KeyValuePair({
            id      : '2',
            value   : 'BENEFICIARY'
        }))

        .setIn(['peopleSearchState', 'orderByConfig'], new SearchResultsOrderByConfig({
            sortColumn   : 'lastName',
            sortOrder    : 'asc'
        }));
}) as SearchState;
