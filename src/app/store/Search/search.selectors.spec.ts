import {async} from '@angular/core/testing';
import {Iterable} from 'immutable';

import {SearchStateSelectors} from './search.selectors';
import {SearchBoxState} from './types/search-box-state.model';
import {SearchResultsOrderByConfig} from './types/search-results-order-by-config.model';
import {SearchResultsPaging} from './types/search-results-paging.model';
import {KeyValuePair} from '../types/key-value-pair.model';
import {SearchParameters} from './types/search-parameters.model';
import {MockRedux} from '../../../testing/ng2-redux-subs';
import {ROOT_REDUCER} from '../app-store';

describe('Search State Selectors', () => {
    let searchSelectors : SearchStateSelectors,
        mockRedux       : MockRedux;

    // setup tasks to perform before each test
    beforeEach(() => {
        mockRedux = new MockRedux();

        mockRedux.configureStore(ROOT_REDUCER, {}, undefined, undefined);

        // Initialize mock NgRedux and create a new instance of the
        searchSelectors = new SearchStateSelectors(mockRedux);
    });

    // test definitions
    it('searchTypes method should return SearchState searchTypes subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.searchTypes().first().subscribe(val => {
            // check returned state for Immutable List
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('isHeaderSearchActive method should return SearchState.headerSearchState.isHeaderSearchActive value', async(() => {
        // subscribe to Redux store updates
        const searchActive = searchSelectors.isHeaderSearchActive();

        expect(typeof searchActive === 'boolean').toEqual(true);
    }));

    it('isPeopleSearchActive method should return SearchState.peopleSearchState.isPeopleSearchActive value', async(() => {
        // subscribe to Redux store updates
        const searchActive = searchSelectors.isPeopleSearchActive();

        expect(typeof searchActive === 'boolean').toEqual(true);
    }));

    it('headerSearchText method should return SearchState.headerSearchState.searchBoxState.searchText subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.headerSearchText().first().subscribe(val => {
            // check returned state for correct type
            expect(typeof val === 'string').toEqual(true);
        });
    }));

    it('peopleSearchText method should return SearchState.peopleSearchState.searchBoxState.searchText subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.peopleSearchText().first().subscribe(val => {
            // check returned state for correct type
            expect(typeof val === 'string').toEqual(true);
        });
    }));

    it('headerSearchBoxState method should return SearchState.headerSearchState.searchBoxState subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.headerSearchBoxState().first().subscribe(val => {
            // check returned state for correct type
            expect(val instanceof SearchBoxState).toEqual(true);
        });
    }));

    it('peopleSearchBoxState method should return SearchState.peopleSearchState.searchBoxState subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.peopleSearchBoxState().first().subscribe(val => {
            // check returned state for correct type
            expect(val instanceof SearchBoxState).toEqual(true);
        });
    }));

    it('selectedHeaderSearchType method should return KeyValuePair instance', async(() => {
        const searchType = searchSelectors.selectedHeaderSearchType();

        // check returned state for correct type
        expect(searchType instanceof KeyValuePair).toEqual(true);
    }));

    it('headerSearchResults method should return SearchState.headerSearchState.searchResults subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.headerSearchResults().first().subscribe(val => {
            // check returned state for Immutable Listd
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('peopleSearchResults method should return SearchState.peopleSearchState.searchResults subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.peopleSearchResults().first().subscribe(val => {
            // check returned state for Immutable Listd
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('headerSearchOrderByConfig method should return SearchState.headerSearchState.orderByConfig subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.headerSearchOrderByConfig().first().subscribe(val => {
            // check returned state for Immutable List
            expect(val instanceof SearchResultsOrderByConfig).toEqual(true);
        });
    }));

    it('peopleSearchOrderByConfig method should return SearchState.peopleSearchState.orderByConfig subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.peopleSearchOrderByConfig().first().subscribe(val => {
            // check returned state for Immutable List
            expect(val instanceof SearchResultsOrderByConfig).toEqual(true);
        });
    }));

    it('headerSearchResultsPaging method should return SearchState.headerState.searchResultsPaging subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.headerSearchResultsPaging().first().subscribe(val => {
            // check returned state for Immutable List
            expect(val instanceof SearchResultsPaging).toEqual(true);
        });
    }));

    it('peopleSearchResultsPaging method should return SearchState.peopleSearchState.searchResultsPaging subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.peopleSearchResultsPaging().first().subscribe(val => {
            // check returned state for Immutable List
            expect(val instanceof SearchResultsPaging).toEqual(true);
        });
    }));

    it('headerSearchSelectedPageSize method should return SearchState.headerState.selectedPageSize subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.headerSearchSelectedPageSize().first().subscribe(val => {
            // check returned state for Immutable List
            expect(val instanceof KeyValuePair).toEqual(true);
        });
    }));

    it('headerSearchPageSizeOptions method should return SearchState.headerSearchState.searchResultsPaging.pageSizeOptions subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.headerSearchPageSizeOptions().first().subscribe(val => {
            // check returned state for Immutable List
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('peopleSearchPageSizeOptions method should return SearchState.peopleSearchState.searchResultsPaging.pageSizeOptions subscription', async(() => {
        // subscribe to Redux store updates
        searchSelectors.peopleSearchPageSizeOptions().first().subscribe(val => {
            // check returned state for Immutable List
            expect(Iterable.isIndexed(val)).toEqual(true);
        });
    }));

    it('headerSearchParameters method should return SearchParameters instance', async(() => {
        const searchParameters = searchSelectors.headerSearchParameters();

        // check returned state for correct type
        expect(searchParameters instanceof SearchParameters).toEqual(true);
    }));

    it('peopleSearchParameters method should return SearchParameters instance', async(() => {
        const searchParameters = searchSelectors.peopleSearchParameters();

        // check returned state for correct type
        expect(searchParameters instanceof SearchParameters).toEqual(true);
    }));
});
