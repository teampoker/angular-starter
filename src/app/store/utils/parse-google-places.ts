/**
 * interface for google.maps.places.PlaceResult.address_components
 */
export interface IAddressComponent {
    long_name   : string;
    short_name  : string;
    types       : Array<string>;
}

/**
 * Helper function to grab address information from a Google Places response
 *
 * @param addressComponents
 * @param componentType
 * @param shortName
 * @returns {string}
 */
export function getAddressComponent(addressComponents : Array<IAddressComponent>, componentType : string, shortName : boolean = false) : string | undefined {
    const component = addressComponents.find(addressComponent => addressComponent.types.indexOf(componentType) !== -1);

    if (component) {
        return shortName ? component.short_name : component.long_name;
    }
}
