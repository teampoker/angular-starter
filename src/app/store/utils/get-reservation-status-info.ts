/**
 * Helper function to find the correct color for the reservation status
 *
 * @param statusId
 * @returns {string}
 */
export function getStatusColor(statusId : number) : string {
    switch (statusId) {
        case 1: // Pending
            return '#ff8800'; // $Orange
        case 2: // Approved
            return '#3CA749'; // $Green
        case 3: // Deniied
        case 4: // Cancelled
            return '#B6091A'; // $Red
        default:
            return '';
    }
}

/**
 * Returns boolean used to show denied reaons
 * @returns {boolean}
 */
export function showDeniedReason(statusId : number) : boolean {
     return statusId === 3; // I don't know what the denied status id is since no one seems to know but just adding this for now and we can change it later.
 }
