/**
 * parse a raw phone number string and extract the area code
 * @param phoneRawString raw phone number info
 * @returns {string} extracted area code
 */
export function parsePhoneNumberAreaCode(phoneRawString : string) : string {
    // look for presence of opening parentheses and assume a closing parentheses if found, then extract the area code value
    return phoneRawString.indexOf('(') === 0 ? phoneRawString.substring(1, 4).trim() : phoneRawString.substring(0, 3).trim();
}

/**
 * parse a raw phone number string and extract contact number
 * @param phoneRawString raw phone number info
 * @returns extracted contact number
 */
export function parsePhoneNumberContactNumber(phoneRawString : string) : string {
    // look for presence of opening parentheses and assume a closing parentheses if found.  Account for an extra space between
    // the closing parenthese, if found, and the first digit of the contact number
    return phoneRawString.indexOf('(') === 0 ? phoneRawString.indexOf(') ') === -1 ?
            phoneRawString.substring(5, 13).substring(0, 3).trim() + phoneRawString.substring(5, 13).substring(4, 8).trim() : phoneRawString.substring(6, 14).substring(0, 3).trim() + phoneRawString.substring(6, 14).substring(4, 8).trim()
        : phoneRawString.substring(4, 7).trim() + phoneRawString.substring(8, 12).trim();
}

/**
 * formats phone number for api consumption
 * @param value : user entered phone number
 */
export function formatPhoneForApi(value : string) : string {
    value = value.length === 15 && value[value.length - 1] === '_' ? value.substr(0, 14) : value;
    const PHONE_NUM_REGEXP : any = /^(\(\d{3}\))|(\d{3}-)\d{3}-\d{4}$/;

    if (PHONE_NUM_REGEXP.test(value) && value[0] === '(') {
        return `${value.slice(1, 4)}-${value.slice(6, 9)}-${value.slice(10, 14)}`;
    }
    else {
        return value;
    }
}

export const PHONE_MASK_REGEX : Array <any> = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
