import {List} from 'immutable';

import {IPayloadAction} from '../app-store';
import {FormsState} from './types/forms-state.model';
import {FormsActions} from './forms.actions';
import {
    fromApi as formTypeFromApi,
    FormType
} from './types/form-type.model';
import {KeyValuePair} from '../types/key-value-pair.model';
import {INITIAL_FORMS_STATE} from './forms.initial-state';
import {BeneficiaryFormPhone} from './types/form-phone.model';
import {
    parsePhoneNumberAreaCode,
    parsePhoneNumberContactNumber
} from '../utils/parse-phone-numbers';
import {FormStateDetail} from './types/form-state-detail.model';

/**
 * Beneficiary Forms state reducer
 *
 * @param state
 * @param action
 * @returns {FormsState}
 * @constructor
 */
export const FORMS_STATE_REDUCER = (state : FormsState = INITIAL_FORMS_STATE, action : IPayloadAction) : FormsState => {
    switch (action.type) {
        case FormsActions.BENEFICIARY_FORM_INIT_FORM_STATE :
            state = new FormsState();

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_BENEFICIARY_FORMS :
            state = state.set('beneficiaryForms', List<FormStateDetail>(action.payload.map(value => new FormStateDetail(value)))) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_FORM_VALID :
            // update valid flag for the FormGroup
            state = state.setIn(['addBeneficiaryForm', 'isFormValid'], action.payload) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_PHONE_FORM_VALID :
            // update valid flag for this phone's FormGroup
            state = state.setIn(['addBeneficiaryForm', 'telecommunicationsNumbers', action.payload.primaryUpdateIndex, 'isFormValid'], action.payload.isValid) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_CANCEL_FORM :
            state = state.withMutations(record => record
                .set('addBeneficiaryForm', new FormStateDetail())
                .setIn(['addBeneficiaryForm', 'isFormVisible'], false)) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_FORM_METADATA :
            // update the beneficiary
            state = state.set('formTypes', List<FormType>(action.payload.map(value => formTypeFromApi(value)))) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_AUTH_REASONS :
            // update the beneficiary
            state = state.set('authReasons', List<KeyValuePair>(action.payload.map(value => KeyValuePair.fromApi(value)))) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_FORM_VISIBLE :
            // clear addBeneficiaryForm
            state = state.set('addBeneficiaryForm', new FormStateDetail()) as FormsState;

            // show form
            state = state.setIn(['addBeneficiaryForm', 'isFormVisible'], action.payload) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_SELECTED_FORM_TYPE :
            // update selected form type
            state = state.withMutations(record => record
                .setIn(['addBeneficiaryForm', 'type'], action.payload)

                // clear previous service offerings selections
                .setIn(['addBeneficiaryForm', 'serviceOfferings'], List())) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_SELECTED_SERVICE_OFFERING :
            // clear previous service offerings selections
            state = state.setIn(['addBeneficiaryForm', 'serviceOfferings'], List()) as FormsState;

            // check type here for MNF - Additional Passenger
            if (state.getIn(['addBeneficiaryForm', 'type', 'value']) === 'MNF - Additional Passenger') {
                state = state.setIn(['addBeneficiaryForm', 'numPassengers'], parseInt(action.payload.get('value'), 10)) as FormsState;
            }
            else {
                state = state.updateIn(['addBeneficiaryForm', 'serviceOfferings'], value => value.push(action.payload)) as FormsState;

                // clear # of passengers field
                state = state.setIn(['addBeneficiaryForm', 'numPassengers'], undefined) as FormsState;
            }

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_SELECTED_AUTHORIZATION_REASON :
            // update selected form type
            state = state.setIn(['addBeneficiaryForm', 'authorizationReason'], action.payload) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_SELECTED_NO_EXPIRATION :
            state = state.withMutations(record => record
                .setIn(['addBeneficiaryForm', 'noExpiration'], action.payload)
                .setIn(['addBeneficiaryForm', 'fromDate'], '')
                .setIn(['addBeneficiaryForm', 'thruDate'], '')) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_EFFECTIVE_DATE :
            // update selected effective date
            state = state.setIn(['addBeneficiaryForm', 'fromDate'], action.payload) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_EXPIRATION_DATE :
            // update selected expiration date
            state = state.setIn(['addBeneficiaryForm', 'thruDate'], action.payload) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_AUTHORIZATION :
            // update authorized by
            state = state.setIn(['addBeneficiaryForm', action.payload.fieldName], action.payload.fieldValue) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_PHONE_FIELD_VALUE :
            if (action.payload.fieldValue !== undefined) {
                state = state.withMutations(record => record
                    .setIn(['addBeneficiaryForm', 'telecommunicationsNumbers', action.payload.primaryUpdateIndex, action.payload.fieldName], action.payload.fieldValue)
                    .setIn(['addBeneficiaryForm', 'telecommunicationsNumbers', action.payload.primaryUpdateIndex, 'areaCode'], parsePhoneNumberAreaCode(action.payload.fieldValue))
                    .setIn(['addBeneficiaryForm', 'telecommunicationsNumbers', action.payload.primaryUpdateIndex, 'contactNumber'], parsePhoneNumberContactNumber(action.payload.fieldValue))) as FormsState;
            }

            break;
        case FormsActions.BENEFICIARY_FORM_UPDATE_PHONE_TYPE :
            state = state.setIn(['addBeneficiaryForm', 'telecommunicationsNumbers', action.payload.primaryUpdateIndex, action.payload.fieldName], action.payload.fieldValue) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_ADD_ADDITIONAL_PHONE :
            state = state.updateIn(['addBeneficiaryForm', 'telecommunicationsNumbers'], value => value.push(new BeneficiaryFormPhone())) as FormsState;

            break;
        case FormsActions.BENEFICIARY_FORM_REMOVE_ADDITIONAL_PHONE :
            state = state.deleteIn(['addBeneficiaryForm', 'telecommunicationsNumbers', action.payload]) as FormsState;

            break;
        default :
            return state;
    }

    return state;
};
