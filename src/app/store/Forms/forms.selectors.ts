import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {NgRedux} from '@angular-redux/store';
import {List} from 'immutable';

import {IAppStore} from '../app-store';
import {FormType} from './types/form-type.model';
import {KeyValuePair} from '../types/key-value-pair.model';
import {FormStateDetail} from './types/form-state-detail.model';

@Injectable()

/**
 * implementation for FormsStateSelectors: responsible for exposing custom state subscriptions to Beneficiary Form state
 */
export class FormsStateSelectors {
    /**
     * FormsStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to BeneficiaryState.beneficiaryFormState
     * @returns {Observable<List<FormStateDetail>>}
     */
    beneficiaryForms() : Observable<List<FormStateDetail>> {
        return this.store.select(state => state.formsState.get('beneficiaryForms'));
    }

    /**
     * expose Observable to BeneficiaryState.addBeneficiaryForm
     * @returns {Observable<FormStateDetail>}
     */
    addBeneficiaryForm() : Observable<FormStateDetail> {
        return this.store.select(state => state.formsState.get('addBeneficiaryForm'));
    }

    /**
     * expose Observable to FormsState.formTypes
     * @returns {Observable<List<FormType>>}
     */
    beneficiaryFormTypes() : Observable<List<FormType>> {
        return this.store.select(state => state.formsState.get('formTypes'));
    }

    /**
     * expose Observable to FormsState.authReasons
     * @returns {Observable<List<KeyValuePair>>}
     */
    beneficiaryFormAuthReasons() : Observable<List<KeyValuePair>> {
        return this.store.select(state => state.formsState.get('authReasons'));
    }

    /**
     * returns computed form valid state
     * @returns {Observable<boolean>}
     */
    isBeneficiaryFormValid() : Observable<boolean> {
        let formValid               : boolean = false,
            phonesValid             : boolean = false,
            validationSubscription  : Subscription;

        return Observable.create(observer => {
            // pull necessary beneficiaryInfo data
            validationSubscription = Observable.combineLatest(
                this.store.select(state => state.formsState.getIn(['addBeneficiaryForm', 'isFormValid'])),
                this.store.select(state => state.formsState.getIn(['addBeneficiaryForm', 'telecommunicationsNumbers']))
            )
            .subscribe(val => {
                // update base form validation state
                formValid = val[0];

                // compute aggregate valiation state of all phone FormGroup instances
                val[1].forEach(phone => phonesValid = phone.get('isFormValid'));

                // emit new form validation state
                observer.next(formValid && phonesValid);
            });

            // onComplete handler
            return () => {
                // unsubscribe from Redux store
                validationSubscription.unsubscribe();
            };
        });
    }
}
