import {
    fromJS,
    Map,
    Record,
    List
} from 'immutable';

import {
    IFormStateDetail,
    FormStateDetail
} from './form-state-detail.model';
import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';
import {
    fromApi as formTypeFromApi,
    IFormType,
    FormType
} from './form-type.model';

export interface IFormState {
    addBeneficiaryForm  : IFormStateDetail;
    beneficiaryForms    : Array<IFormStateDetail>;
    authReasons         : Array<IKeyValuePair>;
    formTypes           : Array<IFormType>;
}

export const FORMS_STATE = Record({
    addBeneficiaryForm  : new FormStateDetail(),
    beneficiaryForms    : List<FormStateDetail>(),
    authReasons         : List<KeyValuePair>(),
    formTypes           : List<FormType>()
});

export class FormsState extends FORMS_STATE {
    addBeneficiaryForm  : FormStateDetail;
    beneficiaryForms    : List<FormStateDetail>;
    authReasons         : List<KeyValuePair>;
    formTypes           : List<FormType>;

    constructor(values?  : FormsState | IFormState) {
        let convertedValues  : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof FormsState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // addBeneficiaryForm
                convertedValues = convertedValues.set('addBeneficiaryForm', new FormStateDetail(convertedValues.get('addBeneficiaryForm')));

                // beneficiaryForms
                convertedValues = convertedValues.set('beneficiaryForms', List(convertedValues.get('beneficiaryForms', []).map(value => new FormStateDetail(value))));

                // authReasons
                if (convertedValues.getIn(['authReasons', 0, 'uuid'])) {
                    convertedValues = convertedValues.set('authReasons', List(convertedValues.get('authReasons', []).map(value => KeyValuePair.fromApi(value))));
                }
                else if (convertedValues.get('authReasons')) {
                    convertedValues = convertedValues.set('authReasons', List(convertedValues.get('authReasons', []).map(value => new KeyValuePair(value))));
                }

                // formTypes
                if (convertedValues.getIn(['formTypes', 0, 'uuid'])) {
                    convertedValues = convertedValues.set('formTypes', List(convertedValues.get('formTypes', []).map(value => formTypeFromApi(value))));
                }
                else if (convertedValues.get('formTypes')) {
                    convertedValues = convertedValues.set('formTypes', List(convertedValues.get('formTypes', []).map(value => new FormType(value))));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
