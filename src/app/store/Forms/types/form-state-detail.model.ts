import {
    fromJS,
    Map,
    Record,
    List
} from 'immutable';
import * as moment from 'moment';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';
import {
    fromApi as formTypeFromApi,
    toApi as formTypeToApi,
    IFormType,
    FormType
} from './form-type.model';
import {
    toApi as beneficiaryPhoneFormToApi,
    IBeneficiaryFormPhone,
    BeneficiaryFormPhone
} from './form-phone.model';

export interface IFormStateDetail {
    uuid                        : string;
    noExpiration                : boolean;
    numPassengers               : number;
    fromDate                    : string;
    thruDate                    : string;
    authorizedBy                : string;
    authorizationCode           : string;
    note                        : string;
    personUuid                  : string;
    authorizationReason         : IKeyValuePair;
    type                        : IFormType;
    serviceOfferings            : Array<IKeyValuePair>;
    telecommunicationsNumbers   : Array<IBeneficiaryFormPhone>;
    version                     : number;
    isFormVisible               : boolean;
    isFormValid                 : boolean;
}

export const FORM_STATE_DETAIL = Record({
    uuid                        : undefined,
    noExpiration                : false,
    numPassengers               : undefined,
    fromDate                    : moment().format('MM/DD/YYYY'),    // default value for effectiveDate is today's date
    thruDate                    : '',
    authorizedBy                : '',
    authorizationCode           : '',
    note                        : '',
    personUuid                  : undefined,
    authorizationReason         : new KeyValuePair(),
    type                        : new FormType(),
    serviceOfferings            : List<KeyValuePair>(),
    telecommunicationsNumbers   : List<BeneficiaryFormPhone>(),
    version                     : undefined,
    isFormVisible               : false,
    isFormValid                 : false
});

export class FormStateDetail extends FORM_STATE_DETAIL {
    uuid                        : string;
    noExpiration                : boolean;
    numPassengers               : number;
    fromDate                    : string;
    thruDate                    : string;
    authorizedBy                : string;
    authorizationCode           : string;
    note                        : string;
    authorizationReason         : KeyValuePair;
    type                        : FormType;
    serviceOfferings            : List<KeyValuePair>;
    telecommunicationsNumbers   : List<BeneficiaryFormPhone>;
    version                     : number;
    isFormVisible               : boolean;
    isFormValid                 : boolean;

    constructor(values?  : FormStateDetail | IFormStateDetail) {
        let convertedValues  : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof FormStateDetail) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // noExpiration
                convertedValues.get('fromDate') === undefined ? convertedValues.set('noExpiration', true) : convertedValues.set('noExpiration', false);

                // authorizationReason
                convertedValues = convertedValues.set('authorizationReason', new KeyValuePair(convertedValues.get('authorizationReason')));

                // type
                if (convertedValues.getIn(['type', 'name'])) {
                    convertedValues = convertedValues.set('type', formTypeFromApi(convertedValues.get('type')));
                }
                else {
                    convertedValues = convertedValues.set('type', new FormType(convertedValues.get('type')));
                }

                // serviceOfferings
                if (convertedValues.getIn(['serviceOfferings', 0, 'uuid'])) {
                    convertedValues = convertedValues.set('serviceOfferings', List(convertedValues.get('serviceOfferings', []).map(value => KeyValuePair.fromApi(value))));
                }
                else if (convertedValues.get('serviceOfferings')) {
                    convertedValues = convertedValues.set('serviceOfferings', List(convertedValues.get('serviceOfferings', []).map(value => new KeyValuePair(value))));
                }

                // telecommunicationsNumbers
                convertedValues = convertedValues.set('telecommunicationsNumbers', List(convertedValues.get('telecommunicationsNumbers', []).map(value => new BeneficiaryFormPhone(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * transform Immutable FormStateDetail model into
 * appropriate POJO for processing by API endpoints
 *
 * @param model Immutable model to transform
 * @param isEdit POJO needs will be used for a PUT operation if this is true
 *
 * @returns {any} POJO representation
 */
export function toApi(model : FormStateDetail, isEdit : boolean) : any {
    let newObject : any;

    // map immutable model to POJO
    newObject = {
        numPassengers               : model.get('numPassengers', undefined),
        fromDate                    : model.get('fromDate') ? moment(model.get('fromDate')).format('YYYY-MM-DD') : undefined,
        thruDate                    : model.get('thruDate') ? moment(model.get('thruDate')).format('YYYY-MM-DD') : undefined,
        authorizedBy                : model.get('authorizedBy', undefined),
        authorizationCode           : model.get('authorizationCode', undefined),
        note                        : model.get('note', undefined),
        personUuid                  : model.get('personUuid', undefined),
        authorizationReason         : KeyValuePair.toApi(model.get('authorizationReason', undefined)),
        type                        : formTypeToApi(model.get('type', undefined)),
        serviceOfferings            : model.get('serviceOfferings', []).map(service => KeyValuePair.toApi(service)).toJS(),
        telecommunicationsNumbers   : model.get('telecommunicationsNumbers', []).map(phone => beneficiaryPhoneFormToApi(phone)).toJS(),
        version                     : model.get('version', undefined)
    };

    // delete version if this is a POST
    if (!isEdit) {
        delete newObject.version;
    }

    return newObject;
}
