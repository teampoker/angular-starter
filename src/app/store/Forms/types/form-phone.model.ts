import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IBeneficiaryFormPhone {
    isFormValid                 : boolean;
    uuid                        : string;
    phone                       : string;   // this is only used to bind the entered phone text to the UI
    contactNumber               : string;
    countryCode                 : string;
    areaCode                    : string;
    purposeType                 : IKeyValuePair;
}

export const BENEFICIARY_FORM_PHONE = Record({
    isFormValid                 : false,
    uuid                        : '',
    phone                       : '',
    contactNumber               : '',
    countryCode                 : '',
    areaCode                    : '',
    purposeType                 : new KeyValuePair()
});

export class BeneficiaryFormPhone extends BENEFICIARY_FORM_PHONE {
    isFormValid                 : boolean;
    uuid                        : string;
    phone                       : string;
    contactNumber               : string;
    countryCode                 : string;
    areaCode                    : string;
    purposeType                 : IKeyValuePair;

    constructor(values? : BeneficiaryFormPhone | IBeneficiaryFormPhone) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof BeneficiaryFormPhone) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // purposeType
                if (convertedValues.getIn(['purposeType', 'uuid'])) {
                    convertedValues = convertedValues.set('purposeType', KeyValuePair.fromApi(convertedValues.get('purposeType')));
                }
                else {
                    convertedValues = convertedValues.set('purposeType', new KeyValuePair(convertedValues.get('purposeType')));
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * transform Immutable FormStateDetail model into
 * appropriate POJO for processing by API endpoints
 *
 * @param model Immutable model to transform
 *
 *
 * @returns {any} POJO representation
 */
export function toApi(model : BeneficiaryFormPhone) : any {
    return {
        uuid                        : model.get('uuid', undefined),
        contactNumber               : model.get('contactNumber', ''),
        countryCode                 : model.get('countryCode', ''),
        areaCode                    : model.get('areaCode', ''),
        purposeType                 : KeyValuePair.toApi(model.get('purposeType', undefined))
    };
}
