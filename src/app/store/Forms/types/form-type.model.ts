import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IKeyValuePair,
    KeyValuePair
} from '../../types/key-value-pair.model';

export interface IFormType {
    id                      : number;
    value                   : string;
    serviceCategoryTypeUuid : string;
    uuid                    : string;
    serviceOfferings        : Array<IKeyValuePair>;
}

export const FORM_TYPE = Record({
    id                      : 0,
    value                   : 'SELECT',
    serviceCategoryTypeUuid : '',
    uuid                    : '',
    serviceOfferings        : List<KeyValuePair>()
});

const apiMap = Map({
    id                      : 'id',
    name                    : 'value',
    serviceCategoryTypeUuid : 'serviceCategoryTypeUuid',
    uuid                    : 'uuid',
    serviceOfferings        : 'serviceOfferings'
});

export class FormType extends FORM_TYPE {
    id                      : number;
    value                   : string;
    serviceCategoryTypeUuid : string;
    uuid                    : string;
    serviceOfferings        : List<KeyValuePair>;

    constructor(values?  : FormType | IFormType) {
        let convertedValues : Map<any, any>;

        /**
         * used to populate Select a Reason dropdown
         */
        const numOfPassengers : List<KeyValuePair> = List<KeyValuePair>([
            new KeyValuePair({
                id      : '0',
                value   : '1'
            }),
            new KeyValuePair({
                id      : '1',
                value   : '2'
            }),
            new KeyValuePair({
                id      : '2',
                value   : '3'
            }),
            new KeyValuePair({
                id      : '3',
                value   : '4'
            }),
            new KeyValuePair({
                id      : '4',
                value   : '5'
            })
        ]);

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof FormType) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                if (convertedValues.get('value') === 'MNF - Additional Passenger') {
                    convertedValues = convertedValues.set('serviceOfferings', numOfPassengers);
                }
                else {
                    // serviceOfferings
                    if (convertedValues.getIn(['serviceOfferings', 0, 'uuid'])) {
                        convertedValues = convertedValues.set('serviceOfferings', List(convertedValues.get('serviceOfferings', []).map(value => KeyValuePair.fromApi(value))));
                    }
                    else if (convertedValues.get('serviceOfferings')) {
                        convertedValues = convertedValues.set('serviceOfferings', List(convertedValues.get('serviceOfferings', []).map(value => new KeyValuePair(value))));
                    }
                }
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}

/**
 * map a POJO to an Immutable FormType Record instance
 *
 * @param rawObject POJO returned from API endpoint
 *
 * @returns {FormType}
 */
export function fromApi(rawObject : any) : FormType {
    const newObject : any = {};

    if (Map.isMap(rawObject)) {
        apiMap.forEach((translatedKey, apiKey) => {
            newObject[translatedKey] = rawObject.get(apiKey);
        });
    }
    else {
        apiMap.forEach((translatedKey, apiKey) => {
            newObject[translatedKey] = rawObject[apiKey];
        });
    }

    return new FormType(newObject);
}

/**
 * transform Immutable FormStateDetail model into
 * appropriate POJO for processing by API endpoints
 *
 * @param model Immutable model to transform
 *
 * @returns {any} POJO representation
 */
export function toApi(model : FormType | any) : any {
    const newObject : any = {};

    if (Map.isMap(model)) {
        apiMap.forEach((translatedKey, apiKey) => {
            newObject[translatedKey] = model.get(apiKey);
        });
    }
    else {
        apiMap.forEach((translatedKey, apiKey) => {
            newObject[apiKey] = model[translatedKey];
        });
    }

    // delete unused properties
    delete newObject.id;
    delete newObject.name;
    delete newObject.serviceCategoryTypeUuid;
    delete newObject.serviceOfferings;

    return newObject;
}
