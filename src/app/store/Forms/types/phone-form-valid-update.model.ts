import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IPhoneFormValidUpdate {
    isValid                 : boolean;
    primaryUpdateIndex?     : number;
    secondaryUpdateIndex?   : number;
    tertiaryUpdateIndex?    : number;
}

const PHONE_FORM_VALID_UPDATE = Record({
    isValid                 : false,
    primaryUpdateIndex      : 0,
    secondaryUpdateIndex    : 0,
    tertiaryUpdateIndex     : 0
});

export class PhoneFormValidUpdate extends PHONE_FORM_VALID_UPDATE {
    isValid                 : boolean;
    primaryUpdateIndex?     : number;
    secondaryUpdateIndex?   : number;
    tertiaryUpdateIndex?    : number;

    constructor(values? : PhoneFormValidUpdate | IPhoneFormValidUpdate) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof PhoneFormValidUpdate) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        super(convertedValues);
    }
}
