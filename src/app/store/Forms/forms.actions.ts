import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../app-store';
import {NavActions} from '../Navigation/nav.actions';
import {BeneficiaryStateSelectors} from '../Beneficiary/selectors/beneficiary.selectors';
import {BeneficiaryFormsService} from '../../areas/Beneficiary/services/BeneficiaryForms/beneficiary-forms.service';
import {FormType} from './types/form-type.model';
import {KeyValuePair} from '../types/key-value-pair.model';
import {Beneficiary} from '../Beneficiary/types/beneficiary.model';
import {IBeneficiaryFieldUpdate} from '../Beneficiary/types/beneficiary-field-update.model';
import {
    EnumAlertType,
    AlertItem
} from '../Navigation/types/alert-item.model';
import {FormsStateSelectors} from './forms.selectors';
import {FormStateDetail} from './types/form-state-detail.model';
import {IPhoneFormValidUpdate} from './types/phone-form-valid-update.model';

@Injectable()

/**
 * Implementation of BeneficiaryInfoActions: Redux Action Creator Service that exposes methods to mutate Beneficiary Information state
 */
export class FormsActions {
    /**
     * available Redux Actions
     * @type {string}
     */
    static BENEFICIARY_FORM_INIT_FORM_STATE                         : string = 'BENEFICIARY_FORM_INIT_FORM_STATE';
    static BENEFICIARY_FORM_UPDATE_FORM_METADATA                    : string = 'BENEFICIARY_FORM_UPDATE_FORM_METADATA';
    static BENEFICIARY_FORM_UPDATE_AUTH_REASONS                     : string = 'BENEFICIARY_FORM_UPDATE_AUTH_REASONS';
    static BENEFICIARY_FORM_UPDATE_FORM_VISIBLE                     : string = 'BENEFICIARY_FORM_UPDATE_FORM_VISIBLE';
    static BENEFICIARY_FORM_UPDATE_SELECTED_FORM_TYPE               : string = 'BENEFICIARY_FORM_UPDATE_SELECTED_FORM_TYPE';
    static BENEFICIARY_FORM_UPDATE_SELECTED_SERVICE_OFFERING        : string = 'BENEFICIARY_FORM_UPDATE_SELECTED_SERVICE_OFFERING';
    static BENEFICIARY_FORM_UPDATE_SELECTED_AUTHORIZATION_REASON    : string = 'BENEFICIARY_FORM_UPDATE_SELECTED_AUTHORIZATION_REASON';
    static BENEFICIARY_FORM_UPDATE_SELECTED_NO_EXPIRATION           : string = 'BENEFICIARY_FORM_UPDATE_SELECTED_NO_EXPIRATION';
    static BENEFICIARY_FORM_UPDATE_EFFECTIVE_DATE                   : string = 'BENEFICIARY_FORM_UPDATE_EFFECTIVE_DATE';
    static BENEFICIARY_FORM_UPDATE_PHONE_FIELD_VALUE                : string = 'BENEFICIARY_FORM_UPDATE_PHONE_FIELD_VALUE';
    static BENEFICIARY_FORM_UPDATE_PHONE_TYPE                       : string = 'BENEFICIARY_FORM_UPDATE_PHONE_TYPE';
    static BENEFICIARY_FORM_UPDATE_EXPIRATION_DATE                  : string = 'BENEFICIARY_FORM_UPDATE_EXPIRATION_DATE';
    static BENEFICIARY_FORM_ADD_ADDITIONAL_PHONE                    : string = 'BENEFICIARY_FORM_ADD_ADDITIONAL_PHONE';
    static BENEFICIARY_FORM_REMOVE_ADDITIONAL_PHONE                 : string = 'BENEFICIARY_FORM_REMOVE_ADDITIONAL_PHONE';
    static BENEFICIARY_FORM_CANCEL_FORM                             : string = 'BENEFICIARY_FORM_CANCEL_FORM';
    static BENEFICIARY_FORM_UPDATE_AUTHORIZATION                    : string = 'BENEFICIARY_FORM_UPDATE_AUTHORIZATION';
    static BENEFICIARY_FORM_UPDATE_BENEFICIARY_FORMS                : string = 'BENEFICIARY_FORM_UPDATE_BENEFICIARY_FORMS';
    static BENEFICIARY_FORM_UPDATE_FORM_VALID                       : string = 'BENEFICIARY_FORM_UPDATE_FORM_VALID';
    static BENEFICIARY_FORM_UPDATE_PHONE_FORM_VALID                 : string = 'BENEFICIARY_FORM_UPDATE_PHONE_FORM_VALID';

    /**
     * FormsActions constructor
     * @param store
     * @param navActions
     * @param formsService
     * @param beneficiarySelectors
     * @param formsSelectors
     * @param beneficiaryFormsService
     */
    constructor(
        private store                       : NgRedux<IAppStore>,
        private navActions                  : NavActions,
        private formsService                : BeneficiaryFormsService,
        private beneficiarySelectors        : BeneficiaryStateSelectors,
        private formsSelectors              : FormsStateSelectors,
        private beneficiaryFormsService     : BeneficiaryFormsService
    ) {}

    /**
     * dispatch action to update beneficiary form types metadata on Redux store with results
     * @param metadata
     */
    private updateBeneficiaryFormsMetadata(metadata : any) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_FORM_METADATA,
            payload : metadata
        });
    }

    /**
     * dispatch action to update beneficiary form types metadata on Redux store with results
     * @param metadata
     */
    private updateBeneficiaryAuthReasonsMetadata(metadata : any) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_AUTH_REASONS,
            payload : metadata
        });
    }

    /**
     * dispatch action to update the list of forms associated with the current selected beneficiary
     * @param forms
     */
    private updateBeneficiaryForms(forms : any) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_BENEFICIARY_FORMS,
            payload : forms
        });
    }

    /**
     * request form types metadata
     */
    getFormTypes() {
        this.beneficiaryFormsService.getFormTypes().subscribe(response => {
            this.updateBeneficiaryFormsMetadata(response);
        });
    }

    /**
     * request auth reasons metadata
     */
    getAuthReasons() {
        this.beneficiaryFormsService.getAuthorizationReasons().subscribe(response => {
            this.updateBeneficiaryAuthReasonsMetadata(response);
        });
    }

    /**
     * request list of forms for a beneficiary
     */
    getForms(uuid : string) {
        this.beneficiaryFormsService.getForms(uuid).subscribe(response => {
            this.updateBeneficiaryForms(response);
        });
    }

    /**
     * dispatch action to pre-flight initialize the FormsState
     */
    initFormsState() {
        this.store.dispatch({ type : FormsActions.BENEFICIARY_FORM_INIT_FORM_STATE });
    }

    /**
     * dispatch action to update validity state of a given phone FormGroup
     * @param isFormValid
     */
    updateFormValid(isFormValid : boolean) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_FORM_VALID,
            payload : isFormValid
        });
    }

    /**
     * dispatch action to update validity state of a given phone FormGroup
     * @param phoneValidUpdate
     */
    updatePhoneFormValid(phoneValidUpdate : IPhoneFormValidUpdate) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_PHONE_FORM_VALID,
            payload : phoneValidUpdate
        });
    }

    /**
     * dispatch action with updated input data for beneficiary phone field
     * @param update
     */
    updatePhoneField(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_PHONE_FIELD_VALUE,
            payload : update
        });
    }

    /**
     * dispatch action with updated input data for beneficiary phone type selection
     * @param update
     */
    updatePhoneType(update : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_PHONE_TYPE,
            payload : update
        });
    }

    /**
     * dispatch action to add additional beneficiary phone
     */
    addAdditionalFormPhone() {
        this.store.dispatch({ type : FormsActions.BENEFICIARY_FORM_ADD_ADDITIONAL_PHONE });
    }

    /**
     * dispatch action to remove additional beneficiary phone
     * @param removeIndex
     */
    removeAdditionalFormPhone(removeIndex : number) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_REMOVE_ADDITIONAL_PHONE,
            payload : removeIndex
        });
    }

    /**
     * dispatch action to update user beneficiary info editing flag on redux store
     */
    updateBeneficiaryShowAddForm() {
        this.navActions.toggleIsPoppedNavState(true);

        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_FORM_VISIBLE,
            payload : true
        });
    }

    /**
     * dispatch action to update user beneficiary form type selected
     * @param choice selected form type
     */
    updateSelectedFormType(choice : FormType) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_SELECTED_FORM_TYPE,
            payload : choice
        });
    }

    /**
     * dispatch action to update user beneficiary service offering selected
     * @param choice
     */
    updateSelectedServiceOffering(choice : KeyValuePair) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_SELECTED_SERVICE_OFFERING,
            payload : choice
        });
    }

    /**
     * dispatch action to update user beneficiary authorization reason selected
     * @param choice
     */
    updateSelectedAuthorizationReason(choice : KeyValuePair) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_SELECTED_AUTHORIZATION_REASON,
            payload : choice
        });
    }

    /**
     * dispatch action to update user no expiration selected
     * @param choice
     */
    updateSelectedNoExpiration(choice : boolean) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_SELECTED_NO_EXPIRATION,
            payload : choice
        });
    }

    /**
     * dispatch action to update effective date
     * @param choice
     */
    updateEffectiveDate(choice : string) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_EFFECTIVE_DATE,
            payload : choice
        });
    }

    /**
     * dispatch action to update expiration date
     * @param choice
     */
    updateExpirationDate(choice : string) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_EXPIRATION_DATE,
            payload : choice
        });
    }

    /**
     * dispatch action to update authorized fields
     * @param value
     */
    updateAuthorizedFields(value : IBeneficiaryFieldUpdate) {
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_AUTHORIZATION,
            payload : value
        });
    }

    /**
     * dispatch action to reset relevant form state when user cancels add/edit of a form
     */
    cancelForm() {
        // toggle modal styles
        this.navActions.toggleIsPoppedNavState(false);

        this.store.dispatch({ type : FormsActions.BENEFICIARY_FORM_CANCEL_FORM });
    }

    /**
     * trigger save workflow for created/edit Beneficiary Form
     */
    saveBeneficiaryForm() {
        let beneficiaryModel    : Beneficiary = new Beneficiary(),
            createFormPayload   : FormStateDetail = new FormStateDetail();

        // toggle modal styles
        this.navActions.toggleIsPoppedNavState(false);

        // grab Beneficiary Domain Model
        this.beneficiarySelectors.beneficiary().subscribe(value => beneficiaryModel = value).unsubscribe();

        // grab current forms state
        this.formsSelectors.addBeneficiaryForm().subscribe(value => createFormPayload = value).unsubscribe();

        // save the updated Beneficiary Info data
        this.formsService.createForm(beneficiaryModel.get('uuid'), createFormPayload).subscribe(response => {
            // save was successful, refresh list of forms
            this.getForms(beneficiaryModel.get('uuid'));
        }, error => {
            this.navActions.updateAlertMessageState(
                new AlertItem({ alertType : EnumAlertType.ERROR, message : error }));
        });

        // hide the Beneficiary Form
        this.store.dispatch({
            type    : FormsActions.BENEFICIARY_FORM_UPDATE_FORM_VISIBLE,
            payload : false
        });
    }
}
