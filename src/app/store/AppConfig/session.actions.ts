import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {Observable} from 'rxjs';

import {IAppStore} from '../app-store';
import {SessionManagerService} from '../../shared/services/AppConfig/session-manager.service';

@Injectable()

export class SessionActions {
    static SESSION_START    : string = 'SESSION_START';
    static SESSION_TIMEOUT  : string = 'SESSION_TIMEOUT';
    static SESSION_END      : string = 'SESSION_END';

    /**
     * Default number of minutes for a session length.
     * @type {number}
     */
    static SESSION_TIMEOUT_DEFAULT : number = 15;

    /**
     *
     * @param store Redux store to dispatch app state changes.
     * @param sessionService API service to handle calls to server.
     */
    constructor(
        private store           : NgRedux<IAppStore>,
        private sessionService  : SessionManagerService) {
    }

    /**
     * Begins a new session.
     */
    begin() : Observable<any> {
        return Observable.create(observer => {
            let timeoutMinutes : number;

            this.sessionService
                .getSessionConfig()
                .subscribe(response => {
                        // collect the response from the service
                        timeoutMinutes = response.timeoutMinutes;

                        // dispatch the redux action
                        this.store.dispatch({
                            type: SessionActions.SESSION_START,
                            payload: timeoutMinutes
                        });

                        // let the next observer know what's up.
                        observer.next(timeoutMinutes);
                        observer.complete();
                    },
                    error => {
                        console.warn('An error occurred obtaining session configuration. Using default session length.');
                        // use a default value, something went wrong...
                        // dispatch the redux action
                        this.store.dispatch({
                            type: SessionActions.SESSION_START,
                            payload: SessionActions.SESSION_TIMEOUT_DEFAULT
                        });
                        observer.next(SessionActions.SESSION_TIMEOUT_DEFAULT);
                        observer.complete();
                    });
        });
    }

    /**
     * Ends a user's session
     */
    end() {
        this.store.dispatch({type: SessionActions.SESSION_END});
    }

}
