import {select} from '@angular-redux/store';
import {Observable} from 'rxjs';

export class SessionStateSelectors {
    constructor() {}

    /**
     * Expose the session timeout.
     * @returns {Observable<number>}
     */
    @select(state => state.sessionState.timeoutMinutes)
    timeoutMinutes : Observable<number>;
}
