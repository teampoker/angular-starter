import {IPayloadAction} from '../app-store';
import {SessionState} from './types/session-state.model';
import {SessionActions} from './session.actions';
import {INITIAL_SESSION_STATE} from './session.initial-state';

export const SESSION_STATE_REDUCER = (state : SessionState = INITIAL_SESSION_STATE, action : IPayloadAction) : SessionState => {
    switch (action.type) {
        case SessionActions.SESSION_START :
            state = state.set('timeoutMinutes', action.payload) as SessionState;

            break;
        case SessionActions.SESSION_END:
            break;
        default :
            return state;
    }

    return state;
};
