import {RequestOptions} from '@angular/http';
import {Observable} from 'rxjs';

import {SessionActions} from './session.actions';
import {MockRedux} from '../../../testing/ng2-redux-subs';
import {SessionManagerService} from '../../shared/services/AppConfig/session-manager.service';
import {SESSION_MOCK} from '../../shared/services/AppConfig/session-manager.service.mock';
import {BackendService} from '../../shared/services/Backend/backend.service';

/**
 * Mocked dependency of session actions, the service that can integrate with API.
 */
class MockSessionManagerService extends SessionManagerService {
    constructor(backend : BackendService) {
        super(backend);
    }
}

/**
 * Mocked backend service so we don't rely on actual API endpoints or HTTP services.
 */
class MockBackendService extends BackendService {
    constructor() {
        super(undefined, undefined);
    }

    /**
     * Set this to whatever fits your test scenario.
     */
    expectedResponse : any;

    /**
     * Overriding to return mocked expectation.  Using same contract as super class.
     * @param endpoint
     * @param handle
     * @param mock
     * @param data
     * @param options
     * @returns {any}
     */
    get(endpoint : string,
        handle : string,
        mock : any,
        data? : any,
        options? : RequestOptions) {

        return Observable.create(observer => {
            if (this.expectedResponse) {
                observer.next(this.expectedResponse);
            }
            else {
                observer.error(undefined);
            }
            observer.complete();
        });
    }
}

describe('Action: SessionActions', () => {
    let sessionActions      : SessionActions,
        sessionMgrService   : SessionManagerService,
        store               : MockRedux,
        backend             : MockBackendService;

    beforeEach(() => {
        store             = new MockRedux();
        backend           = new MockBackendService();
        sessionMgrService = new MockSessionManagerService(backend as BackendService);
        sessionActions    = new SessionActions(store, sessionMgrService);
    });

    it('should dispatch session start action using service value', () => {
        // arrange
        spyOn(store, 'dispatch');

        backend.expectedResponse = SESSION_MOCK.getSessionConfig;

        // act
        sessionActions
            .begin()
            .subscribe(value => {
                // assert
                expect(value).toBeDefined();
                expect(value).toEqual(SESSION_MOCK.getSessionConfig[0].timeoutMinutes);

                // ensure the redux store got what we expect
                expect(store.dispatch).toHaveBeenCalledWith({
                    type    : SessionActions.SESSION_START,
                    payload : SESSION_MOCK.getSessionConfig[0].timeoutMinutes
                });
            });
    });

    it('should have a default timeout defined', () => {
        expect(SessionActions.SESSION_TIMEOUT_DEFAULT).toBeDefined();
        expect(SessionActions.SESSION_TIMEOUT_DEFAULT).toBeGreaterThan(0);
    });

    it('should use default session length in absence of service value', () => {
        // arrange
        spyOn(store, 'dispatch');

        backend.expectedResponse = null;

        // act
        sessionActions
            .begin()
            .subscribe(value => {
                    // assert
                    expect(value).toBeDefined();
                    expect(value).toEqual(SessionActions.SESSION_TIMEOUT_DEFAULT);

                    // ensure the redux store got what we expect
                    expect(store.dispatch).toHaveBeenCalledWith({
                        type    : SessionActions.SESSION_START,
                        payload : SESSION_MOCK.getSessionConfig[0].timeoutMinutes
                    });
                },
                error => {

                });
    });

    it('should dispatch session end action', () => {
        spyOn(store, 'dispatch');
        sessionActions.end();
        expect(store.dispatch).toHaveBeenCalledWith({type : SessionActions.SESSION_END});
    });
});
