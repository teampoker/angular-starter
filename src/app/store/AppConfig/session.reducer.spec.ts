import {SESSION_STATE_REDUCER} from './session.reducer';
import {IPayloadAction} from '../app-store';
import {SessionActions} from './session.actions';
import {SessionState} from './types/session-state.model';
import {INITIAL_SESSION_STATE} from './session.initial-state';

describe('Reducer: SessionReducer', () => {

    it('should return the initial state', () => {
        expect(
            SESSION_STATE_REDUCER(undefined, {} as IPayloadAction)
        )
            .toEqual(jasmine.objectContaining(
                {
                    timeoutMinutes : 0
                })
            );
    });

    it('should handle SESSION_START', () => {
        const SESSION_LENGTH : number = 10;
        expect(
            SESSION_STATE_REDUCER(undefined, {
                type    : SessionActions.SESSION_START,
                payload : SESSION_LENGTH
            }))
            .toEqual(jasmine.objectContaining(
                {
                    timeoutMinutes : SESSION_LENGTH
                })
            );

        // ensure that the new state value contains the payload provided
        expect(
            SESSION_STATE_REDUCER(INITIAL_SESSION_STATE, {
                type    : SessionActions.SESSION_START,
                payload : SESSION_LENGTH
            }))
            .toEqual(jasmine.objectContaining(
                {
                    timeoutMinutes : SESSION_LENGTH
                })
            );
    });

    it('should handle SESSION_END', () => {
        const SESSION_LENGTH : number = 14;
        // established session timeout is not destroyed on session end
        expect(
            SESSION_STATE_REDUCER({timeoutMinutes : SESSION_LENGTH} as SessionState, {
                type : SessionActions.SESSION_END
            }))
            .toEqual(jasmine.objectContaining(
                {
                    timeoutMinutes : SESSION_LENGTH
                })
            );

    });
});
