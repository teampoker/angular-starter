import {SessionState} from './types/session-state.model';

export const INITIAL_SESSION_STATE = new SessionState();
