import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ISessionState {
    timeoutMinutes : number;
}

export const SESSION_STATE = Record({
    timeoutMinutes : 0
});

export class SessionState extends SESSION_STATE {
    timeoutMinutes : number;

    constructor(values? : SessionState | ISessionState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof SessionState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
