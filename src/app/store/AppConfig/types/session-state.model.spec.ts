import {SessionState} from './session-state.model';

describe('Model: SessionState', () => {

    it('should be instantiated without constructor params', () => {
        expect(
            new SessionState()
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        timeoutMinutes : 0
                    })
            );
    });

    it('should be instantiated from an undefined param', () => {
        expect(
            new SessionState(undefined)
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        timeoutMinutes : 0
                    }
                )
            );
    });

    it('should be instantiated from bracket notation', () => {
        const SESSION_TIMEOUT_PROPERTY_VALUE : number = 14;
        expect(
            new SessionState({timeoutMinutes : SESSION_TIMEOUT_PROPERTY_VALUE})
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        timeoutMinutes : SESSION_TIMEOUT_PROPERTY_VALUE
                    }
                )
            );
    });

    it('should be instantiated using another model object', () => {
        const SESSION_TIMEOUT_PROPERTY_VALUE : number = 42;
        expect(
            new SessionState(new SessionState({timeoutMinutes : SESSION_TIMEOUT_PROPERTY_VALUE}))
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        timeoutMinutes : SESSION_TIMEOUT_PROPERTY_VALUE
                    }
                )
            );
    });
});
