import {async} from '@angular/core/testing';
import {Observable} from 'rxjs/Observable';
import * as moment from 'moment';

import {DashboardActions} from './dashboard.actions';
import {MockRedux} from '../../../testing/ng2-redux-subs';
import {DashboardService} from '../../areas/Dashboard/services/Dashboard/dashboard.service';
import {CallActivity} from './types/call-activity.model';
import {EligibilityQueue} from './types/eligibility-queue.model';
import {DashboardInfo} from './types/dashboard-info.model';

// Mock the DashboardService with the methods we need to trigger
class MockDashboardService extends DashboardService {
    constructor() {
        super(undefined);
    }

    getCallActivity() {
        return Observable.create(observer => {
            // update Redux store
            observer.next(new CallActivity());
        });
    }

    getEligibilityQueue() {
        return Observable.create(observer => {
            // update Redux store
            observer.next(new EligibilityQueue());
        });
    }

    dateTimeRefresh() {
        return Observable.create(observer => {
            observer.next(moment().format());
        });
    }
}

describe('Dashboard Action Creators', () => {
    let dashboardActions       : DashboardActions,
        mockRedux              : MockRedux,
        dashboardService       : DashboardService;

    // setup tasks to perform before each test
    beforeEach(() => {
        // Initialize mock NgRedux and create a new instance of the
        // ActionCreator Service to be tested.
        mockRedux              = new MockRedux();
        dashboardService       = new MockDashboardService();
        dashboardActions       = new DashboardActions(mockRedux, dashboardService);
    });

    // test definitions
    it('dashboardRefresh method should dispatch DASHBOARD_REFRESH action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : DashboardActions.DASHBOARD_REFRESH,
            payload : new DashboardInfo()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        dashboardActions.dashboardRefresh(new DashboardInfo());

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('updateCallActivity method should dispatch CALL_ACTIVITY_UPDATE action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : DashboardActions.CALL_ACTIVITY_UPDATE,
            payload : new CallActivity()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        dashboardActions.updateCallActivity(new CallActivity());

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('currentDateTimeRefresh method should dispatch CURRENT_DATE_TIME_REFRESH action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : DashboardActions.CURRENT_DATE_TIME_REFRESH,
            payload : moment().format()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        dashboardActions.currentDateTimeRefresh();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('getCallActivity method should dispatch CALL_ACTIVITY_UPDATE action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : DashboardActions.CALL_ACTIVITY_UPDATE,
            payload : new CallActivity()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        dashboardActions.getCallActivity();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));

    it('getEligibilityQueue method should dispatch ELIGIBILITY_QUEUE_UPDATE action', async(() => {
        // action expected to be dispatched
        const expectedAction = {
            type    : DashboardActions.ELIGIBILITY_QUEUE_UPDATE,
            payload : new EligibilityQueue()
        };

        // spy on mock dispatch method
        spyOn(mockRedux, 'dispatch');

        dashboardActions.getEligibilityQueue();

        expect(mockRedux.dispatch).toHaveBeenCalled();
        expect(mockRedux.dispatch).toHaveBeenCalledWith(expectedAction);
    }));
});
