import {INITIAL_DASHBOARD_STATE} from './dashboard.initial-state';
import {DashboardActions} from './dashboard.actions';
import {IPayloadAction} from '../app-store';
import {DashboardState} from './types/dashboard-state.model';
import {DashboardInfo} from './types/dashboard-info.model';
import {CallActivity} from './types/call-activity.model';
import {EligibilityQueue} from './types/eligibility-queue.model';

/**
 * App Dashboard State Reducer
 *
 * @param state
 * @param action
 * @returns {any}
 * @constructor
 */
export const DASHBOARD_STATE_REDUCER = (state : DashboardState = INITIAL_DASHBOARD_STATE, action : IPayloadAction) : DashboardState => {
    switch (action.type) {
        case DashboardActions.DASHBOARD_REFRESH :
            state = state.merge({ dashboard : new DashboardInfo(action.payload) }) as DashboardState;

            break;
        case DashboardActions.CALL_ACTIVITY_UPDATE :
            state = state.merge({ callActivity : new CallActivity(action.payload) }) as DashboardState;

            break;
        case DashboardActions.ELIGIBILITY_QUEUE_UPDATE :
            state = state.merge({ eligibilityQueue : new EligibilityQueue(action.payload) }) as DashboardState;

            break;
        case DashboardActions.CURRENT_DATE_TIME_REFRESH :
            state = state.setIn(['dashboard', 'currentDateTime'], action.payload) as DashboardState;

            break;
        default:
            return state;
    }

    return state;
};
