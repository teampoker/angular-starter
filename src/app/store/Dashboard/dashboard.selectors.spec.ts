import {async} from '@angular/core/testing';

import {DashboardStateSelectors} from './dashboard.selectors';
import {DashboardInfo} from './types/dashboard-info.model';
import {CallActivity} from './types/call-activity.model';
import {EligibilityQueue} from './types/eligibility-queue.model';
import {MockRedux} from '../../../testing/ng2-redux-subs';
import {ROOT_REDUCER} from '../app-store';

describe('Dashboard State Selectors', () => {
    let dashboardStateSelectors : DashboardStateSelectors,
        mockRedux               : MockRedux;

    // setup tasks to perform before each test
    beforeEach(() => {
        mockRedux = new MockRedux();

        mockRedux.configureStore(ROOT_REDUCER, {}, undefined, undefined);

        // Initialize mock NgRedux and create a new instance of the
        dashboardStateSelectors = new DashboardStateSelectors(mockRedux);
    });

    // test definitions
    it('dashboard method should return DashboardState.dashboard subscription', async(() => {
        // subscribe to Redux store updates
        dashboardStateSelectors.dashboard().first().subscribe(val => {
            // check returned state for correct type
            expect(val instanceof DashboardInfo).toEqual(true);
        });
    }));

    it('callActivity method should return DashboardState.callActivity subscription', async(() => {
        // subscribe to Redux store updates
        dashboardStateSelectors.callActivity().first().subscribe(val => {
            // check returned state for correct type
            expect(val instanceof CallActivity).toEqual(true);
        });
    }));

    it('eligibilityQueue method should return DashboardState.eligibilityQueue subscription', async(() => {
        // subscribe to Redux store updates
        dashboardStateSelectors.eligibilityQueue().first().subscribe(val => {
            // check returned state for correct type
            expect(val instanceof EligibilityQueue).toEqual(true);
        });
    }));

    it('currentDateTime method should return DashboardState.currentDateTime subscription', async(() => {
        // subscribe to Redux store updates
        dashboardStateSelectors.currentDateTime().first().subscribe(val => {
            // check returned state for correct type
            expect(val).toEqual('0');
        });
    }));
});
