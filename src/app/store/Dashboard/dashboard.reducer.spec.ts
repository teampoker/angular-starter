import {INITIAL_DASHBOARD_STATE} from './dashboard.initial-state';
import {DASHBOARD_STATE_REDUCER} from './dashboard.reducer';
import * as moment from 'moment';

import {DashboardActions} from './dashboard.actions';
import {DashboardInfo} from './types/dashboard-info.model';
import {CallActivity} from './types/call-activity.model';
import {EligibilityQueue} from './types/eligibility-queue.model';

describe('Dashboard State Reducer', () => {
    it('should init initial state', () => {
        expect(
            DASHBOARD_STATE_REDUCER(undefined, {
                type    : undefined,
                payload : undefined
            })
        )
        .toEqual(INITIAL_DASHBOARD_STATE);
    });

    it('should handle DASHBOARD_REFRESH action', () => {
        expect(
            DASHBOARD_STATE_REDUCER(undefined, {
                type    : DashboardActions.DASHBOARD_REFRESH,
                payload : new DashboardInfo()
            })
            .get('dashboard') instanceof DashboardInfo
        )
        .toEqual(true);
    });

    it('should handle CALL_ACTIVITY_UPDATE action', () => {
        expect(
            DASHBOARD_STATE_REDUCER(undefined, {
                type    : DashboardActions.CALL_ACTIVITY_UPDATE,
                payload : new CallActivity()
            })
            .get('callActivity') instanceof CallActivity
        )
        .toEqual(true);
    });

    it('should handle ELIGIBILITY_QUEUE_UPDATE action', () => {
        expect(
            DASHBOARD_STATE_REDUCER(undefined, {
                type    : DashboardActions.ELIGIBILITY_QUEUE_UPDATE,
                payload : new EligibilityQueue()
            })
            .get('eligibilityQueue') instanceof EligibilityQueue
        )
        .toEqual(true);
    });

    it('should handle CURRENT_DATE_TIME_REFRESH action', () => {
        expect(
            DASHBOARD_STATE_REDUCER(undefined, {
                type    : DashboardActions.CURRENT_DATE_TIME_REFRESH,
                payload : moment().format()
            })
            .getIn(['dashboard', 'currentDateTime'])
        )
        .toEqual(moment().format());
    });
});
