import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {IAppStore} from '../app-store';

import {DashboardService} from '../../areas/Dashboard/services/Dashboard/dashboard.service';
import {DashboardInfo} from './types/dashboard-info.model';
import {EligibilityQueue} from './types/eligibility-queue.model';

@Injectable()

export class DashboardActions {
    static DASHBOARD_REFRESH            : string = 'DASHBOARD_REFRESH';
    static CALL_ACTIVITY_UPDATE         : string = 'CALL_ACTIVITY_UPDATE';
    static CURRENT_DATE_TIME_REFRESH    : string = 'CURRENT_DATE_TIME_REFRESH';
    static ELIGIBILITY_QUEUE_UPDATE     : string = 'ELIGIBILITY_QUEUE_UPDATE';

    constructor(
        private store               : NgRedux<IAppStore>,
        private dashboardService    : DashboardService
    ) {}

    dashboardRefresh(data? : DashboardInfo) {
        this.store.dispatch({
            type    : DashboardActions.DASHBOARD_REFRESH,
            payload : data
        });
    }

    /**
     * update the callActivity state
     */
    updateCallActivity(data : any) {
        this.store.dispatch({
            type    : DashboardActions.CALL_ACTIVITY_UPDATE,
            payload : data
        });
    }

    /**
     * update the eligibilityQueue state
     */
    updateEligibilityQueue(data : EligibilityQueue) {
        this.store.dispatch({
            type    : DashboardActions.ELIGIBILITY_QUEUE_UPDATE,
            payload : data
        });
    }

    /**
     * refresh our currentDateTime string
     */
    currentDateTimeRefresh() {
        this.dashboardService.dateTimeRefresh().subscribe(response => {
            // update update current date time
            this.store.dispatch({
                type    : DashboardActions.CURRENT_DATE_TIME_REFRESH,
                payload : response
            });
        }, error => {

        });
    }

    /**
     * Connects to the API gateway.
     *
     * retrieves call details for callActivity
     */
    getCallActivity() {
        // query for callActivity information
        this.dashboardService.getCallActivity().subscribe(response => {
            // update beneficiary results
            this.updateCallActivity(response);
        }, error => {
            this.updateCallActivity(undefined);
        });
    }

    /**
     * Connects to the API gateway.
     *
     * retrieves details for eligibilityQueue
     */
    getEligibilityQueue() {
        // query for eligibilityQueue information
        this.dashboardService.getEligibilityQueue().subscribe(response => {
            // update beneficiary results
            this.updateEligibilityQueue(response);
        }, error => {
            this.updateEligibilityQueue(undefined);
        });
    }
}
