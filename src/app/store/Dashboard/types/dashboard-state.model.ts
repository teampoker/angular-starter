import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IDashboardInfo,
    DashboardInfo
} from './dashboard-info.model';

import {
    IEligibilityQueue,
    EligibilityQueue
} from './eligibility-queue.model';

import {
    ICallActivity,
    CallActivity
} from './call-activity.model';

export interface IDashboardState {
    dashboard           : IDashboardInfo;
    callActivity        : ICallActivity;
    eligibilityQueue    : IEligibilityQueue;
}

export const DASHBOARD_STATE = Record({
    dashboard           : new DashboardInfo(),
    callActivity        : new CallActivity(),
    eligibilityQueue    : new EligibilityQueue()
});

/**
 * type definition for Redux Store dashboard state
 */
export class DashboardState extends DASHBOARD_STATE {
    dashboard           : DashboardInfo;
    callActivity        : CallActivity;
    eligibilityQueue    : EligibilityQueue;

    constructor(values? : DashboardState | IDashboardState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof DashboardState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // dashboard
                convertedValues = convertedValues.set('dashboard', new DashboardInfo(convertedValues.get('dashboard')));

                // callActivity
                convertedValues = convertedValues.set('callActivity', new CallActivity(convertedValues.get('callActivity')));

                // eligibilityQueue
                convertedValues = convertedValues.set('eligibilityQueue', new EligibilityQueue(convertedValues.get('eligibilityQueue')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
