import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IDashboardInfo {
    currentDateTime : string;
}

export const DASHBOARD_INFO = Record({
    currentDateTime : '0'
});

export class DashboardInfo extends DASHBOARD_INFO {
    currentDateTime : string;

    constructor(values? : DashboardInfo | IDashboardInfo) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof DashboardInfo) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
