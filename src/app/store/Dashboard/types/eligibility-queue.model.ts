import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface IEligibilityQueue {
    nextDay               : number;
    urgent                : number;
    standard              : number;
}

export const ELIGIBILITY_QUEUE = Record({
    nextDay               : 0,
    urgent                : 0,
    standard              : 0
});

/**
 * type definition for Redux Store eligibilityQueue state
 */
export class EligibilityQueue extends ELIGIBILITY_QUEUE {
    nextDay               : number;
    urgent                : number;
    standard              : number;

    constructor(values? : EligibilityQueue | IEligibilityQueue) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof EligibilityQueue) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
