import {
    fromJS,
    Map,
    Record
} from 'immutable';

export interface ICallActivity {
    callsReceived         : number;
    callsPlaced           : number;
    avgCallTime           : string;
}

export const CALL_ACTIVITY = Record({
    callsReceived         : 0,
    callsPlaced           : 0,
    avgCallTime           : '0'
});

/**
 * type definition for Redux Store callActivity state
 */
export class CallActivity extends CALL_ACTIVITY {

    constructor(values? : CallActivity | ICallActivity) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof CallActivity) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
