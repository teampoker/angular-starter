import {DashboardState} from './types/dashboard-state.model';

export const INITIAL_DASHBOARD_STATE = new DashboardState();
