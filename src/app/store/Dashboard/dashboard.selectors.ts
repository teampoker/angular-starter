import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../app-store';
import {DashboardInfo} from './types/dashboard-info.model';
import {CallActivity} from './types/call-activity.model';
import {EligibilityQueue} from './types/eligibility-queue.model';

@Injectable()

/**
 * implementation for DashboardStateSelectors: responsible for exposing custom state subscriptions to DashboardState
 */
export class DashboardStateSelectors {
    /**
     * DashboardStateSelectors constructor
     * @param store
     */
    constructor(private store : NgRedux<IAppStore>) {}

    /**
     * expose Observable to DashboardState.dashboard
     * @returns {Observable<DashboardInfo>}
     */
    dashboard() : Observable<DashboardInfo> {
        return this.store.select(state => state.dashboardState.get('dashboard'));
    }

    /**
     * expose Observable to DashboardState.dashboard.currentDateTime
     * @returns {Observable<string>}
     */

    currentDateTime() : Observable<string> {
        return this.store.select(state => state.dashboardState.getIn(['dashboard', 'currentDateTime']));
    }

    /**
     * expose Observable to DashboardState.callActivity
     * @returns {Observable<CallActivity>}
     */
    callActivity() : Observable<CallActivity> {
        return this.store.select(state => state.dashboardState.get('callActivity'));
    }

    /**
     * expose Observable to DashboardState.eligibilityQueue
     * @returns {Observable<EligibilityQueue>}
     */

    eligibilityQueue() : Observable<EligibilityQueue> {
        return this.store.select(state => state.dashboardState.get('eligibilityQueue'));
    }
}
