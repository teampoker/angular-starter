import {
    TestBed,
    inject
} from '@angular/core/testing';
import {
    NgRedux,
    NgReduxModule
} from '@angular-redux/store';
import {HttpModule} from '@angular/http';
import {Observable} from 'rxjs';

import {IAppStore} from '../app-store';
import {UserActions} from './user.actions';
import {UserService} from '../../shared/services/User/user.service';
import {USER_MOCK} from '../../shared/services/User/user.service.mocks';
import {APIMockService} from '../../shared/services/Mock/api-mock.service';
import {BackendService} from '../../shared/services/Backend/backend.service';
import {UserInfoState} from './types/user-info-state.model';
import {UserStateSelectors} from './user.selectors';
import {NavActions} from '../Navigation/nav.actions';
import {AlertItem} from '../Navigation/types/alert-item.model';

/**
 * Mocked user service
 */
export class MockUserService extends UserService {
    constructor() {
        super(undefined);
    }

    getUserResponse : any;

    getUser() : Observable<any> {
        return Observable.create(observer => {
            if (this.getUserResponse) {
                if (this.getUserResponse.error) {
                    observer.error(this.getUserResponse);
                }
                else {
                    observer.next(this.getUserResponse);
                }
            }
            else {
                observer.error(undefined);
            }
        });
    }
}

/**
 * Mocked Navigation Action Creator service
 */
class MockNavActions extends NavActions {
    constructor(private injectedStore : NgRedux<IAppStore>) {
        super(
            injectedStore,
            undefined,
            undefined,
            undefined
        );
    }
}

describe('User Action Creators', () => {
    let userActions     : UserActions,
        reduxStore      : NgRedux<IAppStore>,
        mockService     : MockUserService,
        userState       : UserStateSelectors;

    beforeAll(() => {
        // HACK: prevent errors in karma when user service sets window.location.href
        window.onbeforeunload = () => 'Oh no you didn\'t';
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports   : [
                HttpModule,
                NgReduxModule
            ],
            providers : [
                UserActions,
                {
                    provide  : UserService,
                    useClass : MockUserService
                },
                APIMockService,
                BackendService,
                UserStateSelectors,
                {
                    provide  : NavActions,
                    useClass : MockNavActions,
                    deps     : [
                        NgRedux
                    ]
                }
            ]
        });
    });

    // setup tasks to perform before each test
    beforeEach(
        inject([
                UserActions,
                UserService,
                NgRedux,
                UserStateSelectors
            ],
            (
                actions,
                service,
                store,
                stateSelector
            ) => {

                reduxStore  = store;
                mockService = service;
                userActions = actions;
                userState   = stateSelector;

                // configure the redux store and ensure it's been initialized to our satisfaction
                expect(reduxStore).toBeDefined('redux store not injected or initialized');
                expect(reduxStore.dispatch).toBeDefined('redux store is missing dispatch()');

                // create the spy in case any tests need to sniff around
                spyOn(reduxStore, 'dispatch');

            }));

    it('userLogin() should dispatch USER_LOGOUT action', () => {
        const expectedAction = {
            type : UserActions.USER_LOGIN
        };

        userActions.userLogin();

        expect(reduxStore.dispatch).toHaveBeenCalled();
        expect(reduxStore.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    it('userLogout() should dispatch USER_LOGOUT action', () => {
        const expectedAction : any = {
            type : UserActions.USER_LOGOUT
        };

        userActions.userLogout();

        expect(reduxStore.dispatch).toHaveBeenCalled();
        expect(reduxStore.dispatch).toHaveBeenCalledWith(expectedAction);
    });

    describe('userInfo()', () => {
        it('should pull user info from API into store', () => {
            const expectedPayload : any = UserInfoState.fromApi(USER_MOCK.getUserInfo[0]);

            mockService.getUserResponse = USER_MOCK.getUserInfo[0];

            // trigger the action to get info
            userActions.getInfo();

            expect(reduxStore.dispatch).toHaveBeenCalled();
            expect(reduxStore.dispatch).toHaveBeenCalledWith({
                type : UserActions.USER_INFO_REQUESTED
            });

            expect(reduxStore.dispatch).toHaveBeenCalledWith({
                type    : UserActions.USER_INFO_RECEIVED,
                payload : expectedPayload
            });
        });

        it('should blank out user info when API error', () => {
            // setup the user service API response to use
            mockService.getUserResponse = {
                timestamp : 1487181805554,
                status    : 404,
                error     : 'Not Found',
                message   : 'No message available',
                path      : '/userInfo'
            };

            // trigger the action to get info
            userActions.getInfo();

            expect(reduxStore.dispatch).toHaveBeenCalled();
            expect(reduxStore.dispatch).toHaveBeenCalledWith({
                type : UserActions.USER_INFO_REQUESTED
            });

            expect(reduxStore.dispatch).toHaveBeenCalledWith({
                type    : UserActions.USER_INFO_RECEIVED,
                payload : undefined
            });
        });

        it('should blank out user info when API provides bad response', () => {
            // setup the user service API response to use
            mockService.getUserResponse = undefined;

            // trigger the action to get info
            userActions.getInfo();

            expect(reduxStore.dispatch).toHaveBeenCalled();

            const expectedCalls : Array<any> = [
                {
                    type : UserActions.USER_INFO_REQUESTED
                },
                {
                    type    : 'ALERT_MESSAGE_UPDATE',
                    payload : new AlertItem({
                        alertType : 2,
                        message   : 'User service returned error: undefined',
                        durable   : false
                    })
                },
                {
                    type    : UserActions.USER_INFO_RECEIVED,
                    payload : undefined
                }
            ];
            expect(reduxStore.dispatch).toHaveBeenCalledTimes(expectedCalls.length);

            // can't for the life of me figure out how to verify all 3 calls...
            expect(reduxStore.dispatch).toHaveBeenCalledWith({
                type : UserActions.USER_INFO_REQUESTED
            });

            expect(reduxStore.dispatch).toHaveBeenCalledWith({
                type    : UserActions.USER_INFO_RECEIVED,
                payload : undefined
            });
        });
    });
});
