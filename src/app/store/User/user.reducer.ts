import {INITIAL_USER_STATE} from './user.initial-state';
import {IPayloadAction} from '../app-store';
import {UserState} from './types/user-state.model';
import {UserActions} from './user.actions';

/**
 * Reducer function for Redux store actions.
 *
 * @param state
 * @param action Redux action that should conform to the payload action interface.
 * @returns {any} Returns the Redux state that's specific to UserState
 * @constructor
 */
export const USER_STATE_REDUCER : any = (state : UserState = INITIAL_USER_STATE, action : IPayloadAction | any) : UserState => {
    switch (action.type) {
        case UserActions.USER_LOGIN :
            state = state.set('userAuthenticated', true) as UserState;
            break;

        case UserActions.USER_LOGOUT :
            state = state.set('userAuthenticated', false) as UserState;
            break;

        case UserActions.USER_INFO_REQUESTED:
            state = state.set('isFetching', true) as UserState;
            break;

        case UserActions.USER_INFO_RECEIVED:
            state = state.withMutations(record => {
                record
                    .set('isFetching', false)
                    .set('userInfo', action.payload);
            }) as UserState;
            break;

        default :
            return state;
    }

    return state;
};
