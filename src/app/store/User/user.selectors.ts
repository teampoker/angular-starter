import {Injectable} from '@angular/core';
import {
    select
} from '@angular-redux/store';
import {Observable} from 'rxjs';

@Injectable()

/**
 * implementation for UserStateSelectors: responsible for exposing custom state subscriptions to UserState
 */
export class UserStateSelectors {
    /**
     * UserStateSelectors constructor
     */
    constructor() {}

    /**
     * Flag that indicates if the user is presently authenticated for the app.
     * @returns {Observable<boolean>} true, if the user has been authenticated. False, if not.
     */
    @select(state => state.userState.userAuthenticated)
    isUserAuthenticated : Observable<boolean>;

    /**
     * The current user's system username.
     * @returns {Observable<string>}
     */
    @select(state => state.userState.userInfo.name)
    userName : Observable<string>;
}
