import {UserState} from './types/user-state.model';

export const INITIAL_USER_STATE : UserState = new UserState();
