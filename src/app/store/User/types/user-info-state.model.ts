import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

export interface IUserInfoState {
    bestTimeToCall      : string;
    city                : string;
    companyName         : string;
    contactPreference   : Array<string>;
    DOB                 : string;
    email               : string;
    employeeId          : string;
    firstName           : string;
    gender              : string;
    id                  : string;
    isActive            : boolean;
    lastName            : string;
    language            : string;
    middleName          : string;
    mobilePhone         : string;
    phoneType           : string;
    name                : string;
    phone               : string;
    postalCode          : number;
    SSN                 : string;
    state               : string;
    street              : string;
    timeZone            : string;
    title               : string;
    userName            : string;
    userType            : string;
}

export const userInfo = Record({
    bestTimeToCall    : '',
    city              : '',
    companyName       : '',
    contactPreference : List<string>(),
    DOB               : '',
    email             : '',
    employeeId        : '',
    firstName         : '',
    gender            : '',
    id                : '',
    isActive          : false,
    lastName          : '',
    language          : '',
    middleName        : '',
    mobilePhone       : '',
    phoneType         : '',
    name              : '',
    phone             : '',
    postalCode        : 0,
    SSN               : '',
    state             : '',
    street            : '',
    timeZone          : '',
    title             : '',
    userName          : '',
    userType          : ''
});

/**
 * Immutable data property mapping between the structure returned by the API, and this Map.
 *
 * Key      = [API DATA KEY]
 * Value    = [IMMUTABLE PROPERTY NAME]
 *
 * @type {Map<K, V>|Map<string, V>|any}
 */
const apiMap = Map({
    fullName  : 'name',
    firstName : 'firstName',
    lastName  : 'lastName',
    userName  : 'userName'
});

export class UserInfoState extends userInfo {
    bestTimeToCall      : string;
    city                : string;
    companyName         : string;
    contactPreference   : Array<string>;
    DOB                 : string;
    email               : string;
    employeeId          : string;
    firstName           : string;
    gender              : string;
    id                  : string;
    isActive            : boolean;
    lastName            : string;
    language            : string;
    middleName          : string;
    mobilePhone         : string;
    phoneType           : string;
    name                : string;
    phone               : string;
    postalCode          : number;
    SSN                 : string;
    state               : string;
    street              : string;
    timeZone            : string;
    title               : string;
    userName            : string;
    userType            : string;

    constructor(values? : UserInfoState | IUserInfoState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof UserInfoState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);
            }

            // contactPreference
            convertedValues = convertedValues.set('contactPreference', List(convertedValues.get('contactPreference')));
        }

        // call parent constructor
        super(convertedValues);
    }

    /**
     * Map the JSON object to properties we understand.
     *
     * NOTE: Anything NOT defined in the map constant gets dropped. All your base are belong to us.
     *
     * @param rawObject
     * @returns {UserInfoState}
     */
    static fromApi(rawObject : any) : UserInfoState {
        const newObject : any = {};
        apiMap.forEach((translatedKey, apiKey) => {
            newObject[translatedKey] = rawObject[apiKey];
        });
        return new UserInfoState(newObject);
    }
}
