import {USER_MOCK} from '../../../shared/services/User/user.service.mocks';
import {UserInfoState} from './user-info-state.model';

describe('Model: UserInfoState', () => {

    it('should convert API values', () => {
        const mockData : any           = USER_MOCK.getUserInfo[0];
        const userInfo : UserInfoState = UserInfoState.fromApi(mockData);
        expect(userInfo).toBeDefined();
        expect(userInfo).toEqual(
            jasmine.objectContaining(
                {
                    firstName : mockData.firstName,
                    lastName  : mockData.lastName,
                    name      : mockData.fullName
                }
            )
        );
    });

    it('should drop API return values that are not mapped', () => {
        const mockData : any           = USER_MOCK.getUserInfo[0];
        mockData.fakeField             = 'licking windows and eating crayons means I\'m special...or a dog';
        const userInfo : UserInfoState = UserInfoState.fromApi(mockData);

        expect(userInfo).toBeDefined();
        expect(userInfo).toEqual(
            jasmine.objectContaining(
                {
                    firstName : mockData.firstName,
                    lastName  : mockData.lastName,
                    name      : mockData.fullName
                }
            )
        );
        expect(userInfo.contains(mockData.fakeField)).toBeFalsy('Expected fakeField property to be dropped.');
    });

    it('should be able to construct itself...using itself', () => {
        const mockData : any                   = USER_MOCK.getUserInfo[0];
        const originalUserInfo : UserInfoState = UserInfoState.fromApi(mockData);
        const newUserInfo : UserInfoState      = new UserInfoState(originalUserInfo);

        expect(newUserInfo).toBeDefined('user info was not defined.');
        expect(newUserInfo.firstName).toEqual(mockData.firstName);
    });
});
