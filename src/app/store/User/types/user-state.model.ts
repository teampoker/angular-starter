import {
    fromJS,
    Map,
    Record
} from 'immutable';

import {
    IUserInfoState,
    UserInfoState
} from './user-info-state.model';
import {
    IUserProfileState,
    UserProfileState
} from './user-profile-state.model';

export interface IUserState {
    isFetching          : boolean;
    userAuthenticated   : boolean;
    userPasswordReset   : boolean;
    userInfo            : IUserInfoState;
    userProfileState    : IUserProfileState;
}

export const USER_STATE = Record({
    isFetching          : false,
    userAuthenticated   : false,
    userPasswordReset   : false,
    userInfo            : new UserInfoState(),
    userProfileState    : new UserProfileState()
});

/**
 * type definition for Redux Store user state
 */
export class UserState extends USER_STATE {
    isFetching          : boolean;
    userAuthenticated   : boolean;
    userPasswordReset   : boolean;
    userInfo            : UserInfoState;
    userProfileState    : UserProfileState;

    constructor(values? : UserState | IUserState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof UserState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // UserInfoState
                convertedValues = convertedValues.set('userInfo', new UserInfoState(convertedValues.get('userInfo')));

                // UserProfileState
                convertedValues = convertedValues.set('userProfileState', new UserProfileState(convertedValues.get('userProfileState')));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
