import {
    fromJS,
    List,
    Map,
    Record
} from 'immutable';

import {
    IDecisionGroupOption,
    DecisionGroupOption
} from './decision-group-option.model';

export interface IUserProfileState {
    contactPreferenceOptions    : Array<IDecisionGroupOption>;
    personalInfoEditing         : boolean;
    personalInfoExpanded        : boolean;
    communicationExpanded       : boolean;
}

export const userProfileState = Record({
    contactPreferenceOptions : List([
        {
            label   : 'Email',
            checked : false
        },
        {
            label   : 'Paper Mail',
            checked : false
        },
        {
            label   : 'Text',
            checked : false
        }
    ]).map(value => new DecisionGroupOption(value)),
    personalInfoEditing         : false,
    personalInfoExpanded        : true,
    communicationExpanded       : true
});

export class UserProfileState extends userProfileState {
    contactPreferenceOptions    : List<DecisionGroupOption>;
    personalInfoEditing         : boolean;
    personalInfoExpanded        : boolean;
    communicationExpanded       : boolean;

    constructor(values? : UserProfileState | IUserProfileState) {
        let convertedValues : Map<any, any>;

        // check for defined values
        if (values) {
            // is this already correct Immutable Record type?
            if (values instanceof UserProfileState) {
                // convert to Map
                convertedValues = Map(values);
            }
            else {
                // convert to immutable
                convertedValues = fromJS(values);

                // contactPreferenceOptions
                convertedValues = convertedValues.set('contactPreferenceOptions', List(convertedValues.get('contactPreferenceOptions', []).map(value => new DecisionGroupOption(value))));
            }
        }

        // call parent constructor
        super(convertedValues);
    }
}
