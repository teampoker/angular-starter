import {USER_STATE_REDUCER} from './user.reducer';
import {INITIAL_USER_STATE} from './user.initial-state';
import {UserActions} from './user.actions';
import {UserState} from './types/user-state.model';
import * as USER_INFO_STATE from './types/user-info-state.model';

describe('Reducer: UserReducer', () => {

    it('should return the initial state', () => {
        expect(
            USER_STATE_REDUCER(undefined, {})
        )
            .toEqual(INITIAL_USER_STATE);
    });

    it('should indicate user authenticated when USER_LOGIN', () => {
        expect(
            USER_STATE_REDUCER(new UserState(), {type : UserActions.USER_LOGIN})
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        userAuthenticated : true
                    }
                )
            );
    });

    it('should capture user NOT authenticated when USER_LOGOUT', () => {
        expect(
            USER_STATE_REDUCER(new UserState(), {type : UserActions.USER_LOGOUT})
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        userAuthenticated : false
                    }
                )
            );
    });

    it('should indicate user is being fetched', () => {
        expect(
            USER_STATE_REDUCER(
                new UserState(),
                {
                    type : UserActions.USER_INFO_REQUESTED
                })
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        isFetching : true
                    }
                )
            );
    });

    it('should retain user information when received', () => {
        expect(
            USER_STATE_REDUCER(
                new UserState(),
                {
                    type    : UserActions.USER_INFO_RECEIVED,
                    payload : USER_INFO_STATE
                }
            )
        )
            .toEqual(
                jasmine.objectContaining(
                    {
                        isFetching : false,
                        userInfo   : USER_INFO_STATE
                    }
                )
            );
    });
});
