import {async} from '@angular/core/testing';

import {UserStateSelectors} from './user.selectors';

describe('User State Selectors', () => {
    let userSelectors   : UserStateSelectors;

    // setup tasks to perform before each test
    beforeEach(() => {
        userSelectors = new UserStateSelectors();
    });

    it('isUserAuthenticated method should return UserState.userAuthenticated subscription', async(() => {
        // subscribe to Redux store updates
        userSelectors.isUserAuthenticated.first().subscribe(val => {
            // check returned state for correct type
            expect(typeof val === 'boolean').toEqual(true);
        });
    }));

    it('userName method should return UserState.userInfo.name subscription', async(() => {
        // subscribe to Redux store updates
        userSelectors.userName.first().subscribe(val => {
            // check returned state for correct type
            expect(typeof val === 'string').toEqual(true);
        });
    }));
});
