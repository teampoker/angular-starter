import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';

import {IAppStore} from '../app-store';
import {UserService} from '../../shared/services/User/user.service';
import {UserInfoState} from './types/user-info-state.model';
import {NavActions} from '../Navigation/nav.actions';
import {
    AlertItem,
    EnumAlertType
} from '../Navigation/types/alert-item.model';

@Injectable()

export class UserActions {
    static USER_LOGOUT          : string  = 'USER_LOGOUT';
    static USER_LOGIN           : string  = 'USER_LOGIN';
    static USER_INFO_REQUESTED  : string  = 'USER_INFO_REQUESTED';
    static USER_INFO_RECEIVED   : string  = 'USER_INFO_RECEIVED';

    /**
     *
     * @param store
     * @param userService
     * @param navActions
     */
    constructor(
        private store       : NgRedux<IAppStore>,
        private userService : UserService,
        private navActions  : NavActions
    ) {}

    /**
     * Marks the current user as having logged into the app.
     */
    userLogin() : void {
        this.store.dispatch({ type : UserActions.USER_LOGIN });
    }

    /**
     * Logs the current user out of the app.
     */
    userLogout() : void {
        // dispatch the redux action
        this.store.dispatch({ type : UserActions.USER_LOGOUT });
    }

    /**
     * Retrieve the user's information from the API.
     */
    getInfo() : void {
        this.store.dispatch({ type : UserActions.USER_INFO_REQUESTED });

        this.userService.getUser().subscribe(userInfo => {
                let user : UserInfoState;

                try {
                    // sanitize the data provided to make sure it conforms to our data model
                    user = UserInfoState.fromApi(userInfo);
                }
                catch (error) {
                    // something went wrong in translating / mapping the object... we should prolly tell someone
                    this.navActions.updateAlertMessageState(
                        new AlertItem(
                            {
                                alertType   : EnumAlertType.ERROR,
                                message     : 'Problem updating user info: ' + error
                            }));

                    user = undefined;
                }

                // always call the update method - it will counter the first dispatch action
                this.updateUserInfo(user);
            },
            error => {
                this.navActions.updateAlertMessageState(
                    new AlertItem(
                        {
                            alertType   : EnumAlertType.ERROR,
                            message     : 'User service returned error: ' + error
                        }));

                this.updateUserInfo(undefined);
            });
    }

    /**
     * Update the user's information in the redux store.
     * @param userInfo
     */
    updateUserInfo(userInfo : UserInfoState) : void {
        this.store.dispatch(
            {
                type    : UserActions.USER_INFO_RECEIVED,
                payload : userInfo
            });
    }
}
