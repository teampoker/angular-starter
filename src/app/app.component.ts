import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';
import {Router} from '@angular/router';
import {
    Idle,
    DEFAULT_INTERRUPTSOURCES
} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import {Subscription} from 'rxjs/Subscription';

import './sass-config.scss';
import {UserActions} from './store/User/user.actions';
import {UserStateSelectors} from './store/User/user.selectors';
import {UserNotificationService} from './shared/services/UserNotification/user-notification.service';
import {NavStateSelectors} from './store/Navigation/nav.selectors';
import {SearchActions} from './store/Search/search.actions';
import {MetaDataTypesActions} from './store/MetaDataTypes/meta-data-types.actions';
import {
    AlertItem,
    EnumAlertType
} from './store/Navigation/types/alert-item.model';
import {SessionActions} from './store/AppConfig/session.actions';
import {NavActions} from './store/Navigation/nav.actions';
import {AppService} from './shared/services/App/app.service';

@Component({
    selector        : 'angular-starter-app',
    templateUrl     : './app.component.html',
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation of AppComponent: Top Level Root Component for the application
 */
export class AppComponent {

    /**
     * AppComponent constructor
     *
     * @param searchActions
     * @param userActions
     * @param userStateSelectors
     * @param userNotifications
     * @param sessionActions
     * @param navActions
     * @param navSelectors
     * @param metaTypesActions
     * @param appService
     * @param router
     * @param idle Service responsible for detecting user sitting idle.
     * @param keepalive ball to the chain of the Idle service.
     */
    constructor(
        private searchActions       : SearchActions,
        private userActions         : UserActions,
        private userStateSelectors  : UserStateSelectors,
        private userNotifications   : UserNotificationService,
        private sessionActions      : SessionActions,
        private navActions          : NavActions,
        private navSelectors        : NavStateSelectors,
        private metaTypesActions    : MetaDataTypesActions,
        private appService          : AppService,
        private router              : Router,
        private idle                : Idle,
        private keepalive           : Keepalive
    ) {
        /**
         * Decoupled subscriptions from navSelectors because any time one was emitted, both were.
         * This reduces the overhead and keeps them independent.
         */
        this.stateSubscriptions.push(
            this.navSelectors.isPopped().subscribe(value => this.isPopped = value)
        );
        this.stateSubscriptions.push(
            this.navSelectors.alertMessage().subscribe(value => {
                this.userNotifications.handleAlert(value);
            })
        );
    }

    /**
     * Collection of Redux state subscriptions.  Tracked so they can be unsubscribed as needed.
     */
    private stateSubscriptions : Array<Subscription> = [];

    /**
     * simple func used to detect IE
     * @returns {boolean}
     */
    private detectIE() : boolean {
        const ua      = window.navigator.userAgent,
              trident = ua.indexOf('Trident/'),
              edge    = ua.indexOf('Edge/'),
              msie    = ua.indexOf('MSIE ');

        return msie > 0 || trident > 0 || edge > 0;
    }

    /**
     * current nav state isPopped value
     */
    isPopped : boolean;

    /**
     * is application running in the most dreaded browser known to man?
     */
    isIEBrowser : boolean;

    /**
     * Indicator if the application is unavailable to users, aka "busy".
     */
    isBusy : boolean;

    // this notification stuff is only for testing....
    testMessage : AlertItem;

    createTestNotification() {
        this.testMessage = this.testMessage || new AlertItem({ alertType : EnumAlertType.ERROR, message : 'Ta-da!' });

        switch (this.testMessage.alertType) {
            case EnumAlertType.ERROR:
                this.testMessage = new AlertItem({alertType: EnumAlertType.WARNING, message: 'Uh oh.... You must have done something really bad to have gotten this message.'});
                break;
            case EnumAlertType.WARNING:
                this.testMessage = new AlertItem({alertType: EnumAlertType.INFO, message: 'The style guide thinks we speak Latin: \n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.'});
                break;
            case EnumAlertType.INFO:
                this.testMessage = new AlertItem({alertType: EnumAlertType.SUCCESS, message: 'Ta-da! Congratulations - you can close out another one of your test scripts and clock a few more hours on your timecard.'});
                break;
            case EnumAlertType.SUCCESS:
                this.testMessage = new AlertItem({alertType: EnumAlertType.ERROR, message: 'Uh oh.... You must have done something really bad to have gotten this message.  Don\'t worry, I won\'t tell anyone.', durable: true});
                break;
            default:
                break;
        }

        this.navActions.updateAlertMessageState(this.testMessage);
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // detect IE
        this.isIEBrowser = this.detectIE();

        // Configure the angular session and idle timeout.
        this.sessionActions
            .begin()
            .subscribe(sessionLengthInMinutes => {
                /**
                 * wrapping the calls to indicate the app is busy while this block executes
                 */
                this.appService.indicateBusy(() => {
                    // get meta data types
                    this.metaTypesActions.getMetaDataTypes();

                    // get search types
                    this.searchActions.getSearchTypes();
                });

                // configure the application idle timer to begin after 5 seconds of inactivity
                this.idle.setIdle(5);

                // set the timeout, in number of seconds
                this.idle.setTimeout(sessionLengthInMinutes * 60);

                // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
                this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

                this.idle.onTimeout.subscribe(() => {
                    // log the user out when the idle timer expires
                    this.userActions.userLogout();
                });

                // if there's a server-side keep alive requirement, we would use the following:
                // keepalive.interval(15); // set the interval of the keepalive
                // keepalive.onPing.subscribe(() => call something server side; );
                this.idle.setKeepaliveEnabled(false);

                // now that everything is configured, kick off the idle timer
                this.idle.watch();
            });

        this.userActions.getInfo();
        this.userActions.userLogin();

        // setup what happens when a user is no longer authenticated in the app
        this.userStateSelectors.isUserAuthenticated.subscribe(value => {
            if (value === false) {
                this.router.navigate(['Logout']);
            }
        });
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        // unsubscribe any Observable subscriptions.
        this.stateSubscriptions.forEach(sub => sub.unsubscribe());

        // stop the idle timer
        this.idle.stop();
    }
}
