import {
    Component,
    Input,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    forwardRef,
    Renderer,
    ElementRef,
    ViewChild
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    NG_VALUE_ACCESSOR,
    NG_VALIDATORS,
    ControlValueAccessor,
    Validators,
    ValidatorFn
} from '@angular/forms';
import * as moment from 'moment';

/**
 * This file contains a series of exported factory functions that
 * create validation functions using a factory that creates a closure.
 * The idea is to produce a validator that can be "configured" during
 * the initialization of the component that is consuming the contained
 * validation logic.
 *
 * a local validator func can be created inside the consuming component
 * that handles the form level validation, but leveraging the below validation logic
 */

/**
 * validates entered hours value from html number input
 * @param minValue configurable minimum value the hours cannot be less than
 * @param maxValue configurable maximum value the hours cannot be greater than
 */
export function createHoursValidator(minValue : number, maxValue : number) : ValidatorFn {
    return function validateHours(c : FormControl) : { [key : string] : any } {
        // check for valid input
        if (c.value !== undefined && c.value !== '' && c.value !== 'HH') {
            const parsedHour = parseInt(c.value, 10);

            // we have a value but verify it's in the expected range of values
            if (!Number.isNaN(parsedHour) && parsedHour >= minValue && parsedHour <= maxValue) {
                return null;
            }
        }

        return {
            invalidHour : {
                valid : false
            }
        };
    };
}

/**
 * validates entered minutes value from html number input
 * @param minValue configurable minimum value the minutes cannot be less than
 * @param maxValue configurable maximum value the minutes cannot be greater than
 */
export function createMinutesValidator(minValue : number, maxValue : number) : ValidatorFn {
    return function validateHours(c : FormControl) : { [key : string] : any } {
        // check for valid input
        if (c.value !== undefined && c.value !== '' && c.value !== 'MM') {
            const parsedMinute = parseInt(c.value, 10);

            // we have a value but verify it's in the expected range of values
            if (!Number.isNaN(parsedMinute) && parsedMinute >= minValue && parsedMinute <= maxValue) {
                return null;
            }
        }

        return {
            invalidHour : {
                valid : false
            }
        };
    };
}

/**
 * register a validator function that returns null if the control value is valid, or an error object when it’s not.
 * this func is exported to allow for testing a component that has the form with the DOM i.e. it can be imported
 * into the parent form component's specs
 *
 * @param timeFormat time format string
 * @param dayOfYear date value associated with the selected time
 * @param minTime if populated the selected time cannot be less than this time
 * @param maxTime if populated the selected time cannot be greater than this time
 *
 * @returns {any}
 */
export function createTimeValidator(timeFormat : string, dayOfYear? : string, minTime? : string, maxTime? : string) : ValidatorFn {
    return function validateSelectedTime(c : FormControl) : { [key : string] : any } {
        // validate the selected time
        const dateTime  : any     = moment(c.value, timeFormat),
              validTime : boolean = dateTime.isValid();

        // update dateTime with date info
        if (dayOfYear !== undefined) {
            const selectedDay : any = moment(dayOfYear);

            dateTime.year(selectedDay.year()).month(selectedDay.month()).date(selectedDay.date());
        }

        // is selected time a valid moment?
        if (validTime) {
            // do we need to check against a minTime or maxTime?
            if (maxTime !== undefined) {
                // update maxTime with date info
                const selectedDay       : any = moment(dayOfYear),
                      maximumPickupTime : any = moment(maxTime, timeFormat).year(selectedDay.year()).month(selectedDay.month()).date(selectedDay.date());

                console.debug('verifying selected pickup time of ' + dateTime.format('MM/DD/YYYY h:mm A') + ' is less than the maximum available pickup time of ' + maximumPickupTime.format('MM/DD/YYYY h:mm A'));
                if (dateTime.isAfter(maximumPickupTime)) {
                    console.error('selected pickup time of ' + dateTime.format('MM/DD/YYYY h:mm A') + ' is greater than maximum available pickup time of ' + maximumPickupTime.format('MM/DD/YYYY h:mm A'));
                }
                else {
                    return null;
                }
            }
            else if (minTime !== undefined) {
                // update minTime with date info
                const selectedDay       : any = moment(dayOfYear),
                      minimumPickupTime : any = moment(minTime, timeFormat).year(selectedDay.year()).month(selectedDay.month()).date(selectedDay.date());

                console.debug('verifying selected pickup time of ' + dateTime.format('MM/DD/YYYY h:mm A') + ' is greater than the minimum available pickup time of ' + minimumPickupTime.format('MM/DD/YYYY h:mm A'));
                if (dateTime.isBefore(moment(minTime, timeFormat))) {
                    console.error('selected pickup time of ' + dateTime.format('MM/DD/YYYY h:mm A') + ' is less than minimum available pickup time of ' + minimumPickupTime.format('MM/DD/YYYY h:mm A'));
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        }

        return {
            invalidTime : {
                valid : false
            }
        };
    };
}

@Component({
    selector        : 'time-picker',
    templateUrl     : './time-picker.component.html',
    styleUrls       : ['./time-picker.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush,

    /**
     * extend the multi-provider for NG_VALUE_ACCESSOR with our own value accessor instance
     */
    providers : [
        {
            /**
             * we are extending what is being injected for the NG_VALUE_ACCESSOR token
             */
            provide : NG_VALUE_ACCESSOR,

            /**
             * we use useExisting because TimePickerComponent will be already created as a directive dependency in the component that uses it.
             * If we don’t do that, we get a new instance as this is how DI in Angular works
             */
            useExisting : forwardRef(() => TimePickerComponent),

            /**
             * multi value of true signifies we are providing multiple values for a single token i.e. NG_VALUE_ACCESSOR in this case
             */
            multi : true
        },
        {
            /**
             * we are extending what is being injected for the NG_VALIDATORS token
             */
            provide : NG_VALIDATORS,

            /**
             * custom validation for selected date values
             * implements the Validator interface:
             *
             *  validate(c: AbstractControl): {
             *     [key: string]: any;
             *  };
             */
            useValue : createTimeValidator,

            /**
             * multi value of true signifies we are providing multiple values for a single token i.e. NG_VALUE_ACCESSOR in this case
             */
            multi : true
        }
    ]
})

/**
 * implementation for TimePickerComponent
 */
export class TimePickerComponent implements ControlValueAccessor {
    /**
     * TimePickerComponent constructor
     * @param cd
     * @param builder
     * @param renderer
     */
    constructor(
        private cd          : ChangeDetectorRef,
        private builder     : FormBuilder,
        private renderer    : Renderer
    ) {
        // init form input fields
        this.hoursField     = new FormControl('', Validators.required);
        this.minutesField   = new FormControl('', Validators.required);

        // build reservation appointment FormControl group
        this.timePickerForm = builder.group({
            hoursField      : this.hoursField,
            minutesField    : this.minutesField
        });

        this.hoursField.valueChanges.subscribe(value => {
            // update hours
            this.updateHours(value);
        });

        this.minutesField.valueChanges.subscribe(value => {
            // update minutes
            this.updateMinutes(value);
        });
    }

    /**
     * placeholder for DOM element references
     */
    @ViewChild('hoursInput') hoursInput : ElementRef;
    @ViewChild('minutesInput') minutesInput : ElementRef;
    @ViewChild('meridiemInput') meridiemInput : ElementRef;

    /**
     * current selected locale
     * @type {string}
     */
    @Input() selectedLocale : string = 'en-US';

    /**
     * flag used to indicate time picker should only allow time to
     * be decremented to a value that is less than the current selectedTime
     * that is passed in through ngModel
     * @type {boolean}
     */
    @Input() isDecrementOnly : boolean = false;

    /**
     * flag used to indicate time picker should only allow time to
     * be incremented to a value that is greater than the current selectedTime
     * that is passed in through ngModel
     * @type {boolean}
     */
    @Input() isIncrementOnly : boolean = false;

    /**
     * format string to use for time display strings
     * @type {string}
     */
    @Input() timeFormat : string;

    /**
     * meridiem labels
     * @type {[string,string]}
     */
    @Input() meridiems : Array<string> = ['AM', 'PM'];

    /**
     * hours change step
     * @type {number}
     */
    @Input() hourStep : number;

    /**
     * minutes change step
     * @type {number}
     */
    @Input() minuteStep : number;

    /**
     * if true works in 12H mode and displays AM/PM.
     * if false works in 24H mode and hides AM/PM
     * @type {boolean}
     */
    @Input() showMeridiem : boolean = true;

    /**
     * if true hours and minutes fields will be readonly
     * @type {boolean}
     */
    @Input() isReadOnly : boolean = false;

    /**
     * if true spinner is displayed
     * @type {boolean}
     */
    @Input() showSpinners : boolean = false;

    /**
     * store selected hours value here
     */
    private hours : string;

    /**
     * store selected minutes value here
     */
    private minutes : string;

    /**
     * currently selected meridiem i.e AM or PM stored here
     * @type {string}
     */
    private selectedMeridiem : string;

    /**
     * currently selected time
     * @type {moment.Moment}
     */
    private selectedTime : any;

    /**
     * list of keypress values that are allowed, all others are excluded
     * @type {[string,string,string,string,string,string,string]}
     */
    private constrainKeys  : Array<string> = ['ArrowUp', 'ArrowDown', '-', '+', '.', 'e', 'E'];

    /**
     * takes current hours as a parameter, determines if time picker is using meridiems
     * and if so, inspects the value and returns a modified value, if necessary
     * between 1 and 12
     * @param value
     */
    private adjustHoursForMeridiem(value : number) : number {
        let newHours : number;

        // are we in military time or not?
        if (!this.showMeridiem) {
            newHours = value;
        }
        else {
            if (value > 12) {
                newHours = value - 12;
            }
            else if (value === 0) {
                newHours = 12;
            }
            else {
                newHours = value;
            }
        }

        return newHours;
    }

    /**
     * validate and update local hours value
     */
    private updateHours(value : number | string) {
        // if not a string...
        if (typeof value === 'number') {
            // verify we got a good value within expected ranges
            if (!this.hoursField.valid) {
                // indicate to consuming components an invalid time has been selected
                this.propagateChange('Invalid date');

                this.selectedTime = 'Invalid date';

                // add a check for '00' here as for some reason the HTML number input doesn't properly
                // update when we pass the value of '00' to ngModel, it will show extra trailing zeroes
                /*if (value === 0) {
                    this.hoursField.setValue('0');

                    this.hours = '0';
                }*/

                this.hoursField.setValue('');
            }
            else {
                // adjust for military time if needed
                value = this.adjustHoursForMeridiem(value);

                // update hours on selectedTime
                if (this.selectedTime !== undefined && this.selectedTime !== 'Invalid date') {
                    this.selectedTime.hour(value);
                }
                else {
                    // init selectedTime to new moment
                    this.selectedTime = moment().locale(this.selectedLocale).hour(value).minutes(this.minTimeMinute);
                }

                // update UI
                this.hours = value.toString();

                // propagate change externally
                this.emitSelectedTime();
            }
        }
        // check for empty/null value
        // tslint:disable-next-line: triple-equals
        else if (value == undefined) {
            // indicate to consuming components an invalid time has been selected
            this.propagateChange('Invalid date');

            this.selectedTime = 'Invalid date';
        }
    }

    /**s
     * validate and update local minutes value
     */
    private updateMinutes(value : number | string) {
        // if not a string or number...
        if (typeof value === 'number') {
            // verify we got a good value within expected ranges
            if (!this.minutesField.valid) {
                // indicate to consuming components an invalid time has been selected
                this.propagateChange('Invalid date');

                this.selectedTime = 'Invalid date';

                this.minutesField.setValue('');
            }
            else {
                // update hours on selectedTime
                if (this.selectedTime !== undefined && this.selectedTime !== 'Invalid date') {
                    this.selectedTime.minute(value);
                }
                else {
                    // init selectedTime to new moment
                    this.selectedTime = moment().locale(this.selectedLocale).hour(this.minTimeHour).minutes(value);
                }

                // update UI to always show two digits
                this.minutes = value.toLocaleString(this.selectedLocale, { minimumIntegerDigits : 2, useGrouping : false });

                this.minutesField.setValue(this.minutes);

                // propagate change externally
                this.emitSelectedTime();
            }
        }
        // check for empty/null value
        // tslint:disable-next-line: triple-equals
        else if (value == undefined) {
            // indicate to consuming components an invalid time has been selected
            this.propagateChange('Invalid date');

            this.selectedTime = 'Invalid date';
        }
    }

    /**
     * given a populated this.selectedTime value, parse into hours, minutes, and meridiem
     */
    private parseSelectedTime() {
        // separate into hours/minutes
        let newHours   : number,
            newMinutes : number;

        // are we in military time or not?
        newHours   = this.adjustHoursForMeridiem(this.selectedTime.hour());
        newMinutes = this.selectedTime.minute();

        // AM or PM?
        this.selectedMeridiem = this.selectedTime.format('A');

        // trigger change detection here or the selectedMeridiem won't update in the UI
        this.cd.markForCheck();

        // set the values in the UI (initial validation styles don't set properly if we skip this)
        this.hoursField.setValue(newHours);
        this.minutesField.setValue(newMinutes);
    }

    /**
     * propagates updated selected time value externally to any listeners
     */
    private emitSelectedTime() {
        // hours and minutes values populated?
        if (this.hoursField.valid && this.minutesField.valid && this.selectedMeridiem) {
            // emit new selected time value
            this.propagateChange(moment(this.hours + ':' + this.minutes + ' ' + this.selectedMeridiem, 'h:mm A').format(this.timeFormat));

            // indicate control is touched
            if (this.propagateTouched) {
                this.propagateTouched();
            }
        }
        else {
            this.propagateChange('Invalid date');
        }
    }

    /**
     * inspects the current timePickerForm valid state
     */
    private isSelectedTimeValid() : boolean {
        // return form valid state for hours and minutes field
        return this.hoursField.valid && this.minutesField.valid;

    }

    /**
     * inspects the isDecrementOnly/isIncrementOnly flags to determine if
     * the min/max allowable time values need to be adjusted from their default values
     */
    private adjustTimeRanges() {
        // inspect the isDecrementOnly flag
        if (this.isDecrementOnly) {
            // adjust the min/max ranges based on ngModel value
            if (this.selectedTime !== undefined) {
                this.maxTimeHour    = this.adjustHoursForMeridiem(this.selectedTime.hour());
                this.maxTimeMinute  = this.selectedTime.minute();
            }
        }
        // inspect the isIncrementOnly flag
        else if (this.isIncrementOnly) {
            // adjust the min/max ranges based on ngModel value
            if (this.selectedTime !== undefined) {
                this.minTimeHour    = this.adjustHoursForMeridiem(this.selectedTime.hour());
                this.minTimeMinute  = this.selectedTime.minute();
            }
        }

        // configure dynamic hours validators
        this.hoursField.setValidators(Validators.compose([
            Validators.required,
            createHoursValidator(this.minTimeHour, this.maxTimeHour)
        ]));

        // configure dynamic minutes validators
        this.minutesField.setValidators(Validators.compose([
            Validators.required,
            createMinutesValidator(this.minTimeMinute, this.maxTimeMinute)
        ]));

        this.hoursField.updateValueAndValidity();
        this.minutesField.updateValueAndValidity();
    }

    /**
     * the method set in registerOnChange, it is just
     * a placeholder for a method that takes one parameter,
     * we use it to emit value changes back to the form
     *
     * @param _
     */
    private propagateChange = (_ : any) => { };

    /**
     * the method set in registerOnTouched
     * we use it to emit touched state back to the form
     */
    private propagateTouched : () => {};

    /**
     * time picker controls form group
     */
    timePickerForm : FormGroup;

    /**
     * hours field numeric input
     */
    hoursField : FormControl;

    /**
     * minutes field numeric inputs
     */
    minutesField : FormControl;

    /**
     * minimum hour value that can be selected
     * @type {number}
     */
    minTimeHour : number;

    /**
     * maximum hour value that can be selected
     * @type {number}
     */
    maxTimeHour : number;

    /**
     * maximum minutes value that can be selected
     * @type {number}
     */
    minTimeMinute : number = 0;

    /**
     * minimum minutes value that can be selected
     * @type {number}
     */
    maxTimeMinute : number = 59;

    /**
     * THESE FOUR FUNCTIONS THAT FOLLOW ARE FUNCTIONS
     * WE HAVE TO IMPLEMENT FROM THE ControlValueAccessor INTERFACE
     * THEY ARE USED INTERNALLY TO COMMUNICATE WITH EXTERNAL COMPONENTS
     */

    /**
     * allows US to update our internal model with incoming values,
     * for example if WE use ngModel to bind our control to data
     *
     * @param value
     */
    writeValue(value : string) {
        if (value !== undefined && value !== null && value !== '') {
            if (value !== this.selectedTime && value !== 'Invalid date') {
                // parse new time
                this.selectedTime = moment(value, this.timeFormat);

                this.parseSelectedTime();

                // check to see if we need to adjust the default time ranges for custom validation
                this.adjustTimeRanges();

                // an initial value was supplied so go ahead and propagate the selected date upstream
                // NOTE: it's possible to have the timepicker be pre-populated
                // with a valid date value, pass validation, but the control
                // still shows as invalid in the consuming Form.
                this.propagateChange(moment(this.hours + ':' + this.minutes + ' ' + this.selectedMeridiem, 'h:mm A').format(this.timeFormat));

                if (this.propagateTouched) {
                    this.propagateTouched();
                }
            }
            else {
                // null time picker
                this.hours              = undefined;
                this.minutes            = undefined;
                this.selectedMeridiem   = undefined;
                this.hoursField.setValue('');
                this.minutesField.setValue('');
            }
        }
        else {
            // null time picker
            this.hours              = undefined;
            this.minutes            = undefined;
            this.selectedMeridiem   = undefined;
            this.hoursField.setValue('');
            this.minutesField.setValue('');
        }
    }

    /**
     * This function is called when the control status changes to or from "DISABLED".
     * Depending on the value, it will enable or disable the appropriate DOM element
     * @param isDisabled
     */
    setDisabledState(isDisabled : boolean) {
        this.renderer.setElementProperty(this.hoursInput.nativeElement, 'disabled', isDisabled);
        this.renderer.setElementProperty(this.minutesInput.nativeElement, 'disabled', isDisabled);
        this.renderer.setElementProperty(this.meridiemInput.nativeElement, 'disabled', isDisabled);
    }

    /**
     * accepts a callback function which WE can call when changes happen
     * so that you WE notify the outside world that the data model has changed
     *
     * @param fn
     */
    registerOnChange(fn : any) {
        this.propagateChange = fn;
    }

    /**
     * accepts a callback function which WE can call when WE want to set our control to touched.
     * This is then managed by Angular 2 FOR adding the correct touched state and classes to the actual element tag in the DOM
     *
     * @param fn
     */
    registerOnTouched(fn : any) {
        this.propagateTouched = fn;
    }

    /**
     * event handler for toggle of selected meridiem value
     * @param updateMeridiem 'AM' or 'PM'
     */
    updateMeridiem(updateMeridiem : string) {
        if (updateMeridiem !== undefined) {
            // make sure they didn't just click the same meridiem value again
            if (this.selectedMeridiem !== updateMeridiem) {
                // increase/decrease time by 12 hours
                if (this.isSelectedTimeValid()) {
                    // is a previously selected meridiem value available?
                    if (this.selectedMeridiem) {
                        updateMeridiem === 'PM' ? this.selectedTime = moment(this.selectedTime, this.timeFormat).add('hours', 12) : this.selectedTime = moment(this.selectedTime, this.timeFormat).subtract('hours', 12);

                        // update chosen meridiem value
                        this.selectedMeridiem = updateMeridiem;
                    }
                    else {
                        // update chosen meridiem value
                        this.selectedMeridiem = updateMeridiem;

                        // look for PM
                        if (this.selectedMeridiem === 'PM') {
                            this.selectedTime = moment(this.selectedTime, this.timeFormat).add('hours', 12);
                        }
                    }

                    // propagate changes
                    this.emitSelectedTime();
                }
                else {
                    // just store the selected meridiem for later
                    this.selectedMeridiem = updateMeridiem;
                }

                // indicate control is touched
                if (this.propagateTouched) {
                    this.propagateTouched();
                }
            }
        }
    }

    /**
     * increment hours by hourStep value
     */
    incrementHours() {
        // increase time by hour step
        this.selectedTime = moment(this.selectedTime, this.timeFormat).add('hours', this.hourStep);

        this.parseSelectedTime();
    }

    /**
     * decrement hours by hourStep value
     */
    decrementHours() {
        // decrease time by hour step
        this.selectedTime = moment(this.selectedTime, this.timeFormat).subtract('hours', this.hourStep);

        this.parseSelectedTime();
    }

    /**
     * increment minutes by minuteStep value
     */
    incrementMinutes() {
        // increase time by minute step
        let adjustedMinutes : number;
        // this.selectedTime = moment(this.selectedTime, this.timeFormat).add('minutes', this.minuteStep);
        adjustedMinutes = this.minutes === undefined ? this.minuteStep : parseInt(this.minutes, 10) + this.minuteStep;

        this.minutes = adjustedMinutes.toLocaleString(this.selectedLocale, { minimumIntegerDigits : 2, useGrouping : false });

        this.minutesField.setValue(this.minutes);
        // this.parseSelectedTime();
    }

    /**
     * decrement minutes by minuteStep value
     */
    decrementMinutes() {
        if (this.selectedTime !== undefined) {
            // decrease time by minute step
            this.selectedTime = moment(this.selectedTime, this.timeFormat).subtract('minutes', this.minuteStep);

            this.parseSelectedTime();
        }
    }

    /**
     * event handler for specific keypress events on
     * the hours field of the timepicker i.e. up/down arrow keypress values
     * @param event
     */
    keyPressAdjustHours(event : any) {
        if (this.constrainKeys.indexOf(event.key) > -1 || this.isReadOnly) {
            event.preventDefault();
        }

        // keyCode 38 === Up Arrow
        if (event.keyCode === 38) {
            this.incrementHours();
        }
        // keyCode 40 === Down Arrow
        else if (event.keyCode === 40) {
            this.decrementHours();
        }
    }

    /**
     * event handler for specific keypress events on
     * the minutes field of the timepicker i.e. up/down arrow keypress values
     * @param event
     */
    keyPressAdjustMinutes(event : any) {
        if (this.constrainKeys.indexOf(event.key) > -1 || this.isReadOnly) {
            event.preventDefault();
        }

        // keyCode 38 === Up Arrow
        if (event.keyCode === 38) {
            this.incrementMinutes();
        }
        // keyCode 40 === Down Arrow
        else if (event.keyCode === 40) {
            this.decrementMinutes();
        }
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // adjust min/max time based on military time enabled/disabled
        if (this.showMeridiem) {
            this.minTimeHour = 1;
            this.maxTimeHour = 12;
        }
        else {
            this.minTimeHour = 0;
            this.maxTimeHour = 23;
        }

        // check to see if we need to adjust the default time ranges for custom validation
        this.adjustTimeRanges();
    }
}
