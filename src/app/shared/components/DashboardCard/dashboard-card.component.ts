import {Component, Input, ChangeDetectionStrategy} from '@angular/core';

@Component({
    selector        : 'dashboard-card',
    templateUrl     : 'dashboard-card..component.html',
    styleUrls       : ['dashboard-card..component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for DashboardCardComponent: handles display of section panels
 */
export class DashboardCardComponent {
    /**
     * DashboardCardComponent constructor
     */
    constructor() {}

    /**
     * String containing widget information
     * @type {String}
     */
    @Input() title : string;

    /**
     * component init lifecycle hook
     */
    ngOnInit() {

    }
}
