import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';
import {List} from 'immutable';

import {NavActions} from '../../../store/Navigation/nav.actions';
import {TaskItem} from '../../../store/Navigation/types/task-item.model';

@Component({
    selector        : 'task-list',
    templateUrl     : 'task-list.component.html',
    styleUrls       : ['task-list.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for TaskListComponent: handles task list items
 */
export class TaskListComponent {
    /**
     * DashboardCardComponent constructor
     */
    constructor(private navActions : NavActions) {}

    /**
     * task list data
     * @type {number}
     */
    @Input() taskList : List<TaskItem>;

    /**
     * user selected task name
     */
    task : string;

    /**
     * user selected task status
     */
    toggleTasks : boolean;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event for adding items to taskList
     */
    addTaskItem(task : string) {
        this.navActions.addTaskToList(new TaskItem({
            task,
            status : false
        }));

        // resetting input with empty string
        this.task = '';
    }

    /**
     * event for updating items already contained within taskList
     * @param event
     * @param index number
     */
    updateTaskList(event : any, index : number) {
        const itemStatus = event.currentTarget.checked;  // so our change event fires BEFORE the model updates so we should grab the actual value of checked here

        this.navActions.updateTaskList(new TaskItem({
            task    : this.taskList.get(index).get('task'),
            status  : itemStatus
        }), index);
    }
}
