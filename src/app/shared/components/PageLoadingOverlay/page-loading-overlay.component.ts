import {
    Component,
    ChangeDetectionStrategy,
    Input
} from '@angular/core';

@Component({
    selector        : 'page-loading-overlay',
    templateUrl     : 'page-loading-overlay.component.html',
    changeDetection : ChangeDetectionStrategy.OnPush
})
export class PageLoadingOverlayComponent {
    constructor() {}

    @Input() textToDisplay : string = 'Loading...';
}
