import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
    SimpleChanges
} from '@angular/core';
import {
    FormArray,
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';

import {KeyValuePair} from '../../../store/types/key-value-pair.model';
import {BeneficiaryConnectionsActions} from '../../../store/Beneficiary/actions/beneficiary-connections.actions';
import {BeneficiaryConnectionsDropdownTypes} from '../../../store/Beneficiary/types/beneficiary-connections-dropdown-types.model';
import {BeneficiaryConnectionsStateDetail} from '../../../store/Beneficiary/types/beneficiary-connections-state-detail.model';
import {IBeneficiaryFieldUpdate} from '../../../store/Beneficiary/types/beneficiary-field-update.model';
import {EnumConnectionTypes} from '../../../store/Beneficiary/types/beneficiary-connection-types.model';
import {createDateValidator} from '../DatePicker/date-picker.component';

@Component({
    selector        : 'beneficiary-connection',
    templateUrl     : 'beneficiary-connection.component.html',
    styleUrls       : ['beneficiary-connection.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryConnectionComponent: handles display of this section panel
 */
export class BeneficiaryConnectionComponent {
    /**
     * BeneficiaryConnectionComponent constructor
     * @param builder
     * @param beneficiaryConnectionsActions
     */
    constructor(
        private builder                       : FormBuilder,
        private beneficiaryConnectionsActions : BeneficiaryConnectionsActions
    ) {
        // init form input fields
        this.salutation       = new FormControl('', undefined);
        this.firstName        = new FormControl('', Validators.required);
        this.lastName         = new FormControl('', Validators.required);
        this.middleName       = new FormControl('', undefined);
        this.suffix           = new FormControl('', undefined);
        this.birthDateEnabled = new FormControl('', undefined);
        this.birthDate        = new FormControl('', undefined);
        this.phones           = new FormArray([this.createPhoneFormGroup()]);
        this.parentFamily     = new FormControl('', undefined);
        this.socialWorker     = new FormControl('', undefined);
        this.nurseDoctor      = new FormControl('', undefined);
        this.planClient       = new FormControl('', undefined);
        this.emergencyContact = new FormControl('', undefined);
        this.driver           = new FormControl('', undefined);

        // build Beneficiary Information FormControl group
        this.connectionForm = builder.group({
            salutation              : this.salutation,
            firstName               : this.firstName,
            middleName              : this.middleName,
            lastName                : this.lastName,
            suffix                  : this.suffix,
            birthDate               : this.birthDate,
            birthDateEnabled        : this.birthDateEnabled,
            phones                  : this.phones,
            connectionTypesGroup    : builder.group({
                parentFamily     : this.parentFamily,
                socialWorker     : this.socialWorker,
                nurseDoctor      : this.nurseDoctor,
                planClient       : this.planClient,
                emergencyContact : this.emergencyContact,
                driver           : this.driver
            }, {
                validator : (formGroup : FormGroup) => this.connectionTypeRequired(formGroup)
            })
        });

        this.connectionForm.statusChanges.subscribe(() => {
            // emit updated form valid state
            this.connectionFormValid.emit(this.connectionForm.valid);
        });

        this.firstName.valueChanges.debounceTime(250).subscribe((value : string) => {
            // only emit value when updating it
            if (this.isConnectionAddedManually) {
                // emit updated value
                this.updateConnectionField.emit({
                    fieldName   : 'firstName',
                    fieldValue  : value
                });
            }
        });

        this.lastName.valueChanges.debounceTime(250).subscribe((value : string) => {
            // only emit value when updating it
            if (this.isConnectionAddedManually) {
                // emit updated value
                this.updateConnectionField.emit({
                    fieldName   : 'lastName',
                    fieldValue  : value
                });
            }
        });

        this.middleName.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateConnectionField.emit({
                fieldName   : 'middleName',
                fieldValue  : value
            });
        });

        this.suffix.valueChanges.debounceTime(250).subscribe((value : string) => {
            // emit updated value
            this.updateConnectionField.emit({
                fieldName   : 'suffix',
                fieldValue  : value
            });
        });

        this.birthDate.valueChanges.subscribe((value : string) => {
            // emit updated value
            this.updateConnectionField.emit({
                fieldName   : 'birthDate',
                fieldValue  : value
            });
        });
    }

    /**
     * toggle used to active add a connection fields
     */
    @Input() isAddConnectionActive : boolean;

    /**
     * toggle used to expand add a connection fields
     */
    @Input() isAddConnectionExpanded : boolean;

    /**
     * flag indicating if connection form is being edited or if it is part of a new connection being added
     */
    @Input() isEditConnectionActive : boolean;

    /**
     * flag indicating if Add Additional Phone workflow is active
     */
    @Input() isAddPhoneActive : boolean;

    /**
     * flag indicating if CSR is manually adding a connection
     */
    @Input() isConnectionAddedManually : boolean;

    /**
     * flag indicating if CSR is adding a connection through search
     */
    @Input() isConnectionAddedFromSearch : boolean;

    /**
     * available types for dropdowns
     */
    @Input() dropdownTypes : BeneficiaryConnectionsDropdownTypes;

    /**
     * beneficiary connection info
     */
    @Input() connection : BeneficiaryConnectionsStateDetail;

    /**
     * event triggered when beneficiary connection fields are edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() updateConnectionField : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when connection form valid state changes
     * @type {EventEmitter<boolean>}
     */
    @Output() connectionFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * dynamically creates connections form array for editing
     */
    private buildFormControls(value : number) {
        // grab reference to array of form phone controls
        const phoneControls : FormArray = this.connectionForm.get('phones') as FormArray;

        if (phoneControls.length > 0) {
            // tslint:disable-next-line: prefer-const
            for (let i = 0, len = phoneControls.length; i < len; i++) {
                // clear existing FormGroup item
                phoneControls.removeAt(i);
            }
        }

        // tslint:disable-next-line: prefer-const
        for (let i = 0, len = value; i < len; i++) {
            // clear existing FormGroup item
            phoneControls.removeAt(i);

            // push new FormGroup item
            phoneControls.push(this.createPhoneFormGroup());
        }
    }

    /**
     * form group builder used to add dynamically added phone fields
     */
    private createPhoneFormGroup() {
        return this.builder.group({
            phoneType             : ['', undefined],
            phone                 : ['', undefined],
            phoneNotifications    : ['', undefined]
        });
    }

    /**
     * based on the provided parameter, this method will enable/disable
     * the beneficiary person fields on the connectionForm
     * and update form validation to match the new enabled/disabled state
     *
     * @param disable
     */
    private disablePersonFields(disable : boolean) {
        if (disable) {
            this.connectionForm.get('firstName').disable();
            this.connectionForm.get('middleName').disable();
            this.connectionForm.get('lastName').disable();
            this.connectionForm.get('suffix').disable();

            this.firstName.setValidators([]);
            this.lastName.setValidators([]);

            this.firstName.updateValueAndValidity();
            this.lastName.updateValueAndValidity();
        }
        else {
            this.connectionForm.get('firstName').enable();
            this.connectionForm.get('middleName').enable();
            this.connectionForm.get('lastName').enable();
            this.connectionForm.get('suffix').enable();

            this.firstName.setValidators([Validators.required]);
            this.lastName.setValidators([Validators.required]);

            this.firstName.updateValueAndValidity();
            this.lastName.updateValueAndValidity();
        }
    }

    /**
     * custom validator implementation for connectionTypes checkbox group
     */
    private connectionTypeRequired(checkBoxGroup : FormGroup) {
        for (const key in checkBoxGroup.controls) {
            if (checkBoxGroup.controls.hasOwnProperty(key)) {
                const control : FormControl = checkBoxGroup.controls[key] as FormControl;

                if (control.value) {
                    return null;
                }
            }
        }

        return {
            validateDays : {
                valid : false
            }
        };
    }

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * Enum connection types to match against
     * @type {EnumConnectionTypes}
     */
    connectionTypes : typeof EnumConnectionTypes = EnumConnectionTypes;

    /**
     * Object contains form group
     */
    connectionForm : FormGroup;

    /**
     * first name input field
     */
    firstName : FormControl;

    /**
     * middle name input field
     */
    middleName : FormControl;

    /**
     * last name input field
     */
    lastName : FormControl;

    /**
     * suffix input field
     */
    suffix : FormControl;

    /**
     * date of birth input field
     */
    birthDate : FormControl;

    /**
     * checkbox that enables date of birth field
     */
    birthDateEnabled : FormControl;

    /**
     * parentFamily input field
     */
    parentFamily : FormControl;

    /**
     * socialWorker input field
     */
    socialWorker : FormControl;

    /**
     * nurseDoctor input field
     */
    nurseDoctor : FormControl;

    /**
     * planClient input field
     */
    planClient : FormControl;

    /**
     * emergencyContact input field
     */
    emergencyContact : FormControl;

    /**
     * driver input field
     */
    driver : FormControl;

    /**
     * phones
     */
    phones : FormArray;

    /**
     * boolean used to validate beneficiaryPhones form
     */
    validPhoneForm : boolean = true;

    /**
     * salutation selection form input
     */
    salutation : FormControl;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for add phones
     */
    addPhones() {
        // add new phone record
        this.beneficiaryConnectionsActions.addAdditionalConnectionPhone();

        // toggle flag
        this.beneficiaryConnectionsActions.toggleAddPhoneActive();
    }

    /**
     * event handler for remove phones
     * @param phoneIndex index of phone in phones collection
     */
    removePhones(phoneIndex : number) {
        // remove phone record
        this.beneficiaryConnectionsActions.removeAdditionalConnectionPhone(phoneIndex);
    }

    /**
     * event handler for canceling edit mode
     */
    cancelEdit() {
        this.beneficiaryConnectionsActions.cancelEditBeneficiaryConnection();
    }

    /**
     * event handler for canceling edit mode
     */
    cancelAdd() {
        this.beneficiaryConnectionsActions.cancelAddBeneficiaryConnection();
    }

    /**
     * event handler for canceling edit mode
     */
    closeAddClick() {
        // close the add connections form
        this.beneficiaryConnectionsActions.closeAddBeneficiaryConnection();

        // save beneficiary connections data
        this.beneficiaryConnectionsActions.saveBeneficiaryConnections();
    }

    /**
     * event handler for canceling edit mode
     */
    closeEditClick() {
        // save beneficiary connections data
        this.beneficiaryConnectionsActions.saveBeneficiaryConnections();

        // close the edit connections form
        this.beneficiaryConnectionsActions.closeEditBeneficiaryConnection();
    }

    /**
     * update form valid state
     */
    emitValidForm() {
        this.connectionFormValid.emit(this.validPhoneForm);
    }

    /**
     * toggle Add A Connection expanded/collapsed state
     */
    toggleAddExpanded() {
        // dispatch toggle action
        this.beneficiaryConnectionsActions.toggleAddConnectionExpanded();

        // set flag to indicate CSR is manually adding a connection
        if (!this.isConnectionAddedManually) {
            this.beneficiaryConnectionsActions.toggleConnectionAddedManually(true);
        }

        this.beneficiaryConnectionsActions.toggleConnectionAddedFromSearch(false);
    }

    /**
     * update the phone form valid state
     */
    onPhoneFormValid(event : boolean) {
        // update phone form valid state
        this.validPhoneForm = event;

        // bubble new phone form valid state up
        this.emitValidForm();
    }

    /**
     * event handler for phone edit events
     * @param choice phone field value modified
     * @param updateIndex index of phone in connection's phone array
     */
    onUpdatePhoneField(choice : IBeneficiaryFieldUpdate, updateIndex : number) {
        // add index
        choice.secondaryUpdateIndex = updateIndex;

        // emit updated value
        this.updateConnectionField.emit(choice);
    }

    /**
     * event handler for salutation selection event
     * @param choice
     * @param event
     */
    updateSalutation(choice : KeyValuePair, event : any) {
        event.preventDefault();

        // emit updated value
        this.updateConnectionField.emit({
            fieldName   : 'personalTitle',
            fieldValue  : choice.value
        });
    }

    /**
     * event handler for update connection type ParentFamily selection event
     * @param value
     */
    updateParentFamily(value : boolean) {
        // emit updated value
        this.updateConnectionField.emit({
            fieldName   : 'parentFamily',
            fieldValue  : !value
        });
    }

    /**
     * event handler for update connection type SocialWorker selection event
     * @param value
     */
    updateSocialWorker(value : boolean) {
        // emit updated value
        this.updateConnectionField.emit({
            fieldName   : 'socialWorker',
            fieldValue  : !value
        });
    }

    /**
     * event handler for update connection type NurseDoctor selection event
     * @param value
     */
    updateNurseDoctor(value : boolean) {
        // emit updated value
        this.updateConnectionField.emit({
            fieldName   : 'nurseDoctor',
            fieldValue  : !value
        });
    }

    /**
     * event handler for update connection type ClientPlan selection event
     * @param value
     */
    updateClientPlan(value : boolean) {
        // emit updated value
        this.updateConnectionField.emit({
            fieldName   : 'planClient',
            fieldValue  : !value
        });
    }

    /**
     * event handler for update connection type EmergencyContact selection event
     * @param value
     */
    updateEmergencyContact(value : boolean) {
        // emit updated value
        this.updateConnectionField.emit({
            fieldName   : 'emergencyContact',
            fieldValue  : !value
        });
    }

    /**
     * event handler for update connection type Driver selection event
     * @param value
     */
    updateDriver(value : boolean) {
        // emit updated value
        this.updateConnectionField.emit({
            fieldName   : 'driver',
            fieldValue  : !value
        });
    }

    /**
     * event handler for birth date checkbox
     * @param event
     */
    updateBirthDateEnabled(event : any) {
        const itemChecked = event.currentTarget.checked;  // so our change event fires BEFORE the model updates so we should grab the actual value of checked here

        // emit updated value
        this.updateConnectionField.emit({
            fieldName   : 'birthDateEnabled',
            fieldValue  : itemChecked
        });

        // update form validation based on selection
        if (itemChecked) {
            this.connectionForm.controls['birthDate'].setValidators(Validators.compose([
                Validators.required,
                createDateValidator(this.dateFormat)
            ]));
        }
        else {
            this.connectionForm.controls['birthDate'].setValidators([]);
        }

        this.connectionForm.controls['birthDate'].updateValueAndValidity();
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     * @param changes
     */
    ngOnChanges(changes : SimpleChanges) {
        // changes.prop contains the old and the new value...
        if (changes['connection']) {
            // if not valid previous value then component has just initialized
            if (changes['connection'].previousValue && changes['connection'].previousValue instanceof BeneficiaryConnectionsStateDetail) {
                // build form controls if any items were added/remove from connection
                if (changes['connection'].currentValue.get('phones').size !== changes['connection'].previousValue.get('phones').size) {
                    this.buildFormControls(changes['connection'].currentValue.get('phones').size);
                }
            }
            else {
                // component just initialized
                this.buildFormControls(changes['connection'].currentValue.get('phones').size);
            }
        }

        if (changes['isEditConnectionActive']) {
            if (changes['isEditConnectionActive'].currentValue) {
                // disable person fields
                this.disablePersonFields(true);
            }
        }

        if (changes['isConnectionAddedFromSearch']) {
            if (changes['isConnectionAddedFromSearch'].currentValue) {
                // disable person fields
                this.disablePersonFields(true);
            }
        }

        if (changes['isConnectionAddedManually']) {
            if (changes['isConnectionAddedManually'].currentValue) {
                // disable person fields
                this.disablePersonFields(false);
            }
        }
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        if (this.isEditConnectionActive || this.isConnectionAddedFromSearch) {
            // disable person fields
            this.disablePersonFields(true);
        }
    }
}
