import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    Renderer,
    ViewChild
} from '@angular/core';

import {PopoverDirective} from '../../directives/Popover/popover.directive';

export type popoverPlacement = 'top'
    | 'bottom'
    | 'left'
    | 'right'
    | 'auto'
    | 'auto top'
    | 'auto bottom'
    | 'auto left'
    | 'auto right';

@Component({
    selector    : 'popover-content',
    templateUrl : 'popover-content.component.html',
    styleUrls   : ['popover-content.component.scss']
})
export class PopoverContentComponent implements AfterViewInit, OnDestroy {
    @Input() animation : boolean = true;

    @Input() closeOnClickOutside : boolean = false;

    @Input() closeOnMouseOutside : boolean = false;

    @Input() content : string;

    @Input() placement : popoverPlacement = 'bottom';

    @Input() title : string;

    @ViewChild('popoverDiv') popoverDiv : ElementRef;

    constructor(
        private element : ElementRef,
        private cdr : ChangeDetectorRef,
        private renderer : Renderer
    ) {}

    displayType : string                   = 'none';
    effectivePlacement : string;
    isIn : boolean                         = false;
    left : number                          = -10000;
    listenClickFunc : any;
    listenMouseFunc : any;
    popover : PopoverDirective;
    onCloseFromOutside : EventEmitter<any> = new EventEmitter();
    top : number                           = -10000;

    private getEffectivePlacement(placement : string, hostElement : HTMLElement, targetElement : HTMLElement) : string {
        const placementParts = placement.split(' ');
        if (placementParts[0] !== 'auto') {
            return placement;
        }

        const hostElBoundingRect = hostElement.getBoundingClientRect();

        const desiredPlacement = placementParts[1] || 'bottom';

        if (desiredPlacement === 'top' && hostElBoundingRect.top - targetElement.offsetHeight < 0) {
            return 'bottom';
        }
        if (desiredPlacement === 'bottom' && hostElBoundingRect.bottom + targetElement.offsetHeight > window.innerHeight) {
            return 'top';
        }
        if (desiredPlacement === 'left' && hostElBoundingRect.left - targetElement.offsetWidth < 0) {
            return 'right';
        }
        if (desiredPlacement === 'right' && hostElBoundingRect.right + targetElement.offsetWidth > window.innerWidth) {
            return 'left';
        }

        return desiredPlacement;
    }

    private getStyle(nativeEl : HTMLElement, cssProp : string) : string {
        // IE fix
        if ((nativeEl as any).currentStyle) {
            return (nativeEl as any).currentStyle[cssProp];
        }

        if (window.getComputedStyle) {
            return (window.getComputedStyle as any)(nativeEl)[cssProp];
        }

        // finally try and get inline style
        return (nativeEl.style as any)[cssProp];
    }

    private isStaticPositioned(nativeEl : HTMLElement) : boolean {
        return (this.getStyle(nativeEl, 'position') || 'static') === 'static';
    }

    private offset(nativeEl : any) : { width : number, height : number, top : number, left : number } {
        const boundingClientRect = nativeEl.getBoundingClientRect();
        return {
            width  : boundingClientRect.width || nativeEl.offsetWidth,
            height : boundingClientRect.height || nativeEl.offsetHeight,
            top    : boundingClientRect.top + (window.pageYOffset || window.document.documentElement.scrollTop),
            left   : boundingClientRect.left + (window.pageXOffset || window.document.documentElement.scrollLeft)
        };
    }

    private parentOffsetEl(nativeEl : HTMLElement) : any {
        let offsetParent : any = nativeEl.offsetParent || window.document;
        while (offsetParent && offsetParent !== window.document && this.isStaticPositioned(offsetParent)) {
            offsetParent = offsetParent.offsetParent;
        }
        return offsetParent || window.document;
    }

    private position(nativeEl : HTMLElement) : { width : number, height : number, top : number, left : number } {
        let offsetParentBCR  = { top : 0, left : 0 };
        const elBCR          = this.offset(nativeEl);
        const offsetParentEl = this.parentOffsetEl(nativeEl);
        if (offsetParentEl !== window.document) {
            offsetParentBCR = this.offset(offsetParentEl);
            offsetParentBCR.top += offsetParentEl.clientTop - offsetParentEl.scrollTop;
            offsetParentBCR.left += offsetParentEl.clientLeft - offsetParentEl.scrollLeft;
        }

        const boundingClientRect = nativeEl.getBoundingClientRect();
        return {
            width  : boundingClientRect.width || nativeEl.offsetWidth,
            height : boundingClientRect.height || nativeEl.offsetHeight,
            top    : elBCR.top - offsetParentBCR.top,
            left   : elBCR.left - offsetParentBCR.left
        };
    }

    private positionElements(hostEl : HTMLElement, targetEl : HTMLElement, positionStr : string, appendToBody : boolean = false) : { top : number, left : number } {
        const positionStrParts = positionStr.split('-');
        let pos0               = positionStrParts[0];
        const pos1             = positionStrParts[1] || 'center';
        const hostElPos        = appendToBody ? this.offset(hostEl) : this.position(hostEl);
        const targetElWidth    = targetEl.offsetWidth;
        const targetElHeight   = targetEl.offsetHeight;

        this.effectivePlacement = pos0 = this.getEffectivePlacement(pos0, hostEl, targetEl);

        const shiftWidth : any = {
            center : () => hostElPos.left + hostElPos.width / 2 - targetElWidth / 2,
            left   : () => hostElPos.left,
            right  : () => hostElPos.left + hostElPos.width
        };

        const shiftHeight : any = {
            center : () => hostElPos.top + hostElPos.height / 2 - targetElHeight / 2,
            top    : () => hostElPos.top,
            bottom : () => hostElPos.top + hostElPos.height
        };

        let targetElPos : { top : number, left : number };
        switch (pos0) {
            case 'right':
                targetElPos = {
                    top  : shiftHeight[pos1](),
                    left : shiftWidth[pos0]()
                };
                break;

            case 'left':
                targetElPos = {
                    top  : shiftHeight[pos1](),
                    left : hostElPos.left - targetElWidth
                };
                break;

            case 'bottom':
                targetElPos = {
                    top  : shiftHeight[pos0](),
                    left : shiftWidth[pos1]()
                };
                break;

            default:
                targetElPos = {
                    top  : hostElPos.top - targetElHeight,
                    left : shiftWidth[pos1]()
                };
                break;
        }

        return targetElPos;
    }

    hide() {
        this.top  = -10000;
        this.left = -10000;
        this.isIn = true;
        this.popover.hide();
    }

    hideFromPopover() {
        this.top  = -10000;
        this.left = -10000;
        this.isIn = true;
    }

    /**
     * Closes dropdown if user clicks outside of this directive.
     */
    onDocumentMouseDown = (event : any) => {
        const element = this.element.nativeElement;

        if (!element || !this.popover) {
            return;
        }
        if (element.contains(event.target) || this.popover.getElement().contains(event.target)) {
            return;
        }
        if (this.closeOnClickOutside === false && this.closeOnMouseOutside === false) {
            return;
        }

        this.hide();
        this.onCloseFromOutside.emit(undefined);
        this.cdr.detectChanges();
    }

    show() {
        if (!this.popover || !this.popover.getElement()) {
            return;
        }

        const p          = this.positionElements(this.popover.getElement(), this.popoverDiv.nativeElement, this.placement);
        this.displayType = 'block';
        this.top         = p.top;
        this.left        = p.left;
        this.isIn        = true;
    }

    ngAfterViewInit() {
        if (this.closeOnClickOutside) {
            this.listenClickFunc = this.renderer.listenGlobal('document', 'mousedown', (event : any) => this.onDocumentMouseDown(event));
        }
        if (this.closeOnMouseOutside) {
            this.listenMouseFunc = this.renderer.listenGlobal('document', 'mouseover', (event : any) => this.onDocumentMouseDown(event));
        }

        this.show();
        this.cdr.detectChanges();
    }

    ngOnDestroy() {
        if (this.closeOnClickOutside) {
            this.listenClickFunc();
        }
        if (this.closeOnMouseOutside) {
            this.listenMouseFunc();
        }
    }
}
