import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Output,
    EventEmitter
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {List} from 'immutable';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

import {SearchActions} from '../../../store/Search/search.actions';
import {BeneficiaryConnectionsActions} from '../../../store/Beneficiary/actions/beneficiary-connections.actions';
import {SearchStateSelectors} from '../../../store/Search/search.selectors';
import {PeopleSearchResult} from '../../../store/Search/types/people-search-result';
import {SearchResultsOrderByConfig} from '../../../store/Search/types/search-results-order-by-config.model';

@Component({
    selector        : 'person-finder',
    templateUrl     : 'person-finder.component.html',
    styleUrls       : ['person-finder.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for PersonFinderComponent: handles display of people finder
 */
export class PersonFinderComponent {
    /**
     * PersonFinderComponent constructor
     * @param cd
     * @param builder
     * @param searchActions
     * @param searchSelectors
     * @param beneficiaryConnectionsActions
     */
    constructor(
        private cd                              : ChangeDetectorRef,
        private builder                         : FormBuilder,
        private searchActions                   : SearchActions,
        private searchSelectors                 : SearchStateSelectors,
        private beneficiaryConnectionsActions   : BeneficiaryConnectionsActions
    ) {
        // subscribe to aggregated Redux state changes globally that we're interested in
        this.stateSubscription = Observable.combineLatest(
            this.searchSelectors.peopleSearchText(),
            this.searchSelectors.isPeopleSearchShowAllActive(),
            this.searchSelectors.peopleSearchResults(),
            this.searchSelectors.peopleSearchOrderByConfig()
        )
        .subscribe(val => {
            // update local state
            this.searchText             = val[0];
            this.isShowAllActive        = val[1];
            this.searchResults          = val[2];
            this.resultsOrderByConfig   = val[3];

            // trigger change detection
            this.cd.markForCheck();
        });

        // init form input fields
        this.searchBox = new FormControl('', Validators.required);

        // build personFinder FormControl group
        this.personFinderForm = builder.group({
            searchBox : this.searchBox
        });

        // subscribe to any form changes
        this.searchBox.valueChanges.debounceTime(250).subscribe((value : string) => {
            // update people search text
            this.searchActions.updatePeopleSearchText(value);
        });
    }

    /**
     * custom event emitted when search results order by config is changed
     * @type {EventEmitter<boolean>}
     */
    @Output() orderByConfigUpdated : EventEmitter<SearchResultsOrderByConfig> = new EventEmitter<SearchResultsOrderByConfig>();

    /**
     * Redux state subscription
     */
    private stateSubscription : Subscription;

    /**
     * user's entered people search text value
     */
    searchText : string;

    /**
     * expanded state of show all results tied to this value
     */
    isShowAllActive : boolean;

    /**
     * Current list of people search results
     */
    searchResults : List<PeopleSearchResult>;

    /**
     * people search results order by configuration
     */
    resultsOrderByConfig : SearchResultsOrderByConfig;

    /**
     * Object contains form group
     */
    personFinderForm : FormGroup;

    /**
     * searchBox input field
     */
    searchBox : FormControl;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for toggling show all search results
     */
    toggleShowAll() {
        this.searchActions.updatePeopleSearchShowAllResults();
    }

    /**
     * event handler for clearing search text
     */
    clearSearchField() {
        this.searchActions.updatePeopleSearchShowAllResults(false);

        this.personFinderForm.reset();
    }

    /**
     * asc / desc sort button click event handler
     * @param value
     * @param direction
     */
    changeOrderBy(value : string, direction : string) {
        let config = new SearchResultsOrderByConfig();

        config = config.set('sortColumn', value).set('sortOrder', direction) as SearchResultsOrderByConfig;

        // emit new config
        this.orderByConfigUpdated.emit(config);
    }

    /**
     * event handler for select a connection
     * @param person selected person
     */
    onSelectConnectionFromPeople(person : PeopleSearchResult) {
        // dispatch action to add connection
        this.beneficiaryConnectionsActions.addBeneficiaryConnectionFromPeopleSearch(person);

        // clear search text
        this.searchActions.updatePeopleSearchText('');

        // toggle show all results
        this.searchActions.updatePeopleSearchShowAllResults(true);

        // set flag to indicate CSR is adding a connection from search
        this.beneficiaryConnectionsActions.toggleConnectionAddedFromSearch(true);
        this.beneficiaryConnectionsActions.toggleConnectionAddedManually(false);
    }

    /**
     * component on destroy lifecycle hook
     */
    ngOnDestroy() {
        this.stateSubscription.unsubscribe();
    }
}
