/**
 * Copyright (c) 2016 Allen Kim <allenhwkim@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import {
    Injectable,
    Optional
} from '@angular/core';
import {
    Http,
    Response
} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {List} from 'immutable';

/**
 * provides auto-complete related utility functions
 */
@Injectable()
export class AutoComplete {
    constructor(@Optional() private http : Http) {}

    pathToData : string;
    source : Array<any> | List<any> | string;

    filter(list : any, keyword : string) {
        return list.filter(el => !!JSON.stringify(el).match(new RegExp(keyword, 'i')));
    }

    /**
     * return remote data from the given source and options, and data path
     */
    getRemoteData(keyword : string) : Observable<Response> {
        if (typeof this.source !== 'string') {
            throw new Error('Invalid type of source, must be a string. e.g. http://www.google.com?q=:my_keyword');
        }
        else if (!this.http) {
            throw new Error('Http is required.');
        }

        const matches = this.source.match(/:[a-zA-Z_]+/);

        if (matches === null) {
            throw new Error('Replacement word is missing.');
        }

        const replacementWord = matches[0];
        const url             = this.source.replace(replacementWord, keyword);

        return this.http.get(url)
            .map(resp => resp.json())
            .map(resp => {
                let list = resp.data || resp;

                if (this.pathToData) {
                    const paths = this.pathToData.split('.');
                    paths.forEach(prop => list = list[prop]);
                }

                return list;
            });
    };
}
