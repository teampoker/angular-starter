/**
 * Copyright (c) 2016 Allen Kim <allenhwkim@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import {TranslateService} from 'ng2-translate/ng2-translate';
import {AutoComplete} from './auto-complete.provider';
import {List} from 'immutable';

/**
 * show a selected date in monthly calendar
 * Each filteredList item has the following property in addition to data itself
 *   1. displayValue as string e.g. Allen Kim
 *   2. dataValue as any e.g.
 */
@Component({
    templateUrl: 'auto-complete.component.html',
    providers: [AutoComplete],
    styleUrls: [
        'auto-complete.component.scss'
    ],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutoCompleteComponent implements OnInit {
    constructor(
        elementRef          : ElementRef,
        public autoComplete : AutoComplete,
        private translate   : TranslateService,
        private cd          : ChangeDetectorRef
    ) {
        this.el = elementRef.nativeElement;
    }

    /**
     * public input properties
     */

    /**
     * reservation reset form indicator
     */
    @Input() resetForm : boolean = false;

    @Input() acceptUserInput : boolean;
    @Input() blankOptionText : string;
    @Input() disableFilter : boolean;
    @Input() displayPropertyName : string = 'value';
    @Input() listFormatter : (arg : any) => string;
    @Input() loadingText : string = 'Loading';
    @Input() maxNumList : number;
    @Input() minChars : number;
    @Input() noMatchFoundText : string;
    @Input() pathToData : string;
    @Input() placeholder : string;
    @Input() showInputTag : boolean = true;
    @Input() source : Array<any> | List<any> | string;
    @Input() valuePropertyName : string = 'id';

    @Output() valueSelected : EventEmitter<any> = new EventEmitter();
    @Output() inputChanged : EventEmitter<any> = new EventEmitter();

    @ViewChild('autoCompleteInput') autoCompleteInput : ElementRef;

    private defaultListFormatter(data : any) : string {
        let html : string = '';
        html += data[this.valuePropertyName] ? `<b>(${data[this.valuePropertyName]})</b>` : '';
        html += data[this.displayPropertyName] ? `<span>${data[this.displayPropertyName]}</span>` : data;
        return html;
    }

    private delay : any = (() => {
        let timer = 0;

        return (callback : any, ms : number) => {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    dropdownVisible : boolean = false;
    el : HTMLElement;           // this component  element `<ng2-auto-complete>`
    filteredList : Array<any> | List<any> = [];
    isLoading : boolean = false;
    itemIndex : number = 0;
    keyword : string;
    minCharsEntered : boolean = false;

    get emptyList() : boolean {
        return !(
            this.isLoading ||
            (this.minCharsEntered && !this.isLoading && this.getListLength() === 0) ||
            (this.getListLength())
        );
    }

    getFormattedList(data : any) : string {
        const formatter = this.listFormatter || this.defaultListFormatter;

        return this.translate.instant(formatter.apply(this, [data]).trim());
    }

    getItemFromList(index : number) : any {
        return List.isList(this.filteredList) ?
            (this.filteredList as List<any>).get(index) : this.filteredList[index];
    }

    getListLength() : number {
        return List.isList(this.filteredList) ?
            (this.filteredList as List<any>).count() : (this.filteredList as Array<any>).length;
    }

    hideDropdownList() {
        this.dropdownVisible = false;
    }

    inputElKeyHandler(evt : any) {
        const totalNumItem = this.getListLength();

        switch (evt.keyCode) {
            case 27: // ESC, hide auto complete
                this.hideDropdownList();
                this.cd.markForCheck();
                break;

            case 38: // UP, select the previous li el
                this.itemIndex = (totalNumItem + this.itemIndex - 1) % totalNumItem;
                this.cd.markForCheck();
                break;

            case 40: // DOWN, select the next li el or the first one
                this.dropdownVisible = true;
                this.itemIndex = (totalNumItem + this.itemIndex + 1) % totalNumItem;
                this.cd.markForCheck();
                break;

            case 13: // ENTER, choose it!!
                if (totalNumItem > 0) {
                    this.selectOne(this.getItemFromList(this.itemIndex));
                }
                evt.preventDefault();
                this.cd.markForCheck();
                break;
            default:
                break;
        }
    }

    isSourceArray() : boolean {
        return Array.isArray(this.source);
    }

    isSourceList() : boolean {
        return List.isList(this.source);
    }

    reloadList(keyword : string = '') {
        this.filteredList = [];

        this.minCharsEntered = this.minChars <= keyword.length;

        if (!this.minCharsEntered) {
            this.cd.markForCheck();
            return;
        }

        if (this.isSourceArray() || this.isSourceList()) {    // local source
            this.isLoading = false;
            this.filteredList = this.disableFilter ? this.source : this.autoComplete.filter(this.source, keyword);

            if (this.maxNumList) {
                this.filteredList = this.filteredList.slice(0, this.maxNumList) as any;
            }

            this.cd.markForCheck();
        }
        else {                 // remote source
            this.isLoading = true;

            if (typeof this.source === 'function') {
                // custom function that returns observable
                this.source(keyword).subscribe(
                    resp => {
                        if (this.pathToData) {
                            const paths = this.pathToData.split('.');
                            paths.forEach(prop => resp = resp[prop]);
                        }

                        this.filteredList = resp;
                        if (this.maxNumList) {
                            this.filteredList = this.filteredList.slice(0, this.maxNumList) as any;
                        }
                    },
                    error => null,
                    () => {
                        // complete
                        this.isLoading = false;

                        this.cd.markForCheck();
                    }
                );
            }
            else {
                // remote source

                this.autoComplete.getRemoteData(keyword).subscribe(resp => {
                        this.filteredList = resp as any;
                        if (this.maxNumList) {
                            this.filteredList = this.filteredList.slice(0, this.maxNumList) as any;
                        }
                    },
                    error => null,
                    () => {
                        this.isLoading = false;
                        this.cd.markForCheck();
                    }
                );
            }
        }
    }

    reloadListInDelay(evt : any) {
        const delayMs = this.isSourceArray() || this.isSourceList() ? 10 : 500;
        const keyword = evt.target.value;

        // executing after user stopped typing
        this.delay(() => this.reloadList(keyword), delayMs);
        this.inputChanged.emit(keyword);
    }

    selectOne(data : any) {
        this.valueSelected.emit(data);
    }

    showDropdownList(event : any) {
        this.dropdownVisible = true;
        this.reloadList(event.target.value);
    }

    /**
     * user enters into input el, shows list to select, then select one
     */
    ngOnInit() {
        this.autoComplete.source = this.source;
        this.autoComplete.pathToData = this.pathToData;
        setTimeout(() => {
            if (this.autoCompleteInput) {
                this.autoCompleteInput.nativeElement.focus();
            }
        });
    }
}
