import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Output
} from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import {List} from 'immutable';
import {Observable} from 'rxjs';

import {ReservationStateSelectors} from '../../../store/Reservation/selectors/reservation.selectors';
import {NameUuid} from '../../../store/types/name-uuid.model';
import {NavActions} from '../../../store/Navigation/nav.actions';

@Component({
    selector : 'reservation-cancel-modal',
    templateUrl : 'reservation-cancel-modal.component.html',
    styleUrls : ['reservation-cancel-modal.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
export class ReservationCancelModalComponent {
    /**
     * ReservationCancelModalComponent constructor
     * @param {FormBuilder} fb
     * @param {ReservationStateSelectors} reservationSelectors
     * @param {NavActions} navActions
     */
    constructor(
        private fb                      : FormBuilder,
        private reservationSelectors    : ReservationStateSelectors,
        private navActions              : NavActions

    ) {
        this.form = this.fb.group({
            cancelComment : new FormControl(''),
            cancelReason : new FormControl(undefined, Validators.required)
        });

        this.cancelReasons = this.reservationSelectors.getCancelReasons();
    }

    /**
     * event fired when cancelling the cancellation
     * @type {EventEmitter<undefined>}
     */
    @Output() cancel : EventEmitter<undefined> = new EventEmitter();

    /**
     * event fired when the cancellation is confirmed
     * @type {EventEmitter<{ eventReasonUuid : string, eventComment : string }>}
     */
    @Output() confirm : EventEmitter<{ eventReasonUuid : string, eventComment : string }> = new EventEmitter();

    /**
     * observable of the list of cancellations
     * @type {Observable<List<NameUuid>>}
     */
    cancelReasons : Observable<List<NameUuid>>;

    /**
     * the form
     * @type {FormGroup}
     */
    form : FormGroup;

    /**
     * the reason for the cancellation
     * @type {NameUuid}
     */
    selectedReason : NameUuid = new NameUuid();

    /**
     * cancels the...cancellation
     */
    onCancel() {
        this.cancel.emit();
    }

    /**
     * confirms the form entries
     */
    onConfirm() {
        this.confirm.emit({
            eventComment : this.form.get('cancelComment').value,
            eventReasonUuid : this.selectedReason.get('uuid')
        });
        this.navActions.toggleIsPoppedNavState(false);
    }

    /**
     * sets the selected reason
     * @param {NameUuid} reason
     */
    onSelectReason(reason : NameUuid) {
        this.selectedReason = reason;
    }

    /**
     * ngFor item tracker
     * @param {number} index
     * @param {NameUuid} reason
     * @returns {string}
     */
    trackByUuid(index : number, reason : NameUuid) : string {
        return reason.get('uuid');
    }
}
