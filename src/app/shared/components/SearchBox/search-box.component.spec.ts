import {
    ComponentFixture,
    TestBed,
    async
} from '@angular/core/testing';
import {TranslateModule} from 'ng2-translate';
import {
    FormsModule,
    ReactiveFormsModule
} from '@angular/forms';
import {NgReduxModule} from '@angular-redux/store';

import {SearchBoxComponent} from './search-box.component';
import {SearchStateSelectors} from '../../../store/Search/search.selectors';

// test definitions here
describe('Component: SearchBoxComponent', () => {
    let componentInstance   : SearchBoxComponent,
        fixture             : ComponentFixture<SearchBoxComponent>;

    // use the async() wrapper to allow the Angular template compiler time to read the files
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations : [
                SearchBoxComponent     // declare the test component
            ],
            imports      : [
                TranslateModule.forRoot(),   // import the translation for template pipes
                FormsModule,
                ReactiveFormsModule,
                NgReduxModule
            ],
            providers    : [
                SearchStateSelectors
            ]
        });
    }));

    beforeEach(() => {
        fixture           = TestBed.createComponent(SearchBoxComponent);
        componentInstance = fixture.componentInstance;  // search box test instance
    });

    describe('defaults', () => {
        it('should be defined', () => {
            expect(componentInstance).toBeDefined();
        });
    });

    it('should emit search result event when submitted', () => {
        // arrange the test
        spyOn(componentInstance.getSearchResults, 'emit');
        componentInstance.searchText = 'arya';

        // trigger the form submit
        componentInstance.onSubmit();

        expect(componentInstance.getSearchResults.emit).toHaveBeenCalled();
    });

    xit('should emit update search type event when category changed', () => {
        // arrange the test
        spyOn(componentInstance.updateSearchType, 'emit');

        // TODO: setup the trigger for category change here

        expect(componentInstance.getSearchResults.emit).toHaveBeenCalled();
    });
});
