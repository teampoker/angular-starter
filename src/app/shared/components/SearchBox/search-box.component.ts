import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {List} from 'immutable';

import {KeyValuePair} from '../../../store/types/key-value-pair.model';
import {SearchStateSelectors} from '../../../store/Search/search.selectors';

@Component({
    selector        : 'search-box',
    templateUrl     : './search-box.component.html',
    styleUrls       : ['./search-box.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})
export class SearchBoxComponent {
    /**
     * StatementsComponent constructor
     * @param builder
     * @param searchSelector used by template to toggle search button state
     */
    constructor(
        private builder         : FormBuilder,
        private searchSelector  : SearchStateSelectors
    ) {
        // init form input fields
        this.searchBox = new FormControl('', Validators.required);

        // build FormControl group
        this.searchForm = builder.group({
            searchBox : this.searchBox
        });

        // subscribe to any form changes
        this.searchBox.valueChanges.debounceTime(100).subscribe((value : string) => {
            // emit new search request event
            this.updateSearchText.emit(value.trim());
        });
    }

    /**
     * list of available categories
     */
    @Input() searchTypeOptions : List<KeyValuePair>;

    /**
     * currently selected category
     */
    @Input() selectedCategory : KeyValuePair;

    /**
     * user's entered search text value
     */
    @Input() searchText : string;

    /**
     * custom event emitted when user enters search text
     */
    @Output() updateSearchText : EventEmitter<string> = new EventEmitter<string>();

    /**
     * custom event emitted when user chooses a search category
     */
    @Output() updateSearchType : EventEmitter<KeyValuePair> = new EventEmitter<KeyValuePair>();

    /**
     * custom event emitted when user clicks submit
     */
    @Output() getSearchResults : EventEmitter<any> = new EventEmitter<any>();

    /**
     * grouping object for search form
     */
    searchForm : FormGroup;

    /**
     * search input field
     */
    searchBox : FormControl;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for search category selection event
     * @param choice selected option value
     * @param event DOM event
     */
    selectSearchCategory(choice : KeyValuePair, event : any) {
        // update chosen search category
        this.updateSearchType.emit(choice);
    }

    /**
     * search submission event
     * triggered on submission method connects to search service to get search results.
     */
    onSubmit() {
        // trigger search event
        this.getSearchResults.emit();
    }
}
