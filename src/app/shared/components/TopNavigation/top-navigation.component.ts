import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {List} from 'immutable';

import {KeyValuePair} from '../../../store/types/key-value-pair.model';
import {NavActions} from '../../../store/Navigation/nav.actions';
import {SearchActions} from '../../../store/Search/search.actions';
import {SearchStateSelectors} from '../../../store/Search/search.selectors';
import {NavStateSelectors} from '../../../store/Navigation/nav.selectors';
import {SearchBoxState} from '../../../store/Search/types/search-box-state.model';
import {TaskItem} from '../../../store/Navigation/types/task-item.model';
import {
    EnumNavOption,
    NavOption
} from '../../../store/Navigation/types/nav-option.model';
import {UserStateSelectors} from '../../../store/User/user.selectors';

@Component({
    selector        : 'top-navigation',
    templateUrl     : './top-navigation.component.html',
    styleUrls       : ['./top-navigation.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for TopNavigationComponent: responsible for top level header menu navigation
 */
export class TopNavigationComponent {
    /**
     * TopNavigationComponent constructor
     * @param cd
     * @param navActions
     * @param searchActions
     * @param searchSelectors
     * @param navSelectors
     * @param userSelectors
     * @param router
     */
    constructor(
        private cd                  : ChangeDetectorRef,
        private navActions          : NavActions,
        private searchActions       : SearchActions,
        private searchSelectors     : SearchStateSelectors,
        private navSelectors        : NavStateSelectors,
        private userSelectors       : UserStateSelectors,
        private router              : Router
    ) {
        // subscribe to aggregated Redux state changes globally that we're interested in
        this.stateSubscription = Observable.combineLatest(
            this.searchSelectors.headerSearchBoxState(),
            this.navSelectors.activeNavState(),
            this.navSelectors.topNavConfig(),
            this.navSelectors.taskList(),
            this.navSelectors.toggleTaskList()
        )
        .subscribe(val => {
            // update local state
            this.navSearchState = val[0];
            this.activeNavState = val[1];
            this.navConfig      = val[2];
            this.taskList       = val[3];
            this.toggleTasks    = val[4];

            // trigger change detection
            this.cd.markForCheck();
        });

        // subscribing outside the combineLatest block to avoid coupling with other selectors
        this.userSelectors.userName.subscribe(name => {
            this.userFullName = name;

            // trigger change detection to pickup the new value
            this.cd.markForCheck();
        });
    }

    /**
     * subscription to component's redux state
     */
    private stateSubscription : Subscription;

    /**
     * current snapshot of active nav state
     */
    activeNavState : EnumNavOption;

    /**
     * current snapshot of top nav search state
     */
    navSearchState : SearchBoxState;

    /**
     * current snapshot of topNavConfig
     */
    navConfig : List<NavOption>;

    /**
     * current snapshot of taskList
     */
    taskList : List<TaskItem>;

    /**
     * toggle state of taskList
     */
    toggleTasks : boolean;

    /**
     * user's display name
     */
    userFullName : string;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for search text update event
     */
    onUpdateSearchText(searchText : string) {
        this.searchActions.updateHeaderSearchText(searchText);
    }

    /**
     * event handler for search category update event
     */
    onUpdateSearchType(searchType : KeyValuePair) {
        this.searchActions.updateSelectedHeaderSearchType(searchType);
    }

    /**
     * event handler for search event
     */
    onGetSearchResults() {
        // reset configured search parameters to defaults
        this.searchActions.resetSearchParameters();

        // query for search results
        this.searchActions.getHeaderSearchResults();
    }

    /**
     * updates app navigation state and handle router navigation step
     * @param navItem current nav state to navigate to
     */
    doMenuNavigation(navItem : NavOption) {
        switch (navItem.baseNavOption) {
            case EnumNavOption.TASKS:
                this.navActions.toggleTaskList(this.toggleTasks);
                break;
            case EnumNavOption.LOGOUT:
                // log the user out
                this.router.navigate(['Logout']);
                break;
            default:
                // navigate within the application
                this.navActions.doMenuNavigation(navItem);
                break;
        }
    }

    /**
     * Component on destroy lifecycle hook
     */
    ngOnDestroy () {
        // unsubscribe
        this.stateSubscription.unsubscribe();
    }
}
