import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    Validators
} from '@angular/forms';
import {List} from 'immutable';

import {KeyValuePair} from '../../../store/types/key-value-pair.model';
import {BeneficiaryPhone} from '../../../store/Beneficiary/types/beneficiary-phone.model';
import {IBeneficiaryFieldUpdate} from '../../../store/Beneficiary/types/beneficiary-field-update.model';
import {formatPhoneForApi, PHONE_MASK_REGEX} from '../../../store/utils/parse-phone-numbers';

@Component({
    selector        : 'beneficiary-connection-phone',
    templateUrl     : 'beneficiary-connection-phone.component.html',
    styleUrls       : ['beneficiary-connection-phone.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryConnectionPhoneComponent: handles display of this section panel
 */
export class BeneficiaryConnectionPhoneComponent {
    /**
     * BeneficiaryConnectionPhoneComponent constructor
     * @param builder
     */
    constructor(private builder : FormBuilder) {
        // init form input fields
        this.phone              = new FormControl('', Validators.compose([
            Validators.required,
            Validators.pattern(/^(\(\d{3}\))|(\d{3}-)\d{3}-\d{4}$/)
        ]));
        this.phoneType          = new FormControl('', this.validateSelectDropDown);
        this.phoneNotifications = new FormControl('', Validators.required);

        // build Beneficiary Information FormControl group
        this.phoneForm = builder.group({
            phone              : this.phone,
            phoneType          : this.phoneType,
            phoneNotifications : this.phoneNotifications
        });

        this.phoneForm.valueChanges.subscribe(() => {
            // emit update form valid state
            this.phoneFormValid.emit(this.phoneForm.valid);
        });

        this.phone.valueChanges.debounceTime(250).subscribe((value : string) => {
            // format entered value for format api expects
            value = formatPhoneForApi(value);

            // emit updated value
            this.updatePhoneField.emit({
                fieldName   : 'phone',
                fieldValue  : value
            });
        });
    }

    /**
     * beneficiary phone instance
     */
    @Input() connectionPhone : BeneficiaryPhone;

    /**
     * if flag is true, phone values are for read only display
     */
    @Input() isPhoneReadOnly : boolean;

    /**
     * available solicitationIndicator types for dropdowns
     */
    @Input() solicitationIndicatorTypes : List<KeyValuePair>;

    /**
     * available phone types for dropdowns
     */
    @Input() phoneTypes : List<KeyValuePair>;

    /**
     * event triggered when beneficiary phone form fields are edited/updated
     * @type {EventEmitter<IBeneficiaryFieldUpdate>}
     */
    @Output() updatePhoneField : EventEmitter<IBeneficiaryFieldUpdate> = new EventEmitter<IBeneficiaryFieldUpdate>();

    /**
     * event triggered when the beneficiary information form is edited/updated
     * @type {"events".EventEmitter}
     */
    @Output() phoneFormValid : EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * custom validation for authorizationReason FormControl
     */
    private validateSelectDropDown(c : FormControl) : { [key : string] : any } {
        if (c.value && c.value !== 'SELECT') {
            return null;
        }

        return {
            validatePhoneType : {
                valid : false
            }
        };
    }

    /**
     * Object contains form group
     */
    phoneForm : FormGroup;

    /**
     * phone input field
     */
    phone : FormControl;

    /**
     * phoneType input field
     */
    phoneType : FormControl;

    /**
     * phoneNotifications input field
     */
    phoneNotifications : FormControl;

    /**
     * phoneMaskRegex : regext for phone number mask
     */
    phoneMaskRegex  :  Array<any> = PHONE_MASK_REGEX;

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * event handler for phone type selection event
     * @param choice selected phone type
     */
    updatePhoneType(choice : KeyValuePair) {
        // emit updated value
        this.updatePhoneField.emit({
            fieldName   : 'phoneType',
            fieldValue  : choice
        });
    }

    /**
     * event handler for phoneNotifications selection event
     * @param choice selected phone notifications type
     */
    updatePhoneNotifications(choice : KeyValuePair) {
        // emit updated value
        this.updatePhoneField.emit({
            fieldName   : 'phoneNotifications',
            fieldValue  : choice
        });
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        // emit update
        this.phoneFormValid.emit(this.phoneForm.valid);

        if (this.isPhoneReadOnly) {
            // TODO remove this later when they let CSR edit the connection person fields
            this.phoneForm.get('phone').disable();
        }
    }
}
