import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    Output,
    SimpleChanges
} from '@angular/core';
import {List} from 'immutable';
import * as moment from 'moment';
import {Subscription} from 'rxjs';

import {ReservationActions} from '../../../store/Reservation/actions/reservation.actions';
import {ReservationStateSelectors} from '../../../store/Reservation/selectors/reservation.selectors';
import {ReservationCardState} from '../../../store/Reservation/types/reservation-card-state.model';
import {Reservation} from '../../../store/Reservation/types/reservation.model';
import {ReservationLegState} from '../../../store/Reservation/types/reservation-leg-state.model';
import {getStatusColor, showDeniedReason} from '../../../store/utils/get-reservation-status-info';
import {NavActions} from '../../../store/Navigation/nav.actions';

@Component({
    selector        : 'reservation-card',
    templateUrl     : 'reservation-card.component.html',
    styleUrls       : ['reservation-card.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for ReservationCardComponent: handles display of section panels
 */
export class ReservationCardComponent implements OnChanges {
    /**
     * ReservationCardComponent constructor
     *
     * @param {ReservationActions} reservationActions
     * @param {ReservationStateSelectors} reservationSelectors
     * @param {ChangeDetectorRef} cd
     * @param {NavActions} navActions
     */
    constructor(
        private reservationActions      : ReservationActions,
        private reservationSelectors    : ReservationStateSelectors,
        private cd                      : ChangeDetectorRef,
        private navActions              : NavActions
    ) {}

    /**
     * Beneficiary's Uuid
     * @type {String}
     */
    @Input() beneficiaryUuid : string;

    /**
     * reservation Card State ui
     * @type {ReservationCardState}
     */
    @Input() reservationCardState : ReservationCardState;

    /**
     * List of reservations
     * @type {List<Reservation>}
     */
    @Input() reservations : List<Reservation>;

    /**
     * String containing widget information
     * @type {String}
     */
    @Input() title : string;

    /**
     * emits an event when a user closes "more details"
     * @type {EventEmitter<string>}
     */
    @Output() onCloseDetails : EventEmitter<undefined> = new EventEmitter<undefined>();

    /**
     * emits an event when a user requests more details for an item
     * @type {EventEmitter<string>}
     */
    @Output() onShowDetails : EventEmitter<string> = new EventEmitter<string>();

    /**
     * emits an event when a user toggles a leg
     * @type {EventEmitter<number>}
     */
    @Output() onToggleLeg : EventEmitter<number> = new EventEmitter<number>();

    /**
     * custom event emitted when user wishes to navigate to add beneficiary profile
     */
    @Output() viewEditReservation : EventEmitter<any> = new EventEmitter<any>();

    /**
     * subscription for selected reservation retrieval
     */
    private reservationSubscription : Subscription;

    /**
     * Ascending sorting function based on date
     * @param a
     * @param b
     * @returns {number}
     */
    private sortAsc(a : Reservation, b : Reservation) : number {
        return (moment.utc(a.getIn(['legs', 0, 'reservationDateTime', 'date']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]') as any) -
            (moment.utc(b.getIn(['legs', 0, 'reservationDateTime', 'date']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]') as any);
    }

    /**
     * Ascending sorting function based on date
     * @param a
     * @param b
     * @returns {number}
     */
    private sortDesc(a : Reservation, b : Reservation) : number {
        return (moment.utc(b.getIn(['legs', 0, 'reservationDateTime', 'date']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]') as any) -
            (moment.utc(a.getIn(['legs', 0, 'reservationDateTime', 'date']), 'YYYY-MM-DDTHH:mm:ssZ[UTC]') as any);
    }

    /**
     * Date format to use in date picker
     * @type {string}
     */
    dateFormat : string = 'MM/DD/YYYY';

    /**
     * Flag for view more/less.
     * @type {boolean}
     */
    expanded : boolean;

    /**
     * Selected reservation
     * @type {Reservation}
     */
    reservation : Reservation = new Reservation();

    /**
     * the reservation that is selected to be cancelled
     * @type {Reservation}
     */
    reservationToCancel : Reservation;

    /**
     * Date format to use in date picker
     * @type {string}
     */
    timeFormat : string = 'h:mm a';

    /**
     * Date format sent by the api
     * @type {string}
     */
    utcFormat : string = 'YYYY-MM-DDTHH:mm:ssZ[UTC]';

    /**
     * starts the cancellation confirmation workflow
     * @param {Reservation} reservation
     */
    cancelReservation(reservation : Reservation) {
        this.reservationToCancel = reservation;
        this.navActions.toggleIsPoppedNavState(true);
    }

    /**
     * executes cancellation API workflow
     * @param {{ eventReasonUuid : string, eventComment : string }} reason
     */
    confirmCancelReservation(reason : { eventReasonUuid : string, eventComment : string }) {
        this.reservationActions.cancelReservation(this.reservationToCancel, reason.eventReasonUuid, reason.eventComment);
        this.reservationToCancel = undefined;
    }

    /**
     * cancels the cancellation workflow
     */
    cancelReservationCancellation() {
        this.reservationToCancel = undefined;
        this.navActions.toggleIsPoppedNavState(false);
    }

    /**
     * event handler for closing reservation details
     */
    closeDetails() {
        this.reservation = new Reservation();

        if (this.reservationSubscription) {
            this.reservationSubscription.unsubscribe();
        }

        this.onCloseDetails.emit();
    }

    /**
     * event handler for editing a reservation
     * @param uuid {string}
     */
    editReservation(uuid : string) {
        // emit to viewEditReservation
        this.viewEditReservation.emit(uuid);
    }

    /**
     * Returns reservations for display
     * @returns {List<Reservation>}
     */
    getReservations() : List<Reservation> {
        let reservations : List<Reservation>;

        if (this.title === 'ScheduledReservations') {
            reservations = this.reservations
                .sort(this.sortAsc)
                .take(5) as List<Reservation>;
        }
        else {
            reservations = this.reservations
                .sort(this.sortDesc)
                .take(5) as List<Reservation>;
        }

        return reservations;
    }

    /**
     * Returns color for status
     * @returns {string}
     */
     getStatusColor(statusId : number) {
         return getStatusColor(statusId);
     }

    /**
     * Returns boolean used to show denied reaons
     * @returns {boolean}
     */
     showDeniedReason (statusId : number) {
         return showDeniedReason(statusId);
     }

    /**
     * Returns a sorted list of legs
     * @param {Reservation} reservation
     * @returns {List<ReservationLegState>}
     */
    getSortedLegs(reservation : Reservation) : List<ReservationLegState> {
        return reservation.get('legs').sortBy(leg => leg.get('ordinality'));
    }

    /**
     * event handler for showing reservation details
     * @param uuid {string}
     */
    showDetails(uuid : string) {
        this.onShowDetails.emit(uuid);
    }

    /**
     * event handler for toggling view more/less
     */
    toggleExpanded(index : number) {
        this.onToggleLeg.emit(index);
    }

    ngOnChanges(changes : SimpleChanges) {
        const selectedItemUuid = this.reservationCardState.get('selectedItemUuid');

        if (selectedItemUuid && this.reservation.get('uuid', '') !== selectedItemUuid) {
            if (this.reservationSubscription) {
                this.reservationSubscription.unsubscribe();
            }

            this.reservationActions.fetchReservation(selectedItemUuid);

            this.reservationSubscription = this.reservationSelectors
                .getReservation(selectedItemUuid)
                .subscribe(reservation => {
                    this.reservation = reservation;
                    this.cd.markForCheck();
                });
        }
    }
}
