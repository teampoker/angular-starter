import {
    Component,
    trigger,
    state,
    style,
    transition,
    animate,
    ApplicationRef
} from '@angular/core';
import {
    Toast,
    ToastrService
} from 'ngx-toastr';
import {ToastData} from 'ngx-toastr/toastr-config';
import {ToastRef} from 'ngx-toastr/toast-injector';
import {DomSanitizer} from '@angular/platform-browser';

// tslint:disable: no-access-missing-member
@Component({
    // tslint:disable-next-line: component-selector
    selector    : '[toast-component]',
    templateUrl : './user-notification.component.html',
    styleUrls   : ['./user-notification.component.scss'],
    animations  : [
        trigger('flyInOut', [
            state('inactive', style({
                display : 'none',
                opacity : 0
            })),
            state('active', style({
                opacity : 1
            })),
            state('removed', style({
                opacity : 0
            })),
            transition('inactive <=> active', animate('300ms ease-in')),
            transition('active <=> removed', animate('300ms ease-in'))
        ])
    ]
})
// tslint:disable: no-access-missing-member
export class UserNotificationComponent extends Toast {
    /**
     * Component constructor.
     * @param toastrService
     * @param data
     * @param toastRef
     * @param appRef
     * @param sanitizer
     */
    constructor(
        toastrService   : ToastrService,
        data            : ToastData,
        toastRef        : ToastRef<any>,
        appRef          : ApplicationRef,
        sanitizer       : DomSanitizer
    ) {
        super(toastrService, data, toastRef, appRef, sanitizer);
    }
}
