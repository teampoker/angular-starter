import {
    Component,
    Input,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {List} from 'immutable';
import {Subscription} from 'rxjs/Subscription';

import {KeyValuePair} from '../../../store/types/key-value-pair.model';
import {MetaDataTypesSelectors} from '../../../store/MetaDataTypes/meta-data-types.selectors';
import {EnumConnectionTypes} from '../../../store/Beneficiary/types/beneficiary-connection-types.model';

@Component({
    selector        : 'connection-type-name',
    templateUrl     : 'connection-type-name.component.html',
    styleUrls       : ['connection-type-name.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Used to display connection type name
 */
export class ConnectionTypeNameComponent {
    /**
     * ConnectionTypeNameComponent constructor
     * @param metaDataTypesSelectors
     * @param cd
     */
    constructor(
        private cd                      : ChangeDetectorRef,
        private metaDataTypesSelectors  : MetaDataTypesSelectors
    ) {
        // setup subscription to beneficiary state
        this.stateSubscription = this.metaDataTypesSelectors.personConnectionTypes().subscribe(types => {
            // update local state
            this.personConnectionTypes = types;

            // trigger change detection
            this.cd.markForCheck();
        });
    }

    /**
     * String containing widget information
     * @type {String}
     */
    @Input() connectionType : EnumConnectionTypes;

    /**
     * Redux state subscription
     */
    private stateSubscription : Subscription;

    /**
     * Enum connection types to match against
     * @type {EnumConnectionTypes}
     */
    private connectionTypes : typeof EnumConnectionTypes = EnumConnectionTypes;

    /**
     * string placeholder for connectionTypeDisplay
     * @type {string}
     */
    connectionTypeDisplay : string = '';

    /**
     * list of all possible connection types
     */
    personConnectionTypes : List<KeyValuePair>;

    /**
     * method to figure out which person connection type name to display
     */
     findConnectionTypeDisplay () {
         switch (this.connectionType) {
             case this.connectionTypes.DRIVER :
                 this.connectionTypeDisplay = this.personConnectionTypes.find(value => value.get('value') === 'Driver').get('value');

                 break;
             case this.connectionTypes.EMERGENCY_CONTACT :
                 this.connectionTypeDisplay = this.personConnectionTypes.find(value => value.get('value') === 'Emergency Contact').get('value');

                 break;
             case this.connectionTypes.NURSE_DOCTOR :
                 this.connectionTypeDisplay = this.personConnectionTypes.find(value => value.get('value') === 'Nurse / Doctor / Counselor').get('value');

                 break;
             case this.connectionTypes.PARENT_FAMILY :
                 this.connectionTypeDisplay = this.personConnectionTypes.find(value => value.get('value') === 'Parent / Family').get('value');

                 break;
             case this.connectionTypes.PLAN_CLIENT :
                 this.connectionTypeDisplay = this.personConnectionTypes.find(value => value.get('value') === 'Plan / Client').get('value');

                 break;
             case this.connectionTypes.SOCIAL_WORKER :
                 this.connectionTypeDisplay = this.personConnectionTypes.find(value => value.get('value') === 'Social / Case Worker / Case Manager').get('value');

                 break;
             default :
                 this.connectionTypeDisplay = this.personConnectionTypes.find(value => value.get('value') === 'Driver').get('value');

                 break;
         }
     }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        this.findConnectionTypeDisplay();
    }

    /**
     * component destroy lifecycle hook
     */
    ngOnDestroy() {
        // unsubscribe
        this.stateSubscription.unsubscribe();
    }
}
