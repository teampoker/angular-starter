import {
    Component,
    Input,
    Output,
    EventEmitter,
    ElementRef,
    ViewChild,
    ChangeDetectionStrategy,
    forwardRef,
    Renderer
} from '@angular/core';
import {
    FormControl,
    NG_VALUE_ACCESSOR,
    NG_VALIDATORS,
    ControlValueAccessor,
    ValidatorFn
} from '@angular/forms';
import * as moment from 'moment';
import * as pikaday from 'pikaday';

/**
 * This validator function  takes in the form control and returns a key-value pair of validated items. If the form control is valid, it returns null
 *
 * func is exported to allow for testing a component that has the form with the DOM i.e. it can be imported
 * into the parent form component's specs
 *
 * @param dateFormat format string to use when validating selected date
 * @param allowBlank flag that indicates an empty string value should NOT trigger validation error
 * @returns {any}
 */
export function createDateValidator(dateFormat : string, allowBlank? : boolean) : ValidatorFn {
    return function validateSelectedDate(c : FormControl) : { [key : string] : any } {
        // validate the selected date
        const validDate : boolean = moment(c.value, dateFormat).isValid();

        if (validDate) {
            return null;
        }
        else {
            // check for empty string
            if (c.value === '' && allowBlank) {
                return null;
            }

            return {
                invalidDate : {
                    valid : false
                }
            };
        }
    };
}

@Component({
    selector        : 'date-picker',
    templateUrl     : 'date-picker.component.html',
    styleUrls       : ['date-picker.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush,

    /**
     * Since our custom component now implements the ControlValueAccessor we can register it as a provider.
     * extend the multi-provider for NG_VALUE_ACCESSOR with our own value accessor instance
     */
    providers: [
        {
            /**
             * we are extending what is being injected for the NG_VALUE_ACCESSOR token
             */
            provide : NG_VALUE_ACCESSOR,

            /**
             * we use useExisting because DatePickerComponent will be already created as a directive dependency in the component that uses it.
             * If we don’t do that, we get a new instance as this is how DI in Angular works
             *
             * We need to use forwardRef since our Component is not defined at this point
             */
            useExisting : forwardRef(() => DatePickerComponent),

            /**
             * we use multi: true because NG_VALUE_ACCESSOR can actually have multiple providers registered with the same component
             */
            multi : true
        },
        {
            /**
             * we are extending what is being injected for the NG_VALIDATORS token
             */
            provide : NG_VALIDATORS,

            /**
             * custom validation for selected date values
             * implements the Validator interface:
             *
             *  validate(c: AbstractControl): {
             *     [key: string]: any;
             *  };
             */
            useValue : createDateValidator,

            /**
             * multi value of true signifies we are providing multiple values for a single token i.e. NG_VALUE_ACCESSOR in this case
             */
            multi : true
        }
    ]
})

/**
 * implementation for DatePickerComponent: implements an Angular 2.x wrapped pikaday date picker instance
 * The ControlValueAccessor interface will provide us with methods that will interface operations for our control to write values to the native browser DOM
 * reference: https://angular.io/docs/ts/latest/api/forms/index/ControlValueAccessor-interface.html
 */
export class DatePickerComponent implements ControlValueAccessor {
    /**
     * DatePickerComponent constructor
     * @param renderer
     */
    constructor(private renderer : Renderer) {}

    /**
     * placeholder for datepicker DOM element reference
     */
    @ViewChild('datepicker') input : ElementRef;

    /**
     * date format for date picker
     * @type {string}
     */
    @Input() dateFormat : string;

    /**
     * stores custom element to trigger opening of datepicker
     */
    @Input() dateTrigger : string;

    /**
     * minimum available date user can select
     */
    @Input() minDate : string;

    /**
     * maximum available date user can select
     */
    @Input() maxDate : string;

    /**
     * boolean indicator used to disable datepicker field
     */
    @Input() disabled : boolean = false;

    /**
     * event triggered when the date picker closes
     * @type {EventEmitter}
     */
    @Output() onClose : EventEmitter<undefined> = new EventEmitter();

    /**
     * event triggered when the date picker opens
     * @type {EventEmitter}
     */
    @Output() onOpen : EventEmitter<undefined> = new EventEmitter();

    /**
     * store current selected date here
     * @type {string}
     */
    private selectedDate : string = '';

    /**
     * variable to hold instance of pikaday date picker
     * @type {any}
     */
    private picker : any;

    /**
     * Keys to allow in date input.
     * Backspace(8), tab(9), arrows(37-40), delete(46), numbers(48-57, 96-105), forward slash(111, 191)
     */
    private constrainKeys : Array<number> = [8, 9, 37, 38, 39, 40, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 111, 191];

    /**
     * the method set in registerOnChange, it is just
     * a placeholder for a method that takes one parameter,
     * we use it to emit value changes back to the form
     *
     * @param _
     */
    private propagateChange = (_ : any) => { };

    /**
     * the method set in registerOnTouched
     * we use it to emit touched state back to the form
     */
    private propagateTouched : () => {};

    /**
     * THESE FOUR FUNCTIONS THAT FOLLOW ARE FUNCTIONS
     * THAT ARE IMPLEMENTED FROM THE ControlValueAccessor INTERFACE
     * THEY ARE USED INTERNALLY TO COMMUNICATE WITH EXTERNAL COMPONENTS
     */

    /**
     * allows US to update our internal model with incoming values,
     * for example if WE use ngModel to bind our control to data
     * this is the initial value set to the component
     * @param value
     */
    writeValue(value : string) {
        if (value !== undefined && value !== null && value !== this.selectedDate) {
            this.selectedDate = moment(value).format(this.dateFormat);

            // update pikaday
            if (this.picker) {
                this.picker.setDate(this.selectedDate);
            }

            // an initial value was supplied so go ahead and propagate the selected date upsteam
            // NOTE: it's possible to have the datepicker be pre-populated
            // with a valid date value, pass validation, but the control
            // still shows as invalid in the consuming Form.
            // Suspect the issue is related to the wrapping of pikaday
            this.propagateChange(this.selectedDate);

            if (this.propagateTouched) {
                this.propagateTouched();
            }
        }
        else {
            // always clear out the datepicker
            if (this.picker) {
                this.picker.setDate('');
            }
        }
    }

    /**
     * This function is called when the control status changes to or from "DISABLED".
     * Depending on the value, it will enable or disable the appropriate DOM element
     * @param isDisabled
     */
    setDisabledState(isDisabled : boolean) {
        this.renderer.setElementProperty(this.input.nativeElement, 'disabled', isDisabled);
    }

    /**
     * registers 'fn' that will be fired when changes are made
     * this is how we emit the changes back to the form
     *
     * @param fn
     */
    registerOnChange(fn : any) {
        this.propagateChange = fn;
    }

    /**
     * accepts a callback function which WE can call when WE want to set our control to touched.
     * This is then managed by Angular 2 FOR adding the correct touched state and classes to the actual element tag in the DOM
     *
     * @param fn
     */
    registerOnTouched(fn : any) {
        this.propagateTouched = fn;
    }

    /**
     * keydown check to make sure valid characters only can be entered
     * @param event - click event
     */
    keydownDateCheck(event : any) {
        if (this.constrainKeys.indexOf(event.keyCode) === -1) {
            event.preventDefault();
        }
    }

    /**
     * on blur check to make sure valid characters only can be entered
     * @param event - blur event
     */
    blurDateCheck(event : any) {
        // trigger callback so parent form updates datepicker's touched value
        if (this.propagateTouched) {
            this.propagateTouched();
        }

        this.selectionMade(event.target.value);
    }

    /**
     * click event handler that stores user's selection
     * @param date selected date object
     */
    selectionMade(date : any) {
        // format the date value
        const formattedDate = moment(date).format(this.dateFormat);

        // set selected value
        this.picker.setMoment(date, true);

        // check for Invalid date and propagate new value
        this.propagateChange(date === '' ? date : formattedDate);

        // trigger touched event
        if (this.propagateTouched) {
            this.propagateTouched();
        }
    }

    /**
     * component initialization lifecycle hook
     */
    ngAfterViewInit() {
        // init the pikaday datepicker control
        this.picker = new pikaday({
            field     : this.input.nativeElement,
            format    : this.dateFormat,
            trigger   : this.dateTrigger ? this.dateTrigger : undefined,
            minDate   : this.minDate ? moment(this.minDate).toDate() : undefined,
            maxDate   : this.maxDate ? moment(this.maxDate).toDate() : undefined,
            yearRange : [1900, moment().add('year', 1).year()],
            onClose   : () => this.onClose.emit(),
            onOpen    : () => this.onOpen.emit(),
            onSelect  : () => this.selectionMade(this.picker.getMoment())
        });

        // if initial selected date value was supplied, populate the picker with value
        if (this.selectedDate) {
            this.picker.setMoment(moment(this.selectedDate));
        }
    }
}
