import {
    Component,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {List} from 'immutable';

import {NavActions} from '../../../store/Navigation/nav.actions';
import {NavStateSelectors} from '../../../store/Navigation/nav.selectors';
import {
    EnumNavOption,
    NavOption
} from '../../../store/Navigation/types/nav-option.model';

@Component({
    selector         : 'left-navigation',
    templateUrl      : './left-navigation.component.html',
    styleUrls        : ['./left-navigation.component.scss'],
    changeDetection  : ChangeDetectionStrategy.OnPush
})

/**
 * implementation for LeftNavigationComponent: responsible for left menu navigation
 */
export class LeftNavigationComponent {
    /**
     *  LeftNavigationComponent constructor
     * @param cd
     * @param navActions
     * @param navSelectors
     */
    constructor(
        private cd                  : ChangeDetectorRef,
        private navActions          : NavActions,
        private navSelectors        : NavStateSelectors
    ) {
        // subscribe to aggregated Redux state changes globally that we're interested in
        this.stateSubscription = Observable.combineLatest(
            this.navSelectors.extendSideMenu(),
            this.navSelectors.activeNavState(),
            this.navSelectors.leftNavConfig()
        )
        .subscribe(val => {
            // update local state
            this.extendMenu     = val[0];
            this.activeNavState = val[1];
            this.navConfig      = val[2];

            // trigger change detection
            this.cd.markForCheck();
        });
    }

    /**
     * custom track by index function for ngFor directives
     * @param index
     * @param obj
     * @returns {number}
     */
    trackByIndex(index : number, obj : any) : any {
        return index;
    }

    /**
     * subscription to component's redux state
     */
    private stateSubscription : Subscription;

    /**
     * current snapshot of active nav state
     */
    activeNavState : EnumNavOption;

    /**
     * current snapshot of leftNavConfig
     */
    navConfig : List<NavOption>;

    /**
     * controls the visible state of the extended navigation view
     * @type {boolean}
     */
    extendMenu : boolean;

    /**
     * used to toggle left-navigation extender
     */
    toggleNavigation() {
        this.navActions.toggleSideNavigation(!this.extendMenu);
    }

    /**
     * updates app navigation state and handle router navigation step
     * @param navItem current nav state to navigate to
     */
    doMenuNavigation(navItem : NavOption) {
        this.navActions.doMenuNavigation(navItem, false);
    }

    /**
     * Component on destroy lifecycle hook
     */
    ngOnDestroy () {
        // unsubscribe
        this.stateSubscription.unsubscribe();
    }
}
