import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy
} from '@angular/core';

import {SearchResultsPaging} from '../../../store/Search/types/search-results-paging.model';

@Component({
    selector        : 'pagination-component',
    templateUrl     : './pagination.component.html',
    styleUrls       : ['./pagination.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for PaginationComponent: handles display of section panels
 */
export class PaginationComponent {
    /**
     * PaginationComponent constructor
     */
    constructor() {}

    /**
     * paging information from search results
     */
    @Input() paginationConfig : SearchResultsPaging;

    /**
     * custom event emitted when user increments page index
     * @type {EventEmitter<any>}
     */
    @Output() incrementPageIndex : EventEmitter<any> = new EventEmitter<any>();

    /**
     * custom event emitted when user decrements page index
     * @type {EventEmitter<any>}
     */
    @Output() decrementPageIndex : EventEmitter<any> = new EventEmitter<any>();

    /**
     * event handler for increment page index button click
     */
    emitIncrementPageIndex() {
        this.incrementPageIndex.emit();
    }

    /**
     * event handler for decrement page index button click
     */
    emitDecrementPageIndex() {
        this.decrementPageIndex.emit();
    }
}
