import {
    Component,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    selector        : 'beneficiary-notes',
    templateUrl     : 'beneficiary-notes.component.html',
    styleUrls       : ['beneficiary-notes.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for BeneficiaryNotesComponent: handles display of the Beneficiary Notes panel
 */
export class BeneficiaryNotesComponent {
    /**
     * BeneficiaryNotesComponent constructor
     */
    constructor() {}
}
