import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';

import {BeneficiaryPlansStateSummary} from '../../../store/Beneficiary/types/beneficiary-plans-state-summary.model';
import {SearchBeneficiaryPlanState} from '../../../store/Search/types/search-beneficiary-plan-state.model';

@Component({
    selector        : 'tooltip-component',
    templateUrl     : './tooltip.component.html',
    styleUrls       : ['./tooltip.component.scss'],
    changeDetection : ChangeDetectionStrategy.OnPush
})

/**
 * Implementation for TooltipComponent: handles display of this section panel
 */
export class TooltipComponent {
    /**
     * TooltipComponent constructor
     */
    constructor() {}

    /**
     * Object contains tooltip information
     * @type {Object}
     */
    @Input() plan : BeneficiaryPlansStateSummary | SearchBeneficiaryPlanState;

    /**
     * boolean toggle used to show/hide tooltip
     * @type {boolean}
     */
    showTooltip : boolean = false;

    /**
     * string placeholder uysed for date ranges
     * @type {string}
     */
    dateRanges : string = '';

    /**
     * method used to toggle toolip visibility
     */
    toggleTooltip() {
        this.showTooltip = !this.showTooltip;
    }

    /**
     * component init lifecycle hook
     */
    ngOnInit() {
        this.dateRanges = this.plan.get('fromDate') && this.plan.get('thruDate') ? this.plan.get('fromDate') + ' - ' + this.plan.get('thruDate') :
                this.plan.get('fromDate') ? this.plan.get('fromDate') :
                        this.plan.get('thruDate') ? this.plan.get('thruDate') : '';
    }
}
