import {Component, OnInit} from '@angular/core';
import {UserActions} from '../../../store/User/user.actions';
import {SessionActions} from '../../../store/AppConfig/session.actions';

@Component({
               templateUrl: './logout.component.html'
           })
export class LogoutComponent implements OnInit {
    constructor(private userActions     : UserActions,
                private sessionActions  : SessionActions
    ) {
    }

    ngOnInit() : void {
        console.debug('logging user out via LogoutComponent.init()');
        this.userActions.userLogout();
        this.sessionActions.end();
        window.location.pathname = '/logout';
    }
}
