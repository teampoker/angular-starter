import {CalculateAgeAtDeathPipe} from './calculate-age-at-death.pipe';

/**
 * These unit tests attempt to test only the pipe functionality while avoiding duplication of the 100k+ tests that
 * Moment.js performs.
 */
describe('Pipe: CalculateAgeAtDeathPipe', () => {
    let pipe                        : CalculateAgeAtDeathPipe,
        testDate                    : string,
        expectedDifferenceInYears   : number;

    beforeEach(() => {
        pipe                        = new CalculateAgeAtDeathPipe();
        expectedDifferenceInYears   = 0;
        testDate                    = '';
    });

    it('should return 0 for anything it doesn\'t understand', () => {
        expect(pipe.transform('', '')).toEqual(0);                                              // empty string
        expect(pipe.transform('  ', '  ')).toEqual(0);                                          // whitespace
        expect(pipe.transform('1483547883', '34563456')).toEqual(0);                            // timestamp in ticks since epoch
        expect(pipe.transform('not a real date', 'definitely not a real date')).toEqual(0);     // unparseable date string
    });
});
