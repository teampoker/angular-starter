import {
    Pipe,
    PipeTransform
} from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name : 'calculateAgeAtDeath'
})

/**
 * Calculates final age of individual based on provided birthDate and deathDate
 */
export class CalculateAgeAtDeathPipe implements PipeTransform {

    /**
     * Transforms a formatted date value into the number of years between birthDate and deathDate.
     *
     * @param deathDate individual's date of death
     * @param birthDate individual's date of birth
     *
     * @returns {number} The number of whole years between the birthDate and deathDate.
     */
    transform(deathDate : string, birthDate : string) : number {
        // if we can parse the parameter, then try to calculate the difference in years
        if (moment(deathDate).isValid() && moment(birthDate).isValid()) {
            return moment(deathDate).diff(moment(birthDate), 'years', false);
        }
        else {
            // couldn't parse the parameter, so we will always return zero
            return 0;
        }
    }
}
