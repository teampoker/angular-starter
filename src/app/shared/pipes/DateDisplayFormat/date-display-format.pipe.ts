import {
    Pipe,
    PipeTransform
} from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name : 'dateDisplayFormat'
})

/**
 * implementation of DateDisplayFormatPipe: format birth date values for display in UI
 */
export class DateDisplayFormatPipe implements PipeTransform {
    transform(value : string, args? : any, format? : any) : string {
        // is valid?
        if (moment(value).isValid()) {
            if (args) {
                // TODO: replace with angular2localization implementation later
                return moment(value).locale('en-US').format(args);
            }
            else {
                return moment(value).locale('en-US').format('MM/DD/YYYY');
            }
        }
        else if (moment(value, format).isValid()) {
            return moment(value, format).locale('en-US').format(args);
        }
        else {
            return 'N/A';
        }
    }
}
