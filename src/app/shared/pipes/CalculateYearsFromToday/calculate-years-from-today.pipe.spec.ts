import * as moment from 'moment';

import {CalculateYearsFromTodayPipe} from './calculate-years-from-today.pipe';

/**
 * These unit tests attempt to test only the pipe functionality while avoiding duplication of the 100k+ tests that
 * Moment.js performs.
 */
describe('Pipe: CalculateYearsFromToday', () => {
    let pipe : CalculateYearsFromTodayPipe,
        testDate    : string,
        expectedDifferenceInYears   : number;

    beforeEach(() => {
        pipe = new CalculateYearsFromTodayPipe();
        expectedDifferenceInYears = 0;
        testDate = '';
    });

    it('should return 0 for anything it doesn\'t understand', () => {
        expect(pipe.transform('')).toEqual(0);                  // empty string
        expect(pipe.transform('  ')).toEqual(0);                // whitespace
        expect(pipe.transform('1483547883')).toEqual(0);        // timestamp in ticks since epoch
        expect(pipe.transform('not a real date')).toEqual(0);   // unparseable date string
    });

    it('should handle dates in the past', () => {
        testDate = '1995-12-31';
        expectedDifferenceInYears = moment().diff(moment(testDate), 'years', false);
        expect(pipe.transform(testDate)).toEqual(expectedDifferenceInYears);
        expect(expectedDifferenceInYears).not.toContain('-');
    });

    it('should handle dates in the future', () => {
        testDate = '2511-05-01T04:56:43.932';
        expectedDifferenceInYears = moment().diff(moment(testDate), 'years', false);
        expect(pipe.transform(testDate)).toEqual(expectedDifferenceInYears);
    });

    it('should handle different date formats', () => {
        testDate = '1957-04-14';
        expectedDifferenceInYears = moment().diff(moment(testDate), 'years', false);
        expect(pipe.transform(testDate)).toEqual(expectedDifferenceInYears);

        testDate = '04/14/1957';
        // should be the same difference as above, just changed date format
        expect(pipe.transform(testDate)).toEqual(expectedDifferenceInYears);
    });
});
