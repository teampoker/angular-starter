import {
    Pipe,
    PipeTransform
} from '@angular/core';
import * as moment from 'moment';

@Pipe({
      name : 'calculateYearsFromToday'
  })

/**
 * Calculates years between a given date and the current system date.
 *
 * Notes:
 * <ul>
 *     <li>
 *         Uses the Moment.js library for formatting and handling dates.
 *     </li>
 *     <li>
 *         Calculates the difference in years using the Moment.js algorithm, which the interesting thing is that
 *         it doesn't just take the difference between the year. It considers a year to be 365 days, so this approach
 *         is good for things like comparing dates and calculating age.
 *     </li>
 * </ul>
 *
 * @see <a href="https://github.com/moment/moment/pull/571">GitHub discussion about calculating dates</a>
 */
export class CalculateYearsFromTodayPipe implements PipeTransform {

    /**
     * Transforms a formatted date value into the number of years between that date and today.
     *
     * @param date A string value of a formatted date.
     * @returns {number} The number of whole years between the system date and the parsed date.
     */
    transform(date : string) : number {
        // if we can parse the parameter, then try to calculate the difference in years
        if (moment(date).isValid()) {
            return moment().diff(moment(date), 'years', false);
        }
        else {
            // couldn't parse the parameter, so we will always return zero
            return 0;
        }
    }
}
