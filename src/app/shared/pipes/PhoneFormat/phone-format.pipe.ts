import {
    Pipe,
    PipeTransform
} from '@angular/core';

@Pipe({
    name : 'phoneFormat'
})

/**
 * implementation of PhoneFormatPipe: format phone numbers in format '5555555555' to '(555)555-5555'
 */
export class PhoneFormatPipe implements PipeTransform {
    transform(value : string) : string {

        // is defined?
        if (value) {
            if (value === '') {
                return 'N/A';
            }

            if (value.indexOf('(') === -1 && value[3] !== '-') {
                return '(' + value.substr(0, 3) + ') ' + value.substr(3, 3) + '-' + value.substr(6, 4);
            }
            else if (value.indexOf('(') !== -1) {
                return value;
            }
            else if (value[3] === '-' && value[7] === '-') {
                return '(' + value.substr(0, 3) + ') ' + value.substr(4);
            }
        }
        else {
            return 'N/A';
        }
    }
}
