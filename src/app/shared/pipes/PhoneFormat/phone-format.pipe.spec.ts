import {PhoneFormatPipe} from './phone-format.pipe';

describe('Pipe: PhoneFormat', () => {
    let pipe : PhoneFormatPipe;

    beforeEach(() => {
        pipe = new PhoneFormatPipe();
    });

    it('should return N/A for invalid inputs', () => {
        const INVALID_VALUE_OUTCOME = 'N/A';
        expect(pipe.transform('')).toEqual(INVALID_VALUE_OUTCOME); // empty strings
        expect(pipe.transform(undefined)).toEqual(INVALID_VALUE_OUTCOME); // undefined
        expect(pipe.transform(null)).toEqual(INVALID_VALUE_OUTCOME); // null
    });

    it('should format numeric values into readable phone numbers', () => {
        // format phone numbers in format '5555555555' to '(555)555-5555'
        expect(pipe.transform('1234567890')).toEqual('(123) 456-7890');
        expect(pipe.transform('1234567')).toEqual('(123) 456-7'); // string is too short
        expect(pipe.transform('123456')).toEqual('(123) 456-'); // string is too short
        expect(pipe.transform('1')).toEqual('(1) -'); // string is too short
    });

    it('should format whitespace values into phone template', () => {
        const BLANK_TEMPLATE_OUTCOME = '(  ) -';
        expect(pipe.transform('  ')).toEqual(BLANK_TEMPLATE_OUTCOME);
        expect(pipe.transform('  .      ')).toEqual('(  .)    -   ');
    });

    it('should leave values alone', () => {
        let testInputValue : string = '(1234567890';
        expect(pipe.transform(testInputValue)).toEqual(testInputValue);
        testInputValue = '(123)456-7890';
        expect(pipe.transform(testInputValue)).toEqual(testInputValue);
        testInputValue = '(123) 456-7890';
        expect(pipe.transform(testInputValue)).toEqual(testInputValue);
    });

    it('should truncate anything past 10 characters', () => {
        expect(pipe.transform('12345678901')).toEqual('(123) 456-7890');
    });

});
