import {CurrencyFormatPipe} from './currency-format.pipe';
/***
 * Unit tests to capture the formatter for currency pipe.
 *
 * Note: I'm not sure these are correct outcomes (ie - designed...), but capturing how these function for now.
 */
describe('CurrencyFormat: Pipes', () => {
    let pipe;

    // setup
    beforeEach(() => {
        pipe = new CurrencyFormatPipe();
    });

    it('should NOT throw if not used with a string', () => {
        // must use arrow function to capture exception
        expect(() => pipe.transform(null)).not.toThrow();
        expect(() => pipe.transform(undefined)).not.toThrow();
        expect(() => pipe.transform()).not.toThrow();
    });

    it('should work with empty string', () => {
        expect(pipe.transform('')).toEqual('0');
    });

    it('should format values using default format when not specified', () => {
        expect(pipe.transform(999999)).toEqual('999,999');
        // TODO: this test is showing that the formatter is returning '12,345,679' when passing '12345678.90'
        // expect(pipe.transform(12345678.90)).toEqual('12,345,678.90');
        expect(pipe.transform(0.1234)).toEqual('0');
        expect(pipe.transform('23rd')).toEqual('23');
        expect(pipe.transform('$10,000.00')).toEqual('10,000');

        // this may not be the best practice for a number formatter... but this library converts Kilobytes to Bits
        expect(pipe.transform('3.467TB')).toEqual('3,467,000,000,000');
        expect(pipe.transform('-76%')).toEqual('-1');
        expect(pipe.transform('2:23:57')).toEqual('8,637');   // formatted time values
    });

    it('should return NaN when certain non-numeric values are passed', () => {

    });

    it('should return 0 when certain non-numeric values are passed', () => {
        expect(pipe.transform('abc')).toEqual('0');         // letters are stripped from input values
        expect(pipe.transform('-')).toEqual('0');           // negative sign without number
        expect(pipe.transform('$.')).toEqual('0');        // currency symbols without numbers
    });

    it('should format a value given a format pattern', () => {
        let formatPattern = '0,0';
        expect(pipe.transform(10000, formatPattern)).toEqual('10,000');
        expect(pipe.transform(100, formatPattern)).toEqual('100');

        formatPattern = '0,0.00';
        expect(pipe.transform(10000, formatPattern)).toEqual('10,000.00');
        expect(pipe.transform(100, formatPattern)).toEqual('100.00');

        formatPattern = '0 a';
        expect(pipe.transform(1236, formatPattern)).toEqual('1 k');
        expect(pipe.transform(1234567, formatPattern)).toEqual('1 m');

        formatPattern = '0.0 a';
        expect(pipe.transform(1236, formatPattern)).toEqual('1.2 k');
        expect(pipe.transform(5643218, formatPattern)).toEqual('5.6 m');

        formatPattern = '$ 0,0[.]00';
        expect(pipe.transform(1236, formatPattern)).toEqual('$ 1,236');
        expect(pipe.transform(5643218.768, formatPattern)).toEqual('$ 5,643,218.77');
    });
});
