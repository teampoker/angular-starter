import * as moment from 'moment';
import {DateTimeFormatPipe} from './date-time-format.pipe';

describe('Pipe: DateTimeFormat', () => {
    let pipe : DateTimeFormatPipe;

    beforeEach(() => {
        pipe = new DateTimeFormatPipe();
    });

    xit('should return greetings, but can\'t tell for sure because it relies on system time', () => {});

    it('should format dates and times using the moment framework', () => {
        const date : any = '1/1/2015', // <-- date when this whole project started
              args : any = '';

        expect(pipe.transform(date, args)).toEqual(moment(date).format(args));
    });
});
