import {Pipe, PipeTransform} from '@angular/core';

import * as moment from 'moment';

@Pipe({
    name: 'dateTimeFormat'
})
export class DateTimeFormatPipe implements PipeTransform {
    transform(value? : any, args? : any, format? : any) : any {
        // is valid?
        if (moment(value).isValid()) {
            if (args === 'greeting') {
                let greeting;

                const SPLIT_AFTERNOON   = 12, // 24hr time to split the afternoon
                      SPLIT_EVENING     = 17, // 24hr time to split the evening
                      currentHour       = parseFloat(moment().format('HH'));

                if (currentHour >= SPLIT_AFTERNOON && currentHour <= SPLIT_EVENING) {
                    greeting = 'afternoon';
                }
                else if (currentHour >= SPLIT_EVENING) {
                    greeting = 'evening';
                }
                else {
                    greeting = 'morning';
                }

                return greeting;
            }
            else {
                return moment(value).locale('en-US').format(args);
            }
        }
        else if (moment(value, format).isValid()) {
            return moment(value, format).locale('en-US').format(args);
        }
        else {
            return 'N/A';
        }
    }
}
