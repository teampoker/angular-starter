/*!
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 rajan-g
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import {
    Directive,
    ElementRef,
    EventEmitter,
    Output
} from '@angular/core';
import {NgModel} from '@angular/forms';
import * as GoogleMapsLoader from 'google-maps';

@Directive({
    selector    : '[googlePlaces]',
    providers   : [NgModel]
})

export class GooglePlacesDirective {
    constructor(el : ElementRef, private model : NgModel) {
        this.el = el.nativeElement;
        this.modelValue = this.model;

        // set Google Maps libraries that we need
        (GoogleMapsLoader as any).LIBRARIES = ['places'];

        if (GOOGLE_KEY_ENABLED) {
            (GoogleMapsLoader as any).KEY = GOOGLE_KEY;
        }

        // make sure Google Maps library is loaded before using
        GoogleMapsLoader.load(google => {
            this.autocomplete = new google.maps.places.Autocomplete(this.el, {
                componentRestrictions : {
                    country : 'us'
                }
            });

            google.maps.event.addListener(this.autocomplete, 'place_changed', () => {
                const place = this.autocomplete.getPlace();

                this.invokeEvent(place);
            });
        });
    }

    /**
     * listener for address selections
     * @type {EventEmitter}
     */
    @Output() setAddress : EventEmitter<any> = new EventEmitter();

    /**
     * native element reference
     */
    private el : HTMLInputElement;

    /**
     * Google Places autocomplete object
     */
    autocomplete : any;

    /**
     * Reference to NgModel passed
     */
    modelValue : any;

    /**
     * Emits place changes to output listener
     * @param place
     */
    invokeEvent(place : any) {
        this.setAddress.emit(place);
    }
}
