import {
    ComponentFactoryResolver,
    ComponentRef,
    Directive,
    EventEmitter,
    HostListener,
    Input,
    OnChanges,
    Output,
    SimpleChange,
    ViewContainerRef
} from '@angular/core';

import {PopoverContentComponent} from '../../components/PopoverContent/popover-content.component';

@Directive({
    selector: '[popover]',
    exportAs: 'popover'
})
export class PopoverDirective implements OnChanges {
    constructor(
        private viewContainerRef : ViewContainerRef,
        private resolver : ComponentFactoryResolver
    ) {}

    /* tslint:disable-next-line:no-input-rename */
    @Input('popover') content : string | PopoverContentComponent;

    @Input() popoverAnimation : boolean;

    @Input() popoverCloseOnClickOutside : boolean;

    @Input() popoverCloseOnMouseOutside : boolean;

    @Input() popoverDisabled : boolean;

    @Input() popoverDismissTimeout : number = 0;

    @Input() popoverOnHover : boolean = false;

    @Input() popoverPlacement :
                 'top'
                 | 'bottom'
                 | 'left'
                 | 'right'
                 | 'auto'
                 | 'auto top'
                 | 'auto bottom'
                 | 'auto left'
                 | 'auto right';

    @Input() popoverTitle : string;

    @Output() onHidden : EventEmitter<PopoverDirective> = new EventEmitter<PopoverDirective>();

    @Output() onShown : EventEmitter<PopoverDirective> = new EventEmitter<PopoverDirective>();

    @HostListener('click')
    showOrHideOnClick() {
        if (this.popoverOnHover) {
            return;
        }
        if (this.popoverDisabled) {
            return;
        }
        this.toggle();
    }

    @HostListener('focusin')
    @HostListener('mouseenter')
    showOnHover() {
        if (!this.popoverOnHover) {
            return;
        }
        if (this.popoverDisabled) {
            return;
        }
        this.show();
    }

    @HostListener('focusout')
    @HostListener('mouseleave')
    hideOnHover() {
        if (this.popoverCloseOnMouseOutside || !this.popoverOnHover || this.popoverDisabled) {
            return;
        } // don't do anything since not we control this

        this.hide();
    }

    private PopoverComponent : any = PopoverContentComponent;
    private popover : ComponentRef<PopoverContentComponent>;
    private visible : boolean;

    getElement() {
        return this.viewContainerRef.element.nativeElement;
    }

    hide() {
        if (!this.visible) {
            return;
        }

        this.visible = false;
        if (this.popover) {
            this.popover.destroy();
        }

        if (this.content instanceof PopoverContentComponent) {
            (this.content as PopoverContentComponent).hideFromPopover();
        }

        this.onHidden.emit(this);
    }

    show() {
        if (this.visible) {
            return;
        }

        this.visible = true;

        if (typeof this.content === 'string') {
            const factory = this.resolver.resolveComponentFactory(this.PopoverComponent);
            if (!this.visible) {
                return;
            }

            this.popover    = this.viewContainerRef.createComponent(factory) as ComponentRef<PopoverContentComponent>;
            const popover   = this.popover.instance as PopoverContentComponent;
            popover.popover = this;
            popover.content = this.content as string;
            if (this.popoverPlacement !== undefined) {
                popover.placement = this.popoverPlacement;
            }
            if (this.popoverAnimation !== undefined) {
                popover.animation = this.popoverAnimation;
            }
            if (this.popoverTitle !== undefined) {
                popover.title = this.popoverTitle;
            }
            if (this.popoverCloseOnClickOutside !== undefined) {
                popover.closeOnClickOutside = this.popoverCloseOnClickOutside;
            }
            if (this.popoverCloseOnMouseOutside !== undefined) {
                popover.closeOnMouseOutside = this.popoverCloseOnMouseOutside;
            }

            popover.onCloseFromOutside.subscribe(() => this.hide());
            // if dismissTimeout option is set, then this popover will be dismissed in dismissTimeout time
            if (this.popoverDismissTimeout > 0) {
                setTimeout(() => this.hide(), this.popoverDismissTimeout);
            }
        }
        else {
            const popover   = this.content as PopoverContentComponent;
            popover.popover = this;
            if (this.popoverPlacement !== undefined) {
                popover.placement = this.popoverPlacement;
            }
            if (this.popoverAnimation !== undefined) {
                popover.animation = this.popoverAnimation;
            }
            if (this.popoverTitle !== undefined) {
                popover.title = this.popoverTitle;
            }
            if (this.popoverCloseOnClickOutside !== undefined) {
                popover.closeOnClickOutside = this.popoverCloseOnClickOutside;
            }
            if (this.popoverCloseOnMouseOutside !== undefined) {
                popover.closeOnMouseOutside = this.popoverCloseOnMouseOutside;
            }

            popover.onCloseFromOutside.subscribe(() => this.hide());
            // if dismissTimeout option is set, then this popover will be dismissed in dismissTimeout time
            if (this.popoverDismissTimeout > 0) {
                setTimeout(() => this.hide(), this.popoverDismissTimeout);
            }
            popover.show();
        }

        this.onShown.emit(this);
    }

    toggle() {
        this.visible ? this.hide() : this.show();
    }

    ngOnChanges(changes : { [propertyName : string] : SimpleChange }) {
        if (changes['popoverDisabled']) {
            if (changes['popoverDisabled'].currentValue) {
                this.hide();
            }
        }
    }
}
