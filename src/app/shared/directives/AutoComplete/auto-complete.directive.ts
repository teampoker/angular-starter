/**
 * Copyright (c) 2016 Allen Kim <allenhwkim@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {
    ComponentFactoryResolver,
    ComponentRef,
    Directive,
    EventEmitter,
    Host,
    HostListener,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Optional,
    Output,
    SimpleChanges,
    SkipSelf,
    ViewContainerRef
} from '@angular/core';
import {
    AbstractControl,
    ControlContainer,
    FormControl,
    FormGroup
} from '@angular/forms';
import {TranslateService} from 'ng2-translate';
import {
    Iterable,
    List
} from 'immutable';

import {AutoCompleteComponent} from '../../components/AutoComplete/auto-complete.component';

/**
 * display auto-complete section with input and dropdown list when it is clicked
 */
@Directive({
    selector: '[autoComplete]'
})
export class AutoCompleteDirective implements OnChanges, OnDestroy, OnInit {
    constructor(
        private translate                                   : TranslateService,
        private resolver                                    : ComponentFactoryResolver,
        public  viewContainerRef                            : ViewContainerRef,
        @Optional() @Host() @SkipSelf() private parentForm  : ControlContainer
    ) {
        this.el = this.viewContainerRef.element.nativeElement;
    }

    @HostListener('focus') onFocus() {
        this.showAutoCompleteDropdown();
    }

    @HostListener('blur') onBlur() {
        this.hideAutoCompleteDropdown();
    }

    @HostListener('keydown', ['$event']) onKeyDown($event : KeyboardEvent) {
        this.keydownEventHandler($event);
    }

    @HostListener('input', ['$event']) onInput($event : any) {
        this.inputEventHandler($event);
    }

    /**
     * reservation reset form indicator
     */
    @Input() resetForm : boolean = false;

    @Input() acceptUserInput : boolean;
    @Input() autoCompletePlaceholder : string;
    @Input() blankOptionText : string;
    @Input() disableFilter : boolean;
    @Input() displayFormatter : (arg : any) => string;
    @Input() displayPropertyName : string;
    // If [formControl] is used on the anchor where our directive is sitting
    // a form is not necessary to use a formControl we should also support this
    @Input() extFormControl : FormControl;
    @Input() formControlName : string;
    @Input() listFormatter : (arg : any) => string;
    @Input() loadingText : string = 'Loading';
    @Input() maxNumList : string;
    @Input() minChars : number = 0;
    @Input() ngModel : string;
    @Input() noMatchFoundText : string;
    @Input() pathToData : string;
    @Input() source : Array<any> | List<any> | string;
    @Input() valuePropertyName : string;

    @Output() ngModelChange : EventEmitter<any>  = new EventEmitter();
    @Output() valueChanged : EventEmitter<any>  = new EventEmitter();

    private bindAttributes(component : any) {
        component.disableFilter       = this.disableFilter;
        component.listFormatter       = this.listFormatter;
        component.pathToData          = this.pathToData;
        component.minChars            = this.minChars;
        component.valuePropertyName   = this.valuePropertyName || 'id';
        component.displayPropertyName = this.displayPropertyName || 'value';
        component.source              = this.source;
        component.blankOptionText     = this.blankOptionText;
        component.noMatchFoundText    = this.noMatchFoundText;
        component.acceptUserInput     = this.acceptUserInput;
        component.loadingText         = this.loadingText;
        component.maxNumList          = parseInt(this.maxNumList, 10);
    }

    private elementIn(el : Node, containerEl : Node) : boolean {
        // tslint:disable-next-line: no-conditional-assignment
        while (el = el.parentNode) {
            if (el === containerEl) {
                return true;
            }
        }
        return false;
    }

    private inputEventHandler(event : any) {
        if (this.componentRef) {
            const component           = this.componentRef.instance as AutoCompleteComponent;
            component.dropdownVisible = true;
            component.reloadListInDelay(event);
        }
        else {
            this.showAutoCompleteDropdown();
        }
    }

    private keydownEventHandler(event : KeyboardEvent) {
        if (this.componentRef) {
            const component = this.componentRef.instance as AutoCompleteComponent;
            component.inputElKeyHandler(event);
        }
    }

    private moveAutocompleteDropDownAfterInputEl() {
        this.inputEl = this.el as HTMLInputElement;
        if (this.el.tagName !== 'INPUT' && this.acDropdownEl) {
            this.inputEl = this.el.querySelector('input') as HTMLInputElement;
            this.inputEl.parentElement.insertBefore(this.acDropdownEl, this.inputEl.nextSibling);
        }
    }

    acDropdownEl : HTMLElement; // auto complete element
    componentRef : ComponentRef<AutoCompleteComponent>;
    el : HTMLElement;   // this element element, can be any
    formControl : AbstractControl;
    inputEl : HTMLInputElement;  // input element of this element

    addToStringFunction(val : any) : any {
        if (Iterable.isIterable(val) && val.get(this.displayPropertyName) === 'PLUS_ADD_A_CONNECTION') {
            const displayVal = '';
            val.toString = () => displayVal;
        }
        else if (val && typeof val === 'object') {
            let displayVal = '';

            displayVal = this.displayFormatter ?
                this.displayFormatter(val) :
                val[this.displayPropertyName || 'value'];

            val.toString = () => displayVal;
        }

        return val;
    }

    componentInputChanged(val : string) {
        if (this.acceptUserInput) {
            this.inputEl.value = val;
            if ((this.parentForm && this.formControlName) || this.extFormControl) {
                this.formControl.patchValue(val);
            }
            if (this.ngModel) {
                this.ngModelChange.emit(val);
            }
            this.valueChanged.emit(val);
        }
    }

    hideAutoCompleteDropdown(event? : any) {
        if (this.componentRef) {
            // Document level click to hide dropdown
            if (
                event && event.type === 'click' &&
                event.target.tagName !== 'INPUT' && !this.elementIn(event.target, this.acDropdownEl)
            ) {
                this.componentRef.destroy();
                this.componentRef = undefined;
            }
            else if (!event) {
                this.componentRef.destroy();
                this.componentRef = undefined;
            }
        }
    }

    selectNewValue(val : any) {
        if (val !== '') {
            val = this.addToStringFunction(val);
        }
        if ((this.parentForm && this.formControlName) || this.extFormControl) {
            if (!!val) {
                this.formControl.patchValue(val);
            }
        }
        if (val !== this.ngModel) {
            this.ngModelChange.emit(val);
        }

        this.valueChanged.emit(val);

        if (this.inputEl) {
            this.inputEl.value = '' + val;
        }

        this.hideAutoCompleteDropdown();
    }

    // Show auto-complete list below the current element
    showAutoCompleteDropdown() {
        const factory = this.resolver.resolveComponentFactory(AutoCompleteComponent);

        this.componentRef = this.viewContainerRef.createComponent(factory);

        const component        = this.componentRef.instance;
        // Do NOT display autocomplete input tag separately
        component.showInputTag = false;

        this.bindAttributes(component);

        component.valueSelected.subscribe(val => this.selectNewValue(val));
        component.inputChanged.subscribe(val => this.componentInputChanged(val));

        this.acDropdownEl               = this.componentRef.location.nativeElement;
        this.acDropdownEl.style.display = 'none';

        // If this element is not an input tag, move dropdown after input tag
        // so that it displays correctly
        this.moveAutocompleteDropDownAfterInputEl();

        setTimeout(() => {
            component.reloadList(this.inputEl.value);
            this.styleAutoCompleteDropdown();
            component.dropdownVisible = true;
        });
    }

    styleAutoCompleteDropdown() {
        if (this.componentRef) {
            // Setting width/height auto complete
            const thisElBCR      = this.el.getBoundingClientRect();
            const thisInputElBCR = this.inputEl.getBoundingClientRect();
            const closeToBottom  = thisInputElBCR.bottom + 100 > window.innerHeight;

            this.acDropdownEl.style.width    = thisElBCR.width + 'px';
            this.acDropdownEl.style.position = 'absolute';
            this.acDropdownEl.style.zIndex   = '1';
            this.acDropdownEl.style.left     = '0';
            this.acDropdownEl.style.display  = 'inline-block';

            if (closeToBottom) {
                this.acDropdownEl.style.bottom = `${thisInputElBCR.height}px`;
            }
            else {
                this.acDropdownEl.style.top = `${thisInputElBCR.height}px`;
            }
        }
    }

    /**
     * Lifecycle hook that is called when any data-bound property of a directive changes
     */
    ngOnChanges(changes : SimpleChanges) {
        if (changes['ngModel']) {
            this.ngModel = this.addToStringFunction(changes['ngModel'].currentValue);
        }

        if (this.componentRef && this.componentRef.instance) {
            this.bindAttributes(this.componentRef.instance);
        }

        // reset form
        if (this.resetForm) {
            this.selectNewValue('');
        }
    }

    ngOnInit() {
        // Wrap this element with <div class='ng2-auto-complete'>
        const divEl          = document.createElement('div');
        divEl.className      = 'ng2-auto-complete-wrapper';
        divEl.style.position = 'relative';
        this.el.parentElement.insertBefore(divEl, this.el.nextSibling);
        divEl.appendChild(this.el);

        // if initial value is available, pass it to translate service so placeholder text is automagically displayed as translated value
        if (this.ngModel !== undefined && this.ngModel !== '') {
            this.translate.get(this.ngModel.toString()).subscribe(res => this.ngModel = res).unsubscribe();
        }

        // Check if we were supplied with a [formControlName] and it is inside a [form]
        // else check if we are supplied with a [FormControl] regardless if it is inside a [form] tag
        if (this.parentForm && this.formControlName) {
            if (this.parentForm['form']) {
                this.formControl = (this.parentForm['form'] as FormGroup).get(this.formControlName);
            }
        }
        else if (this.extFormControl) {
            this.formControl = this.extFormControl;
        }

        // Apply toString() method for the object
        this.selectNewValue(this.ngModel);

        // when somewhere else clicked, hide this autocomplete
        document.addEventListener('click', this.hideAutoCompleteDropdown);
    }

    ngOnDestroy() {
        if (this.componentRef) {
            this.componentRef.instance.valueSelected.unsubscribe();
            this.componentRef.instance.inputChanged.unsubscribe();
        }
        document.removeEventListener('click', this.hideAutoCompleteDropdown);
    }
}
