export const SEARCH_MOCK : any = {

    getSearchTypes : [
        {
            name : 'Beneficiary'
        },
        {
            name : 'Reservation'
        },
        {
            name : 'Person'
        }
    ],
    getSearchTypesWrap : {
        name : 'Beneficiary'
    },

    getHeaderSearchResults : [{
        searchString : 'stark',
        timeOut : false,
        timeInMillis : 4,
        maxScore : 4.875618,
        results : [{
            score : 4.875618,
            uuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
            firstName : 'Arya',
            lastName : 'Stark',
            middleName : 'middle',
            birthDate : '1997-01-12',
            contactMechanisms : [{
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {uuid : '4434867d-7500-476e-b75f-545ad26351e8', type : {uuid : '7a15def7-a536-11e6-9155-125e1c46ef60', name : 'Electronic Address'}, electronicAddressString : 'arya.stark.body@angular-starter.comz'},
                contactMechanismPurposeType : {},
                ordinality : '0'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {
                    uuid : '38e25172-507c-4136-ba93-f4b10d7a0fef',
                    type : {uuid : '81dbe990-a536-11e6-9155-125e1c46ef60', name : 'Telecommunications Number'},
                    contactNumber : '2229968',
                    countryCode : '1',
                    areaCode : '755'
                },
                contactMechanismPurposeType : {},
                ordinality : '0'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {uuid : 'e1717d62-54d2-43b0-8b00-a323310bd7e2', type : {uuid : '7a15def7-a536-11e6-9155-125e1c46ef60', name : 'Electronic Address'}, electronicAddressString : 'todd.crone@angular-starter.com'},
                contactMechanismPurposeType : {},
                ordinality : '1'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {uuid : '9b8bc7a3-de62-4525-adb3-2cea9252d353', type : {uuid : '7a15def7-a536-11e6-9155-125e1c46ef60', name : 'Electronic Address'}, electronicAddressString : 'some.person1@gmail.com'},
                contactMechanismPurposeType : {},
                ordinality : '2'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {
                    uuid : 'c10c5fbe-d779-4203-81b2-03b6f1d49ad1',
                    type : {uuid : '6a73618e-a536-11e6-9155-125e1c46ef60', name : 'Address'},
                    address1 : '123 Todd Crone Avenue',
                    city : '',
                    state : '',
                    postalCode : ''
                },
                contactMechanismPurposeType : {},
                ordinality : '1'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {uuid : '88a7c4a9-6f88-4a76-a5a8-07d7ef268daa', type : {uuid : '7a15def7-a536-11e6-9155-125e1c46ef60', name : 'Electronic Address'}, electronicAddressString : 'madhu@angular-starter.com'},
                contactMechanismPurposeType : {},
                ordinality : '0'
            }],
            serviceCoverages : [{
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                fromDate : '2017-02-01',
                ordinality : 0,
                serviceCoveragePlan : {uuid : 'cc2042b9-b687-11e6-9155-125e1c46ef60', serviceCoverageOrganization : {uuid : 'ba25622b-b687-11e6-9155-125e1c46ef60', name : 'RI EOHHS'}, name : 'Medicaid'},
                classifications : [{
                    value : '2342344234',
                    serviceCoveragePlanClassification : {
                        uuid : '3235aff4-b6a8-11e6-9155-125e1c46ef60',
                        name : 'Beneficiary ID',
                        serviceCoveragePlanClassificationType : {uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60', name : 'Id'}
                    }
                }, {
                    value : 'CNOM',
                    serviceCoveragePlanClassification : {
                        uuid : '3d266eef-b6a8-11e6-9155-125e1c46ef60',
                        name : 'Program',
                        serviceCoveragePlanClassificationType : {uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60', name : 'Benefit'}
                    }
                }]
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                fromDate : '2017-02-01',
                thruDate : '2017-02-25',
                ordinality : 0,
                serviceCoveragePlan : {
                    uuid : 'd70b3f01-b687-11e6-9155-125e1c46ef60',
                    serviceCoverageOrganization : {uuid : 'ba25622b-b687-11e6-9155-125e1c46ef60', name : 'RI EOHHS'},
                    name : 'Temporary Assistance for Needy Families',
                    identifier : 'TANF'
                },
                classifications : []
            }]
        }, {
            score : 4.875618,
            uuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
            firstName : 'Sansa',
            lastName : 'Stark',
            middleName : null,
            birthDate : '1996-02-21',
            contactMechanisms : [{
                personUuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
                contactMechanism : {
                    uuid : '3fdb1cdf-0b2b-4513-a682-b0cadcac2ffd',
                    type : {uuid : '6a73618e-a536-11e6-9155-125e1c46ef60', name : 'Address'},
                    address1 : '123 American Ave',
                    city : 'Jacksonville',
                    state : 'KY',
                    postalCode : '50505'
                },
                contactMechanismPurposeType : {},
                ordinality : '0'
            }, {
                personUuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
                contactMechanism : {
                    uuid : 'a9a1556e-9758-4484-9ca8-43d668736437',
                    type : {uuid : '81dbe990-a536-11e6-9155-125e1c46ef60', name : 'Telecommunications Number'},
                    contactNumber : '5555555',
                    countryCode : '1',
                    areaCode : '859'
                },
                contactMechanismPurposeType : {},
                ordinality : '0'
            }, {
                personUuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
                contactMechanism : {
                    uuid : 'f9286502-3451-4b44-ba49-451fcb27dd92',
                    type : {uuid : '6a73618e-a536-11e6-9155-125e1c46ef60', name : 'Address'},
                    address1 : '555 Some Kinda Way',
                    city : '',
                    state : '',
                    postalCode : ''
                },
                contactMechanismPurposeType : {},
                ordinality : '1'
            }],
            serviceCoverages : [{
                personUuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
                fromDate : '2017-02-08',
                thruDate : '2017-02-10',
                ordinality : 0,
                serviceCoveragePlan : {uuid : 'cc2042b9-b687-11e6-9155-125e1c46ef60', serviceCoverageOrganization : {uuid : 'ba25622b-b687-11e6-9155-125e1c46ef60', name : 'RI EOHHS'}, name : 'Medicaid'},
                classifications : [{
                    value : 'RiteCare',
                    serviceCoveragePlanClassification : {
                        uuid : '3235aff4-b6a8-11e6-9155-125e1c46ef60',
                        name : 'Beneficiary ID',
                        serviceCoveragePlanClassificationType : {uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60', name : 'Id'}
                    }
                }, {
                    value : '5245118964',
                    serviceCoveragePlanClassification : {
                        uuid : '3d266eef-b6a8-11e6-9155-125e1c46ef60',
                        name : 'Program',
                        serviceCoveragePlanClassificationType : {uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60', name : 'Benefit'}
                    }
                }]
            }]
        }],
        _paging : {total : 2, page : 0, size : 10}
    }],
    getHeaderSearchResultsWrap : {
        searchString : 'stark',
        timeOut : false,
        timeInMillis : 4,
        maxScore : 4.875618,
        results : [{
            score : 4.875618,
            uuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
            firstName : 'Arya',
            lastName : 'Stark',
            middleName : 'middle',
            birthDate : '1997-01-12',
            contactMechanisms : [{
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {uuid : '4434867d-7500-476e-b75f-545ad26351e8', type : {uuid : '7a15def7-a536-11e6-9155-125e1c46ef60', name : 'Electronic Address'}, electronicAddressString : 'arya.stark.body@angular-starter.comz'},
                contactMechanismPurposeType : {},
                ordinality : '0'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {
                    uuid : '38e25172-507c-4136-ba93-f4b10d7a0fef',
                    type : {uuid : '81dbe990-a536-11e6-9155-125e1c46ef60', name : 'Telecommunications Number'},
                    contactNumber : '2229968',
                    countryCode : '1',
                    areaCode : '755'
                },
                contactMechanismPurposeType : {},
                ordinality : '0'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {uuid : 'e1717d62-54d2-43b0-8b00-a323310bd7e2', type : {uuid : '7a15def7-a536-11e6-9155-125e1c46ef60', name : 'Electronic Address'}, electronicAddressString : 'todd.crone@angular-starter.com'},
                contactMechanismPurposeType : {},
                ordinality : '1'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {uuid : '9b8bc7a3-de62-4525-adb3-2cea9252d353', type : {uuid : '7a15def7-a536-11e6-9155-125e1c46ef60', name : 'Electronic Address'}, electronicAddressString : 'some.person1@gmail.com'},
                contactMechanismPurposeType : {},
                ordinality : '2'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {
                    uuid : 'c10c5fbe-d779-4203-81b2-03b6f1d49ad1',
                    type : {uuid : '6a73618e-a536-11e6-9155-125e1c46ef60', name : 'Address'},
                    address1 : '123 Todd Crone Avenue',
                    city : '',
                    state : '',
                    postalCode : ''
                },
                contactMechanismPurposeType : {},
                ordinality : '1'
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                contactMechanism : {uuid : '88a7c4a9-6f88-4a76-a5a8-07d7ef268daa', type : {uuid : '7a15def7-a536-11e6-9155-125e1c46ef60', name : 'Electronic Address'}, electronicAddressString : 'madhu@angular-starter.com'},
                contactMechanismPurposeType : {},
                ordinality : '0'
            }],
            serviceCoverages : [{
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                fromDate : '2017-02-01',
                ordinality : 0,
                serviceCoveragePlan : {uuid : 'cc2042b9-b687-11e6-9155-125e1c46ef60', serviceCoverageOrganization : {uuid : 'ba25622b-b687-11e6-9155-125e1c46ef60', name : 'RI EOHHS'}, name : 'Medicaid'},
                classifications : [{
                    value : '2342344234',
                    serviceCoveragePlanClassification : {
                        uuid : '3235aff4-b6a8-11e6-9155-125e1c46ef60',
                        name : 'Beneficiary ID',
                        serviceCoveragePlanClassificationType : {uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60', name : 'Id'}
                    }
                }, {
                    value : 'CNOM',
                    serviceCoveragePlanClassification : {
                        uuid : '3d266eef-b6a8-11e6-9155-125e1c46ef60',
                        name : 'Program',
                        serviceCoveragePlanClassificationType : {uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60', name : 'Benefit'}
                    }
                }]
            }, {
                personUuid : '21e1d494-c684-42c3-8c27-1123780a4c6d',
                fromDate : '2017-02-01',
                thruDate : '2017-02-25',
                ordinality : 0,
                serviceCoveragePlan : {
                    uuid : 'd70b3f01-b687-11e6-9155-125e1c46ef60',
                    serviceCoverageOrganization : {uuid : 'ba25622b-b687-11e6-9155-125e1c46ef60', name : 'RI EOHHS'},
                    name : 'Temporary Assistance for Needy Families',
                    identifier : 'TANF'
                },
                classifications : []
            }]
        }, {
            score : 4.875618,
            uuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
            firstName : 'Sansa',
            lastName : 'Stark',
            middleName : null,
            birthDate : '1996-02-21',
            contactMechanisms : [{
                personUuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
                contactMechanism : {
                    uuid : '3fdb1cdf-0b2b-4513-a682-b0cadcac2ffd',
                    type : {uuid : '6a73618e-a536-11e6-9155-125e1c46ef60', name : 'Address'},
                    address1 : '123 American Ave',
                    city : 'Jacksonville',
                    state : 'KY',
                    postalCode : '50505'
                },
                contactMechanismPurposeType : {},
                ordinality : '0'
            }, {
                personUuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
                contactMechanism : {
                    uuid : 'a9a1556e-9758-4484-9ca8-43d668736437',
                    type : {uuid : '81dbe990-a536-11e6-9155-125e1c46ef60', name : 'Telecommunications Number'},
                    contactNumber : '5555555',
                    countryCode : '1',
                    areaCode : '859'
                },
                contactMechanismPurposeType : {},
                ordinality : '0'
            }, {
                personUuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
                contactMechanism : {
                    uuid : 'f9286502-3451-4b44-ba49-451fcb27dd92',
                    type : {uuid : '6a73618e-a536-11e6-9155-125e1c46ef60', name : 'Address'},
                    address1 : '555 Some Kinda Way',
                    city : '',
                    state : '',
                    postalCode : ''
                },
                contactMechanismPurposeType : {},
                ordinality : '1'
            }],
            serviceCoverages : [{
                personUuid : '925cc680-108b-4e27-ae3b-bc11111686fa',
                fromDate : '2017-02-08',
                thruDate : '2017-02-10',
                ordinality : 0,
                serviceCoveragePlan : {uuid : 'cc2042b9-b687-11e6-9155-125e1c46ef60', serviceCoverageOrganization : {uuid : 'ba25622b-b687-11e6-9155-125e1c46ef60', name : 'RI EOHHS'}, name : 'Medicaid'},
                classifications : [{
                    value : 'RiteCare',
                    serviceCoveragePlanClassification : {
                        uuid : '3235aff4-b6a8-11e6-9155-125e1c46ef60',
                        name : 'Beneficiary ID',
                        serviceCoveragePlanClassificationType : {uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60', name : 'Id'}
                    }
                }, {
                    value : '5245118964',
                    serviceCoveragePlanClassification : {
                        uuid : '3d266eef-b6a8-11e6-9155-125e1c46ef60',
                        name : 'Program',
                        serviceCoveragePlanClassificationType : {uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60', name : 'Benefit'}
                    }
                }]
            }]
        }],
        _paging : {total : 2, page : 0, size : 10}
    }
};
