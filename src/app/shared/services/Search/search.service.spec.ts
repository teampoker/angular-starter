import {
    async,
    inject,
    TestBed
} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {APIMockService} from '../Mock/api-mock.service';
import {BackendService} from '../Backend/backend.service';
import {SearchService} from './search.service';
import {SearchParameters} from '../../../store/Search/types/search-parameters.model';
import {SEARCH_MOCK} from './search.service.mocks';

// unit tests for SearchService
describe('SearchService', () => {
    beforeEach(async(() => {
        // configure the testing module
        TestBed.configureTestingModule({
            imports     : [HttpModule],
            providers   : [
                APIMockService,
                BackendService,
                SearchService
            ]
        });
    }));

    it('can retrieve a list of Search Results when search text is provided', async(inject([SearchService], searchService => {
        const searchParams = new SearchParameters({
            searchText  : 'user entered text',
            pageSize    : '',
            pageOffset  : 0,
            sortColumn  : '',
            sortOrder   : 'asc'
        });

        searchService.getHeaderSearchResults(searchParams, 0.7).subscribe(response => {
            expect(response).toEqual(SEARCH_MOCK.getHeaderSearchResults[0]);
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));

    it('can return an empty result set when search text is NOT provided', async(inject([SearchService], searchService => {
        const searchParams = new SearchParameters({
            searchText  : '',
            pageSize    : '',
            pageOffset  : 0,
            sortColumn  : '',
            sortOrder   : 'asc'
        });

        searchService.getHeaderSearchResults(searchParams, 0.7).subscribe(response => {
            expect(response).toEqual({
                results : []
            });
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));

    xit('can retrieve a list of People Results when search text is provided', async(inject([SearchService], searchService => {
        const searchParams = new SearchParameters({
            searchText  : 'user entered text',
            pageSize    : '',
            pageOffset  : 0,
            sortColumn  : 'lastName',
            sortOrder   : 'asc'

        });

        searchService.getPeopleSearchResults(searchParams, 0.7).subscribe(response => {
            expect(response).toEqual(SEARCH_MOCK.getPeopleSearchResults[0]);
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));

    it('can return an empty result set when search text is NOT provided', async(inject([SearchService], peopleService => {
        const searchParams = new SearchParameters({
            searchText  : '',
            pageSize    : '',
            pageOffset  : 0,
            sortColumn  : '',
            sortOrder   : ''
        });

        peopleService.getPeopleSearchResults(searchParams, 0.7).subscribe(response => {
            expect(response).toEqual({
                results : []
            });
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));

    it('can retrieve a list of Search Types', async(inject([SearchService], searchService => {
        searchService.getSearchTypes().subscribe(response => {
            expect(response).toEqual(SEARCH_MOCK.getSearchTypes);
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));
});
