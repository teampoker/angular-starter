import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {
    URLSearchParams,
    RequestOptions
} from '@angular/http';

import {BackendService} from '../Backend/backend.service';
import {SEARCH_MOCK} from './search.service.mocks';
import {SearchParameters} from '../../../store/Search/types/search-parameters.model';

@Injectable()

/**
 * Implementation of SearchService: handles all backend api requests related to search functionality
 */
export class SearchService {
    /**
     * SearchService constructor
     * @param backendService
     */
    constructor (private backendService : BackendService) { }

    /**
     * getSearchTypes - returns a list fo search type categories
     * @returns {Promise<void>|Promise<any|string>}
     *
     * ROOT ENDPOINT
     */
    getSearchTypes() : Observable<Array<any>> {
        // since this is a root endpoint we have to extract it's base url from the webpack config
        const handle      : string = 'getSearchTypes',
              searchUrl   : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let searchTypes;

            // set search options
            this.backendService.get(searchUrl, handle, SEARCH_MOCK)
                .first()
                .subscribe(response => {
                    // store search types
                    searchTypes = response;

                    // return observer
                    observer.next(searchTypes);
                }, error => {
                    // return error
                    observer.error(undefined);
                });
        });
    }

    /**
     * query backend to do a fuzzy search against user entered text
     *
     * @param searchParams provided search parameters for request
     * @param minScore minimum search score
     *
     * @returns {Array<any>}
     */
    getHeaderSearchResults(searchParams : SearchParameters, minScore : number) : Observable<Array<any>> {
        const handle      : string = 'beneficiarySearch',
              searchUrl   : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let options         : RequestOptions,
                searchResults   : Array<any> = [];

            const queryParams   : URLSearchParams = new URLSearchParams();

            // check for empty string
            if (searchParams.get('searchText') === '' || !searchParams.get('searchText')) {
                // send back empty result
                observer.next({
                    results : []
                });
            }
            else {
                // build query string based on available input parameters

                // was minimum score provided?
                if (minScore) {
                    queryParams.set('page', searchParams.get('pageOffset'));
                    queryParams.set('pageSize', searchParams.get('pageSize'));
                    queryParams.set('sortColumn', searchParams.get('sortColumn'));
                    queryParams.set('sortOrder', searchParams.get('sortOrder'));
                    queryParams.set('minScore', minScore.toString());
                }
                // no minimum score
                else {
                    queryParams.set('page', searchParams.get('pageOffset'));
                    queryParams.set('pageSize', searchParams.get('pageSize'));
                    queryParams.set('sortColumn', searchParams.get('sortColumn'));
                    queryParams.set('sortOrder', searchParams.get('sortOrder'));
                }

                // append search params to HTTP request options
                options = new RequestOptions({
                    search : queryParams
                });

                // dispatch API call
                this.backendService.get(searchUrl + searchParams.get('searchText'), 'getHeaderSearchResults', SEARCH_MOCK, undefined, options)
                    .first()
                    .subscribe(response => {
                        response.length ? searchResults = response[0] : searchResults = response;

                        // return observer
                        observer.next(searchResults);
                    }, error => {
                        // return error
                        observer.error(undefined);
                    });
            }
        });
    }

    /**
     * query backend to do a people finder search
     *
     * @param peopleParams provided people search parameters for request
     * @param minScore minimum people score
     *
     * @returns {Array<any>}
     */
    getPeopleSearchResults(peopleParams : SearchParameters, minScore : number) : Observable<Array<any>> {
        const handle      : string = 'beneficiarySearch',
              searchUrl   : string = API_CONFIG[handle];

        return Observable.create(observer => {
            let options         : RequestOptions,
                searchResults   : Array<any> = [];

            const queryParams   : URLSearchParams = new URLSearchParams();

            // check for empty string
            if (peopleParams.get('searchText') === '' || !peopleParams.get('searchText')) {
                // send back empty result
                observer.next({
                    results : []
                });
            }
            else {
                // build query string based on available input parameters

                // was minimum score provided?
                if (minScore) {
                    queryParams.set('page', peopleParams.get('pageOffset'));
                    queryParams.set('pageSize', peopleParams.get('pageSize'));
                    queryParams.set('sortColumn', peopleParams.get('sortColumn'));
                    queryParams.set('sortOrder', peopleParams.get('sortOrder'));
                    queryParams.set('minScore', minScore.toString());
                }
                // no minimum score
                else {
                    queryParams.set('page', peopleParams.get('pageOffset'));
                    queryParams.set('pageSize', peopleParams.get('pageSize'));
                    queryParams.set('sortColumn', peopleParams.get('sortColumn'));
                    queryParams.set('sortOrder', peopleParams.get('sortOrder'));
                }

                // append people params to HTTP request options
                options = new RequestOptions({
                    search : queryParams
                });

                // dispatch API call
                this.backendService.get(searchUrl + peopleParams.get('searchText'), 'getHeaderSearchResults', SEARCH_MOCK, undefined, options)
                    .first()
                    .subscribe(response => {
                        response.length ? searchResults = response[0] : searchResults = response;

                        // return observer
                        observer.next(searchResults);
                    }, error => {
                        // return error
                        observer.error(undefined);
                    });
            }
        });
    }
}
