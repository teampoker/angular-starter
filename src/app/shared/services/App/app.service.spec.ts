import {
    TestBed,
    inject
} from '@angular/core/testing';
import {AppService} from './app.service';
import {AppStateActions} from '../../../store/App/app-state.actions';
import {NavActions} from '../../../store/Navigation/nav.actions';
import {AppStateSelectors} from '../../../store/App/app-state.selectors';
import {AlertItem} from '../../../store/Navigation/types/alert-item.model';
import {NgReduxModule} from '@angular-redux/store';

/**
 * Mock the Nav Actions dependency
 */
export class MockNavActions extends NavActions {
    constructor() {
        super(undefined, undefined, undefined, undefined);
    }

    updateAlertMessageState(message : AlertItem) {
        return;
    }
}

describe('Service: AppService', () => {
    let testService     : AppService;
    let testSelectors   : AppStateSelectors;
    let testActions     : AppStateActions;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports   : [
                NgReduxModule
            ],
            providers : [
                AppService,
                AppStateActions,
                AppStateSelectors,
                {
                    provide  : NavActions,
                    useClass : MockNavActions
                }
            ]
        });
    });

    beforeEach(inject([AppService, AppStateSelectors, AppStateActions], (service, selectors, actions) => {
        testService   = service;
        testSelectors = selectors;
        testActions   = actions;

        spyOn(testActions, 'busy');
        spyOn(testActions, 'idle');
    }));

    // happy path
    it('should call the busy and idle actions', () => {
        // define a unit of work to be done in a minute
        function mockWork() {

        }

        testService.indicateBusy(mockWork);

        expect(testActions.busy).toHaveBeenCalled();
        expect(testActions.idle).toHaveBeenCalled();
    });

    // exception scenario
    it('should call the busy and idle actions when work throws exception', () => {
        // define a unit of work to be done in a minute
        function mockWork() {
            throw new Error('just when you thought it was safe...');
        }

        testService.indicateBusy(mockWork);

        expect(testActions.busy).toHaveBeenCalled();
        expect(testActions.idle).toHaveBeenCalled();
    });
});
