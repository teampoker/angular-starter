import {Injectable} from '@angular/core';

import {AppStateActions} from '../../../store/App/app-state.actions';
import {NavActions} from '../../../store/Navigation/nav.actions';
import {
    AlertItem,
    EnumAlertType
} from '../../../store/Navigation/types/alert-item.model';

@Injectable()
export class AppService {
    /**
     *
     * @param appStateActions
     * @param navActions Used to wire up the alerts/notifications
     */
    constructor(
        private appStateActions : AppStateActions,
        private navActions      : NavActions
    ) {
    }

    /**
     * Wrapper function that wraps synchronous work.
     * @param work callback that performs some set of actions.
     */
    indicateBusy(work : any) {
        this.appStateActions.busy();

        try {
            work();
        }
        catch (error) {
            // display an error message
            const errorMessage : string = error.message ? error.message : 'Problem encountered. Please contact support.';
            this.navActions.updateAlertMessageState(new AlertItem({
                alertType : EnumAlertType.ERROR,
                message   : errorMessage
            }));
        }
        finally {
            this.appStateActions.idle();
        }
    }

}
