import {
    async,
    inject,
    TestBed
} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {APIMockService} from '../Mock/api-mock.service';
import {BackendService} from '../Backend/backend.service';
import {MetaDataTypesService} from './meta-data-types.service';
import {MetaDataTypesState} from '../../../store/MetaDataTypes/types/meta-data-types-state.model';

describe('MetaDataTypesService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports     : [HttpModule],
            providers   : [
                APIMockService,
                BackendService,
                MetaDataTypesService
            ]
        });
    });

    it('should return Immutable MetaDataTypesState Record type', async(inject([MetaDataTypesService], metaTypesService => {
        // perform the api call
        metaTypesService.getMetaDataTypes().subscribe(response => {
            expect(response instanceof MetaDataTypesState).toEqual(true);
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));
});
