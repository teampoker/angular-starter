export const META_DATA_TYPES_MOCK : any = {

    getCoreMetaDataTypes : [{
        contactMechanismTypes : [{
            name : 'Address',
            purposeTypes : [{name : 'Residential', uuid : '3379836f-a557-11e6-9155-125e1c46ef60'}, {name : 'Mailing', uuid : '3880bcd5-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Work',
                uuid : '3c141258-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Billing', uuid : '40b3786c-a557-11e6-9155-125e1c46ef60'}, {name : 'Temporary', uuid : '44e326ba-a557-11e6-9155-125e1c46ef60'}],
            uuid : '6a73618e-a536-11e6-9155-125e1c46ef60'
        }, {
            name : 'Electronic Address',
            purposeTypes : [{name : 'Personal Email', uuid : '48ded7dd-a557-11e6-9155-125e1c46ef60'}, {name : 'Work Email', uuid : '4c4c379e-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Temporary Email',
                uuid : '4fa8e286-a557-11e6-9155-125e1c46ef60'
            }],
            uuid : '7a15def7-a536-11e6-9155-125e1c46ef60'
        }, {
            name : 'Telecommunications Number',
            purposeTypes : [{name : 'Home Phone', uuid : '52f180c4-a557-11e6-9155-125e1c46ef60'}, {name : 'Personal Cell', uuid : '55cd441d-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Personal SMS',
                uuid : '58d72d80-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Personal Fax', uuid : '5bf0e274-a557-11e6-9155-125e1c46ef60'}, {name : 'Alternate Phone', uuid : '5ed42d9a-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Alternate Cell',
                uuid : '65b17e19-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Alternate SMS', uuid : '6906e7aa-a557-11e6-9155-125e1c46ef60'}, {name : 'Alternate Fax', uuid : '6d003708-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Work Phone',
                uuid : '70ce30df-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Work Cell', uuid : '741b1621-a557-11e6-9155-125e1c46ef60'}, {name : 'Work SMS', uuid : '7745c3e7-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Work Fax',
                uuid : '7ac22c78-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Emergency Phone', uuid : '7df852f1-a557-11e6-9155-125e1c46ef60'}, {name : 'Emergency Cell', uuid : '81256b6d-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Emergency SMS',
                uuid : '846b5568-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Temporary Phone', uuid : 'a4a91b4a-a557-11e6-9155-125e1c46ef60'}, {name : 'Temporary Cell', uuid : 'a93396a2-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Temporary SMS',
                uuid : 'ae395b88-a557-11e6-9155-125e1c46ef60'
            }],
            uuid : '81dbe990-a536-11e6-9155-125e1c46ef60'
        }],
        genderTypes : [{name : 'Female', uuid : 'c8633432-a531-11e6-9155-125e1c46ef60'}, {name : 'Unidentified', uuid : 'fc6d303b-a4ea-11e6-9155-125e1c46ef60'}, {name : 'Male', uuid : 'fcfded02-a531-11e6-9155-125e1c46ef60'}],
        languages : [{family : 'Indo-European', isoCode : 'eng', name : 'English', uuid : '008d1bf0-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'spa',
            name : 'Spanish',
            uuid : '0937fd5e-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Sino-Tibetan', isoCode : 'cmn', name : 'Mandarin', uuid : '0ec9a2a1-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'fra',
            name : 'French',
            uuid : '1335fc11-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Austronesian', isoCode : 'tgl', name : 'Tagalog', uuid : '1853b201-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Austroasiatic',
            isoCode : 'vie',
            name : 'Vietnamese',
            uuid : '1dd57bf7-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Korean', isoCode : 'kor', name : 'Korean', uuid : '22e2acc5-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'deu',
            name : 'German',
            uuid : '270687da-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Afro-Asiatic', isoCode : 'ara', name : 'Arabic', uuid : '2b76a6a4-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'rus',
            name : 'Russian',
            uuid : '2f71c85d-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'inc', name : 'Indic', uuid : '35ef87d7-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'ben',
            name : 'Bengali',
            uuid : '39b99e8c-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'ita', name : 'Italian', uuid : '3d4c8486-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'por',
            name : 'Portuguese',
            uuid : '40c4bdc8-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'hin', name : 'Hindi', uuid : '44182624-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'pol',
            name : 'Polish',
            uuid : '4888c3bc-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Japanese', isoCode : 'jpn', name : 'Japanese', uuid : '4bdc6885-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'fas',
            name : 'Persian',
            uuid : '4fd51907-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'urd', name : 'Urdu', uuid : '5392fe5d-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'guj',
            name : 'Gujarati',
            uuid : '5718a2d6-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'ell', name : 'Greek', uuid : '5a73bd02-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'hbs',
            name : 'Serbo-Croatian',
            uuid : '5d9c7dfa-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'hye', name : 'Armenian', uuid : '60ca9f79-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Afro-Asiatic',
            isoCode : 'heb',
            name : 'Hebrew',
            uuid : '640b98e8-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Austroasiatic', isoCode : 'khm', name : 'Khmer', uuid : '6785f000-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Na-Dene',
            isoCode : 'nav',
            name : 'Navajo',
            uuid : '6b0424ce-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Tai\u2013Kadai', isoCode : 'tha', name : 'Thai', uuid : '6e937194-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'yid',
            name : 'Yiddish',
            uuid : '71e3668f-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Tai\u2013Kadai', isoCode : 'lao', name : 'Laotian', uuid : '750e1690-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Dravidian',
            isoCode : 'tam',
            name : 'Tamil',
            uuid : '7a887c23-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Sino-Tibetan', isoCode : 'yue', name : 'Cantonese', uuid : '7cf3f4bc-a552-11e6-9155-125e1c46ef60'}],
        medicalConditionTypes : [{name : 'Diabetic', uuid : '13e97f14-a536-11e6-9155-125e1c46ef60'}, {name : 'Pregnant - Last Trimester', uuid : '2668168e-a536-11e6-9155-125e1c46ef60'}, {
            name : 'Bariatric',
            uuid : '329c6f17-a536-11e6-9155-125e1c46ef60'
        }],
        modeOfTransportation: [{
            name : 'Mode of Transportation',
            serviceCategories : [{
                name : 'Stretcher Vehicle',
                ordinality : 4,
                serviceOfferings : [{name : 'Stretcher Vehicle', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6f-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c26015-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Mass Transit',
                ordinality : 1,
                serviceOfferings : [{name : 'Mass Transit', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d69-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c2600c-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Ambulance',
                ordinality : 5,
                serviceOfferings : [{name : 'Isolette', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d78-ee51-11e6-9155-125e1c46ef60'}, {
                    name : 'Advanced Life Support',
                    serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'},
                    uuid : 'c1750d71-ee51-11e6-9155-125e1c46ef60'
                }, {name : 'CCT', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d76-ee51-11e6-9155-125e1c46ef60'}, {
                    name : 'Basic Life Support',
                    serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'},
                    uuid : 'c1750d74-ee51-11e6-9155-125e1c46ef60'
                }],
                uuid : '07c26016-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Mileage Reimbursement',
                ordinality : 0,
                serviceOfferings : [{name : 'Mileage Reimbursement', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d65-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c25fff-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Wheelchair Vehicle',
                ordinality : 3,
                serviceOfferings : [{name : 'Wheelchair Vehicle', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6d-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c26012-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Air Travel',
                ordinality : 6,
                serviceOfferings : [{name : 'Air Travel', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d7a-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c26019-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Ambulatory',
                ordinality : 2,
                serviceOfferings : [{name : 'Ambulatory', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6a-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c26010-ee4f-11e6-9155-125e1c46ef60'
            }],
            uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'
        }],
        personConnectionTypes : [{name : 'Parent / Family', uuid : 'adc8584b-a536-11e6-9155-125e1c46ef60'}, {name : 'Emergency Contact', uuid : 'b53d02aa-a536-11e6-9155-125e1c46ef60'}, {
            name : 'Nurse / Doctor / Counselor',
            uuid : 'bafe6e3a-a536-11e6-9155-125e1c46ef60'
        }, {name : 'Social / Case Worker / Case Manager', uuid : 'c22825cf-a536-11e6-9155-125e1c46ef60'}, {name : 'Plan / Client', uuid : 'c854800f-a536-11e6-9155-125e1c46ef60'}, {
            name : 'Driver',
            uuid : 'ce1fe6c7-a536-11e6-9155-125e1c46ef60'
        }],
        physicalCharacteristicTypes : [{name : 'Height', uuid : 'eeea982c-a534-11e6-9155-125e1c46ef60'}, {name : 'Weight', uuid : 'f7149892-a534-11e6-9155-125e1c46ef60'}],
        serviceCategories : [{
            name : 'Mileage Reimbursement',
            ordinality : 0,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Mileage Reimbursement', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d65-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c25fff-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Mass Transit',
            ordinality : 1,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Mass Transit', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d69-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c2600c-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Ambulatory',
            ordinality : 2,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Ambulatory', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6a-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c26010-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Wheelchair Vehicle',
            ordinality : 3,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Wheelchair Vehicle', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6d-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c26012-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Stretcher Vehicle',
            ordinality : 4,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Stretcher Vehicle', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6f-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c26015-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Ambulance',
            ordinality : 5,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Isolette', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d78-ee51-11e6-9155-125e1c46ef60'}, {
                name : 'Advanced Life Support',
                serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'},
                uuid : 'c1750d71-ee51-11e6-9155-125e1c46ef60'
            }, {name : 'CCT', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d76-ee51-11e6-9155-125e1c46ef60'}, {
                name : 'Basic Life Support',
                serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'},
                uuid : 'c1750d74-ee51-11e6-9155-125e1c46ef60'
            }],
            uuid : '07c26016-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Air Travel',
            ordinality : 6,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Air Travel', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d7a-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c26019-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Physicians',
            ordinality : 7,
            serviceCategoryType : {name : 'Treatment Type', serviceCategories : [], uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Ophthalmologist / Optometrist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eec5-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Podiatrist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eee1-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Neonatologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eea7-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Obstetrics & Gynecologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eebd-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Chiropractor', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee73-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Vascular Specialist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71aa-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Endocrinologist ', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee83-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Allergist & Immunologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee62-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Plastic Surgeon', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eedd-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Proctologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71a6-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Neurologist / Neurosurgeon', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeb0-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Primary Care Physician',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71a3-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Cardiologist ', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee6f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Dentist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee78-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Psychologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eee9-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Rheumatologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eef1-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Surgeon', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eef8-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Geriatric Medicine',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee94-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Orthopedic', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eecd-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Heptologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff719e-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Orthodonist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eec9-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Pulmonologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eeec-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Ear, Nose & Throat (ENT)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee8a-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Physiatrist - Physical Medicine & Rehab', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eed9-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Urologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71a8-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Nephrologists', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeab-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Optician',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eed5-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Family Practitioner', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee8f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Oncologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eec1-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Internal Medicine', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eea3-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Nurse Practitioner',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eeb9-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Gastroenterologist (GI)', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee99-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Sports Medicine ',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eef4-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Audiologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee69-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Dermatologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee7d-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Hematologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee9e-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Pediatrician',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eed1-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Psychiatrist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eee5-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Perinatologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71a1-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Nurse Midwife', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeb5-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Acupuncture',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee45-df66-11e6-9155-125e1c46ef60'
            }],
            uuid : '1c502abf-df89-11e6-9155-125e1c46ef60'
        }, {
            name : 'Testing',
            ordinality : 8,
            serviceCategoryType : {name : 'Treatment Type', serviceCategories : [], uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Lead Screening / Testing', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef04-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Sleep Study',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef10-df66-11e6-9155-125e1c46ef60'
            }, {name : 'EKG & EEG Testing', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eefc-df66-11e6-9155-125e1c46ef60'}, {
                name : 'PET Scan',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : 'a44b8e44-ee67-11e6-9155-125e1c46ef60'
            }, {name : 'Lab Work', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef00-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Stress Test',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef13-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Mammogram', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef07-df66-11e6-9155-125e1c46ef60'}, {
                name : 'CAT Scan',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : 'a44b8e32-ee67-11e6-9155-125e1c46ef60'
            }, {name : 'PSA Test', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : 'a44b8e49-ee67-11e6-9155-125e1c46ef60'}, {
                name : 'MRI',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef0b-df66-11e6-9155-125e1c46ef60'
            }],
            uuid : '1c502acb-df89-11e6-9155-125e1c46ef60'
        }, {
            name : 'Treatments',
            ordinality : 9,
            serviceCategoryType : {name : 'Treatment Type', serviceCategories : [], uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Dialysis', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef5b-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Cranial Technologies',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3624a-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Infusion Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef89-df66-11e6-9155-125e1c46ef60'}, {
                name : 'CSTAR',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef53-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Surgery - Outpatient', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eff7-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Abortion',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef1d-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Occupational Therapy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efa8-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Club House - Treatment for Psych Patients',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef4b-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Methadone Treatment', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef9b-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Chemo Therapy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef47-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Mental Health Group Trip', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d36258-ee69-11e6-9155-125e1c46ef60'}, {
                name : 'Group Therapy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef78-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Experimental Treatment / Procedure', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d3624e-ee69-11e6-9155-125e1c46ef60'}, {
                name : 'Prosthetic Services',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efc7-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Rural Health Clinic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efd6-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Wound Care',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f00a-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Adult Day Health', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef21-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Botox Injections - Non-Cosmetic',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef3b-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Midwife Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef9f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Physical Therapy (PT)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efbe-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Smoking Cessation', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efda-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Support Group',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efee-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Storefront', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8a-ee74-11e6-9155-125e1c46ef60'}, {
                name : 'Federally Qualified Health Centers',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef6f-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Gender Reassignment Surgery', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef73-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Hospice Admission',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef81-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Respiratory Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efd2-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Wellness Visit',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3625f-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Breast Reconstruction', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef3f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Colonoscopy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef4e-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Flu Shots', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d36250-ee69-11e6-9155-125e1c46ef60'}, {
                name : 'Radiology & X-Ray',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efce-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Intermediate Care for the Developmentally Disabled',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef8e-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Substance Abuse - Treatment & Evaluation', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efe2-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Orthotic Services',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efb0-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Autism Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef2e-df66-11e6-9155-125e1c46ef60'}, {
                name : 'HIV / AIDS Testing & Treatment Counseling',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d36254-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Equestrian (Horse) Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef7c-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Family Planning Clinic Services',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef6b-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Infertility Services',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef85-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Comprehensive Outpatient Rehabilitation Facilities (CORF)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3623f-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Speech Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efde-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Cardiac Rehab',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef43-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Substance Abuse - Inpatient', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efea-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Substance Abuse',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3625d-ee69-11e6-9155-125e1c46ef60'
            }, {
                name : 'Music Therapy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efa4-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Early Periodic Screening, Diagnosis & Treatment',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef67-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Dialysis Fistula Replacement', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef5f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Medication Management ',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef97-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Surgery - Hospital', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eff3-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Adult Daycare',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef25-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Aquatic Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef2a-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Blood Transfusion - Type & Match',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef37-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Pharmacy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efb9-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Mental Health',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d36255-ee69-11e6-9155-125e1c46ef60'
            }, {
                name : 'Dietary / Nutritional Counseling',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef63-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Treatment at Veteran\'s Affairs Hospital / Clinic (VA)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f002-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Radiation Treatment',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efca-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Prescribed Pediatric Extended Services (PPEC)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3625b-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Massage Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef93-df66-11e6-9155-125e1c46ef60'}, {
                name : 'AA / Self Help Groups',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef18-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Transplant Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723effe-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Dental Treatment - Surgery',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef57-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Preventative Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efc2-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Surgical Follow Up',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723effa-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Weight Control Program', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f006-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Pain Management',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efb4-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Bariatric Surgery',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef33-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Optical - Glasses & Contact Lenses - Pick-up',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efad-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Substance Abuse - Counseling', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efe6-df66-11e6-9155-125e1c46ef60'}],
            uuid : '1c502acf-df89-11e6-9155-125e1c46ef60'
        }, {
            name : 'Non-medical',
            ordinality : 10,
            serviceCategoryType : {name : 'Treatment Type', serviceCategories : [], uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{
                name : 'Hospital to Residence',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f04a-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Court Ordered Services', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f01c-df66-11e6-9155-125e1c46ef60'}, {
                name : 'County Health Department',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f019-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Hospital to Nursing Home', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f046-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Catholic Charities',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc79-ee74-11e6-9155-125e1c46ef60'
            }, {name : 'Diagnostic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f028-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Hospital to Treatment Facility',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f04e-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Health Education', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f037-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Dentures',
                serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f020-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Hospital Admission', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f03e-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Other - Medical',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f078-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Social - HARP', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc91-ee74-11e6-9155-125e1c46ef60'}, {
                name : 'Workman\'s Compensation',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc98-ee74-11e6-9155-125e1c46ef60'
            }, {name : 'Other - Non-Medical', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f07d-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Urgent Care - Visit',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f0a2-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Psych - Discharge', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f086-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Hospital Discharge',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f043-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Congregate Meals',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f015-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Hearing Aids - Testing, Fitting, Repairs', serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f03b-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Respite',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f08e-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Durable Medical Equipment',
                serviceOfferingType : {name : 'Durable Medical Equipment', uuid : '8e4327fe-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f02c-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Nursing Home to Nursing Home', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f070-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Urgent Care - Discharge',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f09e-df66-11e6-9155-125e1c46ef60'
            }, {name : 'WIC - During Pregnancy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f0b2-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Case Management Visit',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f00d-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Fitness Center', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8f-ee74-11e6-9155-125e1c46ef60'}, {
                name : 'Transportation to the Emergency Room',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f099-df66-11e6-9155-125e1c46ef60'
            }, {name : 'SSi Determination Medical Appointment', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f095-df66-11e6-9155-125e1c46ef60'}, {
                name : 'WIC - Assessment',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f0ad-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Nursing Home to Residence', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f074-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Residence to Nursing Home',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f089-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Lamaze / Birthing Classes', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f06d-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Supportive Employment',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc93-ee74-11e6-9155-125e1c46ef60'
            }, {name : 'Emergency Room - Discharge', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f02f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Social - TBI',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f092-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Grocery Store', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f033-df66-11e6-9155-125e1c46ef60'}, {
                name : 'WIC - After Pregnancy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f0a9-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Visitation - Parent visiting child who is hospitalized',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f0a5-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Transportation to the Shelter',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc96-ee74-11e6-9155-125e1c46ef60'
            }, {
                name : 'Diabetic Supplies & Education',
                serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f024-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Community Habilitation', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f011-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Psych - Admission',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f082-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Infant Care - Education', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f052-df66-11e6-9155-125e1c46ef60'}, {
                name : 'INSIGHT',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f068-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Advisory Meetings', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8c-ee74-11e6-9155-125e1c46ef60'}, {
                name : 'School Services',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc86-ee74-11e6-9155-125e1c46ef60'
            }],
            uuid : '1c502ad1-df89-11e6-9155-125e1c46ef60'
        }],
        serviceCoverageOrganizations : [{
            name : 'RI EOHHS',
            serviceCoveragePlans : [{name : 'Temporary Assistance for Needy Families', serviceCoveragePlanClassifications : [], uuid : 'd70b3f01-b687-11e6-9155-125e1c46ef60'}, {
                name : 'Medicaid',
                serviceCoveragePlanClassifications : [{
                    benefitClassificationValues : [],
                    classificationType : {name : 'Id', uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60'},
                    maximum : 1,
                    minimum : 1,
                    name : 'Beneficiary ID',
                    ordinality : 0,
                    uuid : '3235aff4-b6a8-11e6-9155-125e1c46ef60',
                    validationFormat : '^\\d{10}$'
                }, {
                    benefitClassificationValues : [{name : 'CNOM', uuid : 'dc6b9d61-d6f2-11e6-8e0c-0ecb7f4d1c02'}, {name : 'RiteCare', uuid : 'e59eb221-d6f2-11e6-8e0c-0ecb7f4d1c02'}, {
                        name : 'ABD',
                        uuid : 'd37d7735-d6f2-11e6-8e0c-0ecb7f4d1c02'
                    }], classificationType : {name : 'Benefit', uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60'}, maximum : 1, minimum : 0, name : 'Program', ordinality : 1, uuid : '3d266eef-b6a8-11e6-9155-125e1c46ef60'
                }],
                uuid : 'cc2042b9-b687-11e6-9155-125e1c46ef60'
            }, {
                name : 'Elderly Transportation Plan',
                serviceCoveragePlanClassifications : [{
                    benefitClassificationValues : [],
                    classificationType : {name : 'Id', uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60'},
                    maximum : 1,
                    minimum : 0,
                    name : 'Elder Plan ID',
                    ordinality : 0,
                    uuid : 'ebd29f95-1413-4b66-b763-00e93f700666'
                }, {
                    benefitClassificationValues : [{name : 'INSIGHT', uuid : 'ee155081-d6f2-11e6-8e0c-0ecb7f4d1c02'}],
                    classificationType : {name : 'Benefit', uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60'},
                    maximum : 1,
                    minimum : 0,
                    name : 'Benefit',
                    ordinality : 1,
                    uuid : '640df84f-ced3-45f5-a4d3-22326af6aa3f'
                }],
                uuid : 'd1802355-b687-11e6-9155-125e1c46ef60'
            }],
            uuid : 'ba25622b-b687-11e6-9155-125e1c46ef60'
        }],
        serviceCoveragePlanClassificationTypes : [{name : 'Id', uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60'}, {name : 'Group', uuid : '27427c45-b688-11e6-9155-125e1c46ef60'}, {
            name : 'Benefit',
            uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60'
        }, {name : 'Geography', uuid : '2e6d4f9f-b688-11e6-9155-125e1c46ef60'}],
        solicitationIndicatorTypes : [{name : 'No', uuid : 'c62901e5-a534-11e6-9155-125e1c46ef60'}, {name : 'Yes', uuid : 'd30da162-a534-11e6-9155-125e1c46ef60'}],
        specialRequirementTypes : [{name : 'Blind', uuid : '5e59c1ae-a53e-11e6-9155-125e1c46ef60'}, {name : 'Cane/Crutches/Walker', uuid : '64329c8c-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Car Seat',
            uuid : '68ff5eef-a53e-11e6-9155-125e1c46ef60'
        }, {name : 'Deaf', uuid : '6e7b9d90-a53e-11e6-9155-125e1c46ef60'}, {name : 'Door to Door', uuid : '72fafa2c-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Door through Door',
            uuid : '77ed2d97-a53e-11e6-9155-125e1c46ef60'
        }, {name : 'Oxygen', uuid : '7c151038-a53e-11e6-9155-125e1c46ef60'}, {name : 'Ride Alone', uuid : '80887754-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Two man carry down',
            uuid : '851f177e-ff5e-11e6-9155-125e1c46ef60'
        }, {name : 'Service Animal', uuid : '853eab36-a53e-11e6-9155-125e1c46ef60'}, {name : 'Stretcher', uuid : '89635f4b-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Unable to Sign',
            uuid : '8db3d69a-a53e-11e6-9155-125e1c46ef60'
        }, {name : 'Hand to Hand', uuid : '91df9a44-a53e-11e6-9155-125e1c46ef60'}, {name : 'Wheelchair - Manual', uuid : '95f787a7-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Wheelchair - Motorized',
            uuid : '9a2f5581-a53e-11e6-9155-125e1c46ef60'
        }, {name : 'Wheelchair Lift', uuid : '9e51d50b-a53e-11e6-9155-125e1c46ef60'}, {name : 'Other', uuid : 'a2910e38-a53e-11e6-9155-125e1c46ef60'}],
        treatmentType : [{
            name : 'Treatment Type',
            serviceCategories : [{
                name : 'Testing',
                ordinality : 8,
                serviceOfferings : [{name : 'Lead Screening / Testing', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef04-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Sleep Study',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef10-df66-11e6-9155-125e1c46ef60'
                }, {name : 'EKG & EEG Testing', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eefc-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'PET Scan',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : 'a44b8e44-ee67-11e6-9155-125e1c46ef60'
                }, {name : 'Lab Work', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef00-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Stress Test',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef13-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Mammogram', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef07-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'CAT Scan',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : 'a44b8e32-ee67-11e6-9155-125e1c46ef60'
                }, {name : 'PSA Test', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : 'a44b8e49-ee67-11e6-9155-125e1c46ef60'}, {
                    name : 'MRI',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef0b-df66-11e6-9155-125e1c46ef60'
                }],
                uuid : '1c502acb-df89-11e6-9155-125e1c46ef60'
            }, {
                name : 'Physicians',
                ordinality : 7,
                serviceOfferings : [{
                    name : 'Ophthalmologist / Optometrist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eec5-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Podiatrist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eee1-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Neonatologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eea7-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Obstetrics & Gynecologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eebd-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Chiropractor',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee73-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Vascular Specialist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '9cff71aa-ee52-11e6-9155-125e1c46ef60'}, {
                    name : 'Endocrinologist ',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee83-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Allergist & Immunologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee62-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Plastic Surgeon',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eedd-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Proctologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '9cff71a6-ee52-11e6-9155-125e1c46ef60'}, {
                    name : 'Neurologist / Neurosurgeon',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eeb0-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Primary Care Physician', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '9cff71a3-ee52-11e6-9155-125e1c46ef60'}, {
                    name : 'Cardiologist ',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee6f-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Dentist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee78-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Psychologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eee9-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Rheumatologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eef1-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Surgeon',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eef8-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Geriatric Medicine', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee94-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Orthopedic',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eecd-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Heptologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '9cff719e-ee52-11e6-9155-125e1c46ef60'}, {
                    name : 'Orthodonist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eec9-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Pulmonologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeec-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Ear, Nose & Throat (ENT)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee8a-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Physiatrist - Physical Medicine & Rehab', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eed9-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Urologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '9cff71a8-ee52-11e6-9155-125e1c46ef60'
                }, {name : 'Nephrologists', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeab-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Optician',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eed5-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Family Practitioner', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee8f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Oncologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eec1-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Internal Medicine', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eea3-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Nurse Practitioner',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eeb9-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Gastroenterologist (GI)', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee99-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Sports Medicine ',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eef4-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Audiologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee69-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Dermatologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee7d-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Hematologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee9e-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Pediatrician',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eed1-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Psychiatrist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eee5-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Perinatologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '9cff71a1-ee52-11e6-9155-125e1c46ef60'
                }, {name : 'Nurse Midwife', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeb5-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Acupuncture',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee45-df66-11e6-9155-125e1c46ef60'
                }],
                uuid : '1c502abf-df89-11e6-9155-125e1c46ef60'
            }, {
                name : 'Non-medical',
                ordinality : 10,
                serviceOfferings : [{
                    name : 'Hospital to Residence',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f04a-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Court Ordered Services', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f01c-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'County Health Department',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f019-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Hospital to Nursing Home', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f046-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Catholic Charities',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc79-ee74-11e6-9155-125e1c46ef60'
                }, {name : 'Diagnostic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f028-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Hospital to Treatment Facility',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f04e-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Health Education', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f037-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Dentures',
                    serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f020-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Hospital Admission', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f03e-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Other - Medical',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f078-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Social - HARP', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc91-ee74-11e6-9155-125e1c46ef60'}, {
                    name : 'Workman\'s Compensation',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc98-ee74-11e6-9155-125e1c46ef60'
                }, {name : 'Other - Non-Medical', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f07d-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Urgent Care - Visit',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f0a2-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Psych - Discharge', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f086-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Hospital Discharge',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f043-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Congregate Meals',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f015-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Hearing Aids - Testing, Fitting, Repairs',
                    serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f03b-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Respite', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f08e-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Durable Medical Equipment',
                    serviceOfferingType : {name : 'Durable Medical Equipment', uuid : '8e4327fe-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f02c-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Nursing Home to Nursing Home', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f070-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Urgent Care - Discharge',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f09e-df66-11e6-9155-125e1c46ef60'
                }, {name : 'WIC - During Pregnancy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f0b2-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Case Management Visit',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f00d-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Fitness Center', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8f-ee74-11e6-9155-125e1c46ef60'}, {
                    name : 'Transportation to the Emergency Room',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f099-df66-11e6-9155-125e1c46ef60'
                }, {name : 'SSi Determination Medical Appointment', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f095-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'WIC - Assessment',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f0ad-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Nursing Home to Residence', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f074-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Residence to Nursing Home',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f089-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Lamaze / Birthing Classes', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f06d-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Supportive Employment',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc93-ee74-11e6-9155-125e1c46ef60'
                }, {name : 'Emergency Room - Discharge', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f02f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Social - TBI',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f092-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Grocery Store', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f033-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'WIC - After Pregnancy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f0a9-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Visitation - Parent visiting child who is hospitalized',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f0a5-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Transportation to the Shelter',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc96-ee74-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Diabetic Supplies & Education',
                    serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f024-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Community Habilitation', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f011-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Psych - Admission',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f082-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Infant Care - Education', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f052-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'INSIGHT',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f068-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Advisory Meetings', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8c-ee74-11e6-9155-125e1c46ef60'}, {
                    name : 'School Services',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc86-ee74-11e6-9155-125e1c46ef60'
                }],
                uuid : '1c502ad1-df89-11e6-9155-125e1c46ef60'
            }, {
                name : 'Treatments',
                ordinality : 9,
                serviceOfferings : [{name : 'Dialysis', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef5b-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Cranial Technologies',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3624a-ee69-11e6-9155-125e1c46ef60'
                }, {name : 'Infusion Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef89-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'CSTAR',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef53-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Surgery - Outpatient', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eff7-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Abortion',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef1d-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Occupational Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efa8-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Club House - Treatment for Psych Patients',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef4b-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Methadone Treatment', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef9b-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Chemo Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef47-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Mental Health Group Trip', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d36258-ee69-11e6-9155-125e1c46ef60'}, {
                    name : 'Group Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef78-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Experimental Treatment / Procedure', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d3624e-ee69-11e6-9155-125e1c46ef60'}, {
                    name : 'Prosthetic Services',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efc7-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Rural Health Clinic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efd6-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Wound Care',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f00a-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Adult Day Health', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef21-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Botox Injections - Non-Cosmetic',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef3b-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Midwife Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef9f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Physical Therapy (PT)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efbe-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Smoking Cessation', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efda-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Support Group',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efee-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Storefront', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8a-ee74-11e6-9155-125e1c46ef60'}, {
                    name : 'Federally Qualified Health Centers',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef6f-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Gender Reassignment Surgery', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef73-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Hospice Admission',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef81-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Respiratory Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efd2-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Wellness Visit',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3625f-ee69-11e6-9155-125e1c46ef60'
                }, {name : 'Breast Reconstruction', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef3f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Colonoscopy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef4e-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Flu Shots', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d36250-ee69-11e6-9155-125e1c46ef60'}, {
                    name : 'Radiology & X-Ray',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efce-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Intermediate Care for the Developmentally Disabled',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef8e-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Substance Abuse - Treatment & Evaluation',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efe2-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Orthotic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efb0-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Autism Services',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef2e-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'HIV / AIDS Testing & Treatment Counseling',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d36254-ee69-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Equestrian (Horse) Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef7c-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Family Planning Clinic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef6b-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Infertility Services',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef85-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Comprehensive Outpatient Rehabilitation Facilities (CORF)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3623f-ee69-11e6-9155-125e1c46ef60'
                }, {name : 'Speech Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efde-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Cardiac Rehab',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef43-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Substance Abuse - Inpatient', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efea-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Substance Abuse',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3625d-ee69-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Music Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efa4-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Early Periodic Screening, Diagnosis & Treatment',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef67-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Dialysis Fistula Replacement', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef5f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Medication Management ',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef97-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Surgery - Hospital', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eff3-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Adult Daycare',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef25-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Aquatic Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef2a-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Blood Transfusion - Type & Match',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef37-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Pharmacy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efb9-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Mental Health',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d36255-ee69-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Dietary / Nutritional Counseling',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef63-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Treatment at Veteran\'s Affairs Hospital / Clinic (VA)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f002-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Radiation Treatment',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efca-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Prescribed Pediatric Extended Services (PPEC)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3625b-ee69-11e6-9155-125e1c46ef60'
                }, {name : 'Massage Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef93-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'AA / Self Help Groups',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef18-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Transplant Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723effe-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Dental Treatment - Surgery',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef57-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Preventative Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efc2-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Surgical Follow Up',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723effa-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Weight Control Program', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f006-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Pain Management',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efb4-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Bariatric Surgery',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef33-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Optical - Glasses & Contact Lenses - Pick-up',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efad-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Substance Abuse - Counseling', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efe6-df66-11e6-9155-125e1c46ef60'}],
                uuid : '1c502acf-df89-11e6-9155-125e1c46ef60'
            }],
            uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'
        }]
    }],
    getCoreMetaDataTypesWrap : {
        contactMechanismTypes : [{
            name : 'Address',
            purposeTypes : [{name : 'Residential', uuid : '3379836f-a557-11e6-9155-125e1c46ef60'}, {name : 'Mailing', uuid : '3880bcd5-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Work',
                uuid : '3c141258-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Billing', uuid : '40b3786c-a557-11e6-9155-125e1c46ef60'}, {name : 'Temporary', uuid : '44e326ba-a557-11e6-9155-125e1c46ef60'}],
            uuid : '6a73618e-a536-11e6-9155-125e1c46ef60'
        }, {
            name : 'Electronic Address',
            purposeTypes : [{name : 'Personal Email', uuid : '48ded7dd-a557-11e6-9155-125e1c46ef60'}, {name : 'Work Email', uuid : '4c4c379e-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Temporary Email',
                uuid : '4fa8e286-a557-11e6-9155-125e1c46ef60'
            }],
            uuid : '7a15def7-a536-11e6-9155-125e1c46ef60'
        }, {
            name : 'Telecommunications Number',
            purposeTypes : [{name : 'Home Phone', uuid : '52f180c4-a557-11e6-9155-125e1c46ef60'}, {name : 'Personal Cell', uuid : '55cd441d-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Personal SMS',
                uuid : '58d72d80-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Personal Fax', uuid : '5bf0e274-a557-11e6-9155-125e1c46ef60'}, {name : 'Alternate Phone', uuid : '5ed42d9a-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Alternate Cell',
                uuid : '65b17e19-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Alternate SMS', uuid : '6906e7aa-a557-11e6-9155-125e1c46ef60'}, {name : 'Alternate Fax', uuid : '6d003708-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Work Phone',
                uuid : '70ce30df-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Work Cell', uuid : '741b1621-a557-11e6-9155-125e1c46ef60'}, {name : 'Work SMS', uuid : '7745c3e7-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Work Fax',
                uuid : '7ac22c78-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Emergency Phone', uuid : '7df852f1-a557-11e6-9155-125e1c46ef60'}, {name : 'Emergency Cell', uuid : '81256b6d-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Emergency SMS',
                uuid : '846b5568-a557-11e6-9155-125e1c46ef60'
            }, {name : 'Temporary Phone', uuid : 'a4a91b4a-a557-11e6-9155-125e1c46ef60'}, {name : 'Temporary Cell', uuid : 'a93396a2-a557-11e6-9155-125e1c46ef60'}, {
                name : 'Temporary SMS',
                uuid : 'ae395b88-a557-11e6-9155-125e1c46ef60'
            }],
            uuid : '81dbe990-a536-11e6-9155-125e1c46ef60'
        }],
        genderTypes : [{name : 'Female', uuid : 'c8633432-a531-11e6-9155-125e1c46ef60'}, {name : 'Unidentified', uuid : 'fc6d303b-a4ea-11e6-9155-125e1c46ef60'}, {name : 'Male', uuid : 'fcfded02-a531-11e6-9155-125e1c46ef60'}],
        languages : [{family : 'Indo-European', isoCode : 'eng', name : 'English', uuid : '008d1bf0-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'spa',
            name : 'Spanish',
            uuid : '0937fd5e-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Sino-Tibetan', isoCode : 'cmn', name : 'Mandarin', uuid : '0ec9a2a1-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'fra',
            name : 'French',
            uuid : '1335fc11-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Austronesian', isoCode : 'tgl', name : 'Tagalog', uuid : '1853b201-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Austroasiatic',
            isoCode : 'vie',
            name : 'Vietnamese',
            uuid : '1dd57bf7-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Korean', isoCode : 'kor', name : 'Korean', uuid : '22e2acc5-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'deu',
            name : 'German',
            uuid : '270687da-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Afro-Asiatic', isoCode : 'ara', name : 'Arabic', uuid : '2b76a6a4-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'rus',
            name : 'Russian',
            uuid : '2f71c85d-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'inc', name : 'Indic', uuid : '35ef87d7-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'ben',
            name : 'Bengali',
            uuid : '39b99e8c-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'ita', name : 'Italian', uuid : '3d4c8486-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'por',
            name : 'Portuguese',
            uuid : '40c4bdc8-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'hin', name : 'Hindi', uuid : '44182624-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'pol',
            name : 'Polish',
            uuid : '4888c3bc-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Japanese', isoCode : 'jpn', name : 'Japanese', uuid : '4bdc6885-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'fas',
            name : 'Persian',
            uuid : '4fd51907-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'urd', name : 'Urdu', uuid : '5392fe5d-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'guj',
            name : 'Gujarati',
            uuid : '5718a2d6-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'ell', name : 'Greek', uuid : '5a73bd02-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'hbs',
            name : 'Serbo-Croatian',
            uuid : '5d9c7dfa-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Indo-European', isoCode : 'hye', name : 'Armenian', uuid : '60ca9f79-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Afro-Asiatic',
            isoCode : 'heb',
            name : 'Hebrew',
            uuid : '640b98e8-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Austroasiatic', isoCode : 'khm', name : 'Khmer', uuid : '6785f000-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Na-Dene',
            isoCode : 'nav',
            name : 'Navajo',
            uuid : '6b0424ce-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Tai\u2013Kadai', isoCode : 'tha', name : 'Thai', uuid : '6e937194-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Indo-European',
            isoCode : 'yid',
            name : 'Yiddish',
            uuid : '71e3668f-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Tai\u2013Kadai', isoCode : 'lao', name : 'Laotian', uuid : '750e1690-a552-11e6-9155-125e1c46ef60'}, {
            family : 'Dravidian',
            isoCode : 'tam',
            name : 'Tamil',
            uuid : '7a887c23-a552-11e6-9155-125e1c46ef60'
        }, {family : 'Sino-Tibetan', isoCode : 'yue', name : 'Cantonese', uuid : '7cf3f4bc-a552-11e6-9155-125e1c46ef60'}],
        medicalConditionTypes : [{name : 'Diabetic', uuid : '13e97f14-a536-11e6-9155-125e1c46ef60'}, {name : 'Pregnant - Last Trimester', uuid : '2668168e-a536-11e6-9155-125e1c46ef60'}, {
            name : 'Bariatric',
            uuid : '329c6f17-a536-11e6-9155-125e1c46ef60'
        }],
        modeOfTransportation: [{
            name : 'Mode of Transportation',
            serviceCategories : [{
                name : 'Stretcher Vehicle',
                ordinality : 4,
                serviceOfferings : [{name : 'Stretcher Vehicle', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6f-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c26015-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Mass Transit',
                ordinality : 1,
                serviceOfferings : [{name : 'Mass Transit', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d69-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c2600c-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Ambulance',
                ordinality : 5,
                serviceOfferings : [{name : 'Isolette', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d78-ee51-11e6-9155-125e1c46ef60'}, {
                    name : 'Advanced Life Support',
                    serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'},
                    uuid : 'c1750d71-ee51-11e6-9155-125e1c46ef60'
                }, {name : 'CCT', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d76-ee51-11e6-9155-125e1c46ef60'}, {
                    name : 'Basic Life Support',
                    serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'},
                    uuid : 'c1750d74-ee51-11e6-9155-125e1c46ef60'
                }],
                uuid : '07c26016-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Mileage Reimbursement',
                ordinality : 0,
                serviceOfferings : [{name : 'Mileage Reimbursement', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d65-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c25fff-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Wheelchair Vehicle',
                ordinality : 3,
                serviceOfferings : [{name : 'Wheelchair Vehicle', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6d-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c26012-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Air Travel',
                ordinality : 6,
                serviceOfferings : [{name : 'Air Travel', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d7a-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c26019-ee4f-11e6-9155-125e1c46ef60'
            }, {
                name : 'Ambulatory',
                ordinality : 2,
                serviceOfferings : [{name : 'Ambulatory', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6a-ee51-11e6-9155-125e1c46ef60'}],
                uuid : '07c26010-ee4f-11e6-9155-125e1c46ef60'
            }],
            uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'
        }],
        personConnectionTypes : [{name : 'Parent / Family', uuid : 'adc8584b-a536-11e6-9155-125e1c46ef60'}, {name : 'Emergency Contact', uuid : 'b53d02aa-a536-11e6-9155-125e1c46ef60'}, {
            name : 'Nurse / Doctor / Counselor',
            uuid : 'bafe6e3a-a536-11e6-9155-125e1c46ef60'
        }, {name : 'Social / Case Worker / Case Manager', uuid : 'c22825cf-a536-11e6-9155-125e1c46ef60'}, {name : 'Plan / Client', uuid : 'c854800f-a536-11e6-9155-125e1c46ef60'}, {
            name : 'Driver',
            uuid : 'ce1fe6c7-a536-11e6-9155-125e1c46ef60'
        }],
        physicalCharacteristicTypes : [{name : 'Height', uuid : 'eeea982c-a534-11e6-9155-125e1c46ef60'}, {name : 'Weight', uuid : 'f7149892-a534-11e6-9155-125e1c46ef60'}],
        serviceCategories : [{
            name : 'Mileage Reimbursement',
            ordinality : 0,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Mileage Reimbursement', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d65-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c25fff-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Mass Transit',
            ordinality : 1,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Mass Transit', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d69-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c2600c-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Ambulatory',
            ordinality : 2,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Ambulatory', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6a-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c26010-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Wheelchair Vehicle',
            ordinality : 3,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Wheelchair Vehicle', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6d-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c26012-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Stretcher Vehicle',
            ordinality : 4,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Stretcher Vehicle', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d6f-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c26015-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Ambulance',
            ordinality : 5,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Isolette', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d78-ee51-11e6-9155-125e1c46ef60'}, {
                name : 'Advanced Life Support',
                serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'},
                uuid : 'c1750d71-ee51-11e6-9155-125e1c46ef60'
            }, {name : 'CCT', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d76-ee51-11e6-9155-125e1c46ef60'}, {
                name : 'Basic Life Support',
                serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'},
                uuid : 'c1750d74-ee51-11e6-9155-125e1c46ef60'
            }],
            uuid : '07c26016-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Air Travel',
            ordinality : 6,
            serviceCategoryType : {name : 'Mode of Transportation', serviceCategories : [], uuid : 'c1750d59-ee51-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Air Travel', serviceOfferingType : {name : 'Transportation', uuid : '9cff7191-ee52-11e6-9155-125e1c46ef60'}, uuid : 'c1750d7a-ee51-11e6-9155-125e1c46ef60'}],
            uuid : '07c26019-ee4f-11e6-9155-125e1c46ef60'
        }, {
            name : 'Physicians',
            ordinality : 7,
            serviceCategoryType : {name : 'Treatment Type', serviceCategories : [], uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Ophthalmologist / Optometrist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eec5-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Podiatrist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eee1-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Neonatologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eea7-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Obstetrics & Gynecologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eebd-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Chiropractor', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee73-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Vascular Specialist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71aa-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Endocrinologist ', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee83-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Allergist & Immunologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee62-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Plastic Surgeon', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eedd-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Proctologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71a6-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Neurologist / Neurosurgeon', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeb0-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Primary Care Physician',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71a3-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Cardiologist ', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee6f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Dentist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee78-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Psychologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eee9-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Rheumatologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eef1-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Surgeon', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eef8-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Geriatric Medicine',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee94-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Orthopedic', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eecd-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Heptologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff719e-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Orthodonist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eec9-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Pulmonologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eeec-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Ear, Nose & Throat (ENT)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee8a-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Physiatrist - Physical Medicine & Rehab', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eed9-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Urologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71a8-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Nephrologists', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeab-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Optician',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eed5-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Family Practitioner', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee8f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Oncologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eec1-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Internal Medicine', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eea3-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Nurse Practitioner',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eeb9-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Gastroenterologist (GI)', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee99-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Sports Medicine ',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eef4-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Audiologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee69-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Dermatologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee7d-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Hematologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee9e-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Pediatrician',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723eed1-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Psychiatrist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eee5-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Perinatologist',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '9cff71a1-ee52-11e6-9155-125e1c46ef60'
            }, {name : 'Nurse Midwife', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeb5-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Acupuncture',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ee45-df66-11e6-9155-125e1c46ef60'
            }],
            uuid : '1c502abf-df89-11e6-9155-125e1c46ef60'
        }, {
            name : 'Testing',
            ordinality : 8,
            serviceCategoryType : {name : 'Treatment Type', serviceCategories : [], uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Lead Screening / Testing', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef04-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Sleep Study',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef10-df66-11e6-9155-125e1c46ef60'
            }, {name : 'EKG & EEG Testing', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eefc-df66-11e6-9155-125e1c46ef60'}, {
                name : 'PET Scan',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : 'a44b8e44-ee67-11e6-9155-125e1c46ef60'
            }, {name : 'Lab Work', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef00-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Stress Test',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef13-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Mammogram', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef07-df66-11e6-9155-125e1c46ef60'}, {
                name : 'CAT Scan',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : 'a44b8e32-ee67-11e6-9155-125e1c46ef60'
            }, {name : 'PSA Test', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : 'a44b8e49-ee67-11e6-9155-125e1c46ef60'}, {
                name : 'MRI',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef0b-df66-11e6-9155-125e1c46ef60'
            }],
            uuid : '1c502acb-df89-11e6-9155-125e1c46ef60'
        }, {
            name : 'Treatments',
            ordinality : 9,
            serviceCategoryType : {name : 'Treatment Type', serviceCategories : [], uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{name : 'Dialysis', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef5b-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Cranial Technologies',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3624a-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Infusion Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef89-df66-11e6-9155-125e1c46ef60'}, {
                name : 'CSTAR',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef53-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Surgery - Outpatient', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eff7-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Abortion',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef1d-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Occupational Therapy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efa8-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Club House - Treatment for Psych Patients',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef4b-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Methadone Treatment', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef9b-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Chemo Therapy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef47-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Mental Health Group Trip', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d36258-ee69-11e6-9155-125e1c46ef60'}, {
                name : 'Group Therapy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef78-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Experimental Treatment / Procedure', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d3624e-ee69-11e6-9155-125e1c46ef60'}, {
                name : 'Prosthetic Services',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efc7-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Rural Health Clinic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efd6-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Wound Care',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f00a-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Adult Day Health', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef21-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Botox Injections - Non-Cosmetic',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef3b-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Midwife Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef9f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Physical Therapy (PT)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efbe-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Smoking Cessation', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efda-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Support Group',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efee-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Storefront', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8a-ee74-11e6-9155-125e1c46ef60'}, {
                name : 'Federally Qualified Health Centers',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef6f-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Gender Reassignment Surgery', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef73-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Hospice Admission',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef81-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Respiratory Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efd2-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Wellness Visit',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3625f-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Breast Reconstruction', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef3f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Colonoscopy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef4e-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Flu Shots', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d36250-ee69-11e6-9155-125e1c46ef60'}, {
                name : 'Radiology & X-Ray',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efce-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Intermediate Care for the Developmentally Disabled',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef8e-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Substance Abuse - Treatment & Evaluation', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efe2-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Orthotic Services',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efb0-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Autism Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef2e-df66-11e6-9155-125e1c46ef60'}, {
                name : 'HIV / AIDS Testing & Treatment Counseling',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d36254-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Equestrian (Horse) Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef7c-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Family Planning Clinic Services',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef6b-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Infertility Services',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef85-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Comprehensive Outpatient Rehabilitation Facilities (CORF)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3623f-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Speech Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efde-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Cardiac Rehab',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef43-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Substance Abuse - Inpatient', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efea-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Substance Abuse',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3625d-ee69-11e6-9155-125e1c46ef60'
            }, {
                name : 'Music Therapy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efa4-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Early Periodic Screening, Diagnosis & Treatment',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef67-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Dialysis Fistula Replacement', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef5f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Medication Management ',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef97-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Surgery - Hospital', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eff3-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Adult Daycare',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef25-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Aquatic Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef2a-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Blood Transfusion - Type & Match',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef37-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Pharmacy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efb9-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Mental Health',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d36255-ee69-11e6-9155-125e1c46ef60'
            }, {
                name : 'Dietary / Nutritional Counseling',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef63-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Treatment at Veteran\'s Affairs Hospital / Clinic (VA)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f002-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Radiation Treatment',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efca-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Prescribed Pediatric Extended Services (PPEC)',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '75d3625b-ee69-11e6-9155-125e1c46ef60'
            }, {name : 'Massage Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef93-df66-11e6-9155-125e1c46ef60'}, {
                name : 'AA / Self Help Groups',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef18-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Transplant Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723effe-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Dental Treatment - Surgery',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef57-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Preventative Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efc2-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Surgical Follow Up',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723effa-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Weight Control Program', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f006-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Pain Management',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efb4-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Bariatric Surgery',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723ef33-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Optical - Glasses & Contact Lenses - Pick-up',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723efad-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Substance Abuse - Counseling', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efe6-df66-11e6-9155-125e1c46ef60'}],
            uuid : '1c502acf-df89-11e6-9155-125e1c46ef60'
        }, {
            name : 'Non-medical',
            ordinality : 10,
            serviceCategoryType : {name : 'Treatment Type', serviceCategories : [], uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'},
            serviceOfferings : [{
                name : 'Hospital to Residence',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f04a-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Court Ordered Services', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f01c-df66-11e6-9155-125e1c46ef60'}, {
                name : 'County Health Department',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f019-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Hospital to Nursing Home', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f046-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Catholic Charities',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc79-ee74-11e6-9155-125e1c46ef60'
            }, {name : 'Diagnostic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f028-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Hospital to Treatment Facility',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f04e-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Health Education', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f037-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Dentures',
                serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f020-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Hospital Admission', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f03e-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Other - Medical',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f078-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Social - HARP', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc91-ee74-11e6-9155-125e1c46ef60'}, {
                name : 'Workman\'s Compensation',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc98-ee74-11e6-9155-125e1c46ef60'
            }, {name : 'Other - Non-Medical', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f07d-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Urgent Care - Visit',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f0a2-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Psych - Discharge', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f086-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Hospital Discharge',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f043-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Congregate Meals',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f015-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Hearing Aids - Testing, Fitting, Repairs', serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f03b-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Respite',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f08e-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Durable Medical Equipment',
                serviceOfferingType : {name : 'Durable Medical Equipment', uuid : '8e4327fe-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f02c-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Nursing Home to Nursing Home', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f070-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Urgent Care - Discharge',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f09e-df66-11e6-9155-125e1c46ef60'
            }, {name : 'WIC - During Pregnancy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f0b2-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Case Management Visit',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f00d-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Fitness Center', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8f-ee74-11e6-9155-125e1c46ef60'}, {
                name : 'Transportation to the Emergency Room',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f099-df66-11e6-9155-125e1c46ef60'
            }, {name : 'SSi Determination Medical Appointment', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f095-df66-11e6-9155-125e1c46ef60'}, {
                name : 'WIC - Assessment',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f0ad-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Nursing Home to Residence', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f074-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Residence to Nursing Home',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f089-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Lamaze / Birthing Classes', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f06d-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Supportive Employment',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc93-ee74-11e6-9155-125e1c46ef60'
            }, {name : 'Emergency Room - Discharge', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f02f-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Social - TBI',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f092-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Grocery Store', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f033-df66-11e6-9155-125e1c46ef60'}, {
                name : 'WIC - After Pregnancy',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f0a9-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Visitation - Parent visiting child who is hospitalized',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f0a5-df66-11e6-9155-125e1c46ef60'
            }, {
                name : 'Transportation to the Shelter',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc96-ee74-11e6-9155-125e1c46ef60'
            }, {
                name : 'Diabetic Supplies & Education',
                serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f024-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Community Habilitation', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f011-df66-11e6-9155-125e1c46ef60'}, {
                name : 'Psych - Admission',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f082-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Infant Care - Education', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f052-df66-11e6-9155-125e1c46ef60'}, {
                name : 'INSIGHT',
                serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                uuid : '6723f068-df66-11e6-9155-125e1c46ef60'
            }, {name : 'Advisory Meetings', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8c-ee74-11e6-9155-125e1c46ef60'}, {
                name : 'School Services',
                serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                uuid : '274afc86-ee74-11e6-9155-125e1c46ef60'
            }],
            uuid : '1c502ad1-df89-11e6-9155-125e1c46ef60'
        }],
        serviceCoverageOrganizations : [{
            name : 'RI EOHHS',
            serviceCoveragePlans : [{name : 'Temporary Assistance for Needy Families', serviceCoveragePlanClassifications : [], uuid : 'd70b3f01-b687-11e6-9155-125e1c46ef60'}, {
                name : 'Medicaid',
                serviceCoveragePlanClassifications : [{
                    benefitClassificationValues : [],
                    classificationType : {name : 'Id', uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60'},
                    maximum : 1,
                    minimum : 1,
                    name : 'Beneficiary ID',
                    ordinality : 0,
                    uuid : '3235aff4-b6a8-11e6-9155-125e1c46ef60',
                    validationFormat : '^\\d{10}$'
                }, {
                    benefitClassificationValues : [{name : 'CNOM', uuid : 'dc6b9d61-d6f2-11e6-8e0c-0ecb7f4d1c02'}, {name : 'RiteCare', uuid : 'e59eb221-d6f2-11e6-8e0c-0ecb7f4d1c02'}, {
                        name : 'ABD',
                        uuid : 'd37d7735-d6f2-11e6-8e0c-0ecb7f4d1c02'
                    }], classificationType : {name : 'Benefit', uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60'}, maximum : 1, minimum : 0, name : 'Program', ordinality : 1, uuid : '3d266eef-b6a8-11e6-9155-125e1c46ef60'
                }],
                uuid : 'cc2042b9-b687-11e6-9155-125e1c46ef60'
            }, {
                name : 'Elderly Transportation Plan',
                serviceCoveragePlanClassifications : [{
                    benefitClassificationValues : [],
                    classificationType : {name : 'Id', uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60'},
                    maximum : 1,
                    minimum : 0,
                    name : 'Elder Plan ID',
                    ordinality : 0,
                    uuid : 'ebd29f95-1413-4b66-b763-00e93f700666'
                }, {
                    benefitClassificationValues : [{name : 'INSIGHT', uuid : 'ee155081-d6f2-11e6-8e0c-0ecb7f4d1c02'}],
                    classificationType : {name : 'Benefit', uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60'},
                    maximum : 1,
                    minimum : 0,
                    name : 'Benefit',
                    ordinality : 1,
                    uuid : '640df84f-ced3-45f5-a4d3-22326af6aa3f'
                }],
                uuid : 'd1802355-b687-11e6-9155-125e1c46ef60'
            }],
            uuid : 'ba25622b-b687-11e6-9155-125e1c46ef60'
        }],
        serviceCoveragePlanClassificationTypes : [{name : 'Id', uuid : '216d3cdb-b688-11e6-9155-125e1c46ef60'}, {name : 'Group', uuid : '27427c45-b688-11e6-9155-125e1c46ef60'}, {
            name : 'Benefit',
            uuid : '2abf4c30-b688-11e6-9155-125e1c46ef60'
        }, {name : 'Geography', uuid : '2e6d4f9f-b688-11e6-9155-125e1c46ef60'}],
        solicitationIndicatorTypes : [{name : 'No', uuid : 'c62901e5-a534-11e6-9155-125e1c46ef60'}, {name : 'Yes', uuid : 'd30da162-a534-11e6-9155-125e1c46ef60'}],
        specialRequirementTypes : [{name : 'Blind', uuid : '5e59c1ae-a53e-11e6-9155-125e1c46ef60'}, {name : 'Cane/Crutches/Walker', uuid : '64329c8c-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Car Seat',
            uuid : '68ff5eef-a53e-11e6-9155-125e1c46ef60'
        }, {name : 'Deaf', uuid : '6e7b9d90-a53e-11e6-9155-125e1c46ef60'}, {name : 'Door to Door', uuid : '72fafa2c-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Door through Door',
            uuid : '77ed2d97-a53e-11e6-9155-125e1c46ef60'
        }, {name : 'Oxygen', uuid : '7c151038-a53e-11e6-9155-125e1c46ef60'}, {name : 'Ride Alone', uuid : '80887754-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Two man carry down',
            uuid : '851f177e-ff5e-11e6-9155-125e1c46ef60'
        }, {name : 'Service Animal', uuid : '853eab36-a53e-11e6-9155-125e1c46ef60'}, {name : 'Stretcher', uuid : '89635f4b-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Unable to Sign',
            uuid : '8db3d69a-a53e-11e6-9155-125e1c46ef60'
        }, {name : 'Hand to Hand', uuid : '91df9a44-a53e-11e6-9155-125e1c46ef60'}, {name : 'Wheelchair - Manual', uuid : '95f787a7-a53e-11e6-9155-125e1c46ef60'}, {
            name : 'Wheelchair - Motorized',
            uuid : '9a2f5581-a53e-11e6-9155-125e1c46ef60'
        }, {name : 'Wheelchair Lift', uuid : '9e51d50b-a53e-11e6-9155-125e1c46ef60'}, {name : 'Other', uuid : 'a2910e38-a53e-11e6-9155-125e1c46ef60'}],
        treatmentType : [{
            name : 'Treatment Type',
            serviceCategories : [{
                name : 'Testing',
                ordinality : 8,
                serviceOfferings : [{name : 'Lead Screening / Testing', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef04-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Sleep Study',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef10-df66-11e6-9155-125e1c46ef60'
                }, {name : 'EKG & EEG Testing', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eefc-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'PET Scan',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : 'a44b8e44-ee67-11e6-9155-125e1c46ef60'
                }, {name : 'Lab Work', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef00-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Stress Test',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef13-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Mammogram', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef07-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'CAT Scan',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : 'a44b8e32-ee67-11e6-9155-125e1c46ef60'
                }, {name : 'PSA Test', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : 'a44b8e49-ee67-11e6-9155-125e1c46ef60'}, {
                    name : 'MRI',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef0b-df66-11e6-9155-125e1c46ef60'
                }],
                uuid : '1c502acb-df89-11e6-9155-125e1c46ef60'
            }, {
                name : 'Physicians',
                ordinality : 7,
                serviceOfferings : [{
                    name : 'Ophthalmologist / Optometrist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eec5-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Podiatrist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eee1-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Neonatologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eea7-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Obstetrics & Gynecologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eebd-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Chiropractor',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee73-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Vascular Specialist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '9cff71aa-ee52-11e6-9155-125e1c46ef60'}, {
                    name : 'Endocrinologist ',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee83-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Allergist & Immunologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee62-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Plastic Surgeon',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eedd-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Proctologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '9cff71a6-ee52-11e6-9155-125e1c46ef60'}, {
                    name : 'Neurologist / Neurosurgeon',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eeb0-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Primary Care Physician', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '9cff71a3-ee52-11e6-9155-125e1c46ef60'}, {
                    name : 'Cardiologist ',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee6f-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Dentist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee78-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Psychologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eee9-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Rheumatologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eef1-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Surgeon',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eef8-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Geriatric Medicine', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee94-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Orthopedic',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eecd-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Heptologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '9cff719e-ee52-11e6-9155-125e1c46ef60'}, {
                    name : 'Orthodonist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eec9-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Pulmonologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeec-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Ear, Nose & Throat (ENT)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee8a-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Physiatrist - Physical Medicine & Rehab', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eed9-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Urologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '9cff71a8-ee52-11e6-9155-125e1c46ef60'
                }, {name : 'Nephrologists', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeab-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Optician',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eed5-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Family Practitioner', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee8f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Oncologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eec1-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Internal Medicine', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eea3-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Nurse Practitioner',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eeb9-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Gastroenterologist (GI)', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee99-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Sports Medicine ',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eef4-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Audiologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee69-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Dermatologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee7d-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Hematologist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ee9e-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Pediatrician',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723eed1-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Psychiatrist', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eee5-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Perinatologist',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '9cff71a1-ee52-11e6-9155-125e1c46ef60'
                }, {name : 'Nurse Midwife', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eeb5-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Acupuncture',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ee45-df66-11e6-9155-125e1c46ef60'
                }],
                uuid : '1c502abf-df89-11e6-9155-125e1c46ef60'
            }, {
                name : 'Non-medical',
                ordinality : 10,
                serviceOfferings : [{
                    name : 'Hospital to Residence',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f04a-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Court Ordered Services', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f01c-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'County Health Department',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f019-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Hospital to Nursing Home', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f046-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Catholic Charities',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc79-ee74-11e6-9155-125e1c46ef60'
                }, {name : 'Diagnostic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f028-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Hospital to Treatment Facility',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f04e-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Health Education', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f037-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Dentures',
                    serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f020-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Hospital Admission', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f03e-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Other - Medical',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f078-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Social - HARP', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc91-ee74-11e6-9155-125e1c46ef60'}, {
                    name : 'Workman\'s Compensation',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc98-ee74-11e6-9155-125e1c46ef60'
                }, {name : 'Other - Non-Medical', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f07d-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Urgent Care - Visit',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f0a2-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Psych - Discharge', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f086-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Hospital Discharge',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f043-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Congregate Meals',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f015-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Hearing Aids - Testing, Fitting, Repairs',
                    serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f03b-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Respite', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f08e-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Durable Medical Equipment',
                    serviceOfferingType : {name : 'Durable Medical Equipment', uuid : '8e4327fe-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f02c-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Nursing Home to Nursing Home', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f070-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Urgent Care - Discharge',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f09e-df66-11e6-9155-125e1c46ef60'
                }, {name : 'WIC - During Pregnancy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f0b2-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Case Management Visit',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f00d-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Fitness Center', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8f-ee74-11e6-9155-125e1c46ef60'}, {
                    name : 'Transportation to the Emergency Room',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f099-df66-11e6-9155-125e1c46ef60'
                }, {name : 'SSi Determination Medical Appointment', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f095-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'WIC - Assessment',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f0ad-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Nursing Home to Residence', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f074-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Residence to Nursing Home',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f089-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Lamaze / Birthing Classes', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f06d-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Supportive Employment',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc93-ee74-11e6-9155-125e1c46ef60'
                }, {name : 'Emergency Room - Discharge', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f02f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Social - TBI',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f092-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Grocery Store', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f033-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'WIC - After Pregnancy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f0a9-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Visitation - Parent visiting child who is hospitalized',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f0a5-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Transportation to the Shelter',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc96-ee74-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Diabetic Supplies & Education',
                    serviceOfferingType : {name : 'Healthcare Supply', uuid : '8e43280b-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f024-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Community Habilitation', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f011-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Psych - Admission',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f082-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Infant Care - Education', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f052-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'INSIGHT',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f068-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Advisory Meetings', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8c-ee74-11e6-9155-125e1c46ef60'}, {
                    name : 'School Services',
                    serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '274afc86-ee74-11e6-9155-125e1c46ef60'
                }],
                uuid : '1c502ad1-df89-11e6-9155-125e1c46ef60'
            }, {
                name : 'Treatments',
                ordinality : 9,
                serviceOfferings : [{name : 'Dialysis', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef5b-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Cranial Technologies',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3624a-ee69-11e6-9155-125e1c46ef60'
                }, {name : 'Infusion Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef89-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'CSTAR',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef53-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Surgery - Outpatient', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eff7-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Abortion',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef1d-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Occupational Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efa8-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Club House - Treatment for Psych Patients',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef4b-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Methadone Treatment', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef9b-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Chemo Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef47-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Mental Health Group Trip', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d36258-ee69-11e6-9155-125e1c46ef60'}, {
                    name : 'Group Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef78-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Experimental Treatment / Procedure', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d3624e-ee69-11e6-9155-125e1c46ef60'}, {
                    name : 'Prosthetic Services',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efc7-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Rural Health Clinic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efd6-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Wound Care',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f00a-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Adult Day Health', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef21-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Botox Injections - Non-Cosmetic',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef3b-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Midwife Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef9f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Physical Therapy (PT)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efbe-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Smoking Cessation', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efda-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Support Group',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efee-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Storefront', serviceOfferingType : {name : 'Non-Medical', uuid : '8e432815-df7d-11e6-9155-125e1c46ef60'}, uuid : '274afc8a-ee74-11e6-9155-125e1c46ef60'}, {
                    name : 'Federally Qualified Health Centers',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef6f-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Gender Reassignment Surgery', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef73-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Hospice Admission',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef81-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Respiratory Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efd2-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Wellness Visit',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3625f-ee69-11e6-9155-125e1c46ef60'
                }, {name : 'Breast Reconstruction', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef3f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Colonoscopy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef4e-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Flu Shots', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '75d36250-ee69-11e6-9155-125e1c46ef60'}, {
                    name : 'Radiology & X-Ray',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efce-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Intermediate Care for the Developmentally Disabled',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef8e-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Substance Abuse - Treatment & Evaluation',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efe2-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Orthotic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efb0-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Autism Services',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef2e-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'HIV / AIDS Testing & Treatment Counseling',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d36254-ee69-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Equestrian (Horse) Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef7c-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Family Planning Clinic Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef6b-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Infertility Services',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef85-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Comprehensive Outpatient Rehabilitation Facilities (CORF)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3623f-ee69-11e6-9155-125e1c46ef60'
                }, {name : 'Speech Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efde-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Cardiac Rehab',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef43-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Substance Abuse - Inpatient', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efea-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Substance Abuse',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3625d-ee69-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Music Therapy',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efa4-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Early Periodic Screening, Diagnosis & Treatment',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef67-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Dialysis Fistula Replacement', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef5f-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Medication Management ',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef97-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Surgery - Hospital', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723eff3-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Adult Daycare',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef25-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Aquatic Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef2a-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Blood Transfusion - Type & Match',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef37-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Pharmacy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efb9-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Mental Health',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d36255-ee69-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Dietary / Nutritional Counseling',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef63-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Treatment at Veteran\'s Affairs Hospital / Clinic (VA)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723f002-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Radiation Treatment',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efca-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Prescribed Pediatric Extended Services (PPEC)',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '75d3625b-ee69-11e6-9155-125e1c46ef60'
                }, {name : 'Massage Therapy', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723ef93-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'AA / Self Help Groups',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef18-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Transplant Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723effe-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Dental Treatment - Surgery',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef57-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Preventative Services', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efc2-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Surgical Follow Up',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723effa-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Weight Control Program', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723f006-df66-11e6-9155-125e1c46ef60'}, {
                    name : 'Pain Management',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efb4-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Bariatric Surgery',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723ef33-df66-11e6-9155-125e1c46ef60'
                }, {
                    name : 'Optical - Glasses & Contact Lenses - Pick-up',
                    serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'},
                    uuid : '6723efad-df66-11e6-9155-125e1c46ef60'
                }, {name : 'Substance Abuse - Counseling', serviceOfferingType : {name : 'Procedure', uuid : '8e432812-df7d-11e6-9155-125e1c46ef60'}, uuid : '6723efe6-df66-11e6-9155-125e1c46ef60'}],
                uuid : '1c502acf-df89-11e6-9155-125e1c46ef60'
            }],
            uuid : 'e90c8720-df87-11e6-9155-125e1c46ef60'
        }]
    },

    getExceptionMetaDataTypes : [{
        queueTaskItemStatusReason : [{
            uuid : '09d83312-ffc3-11e6-bc64-92361f002671',
            name : 'Not Authorized by Facility/Doctor',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '09d83650-ffc3-11e6-bc64-92361f002671', name : 'Duplicate Trip', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '09d837f4-ffc3-11e6-bc64-92361f002671',
            name : 'Member Ineligible',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '09d83970-ffc3-11e6-bc64-92361f002671', name : 'Incomplete Trip Info', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '09d83aec-ffc3-11e6-bc64-92361f002671',
            name : 'Member Refuses Transport',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '1f8327a8-ffc3-11e6-bc64-92361f002671', name : 'Unable to Transport', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '1f832b40-ffc3-11e6-bc64-92361f002671',
            name : 'No Passenger to Accompany Minor',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '1f833162-ffc3-11e6-bc64-92361f002671', name : 'Out of Service Area', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '1f833388-ffc3-11e6-bc64-92361f002671',
            name : 'State of Emergency/Weather',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '3bae99b8-ffc2-11e6-bc64-92361f002671', name : 'Facility Approved', queueTaskItemStatus : {uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}}, {
            uuid : '3bae9cba-ffc2-11e6-bc64-92361f002671',
            name : 'Override Not Authorized',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {
            uuid : '3bae9e90-ffc2-11e6-bc64-92361f002671',
            name : 'Request Could Not Be Authorized',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '3baea016-ffc2-11e6-bc64-92361f002671', name : 'Advance Notice Not Met', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '3baea19c-ffc2-11e6-bc64-92361f002671',
            name : 'Not Authorized By Plan',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : 'b2d2f300-ffc1-11e6-bc64-92361f002671', name : 'Plan Approved', queueTaskItemStatus : {uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}}, {
            uuid : 'b2d2f3e6-ffc1-11e6-bc64-92361f002671',
            name : 'LogistiCare Error',
            queueTaskItemStatus : {uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}
        }, {uuid : 'b2d2f620-ffc1-11e6-bc64-92361f002671', name : 'Manager Approval', queueTaskItemStatus : {uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}}],
        queueTaskItemType : [{uuid : '1798aaa5-f6d1-11e6-9155-125e1c46ef60', name : 'Prior Authorization Required from Plan'}, {
            uuid : '1798aab2-f6d1-11e6-9155-125e1c46ef60',
            name : 'Treatment Type = Other'
        }, {uuid : '1798aab5-f6d1-11e6-9155-125e1c46ef60', name : 'Trip Limit Met'}, {uuid : '1798aab7-f6d1-11e6-9155-125e1c46ef60', name : 'Waive Advance Notice'}, {
            uuid : '1798aabd-f6d1-11e6-9155-125e1c46ef60',
            name : 'Need MNF - Treatment Type'
        }, {uuid : '1798aabf-f6d1-11e6-9155-125e1c46ef60', name : 'Need MNF - Mode of Transportation'}, {uuid : '1798aac1-f6d1-11e6-9155-125e1c46ef60', name : 'Mileage Limit Exceeded'}, {
            uuid : '1798aac4-f6d1-11e6-9155-125e1c46ef60',
            name : 'Facility - Out of Network'
        }, {uuid : '1798aac6-f6d1-11e6-9155-125e1c46ef60', name : 'From Nursing Facility'}, {uuid : '1798aac9-f6d1-11e6-9155-125e1c46ef60', name : 'Call Facility to Verify'}, {
            uuid : '1798aacb-f6d1-11e6-9155-125e1c46ef60',
            name : 'Mass Transit - <5 Business Days Notice'
        }, {uuid : '1798aacd-f6d1-11e6-9155-125e1c46ef60', name : 'Review Plan of Care'}, {uuid : '1798aacf-f6d1-11e6-9155-125e1c46ef60', name : 'Needs Plan of Care'}, {
            uuid : '1798aad2-f6d1-11e6-9155-125e1c46ef60',
            name : 'Exceeds Max Passengers'
        }],
        queueTaskStatus : [{uuid : '22f6989f-f6cf-11e6-9155-125e1c46ef60', name : 'Pending'}, {uuid : '22f698ab-f6cf-11e6-9155-125e1c46ef60', name : 'Approved'}, {
            uuid : '22f698af-f6cf-11e6-9155-125e1c46ef60',
            name : 'Not Authorized'
        }, {uuid : '22f698b1-f6cf-11e6-9155-125e1c46ef60', name : 'Completed'}, {uuid : '22f698b4-f6cf-11e6-9155-125e1c46ef60', name : 'Not Needed'}],
        queueTaskType : [{uuid : 'c10efd36-f6cd-11e6-9155-125e1c46ef60', name : 'Exception'}, {uuid : 'c10efd43-f6cd-11e6-9155-125e1c46ef60', name : 'Eligibility Review'}, {
            uuid : 'c10efd47-f6cd-11e6-9155-125e1c46ef60',
            name : 'Reminder'
        }, {uuid : 'c10efd49-f6cd-11e6-9155-125e1c46ef60', name : 'Call Back'}],
        queueTaskItemStatus : [{uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}, {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}]
    }],
    getExceptionMetaDataTypesWrap : {
        queueTaskItemStatusReason : [{
            uuid : '09d83312-ffc3-11e6-bc64-92361f002671',
            name : 'Not Authorized by Facility/Doctor',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '09d83650-ffc3-11e6-bc64-92361f002671', name : 'Duplicate Trip', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '09d837f4-ffc3-11e6-bc64-92361f002671',
            name : 'Member Ineligible',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '09d83970-ffc3-11e6-bc64-92361f002671', name : 'Incomplete Trip Info', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '09d83aec-ffc3-11e6-bc64-92361f002671',
            name : 'Member Refuses Transport',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '1f8327a8-ffc3-11e6-bc64-92361f002671', name : 'Unable to Transport', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '1f832b40-ffc3-11e6-bc64-92361f002671',
            name : 'No Passenger to Accompany Minor',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '1f833162-ffc3-11e6-bc64-92361f002671', name : 'Out of Service Area', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '1f833388-ffc3-11e6-bc64-92361f002671',
            name : 'State of Emergency/Weather',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '3bae99b8-ffc2-11e6-bc64-92361f002671', name : 'Facility Approved', queueTaskItemStatus : {uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}}, {
            uuid : '3bae9cba-ffc2-11e6-bc64-92361f002671',
            name : 'Override Not Authorized',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {
            uuid : '3bae9e90-ffc2-11e6-bc64-92361f002671',
            name : 'Request Could Not Be Authorized',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : '3baea016-ffc2-11e6-bc64-92361f002671', name : 'Advance Notice Not Met', queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}}, {
            uuid : '3baea19c-ffc2-11e6-bc64-92361f002671',
            name : 'Not Authorized By Plan',
            queueTaskItemStatus : {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}
        }, {uuid : 'b2d2f300-ffc1-11e6-bc64-92361f002671', name : 'Plan Approved', queueTaskItemStatus : {uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}}, {
            uuid : 'b2d2f3e6-ffc1-11e6-bc64-92361f002671',
            name : 'LogistiCare Error',
            queueTaskItemStatus : {uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}
        }, {uuid : 'b2d2f620-ffc1-11e6-bc64-92361f002671', name : 'Manager Approval', queueTaskItemStatus : {uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}}],
        queueTaskItemType : [{uuid : '1798aaa5-f6d1-11e6-9155-125e1c46ef60', name : 'Prior Authorization Required from Plan'}, {
            uuid : '1798aab2-f6d1-11e6-9155-125e1c46ef60',
            name : 'Treatment Type = Other'
        }, {uuid : '1798aab5-f6d1-11e6-9155-125e1c46ef60', name : 'Trip Limit Met'}, {uuid : '1798aab7-f6d1-11e6-9155-125e1c46ef60', name : 'Waive Advance Notice'}, {
            uuid : '1798aabd-f6d1-11e6-9155-125e1c46ef60',
            name : 'Need MNF - Treatment Type'
        }, {uuid : '1798aabf-f6d1-11e6-9155-125e1c46ef60', name : 'Need MNF - Mode of Transportation'}, {uuid : '1798aac1-f6d1-11e6-9155-125e1c46ef60', name : 'Mileage Limit Exceeded'}, {
            uuid : '1798aac4-f6d1-11e6-9155-125e1c46ef60',
            name : 'Facility - Out of Network'
        }, {uuid : '1798aac6-f6d1-11e6-9155-125e1c46ef60', name : 'From Nursing Facility'}, {uuid : '1798aac9-f6d1-11e6-9155-125e1c46ef60', name : 'Call Facility to Verify'}, {
            uuid : '1798aacb-f6d1-11e6-9155-125e1c46ef60',
            name : 'Mass Transit - <5 Business Days Notice'
        }, {uuid : '1798aacd-f6d1-11e6-9155-125e1c46ef60', name : 'Review Plan of Care'}, {uuid : '1798aacf-f6d1-11e6-9155-125e1c46ef60', name : 'Needs Plan of Care'}, {
            uuid : '1798aad2-f6d1-11e6-9155-125e1c46ef60',
            name : 'Exceeds Max Passengers'
        }],
        queueTaskStatus : [{uuid : '22f6989f-f6cf-11e6-9155-125e1c46ef60', name : 'Pending'}, {uuid : '22f698ab-f6cf-11e6-9155-125e1c46ef60', name : 'Approved'}, {
            uuid : '22f698af-f6cf-11e6-9155-125e1c46ef60',
            name : 'Not Authorized'
        }, {uuid : '22f698b1-f6cf-11e6-9155-125e1c46ef60', name : 'Completed'}, {uuid : '22f698b4-f6cf-11e6-9155-125e1c46ef60', name : 'Not Needed'}],
        queueTaskType : [{uuid : 'c10efd36-f6cd-11e6-9155-125e1c46ef60', name : 'Exception'}, {uuid : 'c10efd43-f6cd-11e6-9155-125e1c46ef60', name : 'Eligibility Review'}, {
            uuid : 'c10efd47-f6cd-11e6-9155-125e1c46ef60',
            name : 'Reminder'
        }, {uuid : 'c10efd49-f6cd-11e6-9155-125e1c46ef60', name : 'Call Back'}],
        queueTaskItemStatus : [{uuid : 'b2d2edba-ffc1-11e6-bc64-92361f002671', name : 'Approved'}, {uuid : 'b2d2f08a-ffc1-11e6-bc64-92361f002671', name : 'Not Approved'}]
    }
};
