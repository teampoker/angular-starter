import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {BackendService} from '../Backend/backend.service';
import {META_DATA_TYPES_MOCK} from './meta-data-types.service.mocks';
import {fromApi as metaDataTypesStateFromApi} from '../../../store/MetaDataTypes/types/meta-data-types-state.model';

@Injectable()

/**
 * Implementation of MetaDataTypesService: serves up common data mainly used for dropdown population across the UI
 */
export class MetaDataTypesService {
    /**
     * MetaDataTypesService constructor
     * @param backendService
     */
    constructor (private backendService : BackendService) {}

    /**
     * retrieve the list of available meta data types from the API
     */
    private getCoreMetaDataTypes() {
        const handle    : string = 'getCoreMetaDataTypes',
              apiUrl    : string = API_CONFIG[handle];

        let dataTypes : any;

        return this.backendService.get(apiUrl, handle, META_DATA_TYPES_MOCK)
            .first()
            .map(response => {
                // store data types
                response.length ? dataTypes = response[0] : dataTypes = response;

                // append missing dropdown types
                dataTypes.salutationTypes = [
                    {
                        uuid : 0,
                        name : 'Mrs.'
                    },
                    {
                        uuid : 1,
                        name : 'Miss'
                    },
                    {
                        uuid : 2,
                        name : 'Ms.'
                    },
                    {
                        uuid : 3,
                        name : 'Mr.'
                    },
                    {
                        uuid : 4,
                        name : 'Rev.'
                    },
                    {
                        uuid : 5,
                        name : 'Dr.'
                    }
                ];

                dataTypes.heightInFeetTypes = [
                    {
                        uuid : 0,
                        name : '0'
                    },
                    {
                        uuid : 1,
                        name : '1'
                    },
                    {
                        uuid : 2,
                        name : '2'
                    },
                    {
                        uuid : 3,
                        name : '3'
                    },
                    {
                        uuid : 4,
                        name : '4'
                    },
                    {
                        uuid : 5,
                        name : '5'
                    },
                    {
                        uuid : 6,
                        name : '6'
                    },
                    {
                        uuid : 7,
                        name : '7'
                    }
                ];

                dataTypes.heightInInchesTypes = [
                    {
                        uuid : 0,
                        name : '0'
                    },
                    {
                        uuid : 1,
                        name : '1'
                    },
                    {
                        uuid : 2,
                        name : '2'
                    },
                    {
                        uuid : 3,
                        name : '3'
                    },
                    {
                        uuid : 4,
                        name : '4'
                    },
                    {
                        uuid : 5,
                        name : '5'
                    },
                    {
                        uuid : 6,
                        name : '6'
                    },
                    {
                        uuid : 7,
                        name : '7'
                    },
                    {
                        uuid : 8,
                        name : '8'
                    },
                    {
                        uuid : 9,
                        name : '9'
                    },
                    {
                        uuid : 10,
                        name : '10'
                    },
                    {
                        uuid : 11,
                        name : '11'
                    },
                    {
                        uuid : 12,
                        name : '12'
                    }
                ];

                // update store
                return dataTypes;
            });
    }

    /**
     * retrieve the list of available reservation exception queue meta data types from the API
     */
    private getExceptionMetaDataTypes() {
        const handle    : string = 'getExceptionMetaDataTypes',
              apiUrl    : string = API_CONFIG[handle];

        let dataTypes : any;

        return this.backendService.get(apiUrl, handle, META_DATA_TYPES_MOCK)
            .first()
            .map(response => {
                // store data types
                response.length ? dataTypes = response[0] : dataTypes = response;

                // update store
                return dataTypes;
            });
    }

    /**
     * retrieve the list of available meta data types from the API
     */
    getMetaDataTypes() {
        const dataTypes : any = {};

        return Observable.forkJoin(
                this.getCoreMetaDataTypes(),
                this.getExceptionMetaDataTypes()
            )
            .map(response => {
                // merge meta data types
                Object.assign(dataTypes, ...response);

                // convert to Immutable
                return metaDataTypesStateFromApi(dataTypes);
            });
    }
}
