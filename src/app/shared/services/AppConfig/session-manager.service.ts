import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

import {BackendService} from '../Backend/backend.service';
import {SESSION_MOCK} from './session-manager.service.mock';

@Injectable()

export class SessionManagerService {
    /**
     * SessionManagerService
     * @param backendService
     */
    constructor(private backendService : BackendService) {}

    /**
     * Retrieves the session configuration information from API.
     * @returns {any} response from session endpoint
     */
    getSessionConfig() : Observable<any> {
        // since this is a root endpoint we have to extract it's base url from the webpack config
        const handle : string    = 'getSessionConfig',
              url    : string    = API_CONFIG[handle];

        return Observable.create(observer => {
            // set search options
            this.backendService.get(url, handle, SESSION_MOCK)
                .first()
                .subscribe(response => {
                    response.length ? response = response[0] : response = response;

                    // return observer
                    observer.next(response);
                }, error => {
                    // return error
                    observer.error(error);
                });
        });
    }
}
