import {TestBed, inject, async} from '@angular/core/testing';
import {RequestOptions} from 'http';
import {Observable} from 'rxjs';

import {BackendService} from '../Backend/backend.service';
import {SessionManagerService} from './session-manager.service';
import {SESSION_MOCK} from './session-manager.service.mock';

class MockBackendService {
    constructor() {}

    expectedResponse : any;

    get(endpoint : string, handle : string, mock : any, data? : any, options? : RequestOptions) {
        return Observable.create(observer => {
            observer.next(this.expectedResponse);
        });
    }

}

describe('Service: SessionManagerService', () => {

    beforeEach(() => {
        // configure the testing module
        TestBed.configureTestingModule({
            providers : [
                {
                    provide  : BackendService,
                    useClass : MockBackendService
                },
                SessionManagerService
            ]
        });
    });

    it('should get config response', async(inject(
        [SessionManagerService, BackendService],
        (sessionService, backend) => {
            // setup the expected mock response from the service
            backend.expectedResponse = SESSION_MOCK.getSessionConfig;

            // call the service method and inspect the results
            sessionService.getSessionConfig().subscribe(response => {
                expect(response).toEqual(SESSION_MOCK.getSessionConfig[0]);
            },
            error => {
                expect(error).toBeUndefined();
            });
        })));
});
