import {async, inject, TestBed} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {APIMockService} from '../Mock/api-mock.service';
import {BackendService} from '../Backend/backend.service';
import {UserService} from './user.service';
import {USER_MOCK} from './user.service.mocks';

describe('UserService', () => {
    let serviceToTest : UserService;

    // setup
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports   : [HttpModule],
            providers : [
                APIMockService,
                BackendService,
                UserService
            ]
        });
    });

    beforeEach(inject([UserService], service => {
        serviceToTest = service;
    }));

    // unit tests
    it('should return a user', () => {
        serviceToTest.getUser().subscribe(response => {
                expect(response).toBeDefined();
                expect(response).toEqual(USER_MOCK.getUserInfo[0], 'Does not match expected mock response');
            },
            error => {
                fail('an error should not have been generated');
            },
            () => {
                // ensure that completed was called
            });
    });

    xit('should update current user profile', async(inject([UserService], userService => {
        userService.updateUser(USER_MOCK.getCurrentUser).subscribe(response => {
            expect(response).toEqual(USER_MOCK.getCurrentUser);
        }, error => {
            expect(error).toEqual(undefined);
        });
    })));

});
