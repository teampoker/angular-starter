import {Injectable} from '@angular/core';
import {Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {BackendService} from '../Backend/backend.service';
import {UserInfoState} from '../../../store/User/types/user-info-state.model';
import {USER_MOCK} from './user.service.mocks';

@Injectable()

/**
 * Implementation of UserService: handles all backend api requests related to user login/logout/update functionality
 */
export class UserService {
    /**
     * UserService constructor
     * @param backendService
     */
    constructor(private backendService : BackendService) {}

    /**
     * Get the user's information from API.
     * @returns {any}
     */
    getUser() {
        return Observable.create(observer => {
            const handle    : string = 'getUserInfo',
                  apiUrl    : string = API_CONFIG[handle];

            this.backendService.get(apiUrl, handle, USER_MOCK)
                .first()
                .subscribe(response => {
                    observer.next(response.length ? response[0] : response);
                    observer.complete();
                }, error => {
                    console.error(error);
                    observer.error(error);
                });
        });
    }

    /**
     * updates current user profile info to api
     * @param userProfile updated user profile data
     * @returns {any}
     */
    updateUser(userProfile : UserInfoState) {
        // we need this to handle the POST properly
        const body = JSON.stringify(Object.assign(userProfile, {})),
            headers = new Headers({'Content-Type': 'application/json'}),
            options = new RequestOptions({headers});

        return Observable.create(observer => {
            this.backendService.put('updateUser', USER_MOCK, body, options)
                .first()
                .subscribe(() => {
                    // update store with user profile data
                    observer.next(userProfile);
                    observer.complete();
                }, error => {
                    // update store with user profile data
                    observer.error(undefined);
                });
        });
    }
}
