import {
    TestBed,
    inject
} from '@angular/core/testing';
import {ToastrService} from 'ngx-toastr';

import {UserNotificationService} from './user-notification.service';
import {EnumAlertType} from '../../../store/Navigation/types/alert-item.model';

/**
 * Mock ToastrService, so we don't deal with any 3rd party bugs...
 */
export class MockToastrService {

    /**
     * mocking property that's used by the handlers
     */
    toastrConfig : any = {maxOpened : 0, timeout: 5000, extendedTimeout: 1000};

    errorCalled : boolean = false;

    error(msg : any) : void {
        this.errorCalled = true;
    }

    infoCalled : boolean = false;

    info(msg : any) : void {
        this.infoCalled = true;
    }

    warningCalled : boolean = false;

    warning(msg : any) : void {
        this.warningCalled = true;
    }

    successCalled : boolean = false;

    success(msg : any) : void {
        this.successCalled = true;
    }
}

describe('Service: UserNotificationService', () => {
    let service         : UserNotificationService,
        toastrService   : MockToastrService,
        testAlert       : any;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers : [
                UserNotificationService,
                {
                    provide  : ToastrService,
                    useClass : MockToastrService
                }
            ]
        });

    });

    /**
     * capture each of the DI objects, we need to inspect during each test
     */
    beforeEach(inject([UserNotificationService, ToastrService], (user, toastr) => {
        service = user;
        toastrService = toastr;
        testAlert = {
            alertType   : EnumAlertType.ERROR,
            message     : 'This is only a test...',
            durable     : false
        };

        // ensure mock has been initialized properly
        expect(toastrService.errorCalled).toEqual(false);
        expect(toastrService.warningCalled).toEqual(false);
        expect(toastrService.infoCalled).toEqual(false);
        expect(toastrService.successCalled).toEqual(false);
    }));

    it('should display errors when ERROR', () => {
        testAlert.alertType = EnumAlertType.ERROR;
        service.handleAlert(testAlert);
        expect(toastrService.errorCalled).toEqual(true);
    });

    it('should display warnings when WARNING', () => {
        testAlert.alertType = EnumAlertType.WARNING;
        service.handleAlert(testAlert);
        expect(toastrService.warningCalled).toEqual(true);
    });

    it('should display information when INFO', () => {
        testAlert.alertType = EnumAlertType.INFO;
        service.handleAlert(testAlert);
        expect(toastrService.infoCalled).toEqual(true);
    });

    it('should display success when SUCCESS', () => {
        testAlert.alertType = EnumAlertType.SUCCESS;
        service.handleAlert(testAlert);
        expect(toastrService.successCalled).toEqual(true);
    });

    it('should not display information when the message is blank', () => {
        testAlert.message = '';
        service.handleAlert(testAlert);
        expect(toastrService.infoCalled).toEqual(false);
        expect(toastrService.warningCalled).toEqual(false);
        expect(toastrService.errorCalled).toEqual(false);
        expect(toastrService.successCalled).toEqual(false);
    });

    it('should not display information when the alert is null', () => {
        testAlert = null;
        service.handleAlert(testAlert);
        expect(toastrService.infoCalled).toEqual(false);
        expect(toastrService.warningCalled).toEqual(false);
        expect(toastrService.errorCalled).toEqual(false);
        expect(toastrService.successCalled).toEqual(false);
    });

});
