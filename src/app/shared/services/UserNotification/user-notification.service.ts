import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';

import {
    AlertItem,
    EnumAlertType
} from '../../../store/Navigation/types/alert-item.model';
import {NotifyErrorStrategy} from './Strategies/notify-error-strategy';
import {INotificationStrategy} from './Strategies/notification-strategy.interface';
import {NotifyWarningStrategy} from './Strategies/notify-warning-strategy';
import {NotifySuccessStrategy} from './Strategies/notify-success-strategy';
import {NotifyInfoStrategy} from './Strategies/notify-info-strategy';

@Injectable()
export class UserNotificationService {
    constructor(private toasterService : ToastrService) {
        // default the notification strategy to informational messages, so as not to raise alarms and harm cupcakes
        this.notificationStrategy = new NotifyErrorStrategy(toasterService);
    }

    /**
     * holds reference to the specific instructions of how to handle an alert
     */
    private notificationStrategy : INotificationStrategy;

    /**
     * Handle the notification / alert message.
     * @param alertMessage
     */
    handleAlert(alertMessage : AlertItem) : void {
        if (alertMessage && alertMessage.message && alertMessage.message.length > 0) {
            switch (alertMessage.alertType) {
                case EnumAlertType.ERROR:
                    this.notificationStrategy = new NotifyErrorStrategy(this.toasterService);
                    break;
                case EnumAlertType.WARNING:
                    this.notificationStrategy = new NotifyWarningStrategy(this.toasterService);
                    break;
                case EnumAlertType.SUCCESS:
                    this.notificationStrategy = new NotifySuccessStrategy(this.toasterService);
                    break;
                default:
                    this.notificationStrategy = new NotifyInfoStrategy(this.toasterService);
                    break;
            }

            this.notificationStrategy.handle(alertMessage);
        }
        else {
            console.warn('alert message emitted, but not handled: ' + JSON.stringify(alertMessage));
        }
    }

}
