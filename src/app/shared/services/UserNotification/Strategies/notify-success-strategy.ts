import {ToastrService} from 'ngx-toastr';

import {
    AlertItem,
    EnumAlertType
} from '../../../../store/Navigation/types/alert-item.model';
import {BaseNotifyStrategy} from './base-strategy';

/**
 * Success
 */
export class NotifySuccessStrategy extends BaseNotifyStrategy {
    constructor(toastrService : ToastrService) {
        super(toastrService);
    }

    handleAlert(alertMessage : AlertItem) : void {
        this.toasterService.success(alertMessage.message, EnumAlertType[alertMessage.alertType], this.config);
    }
}
