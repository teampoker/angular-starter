import {ToastrService} from 'ngx-toastr';

import {
    AlertItem,
    EnumAlertType
} from '../../../../store/Navigation/types/alert-item.model';
import {BaseNotifyStrategy} from './base-strategy';

/**
 * Warnings
 */
export class NotifyWarningStrategy extends BaseNotifyStrategy {
    constructor(toastrService : ToastrService) {
        super(toastrService);
    }

    handleAlert(alertMessage : AlertItem) : void {
        this.toasterService.warning(alertMessage.message, EnumAlertType[alertMessage.alertType], this.config);
    }
}
