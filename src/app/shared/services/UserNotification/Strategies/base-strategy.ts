import {
    ToastrService,
    ToastConfig
} from 'ngx-toastr';

import {INotificationStrategy} from './notification-strategy.interface';
import {AlertItem} from '../../../../store/Navigation/types/alert-item.model';
import {UserNotificationComponent} from '../../../components/UserNotification/user-notification.component';

/**
 * Base class for notification strategies to extend.
 * Things that are common across ALL strategies (like some config stuff) should be specified here.
 */
export abstract class BaseNotifyStrategy implements INotificationStrategy {
    /**
     *
     * @param toasterService all strategies will need access to the service. This forces implementations to follow the pattern.
     */
    constructor(protected toasterService : ToastrService) {
        this.config = new ToastConfig();

        // set default timeout to 10 seconds
        this.config.timeOut     = 10 * 1000;
        this.config.progressBar = true;

        // limit the number of notifications that can be seen at any one time to a number I just pulled from my ear
        this.toasterService.toastrConfig.maxOpened = 3;

    }

    /**
     * Configuration override for each toaster message.
     * These would contain any unique settings on the notifification.
     */
    protected config : ToastConfig;

    protected abstract handleAlert(alertMessage : AlertItem) : void;

    /**
     *
     * @param alertMessage
     */
    handle(alertMessage : AlertItem) : void {
        /* common across all user notifications, if
         *  the alert is intended to remain on screen, set the timeout to zero
         *  so that it doesn't disappear after the default timeout*/
        if (alertMessage.durable) {
            this.config.timeOut         = 0;
            this.config.progressBar     = false;
            this.config.extendedTimeOut = 0;
        }

        // use our custom component for rendering, because, even though the default is pretty... we are prettier...
        this.config.toastComponent = UserNotificationComponent;

        // display notifications in the top center of the screen - aiming for foreheads here...
        this.config.positionClass = 'toast-top-full-width';

        // allow user to dismiss any notification - we like to click things
        this.config.closeButton = true;

        // delegate the rest of the handling to the specific strategy
        this.handleAlert(alertMessage);

    }
}
