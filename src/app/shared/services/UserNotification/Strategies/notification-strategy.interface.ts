import {AlertItem} from '../../../../store/Navigation/types/alert-item.model';
/**
 * Interface that uses the GoF Strategy pattern.
 * Implement this interface when you have a certain way of handling an alert.
 */
export interface INotificationStrategy {
    handle(alertMessage : AlertItem) : void;
}
