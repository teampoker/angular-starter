import {Request, Response} from '@angular/http';
import {Observable} from 'rxjs';
import {IHttpInterceptor} from 'angular2-http-interceptor';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class StandardHttpInterceptor implements IHttpInterceptor {
    /**
     *
     * @param router Used to redirect a user in certain situations.
     */
    constructor(private router : Router) {
        this.isRunningLocal = (window.location.hostname === 'localhost');
    }

    /**
     * Indicator that the app is running on a local instance - typically a developer's machine.
     *
     * Should be === true when running the app locally.
     */
    private isRunningLocal : boolean;

    /***
     * Modifies a URL to make request @ localhost.
     * No other changes should be made to the location. (should include path, query params, etc.)
     *
     * @param url Requested resource location.
     * @returns {string} A new URL that includes a path unique to intercepted URLs.
     */
    private replaceHostname(url : string | Request) : string {
        const PATH_INTERCEPT    : string    = '/intercepted';
        const urlToReturn       : URL       = new URL((url instanceof Request) ? url.url : url);

        // use the current environment's protocol and host (including port, if specified)
        urlToReturn.protocol = window.location.protocol;
        urlToReturn.host     = window.location.host;

        // add a path onto the URL so that the local proxy knows that this needs to be handled
        // which needs to be configured in the webpack dev server local proxy as a path rewrite (see webpack.debug.js)
        if (!urlToReturn.pathname.startsWith(PATH_INTERCEPT)) {
            urlToReturn.pathname = PATH_INTERCEPT + urlToReturn.pathname;
        }

        // return the full URL string to caller
        return urlToReturn.href;
    }

    /***
     * Determines if an Http request resource is hosted locally.
     *
     * @param url The Http request url as Request object, or simply a URL string.
     * @returns {boolean} True if the URL specifies localhost in host, or assumes it by not specifying a host. False
     *     otherwise.
     */
    private isRequestToLocalUrl(url : string | Request) : boolean {
        if (url) {
            const urlToInspect : string           = (url instanceof Request) ? url.url : url;
            const urlSpecifiesLocalhost : boolean = (urlToInspect.indexOf('localhost') > 0);
            const urlAssumesLocalhost : boolean   = (!urlToInspect.startsWith('http'));
            return urlSpecifiesLocalhost || urlAssumesLocalhost;
        }
        return false;
    }

    /**
     * Handle an HTTP request before it is turned over to the browser for execution.
     *
     * @param request
     * @returns {Request}
     */
    before(request : Request) : Request {
        /*
         * set a custom header on all HTTP requests.
         * This is so that the proxy host that handles the API requests understands that the application requests
         * are coming from Angular, and not a browser.  This distinction is important so that the proxy can catch
         * certain HTTP responses coming from the backend (such as API Gateway micro-services) and avoid sending a
         * response we don't want to bother with.  Programming is fun.
         * */
        request.headers.append('X-Requested-With', 'XMLHttpRequest');

        /*
         * default all HTTP requests to request json content
         * specifying this here instead of on the HTTP interceptor to allow
         * any application requests to override this - with the interceptor approach
         * the values can't be overridden there*/
        request.headers.append('Content-Type', 'application/json');

        if (this.isRunningLocal) {
            // intercept any HATEOAS urls that may be pointing to a remote resource
            if (!this.isRequestToLocalUrl(request.url)) {
                request.url = this.replaceHostname(request.url);
            }
        }

        return request;
    }

    /**
     * Handler for HTTP responses received.
     *
     * @param response
     * @returns {Observable<Response>}
     */
    after(response : Observable<Response>) : Observable<any> {
        /*
         * Specify the global (application-wide) handling of HTTP error situations.
         * */
        return response.catch(err => {
            switch (err.status) {
                case 401:   // user not recognized
                    this.router.navigate(['Logout']);
                    break;
                case 403:   // unauthorized to perform the requested action
                    break;
                default:
                    // do nothing?
                    break;
            }
            return Observable.throw(err.json().error || 'HTTP error encountered');
        });
    }
}
