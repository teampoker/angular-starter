import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {
    Http,
    RequestOptions
} from '@angular/http';

import {APIMockService} from '../Mock/api-mock.service';

@Injectable()

export class BackendService {
    constructor (
        private http            : Http,
        private apiMockService  : APIMockService
    ) {}

    /**
     * flag to keep track of when we're hitting a live api
     * @type {boolean}
     */
    private isLive : boolean = false;

    /**
     *
     * @param url actual url to use for live data access to an api
     * @param handle endpoint name that maps to an actual api endpoint location
     * @param mock mock data for the endpoint in question
     * @param data any data params to be passed with the http call
     * @param options http request options
     *
     * @returns {any} http client for consuming service to use
     */
    private getEndPoint(url : string, handle : string, mock : any, data : any, options? : RequestOptions) {
        let endpoint;

        // go ahead and turn off random error generation
        this.apiMockService.utils.setRandomErrorsConfig(false);

        // determine appropriate endpoint to access
        if (ENV.indexOf(':mock') !== -1 || ENV === 'test-watch' || ENV === 'test') {
            // set isLive flag to true
            this.isLive = false;

            // return mocked api endpoint
            endpoint = this.apiMockService.mock.createMock(mock[handle]);
        }
        else if (ENV.indexOf(':wrap') !== -1) {
            // set isLive flag to true
            this.isLive = true;

            // return wrapped api endpoint
            endpoint = this.apiMockService.wrap.createWrap(url, mock[handle + 'Wrap'], data, options);
        }
        else {
            // set isLive flag to true
            this.isLive = true;

            // return live api endpoint
            endpoint = {
                get : () => {
                    if (data !== undefined) {
                        return this.http.get(url + data);
                    }
                    else {
                        return this.http.get(url, options);
                    }
                },
                getAll : () => {
                    if (data !== undefined) {
                        return this.http.get(url + data, options);
                    }
                    else {
                        return this.http.get(url, options);
                    }
                },
                post : () => {
                    if (options !== undefined) {
                        return this.http.post(url, data, options);
                    }
                    else {
                        return this.http.post(url, data);
                    }
                },
                put    : () => this.http.put(url, data, options),
                remove : () => this.http.delete(url, data)
            };
        }

        return endpoint;
    }

    /**
     * perform an HTTP GET operation against appropriate backend type (mock, wrap, or live)
     *
     * @param endpoint actual url to use for live data access to an api
     * @param handle name that maps to an actual api endpoint location
     * @param mock mock data for the endpoint in question
     * @param data any data params to be passed with the http call
     * @param options http request options
     *
     * @returns {Observable<any>}
     */
    get(endpoint : string, handle : string, mock : any, data? : any, options? : RequestOptions) {
        const api : any = this.getEndPoint(endpoint, handle, mock, data, options);

        return Observable.create(observer => {
            if (typeof api !== 'undefined') {
                return api.getAll()
                    .map(res => res.json())
                    .first()
                    .subscribe(res => {
                        observer.next(res);
                    }, error => {
                        const msg : any = error.message === undefined ? error : error.message;
                        observer.error(msg);
                    });
            }
            else {
                // indicate there was a problem
                observer.error('no configuration data present for specified endpoint');
            }
        });
    }

    /**
     * perform an HTTP POST operation against appropriate backend type (mock, wrap, or live)
     *
     * @param endpoint name that maps to an actual api endpoint location
     * @param handle name that maps to an actual api endpoint location
     * @param mock mock data for the endpoint in question
     * @param data any data params to be passed with the http call
     * @param options http request options
     *
     * @returns {Observable<any>}
     */
    post(endpoint : string, handle : string, mock : any, data? : any, options? : RequestOptions) {
        const api : any = this.getEndPoint(endpoint, handle, mock, data, options);

        return Observable.create(observer => {
            if (typeof api !== 'undefined') {
                return api.post()
                    .map(res => res.json())
                    .first()
                    .subscribe(res => {
                        observer.next(res);
                    }, error => {
                        const msg : any = error.message === undefined ? error : error.message;
                        observer.error(msg);
                    });
            }
            else {
                // indicate there was a problem
                observer.error('no configuration data present for specified endpoint');
            }
        });
    }

    /**
     * perform an HTTP PUT operation against appropriate backend type (mock, wrap, or live)
     *
     * @param endpoint actual url to use for live data access to an api
     * @param handle name that maps to an actual api endpoint location
     * @param mock mock data for the endpoint in question
     * @param data any data params to be passed with the http call
     * @param options http request options
     *
     * @returns {Observable<any>}
     */
    put(endpoint : string, handle : string, mock : any, data? : any, options? : RequestOptions) {
        const api : any = this.getEndPoint(endpoint, handle, mock, data, options);

        return Observable.create(observer => {
            if (typeof api !== 'undefined') {
                return api.put()
                    .map(res => res.json())
                    .first()
                    .subscribe(res => {
                        observer.next(res);
                    }, error => {
                        const msg : any = error.message === undefined ? error : error.message;
                        observer.error(msg);
                    });
            }
            else {
                // indicate there was a problem
                observer.error('no configuration data present for specified endpoint');
            }
        });
    }

    /**
     * perform an HTTP DELETE operation against appropriate backend type (mock, wrap, or live)
     *
     * @param endpoint name that maps to an actual api endpoint location
     * @param handle name that maps to an actual api endpoint location
     * @param mock mock data for the endpoint in question
     * @param data any data params to be passed with the http call
     * @param options http request options
     *
     * @returns {Observable<any>}
     */
    remove(endpoint : string, handle : string, mock : any, data? : any, options? : RequestOptions) {
        const api : any = this.getEndPoint(endpoint, handle, mock, data, options);

        return Observable.create(observer => {
            if (typeof api !== 'undefined') {
                return api.remove()
                    .map(res => res.json())
                    .first()
                    .subscribe(res => {
                        observer.next(res);
                    }, error => {
                        const msg : any = error.message === undefined ? error : error.message;
                        observer.error(msg);
                    });
            }
            else {
                // indicate there was a problem
                observer.error('no configuration data present for specified endpoint');
            }
        });
    }
}
