import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

/**
 * ng2-translate
 */
import {TranslateModule} from 'ng2-translate/ng2-translate';

/**
 * ng2-bootstrap
 */
import {DropdownModule} from 'ng2-bootstrap';

/**
 * angular2-text-mask
 */
import {TextMaskModule} from 'angular2-text-mask';

/**
 * shared components
 */
import {SvgIconComponent} from './components/SvgIcon/svg-icon.component';
import {AppFooterComponent} from './components/AppFooter/app-footer.component';
import {SearchBoxComponent} from './components/SearchBox/search-box.component';
import {DatePickerComponent} from './components/DatePicker/date-picker.component';
import {TimePickerComponent} from './components/TimePicker/time-picker.component';
import {DecisionGroupComponent} from './components/DecisionGroup/decision-group.component';
import {SectionHeaderComponent} from './components/SectionHeader/section-header.component';
import {DashboardCardComponent} from './components/DashboardCard/dashboard-card.component';
import {ReservationCardComponent} from './components/ReservationCard/reservation-card.component';
import {PaginationComponent} from './components/Pagination/pagination.component';
import {PersonFinderComponent} from './components/PersonFinder/person-finder.component';
import {TooltipComponent} from './components/Tooltip/tooltip.component';
import {TaskListComponent} from './components/TaskList/task-list.component';
import {BeneficiaryConnectionPhoneComponent} from './components/BeneficiaryConnectionPhone/beneficiary-connection-phone.component';
import {BeneficiaryConnectionComponent} from './components/BeneficiaryConnection/beneficiary-connection.component';
import {AutoCompleteComponent} from './components/AutoComplete/auto-complete.component';
import {ConnectionTypeNameComponent} from './components/ConnectionTypeName/connection-type-name.component';
import {UserNotificationComponent} from './components/UserNotification/user-notification.component';
import {PageLoadingOverlayComponent} from './components/PageLoadingOverlay/page-loading-overlay.component';
import {BeneficiaryNotesComponent} from './components/BeneficiaryNotes/beneficiary-notes.component';
import {ReservationCancelModalComponent} from './components/ReservationCancelModal/reservation-cancel-modal.component';

/**
 * shared directives
 */
import {AutoCompleteDirective} from './directives/AutoComplete/auto-complete.directive';
import {GooglePlacesDirective} from './directives/GooglePlaces/google-places.directive';

/**
 * shared pipes
 */
import {CalculateAgeAtDeathPipe} from './pipes/CalculateAgeAtDeath/calculate-age-at-death.pipe';
import {CalculateYearsFromTodayPipe} from './pipes/CalculateYearsFromToday/calculate-years-from-today.pipe';
import {CurrencyFormatPipe} from './pipes/CurrencyFormat/currency-format.pipe';
import {DateTimeFormatPipe} from './pipes/DateTimeFormat/date-time-format.pipe';
import {PhoneFormatPipe} from './pipes/PhoneFormat/phone-format.pipe';
import {DateDisplayFormatPipe} from './pipes/DateDisplayFormat/date-display-format.pipe';
import {PopoverContentComponent} from './components/PopoverContent/popover-content.component';
import {PopoverDirective} from './directives/Popover/popover.directive';

/**
 * Shared application module, essentially wrapping up anything in our app/shared directory.  Note: This Module only
 * imports dependencies needed by components in the 'declarations' array.  Angular, however, lets us re-export other
 * dependencies for consumption by other modules, even though they were not explicitly imported here
 */
@NgModule({
    imports         : [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        DropdownModule.forRoot(),
        TextMaskModule
   ],
    declarations    : [
        CalculateAgeAtDeathPipe,
        CalculateYearsFromTodayPipe,
        CurrencyFormatPipe,
        DateTimeFormatPipe,
        PhoneFormatPipe,
        DateDisplayFormatPipe,
        SvgIconComponent,
        AppFooterComponent,
        SearchBoxComponent,
        DatePickerComponent,
        TimePickerComponent,
        DecisionGroupComponent,
        SectionHeaderComponent,
        DashboardCardComponent,
        PaginationComponent,
        PersonFinderComponent,
        TooltipComponent,
        TaskListComponent,
        BeneficiaryConnectionPhoneComponent,
        BeneficiaryConnectionComponent,
        AutoCompleteComponent,
        AutoCompleteDirective,
        ConnectionTypeNameComponent,
        UserNotificationComponent,
        GooglePlacesDirective,
        ReservationCardComponent,
        PopoverContentComponent,
        PopoverDirective,
        PageLoadingOverlayComponent,
        BeneficiaryNotesComponent,
        ReservationCancelModalComponent
   ],
    exports         : [
        CommonModule,
        RouterModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        DropdownModule,
        TextMaskModule,
        CalculateAgeAtDeathPipe,
        CalculateYearsFromTodayPipe,
        CurrencyFormatPipe,
        DateTimeFormatPipe,
        DateDisplayFormatPipe,
        PhoneFormatPipe,
        SvgIconComponent,
        AppFooterComponent,
        SearchBoxComponent,
        PaginationComponent,
        PersonFinderComponent,
        DatePickerComponent,
        TimePickerComponent,
        DecisionGroupComponent,
        SectionHeaderComponent,
        DashboardCardComponent,
        TooltipComponent,
        TaskListComponent,
        BeneficiaryConnectionPhoneComponent,
        BeneficiaryConnectionComponent,
        AutoCompleteComponent,
        AutoCompleteDirective,
        ConnectionTypeNameComponent,
        GooglePlacesDirective,
        ReservationCardComponent,
        PopoverContentComponent,
        PopoverDirective,
        PageLoadingOverlayComponent,
        BeneficiaryNotesComponent,
        ReservationCancelModalComponent
    ],
    entryComponents : [
        AutoCompleteComponent
    ]
})

export class SharedModule {

}
