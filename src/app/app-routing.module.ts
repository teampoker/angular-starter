import {NgModule} from '@angular/core';
import {
    RouterModule,
    PreloadAllModules
} from '@angular/router';

import {NoContentComponent} from './no-content.component';
import {LogoutComponent} from './shared/components/Logout/logout.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
                {
                    path            : 'Eligibility',
                    loadChildren    : './areas/Eligibility/eligibility.module#EligibilityModule'
                },
                {
                    path            : 'Reservation',
                    loadChildren    : './areas/Reservation/reservation.module#ReservationModule'
                },
                {
                    path            : 'Search',
                    loadChildren    : './areas/Search/search.module#SearchModule'
                },
                {
                    path            : 'Beneficiary',
                    loadChildren    : './areas/Beneficiary/beneficiary.module#BeneficiaryModule'
                },
                {
                    path            : 'ExceptionQueue',
                    loadChildren    : './areas/ExceptionQueue/exception-queue.module#ExceptionQueueModule'
                },

                {
                    path        : 'Logout',
                    component   : LogoutComponent
                },
                /**
                 * default route i.e. route used when no matching route found
                 */
                {
                    path        : '',
                    redirectTo  : 'Dashboard',
                    pathMatch   : 'full'
                },

                /**
                 * 404 handler
                 */
                {
                    path        : '**',
                    component   : NoContentComponent
                }
            ],

            /**
             * enforce use of HashLocationStrategy
             * and preload all lazy loaded modules
             */
            {
                useHash             : true,
                preloadingStrategy  : PreloadAllModules
            })
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {

}
