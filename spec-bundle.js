Error.stackTraceLimit = Infinity;

// polyfills
// fixes for non ES6 browsers
import 'core-js/client/shim';
import 'reflect-metadata';

// Typescript emit HELPERS polyfill
import 'ts-helpers';

import 'zone.js/dist/zone';
import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/proxy';
import 'zone.js/dist/sync-test';
import 'zone.js/dist/jasmine-patch';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';

// RxJS
import 'rxjs/Rx';

// prime Angular testing framework
import * as testing from '@angular/core/testing';
import * as browser from '@angular/platform-browser-dynamic/testing';

// set global jasmine async timeout
jasmine.DEFAULT_TIMEOUT_INTERVAL = 15000;

/**
 * instead of manually maintain a long list of require or import statements
 * we dynamically require a list of all source files found in the 'src' directory
 * matching 'spec.ts'
 */
var appContext = require.context('./src', true, /\.spec\.ts/);

appContext.keys().map(appContext);

testing.TestBed.initTestEnvironment(
    browser.BrowserDynamicTestingModule,
    browser.platformBrowserDynamicTesting()
);
