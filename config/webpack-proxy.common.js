/**
 * Pulled into a separate config file because the configuration of the local dev server is largely the same, regardless of the
 * configuration you've selected.
 *
 * The common configuration is done here, and then each item can be overridden in the specific webpack.*.js config file used for your build.
 */
module.exports = {
    /**
     * webpack-dev-server configuration
     * Reference: http://webpack.github.io/docs/configuration.html#devserver
     * Reference: http://webpack.github.io/docs/webpack-dev-server.html
     */
    devServer : {
        quiet :              true,
        port :               3000,
        inline :             true,
        host :               'localhost',
        historyApiFallback : true,
        watchOptions :       {
            aggregateTimeout : 300,
            poll :             1000
        },
        proxy :              {
            '/api' :          {
                target :             'https://gw.devcloud.angular-starter.com:8443/',
                changeOrigin :       true,     // changes the host header to target URL
                https :              true,
                secure :             false,    // allow our browser to use http ://localhost/ --> no SSL
                rejectUnauthorized : false,
                headers :            {
                    Origin :  'https://angular-starter-web-app-development.app.dev.ose.angular-starter.com',
                    Referer : 'https://angular-starter-web-app-development.app.dev.ose.angular-starter.com/'
                },
                pathRewrite :        {
                    '^/api/' : '/ltng/',
                },
                logLevel :           'debug'
            },
            /* configuring the local proxy to handle any intercepted HATEOAS urls and redirect to gateway */
            '/intercepted/' : {
                target :             'https://gw.devcloud.angular-starter.com:8443/',
                changeOrigin :       true,     // changes the host header to target URL
                https :              true,
                secure :             false,    // allow our browser to use http ://localhost/ --> no SSL
                rejectUnauthorized : false,
                headers :            {
                    Origin :  'https://angular-starter-web-app-development.app.dev.ose.angular-starter.com',
                    Referer : 'https://angular-starter-web-app-development.app.dev.ose.angular-starter.com/'
                },
                pathRewrite :        {
                    '^/intercepted/' : '/'
                },
                logLevel :           'debug'
            },
            /* standing up a local endpoint for the session service to end the user's session */
            '/logout' :       {
                target :             'https://angular-starter-web-app-development.app.dev.ose.angular-starter.com/logout',
                changeOrigin :       true,     // changes the host header to target URL
                https :              true,
                secure :             false,    // allow our browser to use http ://localhost/ --> no SSL
                rejectUnauthorized : false,
                headers :            {
                    Origin :  'https://angular-starter-web-app-development.app.dev.ose.angular-starter.com',
                    Referer : 'https://angular-starter-web-app-development.app.dev.ose.angular-starter.com/'
                },
                logLevel :           'debug'
            }
        }
    }
};
