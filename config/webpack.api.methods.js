/**
 * Created by gregory.stratton on 12/20/2016.
 *
 * Pulled into a separate config file because the methods are
 * required / defined regardless of what configuration is needed.
 *
 * Rather than continue defining them in each configuration file for Web Pack,
 * it's easier to have 1 place where we define these methods.
 */
module.exports = {
    apiMethods : {
        /* dropdown types */
        getCoreMetaDataTypes : 'api/v1/person/metadataTypes',

        /* reservation exception queue metadata */
        getExceptionMetaDataTypes : 'api/v1/reservationException/metadata',

        /* session manager endpoint */
        getSessionConfig : '/timeoutLength',

        /* -----------------------------------------------
            search
         */
        /* search types api */
        getSearchTypes : 'api/v1/searchTypes',

        /* -----------------------------------------------
            dashboard
         */
        /* getDashBoard */
        getDashBoard : 'api/dashboard/',

        /* getTaskList */
        getTaskList : 'api/taskList',

        /* callActivity */
        getCallActivity : 'api/callActivity/',

        /* -----------------------------------------------
            beneficiary
         */
        /* getBeneficiary */
        getBeneficiary : 'api/v1/beneficiary/',

        /* beneficiary search */
        beneficiarySearch : 'api/v1/beneficiary?q=',

        /* new beneficiary profile */
        createBeneficiary : 'api/v1/beneficiary',

        /* -----------------------------------------------
            person
         */
        /* update beneficiary person */
        updateBeneficiaryPerson : 'api/v1/person/',

        /* -----------------------------------------------
            contact
         */
        /* update beneficiary contact mechanisms */
        updateBeneficiaryContactMechanisms : 'api/v1/person/${uuid}/contactMechanisms',

        /* -----------------------------------------------
            plan
         */
        /* update beneficiary service coverages */
        updateBeneficiaryServiceCoverages : 'api/v1/person/${uuid}/serviceCoverages',

        /* -----------------------------------------------
            reservation
         */
        /* reservation */
        getReservation : 'api/v1/reservation/',

        /* list of reservations */
        getReservations : 'api/v1/reservation/person/',

        /* cancel reservation */
        cancelReservation : 'api/v1/reservation/${uuid}/cancel',

        /* createReservation */
        createReservation : 'api/v1/reservation',

        /* editReservation */
        editReservation : 'api/v1/reservation/${uuid}',

        /* reservation leg mileage */
        getReservationLegMileage : 'api/v1/reservation/mileage',

        /* top reservation pickup locations */
        getReservationPickUpLocations : 'api/v1/reservation/person/${uuid}/pickUpLocations',

        /* top reservation dropoff locations */
        getReservationDropOffLocations : 'api/v1/reservation/person/${uuid}/dropOffLocations',

        /* get the pickup time fro a reservation */
        getReservationPickupTime : 'api/v1/reservation/pickUpTime',

        getReservationEventReasons : 'api/v1/reservation/eventReasons',

        getMassTransitRoutes : 'api/v1/reservation/massTransitRoute',

        /* -----------------------------------------------
            rules
         */
        /* treatmentType */
        validateTreatmentType : 'api/v1/rules/treatment',

        /* treatmentType */
        validateLocation : 'api/v1/rules/location',

        /* additional passengers */
        validateEscort : 'api/v1/rules/escort',

        /* mode of transportation */
        validateModeOfTransportation : 'api/v1/rules/modeOfTransportation',

        /* -----------------------------------------------
            eligibility
         */
        /* getEligibilityRequests */
        getEligibilityRequests : 'api/v1/eligibility',

        /* getEligibilityQueue */
        getEligibilityQueue : 'api/v1/eligibilityQueue',

        /* -----------------------------------------------
         forms
         */
        /* getFormTypes */
        getFormTypes : 'api/v1/beneficiary/form/types',

        /* getAuthorizationReasons */
        getAuthorizationReasons : 'api/v1/beneficiary/form/authorizationReasons',

        /* getForms */
        getForms : 'api/v1/beneficiary/{$personUuid}/forms',

        /* getFormDetails */
        getFormDetails : 'api/v1/beneficiary/${personUuid}/form/{$formUuid}',

        /* createForm */
        createForm : 'api/v1/beneficiary/${personUuid}/form',

        /* -----------------------------------------------
         Exception queue endpoints
         */
        /* getExceptionQueue */

        getExceptionQueue : 'api/v1/reservationException',

        /* claimExceptions */
        claimExceptions : 'api/v1/reservationException/assign',

        /* unassignExceptions */
        unassignExceptions : 'api/v1/reservationException/unassign',

        /* submitExceptionReview */
        submitExceptionReview : 'api/v1/reservationException/approval',

        /* User endpoints */
        getUserInfo : '/userInformation',

        /* -----------------------------------------------
            logout
         */
        /* logout session server-side */
        logoutUser : 'logout'
    }
};
